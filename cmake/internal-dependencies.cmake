if ( NOT TARGET cuipron )
    FetchContent_Declare(
        CUIPRON
        GIT_REPOSITORY https://gitlab.com/el.chris/cuipron.git
    )
    FetchContent_GetProperties(cuipron)
    if(NOT cuipron_POPULATED)
        message(STATUS "updating: cuipron")
        FetchContent_Populate(CUIPRON)

        add_subdirectory(${cuipron_SOURCE_DIR} ${cuipron_BINARY_DIR})
    endif()
endif()
