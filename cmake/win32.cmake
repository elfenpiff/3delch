# set( CMAKE_BUILD_TYPE "Release")
# set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /GR /MT /std:c++latest" )
# set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT /DRELEASE /std:c++latest")
# set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MT /std:c++latest")
# set( CMAKE_BUILD_PARALLEL_LEVEL 16 )

# Compiler Flags
set ( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W1 /MP /std:c++latest" )
if ( ADD_LUA_BINDINGS )
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /DADD_LUA_BINDINGS" )
endif ( ADD_LUA_BINDINGS )

# VC specific compiler flags
# set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /GR /MT /std:c++latest" )
# set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MP /MT /DRELEASE /std:c++latest")
# set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MP /MT /std:c++latest")

