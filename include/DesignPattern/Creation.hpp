#pragma once

#include "buildingBlocks/elExpected.hpp"

#include <memory>
#include <optional>

namespace el3D
{
namespace DesignPattern
{
/*
@brief Howto apply the Creation DesignPattern
    1. Your class has to publicly inherit from Creation and give itself as a
        template argument - like this
        > class MyClass :
                public DesignPattern::Creation<MyClass, MyClass_ErrorType>

    2. Befriend the inherited Creation pattern
        > friend struct DesignPattern::Creation<MyClass, MyClass_ErrorType>;

    3. Make all your constructors private to ensure the correct usage of the
        creation pattern by the user

    4. Every Constructor (except copy/move ctor) has to set the member variables
        > create_isInitialized = true;

        if the construction was successful or
        > create_isInitialized = false;
        > create_error = myErrorValue;

        if the construction failed.

    5. Additionally, you can use MyClass::result_t as a user to have a more
        comfortable access to the return value of the creation patterns Create
        method. Actually, it is utils::elExpected<MyClass, MyClass::error_t>
*/
template < typename BaseClass, typename ErrorType >
struct Creation
{
// added since lua cannot handle universal references
#ifdef ADD_LUA_BINDINGS
    template < typename... Targs >
    static std::shared_ptr< BaseClass >
    Lua_Create( const Targs &... args )
    {
        BaseClass *                  newClassPointer = new BaseClass( args... );
        std::shared_ptr< BaseClass > newClass( newClassPointer );

        if ( !newClass->create_isInitialized )
            return nullptr;
        else
            return newClass;
    }
#endif

    using result_t = bb::elExpected< std::unique_ptr< BaseClass >, ErrorType >;
    template < typename... Targs >
    static result_t
    Create( Targs &&... args )
    {
        std::unique_ptr< BaseClass > newClass(
            new BaseClass( std::forward< Targs >( args )... ) );

        if ( !newClass->create_isInitialized )
            return result_t::CreateError( newClass->create_error );
        else
            return result_t::CreateValue( std::move( newClass ) );
    }

    bool      create_isInitialized{ false };
    ErrorType create_error;
};

namespace internal
{
template < template < typename... > class C, typename... Ts >
std::true_type
usesCreationImpl( const C< Ts... > * );

template < template < typename... > class C >
std::false_type
usesCreationImpl( ... );
} // namespace internal

template < typename T >
constexpr bool usesCreation = decltype(
    internal::usesCreationImpl< Creation >( std::declval< T * >() ) )();

} // namespace DesignPattern
} // namespace el3D
