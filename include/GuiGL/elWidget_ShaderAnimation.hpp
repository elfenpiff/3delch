#pragma once

#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "OpenGL/elUniformBufferObject.hpp"
#include "RenderPipeline/elRenderer_ShaderTexture.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Entry;
class elWidget_ShaderAnimation : public elWidget_base
{
  public:
    enum class Error
    {
        InvalidShaderFile,
        UnableToCreateShaderTexture,
        UnableToCreateUniformBufferObject,
        UnableToAttachTexture
    };

    static constexpr uint64_t INVALID_ANIMATION_ID =
        std::numeric_limits< uint64_t >::max();

    elWidget_ShaderAnimation( const constructor_t &constructor );
    virtual ~elWidget_ShaderAnimation();

    virtual bb::elExpected< Error >
    SetUp( const std::string &shaderFile,
           const std::string &shaderGroup            = "default",
           const bool         doPerformDynamicResize = true,
           const std::vector< OpenGL::textureProperties_t >
               outputTextureProperties = { OpenGL::textureProperties_t{} },
           const GLsizei sizeX = 512, const GLsizei sizeY = 512,
           const bool isFlipFlop = false ) noexcept;

    virtual void
    Draw( const renderPass_t renderPass ) noexcept override;
    virtual void
    Reshape() override;

    HighGL::elShaderTexture *
    GetShaderTexture() noexcept;
    virtual void
    SetTextureUniformBlockData( const size_t arrayIndex, const size_t varIndex,
                                const void * varData,
                                const size_t dataSize ) noexcept;

    template < typename T >
    uint64_t
    AnimateUniform( const GLint uniformID, const T &source,
                    const T &destination, const float speed ) noexcept;
    template < typename T >
    uint64_t
    UpdateAnimatedUniform( const uint64_t animationID, const GLint uniformID,
                           const T &destination, const float speed ) noexcept;

    virtual void
    SetDoInfinitLoop( const bool ) noexcept;
    virtual void
    Start() noexcept;
    virtual void
    ReverseStart() noexcept;
    virtual void
    Stop() noexcept;

  protected:
    void
    UpdateTime() noexcept;
    void
    RemoveShaderTexture() noexcept;

    template < typename T >
    using linearAnimation_t = animation::elAnimate_SourceDestination<
        T, animation::interpolation::Linear >;

    glm::vec2                                  shaderSize{ 0, 0 };
    std::string                                shaderFile;
    std::unique_ptr< HighGL::elShaderTexture > animation;
    bool                                       doPerformDynamicResize;
    std::optional< size_t >                    shaderRendererID{ std::nullopt };
    bool                                       doInfinitLoop = true;
    bool                                       doRun         = true;
    bool                                       doRunReverse  = false;
    std::pair< units::Time, units::Time >      animationRange{
        units::Time::Seconds( 0.0 ), units::Time::Seconds( 1.0 ) };
    units::Time                                      startTime;
    std::unique_ptr< OpenGL::elUniformBufferObject > animationUniformBuffer;

    bb::elUniqueIndexVector< std::unique_ptr< animation::elAnimate_base > >
                                                   animatedUniforms;
    bb::product_ptr< OpenGL::textureAttachment_t > attachedTexture;
};
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/elWidget_ShaderAnimation.inl"
