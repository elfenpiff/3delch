#pragma once

#include "GuiGL/widget_ptr.hpp"

namespace el3D
{
namespace GuiGL
{
namespace handler
{
class widgetFactory;
}

class elWidget_base;
class elWidget_Accordion;
class elWidget_Button;
class elWidget_Checkbutton;
class elWidget_ColorPicker;
class elWidget_Dropbutton;
class elWidget_Entry;
class elWidget_FileBrowser;
class elWidget_Graph;
class elWidget_Grid;
class elWidget_Image;
class elWidget_InfoMessage;
class elWidget_KeyValuePair;
class elWidget_Label;
class elWidget_Listbox;
class elWidget_Menu;
class elWidget_Menubar;
class elWidget_MouseCursor;
class elWidget_PopupMenu;
class elWidget_Progress;
class elWidget_Selection;
class elWidget_ShaderAnimation;
class elWidget_Slider;
class elWidget_TabbedWindow;
class elWidget_Table;
class elWidget_Textbox;
class elWidget_TextCursor;
class elWidget_Titlebar;
class elWidget_Tree;
class elWidget_Window;
class elWidget_YesNoDialog;

template < typename T >
using widget_t = widget_ptr< T, handler::widgetFactory >;

using elWidget_base_ptr            = widget_t< elWidget_base >;
using elWidget_Accordion_ptr       = widget_t< elWidget_Accordion >;
using elWidget_Button_ptr          = widget_t< elWidget_Button >;
using elWidget_Checkbutton_ptr     = widget_t< elWidget_Checkbutton >;
using elWidget_ColorPicker_ptr     = widget_t< elWidget_ColorPicker >;
using elWidget_Dropbutton_ptr      = widget_t< elWidget_Dropbutton >;
using elWidget_Entry_ptr           = widget_t< elWidget_Entry >;
using elWidget_FileBrowser_ptr     = widget_t< elWidget_FileBrowser >;
using elWidget_Graph_ptr           = widget_t< elWidget_Graph >;
using elWidget_Grid_ptr            = widget_t< elWidget_Grid >;
using elWidget_Image_ptr           = widget_t< elWidget_Image >;
using elWidget_InfoMessage_ptr     = widget_t< elWidget_InfoMessage >;
using elWidget_KeyValuePair_ptr    = widget_t< elWidget_KeyValuePair >;
using elWidget_Label_ptr           = widget_t< elWidget_Label >;
using elWidget_Listbox_ptr         = widget_t< elWidget_Listbox >;
using elWidget_Menu_ptr            = widget_t< elWidget_Menu >;
using elWidget_Menubar_ptr         = widget_t< elWidget_Menubar >;
using elWidget_MouseCursor_ptr     = widget_t< elWidget_MouseCursor >;
using elWidget_PopupMenu_ptr       = widget_t< elWidget_PopupMenu >;
using elWidget_Progress_ptr        = widget_t< elWidget_Progress >;
using elWidget_Selection_ptr       = widget_t< elWidget_Selection >;
using elWidget_ShaderAnimation_ptr = widget_t< elWidget_ShaderAnimation >;
using elWidget_Slider_ptr          = widget_t< elWidget_Slider >;
using elWidget_TabbedWindow_ptr    = widget_t< elWidget_TabbedWindow >;
using elWidget_Table_ptr           = widget_t< elWidget_Table >;
using elWidget_Textbox_ptr         = widget_t< elWidget_Textbox >;
using elWidget_TextCursor_ptr      = widget_t< elWidget_TextCursor >;
using elWidget_Titlebar_ptr        = widget_t< elWidget_Titlebar >;
using elWidget_Tree_ptr            = widget_t< elWidget_Tree >;
using elWidget_Window_ptr          = widget_t< elWidget_Window >;
using elWidget_YesNoDialog_ptr     = widget_t< elWidget_YesNoDialog >;


} // namespace GuiGL
} // namespace el3D
