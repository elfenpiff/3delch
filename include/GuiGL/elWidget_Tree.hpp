#pragma once

#include "GuiGL/elWidget_Window.hpp"

#include <memory>
#include <optional>

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Tree : public elWidget_Window
{
  public:
    struct entry_t
    {
        entry_t() = default;
        entry_t( elWidget_Button_ptr &&base, elWidget_base_ptr &&subFrame,
                 const std::shared_ptr< std::vector< entry_t > > sub ) noexcept;
        entry_t( const entry_t & ) = delete;
        entry_t &
        operator=( const entry_t & ) = delete;

        entry_t( entry_t &&rhs ) noexcept;
        entry_t &
        operator=( entry_t &&rhs ) noexcept;

        elWidget_Button_ptr                       base;
        elWidget_base_ptr                         subFrame;
        std::shared_ptr< std::vector< entry_t > > sub;
    };

    using treeCallback_t = std::function< void(
        const std::vector< std::string > &, const std::string & ) >;

    elWidget_Tree() = delete;
    elWidget_Tree( const constructor_t &constructor );
    virtual ~elWidget_Tree();

    virtual void
    Reshape() override;

    void
    AddEntries( const std::vector< std::string > root,
                const std::vector< std::string > listOfEntries ) noexcept;
    void
    RemoveEntry( const std::vector< std::string > path ) noexcept;
    entry_t *
    GetEntry( const std::vector< std::string > path ) noexcept;
    void
    Clear() noexcept;

    void
    SetClickOnEntryCallback( const treeCallback_t & ) noexcept;
    void
    SetEntryOpenCallback( const treeCallback_t & ) noexcept;
    void
    SetEntryCloseCallback( const treeCallback_t & ) noexcept;
    void
    AllowDoubleEntries( const bool ) noexcept;

    void
    Lua_AddEntries( const std::string &separator, const std::string &rootPath,
                    const std::string &entryList ) noexcept;

    void
    Lua_RemoveEntry( const std::string &separator,
                     const std::string &path ) noexcept;

    elWidget_Button *
    Lua_GetEntry( const std::string &separator,
                  const std::string &path ) noexcept;

  private:
    void
    ReshapeEntry( entry_t &        entry,
                  const glm::vec2 &offset = glm::vec2{ 0.0f } ) noexcept;
    void
    ButtonCallback( elWidget_base *                   subFrame,
                    const std::vector< std::string > &root,
                    const std::string &               entry,
                    const std::vector< entry_t > *    sub ) noexcept;
    elWidget_Button_ptr
    CreateButton( entry_t &node, elWidget_base *subFrame,
                  const std::string &               theme,
                  const std::vector< std::string > &root,
                  const std::string &               text,
                  const std::vector< entry_t > *    sub ) noexcept;
    void
    ResetRootNode() noexcept;

    static constexpr char BUTTON_CLASS_ENTRY[]   = "treeEntry";
    static constexpr char BUTTON_CLASS_SUBTREE[] = "subTree";

    entry_t rootNode;

    struct
    {
        treeCallback_t clickOnEntry;
        treeCallback_t openEntry;
        treeCallback_t closeEntry;
    } callback;

    bool doAllowDoubleEntries = false;
};
} // namespace GuiGL
} // namespace el3D
