#pragma once
#include "GuiGL/common.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

#include <array>
#include <functional>

namespace el3D
{
namespace GuiGL
{
enum class CallCondition
{
    OnEnter,
    OnLeave,
    InState,
};

namespace handler
{
class Callable
{
  public:
    Callable( CallbackContainer_t* const     callbackContainer,
              const std::function< void() >& callback ) noexcept;
    void
    Call() const noexcept;
    void
    Set( const std::function< void() >& v ) noexcept;

  private:
    CallbackContainer_t*    callbackContainer = nullptr;
    std::function< void() > callback;
};

class callback
{
  protected:
    callback( CallbackContainer_t* const callbackContainer ) noexcept;
    virtual ~callback();

  public:
    void
    SetCallback(
        const std::function< void() >& callback,
        const widgetStates_t           state = STATE_SELECTED,
        const CallCondition callCondition = CallCondition::OnEnter ) noexcept;
    void
    RemoveCallback(
        const widgetStates_t state         = STATE_SELECTED,
        const CallCondition  callCondition = CallCondition::OnEnter ) noexcept;

    void
    SetOnCloseCallback( const std::function< void() >& callback ) noexcept;

  protected:
    void
    CallCallback( const widgetStates_t state,
                  const CallCondition  callCondition ) const noexcept;

    bb::product_ptr< Callable >
    CreateCallback(
        const std::function< void() >& f = std::function< void() >() ) noexcept;
    void
    CallCallable( const std::function< void() >& f ) noexcept;

  private:
    struct entry_t
    {
        std::function< void() > onEnter;
        std::function< void() > onLeave;
        std::function< void() > inState;
    };
    std::array< entry_t, STATE_END > callbacks;
    std::function< void() >          onCloseCallback;
    CallbackContainer_t*             callbackContainer = nullptr;
    bb::elGenericFactory< Callable > factory;
};
} // namespace handler
} // namespace GuiGL
} // namespace el3D
