#pragma once
#include "GLAPI/elEventHandler.hpp"
#include "GuiGL/common.hpp"
#include "HighGL/elEventTexture.hpp"

#include <array>
#include <cstdint>
#include <optional>
#include <utility>

namespace el3D
{
namespace GuiGL
{
class elWidget_base;
namespace handler
{
class settings;
class event
{
  public:
    bool
    DoesEventColorBelongsToMe( const uint32_t ) const noexcept;
    bool
    DoesEventColorBelongsToMeOrChild( const uint32_t ) const noexcept;

    friend class settings;

  protected:
    event() = delete;
    event( elWidget_base* const me, HighGL::elEventTexture* const eventTexture,
           GLAPI::elEventHandler* const eventHandler,
           GLAPI::elEvent* const        stickyExclusiveEvent );
    virtual ~event() = default;

  protected:
    bb::product_ptr< GLAPI::elEvent >
    CreateEventFor( const uniformBufferEvents_t area ) noexcept;
    bb::product_ptr< GLAPI::elEvent >
    CreateEvent() noexcept;
    GLAPI::elEvent&
    GetStickyExclusiveEvent() noexcept;
    uint32_t
    GetColorToCurrentEvent() const noexcept;
    glm::vec4
    GetRGBAColorToEvent( const size_t ) const noexcept;
    uniformBufferEvents_t
    GetCurrentEventColor() const noexcept;
    void
    DetectWidgetState( const SDL_Event& ) noexcept;
    void
    CreateDeMultiSelectEvent() noexcept;
    void
    CreateDeSingleSelectEvent() noexcept;
    void
    CreateHoverEvent() noexcept;
    void
    PerformRelativeMove( const glm::vec2& ) noexcept;
    void
    SetupStateEvents( const widgetStates_t state ) noexcept;
    std::optional< elWidget_base* >
    GetChildFromEventColor( const uint32_t ) noexcept;
    virtual void
    ScrollEventCallback( const SDL_Event e ) noexcept;
    void
    SetActivateEventHandling( const bool ) noexcept;

    void
    RegisterStickyExclusiveEvent(
        const std::function< void( SDL_Event ) >& callback ) noexcept;
    void
    EventMoveWidget( const SDL_Event e ) noexcept;
    void
    EventResizeWidget( const SDL_Event e, const glm::vec2& direction,
                       const position_t horizontalMovement,
                       const position_t verticalMovement ) noexcept;
    bool
    HasMouseMotionJitter( const SDL_Event e ) const noexcept;

  protected:
    GLAPI::elEvent* stickyExclusiveEventPtr{ nullptr };

  private:
    void
    RemoveEvent( GLAPI::elEvent* const event ) noexcept;
    void
    CreateDeSelectEvent( elWidget_base* source ) noexcept;
    void
    PreventVerticalWindowMovement( const glm::vec2& oldSize,
                                   const float      yMovement,
                                   const position_t resizingPosition ) noexcept;
    void
    PreventHorizontalWindowMovement(
        const glm::vec2& oldSize, const float xMovement,
        const position_t resizingPosition ) noexcept;

  private:
    elWidget_base*          me;
    HighGL::elEventTexture* eventTexture{ nullptr };
    GLAPI::elEventHandler*  eventHandler{ nullptr };
    // all GLAPI::elEvents members have to come after createdEvents otherwise
    // we access a removed vector in the dtor
    std::vector< GLAPI::elEvent* > createdEvents;
    bool                           ownsStickyExclusiveEvent{ false };

    bb::product_ptr< GLAPI::elEvent > stickyExclusiveEvent;
    bb::product_ptr< GLAPI::elEvent > deHoverEvent;
    bb::product_ptr< GLAPI::elEvent > deSelectEvent;

    struct handle_t
    {
        bb::product_ptr< GLAPI::elEvent > event;
    };

    bb::product_ptr< HighGL::elEventColorRange > eventColorRange;
    std::array< handle_t, static_cast< int >( UBO_EVENT_END ) -
                              static_cast< int >( UBO_COMMON_END ) >
        eventHandles;
};
} // namespace handler
} // namespace GuiGL
} // namespace el3D
