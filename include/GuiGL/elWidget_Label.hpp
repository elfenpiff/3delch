#pragma once

#include "GLAPI/elFont.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "OpenGL/elTexture_2D.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Entry;
class elWidget_Textbox;
class elWidget_Label : public elWidget_base
{
  public:
    elWidget_Label() = delete;
    elWidget_Label( const constructor_t &constructor );
    virtual ~elWidget_Label() = default;

    void
    SetText( const std::string & ) noexcept;
    std::string
    GetText() const noexcept;
    uint64_t
    GetFontHeight() const noexcept;
    GLAPI::dimension_t
    GetTextSize() const noexcept;
    void
    DoAdjustSizeToTextSize( const bool v ) noexcept;
    uint64_t
    GetFontSize() const noexcept;
    std::string
    GetFontFile() const noexcept;

    friend class elWidget_Entry;
    friend class elWidget_Button;
    friend class elWidget_Textbox;

  protected:
    virtual void
    ReloadConfigSettings() noexcept override;

  private:
    GLAPI::elFont *                         font;
    std::string                             text;
    bb::product_ptr< OpenGL::elTexture_2D > texture;
    bool                                    adjustSizeToTextSize{ true };
};
} // namespace GuiGL
} // namespace el3D
