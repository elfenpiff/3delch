#pragma once

#include "GuiGL/GuiGL.hpp"
#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "GuiGL/common.hpp"
#include "GuiGL/constructor_t.hpp"
#include "GuiGL/handler/callback.hpp"
#include "GuiGL/handler/event.hpp"
#include "GuiGL/handler/settings.hpp"
#include "GuiGL/handler/widgetFactory.hpp"
#include "HighGL/elEventTexture.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elUniformBufferObject.hpp"
#include "utils/elRunOnce.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on


namespace el3D
{
namespace lua
{
class elConfigHandler;
}
namespace OpenGL
{
class elShaderProgram;
}

namespace GLAPI
{
class elFontCache;
}

namespace GuiGL
{
class elWidget_base : public handler::callback,
                      public handler::settings,
                      public handler::event,
                      public handler::widgetFactory
{
  public:
    class NoAnimationShaderFileSet_e : public std::runtime_error
    {
      public:
        explicit NoAnimationShaderFileSet_e( const std::string& msg );
        virtual ~NoAnimationShaderFileSet_e();
    };

    elWidget_base() = delete;
    elWidget_base( const constructor_t& constructor );
    elWidget_base( const elWidget_base& ) = delete;
    elWidget_base( elWidget_base&& )      = delete;
    virtual ~elWidget_base();

    elWidget_base&
    operator=( const elWidget_base& ) = delete;
    elWidget_base&
    operator=( elWidget_base&& ) = delete;

    virtual void
    Draw( const renderPass_t renderPass ) noexcept;
    virtual void
    Reshape();

    virtual void
    SetState( const widgetStates_t ) noexcept;

    widgetType_t
    GetType() const noexcept;

    friend class elWidget_Tree;
    friend class elWidget_Window;
    friend class elWidget_Slider;
    friend class handler::settings;
    friend class handler::widgetFactory;
    friend class handler::event;

  protected:
    virtual void
    ReloadConfigSettings() noexcept;
    void
    DrawMe( const renderPass_t renderPass ) noexcept;
    void
    DrawChildren( const renderPass_t renderPass ) noexcept;
    virtual bool
    AdjustSizeToFitChildren() noexcept;
    void
    AlignWithOtherWidget() noexcept;
    void
    AdjustStickySize() noexcept;
    void
    UpdateColors() noexcept;
    void
    UpdateWindowResolution() noexcept;
    void
    UpdateEventColors() noexcept;
    void
    ReshapeSettings() noexcept;
    void
    ReshapeGeometry() noexcept;

    /// @brief lua helper function to handle string vectors
    static std::vector< std::string >
    SeparatedStringToVector( const std::string& separator,
                             const std::string& value ) noexcept;

    template < typename, typename >
    friend class widget_ptr;

  protected:
    widgetType_t   type;
    elWidget_base* parent;

    utils::elRunOnce runOnce;

    OpenGL::elGeometricObject_VertexObject vo;
    glm::vec4                              viewPort{ 0.0f };
    glm::vec4                              viewPortWithBorders{ 0.0f };

    std::unique_ptr< OpenGL::elUniformBufferObject > uniformBuffer;
    const lua::elConfigHandler*                      theme;
    const GLAPI::elWindow*                           window;
    GLAPI::elFontCache*                              fontCache;
    elWidget_MouseCursor*                            mouseCursor;
    std::function< size_t( HighGL::elShaderTexture* ) >
                                    shaderTextureRegistrator;
    std::function< void( size_t ) > shaderTextureDeregistrator;
    int                             shaderVersion;

    static std::vector< std::string > uniformBufferStructure;
};
} // namespace GuiGL
} // namespace el3D
