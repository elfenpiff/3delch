#pragma once

#include "GLAPI/elEvent.hpp"
#include "GuiGL/elWidget_Window.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Textbox;
class elWidget_Label;
class elWidget_TextCursor;
class elWidget_Entry : public elWidget_Window
{
  public:
    static constexpr uint64_t INFINITE_TEXT_SIZE =
        std::numeric_limits< uint64_t >::max();

    elWidget_Entry() = delete;
    elWidget_Entry( const constructor_t &constructor );
    virtual ~elWidget_Entry();

    void
    RestrictInputSizeTo( const uint64_t &v ) noexcept;
    void
    RestrictInputCharacters( const std::string &inputCharacters ) noexcept;

    void
    SetText( const std::string & ) noexcept;
    std::string
    GetText() const noexcept;
    void
    SetState( const widgetStates_t state ) noexcept override;
    void
    SetOnEnterKeyPressCallback(
        const std::function< void() > &callback ) noexcept;

    friend class elWidget_Textbox;

  private:
    enum SelectionDirection_t
    {
        SELECT_FROM_LEFT_TO_RIGHT,
        SELECT_FROM_RIGHT_TO_LEFT
    };

  private:
    uint64_t
    GetFontHeight() const noexcept;
    void
    CreateEditEvent() noexcept;
    void
    MouseEventCallback( const SDL_Event ) noexcept;
    void
    EditEventCallback( const SDL_Event ) noexcept;
    std::string
    GetKeySign( const SDL_Keycode k ) noexcept;
    void
    IncreaseSelectionToTheLeft() noexcept;
    void
    IncreaseSelectionToTheRight() noexcept;
    void
    DecreaseSelectionToTheLeft() noexcept;
    void
    DecreaseSelectionToTheRight() noexcept;
    void
    SelectRange( const size_t start, const size_t length ) noexcept;
    void
    MoveCursorRight( const size_t steps = 1 ) noexcept;
    void
    MoveCursorLeft( const size_t steps = 1 ) noexcept;
    void
    MoveCursorToPosition( const size_t ) noexcept;
    void
    SetCursorToPosition( const size_t ) noexcept;
    void
    MoveCursorToClickPosition() noexcept;
    void
    SelectTextWithMouse( const float currentMousePosition ) noexcept;
    size_t
    MousePositionToTextPosition( const float ) const noexcept;
    void
    UpdateCursor() noexcept;
    bool
    IsKeyCodePrintable( const SDL_Keycode k ) const noexcept;
    float
    GetCharacterWidthAtPosition( const size_t ) const noexcept;
    float
    GetTextWidth( const std::string & ) const noexcept;
    void
    CopyTextToClipboard() const noexcept;
    void
    CreateClickedOnLabelEvent() noexcept;
    void
    RemoveSelectedText() noexcept;
    void
    BeginTextSelection(
        const SelectionDirection_t selectionDirection ) noexcept;
    void
    AutoScrollToCurrentPosition() noexcept;
    void
    MoveCursorTo( const glm::vec2 & ) noexcept;
    void
    InsertText( const uint64_t position, const std::string &text ) noexcept;

  private:
    bool                              isSelecting                = false;
    bool                              isSelectingFromLeftToRight = true;
    std::string                       tabText{ "    " };
    elWidget_TextCursor_ptr           cursor;
    elWidget_TextCursor_ptr           ghostCursor;
    elWidget_Label_ptr                textLabel;
    float                             fontHeight = 0.0f;
    std::string                       text;
    bb::product_ptr< GLAPI::elEvent > editEvent;
    bb::product_ptr< GLAPI::elEvent > clickedOnLabelEvent;
    uint64_t                          textPosition      = 0;
    uint64_t                          textRightPosition = 0;
    glm::vec2 mouseCursorClickPosition                  = glm::vec2( -1.0f );
    bb::product_ptr< handler::Callable > onEnterKeyPressCallback;
    uint64_t                             maxTextSize = INFINITE_TEXT_SIZE;
    std::string                          inputCharacterRestriction;
};
} // namespace GuiGL
} // namespace el3D
