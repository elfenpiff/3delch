#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_ShaderAnimation;
class elWidget_Progress : public elWidget_base
{
  public:
    elWidget_Progress() = delete;
    elWidget_Progress( const constructor_t &constructor );
    virtual ~elWidget_Progress();

    void
    SetProgress( const float ) noexcept;
    float
    GetProgress() const noexcept;
    virtual void
    SetState( const widgetStates_t ) noexcept override;

    static constexpr float PROGRESS_INDIFFERENT = 2.0f;

  private:
    elWidget_ShaderAnimation_ptr progressAnimation;
    float                        progress = 0.0f;
};
} // namespace GuiGL
} // namespace el3D
