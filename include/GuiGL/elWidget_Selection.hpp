#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Entry;
class elWidget_Selection : public elWidget_base
{
  public:
    elWidget_Selection() = delete;
    elWidget_Selection( const constructor_t& constructor );
    virtual ~elWidget_Selection();

    friend class elWidget_Entry;
};
} // namespace GuiGL
} // namespace el3D
