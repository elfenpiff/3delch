#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Menu : public elWidget_base
{
  public:
    struct entry_t
    {
        entry_t( const char *entry );
        entry_t( const std::initializer_list< entry_t > &sub );
        std::variant< std::string, std::vector< entry_t > > value;
    };

    elWidget_Menu() = delete;
    elWidget_Menu( const constructor_t &constructor );
    virtual ~elWidget_Menu() = default;

    virtual void
    Reshape() override;

    void
    SetEntries( const std::initializer_list< entry_t > &e ) noexcept;


    elWidget_Button *
    GetEntryFromVector( const std::vector< std::string > &entry ) noexcept;
    elWidget_Button *
    GetEntry( const std::string &separator, const std::string &entry ) noexcept;

    void
    AddEntry( const std::string &separator, const std::string &entry ) noexcept;
    void
    Clear() noexcept;

  protected:
    struct menu_t
    {
        elWidget_Button_ptr button;
        elWidget_Label_ptr  buttonArrow;
        elWidget_Menu *     submenu;
    };

    bb::product_ptr< GLAPI::elEvent > closeSubMenuOnClick;
    std::vector< elWidget_Menu_ptr >  subMenus;
    elWidget_Menu *                   activeMenu   = nullptr;
    size_t                            maxWidth     = 0;
    size_t                            widthPadding = 50;
    std::vector< menu_t >             root;

    void
    MenuButtonCallback( elWidget_Button *const button,
                        elWidget_Menu *const   submenu ) noexcept;
    void
    AlignButtonInMenu( elWidget_Button *const button,
                       elWidget_Menu *const   submenu ) noexcept;

    void
    AdjustSize( const glm::vec2 &entryEndPosition ) noexcept;

    virtual void
    AlignSubMenus() noexcept;

    void
    AdjustPositionPointOfOrigin() noexcept;

    void
    AddEntriesToMountPoint( menu_t &point, const entry_t &e ) noexcept;

    void
    CreateMenuButton( menu_t &point, const std::string &name ) noexcept;
    void
    CreateSubMenu( menu_t &point ) noexcept;

    void
    HideMenu( elWidget_Menu *menu ) noexcept;

    bool
    HasEnoughSpaceOnRightSide( const glm::vec2 &    pos,
                               const elWidget_Menu *menu ) const noexcept;

    bool
    HasEnoughSpaceOnTheBottom( const glm::vec2 &    pos,
                               const elWidget_Menu *menu ) const noexcept;
};
} // namespace GuiGL
} // namespace el3D
