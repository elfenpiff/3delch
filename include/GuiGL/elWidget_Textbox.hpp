#pragma once

#include "GuiGL/elWidget_Window.hpp"

#include <limits>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace GuiGL
{
class elWidget_Label;
class elWidget_TextCursor;
class elWidget_Selection;
class elWidget_Textbox : public elWidget_Window
{
  public:
    static constexpr uint64_t TEXT_END = std::numeric_limits< uint64_t >::max();

    elWidget_Textbox() = delete;
    elWidget_Textbox( const constructor_t &constructor );
    virtual ~elWidget_Textbox();

    virtual void
    SetState( const widgetStates_t ) noexcept override;

    void
    SetText( const std::vector< std::string > & ) noexcept;
    std::vector< std::string >
    GetText() const noexcept;
    uint64_t
    GetNumberOfLines() const noexcept;
    void
    ClearText() noexcept;

    std::string
    GetLine( const uint64_t line ) const noexcept;

    elWidget_base &
    GetLineWidget( const uint64_t line ) noexcept;
    void
    InsertLine( const std::string &, const uint64_t line = TEXT_END ) noexcept;
    void
    ModifyLine( const std::string &newLine, const uint64_t line ) noexcept;
    void
    RemoveLine( const uint64_t line ) noexcept;
    void
    SetIsEditable( const bool ) noexcept;

  private:
    void
    AdjustLinePosition( const uint64_t start ) noexcept;
    void
    EditEventCallback( const SDL_Event e ) noexcept;
    void
    MouseEventCallback( const SDL_Event e ) noexcept;
    glm::ivec2
    MousePositionToTextPosition( const glm::vec2 &mousePosition );
    void
    RemoveSelectedText() noexcept;
    void
    InsertTextInLine( const uint64_t line, const uint64_t positionInLine,
                      const std::string &text ) noexcept;
    void
    AutoScrollToCurrentPosition() noexcept;

  private:
    struct properties_t
    {
        uint64_t lineSpacing  = 0;
        uint64_t spaceToLeft  = 0;
        uint64_t pageJumpSize = 10;
    };

    struct range_t
    {
        glm::ivec2 begin{ -1, -1 };
        glm::ivec2 end{ -1, -1 };
    };

    class cursor_t
    {
      public:
        cursor_t( elWidget_Textbox &me, elWidget_TextCursor_ptr &&widget,
                  elWidget_TextCursor_ptr &&ghostWidget );
        void
        UpdateDimensions() noexcept;
        void
        MoveLeft() noexcept;
        void
        MoveRight() noexcept;
        void
        MoveUp( const uint64_t steps = 1 ) noexcept;
        void
        MoveDown( const uint64_t steps = 1 ) noexcept;
        void
        MoveCursorToLineEnd() noexcept;
        void
        MoveCursorToLineBegin() noexcept;
        glm::ivec2
        GetCurrentPosition() const noexcept;
        void
        MoveTo( const glm::ivec2 &position ) noexcept;
        elWidget_TextCursor &
        GetCursor() noexcept;
        elWidget_TextCursor &
        GetGhostCursor() noexcept;

      private:
        void
        ClampPositionToText() noexcept;

      private:
        static constexpr glm::i32 LINE_END =
            std::numeric_limits< glm::i32 >::max();
        elWidget_Textbox &      me;
        elWidget_TextCursor_ptr widget;
        elWidget_TextCursor_ptr ghostWidget; // used for scroll animation
        glm::ivec2              position{ 0, 0 };
    };

    class line_t
    {
      public:
        line_t( elWidget_Label_ptr &&label ) noexcept;
        line_t( line_t &&rhs ) noexcept;
        line_t &
        operator  =( line_t &&rhs ) noexcept;
        ~line_t() = default;

        line_t( const line_t &rhs ) = delete;
        line_t &
        operator=( const line_t &rhs ) = delete;


        void
        SetText( const std::string &v ) noexcept;
        std::string
        GetText() const noexcept;
        uint64_t
        GetTextSize( const std::string &v ) const noexcept;
        void
        SetPosition( const glm::vec2 &v ) noexcept;
        glm::vec2
        GetPosition() const noexcept;
        uint64_t
        GetFontHeight() const noexcept;
        elWidget_Label &
        GetLabelWidget() noexcept;
        elWidget_Selection &
        GetSelectionWidget() noexcept;

      private:
        elWidget_Label_ptr     label;
        elWidget_Selection_ptr selection;
    };

    class selection_t
    {
      public:
        selection_t( elWidget_Textbox &me ) noexcept;
        void
        AdjustSelection() noexcept;
        void
        DoActivateSelection( const bool ) noexcept;
        range_t
        GetSelection() const noexcept;
        bool
        IsSelectionModeActive() const noexcept;

      private:
        void
        UpdateSelectionDimensions() noexcept;
        void
        BeginSelection() noexcept;
        void
        EndSelection() noexcept;

      private:
        elWidget_Textbox &me;
        glm::ivec2        startSelection;
        range_t           currentSelection;
        range_t           previousSelection;
        bool              isSelectionModeActive{ false };
        bool              isSelectingFromTopToBottom{ true };
    };

    properties_t property;
    // property is used in cursor, therefore it must be initialized above
    cursor_t                          cursor;
    std::vector< line_t >             lines;
    selection_t                       selection;
    glm::ivec2                        mouseClickPosition{ -1, -1 };
    bb::product_ptr< GLAPI::elEvent > editEvent;
    bb::product_ptr< GLAPI::elEvent > mouseClickEvent;
    bool                              isEditable{ false };
    uint64_t                          tabWidth{ 4 };
    glm::vec2                         scrollStart{ 2.0f, 2.0f };
};

} // namespace GuiGL
} // namespace el3D
