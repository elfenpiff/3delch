#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
/*!
    \brief There can be at most one titlebar per widget!
*/
class elWidget_Titlebar : public elWidget_base
{
  public:
    elWidget_Titlebar() = delete;
    elWidget_Titlebar( const constructor_t &constructor );
    virtual ~elWidget_Titlebar() = default;

    virtual void
    SetState( const widgetStates_t ) noexcept override;
    void
    SetHasCloseButton( const bool v ) noexcept;
    const elWidget_Button &
    GetCloseButton() const noexcept;
    elWidget_Button &
    GetCloseButton() noexcept;
    void
    SetTitleText( const std::string &text ) noexcept;

  private:
    elWidget_Button_ptr closeButton;
    elWidget_Label_ptr  titleText;
};
} // namespace GuiGL
} // namespace el3D
