#pragma once

#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "utils/elVarWithHistory.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Dropbutton;
class elWidget_Table : public elWidget_Window
{
  protected:
    template < typename T >
    using table_t = std::vector< std::vector< T > >;

  public:
    static constexpr unsigned int NO_SELECTION = -1u;
    enum Mode_t
    {
        TABLEMODE_ROW = 0,
        TABLEMODE_COL,
        TABLEMODE_ENTRY,
        TABLEMODE_NONE
    };

    elWidget_Table() = delete;
    elWidget_Table( const constructor_t &constructor );
    virtual ~elWidget_Table() = default;

    virtual void
    ResetNumberOfColsAndRows( const size_t cols, const size_t rows ) noexcept;

    virtual void
    SetColumnWidth( const size_t col, const float width ) noexcept;
    virtual void
    SetRowHeight( const size_t row, const float height ) noexcept;
    bool
    IsEmpty() const noexcept;
    virtual void
    SetElementDefaultDimensions( const size_t width,
                                 const size_t height ) noexcept;
    void
    SetHoveringMode( const Mode_t mode ) noexcept;
    void
    SetSelectionMode( const Mode_t mode ) noexcept;
    void
    SetOnSelectionCallback( const std::function< void() > & ) noexcept;
    std::vector< glm::uvec2 >
    GetSelection() const noexcept;
    void
    Reshape() override;

    void
    SetHasTitleRow( const bool v ) noexcept;
    bool
    GetHasTitleRow() const noexcept;
    void
    SetTitleRowButtonText( const size_t col, const std::string &text ) noexcept;
    void
    SetElementBaseType( const widgetType_t baseType ) noexcept;

    template < typename T = elWidget_base >
    T *
    GetElement( const size_t col, const size_t row ) noexcept;
    template < typename T = elWidget_base >
    const T *
    GetElement( const size_t col, const size_t row ) const noexcept;


  protected:
    std::function< elWidget_base_ptr() > elementFactory;
    std::vector< elWidget_Button_ptr >   titleRow;
    table_t< elWidget_base_ptr >         elements;
    table_t< size_t >                    dimensions;

    size_t                               columnWidth          = 100;
    size_t                               rowHeight            = 60;
    float                                titlebarToEntrySpace = 5.0f;
    bb::product_ptr< handler::Callable > onSelectionCallback;


    struct highlight_t
    {
        utils::elVarWithHistory< glm::uvec2 > hoverPosition =
            glm::uvec2( NO_SELECTION );
        std::vector< glm::uvec2 > selection;

        elWidget_base_ptr                hoverWidget;
        std::vector< elWidget_base_ptr > selectionWidget;

        Mode_t                       hoverMode          = TABLEMODE_NONE;
        Mode_t                       selectionMode      = TABLEMODE_NONE;
        static constexpr const char *highlightClasses[] = {
            "HighlightCurrentRow", "HighlightCurrentCol",
            "HighlightCurrentEntry" };
        static constexpr const char *selectedClasses[] = {
            "SelectCurrentRow", "SelectCurrentCol", "SelectCurrentEntry" };
    } highlight;

    friend class elWidget_Dropbutton;

  private:
    void
    AdjustTitleRow() noexcept;
    void
    ClearTable() noexcept;
    void
    AlignTableCells() noexcept;
    elWidget_base_ptr
    CreateHighlightWidget( const Mode_t mode, const bool isSelected ) noexcept;
    void
    ReshapeHoverHighlight() noexcept;
    elWidget_base_ptr
    CreateTableElement( const size_t col, const size_t row ) noexcept;
    void
    AdjustMode( Mode_t &mode, elWidget_base_ptr &widget,
                const Mode_t newMode ) noexcept;
    bool
    AdjustSelectionWidgets(
        const glm::uvec2 &selectionChangePosition ) noexcept;

    void
    ResetSelection() noexcept;
    void
    Hover( const glm::uvec2 &v ) noexcept;
    void
    Select( const glm::uvec2 &v ) noexcept;
};
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/elWidget_Table.inl"
