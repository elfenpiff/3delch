#pragma once

#include "GuiGL/elWidget_base.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Window;
class elWidget_TabbedWindow : public elWidget_base
{
  public:
    struct tab_t
    {
        elWidget_Button_ptr button;
        elWidget_Window_ptr window;
    };
    using tabElement_t = bb::elUniqueIndexVector< tab_t >::element_t;


    elWidget_TabbedWindow() = delete;
    elWidget_TabbedWindow( const constructor_t &constructor );
    virtual ~elWidget_TabbedWindow() = default;

    virtual void
    Reshape() override;

    bb::product_ptr< tab_t >
    CreateTab( const std::string &name ) noexcept;

  private:
    tab_t *activeTab = nullptr;
    // should be always last member / cleanup can access other members
    bb::elGenericFactory< tab_t > tabFactory;
};
} // namespace GuiGL
} // namespace el3D
