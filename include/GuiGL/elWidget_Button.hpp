#pragma once

#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Slider;
class elWidget_Menu;
class elWidget_Tree;
class elWidget_Button : public elWidget_base
{
  public:
    elWidget_Button() = delete;
    elWidget_Button( const constructor_t& constructor );
    virtual ~elWidget_Button() = default;

    void
    Reshape() override;
    void
    SetText( const std::string ) noexcept;
    std::string
    GetText() const noexcept;
    uint64_t
    GetFontHeight() const noexcept;
    virtual void
    SetState( const widgetStates_t ) noexcept override;
    void
    SetClickCallback( const std::function< void() >& ) noexcept;
    void
    SetTextAlignment(
        const positionCorner_t position,
        const glm::vec2&       offsetValue = glm::vec2( 0.0 ) ) noexcept;

    friend class elWidget_Slider;
    friend class elWidget_Menu;
    friend class elWidget_Tree;

  private:
    elWidget_Label_ptr                   textLabel;
    std::string                          text;
    bb::product_ptr< handler::Callable > clickCallback;
    positionCorner_t                     textAlignment = POSITION_CENTER;
    glm::vec2                            offset        = glm::vec2( 0.0 );
    struct
    {
        uint64_t width;
        uint64_t height;
    } fontDimension;
};
} // namespace GuiGL
} // namespace el3D
