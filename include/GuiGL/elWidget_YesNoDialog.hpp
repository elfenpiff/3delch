#pragma once

#include "GuiGL/elWidget_InfoMessage.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Button;
class elWidget_Textbox;
class elWidget_YesNoDialog : public elWidget_InfoMessage
{
  public:
    elWidget_YesNoDialog() = delete;
    elWidget_YesNoDialog( const constructor_t &constructor );
    virtual ~elWidget_YesNoDialog();

    void
    SetYesNoCallback( const std::function< void() > &yes,
                      const std::function< void() > &no ) noexcept;

    virtual void
    Reshape() override;

  private:
    void
    ButtonPressCallback( const bool yes ) noexcept;
    void
    SetCallbackOnClose( const std::function< void() > & ) noexcept override;

    elWidget_Button_ptr noButton;

    bb::product_ptr< handler::Callable > yesCallback;
    bb::product_ptr< handler::Callable > noCallback;
};
} // namespace GuiGL
} // namespace el3D
