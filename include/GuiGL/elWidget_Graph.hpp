#pragma once

#include "GuiGL/elWidget_ShaderAnimation.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Graph : public elWidget_ShaderAnimation
{
  public:
    elWidget_Graph() = delete;
    elWidget_Graph( const constructor_t &constructor );
    virtual ~elWidget_Graph();

    bb::elExpected< elWidget_ShaderAnimation::Error >
    SetUpGraphShader( const std::string &shaderFile,
                      const std::string &shaderGroup = "default" );
    void
    SetGraphData( const std::vector< float > &data,
                  const glm::vec2             yDrawRange = { 0.0f, 0.0f } );
    void
    SetGraphDataCallback( const std::function< void() > &v );

    void
    SetGraphDataYDrawRange( const glm::vec2 yDrawRange );

    void
    AddGraphDataValue( const float data );

    void
    ResetGraphData();

    void
    UpdateGraphData();

  private:
    bb::elExpected< elWidget_ShaderAnimation::Error >
    SetUp( const std::string &shaderFile,
           const std::string &shaderGroup            = "default",
           const bool         doPerformDynamicResize = true,
           const std::vector< OpenGL::textureProperties_t >
               outputTextureProperties = { OpenGL::textureProperties_t{} },
           const GLsizei sizeX = 512, const GLsizei sizeY = 512,
           const bool isFlipFlop = false ) noexcept override;
    void
    SetTextureUniformBlockData( const size_t arrayIndex, const size_t varIndex,
                                const void * varData,
                                const size_t dataSize ) noexcept override;
    virtual void
    SetDoInfinitLoop( const bool ) noexcept override;
    virtual void
    Start() noexcept override;
    virtual void
    ReverseStart() noexcept override;
    virtual void
    Stop() noexcept override;

    std::unique_ptr< OpenGL::elUniformBufferObject > graphUniformBuffer;
    size_t                                           maximumGraphValues{ 4096 };
    std::vector< float >                             graphData;
    glm::vec2                            graphDataYRange{ 0.0f, 0.0f };
    bb::product_ptr< handler::Callable > graphDataCallback;

    static const std::vector< std::string > GRAPH_UNIFORM_STRUCTURE;
};
} // namespace GuiGL
} // namespace el3D
