#pragma once

#include "GuiGL/elWidget_ShaderAnimation.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_MouseCursor : public elWidget_ShaderAnimation
{
  public:
    elWidget_MouseCursor() = delete;
    elWidget_MouseCursor( const constructor_t& constructor );
    virtual ~elWidget_MouseCursor() = default;

    glm::vec2
    GetCursorPosition() const noexcept;
    glm::vec2
    GetRelativeCursorPosition() const noexcept;

  private:
    bb::elExpected< elWidget_ShaderAnimation::Error >
    SetUp( const std::string& shaderFile,
           const std::string& shaderGroup            = "default",
           const bool         doPerformDynamicResize = true,
           const std::vector< OpenGL::textureProperties_t >
               outputTextureProperties = { OpenGL::textureProperties_t{} },
           const GLsizei sizeX = 512, const GLsizei sizeY = 512,
           const bool isFlipFlop = false ) noexcept override;
    void
    SetTextureUniformBlockData( const size_t arrayIndex, const size_t varIndex,
                                const void*  varData,
                                const size_t dataSize ) noexcept override;
    virtual void
    SetDoInfinitLoop( const bool ) noexcept override;
    virtual void
    Start() noexcept override;
    virtual void
    ReverseStart() noexcept override;
    virtual void
    Stop() noexcept override;

    bb::product_ptr< GLAPI::elEvent > event;
};
} // namespace GuiGL
} // namespace el3D
