#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Tree;
class elWidget_Button;
class elWidget_Entry;
class elWidget_Table;
class elWidget_Titlebar;
class elWidget_FileBrowser : public elWidget_base
{
  public:
    elWidget_FileBrowser() = delete;
    elWidget_FileBrowser( const constructor_t &constructor );
    virtual ~elWidget_FileBrowser();

    void
    SetRootPath( const std::string &v ) noexcept;
    void
    SetOpenButtonText( const std::string &v ) noexcept;
    void
    SetAbortButtonText( const std::string &v ) noexcept;
    void
    SetAbortCallback( const std::function< void() > &v ) noexcept;
    void
    SetOpenCallback( const std::function< void() > &v ) noexcept;
    std::vector< std::string >
    GetSelection() const noexcept;
    void
    Reshape() override;

  private:
    enum FileTableEntry_t
    {
        FILE_TYPE = 0,
        FILE_NAME = 1,
        FILE_SIZE = 2,
    };

    std::string                          abortButtonText = "abort";
    std::string                          openButtonText  = "open";
    bb::product_ptr< handler::Callable > openCallback;
    bb::product_ptr< handler::Callable > abortCallback;
    std::string                          rootPath;
    std::string                          currentPath;

    elWidget_Tree_ptr   directoryTree;
    elWidget_Button_ptr setRootButton;
    elWidget_Entry_ptr  pathEntry;
    elWidget_Table_ptr  fileTable;
    elWidget_Button_ptr titleBarOpen;
    elWidget_Button_ptr titleBarAbort;

    void
    ClickOnOpen() noexcept;
    void
    ClickOnAbort() noexcept;

    void
    FillDirectoryTree( const std::string &parentPath,
                       const size_t       depth ) noexcept;
    void
    FillFileTable( const std::string &path ) noexcept;

    void
    OpenDirectoryCallback( const std::vector< std::string > &root,
                           const std::string &directory ) noexcept;
    void
    CloseDirectoryCallback( const std::vector< std::string > &root,
                            const std::string &directory ) noexcept;
    void
    ClickDirectoryCallback( const std::vector< std::string > &root,
                            const std::string &directory ) noexcept;
};
} // namespace GuiGL
} // namespace el3D
