#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_ShaderAnimation;
class elWidget_Checkbutton : public elWidget_base
{
  public:
    elWidget_Checkbutton() = delete;
    elWidget_Checkbutton( const constructor_t &constructor );
    virtual ~elWidget_Checkbutton() = default;

    virtual void
    SetState( const widgetStates_t ) noexcept override;

    bool
    IsChecked() const noexcept;
    void
    SetIsChecked( const bool ) noexcept;

  private:
    elWidget_ShaderAnimation_ptr slider;
    bool                         isChecked = false;
};
} // namespace GuiGL
} // namespace el3D
