#pragma once

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_ColorPicker : public elWidget_base
{
  public:
    elWidget_ColorPicker() = delete;
    elWidget_ColorPicker( const constructor_t& constructor );
    virtual ~elWidget_ColorPicker() = default;

    void
    SetOnCloseCallback( const std::function< void() >& v ) noexcept;
    void
    SetOnPickCallback( const std::function< void() >& v ) noexcept;

    glm::uvec3
    GetUintColor() const noexcept;
    glm::vec3
    GetFloatColor() const noexcept;

  private:
    struct color_t
    {
        elWidget_Entry_ptr  entry;
        elWidget_Slider_ptr slider;
        float               colorValue = 0.0f;
    };

    void
    CreateColorGuiElements( color_t& c, const std::string& v,
                            const glm::vec2& position ) noexcept;
    void
    UpdatePreview() noexcept;

  private:
    static constexpr float COLOR_FACTOR = 255.0f;

    elWidget_base_ptr   preview;
    elWidget_Button_ptr pickButton;
    elWidget_Button_ptr closeButton;

    color_t red;
    color_t green;
    color_t blue;
};
} // namespace GuiGL
} // namespace el3D
