#pragma once

#include "utils/elClassSettings.hpp"

#include <climits>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

namespace el3D
{
namespace GuiGL
{
/// will be thrown in the constructor if it is impossible to create any kind of
/// widget and all widgets are unusable
class InvalidShader_e : public std::runtime_error
{
  public:
    explicit InvalidShader_e( const std::string& msg );
    virtual ~InvalidShader_e();
};

class InvalidFont_e : public std::runtime_error
{
  public:
    explicit InvalidFont_e( const std::string& msg );
    virtual ~InvalidFont_e();
};

using CallbackContainer_t = std::vector< std::function< void() > >;


/*!
    \brief Sets globally the dpi scaling for all gui widgets. Be aware that
    you have to run Reshape() on the widgets afterwards so that they are
    reshaping to the new dpi settings
  */
void
SetDPIScaling( const float ) noexcept;
float
GetDPIScaling() noexcept;

enum widgetType_t
{
    TYPE_BASE,
    TYPE_EMPTY,
    TYPE_WINDOW,
    TYPE_MOUSE_CURSOR,
    TYPE_IMAGE,
    TYPE_TEXT_CURSOR,
    TYPE_LABEL,
    TYPE_ENTRY,
    TYPE_BUTTON,
    TYPE_SLIDER,
    TYPE_TABLE,
    TYPE_LISTBOX,
    TYPE_DROPBUTTON,
    TYPE_SHADERANIMATION,
    TYPE_CHECKBUTTON,
    TYPE_PROGRESS,
    TYPE_TABBEDWINDOW,
    TYPE_MENU,
    TYPE_MENUBAR,
    TYPE_POPUPMENU,
    TYPE_TEXTBOX,
    TYPE_INFOMESSAGE,
    TYPE_YESNODIALOG,
    TYPE_TREE,
    TYPE_FILEBROWSER,
    TYPE_TITLEBAR,
    TYPE_GRAPH,
    TYPE_SELECTION,
    TYPE_KEY_VALUE_PAIR,
    TYPE_ACCORDION,
    TYPE_GRID,
    TYPE_COLOR_PICKER,
    TYPE_END
};
const std::vector< std::string > widgetTypeString = {
    "base",        "empty",           "window",
    "mouseCursor", "image",           "textCursor",
    "label",       "entry",           "button",
    "slider",      "table",           "listbox",
    "dropButton",  "shaderAnimation", "checkButton",
    "progress",    "tabbedWindow",    "menu",
    "menuBar",     "popupMenu",       "textbox",
    "infoMessage", "yesNoDialog",     "tree",
    "fileBrowser", "titlebar",        "graph",
    "selection",   "keyValuePair",    "accordion",
    "grid",        "colorPicker",     "end" };

enum verticalPosition_t
{
    VPOSITION_TOP            = 0,
    VPOSITION_TOP_ALIGNED    = 1,
    VPOSITION_CENTER         = 2,
    VPOSITION_BOTTOM_ALIGNED = 3,
    VPOSITION_BOTTOM         = 4
};

enum horizontalPosition_t
{
    HPOSITION_LEFT          = 0,
    HPOSITION_LEFT_ALIGNED  = 1,
    HPOSITION_CENTER        = 2,
    HPOSITION_RIGHT_ALIGNED = 3,
    HPOSITION_RIGHT         = 4
};

enum position_t
{
    POSITION_TOP    = 0,
    POSITION_BOTTOM = 1,
    POSITION_LEFT   = 2,
    POSITION_RIGHT  = 3,
    POSITION_MIDDLE = 4
};

enum positionCorner_t
{
    POSITION_TOP_LEFT      = 0,
    POSITION_TOP_RIGHT     = 1,
    POSITION_BOTTOM_LEFT   = 2,
    POSITION_BOTTOM_RIGHT  = 3,
    POSITION_CENTER        = 4,
    POSITION_TOP_CENTER    = 5,
    POSITION_BOTTOM_CENTER = 6,
    POSITION_CENTER_LEFT   = 7,
    POSITION_CENTER_RIGHT  = 8,
    POSITION_END           = 9
};

enum widgetStates_t : size_t
{
    STATE_DEFAULT = 0,
    STATE_INACTIVE,
    STATE_HOVER,
    STATE_CLICKED,
    STATE_SELECTED,
    STATE_END,
};

enum widgetProperty_t
{
    PROPERTY_SIZE = 2 * STATE_END,
    PROPERTY_STICK_TO_PARENT_SIZE,
    PROPERTY_FONT_FILE,
    PROPERTY_FONT_SIZE,
    PROPERTY_BORDER_SIZE,
    PROPERTY_BORDER_EVENT_INCREASE,
    PROPERTY_ABSOLUT_POSITION_OFFSET, // internal
    PROPERTY_POSITION,
    PROPERTY_IS_MOVABLE,
    PROPERTY_IS_RESIZABLE,
    PROPERTY_IS_UNSELECTABLE,
    PROPERTY_IS_ALWAYS_ON_TOP,
    PROPERTY_IS_ALWAYS_ON_BOTTOM,
    PROPERTY_HAS_MULTISELECTION,
    PROPERTY_SWAP_COLOR,
    PROPERTY_ANIMATION_SPEED,
    PROPERTY_SCROLLBAR_ENABLED,
    PROPERTY_SCROLLBAR_WIDTH,
    PROPERTY_NATURAL_SCROLLING,
    PROPERTY_DISABLE_EVENT_HANDLING,
    PROPERTY_COPY_SELECTED_TEXT_TO_CLIPBOARD,
    PROPERTY_COLOR_TRANSITION_SPEED,
    PROPERTY_ANIMATION_SHADER_FILE,
    PROPERTY_POSITION_POINT_OF_ORIGIN,
    PROPERTY_POSITION_POINT_OF_ORIGIN_FOR_CHILDS, // see positionCorner_t for
                                                  // int vals
    PROPERTY_HAS_VERTICAL_ALIGNMENT,
    PROPERTY_BUTTON_ALIGNMENT, // see  position_t
    PROPERTY_FORWARD_MOVE_EVENT_TO_PARENT,
    PROPERTY_HAS_TITLEBAR,
    PROPERTY_HAS_TITLE_ROW,
    PROPERTY_HAS_FIXED_POSITION, // is not effected by scrolling
    PROPERTY_ADJUST_SIZE_TO_FIT_CHILDREN,
    PROPERTY_END
};

const std::vector< utils::elClassSettings::entry_t > widgetProperty = {
    { "default", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "inactive", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "hover", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "clicked", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "selected", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "glowDefault", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "glowInactive", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "glowHover", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "glowClicked", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "glowSelected", 8, utils::elClassSettings::dataType_t::FLOAT },
    { "size", 2, utils::elClassSettings::dataType_t::FLOAT,
      std::vector< float >{ 1.0f, 1.0f } },
    { "stickToParentSize", 2, utils::elClassSettings::dataType_t::FLOAT,
      std::vector< float >{ -1.0f, -1.0f } },
    { "fontFile", 1, utils::elClassSettings::dataType_t::STRING,
      std::vector< std::string >{ "data/fonts/FiraCode.ttf" } },
    { "fontSize", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ 24 } },
    { "borderSize", 4, utils::elClassSettings::dataType_t::FLOAT },
    { "borderEventIncrease", 4, utils::elClassSettings::dataType_t::FLOAT },
    { "absolutPositionOffset", 2, utils::elClassSettings::dataType_t::FLOAT },
    { "position", 2, utils::elClassSettings::dataType_t::FLOAT },
    { "isMovable", 1, utils::elClassSettings::dataType_t::INT },
    { "isResizable", 1, utils::elClassSettings::dataType_t::INT },
    { "isUnSelectable", 1, utils::elClassSettings::dataType_t::INT },
    { "isAlwaysOnTop", 1, utils::elClassSettings::dataType_t::INT },
    { "isAlwaysOnBottom", 1, utils::elClassSettings::dataType_t::INT },
    { "hasMultiSelection", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false } },
    { "swapColor", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false } },
    { "animationSpeed", 1, utils::elClassSettings::dataType_t::FLOAT,
      std::vector< float >{ 4.0f } },
    { "scrollbarEnabled", 4, utils::elClassSettings::dataType_t::INT },
    { "scrollbarWidth", 4, utils::elClassSettings::dataType_t::FLOAT },
    { "naturalScrolling", 2, utils::elClassSettings::dataType_t::FLOAT,
      std::vector< float >{ 1.0f, 1.0f } },
    { "disableEventHandling", 1, utils::elClassSettings::dataType_t::INT },
    { "doCopySelectedTextToClipboard", 1,
      utils::elClassSettings::dataType_t::INT },
    { "colorTransitionSpeed", 1, utils::elClassSettings::dataType_t::FLOAT,
      std::vector< float >{ 3.0f } },
    { "animationShaderFile", 1, utils::elClassSettings::dataType_t::STRING },
    { "positionPointOfOrigin", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ POSITION_TOP_LEFT } },
    { "positionPointOfOriginForChilds", 1,
      utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ POSITION_END } },
    // used by menu and childs
    { "hasVerticalAlignment", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ true } },
    // used by menubar and tabbedwindow
    { "buttonAlignment", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ POSITION_TOP } },
    { "forwardMoveEventToParent", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false } },
    { "hasTitlebar", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false } },
    // used by table
    { "hasTitleRow", 1, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false } },
    { "hasFixedPosition", 2, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false, false } },
    { "adjustSizeToFitChildren", 2, utils::elClassSettings::dataType_t::INT,
      std::vector< int >{ false, false } },
    { "", 0, utils::elClassSettings::dataType_t::FLOAT } };

enum uniformBufferCommon_t
{
    UBO_COLOR,
    UBO_COLOR_ACCENT,
    UBO_GLOW_COLOR,
    UBO_GLOW_COLOR_ACCENT,
    UBO_GLOW_SCALING,
    UBO_BORDERS,
    UBO_BORDER_EVENT_INCREASE,
    UBO_RESOLUTION,
    UBO_SIZE,
    UBO_TEXTURE_SIZE,
    UBO_POSITION,
    UBO_MOUSE_POSITION,
    UBO_TIME,
    UBO_VIEWPORT,
    UBO_DO_DRAW_EVENT_COLOR,
    UBO_DO_READ_TEXTURE,
    UBO_DO_DRAW_COLORS,
    UBO_DO_INVERT_TEXTURE_COORDINATES,
    UBO_IS_EMPTY,
    UBO_COMMON_END
};

enum uniformBufferEvents_t
{
    UBO_EVENT_COLOR_TOP = UBO_COMMON_END,
    UBO_EVENT_COLOR_BOTTOM,
    UBO_EVENT_COLOR_LEFT,
    UBO_EVENT_COLOR_RIGHT,
    UBO_EVENT_COLOR_TOP_LEFT,
    UBO_EVENT_COLOR_TOP_RIGHT,
    UBO_EVENT_COLOR_BOTTOM_LEFT,
    UBO_EVENT_COLOR_BOTTOM_RIGHT,
    UBO_EVENT_COLOR_INNER,
    UBO_EVENT_END
};

enum renderPass_t
{
    RENDER_PASS_DIFFUSE = 0,
    RENDER_PASS_GLOW    = 1
};

enum runOnce_t
{
    _Method_Reshape_ = 0
};
} // namespace GuiGL
} // namespace el3D
