#pragma once

#include "GuiGL/elWidget_Menu.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_PopupMenu : public elWidget_Menu
{
  public:
    elWidget_PopupMenu() = delete;
    elWidget_PopupMenu( const constructor_t& constructor );
    virtual ~elWidget_PopupMenu() = default;

  protected:
    bb::product_ptr< GLAPI::elEvent > popupEvent;
    bb::product_ptr< GLAPI::elEvent > popupCloseEvent;
};
} // namespace GuiGL
} // namespace el3D
