#pragma once

#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Entry;
class elWidget_TextCursor : public elWidget_base
{
  public:
    elWidget_TextCursor() = delete;
    elWidget_TextCursor( const constructor_t& constructor );
    virtual ~elWidget_TextCursor();

    friend class elWidget_Entry;
};
} // namespace GuiGL
} // namespace el3D
