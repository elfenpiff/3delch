#pragma once

#include "GuiGL/elWidget_Window.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Accordion : public elWidget_Window
{
  public:
    enum class AccordionState
    {
        Open,
        Close
    };
    struct accordion_t
    {
        elWidget_Button_ptr button;
        elWidget_base_ptr   frame;
    };

    elWidget_Accordion() = delete;
    elWidget_Accordion( const constructor_t& constructor ) noexcept;
    virtual ~elWidget_Accordion() = default;

    virtual void
    Reshape() override;

    bb::product_ptr< accordion_t >
    CreateAccordion(
        const std::string&    name,
        const AccordionState& state = AccordionState::Open ) noexcept;

  private:
    void
    ToggleAccordion( accordion_t& accordion ) noexcept;
    void
    RemoveAccordion( accordion_t& accordion ) noexcept;

  private:
    bb::elGenericFactory< accordion_t > accordionFactory;
};
} // namespace GuiGL
} // namespace el3D
