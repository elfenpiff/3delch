#pragma once

#include "GuiGL/elWidget_Menu.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Menubar : public elWidget_Menu
{
  public:
    elWidget_Menubar() = delete;
    elWidget_Menubar( const constructor_t& constructor );
    virtual ~elWidget_Menubar() = default;

    virtual void
    Reshape() override;

    // use SetButtonAlignment to align it to top, bottom, left, right
  protected:
    virtual void
    AlignSubMenus() noexcept override;
};
} // namespace GuiGL
} // namespace el3D
