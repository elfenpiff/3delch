#pragma once

#include "GuiGL/elWidget_base.hpp"

#include <vector>

namespace el3D
{
namespace GuiGL
{
class elWidget_Entry;

namespace Grid
{
enum class Resize : uint64_t
{
    None              = 0,
    FitToColumn       = 1,
    SpanOverColumns_2 = 2,
    SpanOverColumns_3 = 3,
};
struct elementProperties_t
{
    Resize resizeOption = Resize::FitToColumn;
};
} // namespace Grid
class elWidget_Grid : public elWidget_base
{
  public:
    elWidget_Grid() = delete;
    elWidget_Grid( const constructor_t& constructor );
    virtual ~elWidget_Grid() = default;

    template < typename T >
    std::enable_if_t< !std::is_same_v< T, elWidget_Titlebar >, widget_t< T > >
    CreateInGrid(
        const glm::uvec2&         position,
        Grid::elementProperties_t properties = Grid::elementProperties_t(),
        const std::string&        className  = "",
        const std::function< void( elWidget_base* ) >& customDeleter =
            std::function< void( elWidget_base* ) >() ) noexcept;

    void
    SetGridSpacing( const glm::vec2& spacing ) noexcept;
    void
    SetUniformElementHeight( const std::optional< float > v ) noexcept;

    virtual void
    Reshape() override;

  private:
    void
    AdjustGridSizeTo( const glm::uvec2& position ) noexcept;
    void
    Remove( const elWidget_base* const ptr ) noexcept;
    bool
    AdjustSizeToFitChildren() noexcept override;

  private:
    struct element_t
    {
        elWidget_base*            ptr = nullptr;
        Grid::elementProperties_t properties;
    };

    std::vector< std::vector< element_t > > gridContents;
    glm::uvec2                              gridSize    = { 0U, 0U };
    glm::vec2                               gridSpacing = { 0.0f, 0.0f };
    std::optional< float >                  uniformElementHeight;
};
} // namespace GuiGL
} // namespace el3D

#include "GuiGL/elWidget_Grid.inl"
