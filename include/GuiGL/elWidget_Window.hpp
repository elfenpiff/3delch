#pragma once

#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_Window : public elWidget_base
{
  public:
    elWidget_Window() = delete;
    elWidget_Window( const constructor_t& constructor );
    virtual ~elWidget_Window() = default;

    virtual void
    Reshape() override;

    virtual void
    SetState( const widgetStates_t ) noexcept override;

    glm::vec2
    GetScrollFactor() const noexcept;
    void
    SetScrollFactor( const glm::vec2& ) noexcept;
    void
    ScrollTo( const glm::vec2& ) noexcept;
    const glm::vec2&
    GetScrollSpeed() const noexcept;
    glm::vec4
    GetScrollbarWidth() const noexcept;
    void
    SetScrollbarWidth( const glm::vec4& ) noexcept;
    glm::vec2
    GetHasNaturalScrolling() const noexcept;
    void
    SetHasNaturalScrolling( const glm::vec2& ) noexcept;

    glm::ivec4
    GetIsScrollbarEnabled() const noexcept;
    void
    SetIsScrollbarEnabled( const bool enableTop, const bool enableBottom,
                           const bool enableLeft,
                           const bool enableRight ) noexcept;

  protected:
    void
    SetupSlider( const bool enable, const bool isHorizontal, const float width,
                 const positionCorner_t pointOfOrigin,
                 elWidget_Slider_ptr& ) noexcept;
    void
    SliderPositionChange( const bool isHorizontal, const float value ) noexcept;
    void
    UpdateSliderOffsetAndSizeRemainder() noexcept;
    void
    AdjustSliderPositionAndWidth() noexcept;
    void
    ReshapeSlider( elWidget_Slider_ptr& s, const bool isHorizontal ) noexcept;
    void
    CreateScrollEvent() noexcept;
    void
    AdjustChildPositionInWindow() noexcept;
    virtual void
    ScrollEventCallback( const SDL_Event e ) noexcept override;
    void
    UpdateWindowDimensions() noexcept override;
    void
    AdjustRelativeScrollPosition( const glm::vec4& oldDimension,
                                  const glm::vec2& oldSliderOffset ) noexcept;
    glm::vec2
    CalculatePositionOffsetForChilds() const noexcept;

  protected:
    struct
    {
        elWidget_Slider_ptr top;
        elWidget_Slider_ptr bottom;
        elWidget_Slider_ptr left;
        elWidget_Slider_ptr right;
        glm::vec2           sizeRemainder           = glm::vec2( 1.0f );
        glm::vec2           offset                  = glm::vec2( 0.0f );
        glm::vec2           absolutPositionOffset   = { 0.0f, 0.0f };
        glm::vec2           currentRelativePosition = { 0.0f, 0.0f };
    } slider;

    bb::product_ptr< GLAPI::elEvent > scrollEvent;
    glm::vec2                         scrollSpeed{ 75.0f, 75.0f };

    glm::vec2
    GetFinalSliderPosition() const noexcept;

  private:
    void
    SetSliderCurrentRelativePosition( const bool  isHorizontal,
                                      const float value ) noexcept;
    void
    CreateSlider( const bool enableTop, const bool enableBottom,
                  const bool enableLeft, const bool enableRight ) noexcept;
};
} // namespace GuiGL
} // namespace el3D
