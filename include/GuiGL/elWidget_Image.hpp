#pragma once

#include "GLAPI/elImage.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "buildingBlocks/product_ptr.hpp"
#include "utils/elRawPointer.hpp"

namespace el3D
{
namespace OpenGL
{
class elTexture_2D;
}

namespace GuiGL
{
class elWidget_Image : public elWidget_base
{
  public:
    elWidget_Image() = delete;
    elWidget_Image( const constructor_t &constructor );
    virtual ~elWidget_Image();
    void
    SetImageFromTexture( const OpenGL::elTexture_base *const ) noexcept;
    void
    SetImage( const GLAPI::elImage * ) noexcept;
    void
    SetImageFromByteStream( const utils::byteStream_t &byteStream,
                            const uint64_t             width,
                            const uint64_t             height ) noexcept;
    void
    SetImageFromBytePointer( const utils::elRawPointer< utils::byte_t > data,
                             const uint64_t                             width,
                             const uint64_t height ) noexcept;


  private:
    std::unique_ptr< OpenGL::elTexture_2D >        image;
    bb::product_ptr< OpenGL::textureAttachment_t > usedTexture;

    void
    AddTextureToVertexObject(
        const OpenGL::elTexture_base *const texture ) noexcept;
};
} // namespace GuiGL
} // namespace el3D
