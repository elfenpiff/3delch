#pragma once

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Textbox.hpp"
#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
class elWidget_InfoMessage : public elWidget_base
{
  public:
    elWidget_InfoMessage() = delete;
    elWidget_InfoMessage( const constructor_t &constructor );
    virtual ~elWidget_InfoMessage();

    void
    SetText( const std::vector< std::string > & ) noexcept;
    void
    InsertLine( const std::string &, const size_t line ) noexcept;
    void
    ModifyLine( const std::string &, const size_t line ) noexcept;
    void
    RemoveLine( const size_t line ) noexcept;
    virtual void
    SetCallbackOnClose( const std::function< void() > & ) noexcept;

  protected:
    void
    ButtonCallback() noexcept;

    bb::product_ptr< handler::Callable > onCloseCallback;
    elWidget_Textbox_ptr                 textBox;
    elWidget_Button_ptr                  button;
};
} // namespace GuiGL
} // namespace el3D
