#pragma once

#include <atomic>
#include <optional>

namespace el3D
{
namespace concurrent
{
// threadsafe ringbuffer with only 1 element
// copy/move ctor/assignment are not threadsafe!
template < typename T >
class elStreamBuffer
{
  public:
    elStreamBuffer() = default;
    ~elStreamBuffer();

    // not threadsafe
    elStreamBuffer( const elStreamBuffer& );
    // not threadsafe
    elStreamBuffer( elStreamBuffer&& );

    // not threadsafe
    elStreamBuffer&
    operator=( const elStreamBuffer& );
    // not threadsafe
    elStreamBuffer&
    operator=( elStreamBuffer&& );

    void
    Push( const T& t ) noexcept;
    std::optional< T >
    Pop() noexcept;

  private:
    std::atomic< T* > buffer{nullptr};
};
} // namespace concurrent
} // namespace el3D

#include "concurrent/elStreamBuffer.inl"
