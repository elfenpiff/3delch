#pragma once

#include "concurrent/elSmartLock.hpp"
#include "units/Time.hpp"

#include <atomic>
#include <condition_variable>
#include <limits>
#include <optional>
#include <queue>

namespace el3D
{
namespace concurrent
{
/// @brief PUsh POp based data structure
template < typename Container >
class elPuPo
{
  public:
    elPuPo()                = default;
    elPuPo( const elPuPo& ) = default;
    elPuPo( elPuPo&& )      = default;
    ~elPuPo();

    /// @brief helper function which should be called before the destructor
    ///         is called to wake up all threads which are waiting in a blocking
    ///         (multi) pop call
    void
    Terminate() noexcept;


    elPuPo&
    operator=( const elPuPo& ) = default;
    elPuPo&
    operator=( elPuPo&& ) = default;

    void
    Push( const typename Container::ValueType& t );
    template < typename Allocator >
    void
    MultiPush(
        const std::vector< typename Container::ValueType, Allocator >& t );

    std::optional< typename Container::ValueType >
    BlockingPop();
    template < typename OutputContainer >
    std::optional< OutputContainer >
    BlockingMultiPop( const size_t n = 0 );

    std::optional< typename Container::ValueType >
    TryPop();
    template < typename OutputContainer >
    std::optional< OutputContainer >
    TryMultiPop( const size_t n = 0 );

    std::optional< typename Container::ValueType >
    TimedPop( const units::Time& timeout );
    template < typename OutputContainer >
    std::optional< OutputContainer >
    TimedMultiPop( const units::Time& timeout, const size_t n = 0 );

    /// @brief clear the container every pending timed/blocking pop can maybe
    ///         return a nullopt
    void
    Clear() noexcept;
    bool
    IsEmpty() const noexcept;
    size_t
    Size() const noexcept;

  private:
    elSmartLock< Container > base;
    bool                     doTerminate{false};
    size_t                   clearID{0};
};
} // namespace concurrent
} // namespace el3D
#include "concurrent/elPuPo.inl"
