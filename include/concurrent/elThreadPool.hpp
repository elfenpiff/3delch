#pragma once

#include "buildingBlocks/elFiFo.hpp"
#include "concurrent/elPuPo.hpp"

#include <functional>
#include <thread>
#include <vector>

namespace el3D
{
namespace concurrent
{
class elThreadPool
{
  public:
    elThreadPool( const uint64_t numberOfThreads ) noexcept;
    elThreadPool( const elThreadPool& ) = delete;
    elThreadPool( elThreadPool&& )      = delete;
    ~elThreadPool();

    elThreadPool&
    operator=( const elThreadPool& ) = delete;
    elThreadPool&
    operator=( elThreadPool&& ) = delete;

    void
    AddTask( const std::function< void() >& f ) noexcept;
    uint64_t
    GetNumberOfThreads() const noexcept;
    static elThreadPool&
    GetGlobalThreadPool(
        const uint64_t numberOfThreads = GLOBAL_THREADPOOL_SIZE ) noexcept;

  private:
    void
    MainLoop() noexcept;

  private:
    static constexpr uint64_t                       GLOBAL_THREADPOOL_SIZE = 16;
    std::atomic_bool                                keepRunning{true};
    std::vector< std::thread >                      threads;
    elPuPo< bb::elFiFo< std::function< void() > > > tasks;
};
} // namespace concurrent
} // namespace el3D
