#pragma once

#include "buildingBlocks/elFiFo.hpp"
#include "concurrent/elSmartLock.hpp"

#include <functional>
#include <memory>
#include <thread>

namespace el3D
{
namespace concurrent
{
class elBackgroundTask
{
  public:
    elBackgroundTask() noexcept;
    elBackgroundTask( const elBackgroundTask& rhs ) = delete;
    elBackgroundTask( elBackgroundTask&& rhs )      = default;
    ~elBackgroundTask();

    elBackgroundTask&
    operator=( const elBackgroundTask& rhs ) = delete;
    elBackgroundTask&
    operator=( elBackgroundTask&& rhs ) = default;

    void
    AddTask( const std::function< void() >& task ) noexcept;

  private:
    std::vector< std::thread >                      tasks;
    concurrent::elSmartLock< bb::elFiFo< size_t > > reuseableTasks;
};
} // namespace concurrent
} // namespace el3D
