#pragma once

#include "units/Time.hpp"

#include <condition_variable>
#include <memory>
#include <mutex>

namespace el3D
{
namespace concurrent
{
template < typename T >
class elSmartLock
{
  public:
    class Guard
    {
      public:
        Guard( T* const object, std::mutex* const mtx ) noexcept;
        ~Guard() noexcept;

        Guard( const Guard& ) = delete;
        Guard&
        operator=( const Guard& ) = delete;

        Guard( Guard&& );
        Guard&
        operator=( Guard&& );

        T*
        operator->() noexcept;
        const T*
        operator->() const noexcept;

        T&
        operator*() noexcept;
        const T&
        operator*() const noexcept;

      private:
        T*          object;
        std::mutex* mtx;
    };

    template < typename... Targs >
    static elSmartLock
    Create( Targs&&... args ) noexcept;

    elSmartLock() noexcept;
    elSmartLock( const elSmartLock& rhs );
    elSmartLock( elSmartLock&& rhs ) noexcept;

    elSmartLock&
    operator=( const elSmartLock& rhs );
    elSmartLock&
    operator=( elSmartLock&& rhs ) noexcept;

    Guard
    operator->() noexcept;
    const Guard
    operator->() const noexcept;

    Guard
    GetLockGuard() noexcept;
    const Guard
    GetLockGuard() const noexcept;

    T&
    UnsafeAccess() noexcept;
    const T&
    UnsafeAccess() const noexcept;

    T
    GetCopy() const noexcept;

    template < typename Predicate >
    void
    Wait( const Predicate& p ) noexcept;

    template < typename Predicate >
    bool
    WaitFor( const units::Time& timeout, const Predicate& p ) noexcept;

    void
    NotifyOne() noexcept;

    void
    NotifyAll() noexcept;

  private:
    elSmartLock( const std::shared_ptr< T >& object );

    std::shared_ptr< T >    object;
    mutable std::mutex      mtx;
    std::condition_variable condition;
};
} // namespace concurrent
} // namespace el3D
#include "concurrent/elSmartLock.inl"
