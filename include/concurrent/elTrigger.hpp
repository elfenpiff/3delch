#pragma once

#include "units/Time.hpp"

#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <mutex>
#include <optional>

namespace el3D
{
namespace concurrent
{
template < typename T >
class elTrigger
{
  public:
    using predicate_t = std::function< bool( const T& t ) >;

    elTrigger( const T&           initialValue,
               const predicate_t& predicate = predicate_t() ) noexcept;
    ~elTrigger() noexcept;

    std::optional< T >
    BlockingWait( const predicate_t& customPredicate = predicate_t() ) const
        noexcept;
    std::optional< T >
    TimedWait( const units::Time& timeout,
               const predicate_t& customPredicate = predicate_t() ) const
        noexcept;
    std::optional< T >
    TryWait( const predicate_t& customPredicate = predicate_t() ) const
        noexcept;

    void
    Set( const T& value ) noexcept;
    void
    SetWithSetter( const std::function< T( const T& ) >& setter ) noexcept;
    T
    Get() const noexcept;

    void
    TriggerOne() const noexcept;
    void
    TriggerAll() const noexcept;

  private:
    mutable std::mutex              valueMutex;
    mutable std::condition_variable conditionVariable;
    mutable std::atomic< uint64_t > inWaitCounter{0};
    T                               value;
    predicate_t                     predicate;
    bool                            destructorCalled{false};
};
} // namespace concurrent
} // namespace el3D

#include "concurrent/elTrigger.inl"
