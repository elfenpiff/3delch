#pragma once

#include "DesignPattern/Creation.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/types.hpp"
#include "logging/elLog.hpp"
#include "units/Time.hpp"

#include <cstring>
#ifndef _WIN32
#include <semaphore.h>
#else
#include <stdio.h>
#include <windows.h>
#endif
#include <chrono>
#include <stdexcept>

namespace el3D
{
namespace concurrent
{
/*
    \brief The copy and move constructor as well as the copy and move assignment
            operator are not thread safe!!
*/
class elSemaphore : public DesignPattern::Creation< elSemaphore, bb::errno_t >
{
  public:
    ~elSemaphore();

    elSemaphore( const elSemaphore& rhs ) = delete;
    elSemaphore( elSemaphore&& rhs );

    elSemaphore&
    operator=( const elSemaphore& rhs ) = delete;
    elSemaphore&
    operator=( elSemaphore&& rhs );

    bb::elExpected< bb::errno_t >
    Post();
    bb::elExpected< bool, bb::errno_t >
    TryWait();
    bb::elExpected< bool, bb::errno_t >
    TimedWait( const units::Time& timeout );
    bb::elExpected< bb::errno_t >
    Wait();
    bb::elExpected< bb::errno_t >
    Reset();

#ifndef _WIN32
    bb::elExpected< int, bb::errno_t >
    GetValue() const;
#endif

    friend struct DesignPattern::Creation< elSemaphore, bb::errno_t >;

  private:
    elSemaphore();
    elSemaphore( const unsigned int value );
    bb::elExpected< bb::errno_t >
    Init( const unsigned int value );
    bb::elExpected< bb::errno_t >
    Destroy();

  private:
    bool destroySemaphore{ false };
#ifndef _WIN32
    sem_t handle;
#else
    HANDLE                handle;
    static constexpr LONG INITIAL_SEMAPHORE_VALUE = 0;
    static constexpr LONG MAX_SEMAPHORE_VALUE     = LONG_MAX;
#endif
};
} // namespace concurrent
} // namespace el3D
