#pragma once

#include "world/elObject.hpp"
#include "world/types.hpp"

#include <vector>

namespace el3D
{
namespace world
{
struct interactiveMeshProperties_t
{
    color4_t hoverDiffuseColor{ 0xff, 0x99, 0x33, 0xff };
    color4_t hoverGlowColor{ 0xff, 0x99, 0x33, 0x55 };
    bool     doesStickToCamera = false;
};

class elInteractiveMesh
    : public OpenGL::elModelMatrix< elInteractiveMesh,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    elInteractiveMesh( const objectSettings_t             settings,
                       const std::vector< GLfloat >&      vertices,
                       const std::vector< GLuint >&       elements,
                       const interactiveMeshProperties_t& properties =
                           interactiveMeshProperties_t() ) noexcept;

    elInteractiveMesh( const objectSettings_t settings, const float gridSize,
                       const uint64_t                     numberOfGridElements,
                       const interactiveMeshProperties_t& properties =
                           interactiveMeshProperties_t() ) noexcept;

    bool
    HasMouseHover() const noexcept;
    glm::vec3
    GetInteractiveCoordinates() const noexcept;
    void
    SetStickToCamera( const bool value ) noexcept;

    void
    SetPosition( const glm::vec3& position ) noexcept override;
    void
    SetScaling( const glm::vec3& scaling ) noexcept override;
    void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    glm::vec3
    GetPosition() const noexcept override;
    glm::vec3
    GetScaling() const noexcept override;
    glm::vec3
    GetRotationAxis() const noexcept override;
    float
    GetRotationDegree() const noexcept override;

    void
    MainLoopCallback() noexcept;

  private:
    void
    EventColorCallback() noexcept;
    void
    RepositionToCamera() noexcept;
    void
    Create( const std::vector< GLfloat >& vertices,
            const std::vector< GLuint >&  elements ) noexcept;

  private:
    static constexpr uint32_t INVALID_COLOR =
        std::numeric_limits< uint32_t >::max();
    static constexpr uint64_t INVALID_OFFSET =
        std::numeric_limits< uint64_t >::max();

    interactiveMeshProperties_t properties;

    bb::product_ptr< OpenGL::elGeometricObject_VertexObject > object;
    bb::product_ptr< OpenGL::elTexture_2D >                   objectColor;
    bb::product_ptr< OpenGL::elTexture_2D >                   objectNormal;
    bb::product_ptr< OpenGL::elTexture_2D >                   objectSpecular;
    bb::product_ptr< OpenGL::elTexture_2D >                   objectRoughness;
    bb::product_ptr< OpenGL::elTexture_2D >                   objectEmission;

    bb::product_ptr< HighGL::elEventColorRange > eventColorRange;

    struct section_t
    {
        glm::vec3 coordinates;
        uint64_t  elementStart;
    };

    std::map< uint32_t, section_t > color2section;
    uint32_t                        activeColorIndex    = INVALID_COLOR;
    uint64_t                        activeElementOffset = INVALID_OFFSET;
    uint64_t                        numberOfVertices    = 0U;
    float                           gridDiameter        = 0.0f;
    bool                            doesStickToCamera   = false;
};
} // namespace world
} // namespace el3D
