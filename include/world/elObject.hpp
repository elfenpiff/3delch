#pragma once

#include "3Delch/elScene.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"

namespace el3D
{
namespace world
{
class elPhysics;
class elWorld;
struct objectSettings_t
{
    elWorld*                                       world         = nullptr;
    _3Delch::elScene*                              scene         = nullptr;
    HighGL::elGeometricObject_UnifiedVertexObject* uvo           = nullptr;
    elPhysics*                                     physics       = nullptr;
    HighGL::elGeometricObject_UnifiedVertexObject* silhouetteUvo = nullptr;
    HighGL::elGeometricObject_UnifiedVertexObject* wireframeUvo  = nullptr;
};
class elObject
{
  public:
    elObject( const objectSettings_t settings ) noexcept;
    elObject( const elObject& ) = delete;
    elObject( elObject&& )      = delete;
    virtual ~elObject()         = default;

    elObject&
    operator=( const elObject& ) = delete;
    elObject&
    operator=( elObject&& ) = delete;

  protected:
    _3Delch::elScene&
    GetScene() const noexcept;
    HighGL::elGeometricObject_UnifiedVertexObject&
    GetUvo() const noexcept;
    elPhysics&
    GetPhysics() const noexcept;
    HighGL::elGeometricObject_UnifiedVertexObject&
    GetSilhouetteUvo() const noexcept;
    HighGL::elGeometricObject_UnifiedVertexObject&
    GetWireframeUvo() const noexcept;
    elWorld&
    GetWorld() const noexcept;

    template < OpenGL::RenderTarget Target, typename T, typename... Targs >
    typename _3Delch::ObjectSettings< static_cast< int >( Target ), T >::Type
    CreateCustomObject( Targs&&... args ) noexcept;

    bb::product_ptr< HighGL::elLight_DirectionalLight >
    CreateDirectionalLight( const glm::vec3& color, const glm::vec3& direction,
                            const GLfloat diffuseIntensity,
                            const GLfloat ambientIntensity ) noexcept;

    bb::product_ptr< HighGL::elLight_PointLight >
    CreatePointLight( const glm::vec3& color, const glm::vec3& attenuation,
                      const glm::vec3& position, const float diffuseIntensity,
                      const float ambientIntensity ) noexcept;

    template < typename... Targs >
    bb::elExpected< bb::product_ptr< HighGL::elSkybox >,
                    HighGL::elSkybox_Error >
    CreateSkybox( Targs&&... args ) noexcept;

  private:
    objectSettings_t settings;
};
} // namespace world
} // namespace el3D

#include "world/elObject.inl"
