#pragma once

#include "world/elModel.hpp"
#include "world/elModelGroup.hpp"
#include "world/elObject.hpp"
#include "world/types.hpp"

namespace el3D
{
namespace world
{
enum class ModelEffectType
{
    Silhouette,
    Wireframe
};

struct ModelEffectProperties
{
    bool            performExpensiveVertexElimination = false;
    float           vertexEliminationMinDistance      = 0.001f;
    color4_t        diffuseColor{ 0xff, 0x99, 0x33, 0xff };
    color4_t        glowColor{ 0xff, 0x99, 0x33, 0x00 };
    float           lineSize   = 2.0f;
    ModelEffectType effectType = ModelEffectType::Wireframe;
};

class elModelEffect : public elObject
{
  public:
    using ModelMatrixGetter_t = std::function< glm::mat4() >;

    elModelEffect(
        const objectSettings_t          settings,
        const OpenGL::objectGeometry_t& objectGeometry,
        const ModelMatrixGetter_t&      modelMatrixGetter = []
        { return glm::mat4( 1.0 ); },
        const ModelEffectProperties& properties =
            ModelEffectProperties() ) noexcept;

    elModelEffect( const objectSettings_t settings, const elModel& model,
                   const ModelEffectProperties& properties =
                       ModelEffectProperties() ) noexcept;
    elModelEffect( const objectSettings_t       settings,
                   const elModelGroup&          modelGroup,
                   const ModelEffectProperties& properties =
                       ModelEffectProperties() ) noexcept;

    void
    SetDiffuseColor( const color4_t& color ) noexcept;
    void
    SetGlowColor( const color4_t& color ) noexcept;
    void
    SetLineSize( const float value ) noexcept;

  private:
    void
    SetBuffer( const color4_t&                                      element,
               const HighGL::UnifiedVertexObject::floatBufferList_t bufferType,
               const float adjustment ) noexcept;

  private:
    bb::product_ptr< HighGL::UnifiedVertexObject::Object > object;
    ModelMatrixGetter_t                                    modelMatrixGetter;
    utils::CallbackGuard                                   updateCallback;
    float                                                  lineSize = 1.0f;
};
} // namespace world
} // namespace el3D
