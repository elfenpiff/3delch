#pragma once

#include "3Delch/elScene.hpp"
#include "GuiGL/GuiGL.hpp"
#include "RenderPipeline/elRenderer_GUI.hpp"

#include <string>

namespace el3D
{
namespace world
{
class elGui
{
  public:
    elGui( _3Delch::elScene& scene ) noexcept;
    elGui( const elGui& ) = delete;
    elGui( elGui&& )      = delete;
    elGui&
    operator=( const elGui& ) = delete;
    elGui&
    operator=( elGui&& ) = delete;

    GuiGL::elWidget_MouseCursor&
    GetMouseCursor() noexcept;
    void
    SetShowMouseCursor( const bool ) noexcept;
    bool
    DoesShowMouseCursor() const noexcept;

  protected:
    template < typename T >
    GuiGL::widget_t< T >
    Create( const std::string& className = "" ) noexcept;

  private:
    RenderPipeline::elRenderer_GUI* gui = nullptr;
};
} // namespace world
} // namespace el3D

#include "world/elGui.inl"
