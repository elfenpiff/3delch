#pragma once

#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "world/elObject.hpp"
#include "world/types.hpp"

#include <optional>

namespace el3D
{
namespace world
{
enum class GeometricType
{
    Points,
    Lines,
    LineStrip
};

struct geometricModelProperties_t
{
    std::vector< GLfloat > vertices;
    std::vector< GLfloat > colorPerVertex;
    glm::vec3              position     = { 0.0f, 0.0f, 0.0f };
    color4_t               uniformColor = { 0xff, 0xff, 0xff, 0xff };
    float                  drawSize     = 1.0f;
    GeometricType          type         = GeometricType::Points;
};

class elGeometricModel
    : public OpenGL::elModelMatrix< elGeometricModel,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    elGeometricModel( const objectSettings_t            settings,
                      const geometricModelProperties_t& properties ) noexcept;

    void
    SetDrawSize( const float size ) noexcept;
    void
    SetColor( const color4_t& color ) noexcept;
    void
    SetColorPerVertex( const std::vector< GLfloat >& colorPerVertex ) noexcept;

    void
    SetPosition( const glm::vec3& position ) noexcept override;
    void
    SetScaling( const glm::vec3& scaling ) noexcept override;
    void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    glm::vec3
    GetPosition() const noexcept override;
    glm::vec3
    GetScaling() const noexcept override;
    glm::vec3
    GetRotationAxis() const noexcept override;
    float
    GetRotationDegree() const noexcept override;

  private:
    OpenGL::DrawMode
    GeometricType2DrawMode( const GeometricType type ) const noexcept;

  private:
    bb::product_ptr< OpenGL::elGeometricObject_NonVertexObject > object;
};
} // namespace world
} // namespace el3D
