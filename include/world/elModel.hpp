#pragma once

#include "3Delch/elScene.hpp"
#include "HighGL/elCameraController_StickToObject.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elModelMatrix.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "utils/byteStream_t.hpp"
#include "world/elObject.hpp"
#include "world/elPhysics.hpp"
#include "world/types.hpp"

namespace el3D
{
namespace world
{
enum class TextureType
{
    Diffuse,
    Normal,
    Emission,
};

enum class GeometricObjectFacing
{
    Inner,
    Outer
};

constexpr const char* TEXTURE_TYPE_STRING[] = {
    "TextureType::Diffuse", "TextureType::Normal", "TextureType::Emission" };

class elModel
    : public OpenGL::elModelMatrix< elModel,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    elModel( const objectSettings_t          settings,
             const OpenGL::objectGeometry_t& objectGeometry,
             const GeometricObjectFacing     facing =
                 GeometricObjectFacing::Outer ) noexcept;
    elModel( const objectSettings_t settings,
             bb::product_ptr< HighGL::UnifiedVertexObject::Object >&&
                 object ) noexcept;

    elModel( const elModel& ) = delete;
    elModel( elModel&& )      = delete;
    elModel&
    operator=( const elModel& ) = delete;
    elModel&
    operator=( elModel&& ) = delete;

    virtual void
    SetPosition( const glm::vec3& ) noexcept override;
    virtual void
    SetScaling( const glm::vec3& ) noexcept override;
    virtual void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    virtual void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    virtual glm::vec3
    GetPosition() const noexcept override;
    virtual glm::vec3
    GetScaling() const noexcept override;
    virtual glm::vec3
    GetRotationAxis() const noexcept override;
    virtual float
    GetRotationDegree() const noexcept override;

    bool
    SetTexture( const TextureType type, const uint64_t x, const uint64_t y,
                const utils::byte_t* const dataPtr ) noexcept;
    bool
    SetTexture( const TextureType type, const std::string& fileName ) noexcept;

    void
    SetDiffuseColor( const color4_t& color ) noexcept;
    void
    SetEmissionColor( const color4_t& color ) noexcept;
    void
    SetSpecularColor( const color3_t&     color,
                      const utils::byte_t metallic ) noexcept;
    void
    SetShininessAndRoughness( const uint8_t shininess,
                              const uint8_t roughness ) noexcept;

    template < typename ShapeType, typename... ShapeTypeCTorArgs >
    void
    SetPhysicalProperties( const physicalProperties_t physicalProperties,
                           const ShapeTypeCTorArgs&... args ) noexcept;
    std::vector< GLfloat >
    GetVertices() const noexcept;
    std::vector< GLuint >
    GetElements() const noexcept;

    glm::mat4
    GetModelMatrix() const noexcept;

    const std::string&
    GetName() const noexcept;

    glm::vec3
    GetMin() const noexcept;
    glm::vec3
    GetMax() const noexcept;
    glm::vec3
    GetDimension() const noexcept;

    bb::product_ptr< elModel >
    Clone() const noexcept;

    bool
    AttachCamera( OpenGL::elCamera_Perspective& camera,
                  const glm::vec3&              positionOffset,
                  const glm::vec3               lookAtOffset ) noexcept;

  private:
    friend class elInteractiveSet;
    friend class elVehicle;

    void
    SetCustomPhysicalTransformGetter(
        const std::function< const btTransform&() >& v ) noexcept;

    bool
    AddTexture( const std::string& uniqueIdentifier, const TextureType type,
                const GLvoid* const pixels ) noexcept;
    template < typename T, uint64_t N, typename EnumType >
    void
    SetBuffer( const std::array< T, N >& element, const EnumType bufferType,
               const float adjustment = 1.0f ) noexcept;
    template < typename EnumType >
    void
    SetBuffer( const glm::vec4& element, const EnumType bufferType ) noexcept;
    void
    SetBuffer( const GLuint element,
               const HighGL::UnifiedVertexObject::uintBufferList_t
                   bufferType ) noexcept;

    glm::vec3
    GetExtrema( const std::function< float( float, float ) >& extremaGetter )
        const noexcept;

  private:
    bb::product_ptr< HighGL::UnifiedVertexObject::Object >    object;
    bb::product_ptr< elPhysics::Object >                      physical;
    std::optional< HighGL::elCameraController_StickToObject > cameraAttachment;
};
} // namespace world
} // namespace el3D

#include "world/elModel.inl"
