#pragma once

#include "HighGL/elSkybox.hpp"
#include "animation/elAnimatable.hpp"
#include "world/elObject.hpp"
#include "world/elSky_internal.hpp"

#include <memory>

namespace el3D
{
namespace world
{
static constexpr internal::ColoredSkybox_t           ColoredSkybox{};
static constexpr internal::ColoredSkyboxFromConfig_t ColoredSkyboxFromConfig{};
static constexpr internal::TexturedSkybox_t          TexturedSkybox{};
static constexpr internal::TexturedSkyboxFromConfig_t
    TexturedSkyboxFromConfig{};


class elSky : public animation::elAnimatable< elSky >, public elObject
{
  public:
    elSky( const objectSettings_t settings, const internal::ColoredSkybox_t,
           const glm::vec4&       color ) noexcept;
    elSky( const objectSettings_t settings, const internal::TexturedSkybox_t,
           const HighGL::elSkybox::textureConfig_t& textureConfig ) noexcept;
    elSky( const objectSettings_t settings,
           const internal::ColoredSkyboxFromConfig_t ) noexcept;
    elSky( const objectSettings_t settings,
           const internal::TexturedSkyboxFromConfig_t ) noexcept;
    virtual ~elSky() = default;

    void
    SetColor( const glm::vec4& color ) noexcept;
    void
    UseForReflection( const bool v ) noexcept;

  private:
    template < typename T >
    void
    SetupSkybox( const T& setting ) noexcept;

  private:
    bb::product_ptr< HighGL::elSkybox > skybox;
};
} // namespace world
} // namespace el3D
