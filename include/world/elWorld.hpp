#pragma once

#include "3Delch/elScene.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "buildingBlocks/elGenericFactory.hpp"
#include "utils/elCallbackVector.hpp"
#include "world/elModel.hpp"
#include "world/elObject.hpp"
#include "world/elPhysics.hpp"

#include <memory>
#include <vector>

namespace el3D
{
namespace world
{
class elWorld
{
  public:
    elWorld( _3Delch::elScene& scene,
             const glm::vec3&  gravity = { 0.0, -9.8, 0.0 } ) noexcept;
    elWorld( const elWorld& ) = delete;
    elWorld( elWorld&& )      = delete;
    elWorld&
    operator=( const elWorld& ) = delete;
    elWorld&
    operator=( elWorld&& ) = delete;
    virtual ~elWorld()     = default;

  protected:
    friend class elInteractiveSet;
    friend class elModelGroup;
    friend class elVehicle;
    friend class elModel;

    template < typename T, typename... Targs >
    bb::product_ptr< T >
    Create( Targs&&... args ) noexcept;

  private:
    _3Delch::elScene* scene = nullptr;
    elPhysics         physics;
    bb::product_ptr< HighGL::elGeometricObject_UnifiedVertexObject > uvo;

    struct
    {
        std::unique_ptr< OpenGL::elShaderProgram >                       shader;
        bb::product_ptr< HighGL::elGeometricObject_UnifiedVertexObject > uvo;
    } silhouette;

    struct
    {
        std::unique_ptr< OpenGL::elShaderProgram >                       shader;
        bb::product_ptr< HighGL::elGeometricObject_UnifiedVertexObject > uvo;
    } wireframe;

    std::vector< int >   defaultUVOTextureSize{ 1024, 1024, 64 };
    utils::CallbackGuard updateCallback;

    // should be always last member / cleanup can access other members
    bb::elGenericFactory< elObject > factory;
};
} // namespace world
} // namespace el3D

#include "world/elWorld.inl"
