#pragma once

#include "HighGL/elCameraController_StickToObject.hpp"
#include "animation/elAnimatable.hpp"
#include "units/Mass.hpp"
#include "units/Velocity.hpp"
#include "world/elModel.hpp"
#include "world/elModelGroup.hpp"

#include <optional>
#include <vector>

#ifdef _WIN32
#include <btBulletDynamicsCommon.h>
#else
#include <bullet/btBulletDynamicsCommon.h>
#endif

namespace el3D
{
namespace world
{
struct wheelProperties_t
{
    float     width            = 0.5f;
    float     radius           = 1.0f;
    float     connectionHeight = 0.0f;
    glm::vec3 direction{ 0.0f, -1.0f, 0.0f };
    glm::vec3 axis{ -1.0f, 0.0f, 0.0f };
    // x, y, z, w = rotation value in radiant
    glm::vec4 orientation{ 0.0f };
    glm::vec3 scaling{ 1.0f };

    float suspensionRestLength     = 1.0f;
    float suspensionStiffness      = 20.0f;
    float wheelsDampingCompression = 4.4f;
    float wheelsDampingRelaxation  = 2.3f;
    float frictionSlip             = 1000.0f;
    float rollInfluence            = 0.01f;

    float maxBrakeForce    = 75.0f;
    float maxSteeringValue = 0.5f;
    float steeringSpeed    = 2.0f;
    // x = forward, y = backward
    glm::vec2 maxEngineForce = glm::vec2{ 3000.0f };

    glm::vec3                       relativePosition;
    bool                            isFrontWheel;
    bb::product_ptr< elModelGroup > wheelModel;
};

struct vehicleProperties_t
{
    glm::vec3                        baseDimension{ 1.0f, 0.5f, 2.0f };
    units::Mass                      mass = units::Mass::Kilogramm( 1200.0 );
    bb::product_ptr< elModelGroup >  vehicleBodyModel;
    std::vector< wheelProperties_t > wheels;
};

class elVehicle : public elObject
{
  public:
    class elWheel : public animation::elAnimatable< elWheel >
    {
      public:
        void
        SetSteeringValue( const float value ) noexcept;
        float
        GetSteeringValue() const noexcept;
        void
        SetBrake( const float value ) noexcept;
        void
        ApplyEngineForce( const float value ) noexcept;

        void
        SteerToLeft() noexcept;
        void
        SteerToRight() noexcept;
        void
        SteerToCenter() noexcept;
        void
        SteerTo( const float value ) noexcept;

        void
        Forward() noexcept;
        void
        Backward() noexcept;
        void
        RollOut() noexcept;
        void
        Brake() noexcept;
        void
        ReleaseBrake() noexcept;

      private:
        friend class elVehicle;
        elWheel( bb::product_ptr< elModelGroup >&& object, elVehicle& vehicle,
                 const wheelProperties_t& properties ) noexcept;
        void
        UpdateModelMatrix() noexcept;
        void
        Reset() noexcept;

      private:
        elVehicle*                      vehicle;
        uint64_t                        id;
        bb::product_ptr< elModelGroup > object;
        glm::vec4                       orientation;
        glm::vec3                       scaling;
        float                           maxSteeringValue;
        glm::vec2                       maxEngineForce;
        float                           maxBrakeForce;
        float                           steeringSpeed;
        bool                            isFrontWheel;
    };

    elVehicle( const objectSettings_t settings,
               vehicleProperties_t&&  properties ) noexcept;
    elVehicle( const elVehicle& ) = delete;
    elVehicle( elVehicle&& )      = delete;
    elVehicle&
    operator=( const elVehicle& ) = delete;
    elVehicle&
    operator=( elVehicle&& ) = delete;
    ~elVehicle() noexcept    = default;

    elWheel&
    GetWheel( const uint64_t id ) noexcept;
    const elWheel&
    GetWheel( const uint64_t id ) const noexcept;
    uint64_t
    GetNumberOfWheels() const noexcept;

    void
    Forward() noexcept;
    void
    Backward() noexcept;
    void
    RollOut() noexcept;
    void
    Brake() noexcept;
    void
    ReleaseBrake() noexcept;
    void
    SetBrake( const float value ) noexcept;
    void
    ApplyEngineForce( const float value ) noexcept;


    units::Velocity
    GetSpeed() const noexcept;

    glm::vec3
    GetPosition() const noexcept;

    glm::vec3
    GetRotationAxis() const noexcept;
    float
    GetRotationDegree() const noexcept;
    glm::quat
    GetRotation() const noexcept;

    void
    SteerToLeft() noexcept;
    void
    SteerToRight() noexcept;
    void
    SteerToCenter() noexcept;
    void
    SteerTo( const float value ) noexcept;

    bool
    AttachCamera( OpenGL::elCamera_Perspective& camera,
                  const glm::vec3&              positionOffset,
                  const glm::vec3               lookAtOffset ) noexcept;

  private:
    void
    AddWheels( std::vector< wheelProperties_t >&& wheelSettings ) noexcept;
    void
    Reset() noexcept;

  private:
    friend class Wheel;
    bb::product_ptr< elModelGroup > bodyObject;
    std::vector< elWheel >          wheels;

    bb::product_ptr< internal::vehicle_t > vehicle;

    glm::vec3            baseDimensions;
    utils::CallbackGuard callback;

    std::optional< HighGL::elCameraController_StickToObject > cameraAttachment;
};
} // namespace world
} // namespace el3D
