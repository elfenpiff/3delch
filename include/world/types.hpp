#pragma once

#include <array>
#include <cstdint>
#include <glm/glm.hpp>

#ifdef _WIN32
#include <btBulletDynamicsCommon.h>
#else
#include <bullet/btBulletDynamicsCommon.h>
#endif


namespace el3D
{
namespace world
{
using color4_t = std::array< uint8_t, 4 >;
using color3_t = std::array< uint8_t, 3 >;

namespace internal
{
glm::vec3
ToGlm( const color3_t& c ) noexcept;
glm::vec4
ToGlm( const color4_t& c ) noexcept;

struct vehicle_t
{
    vehicle_t( btDiscreteDynamicsWorld& world, btRigidBody& body ) noexcept
        : rayCaster( &world ), vehicle( tuning, &body, &rayCaster )
    {
    }
    btDefaultVehicleRaycaster         rayCaster;
    btRaycastVehicle::btVehicleTuning tuning;
    btRaycastVehicle                  vehicle;
};
} // namespace internal

} // namespace world
} // namespace el3D
