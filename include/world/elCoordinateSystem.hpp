#pragma once

#include "3Delch/elScene.hpp"
#include "HighGL/elCoordinateSystemGenerator.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "buildingBlocks/product_ptr.hpp"
#include "world/elObject.hpp"
#include "world/types.hpp"

namespace el3D
{
namespace world
{
enum CoordinateSystemStyle
{
    MayorMinorPlane,
    MayorMinor,
};

struct CoordinateSystemProperties
{
    glm::vec2 gridSize = { 10, 10 };
    glm::vec3 position = { 0.0f, 0.0f, 0.0f };

    HighGL::elCoordinateSystemGenerator::GridStyle gridStyle =
        HighGL::elCoordinateSystemGenerator::GridStyle::Lines;

    CoordinateSystemStyle style = CoordinateSystemStyle::MayorMinorPlane;

    float    majorDrawSize     = 2.0f;
    float    minorDrawSize     = 1.0f;
    float    majorTileSize     = 1.0f;
    float    minorTileSize     = 0.1f;
    color4_t planeColor        = { 0xe5, 0xe5, 0xe5, 0xff };
    color4_t majorColor        = { 0x00, 0x00, 0x00, 0x33 };
    color4_t minorColor        = { 0x00, 0x00, 0x00, 0x1a };
    bool     doConnectToCamera = true;
};

class elCoordinateSystem
    : public OpenGL::elModelMatrix< elCoordinateSystem,
                                    animation::interpolation::Hermite >,
      public elObject
{
  public:
    elCoordinateSystem( const objectSettings_t            settings,
                        const CoordinateSystemProperties& properties ) noexcept;

    void
    SetMajorDrawSize( const float size ) noexcept;
    void
    SetMinorDrawSize( const float size ) noexcept;

    void
    SetMajorColor( const color4_t& color ) noexcept;
    void
    SetMinorColor( const color4_t& color ) noexcept;
    void
    SetPlaneColor( const color4_t& color ) noexcept;

    virtual void
    SetPosition( const glm::vec3& ) noexcept override;
    virtual void
    SetScaling( const glm::vec3& ) noexcept override;
    virtual void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    virtual void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    virtual glm::vec3
    GetPosition() const noexcept override;
    virtual glm::vec3
    GetScaling() const noexcept override;
    virtual glm::vec3
    GetRotationAxis() const noexcept override;
    virtual float
    GetRotationDegree() const noexcept override;

  private:
    void
    ConnectCamera( const OpenGL::elCamera_Perspective& camera ) noexcept;
    void
    DisconnectCamera() noexcept;
    void
    UpdateCameraPositionCallback() noexcept;

  private:
    static constexpr float PLANE_POSITION_CORRECTOR = 0.001f;

    HighGL::elCoordinateSystemGenerator coordinateSystemGenerator;

    bb::product_ptr< OpenGL::elGeometricObject_NonVertexObject > majorLines;
    bb::product_ptr< OpenGL::elGeometricObject_NonVertexObject > minorLines;
    bb::product_ptr< OpenGL::elGeometricObject_VertexObject >    plane;
    bb::product_ptr< OpenGL::elTexture_2D >                      planeColor;
    bb::product_ptr< OpenGL::elTexture_2D >                      planeNormal;
    bb::product_ptr< OpenGL::elTexture_2D >                      planeSpecular;
    bb::product_ptr< OpenGL::elTexture_2D >                      planeRoughness;
    bb::product_ptr< OpenGL::elTexture_2D >                      planeEmission;
    const OpenGL::elCamera_Perspective* camera = nullptr;
    utils::CallbackGuard                cameraUpdateCallback;
    glm::vec3                           position{ 0.0f };
    glm::vec3                           scaling{ 1.0f };
    glm::vec4                           rotation{ 0.0f };
    glm::mat4                           rotationMatrix{ 1.0f };
    glm::vec3                           planeScalingFactor{ 1.0f };
    glm::vec3                           planePositionAdjustment{ 0.0f };
};
} // namespace world
} // namespace el3D
