#pragma once

#include "animation/elAnimatable.hpp"
#include "units/Angle.hpp"
#include "world/elObject.hpp"

namespace el3D
{
namespace world
{
struct SunProperties
{
    glm::vec3 color            = { 0.9f, 0.9f, 1.0f };
    glm::vec3 direction        = { 0.0f, -1.0f, 0.0f };
    float     diffuseIntensity = 0.8f;
    float     ambientIntensity = 0.1f;
    bool      doCastShadow     = true;
    bool      hasFog           = false;
    float     fogIntensity     = 1.0f;
};

class elSun : public animation::elAnimatable< elSun >, public elObject
{
  public:
    elSun( const objectSettings_t settings,
           const SunProperties&   properties ) noexcept;

    void
    SetPosition( const units::Angle xAngle,
                 const units::Angle yAngle ) noexcept;
    void
    SetDirection( const glm::vec3& direction ) noexcept;

    void
    SetColor( const glm::vec3& color ) noexcept;

    void
    SetDiffuseIntensity( const float intensity ) noexcept;
    void
    SetAmbientIntensity( const float intensity ) noexcept;
    void
    SetCastShadow( const bool value ) noexcept;

    void
    Move( const units::Angle xSourceAngle, const units::Angle ySourceAngle,
          const glm::vec2 direction, const float speed ) noexcept;
    void
    MoveTo( const units::Angle xSourceAngle, const units::Angle ySourceAngle,
            const units::Angle xDestinationAngle,
            const units::Angle yDestinationAngle, const float speed ) noexcept;

    const SunProperties&
    GetProperties() const noexcept;

  private:
    void
    SetPositionImpl( const glm::vec2 angleInRadians ) noexcept;

  private:
    SunProperties                                       properties;
    bb::product_ptr< HighGL::elLight_DirectionalLight > light;
};
} // namespace world
} // namespace el3D
