#pragma once


#ifdef _WIN32
#include <btBulletDynamicsCommon.h>
#else
#include <BulletDynamics/MLCPSolvers/btDantzigSolver.h>
#include <BulletDynamics/MLCPSolvers/btMLCPSolver.h>
#include <BulletDynamics/MLCPSolvers/btSolveProjectedGaussSeidel.h>
#include <bullet/btBulletDynamicsCommon.h>
#endif
#include "buildingBlocks/elGenericFactory.hpp"
#include "units/Time.hpp"
#include "world/types.hpp"

#include <glm/glm.hpp>
#include <memory>
#include <optional>
#include <vector>

namespace el3D
{
namespace world
{
struct physicalProperties_t
{
    float mass = 0.0f;
};

struct geometricProperties_t
{
    glm::vec3 position = glm::vec3( 0.0f );
    glm::vec3 scaling  = glm::vec3( 0.0f );
    glm::vec4 rotation = glm::vec4( 0.0f );
};

btVector3
ToBulletVector( const glm::vec3& v ) noexcept;

class elPhysics
{
  public:
    // args: position, rotation
    using setModelMatrix_t = std::function< void( glm::mat4 ) >;

    class Object
    {
      public:
        Object( elPhysics& physics, btDiscreteDynamicsWorld& dynamicWorld,
                const std::function< btCollisionShape*() >& shapeCreator,
                const physicalProperties_t&                 physicalProperties,
                const geometricProperties_t&                geometry,
                const setModelMatrix_t& setModelMatrix ) noexcept;
        Object( const Object& ) = delete;
        Object( Object&& )      = delete;
        ~Object() noexcept;

        Object&
        operator=( const Object& ) = delete;
        Object&
        operator=( Object&& ) = delete;

        glm::vec3
        GetPosition() const noexcept;
        glm::vec4
        GetRotation() const noexcept;

        bb::product_ptr< Object >
        Clone( const setModelMatrix_t& setModelMatrix ) const noexcept;

        void
        Reset() noexcept;

      private:
        friend class elPhysics;
        friend class elVehicle;
        friend class elModel;
        friend class elModelGroup;

        void
        DisableDeactivation() noexcept;

        void
        SetCustomTransformGetter(
            const std::function< const btTransform&() >& v ) noexcept;

      private:
        elPhysics*                              physics;
        btDiscreteDynamicsWorld*                dynamicWorld;
        std::function< btCollisionShape*() >    shapeCreator;
        std::unique_ptr< btCollisionShape >     shape;
        setModelMatrix_t                        setModelMatrix;
        physicalProperties_t                    physicalProperties;
        geometricProperties_t                   geometry;
        std::unique_ptr< btDefaultMotionState > motionState;
        std::unique_ptr< btRigidBody >          body;
        std::function< const btTransform&() >   getCustomTransform;
    };

    elPhysics( const glm::vec3& gravity ) noexcept;

    elPhysics( const elPhysics& ) = delete;
    elPhysics( elPhysics&& )      = delete;

    elPhysics&
    operator=( const elPhysics& ) = delete;
    elPhysics&
    operator=( elPhysics&& ) = delete;

    void
    Update( const units::Time deltaTime ) noexcept;

    template < typename ShapeType, typename... Targs >
    bb::product_ptr< Object >
    AddCollisionShape( const physicalProperties_t&  physicalProperties,
                       const geometricProperties_t& geometricProperties,
                       const setModelMatrix_t&      setModelMatrix,
                       const Targs&... args ) noexcept;

    bb::product_ptr< internal::vehicle_t >
    CreateVehicle(
        elPhysics::Object&             object,
        const std::function< void() >& vehicleTransformUpdater ) noexcept;

    btDiscreteDynamicsWorld&
    GetDynamicsWorld() noexcept;

  private:
    btDefaultCollisionConfiguration collisionConfiguration;
    btCollisionDispatcher collisionDispatcher{ &collisionConfiguration };
    btDbvtBroadphase      overlappingPairCache;
    btSequentialImpulseConstraintSolver solver; // faster, but inaccurate
    // btDantzigSolver         mlcp;
    // btMLCPSolver            solver{ &mlcp };
    btDiscreteDynamicsWorld dynamicWorld{ &collisionDispatcher,
                                          &overlappingPairCache, &solver,
                                          &collisionConfiguration };

    // should be always last member / cleanup can access other members
    bb::elGenericFactory<
        Object,
        bb::CompositeProduct< internal::vehicle_t, std::function< void() > > >
        factory;
};
} // namespace world
} // namespace el3D

#include "world/elPhysics.inl"
