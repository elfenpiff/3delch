#pragma once
#include "HighGL/elEventTexture.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"

#include <map>

namespace el3D
{
namespace OpenGL
{
class elFrameBufferObject;
} // namespace OpenGL

namespace HighGL
{
class elShaderTexture;
}

namespace RenderPipeline
{
class elRenderer_ShaderTexture : public elRenderer_base
{
  public:
    enum class Refresh
    {
        always,
        once,
        never
    };

    elRenderer_ShaderTexture( internal::rendererShared_t shared );
    ~elRenderer_ShaderTexture();

    virtual void
    Render( const OpenGL::elCamera_Perspective & ) noexcept override;
    virtual void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

    size_t
    AddShaderTexture( const HighGL::elShaderTexture * ) noexcept;
    void
    RemoveShaderTexture( const size_t ) noexcept;
    void
    SetDoRefresh( const size_t index, const Refresh doRefresh ) noexcept;

  private:
    struct shader_t
    {
        const HighGL::elShaderTexture *shader;
        Refresh                        refresh = Refresh::always;
    };
    bb::elUniqueIndexVector< shader_t > shaderTextures;
};
} // namespace RenderPipeline
} // namespace el3D
