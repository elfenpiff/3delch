#pragma once

#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elUniformBufferObject.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"

#include <memory>

namespace el3D
{
namespace RenderPipeline
{
class elRenderer_Final : public elRenderer_base
{
  public:
    struct finalTextures_t
    {
        const OpenGL::elTexture_2D* finalWithPostProcessing;
        const OpenGL::elTexture_2D* gui;
        const OpenGL::elTexture_2D* guiGlow;
        const OpenGL::elTexture_2D* glow;
        const OpenGL::elTexture_2D* screenSpaceReflection;
    };

    elRenderer_Final( const internal::rendererShared_t& shared,
                      const finalTextures_t&            textures );

    void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;
    void
    DrawToScreen( const GLint x, const GLint y ) const noexcept;
    glm::vec4
    GetEventColorAtPosition( const glm::ivec2 position ) const noexcept;

  private:
    struct
    {
        attachmentId_t composedImage;
        attachmentId_t event;
    } texture;

    std::unique_ptr< OpenGL::elShaderProgram >       shader;
    std::unique_ptr< OpenGL::elUniformBufferObject > sceneSettings;
    OpenGL::elGeometricObject_VertexObject           onScreenQuad;

    struct
    {
        bb::product_ptr< OpenGL::textureAttachment_t > finalWithPostProcessing;
        bb::product_ptr< OpenGL::textureAttachment_t > gui;
        bb::product_ptr< OpenGL::textureAttachment_t > guiEvent;
        bb::product_ptr< OpenGL::textureAttachment_t > guiGlow;
        bb::product_ptr< OpenGL::textureAttachment_t > glow;
        bb::product_ptr< OpenGL::textureAttachment_t > screenSpaceReflection;
    } attachedTextures;
};
} // namespace RenderPipeline
} // namespace el3D
