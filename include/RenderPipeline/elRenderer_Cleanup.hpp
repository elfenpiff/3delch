#pragma once

#include "RenderPipeline/elRenderer_base.hpp"

namespace el3D
{
namespace RenderPipeline
{
class elRenderer_Cleanup : public elRenderer_base
{
  public:
    elRenderer_Cleanup( internal::rendererShared_t shared ) noexcept;

    void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

  private:
    struct
    {
        attachmentId_t glow;
        attachmentId_t material;
    } texture;
};
} // namespace RenderPipeline
} // namespace el3D
