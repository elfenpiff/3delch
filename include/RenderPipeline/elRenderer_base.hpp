#pragma once
#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elTexture_2D_Array.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/internal.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace OpenGL
{
class elFrameBufferObject;
}

namespace RenderPipeline
{
enum class elRenderer_Type
{
    deferred,
    forward,
    gui,
    shaderTexture,
    postProcessing,
    final,
    cleanup
};

class elRenderer_base
{
  public:
    virtual ~elRenderer_base();

    virtual void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept = 0;
    virtual void
    Reshape( const uint64_t x, const uint64_t y ) noexcept = 0;
    elRenderer_Type
    GetType() const noexcept;

  protected:
    enum class Attachment
    {
        Color,
        Depth,
        DepthStencil
    };

    struct attachmentId_t
    {
        uint64_t target    = 0u;
        uint64_t textureId = 0u;
    };

    struct arrayAttachementId_t
    {
        std::vector< uint64_t > target;
        uint64_t                textureId = 0u;
    };

    struct cubeMapAttachmentId_t
    {
        // right, left, top, bottom, back, front
        // posX, negX, posY, negY, posZ, negZ
        uint64_t target[6] = { 0u, 0u, 0u, 0u, 0u, 0u };
        uint64_t textureId = 0u;
    };

    struct fboDetails_t
    {
        uint64_t                               attachmentOffset = 0u;
        std::vector< OpenGL::elTexture_base* > outputTextures;
        std::vector< std::unique_ptr< OpenGL::elTexture_base > > ownedTextures;
    };

    elRenderer_base( const elRenderer_Type type,
                     const size_t          numberOfFrameBufferObjects,
                     const internal::rendererShared_t& shared ) noexcept;

    OpenGL::elTexture_2D&
    CreateTexture( attachmentId_t& id, const std::string& identifier,
                   const GLint internalFormat, const GLenum format,
                   const GLenum type, const Attachment attachment,
                   const uint64_t frameBufferNumber = 0u ) noexcept;
    OpenGL::elTexture_2D_Array&
    CreateTextureArray( arrayAttachementId_t& ids,
                        const std::string&    identifier,
                        const GLint internalFormat, const GLenum format,
                        const GLenum type, const uint64_t arraySize,
                        const uint64_t frameBufferNumber = 0u ) noexcept;
    OpenGL::elTexture_CubeMap&
    CreateTextureCubeMap( cubeMapAttachmentId_t& ids,
                          const std::string&     identifier,
                          const GLint internalFormat, const GLenum format,
                          const GLenum   type,
                          const uint64_t frameBufferNumber = 0u ) noexcept;

    void
    AttachTexture( attachmentId_t& id, OpenGL::elTexture_2D* const texture,
                   const Attachment attachment,
                   const uint64_t   frameBufferNumber = 0u );

    std::vector< GLenum >
    GenerateRenderTargets(
        const std::vector< uint64_t >& colorAttachmentTargets,
        const std::vector< GLenum >&   directTargets =
            std::vector< GLenum >() ) const noexcept;

    std::unique_ptr< OpenGL::elShaderProgram >
    CreateShaderProgram(
        const std::string& renderTarget, const std::string& geometricObjectType,
        const std::string&                           shaderGroup,
        const OpenGL::elGLSLParser::staticDefines_t& defines =
            OpenGL::elGLSLParser::staticDefines_t() ) const noexcept;

  protected:
    elRenderer_Type                                type;
    std::string                                    sceneName;
    const lua::elConfigHandler*                    config{ nullptr };
    const GLAPI::elWindow*                         window{ nullptr };
    std::string                                    shaderLookupDirectory;
    std::unique_ptr< OpenGL::elFrameBufferObject > fbo;
    std::vector< fboDetails_t >                    fboDetails;
    std::vector< bb::product_ptr< HighGL::elLight_DirectionalLight > >*
        directionalLights;
    std::vector< bb::product_ptr< HighGL::elLight_PointLight > >* pointLights;
};
} // namespace RenderPipeline
} // namespace el3D
