#pragma once

#include "HighGL/elEventTexture.hpp"
#include "RenderPipeline/elRenderPipeline.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"

namespace el3D
{
namespace GLAPI
{
class elWindow;
}
namespace lua
{
class elConfigHandler;
}

namespace RenderPipeline
{
class elRenderPipelineManager
{
  public:
    using pipelineElement_t = bb::elUniqueIndexVector<
        std::unique_ptr< elRenderPipeline > >::element_t;

    elRenderPipelineManager( const GLAPI::elWindow *const window,
                             const float                  multiSamplingFactor,
                             const lua::elConfigHandler *const config );
    elRenderPipelineManager( const elRenderPipelineManager & ) = delete;
    elRenderPipelineManager( elRenderPipelineManager && )      = delete;

    elRenderPipelineManager &
    operator=( const elRenderPipelineManager & ) = delete;
    elRenderPipelineManager &
    operator=( elRenderPipelineManager && ) = delete;

    pipelineElement_t
    CreatePipeline( const std::string &          sceneName,
                    GLAPI::elFontCache *const    fontCache,
                    GLAPI::elEventHandler *const eventHandler );
    void
    RemovePipeline( const size_t );

    void
    Render();
    void
    Reshape( const uint64_t x, const uint64_t y );
    void
    SetMultiSamplingFactor( const float value );
    float
    GetMultiSamplingFactor() const noexcept;
    void
    SetEventPosition( const glm::vec2 value );
    uint32_t
    GetCurrentEventColorIndex() const noexcept;
    bb::product_ptr< HighGL::elEventColor >
    AcquireEventColor() noexcept;
    bb::product_ptr< HighGL::elEventColorRange >
    AcquireEventColorRange( const uint32_t size ) noexcept;

    static void
    AddRenderTask( const std::function< void() > &task );

    friend class elRenderPipeline;
    friend class elRenderer_Deferred;

  private:
    static std::queue< std::function< void() > > renderTasks;
    const GLAPI::elWindow *                      window;
    const lua::elConfigHandler *                 config;
    float                                        multiSamplingFactor;
    glm::vec2                                    eventPosition{ 0, 0 };
    glm::uvec2                                   renderSize;
    GLint                                        viewportSize[4];
    HighGL::elEventTexture                       eventTexture;
    size_t                                       primaryPipeID = 0;
    bb::elUniqueIndexVector< std::unique_ptr< elRenderPipeline > > pipes;
    bb::elUniqueIDHandler< size_t >                                idHandler;
};
} // namespace RenderPipeline
} // namespace el3D
