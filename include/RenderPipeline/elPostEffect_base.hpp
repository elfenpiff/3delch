#pragma once

#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elTexture_2D.hpp"

#include <cstdint>


namespace el3D
{
namespace RenderPipeline
{
class elPostEffect_base
{
  public:
    virtual ~elPostEffect_base() = default;

    virtual void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept = 0;
    virtual void
    Reshape( const uint64_t x, const uint64_t y ) noexcept = 0;
    virtual OpenGL::elTexture_2D*
    GetOutput() noexcept = 0;
};
} // namespace RenderPipeline
} // namespace el3D
