#pragma once

#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "RenderPipeline/elPostEffect_GTAO.hpp"
#include "RenderPipeline/elPostEffect_base.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"

namespace el3D
{
namespace RenderPipeline
{
class elRenderer_PostProcessing : public elRenderer_base
{
  public:
    enum Output : uint64_t
    {
        GLOW = 0,
        GUI_GLOW,
        SCREEN_SPACE_REFLECTIONS,
        OUTPUT_END
    };

    elRenderer_PostProcessing(
        const internal::rendererShared_t& shared,
        const OpenGL::elTexture_2D&       guiGlow,
        const OpenGL::elTexture_2D&       gBufferPosition,
        const OpenGL::elTexture_2D&       gBufferNormal,
        const OpenGL::elTexture_2D&       finalTexture ) noexcept;

    void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

    const OpenGL::elTexture_2D&
    GetOutput( const Output output ) const noexcept;

    void
    UseSkyboxForReflection( const bool v ) noexcept;

  private:
    std::array< std::unique_ptr< elPostEffect_base >, OUTPUT_END > postEffects;
};
} // namespace RenderPipeline
} // namespace el3D
