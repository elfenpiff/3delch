#pragma once

#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "RenderPipeline/elPostEffect_base.hpp"

#include <memory>

namespace el3D
{
namespace lua
{
class elConfigHandler;
}

namespace RenderPipeline
{
class elPostEffect_GTAO : public elPostEffect_base
{
  public:
    elPostEffect_GTAO( const std::string&                sceneName,
                       const lua::elConfigHandler* const config,
                       const OpenGL::elTexture_2D* const depth,
                       const OpenGL::elTexture_2D* const normal,
                       const OpenGL::elTexture_2D* const diffuse ) noexcept;

    void
    Render( const OpenGL::elCamera_Perspective& camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;
    OpenGL::elTexture_2D*
    GetOutput() noexcept override;
    void
    SetLengthOfOneMeter( const float v ) noexcept;

  private:
    OpenGL::elCamera_Perspective scaledCamera;
    float                        scaling          = 1.0f;
    float                        lengthOfOneMeter = 1.0f;

    std::unique_ptr< OpenGL::elTexture_2D >    randomTexture;
    std::unique_ptr< HighGL::elShaderTexture > ambientOcclusion;
    std::unique_ptr< HighGL::elShaderTexture > spatialBlur;
    std::unique_ptr< HighGL::elShaderTexture > final;
};
} // namespace RenderPipeline
} // namespace el3D
