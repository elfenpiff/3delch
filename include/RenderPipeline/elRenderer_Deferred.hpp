#pragma once
#include "HighGL/elEventTexture.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elLight_DirectionalLight.hpp"
#include "HighGL/elLight_PointLight.hpp"
#include "OpenGL/elCamera_Orthographic.hpp"
#include "OpenGL/elFrameBufferObject.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elUniformBufferObject.hpp"
#include "RenderPipeline/elPostEffect_GTAO.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/types.hpp"

namespace el3D
{
namespace OpenGL
{
class elFrameBufferObject;
}

namespace RenderPipeline
{
namespace Deferred
{
struct objectSettings_t
{
    uint64_t geometricShader{ 0 };
    uint64_t directionalLightShadowMapShader{ 0 };
    uint64_t pointLightShadowMapShader{ 0 };
    GLint    pointLightPositionUid{ 0 };
};

template < typename T >
using Object = bb::product_ptr< bb::CompositeProduct< T, objectSettings_t > >;
} // namespace Deferred

class elRenderer_Deferred : public elRenderer_base
{
  public:
    elRenderer_Deferred( internal::rendererShared_t shared ) noexcept;
    virtual ~elRenderer_Deferred() override;

    void
    Render( const OpenGL::elCamera_Perspective &camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

    OpenGL::elTexture_2D *
    GetOutput() noexcept;
    OpenGL::elTexture_2D *
    GetNormalTexture() noexcept;
    OpenGL::elTexture_2D *
    GetPositionTexture() noexcept;
    OpenGL::elTexture_2D *
    GetDepthTexture() noexcept;

    template < typename T, typename... Targs >
    Deferred::Object< T >
    CreateObject( Targs &&...args ) noexcept;

    void
    SetLengthOfOneMeter( const float v ) noexcept;

  private:
    struct
    {
        attachmentId_t        diffuse;
        attachmentId_t        position;
        attachmentId_t        normal;
        attachmentId_t        specular;
        attachmentId_t        final;
        attachmentId_t        depth;
        attachmentId_t        glow;
        attachmentId_t        material;
        attachmentId_t        event;
        arrayAttachementId_t  directionalLightShadowMaps;
        cubeMapAttachmentId_t pointLightShadowMap;
    } texture;

    enum fboTargets_t : int
    {
        FBO_FINAL_IMAGE        = 0,
        FBO_DIRECTIONAL_SHADOW = 1,
        FBO_POINTLIGHT_SHADOW  = 2,
        FBO_END
    };

    static constexpr uint64_t INVALID_ID =
        std::numeric_limits< uint64_t >::max();

  private:
    Deferred::objectSettings_t
    GenerateObjectSettings(
        OpenGL::elGeometricObject_base &object,
        const uint64_t                  geometricShaderId = INVALID_ID,
        const uint64_t directionalLightShadowShaderId     = INVALID_ID,
        const uint64_t pointLightShadowShaderId = INVALID_ID ) const noexcept;
    void
    CreatePointLightBufferBlock( const int ) noexcept;
    void
    UpdatePointLightBuffer( const size_t k ) noexcept;
    void
    UpdateDirectionalLightBuffer( const HighGL::elLight_DirectionalLight *light,
                                  const size_t arrayIndex ) noexcept;
    void
    UpdateDirectionalLightCameraBuffer(
        const HighGL::elLight_DirectionalLight *light,
        const size_t                            arrayIndex ) noexcept;
    void
    RenderDirectionalLightShadowMap(
        const OpenGL::elCamera_Perspective &camera ) noexcept;
    void
    RenderDirectionalLight( const OpenGL::elCamera &camera ) noexcept;
    void
    RenderPointLightShadowMap(
        const HighGL::elLight_PointLight *const light ) noexcept;
    void
    RenderPointLight( const OpenGL::elCamera_Perspective &camera ) noexcept;
    void
    RenderGeometricObjects( const OpenGL::elCamera &camera ) noexcept;

    static GLint
    AddUniform( const std::string                   &uniformName,
                const OpenGL::elShaderProgram *const shader,
                const std::string                   &shaderName ) noexcept;
    static void
    AddCoordinateAttribute(
        const OpenGL::elShaderProgram *const        shader,
        OpenGL::elShaderProgram::shaderAttribute_t &attribute,
        const std::string                          &shaderName ) noexcept;

  private:
    struct geometricObjectShaderDefaults_t
    {
        std::unique_ptr< OpenGL::elShaderProgram > geometryShader;
        std::unique_ptr< OpenGL::elShaderProgram > directionalLightShadowShader;
        std::unique_ptr< OpenGL::elShaderProgram > pointLightShadowShader;
        void
        SetUp( const ObjectType           object,
               elRenderer_Deferred *const self ) noexcept;
    };

    struct
    {
        geometricObjectShaderDefaults_t unifiedVertexObject;
        geometricObjectShaderDefaults_t instancedObject;
        geometricObjectShaderDefaults_t vertexObject;

    } settings;


    struct directionalLightShader_t
    {
        enum buffer
        {
            COLOR                  = 0,
            DIRECTION              = 1,
            AMBIENT_INTENSITY      = 2,
            DIFFUSE_INTENSITY      = 3,
            BIASED_VIEW_PROJECTION = 4,
            NUMBER_OF_LIGHTS       = 5,
            RESOLUTION             = 6,
            SHADOW_BORDER_SIZE     = 7,
            HAS_SHADOW             = 8,
            CAMERA_POSITION        = 9,
            HAS_FOG                = 10,
            FOG_INTENSITY          = 11
        };

        std::unique_ptr< OpenGL::elShaderProgram >       shader;
        glm::uvec2                                       shadowMapSize{ 0, 0 };
        std::unique_ptr< OpenGL::elUniformBufferObject > uniformBuffer;
        OpenGL::elShaderProgram::shaderAttribute_t       coordAttrib;
        std::vector< OpenGL::elCamera_Orthographic >     camera;
        GLint                                            texNormal;
        GLint                                            texDiffuse;
        GLint                                            texPosition;
        GLint                                            texShadowMap;
        GLint                                            texMaterial;
        GLint                                            texSpecular;
        GLint                                            numberOfLights = 0;

        void
        SetUp( std::unique_ptr< OpenGL::elShaderProgram > &&shader_ ) noexcept;

    } directionalLightShaderSettings;

    struct pointLightShader_t
    {
        enum buffer
        {
            COLOR             = 0,
            POSITION          = 1,
            ATTENUATION       = 2,
            INTENSITY         = 3,
            AMBIENT_INTENSITY = 4,
            RESOLUTION        = 5,
            HAS_SHADOW        = 6,
            CAMERA_POSITION   = 7,
        };

        std::unique_ptr< OpenGL::elShaderProgram >   shader;
        glm::uvec2                                   shadowMapSize{ 0, 0 };
        OpenGL::elShaderProgram::shaderAttribute_t   coordAttrib;
        GLint                                        uidVPMatrix;
        std::vector< OpenGL::elUniformBufferObject > uniformBuffers;
        GLint                                        texPosition;
        GLint                                        texNormal;
        GLint                                        texDiffuse;
        GLint                                        texShadowMap;
        GLint                                        texMaterial;
        GLint                                        texSpecular;
        void
        SetUp( std::unique_ptr< OpenGL::elShaderProgram > &&shader,
               const glm::uvec2 &shadowMapSize ) noexcept;

    } pointLightShaderSettings;

    struct passThroughShader_t
    {
        std::unique_ptr< OpenGL::elShaderProgram > shader;
        OpenGL::elShaderProgram::shaderAttribute_t coordAttrib;
        GLint                                      uidVPMatrix;
        void
        SetUp( std::unique_ptr< OpenGL::elShaderProgram > &&shader ) noexcept;

    } passThrough;

    std::unique_ptr< elPostEffect_GTAO > ambientOcclusion;

    // should be always last member / cleanup can access other members
    bb::elGenericFactory< bb::CompositeProduct< OpenGL::elGeometricObject_base,
                                                Deferred::objectSettings_t > >
        factory;
};
} // namespace RenderPipeline
} // namespace el3D

#include "RenderPipeline/elRenderer_Deferred.inl"
