#pragma once

#include "RenderPipeline/elRenderer_Deferred.hpp"
#include "RenderPipeline/elRenderer_Forward.hpp"
#include "RenderPipeline/elRenderer_GUI.hpp"
#include "RenderPipeline/elRenderer_ShaderTexture.hpp"
#include "RenderPipeline/elRenderPipeline.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
