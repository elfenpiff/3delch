#pragma once
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "HighGL/elEventTexture.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"
#include "lua/elConfigHandler.hpp"

#include <memory>

namespace el3D
{
namespace GLAPI
{
class elEventHandler;
class elWindow;
class elFontCache;
} // namespace GLAPI

namespace GuiGL
{
class elWidget_base;
class elWidget_MouseCursor;
} // namespace GuiGL

namespace OpenGL
{
class elFrameBufferObject;
} // namespace OpenGL

namespace HighGL
{
class elShaderTexture;
}

namespace RenderPipeline
{
class elRenderer_GUI : public elRenderer_base
{
  public:
    elRenderer_GUI(
        internal::rendererShared_t shared, GLAPI::elFontCache *const fontCache,
        const std::function< size_t( HighGL::elShaderTexture * ) >
            &                                  shaderTextureRegistrator,
        const std::function< void( size_t ) > &shaderTextureDeregistrator,
        GLAPI::elEventHandler *const           eventHandler );

    void
    Render( const OpenGL::elCamera_Perspective & ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

    GuiGL::elWidget_base *
    GetRoot() const noexcept;
    template < typename T >
    std::enable_if_t< !std::is_same_v< T, GuiGL::elWidget_MouseCursor >,
                      GuiGL::widget_t< T > >
    Create( const std::string &className = "" );
    GuiGL::elWidget_MouseCursor *
    GetMouseCursor() noexcept;
    void
    SetShowMouseCursor( const bool ) noexcept;
    bool
    DoesShowMouseCursor() const noexcept;
    const OpenGL::elTexture_2D *
    GetGuiTexture() const noexcept;
    const OpenGL::elTexture_2D *
    GetGuiGlowTexture() const noexcept;

  private:
    struct
    {
        attachmentId_t final;
        attachmentId_t event;
        attachmentId_t glow;
        attachmentId_t depth;
    } texture;

    enum fbo_t
    {
        FBO_DIFFUSE = 0,
        FBO_GLOW    = 1,
        FBO_END,
    };

  private:
    std::unique_ptr< OpenGL::elShaderProgram > shader;
    std::unique_ptr< OpenGL::elShaderProgram > guiGlowShader;
    std::unique_ptr< GuiGL::elWidget_base >    cursorLayer;
    GuiGL::elWidget_MouseCursor_ptr            mouseCursor;
    std::unique_ptr< GuiGL::elWidget_base >    windowLayer;
    lua::elConfigHandler                       theme;
    float                                      glowScaling = 0.25f;
    GuiGL::CallbackContainer_t                 callbackContainer;

    mutable size_t screenWidth  = 1;
    mutable size_t screenHeight = 1;
};
} // namespace RenderPipeline
} // namespace el3D

#include "RenderPipeline/elRenderer_GUI.inl"
