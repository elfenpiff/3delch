#pragma once
#include "HighGL/elEventTexture.hpp"
#include "OpenGL/elFrameBufferObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elUniformBufferObject.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/types.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

namespace el3D
{
namespace OpenGL
{
class elFrameBufferObject;
}

namespace RenderPipeline
{
namespace Forward
{
struct objectSettings_t
{
    uint64_t                  geometricShader{ 0 };
    std::optional< uint64_t > directionalLightBufferIndex;
    std::optional< uint64_t > sceneBufferIndex;
};

template < typename T = OpenGL::elGeometricObject_base >
using Object = bb::product_ptr< bb::CompositeProduct< T, objectSettings_t > >;
} // namespace Forward

enum class RenderOrder : uint64_t
{
    BackgroundObject  = 0,
    SolidObject       = 1,
    TransparentObject = 2,
    END
};

class elRenderer_Forward : public elRenderer_base
{
  public:
    elRenderer_Forward( const internal::rendererShared_t shared,
                        OpenGL::elTexture_2D *const      deferredOutput,
                        OpenGL::elTexture_2D *const      deferredNormal,
                        OpenGL::elTexture_2D *const deferredPosition ) noexcept;
    virtual ~elRenderer_Forward() = default;

    void
    Render( const OpenGL::elCamera_Perspective &camera ) noexcept override;
    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept override;

    template < typename T, typename... Targs >
    Forward::Object< T >
    CreateObject( const OpenGL::elShaderProgram *geometricShader,
                  const RenderOrder order, Targs &&...args ) noexcept;

  private:
    static constexpr char     DIRECTIONAL_LIGHT_BLOCK_NAME[] = "light_t";
    static constexpr char     SCENE_BLOCK_NAME[]             = "scene_t";
    static constexpr uint64_t AUTO_SET_GEOMETRIC_SHADER =
        std::numeric_limits< uint64_t >::max();

    struct
    {
        attachmentId_t final;
        attachmentId_t depth;
        attachmentId_t glow;
        attachmentId_t normal;
        attachmentId_t position;
        attachmentId_t material;
        attachmentId_t event;
    } texture;

    struct settingsEntry_t
    {
        std::unique_ptr< OpenGL::elShaderProgram > shader;
        std::optional< uint64_t >                  directionalLightBufferIndex;
        std::optional< uint64_t >                  sceneBufferIndex;
    };

    struct settings_t
    {
        settingsEntry_t unifiedVertexObject;
        settingsEntry_t nonVertexObject;
        settingsEntry_t vertexObject;

        enum class light
        {
            COLOR             = 0,
            DIRECTION         = 1,
            AMBIENT_INTENSITY = 2,
            DIFFUSE_INTENSITY = 3,
            NUMBER_OF_LIGHTS  = 4
        };

        enum class scene
        {
            CAMERA_POSITION = 0
        };

        std::unique_ptr< OpenGL::elUniformBufferObject > directionalLightBuffer;
        std::unique_ptr< OpenGL::elUniformBufferObject > sceneBuffer;
    } settings;

  private:
    Forward::objectSettings_t
    GenerateObjectSettings( OpenGL::elGeometricObject_base &object,
                            const uint64_t                  geometricShaderId =
                                AUTO_SET_GEOMETRIC_SHADER ) const noexcept;

    void
    RenderGeometricObjects( const OpenGL::elCamera &camera ) const noexcept;
    void
    UpdateDirectionalLightBuffer() noexcept;
    void
    UpdateSceneBuffer( const OpenGL::elCamera_Perspective &camera ) noexcept;

    settingsEntry_t
    SetupSettingsEntry( const ObjectType objectType ) noexcept;
    std::unique_ptr< OpenGL::elUniformBufferObject >
    CreateDirectionalLightBuffer(
        const OpenGL::elShaderProgram &shader ) const noexcept;
    std::unique_ptr< OpenGL::elUniformBufferObject >
    CreateSceneBuffer( const OpenGL::elShaderProgram &shader ) const noexcept;

    template < typename T >
    uint64_t
    AcquireGeometricShaderId(
        Forward::Object< T >          &object,
        const OpenGL::elShaderProgram *geometricShader ) noexcept;

    // should be always last member / cleanup can access other members
    std::array<
        bb::elGenericFactory< bb::CompositeProduct<
            OpenGL::elGeometricObject_base, Forward::objectSettings_t > >,
        static_cast< uint64_t >( RenderOrder::END ) >
        factory;
};
} // namespace RenderPipeline
} // namespace el3D

#include "RenderPipeline/elRenderer_Forward.inl"
