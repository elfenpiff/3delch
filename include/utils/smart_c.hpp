#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/types.hpp"
#include "logging/elLog.hpp"

#include <cstring>
#include <iostream>
#include <utility>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on


#ifdef _WIN32
#define smart_c( cfunc, ... )                                                  \
    __smart_c( __FILE__, __LINE__, __FUNCTION__, #cfunc, cfunc, __VA_ARGS__ )
#else
#define smart_c( cfunc, ... )                                                  \
    __smart_c( __FILE__, __LINE__, __PRETTY_FUNCTION__, #cfunc, cfunc,         \
               __VA_ARGS__ )
#endif

namespace el3D
{
namespace utils
{
/**
 * @brief Calls a C function and performs and appropriate error handling whilst
 *          returning the file, line number, function name and errno when an
 *          error has occurred.
 *
 * @param f C function name
 * @param onError an initializer list of all possible error return values
 * @param args a list of arguments with which the function is called
 *
 * @return std::nullopt if on error otherwise the return value is returned
 *          encapsulated in std::optional<Return>
 * @code{.cpp}
    auto val = smart_c(strtol, {LONG_MIN, LONG_MAX}, {}, "-12312", nullptr, 10);
    if ( val.has_value() )
        DoStuffWith(val.value());
    }
 * @endcode
 */
template < typename Func, typename Return, typename... Targs >
bb::elExpected< Return, bb::errno_t >
__smart_c(
    const char* file, const int line, const char* func, const char* funcName,
    Func& f,
    const std::function< bool( std::invoke_result_t< Func&, Targs... > ) >&
        isError,
    Targs... args );
} // namespace utils
} // namespace el3D

#include "utils/smart_c.inl"
