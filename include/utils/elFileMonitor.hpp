#pragma once
#include "units/Time.hpp"

#include <set>

namespace el3D
{
namespace utils
{
class elFileMonitor
{
  public:
    class elFileDescriptorSet
    {
      public:
        elFileDescriptorSet();
        elFileDescriptorSet(
            const std::initializer_list< int >& fileDescriptorList );
        elFileDescriptorSet( const elFileDescriptorSet& ) = delete;
        elFileDescriptorSet( elFileDescriptorSet&& )      = default;

        elFileDescriptorSet&
        operator=( const elFileDescriptorSet& ) = delete;
        elFileDescriptorSet&
        operator=( elFileDescriptorSet&& ) = default;

        bool
        Add( const int fileDescriptor );
        bool
        Remove( const int fileDescriptor );
        bool
        Contains( const int fileDescriptor ) const;

        fd_set*
        GetHandle();

        int
        GetHighestFileDescriptor() const;
        size_t
        GetSize() const;

      private:
        fd_set fileDescriptorSet;
        int    highestFileDescriptor{-1};
        size_t size{0};
    };

    enum class FileType
    {
        Read,
        Write,
        Except
    };

    template < typename T >
    struct descriptorSet_t
    {
        T    read;
        T    write;
        T    except;
        bool doesContainBadFileDescriptor;
    };


    void
    AddFile( const FileType type, const int descriptor );
    void
    RemoveFile( const FileType type, const int descriptor );
    void
    Clear();
    descriptorSet_t< elFileDescriptorSet >
    Monitor( const units::Time& timeout );

  private:
    descriptorSet_t< std::set< int > > descriptorSet;
    int                                highestFileDescriptor{-1};

  private:
    void
    AdjustHighestFileDescriptor();

    descriptorSet_t< elFileDescriptorSet >
    CreateFileDescriptorSet() const;
};
} // namespace utils
} // namespace el3D
