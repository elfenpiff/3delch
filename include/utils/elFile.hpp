#pragma once

#include "logging/elLog.hpp"

#include <fstream>
#include <iterator>
#include <string>
#include <vector>

namespace el3D
{
namespace utils
{
class elFile
{
  public:
    elFile( const std::string &path, const bool isBinary = false ) noexcept
        : path{path}, isBinary{isBinary} {};
    elFile( const elFile & ) = default;
    elFile( elFile && )      = default;
    ~elFile()                = default;

    elFile &
    operator=( const elFile & ) = default;
    elFile &
    operator=( elFile && ) = default;

    bool
    Read();
    bool
    Write();
    std::string
    GetPath() const noexcept;
    std::string
    GetContentAsString() const noexcept;
    std::vector< std::string >
    GetContentAsVector() const noexcept;
    void
    SetContentFromString( const std::string & ) noexcept;
    void
    SetContentFromVector( const std::vector< std::string > & ) noexcept;

    std::vector< char > content;

  private:
    std::string path;
    bool        isBinary;
};
} // namespace utils
} // namespace el3D
