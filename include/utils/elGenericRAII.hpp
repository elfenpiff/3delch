#pragma once

#include <functional>

namespace el3D
{
namespace utils
{
class elGenericRAII
{
  public:
    elGenericRAII() = default;
    elGenericRAII( const std::function< void() >& dtor ) noexcept;
    elGenericRAII( const std::function< void() >& ctor,
                   const std::function< void() >& dtor ) noexcept;
    elGenericRAII( const elGenericRAII& ) = delete;
    elGenericRAII( elGenericRAII&& ) noexcept;

    elGenericRAII&
    operator=( const elGenericRAII& ) = delete;
    elGenericRAII&
    operator=( elGenericRAII&& ) noexcept;
    ~elGenericRAII();

    explicit operator bool() const noexcept;

    void
    Reset() noexcept;

    void
    SetDestructor( const std::function< void() >& dtor ) noexcept;

  private:
    void
    Destroy() noexcept;

  private:
    std::function< void() > dtor;
};
} // namespace utils
} // namespace el3D
