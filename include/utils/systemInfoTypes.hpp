#pragma once

#include "utils/Serializable.hpp"
#include "utils/byteStream_t.hpp"

#include <string>
#include <vector>

namespace el3D
{
namespace utils
{
namespace internal
{
struct cpuCore_t : public utils::Serializable
{
    float frequencyInMhz{ 0.0f };
    float load{ 0.0f };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct cpu_t : public utils::Serializable
{
    std::string              model;
    uint32_t                 numberOfCores{ 0u };
    float                    load{ 0.0f };
    std::vector< cpuCore_t > cores;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct memory_t : public utils::Serializable
{
    uint64_t total{ 0u };
    uint64_t used{ 0u };
    uint64_t cached{ 0u };
    uint64_t buffer{ 0u };
    uint64_t swapTotal{ 0u };
    uint64_t swapUsed{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct netStat_t : public utils::Serializable
{
    uint64_t bytes{ 0u };
    uint64_t packets{ 0u };
    float    byteLoad{ 0.0f };
    float    packetLoad{ 0.0f };
    uint64_t errors{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct networkInterface_t : public utils::Serializable
{
    netStat_t   incoming;
    netStat_t   outgoing;
    std::string interfaceName;
    std::string macAddress;
    std::string ipAddress;
    std::string broadCastAddress;
    std::string networkMask;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct connectionPart_t : public utils::Serializable
{
    std::string ipAddress;
    uint16_t    port{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct connection_t : public utils::Serializable
{
    connectionPart_t source;
    connectionPart_t destination;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct network_t : public utils::Serializable
{
    std::vector< std::string >        dns;
    std::vector< networkInterface_t > interfaces;
    std::vector< connection_t >       connections;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct thread_t : public utils::Serializable
{
    uint32_t tid{ 0u };
    float    cpuLoad{ 0.0f };
    uint64_t memoryUsage{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct process_t : public utils::Serializable
{
    uint32_t                pid{ 0u };
    uint32_t                ppid{ 0u };
    uint32_t                numberOfThreads{ 0u };
    std::vector< thread_t > threads;
    std::string             user;
    std::string             group;
    std::string             command;
    float                   cpuLoad{ 0.0f };
    uint64_t                memoryUsage{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
struct processSummary_t : public utils::Serializable
{
    std::vector< process_t > processList;
    uint32_t                 numberOfProcesses{ 0u };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
} // namespace internal

struct systemInfo_t : public utils::Serializable
{
    internal::cpu_t            cpu;
    internal::memory_t         memory;
    internal::network_t        network;
    internal::processSummary_t process;
    float                      uptime{ 0.0f };
    std::string                hostname;
    std::string                kernelVersion;
    internal::process_t        thisProcess;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};
} // namespace utils
} // namespace el3D
