#pragma once

#include "DesignPattern/Creation.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "utils/byteStream_t.hpp"

#if !defined( _WIN32 ) and !defined( __APPLE__ )
#include <linux/videodev2.h>
#endif

#include <string>


namespace el3D
{
namespace utils
{
enum class elWebcamError
{
    AccessDenied,
    UnableToSetVideoFormat,
    UnableToGetCapability,
    UnableToSetupBufferCount,
    UnableToGetBufferInfo,
    UnableToMapWebcamMemory,
    UnableToActivateStreaming,
    UnableToDeactivateStreaming,
    UnableToGetNextFrame,
    Undefined
};

class elWebcam : public DesignPattern::Creation< elWebcam, elWebcamError >
{
  public:
    ~elWebcam();

    elWebcam( const elWebcam& ) = delete;
    elWebcam( elWebcam&& ) noexcept;
    elWebcam&
    operator=( const elWebcam& ) = delete;
    elWebcam&
    operator=( elWebcam&& ) noexcept;

#if !defined( _WIN32 ) and !defined( __APPLE__ )
    bb::elExpected< v4l2_capability, elWebcamError >
    GetCapability() const noexcept;
    bb::elExpected< elWebcamError >
    SetVideoFormat(
        const uint32_t width, const uint32_t height,
        const uint32_t pixelFormat = V4L2_PIX_FMT_MJPEG,
        const uint32_t bufferType  = V4L2_BUF_TYPE_VIDEO_CAPTURE ) noexcept;
#endif

    bb::elExpected< elWebcamError >
    StartStreaming() noexcept;
    bb::elExpected< elWebcamError >
    StopStreaming() noexcept;

    bb::elExpected< byteStream_t, elWebcamError >
    GetNextFrame() noexcept;

    /// @brief either byteStream has enough space left to store the frame or it
    /// is empty
    bb::elExpected< uint64_t, elWebcamError >
    GetNextFrame( byteStream_t&  byteStream,
                  const uint64_t byteStreamOffset ) noexcept;
    bb::elExpected< uint64_t, elWebcamError >
    GetNextFrame( void* const memory ) noexcept;


    friend DesignPattern::Creation< elWebcam, elWebcamError >;

  private:
    elWebcam( const uint32_t width, const uint32_t height,
              const std::string& deviceFile = "/dev/video0" ) noexcept;

    bb::elExpected< elWebcamError >
    SetupBufferCount( const uint32_t count ) const noexcept;

    bb::elExpected< elWebcamError >
    RefreshBufferInfo() noexcept;

    bb::elExpected< elWebcamError >
    MapWebcamMemory() noexcept;
    bb::elExpected< elWebcamError >
    UpdateBufferSize() noexcept;

    void
    Close() noexcept;
    void
    UnmapMemory() noexcept;

  private:
#if !defined( _WIN32 ) and !defined( __APPLE__ )
    std::string deviceFile;
    int         webcamHandle;
    v4l2_format videoFormat;
    v4l2_buffer bufferInfo;
    bool        isStreaming{false};
    uint32_t    memory = V4L2_MEMORY_MMAP;
    void*       mmapStartAddress{nullptr};
    size_t      mmapLength;
#endif
};
} // namespace utils
} // namespace el3D
