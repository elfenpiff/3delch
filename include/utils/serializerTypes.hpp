#pragma once

#include <cstdint>

namespace el3D
{
namespace utils
{
using variantIndexType_t = uint32_t;


enum class Endian
{
    Little,
    Big
};
} // namespace utils
} // namespace el3D
