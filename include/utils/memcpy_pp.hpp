#pragma once
#include <cstring>
#include <type_traits>

namespace el3D
{
namespace utils
{
template < typename >
class MemCpyTo;

/*
    @code{.cpp}
        // if source and destination are non void pointer
        utils::MemCpy::From(source).To(destination);

        // if source and destination are void pointer
        utils::MemCpy::From(source).To(destination).Size(123);
    @endcode
 */
class MemCpy
{
  public:
    template < typename T >
    static MemCpyTo< T >
    From( const T* const source ) noexcept;
};

template < typename T >
class MemCpySize
{
  public:
    T*
    Size( size_t size ) noexcept;

    template < typename >
    friend class MemCpyTo;

  private:
    MemCpySize( const T* const source, T* const destination ) noexcept;
    const T* const source;
    T* const       destination;
};

template < typename T >
class MemCpyTo
{
  public:
    template < typename U >
    typename std::enable_if<
        std::is_same_v< T, U > &&
            !std::is_same_v< std::remove_const_t< T >, void >,
        T* >::type
    To( U* const destination ) noexcept;

    template < typename U >
    typename std::enable_if<
        std::is_same_v< T, U > &&
            std::is_same_v< std::remove_const_t< T >, void >,
        MemCpySize< T > >::type
    To( U* const destination ) noexcept;

    friend class MemCpy;

  private:
    MemCpyTo( const T* source ) noexcept;
    const T* const source;
};
} // namespace utils
} // namespace el3D

#include "utils/memcpy_pp.inl"
