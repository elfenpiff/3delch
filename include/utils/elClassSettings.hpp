#pragma once

#include <array>
#include <glm/glm.hpp>
#include <string>
#include <variant>
#include <vector>

namespace el3D
{
namespace utils
{
class elClassSettings
{
  private:
    using baseType_t =
        std::variant< std::vector< float >, std::vector< std::string >,
                      std::vector< int > >;
    struct settingsEntry_t
    {
        std::string name;
        baseType_t  value;
    };
    using settings_t = std::vector< settingsEntry_t >;

  public:
    enum class dataType_t : uint64_t
    {
        FLOAT = 0,
        STRING,
        INT,
    };

    struct entry_t
    {
        entry_t( const std::string &name, const size_t size,
                 const dataType_t type );

        template < typename T >
        entry_t( const std::string &name, const size_t size,
                 const dataType_t type, const std::vector< T > &defaultValue );

        std::string name;
        size_t      size;
        dataType_t  type;
        baseType_t  defaultValue;
    };

    elClassSettings( const std::vector< entry_t > &properties );

    template < typename T >
    std::vector< T > &
    Get( const size_t entry ) noexcept;

    template < typename T >
    const std::vector< T > &
    Get( const size_t entry ) const noexcept;

    template < typename T >
    void
    Set( const size_t entry, const T &value, const size_t offset = 0 ) noexcept;

  private:
    settings_t data;
};
} // namespace utils
} // namespace el3D
#include "utils/elClassSettings.inl"
