#pragma once

#include "utils/elAllocator.hpp"

#include <cstdint>
#include <string>
#include <vector>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace utils
{
enum class ByteStreamError
{
    TooShort,
    InvalidSequence,
    LengthInformationMismatch,
    InvalidContent
};

using byte_t = uint8_t;
struct byteStream_t
{
    using stream_t = std::vector< byte_t, elAllocator< byte_t > >;
    byteStream_t() = default;
    explicit byteStream_t( stream_t&& value );
    explicit byteStream_t( const stream_t& value );
    explicit byteStream_t( const size_t size,
                           void* const  preallocatedMemory = nullptr );
    explicit byteStream_t( const std::initializer_list< byte_t >& values );

    std::string
    GetHumanReadableString( const size_t offset = 0 ) const noexcept;

    static void
    ByteStreamErrorToStderr( const std::string&     source,
                             const ByteStreamError& error,
                             const byteStream_t&    stream,
                             const uint64_t         offset ) noexcept;

    stream_t stream;
};
} // namespace utils
} // namespace el3D
