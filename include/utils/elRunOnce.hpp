#pragma once
#include <functional>
#include <vector>

namespace el3D
{
namespace utils
{
class elRunOnce
{
  public:
    elRunOnce( const std::vector< std::function< void() > > &func ) noexcept;
    elRunOnce( const elRunOnce & ) = default;
    elRunOnce( elRunOnce && )      = default;

    elRunOnce &
    operator=( const elRunOnce & ) = default;
    elRunOnce &
    operator=( elRunOnce && ) = default;

    void
    Run() noexcept;

    void
    Call( const size_t i ) noexcept;
    void
    Reset( const size_t i ) noexcept;

  private:
    std::vector< std::function< void() > > func;
    std::vector< bool >                    doCallFunc;
    size_t                                 funcSize;
};
} // namespace utils
} // namespace el3D
