#pragma once

#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "utils/elGenericRAII.hpp"

#include <atomic>
#include <functional>
#include <mutex>


namespace el3D
{
namespace utils
{
class CallbackGuard : public elGenericRAII
{
  public:
    using elGenericRAII::elGenericRAII;

  private:
    using elGenericRAII::SetDestructor;
};

class elCallbackVector
{
  private:
    using callbackVector_t = bb::elUniqueIndexVector< std::function< void() > >;

  public:
    static constexpr uint64_t INVALID_INDEX = callbackVector_t::INVALID_INDEX;

    CallbackGuard
    CreateCallback( const std::function< void() >& callback ) noexcept;
    void
    Execute() noexcept;

  private:
    void
    RemoveCallback( const uint64_t id ) noexcept;

  private:
    std::atomic_bool         updateCallbackCopy{ false };
    callbackVector_t::base_t callbackCopy;
    callbackVector_t         callbacks;
    std::recursive_mutex     callbacksMutex;
};
} // namespace utils
} // namespace el3D
