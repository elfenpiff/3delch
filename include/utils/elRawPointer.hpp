#pragma once

namespace el3D
{
namespace utils
{
template < typename T >
class elRawPointer
{
  public:
    constexpr elRawPointer() noexcept;
    constexpr elRawPointer( const T* t ) noexcept;
    constexpr elRawPointer( T* t ) noexcept;

    constexpr          operator T*() noexcept;
    constexpr          operator T*() const noexcept;
    constexpr T&       operator->() noexcept;
    constexpr const T& operator->() const noexcept;

    constexpr T*
    Get() noexcept;
    constexpr const T*
    Get() const noexcept;

    constexpr T*
    __Get__() noexcept;

  private:
    T* t;
};
} // namespace utils
} // namespace el3D

#include "utils/elRawPointer.inl"
