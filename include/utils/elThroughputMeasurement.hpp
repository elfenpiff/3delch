#pragma once

#include "units/Time.hpp"

#include <chrono>

namespace el3D
{
namespace utils
{
template < typename T >
class elThroughputMeasurement
{
  public:
    elThroughputMeasurement( const units::Time& interval,
                             const T&           value ) noexcept;
    void
    IncrementWith( const T& value ) noexcept;
    T
    Get() const noexcept;

    elThroughputMeasurement&
    operator+=( const T& rhs ) noexcept;

    elThroughputMeasurement&
    operator++() noexcept;

    /// @brief return T per second
    long double
    GetThroughputPerSecond() const noexcept;

  private:
    units::Time interval;
    T           value{ 0 };

    mutable std::chrono::time_point< std::chrono::system_clock > start{
        std::chrono::system_clock::now() };

    mutable T           startValue{ 0 };
    mutable long double throughPut{ 0.0 };
};
} // namespace utils
} // namespace el3D

#include "utils/elThroughputMeasurement.inl"
