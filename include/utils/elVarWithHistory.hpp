#pragma once

namespace el3D
{
namespace utils
{
template < typename T >
class elVarWithHistory
{
  public:
    elVarWithHistory() = default;
    elVarWithHistory( const T& initialValue );
    elVarWithHistory( const elVarWithHistory& ) = default;
    elVarWithHistory( elVarWithHistory&& )      = default;
    elVarWithHistory&
    operator=( const elVarWithHistory& ) = default;
    elVarWithHistory&
    operator=( elVarWithHistory&& ) = default;
    ~elVarWithHistory()             = default;

    elVarWithHistory&
    operator=( const T& rhs );
    elVarWithHistory&
    operator=( T&& rhs );

    operator T() const;
    operator T();

    const T&
    Get() const noexcept;

    T
    PreviousValue() const;

    template < typename... Targs >
    void
    Assign( Targs&&... );

    void
    ResetHistory() noexcept;


  private:
    T value;
    T previousValue;
};
} // namespace utils
} // namespace el3D

#include "utils/elVarWithHistory.inl"

