#pragma once

#include <algorithm>
#include <glm/glm.hpp>
#include <type_traits>

template < typename T >
struct isGLMType_t
{
    static constexpr bool value = std::is_same_v< T, glm::vec2 > ||
                                  std::is_same_v< T, glm::vec3 > ||
                                  std::is_same_v< T, glm::vec4 >;
};

template < typename T >
using enableIfGLMType = typename std::enable_if_t< isGLMType_t< T >::value, T >;
template < typename T >
using enableIfNotGLMType =
    typename std::enable_if_t< !isGLMType_t< T >::value, T >;

namespace el3D
{
namespace utils
{
template < typename T >
enableIfNotGLMType< T >
Distance( const T& p1, const T& p2 ) noexcept
{
    return std::abs( p2 - p1 );
}

template < typename T >
typename enableIfGLMType< T >::value_type
Distance( const T& p1, const T& p2 ) noexcept
{
    return glm::distance( p1, p2 );
}


} // namespace utils
} // namespace el3D
