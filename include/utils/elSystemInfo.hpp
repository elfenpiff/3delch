#pragma once

#include "units/Time.hpp"
#include "utils/systemInfoTypes.hpp"

#include <atomic>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace utils
{
class elSystemInfo
{
  public:
    elSystemInfo( const units::Time intervalTime =
                      units::Time::MilliSeconds( 1000 ) ) noexcept;
    ~elSystemInfo();

    systemInfo_t
    GetSystemInfo() const noexcept;
    units::Time
    GetUpdateIntervalTime() const noexcept;
    uint64_t
    GetCurrentIteration() const noexcept;

  private:
    void
    UpdateLoop() noexcept;
    void
    Update() noexcept;
    void
    UpdateOnce() noexcept;
    void
    UpdateCPU() noexcept;
    void
    UpdateCPULoad() noexcept;
    void
    UpdateMemory() noexcept;
    void
    UpdateUptime() noexcept;
    void
    UpdateHostname() noexcept;
    void
    UpdateKernelVersion() noexcept;
    void
    UpdateThisProcessID() noexcept;
    void
    UpdateNetwork() noexcept;
    void
    UpdateNetworkConnections() noexcept;
    void
    UpdateProcesses() noexcept;
    std::vector< internal::process_t >
    PutThreadsIntoProcess(
        const std::map< uint32_t, internal::process_t > &raw ) const noexcept;

    void
    ExtractStatProcessInformation(
        internal::process_t &process ) const noexcept;
    void
    GetIPAddressOfInterface(
        internal::networkInterface_t &interface ) const noexcept;

    std::vector< std::string >
    ReadFile( const std::string &fileName,
              const std::string &errorMessage ) const noexcept;

  private:
    mutable std::mutex                       systemInfoMutex;
    systemInfo_t                             systemInfo;
    systemInfo_t                             updateThreadSystemInfo;
    std::atomic_bool                         keepRunning{ true };
    std::atomic< uint64_t >                  currentIteration{ 0u };
    units::Time                              intervalTime;
    std::vector< std::array< uint64_t, 4 > > cpuDeltaTimes;
    bool                                     firstCycle{ true };

    std::thread updateLoopThread;
};
} // namespace utils
} // namespace el3D
