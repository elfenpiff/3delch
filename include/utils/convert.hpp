#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace el3D
{
namespace utils
{
template < typename To_T, typename From_T >
To_T
ToGLM( const From_T& c, const size_t offset = 0 ) noexcept;

template < typename GLMType >
std::vector< float >
SetVectorFrom( const uint64_t repetition, const GLMType& v ) noexcept;

template < typename T >
std::string
ToString( const T value, const uint64_t precision ) noexcept;

} // namespace utils
} // namespace el3D

#include "utils/convert.inl"
