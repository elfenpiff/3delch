#pragma once

namespace el3D
{
constexpr char CONSOLE_FG_COLOR_DEFAULT[]       = "\x1b[39m";
constexpr char CONSOLE_FG_COLOR_BLACK[]         = "\x1b[30m";
constexpr char CONSOLE_FG_COLOR_RED[]           = "\x1b[31m";
constexpr char CONSOLE_FG_COLOR_GREEN[]         = "\x1b[32m";
constexpr char CONSOLE_FG_COLOR_YELLOW[]        = "\x1b[33m";
constexpr char CONSOLE_FG_COLOR_BLUE[]          = "\x1b[34m";
constexpr char CONSOLE_FG_COLOR_MAGENTA[]       = "\x1b[35m";
constexpr char CONSOLE_FG_COLOR_CYAN[]          = "\x1b[36m";
constexpr char CONSOLE_FG_COLOR_LIGHT_GRAY[]    = "\x1b[37m";
constexpr char CONSOLE_FG_COLOR_DARK_GRAY[]     = "\x1b[90m";
constexpr char CONSOLE_FG_COLOR_LIGHT_RED[]     = "\x1b[91m";
constexpr char CONSOLE_FG_COLOR_LIGHT_GREEN[]   = "\x1b[92m";
constexpr char CONSOLE_FG_COLOR_LIGHT_YELLOW[]  = "\x1b[93m";
constexpr char CONSOLE_FG_COLOR_LIGHT_BLUE[]    = "\x1b[94m";
constexpr char CONSOLE_FG_COLOR_LIGHT_MAGENTA[] = "\x1b[95m";
constexpr char CONSOLE_FG_COLOR_LIGHT_CYAN[]    = "\x1b[96m";
constexpr char CONSOLE_FG_COLOR_LIGHT_WHITE[]   = "\x1b[97m";
constexpr char CONSOLE_BG_COLOR_DEFAULT[]       = "\x1b[49m";
constexpr char CONSOLE_BG_COLOR_BLACK[]         = "\x1b[40m";
constexpr char CONSOLE_BG_COLOR_RED[]           = "\x1b[41m";
constexpr char CONSOLE_BG_COLOR_GREEN[]         = "\x1b[42m";
constexpr char CONSOLE_BG_COLOR_YELLOW[]        = "\x1b[43m";
constexpr char CONSOLE_BG_COLOR_BLUE[]          = "\x1b[44m";
constexpr char CONSOLE_BG_COLOR_MAGENTA[]       = "\x1b[45m";
constexpr char CONSOLE_BG_COLOR_CYAN[]          = "\x1b[46m";
constexpr char CONSOLE_BG_COLOR_LIGHT_GRAY[]    = "\x1b[47m";
constexpr char CONSOLE_BG_COLOR_DARK_GRAY[]     = "\x1b[100m";
constexpr char CONSOLE_BG_COLOR_LIGHT_RED[]     = "\x1b[101m";
constexpr char CONSOLE_BG_COLOR_LIGHT_GREEN[]   = "\x1b[102m";
constexpr char CONSOLE_BG_COLOR_LIGHT_YELLOW[]  = "\x1b[103m";
constexpr char CONSOLE_BG_COLOR_LIGHT_BLUE[]    = "\x1b[104m";
constexpr char CONSOLE_BG_COLOR_LIGHT_MAGENTA[] = "\x1b[105m";
constexpr char CONSOLE_BG_COLOR_LIGHT_CYAN[]    = "\x1b[106m";
constexpr char CONSOLE_BG_COLOR_LIGHT_WHITE[]   = "\x1b[107m";
constexpr char CONSOLE_FG_256COLOR[]            = "\x1b[38;5;";
} // namespace el3D
