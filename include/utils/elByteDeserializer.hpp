#pragma once

#include "DesignPattern/Creation.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/type_traits_extended.hpp"
#include "units/Time.hpp"
#include "utils/elByteSerializer.hpp"
#include "utils/memcpy_pp.hpp"
#include "utils/serializerTypes.hpp"

#include <optional>
#include <string>
#include <type_traits>
#include <vector>

namespace el3D
{
namespace utils
{
class elByteDeserializer
{
  public:
    template < typename T >
    using result_t = bb::elExpected< T, utils::ByteStreamError >;

    static constexpr size_t MAXIMUM_SUPPORTED_VARIANT_ARGUMENTS = 5;

    explicit elByteDeserializer( const utils::byteStream_t& byteStream,
                                 const size_t byteStreamOffset = 0 );

    template < typename T, typename... Targs >
    bb::elExpected< utils::ByteStreamError >
    Extract( const Endian endianness, T& t, Targs&... args ) noexcept;

    template < typename T >
    std::enable_if_t< std::is_base_of_v< units::BasicUnit< T >, T >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    template < typename T >
    std::enable_if_t< std::is_floating_point_v< T >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    // uint_t
    template < typename T >
    std::enable_if_t< std::is_integral_v< T > && std::is_unsigned_v< T >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    // int_t
    template < typename T >
    std::enable_if_t< std::is_integral_v< T > && !std::is_unsigned_v< T >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    // std::vector<CONVERTABLE_TYPE> // CONVERTABLE_TYPE == type which has an
    //                                      existing Get method here
    template < typename T >
    std::enable_if_t< bb::isVector< T >::value,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    template < typename T >
    std::enable_if_t< bb::isArray< T >::value,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    template < typename T >
    std::enable_if_t< bb::isVariant< T >::value,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    // std::string
    template < typename T >
    std::enable_if_t< std::is_same_v< T, std::string >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    template < typename T >
    std::enable_if_t< std::is_same_v< utils::byteStream_t, T >,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;

    template < typename T >
    std::enable_if_t< internal::IsNotDirectlyConvertable< T >::value,
                      bb::elExpected< T, utils::ByteStreamError > >
    Get( const Endian endianness, const size_t offset ) const noexcept;


  private:
    template < typename T, int N >
    using typeAt = std::remove_reference_t< decltype(
        std::get< N >( std::declval< T >() ) ) >;

    template < uint32_t N, typename Variant_T >
    void
    SetVariantValue( const Endian endianness, Variant_T& v, const size_t offset,
                     bool& setWasSuccessful ) const noexcept;

    template < typename T >
    bb::elExpected< utils::ByteStreamError >
    ExtractWithOffset( const Endian endianness, const size_t offset,
                       T& t ) noexcept;

    template < typename T, typename... Targs >
    bb::elExpected< utils::ByteStreamError >
    ExtractWithOffset( const Endian endianness, const size_t offset, T& t,
                       Targs&... args ) noexcept;

    const utils::byteStream_t& byteStream;
    size_t                     byteStreamSize;
    size_t                     byteStreamOffset{ 0 };
};
} // namespace utils
} // namespace el3D

#include "utils/elByteDeserializer.inl"
