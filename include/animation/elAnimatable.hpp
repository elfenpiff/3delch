#pragma once

#include "animation/Types.hpp"
#include "animation/elAnimate_base.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "utils/elCallbackVector.hpp"

#include <array>
#include <functional>
#include <memory>
#include <optional>
#include <vector>

namespace el3D
{
namespace animation
{
class elAnimatableBase
{
  public:
    using id_t      = std::array< uint8_t, 16 >;
    using setCall_t = std::function< void( void* const, const void* const ) >;
    using animationFactory_t = std::function< elAnimate_base*(
        const elAnimate_base* const, setCall_t* const,
        void* const thisPointer ) >;

    elAnimatableBase() noexcept = default;
    elAnimatableBase( const elAnimatableBase& ) noexcept;
    elAnimatableBase( elAnimatableBase&& ) noexcept;
    virtual ~elAnimatableBase();

    elAnimatableBase&
    operator=( const elAnimatableBase& ) noexcept;
    elAnimatableBase&
    operator=( elAnimatableBase&& ) noexcept;

    static void
    SetGlobalCallbackVectorForUpdateAnimation(
        utils::elCallbackVector& value ) noexcept;

  protected:
    void
    UpdateAnimations() noexcept;

    bool
    HasActiveAnimations() const noexcept;

    void
    SetUpdateCallback( const std::function< void() >& value ) noexcept;

    void
    AddAnimation( elAnimate_base* const animationPtr, setCall_t* const setCall,
                  const animationFactory_t& animationFactory,
                  const id_t                destinationId ) noexcept;
    void
    RemoveAnimation( elAnimate_base* const animationPtr ) noexcept;
    void
    RemoveAllAnimations() noexcept;
    void
    DeregisterUpdateAnimationCallback() noexcept;
    void
    RegisterUpdateAnimationCallback() noexcept;

    struct animate_t
    {
        std::unique_ptr< elAnimate_base > animation;
        std::unique_ptr< setCall_t >      setCall;
        animationFactory_t                animationFactory;
        id_t                              destinationId{ 0u };
    };

    std::vector< animate_t >        animations;
    std::function< void() >         updateCallback;
    static utils::elCallbackVector* globalCallbackVector;
    utils::CallbackGuard            callback;
};

/// @brief Must be always the first from which you inherit if you inherit from
///         multiple classes. If this is not possible Animate() with the child
///         override pointer has to be used.
template < typename Child >
class elAnimatable : public elAnimatableBase
{
  public:
    virtual ~elAnimatable() = default;

    template <
        template < typename, template < typename > class > class AnimationType,
        template < typename > class InterpolationPolicy, typename SetCallback >
    AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >&
    Animate( const SetCallback setter ) noexcept;

    template < template < typename, template < typename > class >
               class AnimationType,
               template < typename > class InterpolationPolicy,
               typename SetCallback, typename ChildOverride >
    AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >&
    Animate( const SetCallback    setter,
             ChildOverride* const childPointer ) noexcept;

    template <
        template < typename, template < typename > class > class AnimationType,
        template < typename > class InterpolationPolicy, typename ValueType >
    AnimationType< ValueType, InterpolationPolicy >&
    AnimateFreeFunction(
        const std::function< void( const ValueType& ) >& setter ) noexcept;

    template < typename SetCallback >
    void
    RemoveAnimation( const SetCallback setter ) noexcept;

    template <
        template < typename, template < typename > class > class AnimationType,
        template < typename > class InterpolationPolicy, typename SetCallback >
    const AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >*
    GetAnimation( const SetCallback setter ) const noexcept;

  private:
    template < typename SetCallback >
    id_t
    CallbackToId( const SetCallback setter ) const noexcept;
};
} // namespace animation
} // namespace el3D

#include "animation/elAnimatable.inl"
