#pragma once

#include "animation/Types.hpp"
#include "units/Time.hpp"

#include <functional>
#include <optional>

namespace el3D
{
namespace animation
{
class elAnimate_base
{
  public:
    elAnimate_base() noexcept;
    virtual ~elAnimate_base() = default;

    elAnimate_base&
    SetOnUpdateCallback( const onUpdateCallback_t& ) noexcept;
    elAnimate_base&
    SetOnArrivalCallback( const onArrivalCallback_t& ) noexcept;
    elAnimate_base&
    SetCustomGetTimeCallback( const getTimeCallback_t& ) noexcept;

    bool
    HasToPerformUpdate() const noexcept;
    void
    Update() noexcept;

    template < typename >
    friend class elAnimatable;

  protected:
    virtual void
    PerformUpdate() noexcept = 0;
    virtual void
    Set() noexcept = 0;
    virtual void
    Reset() noexcept = 0;


    static getTimeCallback_t globalGetTimeCallback;

  protected:
    onUpdateCallback_t     onUpdateCallback;
    onArrivalCallback_t    onArrivalCallback;
    animationSetCallback_t setCallback;

    units::Time                  currentTime = units::Time::Seconds( 0.0 );
    std::optional< units::Time > previousUpdateTime;
    bool                         hasToPerformUpdate = false;

  private:
    getTimeCallback_t getTimeCallback;
};
} // namespace animation
} // namespace el3D
