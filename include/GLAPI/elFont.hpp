#pragma once
#include "GLAPI/common/helperTypes.hpp"
#include "GLAPI/elImage.hpp"
#include "utils/byteStream_t.hpp"

namespace el3D
{
namespace GLAPI
{
class elFontCache;
class elFont
{
  public:
    elFont( elFont && );
    elFont( const elFont & ) = delete;
    ~elFont() noexcept;

    elFont &
    operator=( elFont && );
    elFont &
    operator=( const elFont & ) = delete;

    void
    SetText( const std::string &text, const SDL_Color &color );
    utils::byte_t *
    GetBytePointer() const noexcept;
    dimension_t
    GetTextureSize() const noexcept;
    void
    Resize( const uint64_t x, const uint64_t y ) noexcept;
    void
    CanvasResize( const uint64_t x, const uint64_t y ) noexcept;
    dimension_t
    GetTextSize( const std::string &text ) const noexcept;
    uint64_t
    GetFontHeight() const noexcept;


    friend class text_t;
    friend class elFontCache;

  private:
    elFont( const std::string &fontFile, const uint64_t fontSize,
            const bool fastRenderer );

    std::unique_ptr< elImage > surface;
    TTF_Font *                 font = nullptr;
    bool                       fastRenderer;
};
} // namespace GLAPI
} // namespace el3D
