#pragma once

#include "GLAPI/elFont.hpp"
#include "buildingBlocks/elExpected.hpp"

#include <functional>
#include <map>
#include <string>

namespace el3D
{
namespace GLAPI
{
class elFontCache
{
  public:
    using property_t = std::tuple< std::string, uint64_t, bool >;

    elFontCache();
    elFontCache( const elFontCache& ) = delete;
    elFontCache( elFontCache&& );
    ~elFontCache();

    elFontCache&
    operator=( const elFontCache& ) = delete;

    elFontCache&
    operator=( elFontCache&& );

    bb::elExpected< elFont*, bool >
    Get( const std::string& fontFile, const uint64_t fontSize,
         const bool fastRenderer ) noexcept;

  private:
    std::map< property_t, elFont* > cache;


  private:
    void
    ClearCache() noexcept;
};
} // namespace GLAPI
} // namespace el3D
