#pragma once

#include "DesignPattern/Creation.hpp"
#include "GLAPI/common/helperTypes.hpp"
#include "GLAPI/elWindow.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elRawPointer.hpp"

namespace el3D
{
namespace GLAPI
{
struct FromFileInMemory_t
{
};
struct FromFileInByteStream_t
{
};
struct FromFilePath_t
{
};
struct FromSurface_t
{
};
struct FromArray_t
{
};
constexpr FromFileInMemory_t     FromFileInMemory{};
constexpr FromFileInByteStream_t FromFileInByteStream{};
constexpr FromFilePath_t         FromFilePath{};
constexpr FromSurface_t          FromSurface{};
constexpr FromArray_t            FromArray{};

class elImage : public DesignPattern::Creation< elImage, std::string >
{
  public:
    elImage();
    elImage( elImage && )      = default;
    elImage( const elImage & ) = delete;
    ~elImage()                 = default;

    elImage &
    operator=( elImage && ) = default;
    elImage &
    operator=( const elImage & ) = delete;

    void
    Resize( const uint64_t x, const uint64_t y ) noexcept;
    void
    CanvasResize( const uint64_t x, const uint64_t y ) noexcept;

    utils::elRawPointer< utils::byte_t >
    GetBytePointer() const noexcept;
    utils::byteStream_t
    GetByteStream() const noexcept;
    dimension_t
    GetDimensions() const noexcept;
    Uint8
    GetBytesPerPixel() const noexcept;
    Uint32
    GetFormat() const noexcept;

    friend class elFont;
    friend DesignPattern::Creation< elImage, std::string >;

  private:
    using surface_t = std::shared_ptr< SDL_Surface >;

    elImage( FromFileInMemory_t, void *const rawMemory,
             const uint64_t rawMemorySize,
             const uint32_t format = SDL_PIXELFORMAT_RGBA32 );
    elImage( FromFileInByteStream_t, const utils::byteStream_t &byteStream,
             const uint32_t format = SDL_PIXELFORMAT_RGBA32 );
    elImage( FromFilePath_t, const std::string &path,
             const uint32_t format = SDL_PIXELFORMAT_RGBA32 );
    elImage( FromSurface_t, SDL_Surface * );
    elImage( FromArray_t, const uint64_t width, const uint64_t height,
             const utils::byte_t *const pixels ) noexcept;

    static surface_t
    CreateRGBSurface( const surface_t origin ) noexcept;
    static surface_t
    CanvasResizeSurface( const surface_t origin, const uint64_t x,
                         const uint64_t y ) noexcept;
    static surface_t
    ResizeSurface( const surface_t origin, const uint64_t x,
                   const uint64_t y ) noexcept;
    static surface_t
    ConvertRawSurface( const surface_t origin, const uint32_t format ) noexcept;
    static surface_t
    CreateSurfaceFromByteStream(
        const utils::byteStream_t &byteStream ) noexcept;
    static surface_t
    CreateSurfaceFromRawMemory( void *const    rawMemory,
                                const uint64_t size ) noexcept;
    static surface_t
    CreateSurfaceFromFile( const std::string &filePath ) noexcept;
    static surface_t
    CreateSurfacePointer( SDL_Surface *raw ) noexcept;
    static surface_t
    CreateSurfaceFromByteArray( const uint64_t width, const uint64_t height,
                                const utils::byte_t *pixels ) noexcept;

    static std::string
    GetIMGError() noexcept;

  private:
    surface_t surface{ nullptr };
};
} // namespace GLAPI
} // namespace el3D
