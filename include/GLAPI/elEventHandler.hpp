#pragma once

#include "GLAPI/elEvent.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

#include <functional>
#include <set>
#include <vector>

namespace el3D
{
namespace GLAPI
{
class elEvent;
class elWindow;
class elEventHandler
{
  public:
    elEventHandler()  = default;
    ~elEventHandler() = default;

    // must be deleted since elEvent holds a pointer to this object for later
    // cleanup
    elEventHandler( const elEventHandler& ) = delete;
    elEventHandler( elEventHandler&& )      = delete;

    elEventHandler&
    operator=( const elEventHandler& ) = delete;
    elEventHandler&
    operator=( elEventHandler&& ) = delete;

    bb::product_ptr< elEvent >
    CreateEventForColorIndex( const uint32_t colorIndex ) noexcept;

    bb::product_ptr< elEvent >
    CreateEvent( const std::function< void( elEvent* ) >& cleanupCallback =
                     std::function< void( elEvent* ) >() ) noexcept;

    bool
    IsActive() const noexcept;
    void
    Activate() noexcept;
    void
    Deactivate() noexcept;
    uint64_t
    GetCurrentEventLoopCycle() const noexcept;

    friend class elEvent;
    friend class elWindow;

  private:
    void
    PollCurrentEvents( const uint32_t currentEventColorIndex ) noexcept;
    void
    HandleEvents( const SDL_Event sdlEvent,
                  const uint32_t  currentEventColorIndex ) noexcept;
    void
    RegisterEvent( const elEvent* const event ) noexcept;
    void
    UnregisterEvent( const elEvent* const event ) noexcept;

  private:
    struct event_t
    {
        bool
        operator==( const elEvent* const event ) const noexcept;
        bool
        operator==( const event_t& rhs ) const noexcept;
        bool
        operator<( const event_t& rhs ) const noexcept;

        const elEvent* event;
        mutable bool   isRemoved = false;
    };

    std::vector< std::vector< elEvent* > > eventsWithColorIndex;
    uint64_t                               eventsWithColorIndexSize = 0;
    std::vector< elEvent* >                temporaryEventVector;
    uint64_t                               temporaryColorIndex = 0;

    bool                                   isActive{ true };
    std::set< event_t >                    registeredEvents;
    std::vector< std::function< void() > > registerUnregisterCallbacks;
    uint64_t                               currentEventLoopCycle{ 0 };
    bb::elGenericFactory< elEvent >        factory;
};
} // namespace GLAPI
} // namespace el3D
