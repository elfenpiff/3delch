#pragma once

#include "GLAPI/elWindow.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "logging/elLog.hpp"

#include <iostream>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on


#ifdef _WIN32
#define SDLCall( sdlFunc, ... )                                                \
    GLAPI::__SDLCall( __FILE__, __LINE__, __FUNCTION__, #sdlFunc, sdlFunc,     \
                      __VA_ARGS__ )
#else
#define SDLCall( sdlFunc, ... )                                                \
    GLAPI::__SDLCall( __FILE__, __LINE__, __PRETTY_FUNCTION__, #sdlFunc,       \
                      sdlFunc, __VA_ARGS__ )
#endif

namespace el3D
{
namespace GLAPI
{
template < typename Func, typename... Targs >
bb::elExpected< std::result_of_t< Func&( Targs... ) >, std::string >
__SDLCall( const char* file, const int line, const char* func,
           const char* funcName, Func& f,
           const std::function< bool( std::result_of_t< Func&( Targs... ) > ) >&
               isError,
           Targs... args )
{
    using Return       = std::result_of_t< Func&( Targs... ) >;
    Return returnValue = f( args... );

    if ( isError( returnValue ) )
    {
        std::string errorValue( SDL_GetError() );
        SDL_ClearError();

        logging::elLog::Log( logging::ERROR, file, line, func, 0 )
            << "call of sdl function " << funcName
            << " failed with error: " << errorValue;
        return bb::elExpected< Return, std::string >::CreateError( errorValue );
    }

    return bb::elExpected< Return, std::string >::CreateValue( returnValue );
}


} // namespace GLAPI
} // namespace el3D
