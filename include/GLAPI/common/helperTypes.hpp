#pragma once

#include <cstdint>

namespace el3D
{
namespace GLAPI
{
struct dimension_t
{
    uint64_t width;
    uint64_t height;
};

struct color_t
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

struct glVersion_t
{
    int major;
    int minor;
};

struct windowSize_t
{
    uint64_t x;
    uint64_t y;
};
} // namespace GLAPI
} // namespace el3D
