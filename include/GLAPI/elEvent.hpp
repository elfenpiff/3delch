#pragma once

#include "GLAPI/common/helperTypes.hpp"

#include <SDL.h>
#include <functional>
#include <memory>

namespace el3D
{
namespace GuiGL
{
namespace handler
{
class event;
}
} // namespace GuiGL

namespace GLAPI
{
class elEventHandler;
class elEvent
{

  public:
    using compareCallback_t = std::function< bool( const SDL_Event ) >;
    using callback_t        = std::function< void( const SDL_Event ) >;

    elEvent( elEventHandler* const parent ) noexcept;

    elEvent( const elEvent& ) = default;
    elEvent( elEvent&& )      = default;
    ~elEvent();

    elEvent&
    operator=( const elEvent& ) = default;
    elEvent&
    operator=( elEvent&& ) = default;

    elEvent&
    SetRemovalCondition( const compareCallback_t& ) noexcept;
    elEvent&
    SetCallback( const callback_t& ) noexcept;
    elEvent&
    SetAfterRemovalCallback( const callback_t& ) noexcept;
    bool
    HasDestructionCondition( const SDL_Event ) const noexcept;

    static void
    StartTextInput() noexcept;
    static void
    StopTextInput() noexcept;
    static bool
    HasTextInput() noexcept;
    static void
    MultiInstanceStartTextInput() noexcept;
    static void
    MultiInstanceStopTextInput() noexcept;
    elEvent&
    Register() noexcept;
    void
    Unregister() const noexcept;
    bool
    IsRegistered() const noexcept;
    void
    Reset() noexcept;

    friend class elEventHandler;
    friend class GuiGL::handler::event;

  private:
    void
    Activate() noexcept;
    void
    Deactivate() noexcept;

  private:
    static bool               hasTextInput;
    static size_t             hasTextInputCounter;
    static constexpr uint64_t INVALID_ID = 0;

    elEventHandler*         parent{ nullptr };
    mutable bool            isRegistered{ false };
    bool                    isActive{ true };
    callback_t              callback{ callback_t() };
    callback_t              afterRemovalCallback{ callback_t() };
    compareCallback_t       dtorComparator{ compareCallback_t() };
    std::function< void() > automaticDeRegistrationCallback{
        std::function< void() >() };
};
} // namespace GLAPI
} // namespace el3D
