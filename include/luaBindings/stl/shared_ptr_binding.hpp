#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"

#include <memory>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename T >
void
AddLuaSharedPtrBinding( const std::string& luaClassName,
                        const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/shared_ptr_binding.inl"
#endif
