#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"

#include <map>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename Key, typename Value >
void
AddLuaMapBinding( const std::string& luaClassName,
                  const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/map_binding.inl"
#endif
