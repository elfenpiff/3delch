#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"

#include <vector>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename T, typename Allocator = std::allocator< T > >
void
AddLuaVectorBinding( const std::string& luaClassName,
                     const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/vector_binding.inl"
#endif
