#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"

#include <functional>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename ReturnValue, typename... Arguments >
void
AddLuaFunctionBinding( const std::string& luaClassName,
                       const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/function_binding.inl"
#endif
