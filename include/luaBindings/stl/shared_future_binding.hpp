#pragma once
#ifdef ADD_LUA_BINDINGS

#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"
#include "units/Time.hpp"

#include <future>

namespace el3D
{
namespace luaBindings
{
namespace stl
{
template < typename T >
void
AddLuaSharedFutureBinding( const std::string& luaClassName,
                           const std::string& nameSpace = "" );
}
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/stl/shared_future_binding.inl"
#endif
