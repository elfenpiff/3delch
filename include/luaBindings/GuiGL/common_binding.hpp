
#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "GuiGL/common.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename Type, typename... Targs >
void
AddCTorAndRegisterLuaClass( const std::string& name )
{
    lua::elLuaScript::class_t newClass( name );
    newClass.AddConstructor< Type, Targs... >();
    lua::elLuaScript::RegisterLuaClass( newClass,
                                        ::el3D::GuiGL::NAMESPACE_NAME );
}

LUA_R( GUI_Common );
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
