#pragma once

#include "GuiGL/handler/widgetFactory.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
namespace handler
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
void
AddLuaWidgetFactoryInterface( ::el3D::lua::elLuaScript::class_t& newClass );
template < typename BaseClass, typename ChildType >
void
AddLuaWidgetFactoryCreateInterface(
    ::el3D::lua::elLuaScript::class_t& newClass );
#endif
} // namespace handler
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/GuiGL/handler/widgetFactory_binding.inl"
