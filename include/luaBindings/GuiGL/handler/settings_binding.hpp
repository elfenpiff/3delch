#pragma once

#include "GuiGL/handler/settings.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
namespace handler
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
static void
AddLuaSettingsInterface( ::el3D::lua::elLuaScript::class_t& newClass );
#endif
} // namespace handler
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/GuiGL/handler/settings_binding.inl"
