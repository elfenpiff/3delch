#pragma once

#include "GuiGL/elWidget_Slider.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS

template < typename BaseType, typename ChildType >
struct AddLuaWidgetInterface;

template < typename ChildType >
struct AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Slider, ChildType >
{
    static void
    Do( el3D::lua::elLuaScript::class_t& newClass );
};

LUA_R( elWidget_Slider );
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/GuiGL/elWidget_Slider_binding.inl"
