#pragma once

#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "lua/elLuaScript.hpp"
#include "luaBindings/GuiGL/handler/callback_binding.hpp"
#include "luaBindings/GuiGL/handler/settings_binding.hpp"
#include "luaBindings/GuiGL/handler/widgetFactory_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
void
AddLuaWidgetBinding( const std::string& );

template < typename BaseType, typename ChildType >
struct AddLuaWidgetInterface;

template < typename ChildType >
struct AddLuaWidgetInterface<::el3D::GuiGL::elWidget_base, ChildType >
{
    static void
    Do( ::el3D::lua::elLuaScript::class_t& newClass );
};

LUA_R( elWidget_base );
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/GuiGL/elWidget_base_binding.inl"
