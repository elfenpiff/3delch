#pragma once

#include "GuiGL/elWidget_Button.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS

template < typename BaseType, typename ChildType >
struct AddLuaWidgetInterface;

template < typename ChildType >
struct AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Button, ChildType >
{
    static void
    Do( lua::elLuaScript::class_t& newClass );
};

LUA_R( elWidget_Button );
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/GuiGL/elWidget_Button_binding.inl"
