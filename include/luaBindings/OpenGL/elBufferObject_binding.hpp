#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elBufferObject );
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
