#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace RenderPipeline
{
#ifdef ADD_LUA_BINDINGS
LUA_R( elRenderer_GUI );
#endif
} // namespace RenderPipeline
} // namespace luaBindings
} // namespace el3D
