#pragma once

#include "RenderPipeline/elRenderer_base.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace RenderPipeline
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
void
AddMethodsToLuaClass_elRenderer_base( lua::elLuaScript::class_t& newClass );
#endif
} // namespace RenderPipeline
} // namespace luaBindings
} // namespace el3D

#include "luaBindings/RenderPipeline/elRenderer_base_binding.inl"
