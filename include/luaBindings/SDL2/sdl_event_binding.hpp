#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace SDL2
{
#ifdef ADD_LUA_BINDINGS
LUA_R( sdl_event );
#endif
} // namespace SDL2
} // namespace luaBindings
} // namespace el3D
