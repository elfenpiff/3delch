#include "3Delch/3Delch.hpp"
#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
#ifdef ADD_LUA_BINDINGS
LUA_R( _3Delch );
#endif
} // namespace luaBindings
} // namespace el3D
