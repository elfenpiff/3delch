#pragma once

#include "lua/elLuaScript.hpp"

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_R( shared_ptr );
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
