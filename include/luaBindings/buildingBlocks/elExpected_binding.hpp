#pragma once
#ifdef ADD_LUA_BINDINGS

#include "buildingBlocks/elExpected.hpp"
#include "lua/elLuaScript.hpp"
#include "luaBindings/types.hpp"

namespace el3D
{
namespace luaBindings
{
namespace bb
{
template < typename Key, typename Value >
void
AddLuaExpectedBinding( const std::string& luaClassName,
                       const std::string& nameSpace = "" );

template < typename Value >
void
AddLuaExpectedBinding( const std::string& luaClassName,
                       const std::string& nameSpace = "" );
} // namespace bb
} // namespace luaBindings
} // namespace el3D
#include "luaBindings/buildingBlocks/elExpected_binding.inl"
#endif
