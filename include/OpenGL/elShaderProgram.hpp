#pragma once

#include "DesignPattern/Creation.hpp"
#include "OpenGL/elGLSLParser.hpp"
#include "OpenGL/gl_Call.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <limits>
#include <string>
#include <utility>
#include <vector>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

/*!
\code
    try {
        elShaderProgram prog(vertexCode, fragCode);

        GLint var1ID = prog.GetUniformLocation("var1Name");
        prog.SetUniform(var1ID, glm::vec4(1.0));

        prog.Use();

        ...
        prog.StopUse();

    } catch (std::runtime_error &e) {
        std::cerr << e.what();
    }
\endcode
*/

namespace el3D
{
namespace OpenGL
{
enum class elShaderProgram_Error
{
    CouldNotCompile_VertexShader,
    CouldNotCompile_FragmentShader,
    CouldNotCompile_GeometryShader,
    CouldNotCompile_TessellationControlShader,
    CouldNotCompile_TessellationEvaluationShader,
    CouldNotCompile_ComputeShader,
    CouldNotFindSourceGroup,
    UnableToOpenShaderSourceFile,
    UnableToCreateShader,
    UnableToCompileShader,
    UnableToLinkShader,
    UnableToFindUniformIndices,
    UnableToFindUniformBlockIndex,
    UnableToFindUniformLocation,
    UnableToFindAttributeLocation
};

struct shaderFromSource_t
{
};
struct shaderFromFile_t
{
};

inline constexpr shaderFromSource_t shaderFromSource;
inline constexpr shaderFromFile_t   shaderFromFile;

class elShaderProgram
    : public DesignPattern::Creation< elShaderProgram, elShaderProgram_Error >
{
  public:
    struct shaderAttribute_t
    {
        static constexpr GLuint INVALID_ID =
            std::numeric_limits< GLuint >::max();

        shaderAttribute_t() = default;
        shaderAttribute_t( const GLuint id, const std::string &identifier,
                           const GLint size, const GLuint dimension,
                           const GLenum dataType,
                           const GLuint divisor ) noexcept;
        shaderAttribute_t( const std::string &identifier, const GLint size,
                           const GLuint dimension = 1, const GLuint divisor = 0,
                           const GLenum dataType = GL_FLOAT ) noexcept;

        explicit operator bool() const noexcept;

        GLuint      id{ INVALID_ID };
        std::string identifier;
        GLint       size{ 1 };
        GLuint      dimension{ 1 };
        GLenum      dataType{ GL_FLOAT };
        GLuint      divisor{ 0 };
    };


    /*!
        \param order: vertex, fragment, geometry, tessellation control,
       tessellation
       evaluation
    */
    elShaderProgram()                          = delete;
    elShaderProgram( const elShaderProgram & ) = delete;
    elShaderProgram( elShaderProgram && );
    ~elShaderProgram();

    elShaderProgram &
    operator=( const elShaderProgram & ) = delete;
    elShaderProgram &
    operator=( elShaderProgram && );

    /*!
        \brief      set the file name of the shader source, it is for debugging
                    purposes only
    */
    void
    SetFileName( const std::string & ) noexcept;
    std::string
    GetFileAndGroupName() const noexcept;

    /*!
        \brief      use the shader program, if it is not already in use; you
       need to add
       the shader
       source code and link the shader successfully before you can call Use()
    */
    void
    Bind() const noexcept;

    /*!
        \brief      stop using the shader program
    */
    void
    Unbind() const noexcept;

    /*!
        \brief      calls glValidateProgram and validates current program; a
                    debug feature
    */
    bool
    ValidateProgram() const noexcept;

    /*!
        \brief      Sets the callback function which is called everytime the
                    method Bind() is called. Perfect way to set uniforms which
                    are changing in every frame like the current runtime.
    */
    void
    SetBindCallback(
        const std::function< void( const elShaderProgram & ) > & ) noexcept;

    /*!
       \brief returns a completely setted shaderAttribute_t struct for a
       specific attribute
       \pre the shader attribute must exist, otherwise shaderAttribute_t::id is
       undefined; to check for the existens of the attribute identified by
       identifier use GetAttribLocation
    */
    bb::elExpected< shaderAttribute_t, elShaderProgram_Error >
    GetAttribute( const std::string &identifier, const GLint size,
                  const GLuint dimension = 1, const GLuint divisor = 0,
                  const GLenum dataType = GL_FLOAT ) const;

    bool
    HasAttribute( const std::string & ) const;

    /*!
        \brief      return an attrib id
    */
    bb::elExpected< GLint, elShaderProgram_Error >
    GetAttributeLocation( const std::string & ) const;

    /*!
        \brief      returns an uniform id
    */
    bb::elExpected< GLint, elShaderProgram_Error >
    GetUniformLocation( const std::string & ) const;

    bool
    HasUniform( const std::string & ) const;

    /*!
        \brief      retrieve the index of a named uniform block
    */
    bb::elExpected< GLuint, elShaderProgram_Error >
    GetUniformBlockIndex( const std::string & ) const;

    bool
    HasUniformBlock( const std::string & ) const;

    /*!
        \brief      query information about an active uniform block
    */
    GLint
    GetActiveUniformBlockiv( const GLuint uniformBlockIndex,
                             const GLenum pname ) const noexcept;

    /*!
        \brief      retrieve the index of a named uniform block
      */
    bb::elExpected< std::vector< GLuint >, elShaderProgram_Error >
    GetUniformIndices( const std::vector< std::string > &uniformNames ) const;

    /*!
        \brief      returns information about several active uniform variables
      */
    std::vector< GLint >
    GetActiveUniformsiv( const std::vector< GLuint > &uniformIndices,
                         const GLenum                 pname ) const noexcept;

    template < typename T >
    T
    GetUniform( const GLint ) const noexcept;

    /*!
        \brief      glUniform* interface
        \param     uniformID (see GetUniformLocation); value
    */
    template < typename T >
    void
    SetUniform( const GLint, const T & ) const noexcept;

    GLuint
    GetShaderID() const noexcept;

    friend struct DesignPattern::Creation< elShaderProgram,
                                           elShaderProgram_Error >;

  private:
    elShaderProgram( const shaderFromSource_t, const std::string &vertSrc = "",
                     const std::string &fragSrc  = "",
                     const std::string &geomSrc  = "",
                     const std::string &tCtrlSrc = "",
                     const std::string &tEvalSrc = "",
                     const std::string &compSrc  = "" ) noexcept;
    elShaderProgram( const shaderFromFile_t, const std::string &fileName,
                     const std::string &group, const int shaderVersion,
                     const elGLSLParser::staticDefines_t &defines =
                         elGLSLParser::staticDefines_t() ) noexcept;

    void
    BindNoCallback() const noexcept;
    std::string
    GetInfoLog( const GLuint ) const noexcept;
    bb::elExpected< elShaderProgram_Error >
    SetSource( const std::string &, const GLenum ) noexcept;
    bb::elExpected< elShaderProgram_Error >
    Link() const noexcept;
    bb::elExpected< elShaderProgram_Error >
    Init( const std::string &, const std::string &, const std::string &,
          const std::string &, const std::string &, const std::string & );

  private:
    GLuint                                           id;
    static GLuint                                    activeShader;
    std::string                                      fileName;
    std::string                                      groupName;
    std::vector< std::pair< GLuint, GLenum > >       attachedShader;
    std::function< void( const elShaderProgram & ) > bindCallback;
};
} // namespace OpenGL
} // namespace el3D
