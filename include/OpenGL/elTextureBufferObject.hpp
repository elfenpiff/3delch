#pragma once

#include "OpenGL/elBufferObject.hpp"

namespace el3D
{
namespace OpenGL
{
class elTextureBufferObject : public elBufferObject
{
  public:
    elTextureBufferObject( const size_t n,
                           const GLenum internalFormat = GL_RGBA32F,
                           const GLenum usage = GL_STATIC_DRAW ) noexcept;
    elTextureBufferObject( elTextureBufferObject&& ) noexcept;
    ~elTextureBufferObject();

    elTextureBufferObject&
    operator=( elTextureBufferObject&& ) noexcept;

    void
    Bind( const GLint uniform, const GLenum textureUnit,
          const size_t n = 0 ) const noexcept;
    void
    Unbind( const size_t n = 0 ) const noexcept override;

  private:
    void
    Bind( const size_t n ) const noexcept override;

    std::vector< GLuint > textureIDs;
    GLenum                internalFormat;
};
} // namespace OpenGL
} // namespace el3D
