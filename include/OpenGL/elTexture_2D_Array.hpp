#pragma once

#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elTexture_base.hpp"

namespace el3D
{
namespace OpenGL
{
class elTexture_2D_Array : public elTexture_2D
{
  public:
    elTexture_2D_Array( const std::string& debugIdentifier,
                        const uint64_t sizeX = 1, const uint64_t sizeY = 1,
                        const uint64_t arraySize      = 1,
                        const GLint    internalFormat = GL_RGBA8,
                        const GLenum   format         = GL_RGBA,
                        const GLenum   type = GL_UNSIGNED_BYTE ) noexcept;
    elTexture_2D_Array( elTexture_2D_Array&& ) noexcept;

    elTexture_2D_Array&
    operator=( elTexture_2D_Array&& ) noexcept;

    const elTexture_2D_Array&
    TexImage( const uint64_t index, const GLvoid* pixels ) const noexcept;
    const elTexture_2D_Array&
    TexSubImage( const uint64_t index, const GLint offsetX, const GLint offsetY,
                 const uint64_t width, const uint64_t height,
                 const GLvoid* pixels ) const noexcept;

    template < typename T >
    const elTexture_2D_Array&
    TexImage( const uint64_t                    index,
              const std::initializer_list< T >& pixels ) const noexcept;
    template < typename T >
    const elTexture_2D_Array&
    TexSubImage( const uint64_t index, const GLint offsetX, const GLint offsetY,
                 const uint64_t width, const uint64_t height,
                 const std::initializer_list< T >& pixels ) const noexcept;
};
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elTexture_2D_Array.inl"
