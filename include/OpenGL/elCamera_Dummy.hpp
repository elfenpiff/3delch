#pragma once

#include "OpenGL/elCamera.hpp"

namespace el3D
{
namespace OpenGL
{
class elCamera_Dummy : public elCamera
{
  public:
    elCamera_Dummy() noexcept;
    elCamera_Dummy( const glm::mat4& viewProjection ) noexcept;

    void
    SetProjection( const glm::mat4& value ) noexcept;
    void
    SetView( const glm::mat4& value ) noexcept;
    void
    SetViewProjection( const glm::mat4& value ) noexcept;

  protected:
    void
    Update() const noexcept override;
};
} // namespace OpenGL
} // namespace el3D

