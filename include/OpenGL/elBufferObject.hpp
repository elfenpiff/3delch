#pragma once

#include <GL/glew.h>
#include <algorithm>
#include <vector>

/*!
    \code
        elBufferObject b1(8), b2();

        for(int i = 0; i < 8; i++)
            b1.SetBufferTarget(GL_QUERY_BUFFER, i);

        b2.SetBufferTarget(GL_TEXTURE_BUFFER);
        b2.SetBufferUsage(GL_STATIC_READ);

        std::vector<GLfloat> data(100);

        b1.BufferData(data.size() * sizeof(GLfloat), data.data(), 4);
        b2.BufferSubData(sizeof(GLfloat) * pos,
            data.size() * sizeof(GLfloat), data.data());

        auto bufferSize = b1.GetBufferParameteriv(GL_BUFFER_SIZE, 4);

        GLfloat * stuff = new GLfloat[100]();
        b1.GetBufferSubData(sizeof(GLfloat) * pos, sizeof(GLfloat) * size,
   stuff);

        ...
        b1.Use(4);
        ...
        b2.Use();
        ...
        b2.StopUse();
        ...
        b1.StopUse(4);

    \endcode
*/
namespace el3D
{
namespace OpenGL
{
class elBufferObject
{
  public:
    /*!
       \brief create one or more new buffer on the GPU
       \param "size of the new buffer, default = 1"; defaultBufferTarget =
       GL_ARRAY_BUFFER;
       defaultBufferUsage = GL_STATIC_DRAW
    */
    elBufferObject() noexcept = default;
    elBufferObject( elBufferObject && ) noexcept;
    elBufferObject( const size_t &n, const GLenum &target = GL_ARRAY_BUFFER,
                    const GLenum &usage = GL_STATIC_DRAW ) noexcept;
    virtual ~elBufferObject();

    elBufferObject &
    operator=( elBufferObject && ) noexcept;

    /*!
        \brief returns the number of buffers allocated with glGenBuffers
    */
    size_t
    GetNumberOfBuffers() const noexcept;

    /*!
       \brief bind the buffer with glBindBuffer
       \param internal number of buffer, if you have an elGLBuffer(4) and want
       to use the second buffer elGLBuffer.Use(1)
    */
    virtual void
    Bind( const size_t n = 0 ) const noexcept;

    /*!
       \brief unbinds the buffer with glBindBuffer(GL_ARRAY_BUFFER, 0);
       \param internal number of buffer, default = 0
    */
    virtual void
    Unbind( const size_t n = 0 ) const noexcept;

    /*!
       \brief sets the bufferTarget in glBindBuffer(bufferTarget, bufferID) for
       the Use()
       function
       \param bufferTarget; internal number of buffer, default = 0
    */
    void
    SetBufferTarget( const GLenum, const size_t n = 0 ) noexcept;

    /*!
       \brief sets the bufferUsage in glBufferData/glBufferSubData for the
       BufferData,
       BufferSubData
       function
       \param bufferUsage; internal number of buffer, default = 0
    */
    void
    SetBufferUsage( const GLenum, const size_t n = 0 ) noexcept;

    /*!
       \brief glBufferData; add data to buffer
       \param GLsizeiptr bufferSize; GLvoid * bufferData; size_t internal number
       of
       buffer, default
       = 0
    */
    void
    BufferData( const GLsizeiptr, const GLvoid *, const size_t n = 0 ) const
        noexcept;

    /*!
       \brief glBufferData; add data to buffer
       \param GLintptr dataoffset; GLsizeiptr bufferSize; GLvoid * bufferData;
       size_t
       internal
       number of buffer, default
       = 0
    */
    void
    BufferSubData( const GLintptr, const GLsizeiptr, const GLvoid *,
                   const size_t n = 0 ) const noexcept;

    /*!
       \brief glGetBufferParameteriv returns specific information to a specific
       buffer
       \param GLenum value; size_t n = internal buffer number, default = 0;
       (GLenum target
       is set
       via SetBufferTarget)
    */
    GLint
    GetBufferParameteriv( const GLenum, const size_t n = 0 ) const noexcept;

    /*!
        \code
            GLvoid * data = new GLvoid[sizePtr]();
            buffer.GetBufferSubData(offset, sizePtr, data, 2);
        \endcode
       \brief glGetBufferSubData; writes the content of the buffer into the
       prereserved
       GLvoid * data
       \param GLintptr offset, GLsizei size, GLvoid * data, size_t n = internal
       buffer
       number, default = 0
    */
    void
    GetBufferSubData( const GLintptr, const GLsizei, GLvoid *,
                      const size_t n = 0 ) const noexcept;

  protected:
    GLenum                stdTarget;
    GLenum                stdUsage;
    std::vector< GLuint > bufferIDs;
    std::vector< GLenum > bufferTarget;
    std::vector< GLenum > bufferUsage;
};
} // namespace OpenGL
} // namespace el3D
