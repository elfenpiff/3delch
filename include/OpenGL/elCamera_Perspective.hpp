#pragma once

#include "OpenGL/elCamera.hpp"
#include "units/Angle.hpp"
#include "units/AngularVelocity.hpp"
#include "units/Length.hpp"
#include "units/Time.hpp"
#include "units/Velocity.hpp"
#include "utils/elGenericRAII.hpp"

#include <functional>
#include <optional>

namespace el3D
{
namespace HighGL
{
class elCameraController_base;
}
namespace OpenGL
{
class elCamera_Perspective : public elCamera
{
  public:
    struct state_t
    {
        glm::vec3    position = glm::vec3( 0.0f, 0.0f, 0.0f );
        glm::vec3    up       = glm::vec3( 0.0f, 1.0f, 0.0f );
        glm::vec3    lookAt   = glm::vec3( 0.0f, 0.0f, 1.0f );
        units::Angle fov      = units::Angle::Degree( 45.0 );
    };

    using stateLoader_t      = std::function< state_t() >;
    using stateLoaderGuard_t = utils::elGenericRAII;

    elCamera_Perspective() noexcept;

    // not move or copyable because of the state loader guard
    elCamera_Perspective( elCamera_Perspective&& )      = delete;
    elCamera_Perspective( const elCamera_Perspective& ) = delete;

    elCamera_Perspective&
    operator=( elCamera_Perspective&& ) = delete;
    elCamera_Perspective&
    operator=( const elCamera_Perspective& ) = delete;

    const state_t&
    GetState() const noexcept;
    void
    SetState( const state_t& state ) noexcept;
    glm::vec2
    GetViewPortSize() const noexcept;

    bool
    HasStateLoader() const noexcept;

    void
    SetViewPortSize( const glm::vec2& size ) noexcept;
    void
    SetLookAt( const glm::vec3& value ) noexcept;
    void
    SetPosition( const glm::vec3& value ) noexcept;
    void
    SetUp( const glm::vec3& value ) noexcept;
    void
    SetFov( const units::Angle fov ) noexcept;

  protected:
    friend class HighGL::elCameraController_base;

    virtual void
    Update() const noexcept override;

    void
    GenerateMatrices() const noexcept;

    void
    ResetStateLoader();

    [[nodiscard]] stateLoaderGuard_t
    SetStateLoader( const stateLoader_t& loader ) noexcept;

  protected:
    mutable state_t state;
    glm::vec2       viewPortSize = glm::vec2( 1.0 );
    stateLoader_t   stateLoader;
};
} // namespace OpenGL
} // namespace el3D
