#pragma once

#include "GLAPI/elWindow.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/gl_Call.hpp"

#include <GL/glew.h>
#include <algorithm>
#include <glm/glm.hpp>


namespace el3D
{
namespace OpenGL
{
class elTexture_2D_Array;
class elTexture_CubeMap;
class elFrameBufferObject
{
  public:
    elFrameBufferObject( const size_t numberOfFrameBufferObjects ) noexcept;
    elFrameBufferObject( elFrameBufferObject&& ) noexcept;
    elFrameBufferObject( const elFrameBufferObject& ) = delete;
    ~elFrameBufferObject();

    elFrameBufferObject&
    operator=( const elFrameBufferObject& ) = delete;
    elFrameBufferObject&
    operator=( elFrameBufferObject&& ) noexcept;

    void
    Bind( const size_t n ) const noexcept;
    void
    Bind( const std::vector< GLenum > buffer, const size_t n ) const noexcept;
    void
    Unbind() const noexcept;
    void
    Clear() const noexcept;
    void
    Clear( const size_t n, const size_t attachment ) const noexcept;
    void
    Reshape( const size_t n ) noexcept;

    void
    Read( const GLenum attachment, const size_t n ) const noexcept;
    void
    ReadStop() const noexcept;
    void
    Draw( const size_t n ) const noexcept;
    void
    DrawStop() const noexcept;

    void
    SetSize( const uint64_t sizeX, const uint64_t sizeY,
             const size_t n ) noexcept;
    glm::ivec2
    GetSize( const size_t n ) const noexcept;

    size_t
    AddFramebufferTexture( elTexture_2D* tex, const GLenum attachment,
                           const size_t n ) noexcept;
    size_t
    AddFramebufferTexture( elTexture_2D_Array* tex, const GLint layer,
                           const GLenum attachment, const size_t n ) noexcept;
    size_t
    AddFramebufferTexture( elTexture_CubeMap* tex, const GLenum cubeTarget,
                           const GLenum attachment, const size_t n ) noexcept;
    void
    BlitFrameBuffer( const GLint srcX0, const GLint srcY0, const GLint srcX1,
                     const GLint srcY1, const GLint dstX0, const GLint dstY0,
                     const GLint dstX1, const GLint dstY1,
                     const GLbitfield mask, const GLenum filter );
    elTexture_base*
    GetTexture( const size_t textureIndex, const size_t n ) const noexcept;

    /// @brief reads pixels from an attache elTexture_2D, other attached texture
    ///         formats are not supported
    template < typename T >
    std::vector< T >
    ReadPixels( const GLint x, const GLint y, const GLsizei width,
                const GLsizei height, const GLsizei valueSize,
                const size_t attachment, const size_t n ) noexcept;

    GLint
    GetMaximumNumberOfColorAttachments() const noexcept;
    GLenum
    CheckFramebufferStatus( const size_t n ) const noexcept;
    uint64_t
    GetFrameBufferSize() const noexcept;

  private:
    void
    Attach( const size_t n, const GLenum attachment ) noexcept;
    uint64_t
    AddTexture( const size_t n, elTexture_base* const texture ) noexcept;

    size_t
    AddFramebufferTextureLayer( elTexture_base* const tex, const GLint layer,
                                const GLenum attachment,
                                const size_t n ) noexcept;
    size_t
    AddFramebufferTexture2D( elTexture_base* const tex, const GLenum target,
                             const GLenum attachment, const size_t n ) noexcept;

  private:
    struct frameBuffer_t
    {
        frameBuffer_t( const size_t n );
        struct entry_t
        {
            uint64_t                       sizeX;
            uint64_t                       sizeY;
            std::vector< elTexture_base* > textures;
            std::vector< GLenum >          attachments;
            std::vector< GLenum >          attachmentsNoDepthStencil;
        };

        std::vector< GLuint >  ids;
        std::vector< entry_t > data;
    } frameBuffer;

    mutable GLint viewportSize[4];
};
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elFrameBufferObject.inl"
