#pragma once

#include "OpenGL/elTexture_base.hpp"

namespace el3D
{
namespace OpenGL
{
class elTexture_CubeMap : public elTexture_base
{
  public:
    elTexture_CubeMap( const std::string& debugIdentifier,
                       const uint64_t sizeX = 1, const uint64_t sizeY = 1,
                       const GLint  internalFormat = GL_RGBA8,
                       const GLenum format         = GL_RGBA,
                       const GLenum type = GL_UNSIGNED_BYTE ) noexcept;
    elTexture_CubeMap( elTexture_CubeMap&& ) noexcept;

    elTexture_CubeMap&
    operator=( elTexture_CubeMap&& ) noexcept;

    void
    TexImage( const GLvoid* const dataPositiveX,
              const GLvoid* const dataNegativeX,
              const GLvoid* const dataPositiveY,
              const GLvoid* const dataNegativeY,
              const GLvoid* const dataPositiveZ,
              const GLvoid* const dataNegativeZ ) noexcept;
};
} // namespace OpenGL
} // namespace el3D
