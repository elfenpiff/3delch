#pragma once

#include "OpenGL/elCamera.hpp"
#include "OpenGL/elModelMatrix.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "OpenGL/types.hpp"
#include "animation/elAnimatable.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

namespace el3D
{
namespace OpenGL
{
class elTexture_base;
enum class elGeometricObject_Type : uint64_t
{
    base = 0,
    VertexObject,
    UnifiedVertexObject,
    InstancedObject,
    NonVertexObject,
    End
};

struct textureAttachment_t
{
};
/// maybe move it into base later
class elGeometricObject_ModelMatrix
    : public elModelMatrix< elGeometricObject_ModelMatrix,
                            animation::interpolation::Hermite >
{
  public:
    elGeometricObject_ModelMatrix() = default;
    elGeometricObject_ModelMatrix( const glm::vec3& position,
                                   const glm::vec3& scaling,
                                   const glm::vec3& rotationAxis,
                                   const float      rotationDegree ) noexcept;

    void
    SetPosition( const glm::vec3& ) noexcept override;
    void
    SetScaling( const glm::vec3& ) noexcept override;
    void
    SetRotation( const glm::vec4& rotation ) noexcept override;
    void
    SetModelMatrix( const glm::mat4& matrix ) noexcept override;

    glm::vec3
    GetPosition() const noexcept override;
    glm::vec3
    GetScaling() const noexcept override;
    glm::vec3
    GetRotationAxis() const noexcept override;
    GLfloat
    GetRotationDegree() const noexcept override;

    glm::mat4
    GetModelMatrix() const noexcept;

  protected:
    mutable bool hasUpdatedValues  = true;
    mutable bool hasModelMatrixSet = false;

  private:
    template < typename T >
    using AnimationInterpolation = animation::interpolation::Hermite< T >;
    using parent_t =
        elModelMatrix< elGeometricObject_ModelMatrix, AnimationInterpolation >;

    glm::vec3         position       = glm::vec3( 0.0 );
    glm::vec3         scaling        = glm::vec3( 1.0 );
    glm::vec3         rotationAxis   = glm::vec3( 1.0 );
    GLfloat           rotationDegree = 0.0f;
    mutable glm::mat4 modelMatrix    = glm::mat4( 1.0 );
};

class elGeometricObject_base
{
  public:
    static constexpr uint64_t ATTACHED_TO_ALL_SHADERS =
        std::numeric_limits< uint64_t >::max();

    /// @brief DrawMode defines the GeometricObject since we only distinct
    ///             between
    ///           objects with adjacency or no adjacency. If you would like to
    ///           draw a wireframe you have to handle it via the geometry shader
    elGeometricObject_base( const elGeometricObject_base& )     = delete;
    elGeometricObject_base( elGeometricObject_base&& ) noexcept = default;
    virtual ~elGeometricObject_base() noexcept                  = default;

    elGeometricObject_base&
    operator=( const elGeometricObject_base& ) = delete;
    elGeometricObject_base&
    operator=( elGeometricObject_base&& ) noexcept = default;

    bb::elExpected< size_t, elShaderProgram_Error >
    AddShader( const elShaderProgram* const ) noexcept;
    const elShaderProgram*
    GetShader( const size_t shaderID ) const noexcept;
    void
    Draw( const size_t shaderID, const elCamera& camera ) const noexcept;

    void
    SetPreDrawCallback( const std::function< void() >& ) noexcept;
    void
    SetPostDrawCallback( const std::function< void() >& ) noexcept;

    elGeometricObject_Type
    GetType() const noexcept;

    template < typename T, typename... Targs >
    bb::product_ptr< T >
    CreateTexture( const size_t shaderId, const std::string& uniform,
                   const GLenum textureUnit, Targs&&... args ) noexcept;

    bb::product_guard< textureAttachment_t >
    AttachTexture( const elTexture_base* const, const size_t shaderID,
                   const std::string& uniform, const GLenum textureUnit );

    template < typename T >
    void
    UpdateBuffer( const size_t buffer, const std::vector< T >& data ) noexcept;
    template < typename T >
    void
    UpdateSubBuffer( const size_t buffer, const std::vector< T >& data,
                     const uint64_t elementOffset ) noexcept;

    void
    SetCullFace( const uint64_t shaderId, const CullFace& cullFace ) noexcept;
    void
    SetDepthTest( const uint64_t   shaderId,
                  const DepthTest& depthTest ) noexcept;

  protected:
    template < typename... ObjectBufferArgs >
    elGeometricObject_base( const DrawMode drawMode, const uint64_t dimension,
                            const elGeometricObject_Type type,
                            ObjectBufferArgs&&... args ) noexcept;

    virtual void
    DrawImplementation( const uint64_t  shaderId,
                        const elCamera& camera ) const noexcept = 0;

    void
    CallPreDrawCallback() const noexcept;
    void
    CallPostDrawCallback() const noexcept;
    size_t
    InsertShader( const elShaderProgram* const ) noexcept;
    void
    RemoveShader( const uint64_t shaderId ) noexcept;
    GLenum
    GetDrawModeForOpenGL() const noexcept;
    DrawMode
    GetDrawMode() const noexcept;

    virtual bb::elExpected< size_t, elShaderProgram_Error >
    AttachShader( const uint64_t shaderId ) noexcept = 0;

    struct shader_t
    {
        struct uniform_t
        {
            std::string name;
            GLint       id{ -1 };
        };

        struct texture_t
        {
            const elTexture_base* texture;
            GLint                 uniformID{ -1 };
            GLenum                textureUnit;
        };

        const elShaderProgram*                            shader;
        std::vector< uniform_t >                          uniforms;
        std::vector< elShaderProgram::shaderAttribute_t > attributes;
        std::vector< texture_t >                          textures;
        CullFace  cullFace  = CullFace::Default;
        DepthTest depthTest = DepthTest::Default;

        void
        RegisterUniforms(
            const std::vector< std::string >& uniformList ) noexcept;

        void
        RegisterAttributes(
            const std::vector< elShaderProgram::shaderAttribute_t >&
                attributeList ) noexcept;

        bool
        AttachTexture( const std::string&          uniformName,
                       const elTexture_base* const texture,
                       const GLenum                textureUnit ) noexcept;
        void
        DetachTexture( const elTexture_base* const texture ) noexcept;

        void
        BindTextures() const noexcept;

        friend class elGeometricObject_base;

      private:
        shader_t( const elShaderProgram* const shader ) noexcept;
    };
    bb::elUniqueIndexVector< shader_t > shaders;
    OpenGL::elObjectBuffer              objectBuffer;

  private:
    void
    RemoveTexture( const elTexture_base* const texture ) noexcept;
    void
    DetachTexture( const elTexture_base*, const size_t shaderID ) noexcept;

    struct textureCache_t
    {
        std::string           uniform;
        GLenum                textureUnit;
        const elTexture_base* texture;
    };

    struct texture_t
    {
        std::string                              uniform;
        GLenum                                   textureUnit;
        uint64_t                                 shaderId;
        bb::product_guard< textureAttachment_t > attachment;
    };

    std::vector< textureCache_t > attached2AllTexturesVector;
    std::function< void() >       preDrawCallback;
    std::function< void() >       postDrawCallback;
    elGeometricObject_Type        type;
    DrawMode                      drawMode;
    // should be always last member / cleanup can access other members
    bb::elGenericFactory< textureAttachment_t,
                          bb::CompositeProduct< elTexture_base, texture_t > >
        factory;
};
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elGeometricObject_base.inl"
