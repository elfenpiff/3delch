#pragma once

#include "OpenGL/elBufferObject.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "OpenGL/elVertexArrayObject.hpp"

#include <initializer_list>

namespace el3D
{
namespace OpenGL
{
struct instanceGeometry_t
{
    glm::vec3 position = { 0.0f, 0.0f, 0.0f };
    glm::vec3 scaling  = { 1.0f, 1.0f, 1.0f };
    glm::vec4 rotation = { 0.0f, 0.0f, 0.0f, 0.0f };
};

class elGeometricObject_InstancedObject : public elGeometricObject_base
{
  public:
    elGeometricObject_InstancedObject( const DrawMode          drawMode,
                                       const objectGeometry_t &objectGeometry );


    void
    UpdateGeometry(
        const std::vector< instanceGeometry_t > &geometry ) noexcept;

  protected:
    virtual void
    DrawImplementation(
        const uint64_t          shaderID,
        const OpenGL::elCamera &camera ) const noexcept override;

  private:
    enum uniform_t
    {
        UNIFORM_VP = 0,
    };

    enum buffer_t
    {
        BUFFER_VERTEX = 0,
        BUFFER_ELEMENT,
        BUFFER_TEXTURE_COORDINATE,
        BUFFER_NORMAL,
        BUFFER_TANGENT,
        BUFFER_BITANGENT,
        BUFFER_MVP,
        BUFFER_END
    };

    static const std::vector< elShaderProgram::shaderAttribute_t > ATTRIBUTES;
    static constexpr char VP_ATTRIBUTE_NAME[] = "viewProjection";

    mutable bool                                 hasUpdatedGeometry{ false };
    std::vector< elGeometricObject_ModelMatrix > modelMatrix;
    std::vector< glm::mat4 >                     modelMatrixVector;
    uint64_t                                     numberOfInstances{ 0 };

  private:
    virtual bb::elExpected< size_t, elShaderProgram_Error >
    AttachShader( const uint64_t shaderId ) noexcept override;
};
} // namespace OpenGL
} // namespace el3D
