#pragma once

#include <GL/glew.h>
#include <cstdint>
#include <vector>


namespace el3D
{
namespace OpenGL
{
enum class RenderTarget : int
{
    Forward  = 0,
    Deferred = 1
};

enum class CullFace
{
    Default,
    Disable,
    Front,
    Back,
    FrontAndBack,
};

enum class DepthTest
{
    Default,
    Disable,
    Enable
};

enum class DrawMode
{
    Triangles = 0,
    TrianglesWithAdjacency,
    Points,
    LineStrip,
    Lines,
    End
};

struct objectGeometry_t
{

    std::vector< GLfloat > vertices           = {};
    std::vector< GLuint >  elements           = {};
    std::vector< GLfloat > textureCoordinates = {};
    std::vector< GLfloat > normals            = {};
    std::vector< GLfloat > tangents           = {};
    std::vector< GLfloat > bitangents         = {};

    std::vector< GLuint >  innerElements = {};
    std::vector< GLfloat > innerNormals  = {};
};

template < typename T >
struct DataTypeToEnum;

template < GLenum DataType >
struct EnumToDataType;


uint64_t
SizeOfType( const GLenum type ) noexcept;

} // namespace OpenGL
} // namespace el3D

#include "OpenGL/types.inl"
