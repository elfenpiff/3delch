#pragma once
#include "OpenGL/types.hpp"

#include <GL/glew.h>
#include <string>

#define glVoid( glFunc ) __gl( __FILE__, __LINE__, __func__, #glFunc, glFunc )
#define gl( glFunc, ... )                                                      \
    __gl( __FILE__, __LINE__, __func__, #glFunc, glFunc, __VA_ARGS__ )

namespace el3D
{
namespace OpenGL
{
std::string EnumToString( GLenum );

template < typename T >
T
Get( const GLenum pname ) noexcept;

template < typename T >
T
GetIndex( const GLenum target, const GLuint index ) noexcept;

void
SetDepthTest( const DepthTest depthTest ) noexcept;
DepthTest
GetDepthTestState() noexcept;

void
SetCullFace( const CullFace cullFace ) noexcept;
CullFace
GetCullFaceState() noexcept;

void
Enable( const GLenum cap ) noexcept;
void
Disable( const GLenum cap ) noexcept;
bool
IsEnabled( const GLenum cap ) noexcept;
void
DebugMessageCallback( GLenum source, GLenum type, GLuint id, GLenum severity,
                      GLsizei length, const GLchar *message,
                      const void *userParam );

GLenum
ColorAttachmentFromOffset( const uint64_t offset ) noexcept;

namespace internal
{
struct LastGLCall
{
    static std::string file;
    static int         line;
    static std::string func;
    static std::string glFunc;
};

void
Call( const char *file, const int line, const char *func, const char *glFunc );
} // namespace internal

} // namespace OpenGL
} // namespace el3D

#include "OpenGL/gl_Call.inl"
