#pragma once

#include "OpenGL/elShaderProgram.hpp"

#include <memory>

/*!
    \code
        elVertexArrayObject fuu(2);
        fuu.Bind();
        ....
        fuu.Bind(1);
        ,,,
    \endcode
*/
namespace el3D
{
namespace OpenGL
{
class elVertexArrayObject
{
  public:
    elVertexArrayObject()                              = delete;
    elVertexArrayObject( const elVertexArrayObject & ) = delete;
    elVertexArrayObject( elVertexArrayObject && ) noexcept;
    elVertexArrayObject( const size_t &n = 1 ) noexcept;
    ~elVertexArrayObject();

    elVertexArrayObject &
    operator=( const elVertexArrayObject & ) = delete;
    elVertexArrayObject &
    operator=( elVertexArrayObject && ) noexcept;

    void
    Bind( const size_t &n = 0 ) const noexcept;
    void
    Unbind() const noexcept;

    /*!
       \brief enables the vertex array object for a attribute of a shader;
       glEnableVertexAttribArray
       \param shaderAttribute_t can be acquired with
       elShaderProgram::GetAttrib(...)
    */
    void
    EnableVertexAttribArray( const elShaderProgram::shaderAttribute_t & ) const
        noexcept;

    /*!
        \brief glDisableVertexAttribArray
    */
    void
    DisableVertexAttribArray( const elShaderProgram::shaderAttribute_t & ) const
        noexcept;

    /*!
        \brief glVertexAttribDivisor
    */
    void
    VertexAttribDivisor( const elShaderProgram::shaderAttribute_t & ) const
        noexcept;

  private:
    std::vector< GLuint > vaoIDs;
    static GLuint         activeVertexArrayObject;
};
} // namespace OpenGL
} // namespace el3D
