#pragma once

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "utils/byteStream_t.hpp"

#include <GL/glew.h>
#include <cstdint>
#include <limits>
#include <memory>

namespace el3D
{
namespace OpenGL
{
struct textureProperties_t
{
    std::string identifier;
    GLint       internalFormat = GL_RGBA8;
    GLenum      format         = GL_RGBA;
    GLenum      type           = GL_FLOAT;
    GLenum      attachment     = GL_COLOR_ATTACHMENT0;
    GLint       minFilter      = GL_LINEAR;
    GLint       magFilter      = GL_LINEAR;
    GLint       wrapS          = GL_REPEAT;
    GLint       wrapT          = GL_REPEAT;
    GLint       wrapR          = GL_REPEAT;
    GLint       compareMode    = GL_NONE;
    GLint       compareFunc    = GL_LEQUAL;
};

class elTexture_base
{
  public:
    struct texSize_t
    {
        uint64_t x           = 0;
        uint64_t y           = 0;
        uint64_t z           = 0;
        uint64_t arraySize   = 0;
        uint64_t sizeInBytes = 0;
    };

    elTexture_base( const GLuint id, const GLenum target ) noexcept;
    elTexture_base( elTexture_base&& ) noexcept;
    elTexture_base( const elTexture_base& ) = delete;
    virtual ~elTexture_base();

    elTexture_base&
    operator=( elTexture_base&& ) noexcept;
    elTexture_base&
    operator=( const elTexture_base& ) = delete;

    static std::vector< utils::byte_t >
    GenerateRandomTexture( const size_t size ) noexcept;

    void
    Reshape() const noexcept;

    void
    GenerateMipMap() const noexcept;
    const elTexture_base*
    Bind() const noexcept;
    const elTexture_base*
    BindAndActivate( const GLenum textureUnit ) const noexcept;
    const elTexture_base*
    BindActivateAndSetUniform( const GLint  uniform,
                               const GLenum textureUnit ) const noexcept;
    const elTexture_base*
    Unbind() const noexcept;
    const elTexture_base*
    TexParameteri( const GLenum entry, const GLint value ) const noexcept;
    const elTexture_base*
    TexParameterf( const GLenum entry, const GLfloat value ) const noexcept;
    const elTexture_base*
    TexParameterfv( const GLenum entry, const GLfloat* value ) const noexcept;
    const elTexture_base*
    PixelStorei( const GLenum entry, const GLint value ) const noexcept;

    elTexture_base&
    SetFormat( const GLenum ) noexcept;
    elTexture_base&
    SetInternalFormat( const GLint ) noexcept;
    elTexture_base&
    SetType( const GLenum ) noexcept;
    elTexture_base&
    SetSize( const uint64_t x, const uint64_t y = 0, const uint64_t z = 0,
             const uint64_t arraySize = 0 ) noexcept;
    elTexture_base&
    SetWrap( const GLint wrapS, const GLint wrapT = GL_REPEAT,
             const GLint wrapR = GL_REPEAT ) noexcept;
    elTexture_base&
    SetCompareParameters( const GLint compareMode,
                          const GLint compareFunc = GL_LEQUAL ) noexcept;
    elTexture_base&
    SetMinMagFilter( const GLint minFilter, const GLint magFilter ) noexcept;

    texSize_t
    GetSize() const noexcept;
    GLenum
    GetType() const noexcept;
    GLint
    GetInternalFormat() const noexcept;
    GLenum
    GetFormat() const noexcept;
    GLuint
    GetID() const noexcept;
    GLenum
    GetTarget() const noexcept;

    std::unique_ptr< uint8_t[] >
    GetTexImage() const noexcept;

  protected:
    elTexture_base() noexcept;
    elTexture_base( const std::string& debugIdentifier,
                    const GLint internalFormat, const GLenum format,
                    const GLenum type, const GLenum target ) noexcept;

    void
    DeleteTexture() noexcept;

  protected:
    static constexpr GLuint INVALID_ID{ 0 };
    GLuint                  id{ INVALID_ID };
    GLenum                  target;
    texSize_t               size;
    GLenum                  format;
    GLint                   internalFormat;
    GLenum                  type;
    bool                    hasOwnerShip{ true };
};
} // namespace OpenGL
} // namespace el3D
