#pragma once

#include "GLAPI/elImage.hpp"
#include "OpenGL/elTexture_base.hpp"

namespace el3D
{
namespace OpenGL
{
class elTexture_2D : public elTexture_base
{
  public:
    explicit elTexture_2D( const std::string&    debugIdentifier,
                           const GLAPI::elImage& image ) noexcept;
    elTexture_2D( const std::string& debugIdentifier, const uint64_t sizeX = 1,
                  const uint64_t sizeY          = 1,
                  const GLint    internalFormat = GL_RGBA8,
                  const GLenum   format         = GL_RGBA,
                  const GLenum   type           = GL_UNSIGNED_BYTE ) noexcept;
    elTexture_2D( elTexture_2D&& ) noexcept;

    elTexture_2D&
    operator=( elTexture_2D&& ) noexcept;

    const elTexture_2D&
    TexImage( const GLvoid* pixels ) const noexcept;
    const elTexture_2D&
    TexSubImage( const GLint offsetX, const GLint offsetY, const uint64_t width,
                 const uint64_t height, const GLvoid* pixels ) const noexcept;

    template < typename T >
    const elTexture_2D&
    TexImage( const std::vector< T >& pixels ) const noexcept;
    template < typename T >
    const elTexture_2D&
    TexSubImage( const GLint offsetX, const GLint offsetY, const uint64_t width,
                 const uint64_t          height,
                 const std::vector< T >& pixels ) const noexcept;


  protected:
    elTexture_2D( const std::string& debugIdentifier, const uint64_t sizeX,
                  const uint64_t sizeY, const GLint internalFormat,
                  const GLenum format, const GLenum type,
                  const GLenum target ) noexcept;
};
} // namespace OpenGL
} // namespace el3D

#include "OpenGL/elTexture_2D.inl"
