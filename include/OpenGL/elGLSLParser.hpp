#pragma once

#include "DesignPattern/Creation.hpp"
#include "GLAPI/elWindow.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "utils/elFile.hpp"

#include <GL/glew.h>
#include <iostream>
#include <map>

namespace el3D
{
namespace OpenGL
{
enum class elGLSLParser_Error
{
    CouldNotFindShaderFile,
    UnableToReadShaderFile,
    UnableToReadShaderIncludeFile,
    SyntaxError,
    SearchPathIsNotADirectory,
};

/*!
    \code
        //__default__
        //!vertex
        ... shaderTypes defined here are the default values for all groups not
        ... defining this specific shaderType

        //!fragment
        ... fragment shader code

        ...

        //__groupName__
        //!vertex

        ... vertex shader code

        //!fragment

        ... fragment shader code

        //!geometry

        ... geometry shader code

        //!tess_evaluation

        ... tessellation evaluation shader code

        //!tess_control

        ... tessellation control shader code

        //!compute

        ... compute shader code


        //__anotherGroupName__
        //!compute
        ...

        //__multiGroup1,multiGroup2,multiGroup3__
        //!vertex
        ...
    \endcode
    \code
        try {
            elGLSLParser shader("path/to/shader.glsl");
            std::string vertexShader =
                shader.content["groupName"][GL_VERTEX_SHADER];

            ...

            elShaderProgram shader(elGLSLParser("path/to/shader.glsl"),
                "groupName");

            ...

        } catch(std::runtime_error &e) {
            std::cerr << "could not load shader\n";
        }
    \endcode
*/
class elGLSLParser
    : public DesignPattern::Creation< elGLSLParser, elGLSLParser_Error >
{
  public:
    struct shader_t
    {
        std::string code;
        int         version = -1;
    };

    struct groupType_t
    {
        std::string group;
        GLenum      type;

        bool
        operator<( const groupType_t& rhs ) const noexcept;
    };

    struct define_t
    {
        std::string name;
        std::string value;
    };

    using staticDefines_t = std::map< groupType_t, std::vector< define_t > >;

    elGLSLParser();
    const std::map< std::string, std::map< GLenum, shader_t > >&
    GetContent() const noexcept;

    static bb::elExpected< elGLSLParser_Error >
    AddShaderIncludePath( const std::string& path ) noexcept;

  private:
    elGLSLParser( const std::string& filePath, const int shaderVersion,
                  const staticDefines_t& defines = staticDefines_t() );
    int
    ExtractVersion( const std::string& line ) const noexcept;
    void
    FillWithDefaultGroup() noexcept;
    bb::elExpected< elGLSLParser_Error >
    ParseShader();
    bb::elExpected< GLenum, elGLSLParser_Error >
    ExtractShaderType( const std::string&, const int ) const;
    bb::elExpected< std::vector< std::string >, elGLSLParser_Error >
    ExtractSeparatedGroups( const std::string& ) const;
    bb::elExpected< std::vector< std::string >, elGLSLParser_Error >
    ExtractGroupNames( const std::string&, const int ) const;
    std::string
    ExtractShaderPath() const noexcept;
    bb::elExpected< elGLSLParser_Error >
    AddFileToShaderContent( const std::string&          fileName,
                            std::vector< std::string >& shaderContent,
                            const size_t insertPosition ) noexcept;

    friend class elShaderProgram;
    friend struct DesignPattern::Creation< elGLSLParser, elGLSLParser_Error >;

  private:
    static std::vector< std::string >                     searchPaths;
    std::map< std::string, std::map< GLenum, shader_t > > content;
    utils::elFile                                         shaderFile;
    int                                                   shaderVersion;
    staticDefines_t                                       defines;

    char        groupSeparator           = ',';
    std::string defaultGroupName         = "default";
    std::string groupIdentifierBegin     = "//__";
    std::string groupIdentifierEnd       = "__";
    std::string typeIdentifier           = "//!";
    std::string includeIdentifier        = "//<<";
    std::string vertexIdentifier         = "vertex";
    std::string fragmentIdentifier       = "fragment";
    std::string geometryIdentifier       = "geometry";
    std::string tessEvaluationIdentifier = "tess_evaluation";
    std::string tessControlIdentifier    = "tess_control";
    std::string computeIdentifier        = "compute";
};
} // namespace OpenGL
} // namespace el3D
