#pragma once

#include "buildingBlocks/elVoxelVolume.hpp"
#include "world/elModel.hpp"
#include "world/elWorld.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
class VoxelObject : public world::elWorld
{
  public:
    VoxelObject( _3Delch::elScene& scene ) noexcept;

    void
    Add( const glm::uvec3& position ) noexcept;

    world::elModel&
    Get( const glm::uvec3& position ) noexcept;
    const world::elModel&
    Get( const glm::uvec3& position ) const noexcept;

    void
    Remove( const glm::uvec3& position ) noexcept;
    bool
    DoesContainElementAt( const glm::uvec3& position ) const noexcept;

  private:
    bb::elVoxelVolume< bb::product_ptr< world::elModel > > object;
};
} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D
