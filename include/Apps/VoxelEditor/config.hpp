#pragma once

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
constexpr float NUMBER_OF_VOXELS_PER_UNIT = 10.0f;
}
} // namespace Apps
} // namespace el3D
