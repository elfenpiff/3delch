#pragma once

#include "world/elCoordinateSystem.hpp"
#include "world/elInteractiveSet.hpp"
#include "world/elWorld.hpp"
#include "world/types.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
struct CoordinatesProperties
{
    uint64_t        voxelResolution = 10;
    world::color4_t gridColor       = { 0x00, 0x00, 0x00, 0x1a };
    world::color4_t highlightColor  = { 0x00, 0x00, 0x00, 0x44 };
};

struct CoordinatesCallbacks
{
    std::function< void( glm::uvec3 ) > leftClick;
};

class Coordinates : public world::elWorld
{
  public:
    Coordinates( _3Delch::elScene& scene, const CoordinatesProperties& p,
                 const CoordinatesCallbacks& callbacks ) noexcept;

    void
    EnableHighlight() noexcept;
    void
    DisableHighlight() noexcept;
    void
    ToggleHighlight() noexcept;

    void
    IncreaseHighlightLevel() noexcept;
    void
    DecreaseHighlightLevel() noexcept;

    glm::vec3
    GetCenter() const noexcept;

  private:
    void
    CreateHighlight() noexcept;
    void
    UpdateHighlightPosition() noexcept;

  private:
    static constexpr float LEVEL_VALUE            = 0.1f;
    static constexpr float ANIMATION_SPEED        = 3.0f;
    static constexpr float HIGHLIGHT_LEVEL_OFFSET = 0.001f;

    CoordinatesProperties properties;
    CoordinatesCallbacks  callbacks;
    float                 gridSize = 1.0f;

    bb::product_ptr< world::elCoordinateSystem > x;
    bb::product_ptr< world::elCoordinateSystem > y;
    bb::product_ptr< world::elCoordinateSystem > z;
    bb::product_ptr< world::elCoordinateSystem > highlight;
    bb::product_ptr< world::elInteractiveSet >   highlightInteraction;
    glm::vec3                                    highlightPosition;
};
} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D
