#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Mass : public BasicUnit< Mass >
{
  public:
    Mass() noexcept;

    static Mass
    Gramm( const float_t value ) noexcept;
    static Mass
    Kilogramm( const float_t value ) noexcept;

    float_t
    GetGramm() const noexcept;
    float_t
    GetKilogramm() const noexcept;

  private:
    Mass( const float_t kilogramm ) noexcept;
};
namespace literals
{
Mass operator""_g( unsigned long long ) noexcept;
Mass operator""_g( long double ) noexcept;
Mass operator""_kg( unsigned long long ) noexcept;
Mass operator""_kg( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Mass& v ) noexcept;
} // namespace units
} // namespace el3D
