#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Acceleration : public BasicUnit< Acceleration >
{
  public:
    Acceleration() noexcept;

    static Acceleration
    MeterPerSecondSquare( const float_t value ) noexcept;

    float_t
    GetMeterPerSecondSquare() const noexcept;

  private:
    Acceleration( const float_t meterPerSecondSquare ) noexcept;
};
namespace literals
{
Acceleration operator""_m_s2( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Acceleration& a ) noexcept;
} // namespace units
} // namespace el3D
