#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Length : public BasicUnit< Length >
{
  public:
    Length() noexcept;

    static Length
    Meter( const float_t value ) noexcept;

    float_t
    GetMeter() const noexcept;

  private:
    Length( const float_t meter ) noexcept;
};
namespace literals
{
Length operator""_m( unsigned long long ) noexcept;
Length operator""_m( long double ) noexcept;
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Length& l ) noexcept;
} // namespace units
} // namespace el3D
