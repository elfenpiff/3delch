#pragma once

#include "units/BasicUnit.hpp"

#include <chrono>
#include <cstdint>
#include <iostream>
#include <time.h>
#ifdef _WIN32
#include <Winsock2.h> // required for struct timeval
#endif

namespace el3D
{
namespace units
{
class Time : public BasicUnit< Time >
{
  public:
    Time() noexcept;
    static Time
    TimeSinceEpoch() noexcept;

    static Time
    NanoSeconds( const float_t ) noexcept;
    static Time
    MicroSeconds( const float_t ) noexcept;
    static Time
    MilliSeconds( const float_t ) noexcept;
    static Time
    Seconds( const float_t ) noexcept;
    static Time
    Minutes( const float_t ) noexcept;
    static Time
    Hours( const float_t ) noexcept;
    static Time
    Days( const float_t ) noexcept;

    float_t
    GetNanoSeconds() const noexcept;
    float_t
    GetMicroSeconds() const noexcept;
    float_t
    GetMilliSeconds() const noexcept;
    float_t
    GetSeconds() const noexcept;
    float_t
    GetMinutes() const noexcept;
    float_t
    GetHours() const noexcept;
    float_t
    GetDays() const noexcept;

    operator struct timeval() const noexcept;
    operator struct timespec() const noexcept;
    operator std::chrono::duration< double >() const noexcept;

  private:
    Time( const float_t seconds ) noexcept;
};
namespace literals
{
units::Time operator"" _ns( unsigned long long ) noexcept;
units::Time operator"" _us( unsigned long long ) noexcept;
units::Time operator"" _ms( unsigned long long ) noexcept;
units::Time operator"" _s( unsigned long long ) noexcept;
units::Time operator"" _min( unsigned long long ) noexcept;
units::Time operator"" _h( unsigned long long ) noexcept;
units::Time operator"" _d( unsigned long long ) noexcept;

units::Time operator"" _ns( long double ) noexcept;
units::Time operator"" _us( long double ) noexcept;
units::Time operator"" _ms( long double ) noexcept;
units::Time operator"" _s( long double ) noexcept;
units::Time operator"" _min( long double ) noexcept;
units::Time operator"" _h( long double ) noexcept;
units::Time operator"" _d( long double ) noexcept;

} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Time& t ) noexcept;
} // namespace units
} // namespace el3D
