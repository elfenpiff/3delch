#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Confidence : public BasicUnit< Confidence >
{
  public:
    /// constructor is public since confidence does not have any kind of unit
    Confidence( const float_t value ) noexcept;
    Confidence() noexcept;

    float_t
    Get() const noexcept;
};

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Confidence& c ) noexcept;
} // namespace units
} // namespace el3D
