#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class DataSize : public BasicUnit< DataSize >
{
  public:
    DataSize() noexcept;

    static DataSize
    Bytes( const float_t value ) noexcept;
    static DataSize
    KiloBytes( const float_t value ) noexcept;
    static DataSize
    MegaBytes( const float_t value ) noexcept;
    static DataSize
    GigaBytes( const float_t value ) noexcept;

    float_t
    GetBytes() const noexcept;
    float_t
    GetKiloBytes() const noexcept;
    float_t
    GetMegaBytes() const noexcept;
    float_t
    GetGigaBytes() const noexcept;

  private:
    DataSize( const float_t value ) noexcept;
};
namespace literals
{
DataSize operator""_b( unsigned long long ) noexcept;
DataSize operator""_b( long double ) noexcept;
DataSize operator""_kb( unsigned long long ) noexcept;
DataSize operator""_kb( long double ) noexcept;
DataSize operator""_mb( unsigned long long ) noexcept;
DataSize operator""_mb( long double ) noexcept;
DataSize operator""_gb( unsigned long long ) noexcept;
DataSize operator""_gb( long double ) noexcept;
} // namespace literals
std::ostream&
operator<<( std::ostream& stream, const DataSize& l ) noexcept;
} // namespace units
} // namespace el3D

