#pragma once

#include "units/BasicUnit.hpp"

#include <iostream>

namespace el3D
{
namespace units
{
class Probability : public BasicUnit< Probability >
{
  public:
    Probability() noexcept;

    static Probability
    Percent( const float_t value ) noexcept;
    static Probability
    ZeroToOneValue( const float_t value ) noexcept;

    float_t
    GetPercent() const noexcept;
    float_t
    GetZeroToOneValue() const noexcept;

  private:
    Probability( const float_t percent ) noexcept;
};

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Probability& c ) noexcept;
} // namespace units
} // namespace el3D
