#pragma once

#include "buildingBlocks/float_t.hpp"

#include <iomanip>
#include <sstream>

namespace el3D
{
namespace utils
{
class elByteSerializer;
class elByteDeserializer;
} // namespace utils

namespace units
{

template < typename T >
class BasicUnit
{
  public:
    using float_t = bb::float_t< 64 >;

    std::string
    ToString( const int precision ) const noexcept;

    T
    operator+( const T& rhs ) const noexcept;
    T
    operator-( const T& rhs ) const noexcept;

    BasicUnit< T >&
    operator+=( const T& rhs ) noexcept;
    BasicUnit< T >&
    operator-=( const T& rhs ) noexcept;

    bool
    operator<( const T& rhs ) const noexcept;
    bool
    operator<=( const T& rhs ) const noexcept;
    bool
    operator==( const T& rhs ) const noexcept;
    bool
    operator!=( const T& rhs ) const noexcept;
    bool
    operator>=( const T& rhs ) const noexcept;
    bool
    operator>( const T& rhs ) const noexcept;

    friend class utils::elByteSerializer;
    friend class utils::elByteDeserializer;

  protected:
    BasicUnit( const float_t value ) noexcept;

    float_t value{ 0 };
};
} // namespace units
} // namespace el3D

#include "units/BasicUnit.inl"
