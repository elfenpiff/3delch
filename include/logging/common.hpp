#pragma once

#ifdef _WIN32
#undef ERROR
#endif

#include <array>
#include <functional>
#include <string>

/**
 * @brief All elLog related things are managed in this namespace except the
 * class elLog itself.
 */
namespace el3D
{
namespace logging
{
/**
 * @brief All available loglevels. logLevel_t::END is a dummy element used to
 * get the number of logLevels.
 */
enum logLevel_t
{
    DEBUG = 0,
    INFO,
    WARN,
    ERROR,
    FATAL,
    END
};

/**
 * @brief Array which contains the names of all the loglevels so that the
 *      logLevel_t enum can be translated into a string easily.
 */
constexpr std::array< const char*, logLevel_t::END > logLevelToString{
    { "DEBUG", "INFO", "WARN", "ERROR", "FATAL" } };

using callback_t =
    std::function< void( const std::string& channelName, const logLevel_t,
                         const std::string& file, const std::string& line,
                         const std::string& func, const std::string& msg ) >;


} // namespace logging
} // namespace el3D
