#pragma once

#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "logging/CallbackHandler.hpp"
#include "logging/common.hpp"
#include "logging/policies/detail.hpp"
#include "logging/policies/format.hpp"
#include "logging/policies/output.hpp"
#include "logging/policies/threading.hpp"

#include <algorithm>
#include <array>
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

#define LOG_DEBUG( channel )                                                   \
    el3D::logging::elLog::Log( el3D::logging::DEBUG, __FILE__, __LINE__,       \
                               __func__, channel )
#define LOG_WARN( channel )                                                    \
    el3D::logging::elLog::Log( el3D::logging::WARN, __FILE__, __LINE__,        \
                               __func__, channel )
#define LOG_INFO( channel )                                                    \
    el3D::logging::elLog::Log( el3D::logging::INFO, __FILE__, __LINE__,        \
                               __func__, channel )
#define LOG_ERROR( channel )                                                   \
    el3D::logging::elLog::Log( el3D::logging::ERROR, __FILE__, __LINE__,       \
                               __func__, channel )
#define LOG_FATAL( channel )                                                   \
    el3D::logging::elLog::Log( el3D::logging::FATAL, __FILE__, __LINE__,       \
                               __func__, channel )
#define LOG( channel, logLevel )                                               \
    el3D::logging::elLog::Log( logLevel, __FILE__, __LINE__, __func__, channel )
#define LOG_EX( channel, msg )                                                 \
    el3D::logging::elLog::LogEx( __FILE__, __LINE__, __func__, msg, channel )

namespace el3D
{
namespace logging
{
/**
 * @brief This class provides you with the underlying
 *   LOG_{DEBUG, WARN, INFO, WARN, ERROR, FATAL, EX} function
 *  to log in your application. It provides multiple log message
 *  formatting modes, output modes like console, file and working modes
 *  like in separate thread, threadsafe or no thread safety at all.
 *
 * @code
 *    logging::output::ToTerminal<logging::threading::ThreadSafe> consoleLog;
 *    consoleLog.SetFormatting(logging::format::DefaultColored);
 *    consoleLog.SetLogLevel(logging::INFO);
 *
 *    logging::output::ToFile<logging::threading::InSeparateThread>
 *                  fileLog("file.log");
 *    fileLog.SetLogLevel(logging::DEBUG);
 *
 *    elLog::AddOutput(0, &consoleLog);
 *    elLog::AddOutput(0, fileLog);
 *
 *    ...
 *
 *    // this generates output at the console and in a file
 *    LOG_INFO(0) << "Some Info Message : " << 123;
 *    LOG_ERROR(0) << "Error!";
 *    throw std::runtime_error(LOG_EX(0, "errorMessage"));
 * @endcode
 */
class elLog
{
    /**
     * @brief Proxy object which is being return by elLog to
     *    support the mixing of loglevel argument and stream output
     */
    class Proxy
    {
      private:
        Proxy( const std::vector< output::Base * > *output,
               const CallbackHandler *callback, const std::string &channelName,
               const logLevel_t, const std::string &file, const int line,
               const std::string &func ) noexcept;

      public:
        ~Proxy();

        /**
         * @brief Prints s in the logger
         *
         * @tparam T must be a type which is convertable to string
         *            or a string
         * @param[in] s This will be printed by the logger
         * @return const Proxy& operator<< const
         */
        template < typename T >
        const Proxy &
        operator<<( const T &s ) const noexcept;

        friend class elLog;

      private:
        logLevel_t                           logLevel;
        mutable std::string                  msg;
        const std::vector< output::Base * > *output;
        const CallbackHandler *              callback;
        struct
        {
            std::string channelName;
            std::string file;
            std::string line;
            std::string func;
        } source;
    };

  public:
    static constexpr size_t DEFAULT_CHANNEL_ID     = 0;
    static constexpr char   DEFAULT_CHANNEL_NAME[] = "system";

    /**
     * @brief You should never call this method directly
     *          use LOG_{DEBUG, INFO, WARN, ERROR, FATAL} instead
     * @code
     *    LOG_DEBUG(0) << "some funky debug" << 123.0f;
     * @endcode
     *
     * @param[in] l
     * @param[in] file source file from which you are calling this function
     * @param[in] line source line from which you are calling
     * @param[in] func source function
     * @param[in] channel channelID which was returned by CreateChannel or 0
     *                  for the default channel
     * @return const Proxy Log after calling this function you can
     *              add log messages via <<
     */
    static const Proxy
    Log( const logLevel_t l, const std::string &file, const int line,
         const std::string &func, const size_t channel ) noexcept;

    /**
     * @brief You should never call this method directly
     *          use LOG_EX instead
     *        This method prints msg via the logger with loglevel FATAL.
     *        It should be used in combination with exceptions since it returns
     *        msg.
     * @code
     *    throw std::runtime_error(LOG_EX(0, "error!"));
     * @endcode
     *
     * @param[in] file source file from which you are calling this function
     * @param[in] line source line from which you are calling
     * @param[in] func source function
     * @param[in,out] msg
     * @param[in] channel channelID which was returned by CreateChannel or 0
     *                  for the default channel
     * @return std::string LogEx
     */
    static std::string
    LogEx( const std::string &file, const int line, const std::string &func,
           const std::string &msg, const size_t channel ) noexcept;

    /**
     * @brief Adds a new output target.
     *
     * @param[in] channel channelID which was returned by CreateChannel or 0
     *                  for the default channel
     * @param[in] l pointer of the output target
     */
    static void
    AddOutput( const size_t channel, output::Base *l );

    /**
     * @brief Removes an existing output target
     * @param[in] channel channelID which was returned by CreateChannel or 0
     *                  for the default channel
     * @param[in] l pointer of the output target
     */
    static void
    RemoveOutput( const size_t channel, output::Base *l );

    /**
     * @brief Redirects std::clog, std::cout and std::cerr
     *          to elLog
     */
    static void
    EnableOutputRedirection();

    /**
     * @brief Creates a channel and returns the unique id to the
     *          channel which is required when adding a new log output. If the
     *          channel already exists then it returns the id of the existing
     *          one.
     */
    static size_t
    CreateChannel( const std::string &name ) noexcept;

    /**
     * @brief Removes a channel
     */
    static void
    RemoveChannel( const size_t channel ) noexcept;

    /**
     * @brief Adds a callback to a specified channel.
     * @threadsafe
     *      AddCallback and RemoveCallback can be called concurrently.
     *      They cannot be called concurrently with any other method
     *      like Create/RemoveChannel or Add/RemoveOutput
     */
    static size_t
    AddCallback( const size_t channel, const callback_t &callback ) noexcept;

    /**
     * @brief Adds a callback to a specified channel.
     * @threadsafe
     *      AddCallback and RemoveCallback can be called concurrently.
     *      They cannot be called concurrently with any other method
     *      like Create/RemoveChannel or Add/RemoveOutput
     */
    static void
    RemoveCallback( const size_t channel, const size_t callbackID ) noexcept;


  private:
    struct channel_t
    {
        channel_t( const std::string &name ) noexcept;
        std::string                        name;
        std::vector< output::Base * >      output;
        std::shared_ptr< CallbackHandler > callbacks;
    };

    /**
     * @brief has by default 1 element the default channel
     */
    static bb::elUniqueIndexVector< channel_t > channels;
};
} // namespace logging
} // namespace el3D

#include "logging/LoggingRedirection.hpp"
#include "logging/elLog.inl"
