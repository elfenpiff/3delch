#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "logging/common.hpp"

#include <functional>
#include <mutex>

namespace el3D
{
namespace logging
{
class CallbackHandler
{
  public:
    size_t
    Add( const callback_t& callback ) noexcept;
    void
    Remove( const size_t callbackID ) noexcept;
    void
    Call( const std::string& channelName, const logLevel_t logLevel,
          const std::string& file, const std::string& line,
          const std::string& func, const std::string& msg ) const noexcept;

  private:
    bb::elUniqueIndexVector< callback_t > callbacks;
    mutable std::mutex                    mutex;
};
} // namespace logging
} // namespace el3D
