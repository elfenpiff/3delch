#pragma once

#include "logging/common.hpp"

#include <sstream>
#include <string>

namespace el3D
{
namespace logging
{
namespace redirection
{
class LogBuffer : public std::stringbuf
{
  public:
    LogBuffer( const logLevel_t logLevel, const std::string& source );
    virtual int
    sync() override;

  private:
    logLevel_t  logLevel;
    std::string source;
    std::string buffer;
};

class OutputRedirection
{
  public:
    OutputRedirection( std::ostream& out, std::streambuf* redirectTo );
    ~OutputRedirection();

  private:
    std::ostream&   out;
    std::streambuf* original;
};

} // namespace redirection
} // namespace logging
} // namespace el3D
