#pragma once

#include "buildingBlocks/elFiFo.hpp"
#include "concurrent/elPuPo.hpp"
#include "units/Time.hpp"

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>

namespace el3D
{
namespace logging
{
/**
 * @brief Contains all threading policies which can be used in combination
 *        with logging::output::** classes
 */
namespace threading
{

/**
 * @brief If you want to write your own threading policy than your class
 *        needs to be a child of Base
 */
class Base
{
  public:
    virtual void
    Call( const std::function< void() >& ) const = 0;
};

/**
 * @brief No threading, no thread safety
 */
class Disable : public Base
{
  public:
    void
    Call( const std::function< void() >& ) const override;
};

/**
 * @brief The logging will be performed in the same thread from which
 *        LOG_XX or LOG_EX was called. But it is threadsafe and you can
 *        call multiple LOG_XX instances at once.
 */
class ThreadSafe : public Base
{
  public:
    void
    Call( const std::function< void() >& ) const override;

  private:
    mutable std::mutex mtx;
};

/**
 * @brief The logging will be performed in a separate thread and
 *        LOG_XX and LOG_EX are threadsafe.
 */
class InSeparateThread : public Base
{
  public:
    InSeparateThread() noexcept;
    InSeparateThread( const InSeparateThread& ) = delete;
    InSeparateThread( InSeparateThread&& )      = delete;
    ~InSeparateThread();

    InSeparateThread&
    operator=( const InSeparateThread& ) = delete;
    InSeparateThread&
    operator=( InSeparateThread&& ) = delete;

    void
    Call( const std::function< void() >& ) const override;

  private:
    void
    BackgroundThread() const noexcept;

  private:
    mutable concurrent::elPuPo< bb::elFiFo< std::function< void() > > >
                callQueue;
    std::thread callThread;
    units::Time logClearTimeout = units::Time::MilliSeconds( 100.0f );
};

} // namespace threading
} // namespace logging
} // namespace el3D
