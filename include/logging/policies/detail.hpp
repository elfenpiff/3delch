#pragma once

#include <sstream>
#include <string>

namespace el3D
{
namespace logging
{
/**
 * @brief This namespace containes useful helper functions.
 *
 */
namespace detail
{
/**
 * @brief Converts any string convertable type to a string
 *
 * @tparam T
 * @param[in] msg
 * @return std::string toString
 */
template < typename T >
std::string
toString( const T &msg ) noexcept;

/**
 * @brief Returns the current unix time in milliseconds as a string
 *
 * @return std::string GetCurrentUnixTime
 */
std::string
GetCurrentUnixTime() noexcept;

} // namespace detail
} // namespace logging
} // namespace el3D

#include "logging/policies/detail.inl"
