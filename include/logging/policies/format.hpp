#pragma once

#include "logging/common.hpp"

#include <string>

namespace el3D
{
namespace logging
{
/**
 * @brief Contains all log message formatting functions which can
 *          be set in the logging::output::** via SetFormatting
 * @code
 *  logging::output::ToTerminal consoleLog;
 *  consoleLog.SetFormatting(logging::format::Short);
 *   ...
 * @endcode
 */
namespace format
{

/**
 * @brief Default formatting contains currentUnixTimeStamp, logLevel as string
 * and logmessage
 *
 * @param[in] logLevel
 * @param[in] msg
 * @return std::string Default
 */
std::string
Default( const std::string &channelName, const logLevel_t logLevel,
         const std::string &, const std::string &, const std::string &,
         const std::string &msg ) noexcept;

/**
 * @brief Default formatting with linux terminal colors
 *
 * @param[in] logLevel
 * @param[in] msg
 * @return std::string DefaultColored
 */
std::string
DefaultColored( const std::string &channelName, const logLevel_t logLevel,
                const std::string &, const std::string &, const std::string &,
                const std::string &msg ) noexcept;

/**
 * @brief All parameters and the currentUnixTimeStamp are printed
 *
 * @param[in] logLevel
 * @param[in] file
 * @param[in] line
 * @param[in] func
 * @param[in] msg
 * @return std::string Detailled
 */
std::string
Detailled( const std::string &channelName, const logLevel_t logLevel,
           const std::string &file, const std::string &line,
           const std::string &func, const std::string &msg ) noexcept;

/**
 * @brief Detailled formatting with linux terminal colors
 *
 * @param[in] logLevel
 * @param[in] file
 * @param[in] line
 * @param[in] func
 * @param[in] msg
 * @return std::string DetailledColored
 */
std::string
DetailledColored( const std::string &channelName, const logLevel_t logLevel,
                  const std::string &file, const std::string &line,
                  const std::string &func, const std::string &msg ) noexcept;

/**
 * @brief Prints only the log message
 *
 * @param[in] msg
 * @return std::string Minimal
 */
std::string
Minimal( const std::string &channelName, const logLevel_t, const std::string &,
         const std::string &, const std::string &,
         const std::string &msg ) noexcept;

/**
 * @brief Minimal formatting with colors. The message is colored depending on
 * the log level
 *
 * @param[in] logLevel
 * @param[in] msg
 * @return std::string MinimalColored
 */
std::string
MinimalColored( const std::string &channelName, const logLevel_t logLevel,
                const std::string &file, const std::string &line,
                const std::string &func, const std::string &msg ) noexcept;

/**
 * @brief Prints the source and the log message
 *
 * @param[in] msg
 * @param[in] func
 * @return std::string Short
 */
std::string
Short( const std::string &channelName, const logLevel_t loglevel,
       const std::string &file, const std::string &line,
       const std::string &func, const std::string &msg ) noexcept;

/**
 * @brief Short formatting with colors. The message is colored depending on the
 * log level
 *
 * @param[in] logLevel
 * @param[in] msg
 * @param[in] func
 * @return std::string ShortColored
 */
std::string
ShortColored( const std::string &channelName, const logLevel_t logLevel,
              const std::string &file, const std::string &line,
              const std::string &func, const std::string &msg ) noexcept;


} // namespace format
} // namespace logging
} // namespace el3D
