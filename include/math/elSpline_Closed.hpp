#pragma once

#include "math/elSpline_base.hpp"

namespace el3D
{
namespace math
{
template < typename T >
class elSpline_Closed : public elSpline_base< T >
{
  public:
    elSpline_Closed( const path_t< T >& path, const float factor ) noexcept;
};
} // namespace math
} // namespace el3D

#include "math/elSpline_Closed.inl"
