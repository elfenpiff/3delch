#pragma once

#include <array>
#include <glm/glm.hpp>

namespace el3D
{
namespace math
{
// Moeller Trumbore algorithm
// https://cadxfem.org/inf/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
bool
DoesRayIntersectTriangle( const glm::vec3 &                 rayOrigin,
                          const glm::vec3 &                 rayDirection,
                          const std::array< glm::vec3, 3 > &triangle ) noexcept;

bool
DoesRayIntersectTriangleWithCulling(
    const glm::vec3 &rayOrigin, const glm::vec3 &rayDirection,
    const std::array< glm::vec3, 3 > &triangle ) noexcept;
} // namespace math
} // namespace el3D
