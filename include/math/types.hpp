#pragma once

#include <algorithm>
#include <glm/glm.hpp>

namespace el3D
{
namespace math
{
template < typename T >
static constexpr bool IS_GLM_TYPE =
    std::is_same_v< T, glm::vec2 > || std::is_same_v< T, glm::vec3 > ||
    std::is_same_v< T, glm::vec4 >;

template < typename T >
using GLMType = typename std::enable_if_t< IS_GLM_TYPE< T >, T >;
template < typename T >
using NoGLMType = typename std::enable_if_t< !IS_GLM_TYPE< T >, T >;


} // namespace math
} // namespace el3D
