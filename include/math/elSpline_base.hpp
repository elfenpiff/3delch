#pragma once

#include "math/elCubicCurve.hpp"
#include "math/math.hpp"

#include <cstdint>
#include <glm/glm.hpp> // todo: remove
#include <vector>

namespace el3D
{
namespace math
{
template < typename T >
using path_t = std::vector< T >;

template < typename T >
class elSpline_base
{
  public:
    virtual T
    operator()( const float t ) const noexcept;
    float
    Length() const noexcept;

  protected:
    struct curve_t
    {
        elCubicCurve< T > value;
        float             length = 0.0f;
    };

    struct curveParam_t
    {
        uint64_t curveNumber = 0u;
        float    adjustedT   = 0.0f;
    };

  protected:
    elSpline_base() noexcept = default;

    void
    AddCurve( const elCubicCurve< T >& curve ) noexcept;
    T
    CalculateCatmullRomSplineTangent( const T& p0, const T& p2,
                                      const float t ) const noexcept;
    std::vector< T >
    GenerateTangents( const uint64_t pathSize, const path_t< T >& path,
                      const float factor, const T& tangentStart,
                      const T& tangentEnd ) const noexcept;
    void
    GenerateSpline( const uint64_t pathSize, const path_t< T >& path,
                    const std::vector< T >& tangents ) noexcept;

    curveParam_t
    GetCurveNumberAndAdjustedT( const float t ) const noexcept;

  protected:
    static constexpr uint64_t LENGTH_CALC_STEPS = 1000u;
    std::vector< curve_t >    curves;
    uint64_t                  size         = 0u;
    float                     splineLength = 0.0f;
};
} // namespace math
} // namespace el3D

#include "math/elSpline_base.inl"
