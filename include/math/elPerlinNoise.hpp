#pragma once

#include "math/functions.hpp"

#include <algorithm>
#include <cstdint>
#include <random>

namespace el3D
{
namespace math
{
class elPerlinNoise
{
  private:
    static constexpr uint64_t HASH_LENGTH = 512;
    uint8_t                   hash[HASH_LENGTH];

    static double
    Grad( const uint8_t hash, const double x, const double y,
          const double z ) noexcept;

  public:
    explicit elPerlinNoise(
        const uint32_t seed =
            std::default_random_engine::default_seed ) noexcept;

    /// @return interval [-1.0, 1.0]
    double
    Noise( const double x, const double y = 0.0,
           const double z = 0.0 ) const noexcept;


    /// @return unnormalized octave noise
    double
    AccumulatedOctaveNoise( const uint64_t octaves, const double x,
                            const double y = 0.0,
                            const double z = 0.0 ) const noexcept;

    double
    NormalizedOctaveNoise( const uint64_t octaves, const double x,
                           const double y = 0.0,
                           const double z = 0.0 ) const noexcept;

    static double
    Weight( const uint64_t octaves ) noexcept;
};
} // namespace math
} // namespace el3D
