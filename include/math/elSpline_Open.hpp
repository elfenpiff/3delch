#pragma once

#include "math/elSpline_base.hpp"

namespace el3D
{
namespace math
{
template < typename T >
class elSpline_Open : public elSpline_base< T >
{
  public:
    elSpline_Open( const path_t< T >& path, const float factor,
                   const T& tangentStart, const T& tangentEnd ) noexcept;
    T
    operator()( const float t ) const noexcept override;
};
} // namespace math
} // namespace el3D

#include "math/elSpline_Open.inl"
