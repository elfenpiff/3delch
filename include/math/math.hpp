#pragma once

#include "math/types.hpp"

namespace el3D
{
namespace math
{
template < typename T >
NoGLMType< T >
Distance( const T& p1, const T& p2 ) noexcept;

template < typename T >
typename GLMType< T >::value_type
Distance( const T& p1, const T& p2 ) noexcept;

template < typename T >
NoGLMType< T >
Normalize( const T& value ) noexcept;

template < typename T >
GLMType< T >
Normalize( const T& value ) noexcept;

glm::vec3
ClosestPoint( const glm::vec3 rayOrigin, const glm::vec3 rayDirection,
              const glm::vec3 point ) noexcept;

glm::vec3
Rotate( const glm::vec3& v, const glm::quat& q ) noexcept;

glm::vec3
Rotate( const glm::vec3& v, const glm::vec4 rotation ) noexcept;
} // namespace math
} // namespace el3D

#include "math/math.inl"
