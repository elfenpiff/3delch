#pragma once

#include "3Delch/elScene.hpp"
#include "GLAPI/elEvent.hpp"
#include "GuiGL/GuiGL.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Dropbutton.hpp"
#include "GuiGL/elWidget_Image.hpp"
#include "GuiGL/elWidget_KeyValuePair.hpp"
#include "GuiGL/elWidget_Listbox.hpp"
#include "GuiGL/elWidget_TabbedWindow.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "OpenGL/elObjectRegistry.hpp"
#include "utils/elSystemInfo.hpp"

namespace el3D
{
namespace _3Delch
{
class elIntegratedConsole
{
  public:
    elIntegratedConsole( elScene* const               scene,
                         GLAPI::elEventHandler* const eventHandler ) noexcept;
    elIntegratedConsole( const elIntegratedConsole& ) = delete;
    elIntegratedConsole( elIntegratedConsole&& )      = delete;

    elIntegratedConsole&
    operator=( const elIntegratedConsole& ) = delete;
    elIntegratedConsole&
    operator=( elIntegratedConsole&& ) = delete;

  private:
    void
    RefreshValues() noexcept;
    void
    CreateKeyValuePair( GuiGL::elWidget_KeyValuePair_ptr&       store,
                        const GuiGL::elWidget_KeyValuePair_ptr& previous,
                        const float                             valueOffset,
                        const std::string&                      key ) noexcept;

  private:
    static constexpr char CLASS_NAME[] = "IntegratedConsole";

    elScene*                          scene{ nullptr };
    GuiGL::elWidget_base*             parent{ nullptr };
    GuiGL::elWidget_base_ptr          mainWindow;
    glm::vec2                         mainWindowSize;
    utils::elSystemInfo               systemInfo;
    uint64_t                          systemInfoIteration = 0u;
    bool                              isConsoleActive{ false };
    bool                              hasStaticCamera{ false };
    bool                              hasMouseCursor{ false };
    bb::product_ptr< GLAPI::elEvent > showEvent;
    GLAPI::elEventHandler*            eventHandler{ nullptr };

    GuiGL::elWidget_Dropbutton_ptr selector;
    GuiGL::elWidget_Button_ptr     freeCamera;
    GuiGL::elWidget_Window*        activeWindow{ nullptr };
    struct general_t
    {
        static constexpr float           VALUE_OFFSET = 150.0f;
        GuiGL::elWidget_Window_ptr       window;
        GuiGL::elWidget_KeyValuePair_ptr fps;
        GuiGL::elWidget_KeyValuePair_ptr position;
        GuiGL::elWidget_KeyValuePair_ptr lookAt;
        GuiGL::elWidget_KeyValuePair_ptr zNearFar;
        GuiGL::elWidget_KeyValuePair_ptr fov;
        GuiGL::elWidget_KeyValuePair_ptr viewPortSize;
        GuiGL::elWidget_KeyValuePair_ptr dpiScaling;

        GuiGL::elWidget_KeyValuePair_ptr processID;
        GuiGL::elWidget_KeyValuePair_ptr threads;
        GuiGL::elWidget_KeyValuePair_ptr cpuUsage;
        GuiGL::elWidget_KeyValuePair_ptr memoryUsage;

        GuiGL::elWidget_KeyValuePair_ptr                processor;
        std::vector< GuiGL::elWidget_KeyValuePair_ptr > cores;

        GuiGL::elWidget_KeyValuePair_ptr memoryTotal;
        GuiGL::elWidget_KeyValuePair_ptr memoryUsed;
    } general;

    struct textures_t
    {
        GuiGL::elWidget_Window_ptr                 window;
        GuiGL::elWidget_Listbox_ptr                textureList;
        GuiGL::elWidget_Image_ptr                  preview;
        GuiGL::elWidget_base_ptr                   footer;
        GuiGL::elWidget_Label_ptr                  textureType;
        uint64_t                                   lastTextureUpdateToken{ 0u };
        std::vector< std::pair< GLuint, GLenum > > registeredTextures;
        OpenGL::elObjectRegistry::textureRegistry_t cache;
        std::optional< OpenGL::elTexture_base >     previewTexture;
    } textures;

    utils::CallbackGuard callback;
};
} // namespace _3Delch
} // namespace el3D
