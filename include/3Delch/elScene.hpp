#pragma once

#include "3Delch/elScene_internal.hpp"
#include "GLAPI/elEventHandler.hpp"
#include "GLAPI/elFontCache.hpp"
#include "HighGL/elCameraController_base.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elMeshLoader.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elSkybox.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/types.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "RenderPipeline/elRenderer_Deferred.hpp"
#include "RenderPipeline/elRenderer_Forward.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"
#include "buildingBlocks/elGenericFactory.hpp"
#include "utils/elGenericRAII.hpp"

#include <memory>
#include <string>
#include <utility>

namespace el3D
{
namespace lua
{
class elConfigHandler;
}
namespace world
{
class elObject;
class elWorld;
} // namespace world

namespace _3Delch
{

class elScene
{
  public:
    template < typename T >
    using objectPtr = std::unique_ptr< T, std::function< void( T* ) > >;

    elScene( const std::string& name, const lua::elConfigHandler* const config,
             GLAPI::elWindow* const                         window,
             RenderPipeline::elRenderPipelineManager* const pipelineManager,
             GLAPI::elFontCache* const fontCache ) noexcept;

    const std::string&
    GetSceneName() const noexcept;

    const OpenGL::elCamera_Perspective&
    GetCamera() const noexcept;
    OpenGL::elCamera_Perspective&
    GetCamera() noexcept;

    bool
    SetupFreeFlyCamera() noexcept;
    bool
    SetupOrbitCamera( const glm::vec3 center, const float distance = 1.0f,
                      const glm::vec2& angle = glm::vec2( 0.0f ) ) noexcept;

    void
    EnableInteractiveCamera() noexcept;
    void
    DisableInteractiveCamera() noexcept;

    bool
    HasStaticCamera() const noexcept;


    uint32_t
    GetCurrentEventColorIndex() const noexcept;
    bb::product_ptr< HighGL::elEventColor >
    AcquireEventColor() noexcept;
    bb::product_ptr< HighGL::elEventColorRange >
    AcquireEventColorRange( const uint32_t size ) noexcept;

    RenderPipeline::elRenderer_GUI*
    GUI() noexcept;

    size_t
    RegisterShaderTexture( const HighGL::elShaderTexture* const v ) noexcept;

    void
    DeregisterShaderTexture( const size_t id ) noexcept;

    void
    UseSkyboxForReflections( const bool v ) noexcept;

    void
    SetLengthOfOneMeter( const float v ) noexcept;

    friend class world::elObject;
    friend class world::elWorld;

  private:
    bb::product_ptr< HighGL::elLight_PointLight >
    CreatePointLight( const glm::vec3& color, const glm::vec3& attenuation,
                      const glm::vec3& position, const float diffuseIntensity,
                      const float ambientIntensity ) noexcept;

    bb::product_ptr< HighGL::elLight_DirectionalLight >
    CreateDirectionalLight( const glm::vec3& color, const glm::vec3& direction,
                            const GLfloat diffuseIntensity,
                            const GLfloat ambientIntensity ) noexcept;

    template < OpenGL::RenderTarget Target, typename T, typename... Targs >
    typename ObjectSettings< static_cast< int >( Target ), T >::Type
    CreateCustomObject( Targs&&... args ) noexcept;

    template < typename... Targs >
    bb::elExpected< bb::product_ptr< HighGL::elSkybox >,
                    HighGL::elSkybox_Error >
    CreateSkybox( Targs&&... args ) noexcept;

    struct shaderSource_t
    {
        std::string file;
        std::string group;
    };

    template < typename T, typename... Targs >
    bool
    SetupCameraController( const std::string& source,
                           Targs&&... args ) noexcept;

  private:
    std::string                              sceneName;
    const lua::elConfigHandler*              config{ nullptr };
    RenderPipeline::elRenderPipelineManager* pipelineManager;
    RenderPipeline::elRenderPipeline*        pipeline;
    size_t                                   pipelineID;
    utils::elGenericRAII                     cleanupPipeline;
    GLAPI::elEventHandler*                   eventHandler;

    bb::product_ptr< GLAPI::elEvent >                  cameraEvent;
    std::unique_ptr< HighGL::elCameraController_base > cameraController;
    struct cameraControllerState_t
    {
        bool isInRotationMode = false;
    } cameraControllerState;

    struct forwardRenderer_t
    {
        RenderPipeline::elRenderer_Forward* renderer;
    };

    struct deferredRenderer_t
    {
        RenderPipeline::elRenderer_Deferred* renderer;
    };

    struct
    {
        std::unique_ptr< OpenGL::elShaderProgram > shader;
    } guiRenderer;

    using renderTypeList_t =
        std::tuple< forwardRenderer_t, deferredRenderer_t >;

    renderTypeList_t renderer;

    // should be always last member / cleanup can access other members
    bb::elGenericFactory< HighGL::elSkybox > factory;

    template < OpenGL::RenderTarget Target >
    typename std::tuple_element< static_cast< int >( Target ),
                                 renderTypeList_t >::type&
    GetRenderer() noexcept;
};
} // namespace _3Delch
} // namespace el3D

#include "3Delch/elScene.inl"
