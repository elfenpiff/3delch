#pragma once

#include "RenderPipeline/elRenderer_Deferred.hpp"
#include "RenderPipeline/elRenderer_Forward.hpp"

namespace el3D
{
namespace _3Delch
{
template < int N, typename T = void >
struct ObjectSettings;

template < typename T >
struct ObjectSettings< 0, T >
{
    using Type = RenderPipeline::Forward::Object< T >;
};

template < typename T >
struct ObjectSettings< 1, T >
{
    using Type = RenderPipeline::Deferred::Object< T >;
};

} // namespace _3Delch
} // namespace el3D
