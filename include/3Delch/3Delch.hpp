#pragma once

#include "3Delch/elSceneManager.hpp"
#include "GLAPI/elFont.hpp"
#include "GLAPI/elFontCache.hpp"
#include "GLAPI/elWindow.hpp"
#include "GLAPI/sdlHelper.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elMeshLoader.hpp"
#include "HighGL/elSkybox.hpp"
#include "OpenGL/elObjectRegistry.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "buildingBlocks/elFiFo.hpp"
#include "concurrent/elPuPo.hpp"
#include "lua/elConfigHandler.hpp"
#include "utils/elCallbackVector.hpp"

#include <memory>
#include <string>

namespace el3D
{
namespace HighGL
{
class elCamera;
} // namespace HighGL

namespace GLAPI
{
class elFont;
}

namespace GuiGL
{
class elWidget_MouseCursor;
}

namespace _3Delch
{
enum class _3Delch_Error
{
    UnableToOpenConfigFile,
    UnableToCreateWindow,
    UnableToCreateGeometryShader,
    UnableToCreateDirectionalLightShader,
    UnableToCreatePointLightShader,
    UnableToCreatePassthroughShader,
    UnableToCreateGuiShader,
    UnableToCreateFinalShader,
    UnableToInitializeUnifiedVertexObject
};

_3Delch&
Init( const std::string& configFile ) noexcept;
_3Delch&
GetInstance() noexcept;
const lua::elConfigHandler&
GetConfig() noexcept;
bb::product_ptr< GLAPI::elEvent >
CreateEvent() noexcept;
bb::product_ptr< GLAPI::elEvent >
CreateEventFor( const uint32_t colorIndex ) noexcept;
bb::product_ptr< GLAPI::elEvent >
CreateKeyPressEvent( const GLAPI::KeyState keyState, const SDL_Keycode keyCode,
                     const std::function< void() >& action ) noexcept;
bb::product_ptr< GLAPI::elEvent >
CreateAndRegisterKeyPressEvent(
    const GLAPI::KeyState keyState, const SDL_Keycode keyCode,
    const std::function< void() >& action ) noexcept;


float
GetMultiSamplingFactor() noexcept;
utils::CallbackGuard
CreateMainLoopCallback( const std::function< void() >& callback ) noexcept;
units::Time
GetRuntime() noexcept;
units::Time
GetDeltaTime() noexcept;


class _3Delch
{
  public:
    class Startup_e : public std::runtime_error
    {
      public:
        explicit Startup_e( const _3Delch_Error e );
        virtual ~Startup_e();
        _3Delch_Error errorValue;
    };

    _3Delch( const _3Delch& ) = delete;
    _3Delch( _3Delch&& )      = delete;

    _3Delch&
    operator=( const _3Delch& ) = delete;
    _3Delch&
    operator=( _3Delch&& ) = delete;
    ~_3Delch();

    GLAPI::elWindow*
    Window() noexcept;

    elSceneManager&
    GetSceneManager() noexcept;

    int
    GetShaderVersion() const noexcept;

    void
    StartMainLoop() noexcept;

    void
    StopMainLoopAndDestroy() noexcept;

    glm::ivec2
    GetRenderSize() const noexcept;

    glm::ivec2
    GetWindowSize() const noexcept;

    void
    SetMultiSamplingFactor( const float ) noexcept;

    float
    GetMultiSamplingFactor() const noexcept;

    void
    AddOpenGLThreadTask( const std::function< void() >& task ) noexcept;

    utils::CallbackGuard
    CreateMainLoopCallback( const std::function< void() >& callback ) noexcept;

    friend _3Delch&
    Init( const std::string& ) noexcept;
    friend const lua::elConfigHandler&
    GetConfig() noexcept;
    friend bb::product_ptr< GLAPI::elEvent >
    CreateEvent() noexcept;

  private:
    _3Delch( const std::string& configFile );

    int
    SetInstance();

    int
    LogInitSetter() noexcept;

    int
    LogFinalSetter() noexcept;

    uint32_t
    MainLoopCallback() noexcept;

    float
    SetDPIScaling( const float v ) noexcept;

    std::string
    GetRendererLookupDirectory() const noexcept;

    int
    SetupShaderIncludePath() noexcept;

  private:
    static constexpr float DOTS_PER_DPI_SCALING = 160.0f;
    int                    instanceSetter;
    logging::output::ToTerminal< logging::threading::Disable > terminalLog;
    int                                                        logInitSetter;
    lua::elConfigHandler                                       config;
    std::string rendererLookupDirectory;
    logging::output::ToFile< logging::threading::Disable > fileLog;
    int                                                    logFinalSetter;
    int                                shaderIncludePathSetter;
    std::unique_ptr< GLAPI::elWindow > window;
    int                                shaderVersion;
    concurrent::elPuPo< bb::elFiFo< std::function< void() > > > openGLTasks;
    float multiSamplingFactor = 1.0f;

    utils::elCallbackVector mainLoopCallback;

    std::unique_ptr< elSceneManager > sceneManager;
};
} // namespace _3Delch
} // namespace el3D
