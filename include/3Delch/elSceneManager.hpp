#pragma once

#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/elWindow.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"

#include <memory>

namespace el3D
{
namespace lua
{
class elConfigHandler;
}
namespace _3Delch
{
class elSceneManager
{
  public:
    using sceneElement_t =
        bb::elUniqueIndexVector< std::unique_ptr< elScene > >::element_t;

    elSceneManager( GLAPI::elWindow* const      window,
                    lua::elConfigHandler* const config ) noexcept;
    ~elSceneManager();

    void
    MainLoop() noexcept;

    void
    Reshape( const uint64_t x, const uint64_t y ) noexcept;
    sceneElement_t
    CreateScene( const std::string& sceneName ) noexcept;
    void
    RemoveScene( const size_t index ) noexcept;
    void
    SetMainScene( const size_t index ) noexcept;
    const elScene&
    GetMainScene() const noexcept;
    void
    SetMultiSamplingFactor( const float v ) noexcept;
    float
    GetMultiSamplingFactor() const noexcept;

  private:
    GLAPI::elWindow*                      window;
    lua::elConfigHandler*                 config;
    std::unique_ptr< GLAPI::elFontCache > fontCache;
    std::optional< elIntegratedConsole >  integratedConsole;

    std::unique_ptr< RenderPipeline::elRenderPipelineManager > pipelineManager;
    bb::elUniqueIndexVector< std::unique_ptr< elScene > >      scenes;
    elScene* mainScene{ nullptr };
};
} // namespace _3Delch
} // namespace el3D
