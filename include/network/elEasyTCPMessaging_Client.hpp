#pragma once

#include "concurrent/elActiveObject.hpp"
#include "network/elNetworkSocket_Client.hpp"
#include "network/etm/Transmission_t.hpp"
#include "units/Time.hpp"
#include "utils/elByteDeserializer.hpp"

#include <atomic>
#include <cstdint>
#include <future>
#include <optional>
#include <string>

namespace el3D
{
namespace network
{
class elEasyTCPMessaging_Client
{
  public:
    struct identify_t
    {
        std::string serviceID;
        uint32_t    serviceVersion;
        uint32_t    protocolVersion;
    };

    elEasyTCPMessaging_Client() noexcept;

    bool
    IsServiceAvailableAt( const std::string &hostname,
                          const uint16_t     port ) const noexcept;
    std::optional< identify_t >
    GetServerIdentification( const std::string &hostname,
                             const uint16_t     port ) noexcept;

    std::shared_future< bool >
    Connect( const std::string &hostname, const uint16_t port ) noexcept;
    bool
    IsConnected() const noexcept;
    void
    Disconnect() noexcept;

    units::Time
    GetTimeout() const noexcept;
    void
    SetTimeout( const units::Time &timeout ) noexcept;

    template < typename T >
    bb::elExpected< elNetworkSocket_Error >
    Send( const uint64_t transmissionID, const etm::MessageType messageType,
          const T &message ) noexcept;

    template < typename T >
    std::optional< etm::Transmission_t< T > >
    TryReceive() noexcept;

    template < typename T >
    std::optional< etm::Transmission_t< T > >
    TimedReceive( const units::Time &timeout ) noexcept;

    template < typename T >
    std::optional< etm::Transmission_t< T > >
    BlockingReceive() noexcept;

  private:
    bool
    ConnectImplementation( const std::string &hostname,
                           const uint16_t     port ) noexcept;

    template < typename T >
    std::optional< etm::Transmission_t< T > >
    InternalReceive(
        const std::function< std::optional< utils::byteStream_t >( uint64_t ) >
            &receiver ) noexcept;

  private:
    elNetworkSocket_Client     connection;
    concurrent::elActiveObject activeObject;
    units::Time                timeout = units::Time::Seconds( 2 );
    bool                       hasEstablishedPermanentConnection{ false };
    std::optional< utils::byteStream_t > currentPacketLength{ std::nullopt };
};
} // namespace network
} // namespace el3D

#include "network/elEasyTCPMessaging_Client.inl"
