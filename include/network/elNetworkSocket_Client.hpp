#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "concurrent/elPuPo.hpp"
#include "network/elNetworkSocket_Base.hpp"

#include <array>
#include <future>
#include <string>

#ifndef _WIN32
#include <netdb.h>
#endif


namespace el3D
{
namespace network
{
class elNetworkSocket_Client : public elNetworkSocket_Base
{
  public:
    using ipv4_address = std::array< uint8_t, 4 >;
    elNetworkSocket_Client();
    ~elNetworkSocket_Client();

    bool
    IsPortOpen( const std::string& hostname,
                const uint16_t     port ) const noexcept;
    std::shared_future< bb::elExpected< elNetworkSocket_Error > >
    Connect( const std::string& hostname, const uint16_t port );
    bb::elExpected< elNetworkSocket_Error >
    Disconnect();
    elNetworkSocket::internal::connection_t
    GetConnectionInformation() const noexcept;

    bb::elExpected< elNetworkSocket_Error >
    Send( const utils::byteStream_t& data );
    std::optional< utils::byteStream_t >
    BlockingReceive( const size_t numberOfBytes = 0 );
    std::optional< utils::byteStream_t >
    TryReceive( const size_t numberOfBytes = 0 );
    std::optional< utils::byteStream_t >
    TimedReceive( const units::Time& timeout, const size_t numberOfBytes = 0 );

  private:
    bb::elExpected< elNetworkSocket_Error >
    DisconnectImplementation( const bool pauseThread );
    void
    ListeningThread();
    bb::elExpected< elNetworkSocket_Error >
    ConnectImplementation( const std::string& hostname, const uint16_t port );

    bb::elExpected< std::shared_ptr< struct addrinfo >, elNetworkSocket_Error >
    GetAddressInfo( const std::string& hostname, const uint16_t port ) const;
    bb::elExpected< std::string, elNetworkSocket_Error >
    GetNameInfo( const struct addrinfo* const addressInfo,
                 const int                    flags ) const;

    elNetworkSocket::internal::buffer_t     inboxBuffer;
    elNetworkSocket::internal::connection_t connection;
    std::string                             hostname{ "" };
    uint16_t                                port{ 0 };
};
} // namespace network
} // namespace el3D
