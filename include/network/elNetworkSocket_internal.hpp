#pragma once

#include <cstdint>
#include <vector>
#ifndef _WIN32
#include <arpa/inet.h>
#else
#include <winsock2.h>
#include <ws2tcpip.h>
#endif
#include "buildingBlocks/elCircularBuffer.hpp"
#include "concurrent/elPuPo.hpp"
#include "utils/byteStream_t.hpp"

#include <array>
#include <memory>
#include <string>

namespace el3D
{
namespace network
{
#ifndef _WIN32
using socketHandleType_t = int;
#else
using socketHandleType_t = SOCKET;
#endif

namespace elNetworkSocket
{
namespace internal
{
static constexpr size_t BUFFER_SIZE = 10 * 1024 * 1024;
using buffer_t =
    concurrent::elPuPo< bb::elCircularBuffer< utils::byte_t, BUFFER_SIZE > >;

struct connection_t
{
    socketHandleType_t socket;
    uint64_t           connectionID;
    std::string        hostIP;
    std::string        hostName;
};

struct connectionBuffer_t
{
    connection_t                connection;
    std::shared_ptr< buffer_t > buffer;
};

struct message_t
{
    connection_t        connection;
    utils::byteStream_t message;
};
} // namespace internal
} // namespace elNetworkSocket
} // namespace network
} // namespace el3D
