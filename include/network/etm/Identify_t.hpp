#pragma once

#include "network/etm/etm.hpp"
#include "utils/Serializable.hpp"

#include <cstdint>
#include <string>

namespace el3D
{
namespace network
{
namespace etm
{
struct Identify_t
{
    struct Request : public utils::Serializable
    {
        uint32_t protocol_version = PROTOCOL_VERSION;

        uint64_t
        GetSerializedSize() const noexcept override;
        uint64_t
        Serialize( const utils::Endian  endianness,
                   utils::byteStream_t& byteStream,
                   const uint64_t byteStreamOffset ) const noexcept override;
        bb::elExpected< utils::ByteStreamError >
        Deserialize( const utils::Endian        endianness,
                     const utils::byteStream_t& byteStream,
                     const uint64_t byteStreamOffset ) noexcept override;
    };

    struct Response : public utils::Serializable
    {
        uint32_t    protocol_version = PROTOCOL_VERSION;
        std::string id;
        uint32_t    service_protocol_version;

        uint64_t
        GetSerializedSize() const noexcept override;
        uint64_t
        Serialize( const utils::Endian  endianness,
                   utils::byteStream_t& byteStream,
                   const uint64_t byteStreamOffset ) const noexcept override;
        bb::elExpected< utils::ByteStreamError >
        Deserialize( const utils::Endian        endianness,
                     const utils::byteStream_t& byteStream,
                     const uint64_t byteStreamOffset ) noexcept override;
    };
};
} // namespace etm
} // namespace network
} // namespace el3D
