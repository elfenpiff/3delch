#pragma once

#include "network/etm/Connect_t.hpp"
#include "network/etm/Identify_t.hpp"
#include "network/etm/etm.hpp"
#include "utils/Serializable.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

#include <cstdint>

namespace el3D
{
namespace network
{
namespace etm
{
template < typename T >
struct Request_t : public utils::Serializable
{
    uint32_t id{ 0 };
    T        payload;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};

template <>
struct Request_t< void > : public utils::Serializable
{
    uint32_t id{ 0 };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};

} // namespace etm
} // namespace network
} // namespace el3D

#include "network/etm/Request_t.inl"
