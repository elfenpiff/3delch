#pragma once

#include "logging/elLog.hpp"
#include "network/etm/etm.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

#include <cstdint>

namespace el3D
{
namespace network
{
namespace etm
{
template < typename T = void >
struct TransportType_t : public utils::Serializable
{
    uint32_t transport_type_id{ 0 };
    T        payload;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};

template <>
struct TransportType_t< void > : public utils::Serializable
{
    uint32_t transport_type_id{ 0 };

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;
};

template < typename T = void >
struct Transmission_t : public utils::Serializable
{
    mutable uint64_t     length{ 0 };
    uint64_t             transmission_id = 0u;
    TransportType_t< T > payload;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;

    Transmission_t() = default;
    Transmission_t( const uint64_t    transmission_id,
                    const MessageType transport_type_id ) noexcept;
    Transmission_t( const uint64_t    transmission_id,
                    const MessageType transport_type_id,
                    const T&          payload ) noexcept;
};

template <>
struct Transmission_t< void > : public utils::Serializable
{
    mutable uint64_t        length{ 0 };
    uint64_t                transmission_id = 0u;
    TransportType_t< void > payload;

    uint64_t
    GetSerializedSize() const noexcept override;
    uint64_t
    Serialize( const utils::Endian endianness, utils::byteStream_t& byteStream,
               const uint64_t byteStreamOffset ) const noexcept override;
    bb::elExpected< utils::ByteStreamError >
    Deserialize( const utils::Endian        endianness,
                 const utils::byteStream_t& byteStream,
                 const uint64_t byteStreamOffset ) noexcept override;

    Transmission_t() = default;
    Transmission_t( const uint64_t    transmission_id,
                    const MessageType transport_type_id ) noexcept;
};
} // namespace etm
} // namespace network
} // namespace el3D

#include "network/etm/Transmission_t.inl"
