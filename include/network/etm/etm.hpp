#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "utils/byteStream_t.hpp"

#include <cstdint>

namespace el3D
{
namespace network
{
namespace etm
{
enum class MessageType : uint32_t
{
    Error    = 0,
    End      = 1,
    Request  = 2,
    Response = 3,
    Stream   = 4
};

enum class RequestID : uint32_t
{
    Identify = 0,
    Connect  = 1
};

constexpr uint32_t PROTOCOL_VERSION = 0u;
} // namespace etm
} // namespace network
} // namespace el3D
