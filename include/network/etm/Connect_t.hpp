#pragma once

#include "network/etm/etm.hpp"
#include "utils/Serializable.hpp"

#include <cstdint>
#include <limits>

namespace el3D
{
namespace network
{
namespace etm
{
struct Connect_t
{
    static constexpr uint32_t DYNAMIC_CONNECTION_ID =
        std::numeric_limits< uint32_t >::max();
    static constexpr uint32_t INFINITE_RPC_TIMEOUT =
        std::numeric_limits< uint32_t >::max();

    struct Request : public utils::Serializable
    {
        uint32_t protocol_version        = PROTOCOL_VERSION;
        uint32_t connection_id           = DYNAMIC_CONNECTION_ID;
        uint32_t rpc_interval_timeout_ms = INFINITE_RPC_TIMEOUT;

        uint64_t
        GetSerializedSize() const noexcept override;
        uint64_t
        Serialize( const utils::Endian  endianness,
                   utils::byteStream_t& byteStream,
                   const uint64_t byteStreamOffset ) const noexcept override;
        bb::elExpected< utils::ByteStreamError >
        Deserialize( const utils::Endian        endianness,
                     const utils::byteStream_t& byteStream,
                     const uint64_t byteStreamOffset ) noexcept override;
    };

    struct Response : public utils::Serializable
    {
        uint32_t connection_id;
        uint16_t port;

        uint64_t
        GetSerializedSize() const noexcept override;
        uint64_t
        Serialize( const utils::Endian  endianness,
                   utils::byteStream_t& byteStream,
                   const uint64_t byteStreamOffset ) const noexcept override;
        bb::elExpected< utils::ByteStreamError >
        Deserialize( const utils::Endian        endianness,
                     const utils::byteStream_t& byteStream,
                     const uint64_t byteStreamOffset ) noexcept override;
    };
};

} // namespace etm
} // namespace network
} // namespace el3D
