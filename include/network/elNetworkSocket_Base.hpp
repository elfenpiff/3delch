#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "concurrent/elActiveObject.hpp"
#include "concurrent/elSmartLock.hpp"
#include "network/elNetworkSocket_internal.hpp"
#include "units/Time.hpp"
#include "utils/elFileMonitor.hpp"
#include "utils/elGenericRAII.hpp"

#ifndef _WIN32
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#else
#define WIN32_LEAN_AND_MEAN

#include <Mswsock.h>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment( lib, "Ws2_32.lib" )
#pragma comment( lib, "Mswsock.lib" )
#pragma comment( lib, "AdvApi32.lib" )
#endif


#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <utility>

namespace el3D
{
namespace network
{
enum class elNetworkSocket_Error
{
    UnableToReadSocket,
    UnableToCreateSocket,
    UnableToShutdownSocket,
    UnableToSendMessageToSocket,
    UnableToGetBoundedPortNumber,
    ConnectionRefused,
    ConnectionResetByPeer,
    ConnectionAborted,
    IsNotConnected,
    NameServerFailure,
    AddressFamilyNotSupported,
    AddressResolutionFailed,
    AddressAlreadyInUse,
    AddressIsProtected,
    MaximumNumberOfConnectionsExceeded,
    InternalFailure
};

class elNetworkSocket_Base
{
  protected:
#ifndef _WIN32
    static constexpr socketHandleType_t INVALID_SOCKET = -1;
    static constexpr auto               SEND_ERROR     = -1;
#else
    static constexpr auto SEND_ERROR = SOCKET_ERROR;
#endif

  public:
    elNetworkSocket_Base();
    virtual ~elNetworkSocket_Base();

    elNetworkSocket_Base( const elNetworkSocket_Base& ) = delete;
    /// deleted since it has active object behavior
    elNetworkSocket_Base( elNetworkSocket_Base&& rhs ) = delete;

    elNetworkSocket_Base&
    operator=( const elNetworkSocket_Base& ) = delete;
    /// deleted since it has active object behavior
    elNetworkSocket_Base&
    operator=( elNetworkSocket_Base&& rhs ) = delete;

    bool
    IsConnected() const noexcept;

  protected:
    bb::elExpected< elNetworkSocket_Error >
    SendToSocket( const socketHandleType_t   socketDescriptor,
                  const utils::byteStream_t& data ) const noexcept;

    virtual bb::elExpected< elNetworkSocket_Error >
    Open();
    void
    Close( const socketHandleType_t fileDescriptor );
    bb::elExpected< elNetworkSocket_Error >
    Shutdown( const socketHandleType_t fileDescriptor );
    bb::elExpected< std::optional< utils::byteStream_t >,
                    elNetworkSocket_Error >
    Read( const socketHandleType_t fileDescriptor ) const noexcept;

    utils::elGenericRAII
    GetRAIIReceiveCounter();
    void
    EnableTCPNoDelay( const socketHandleType_t fileDescriptor ) const noexcept;


    socketHandleType_t         socketFileDescriptor{ INVALID_SOCKET };
    concurrent::elActiveObject activeObject;
    std::atomic_bool           doContinueListeningThreadCycle{ false };
    std::atomic_bool           isConnected{ false };
    concurrent::elSmartLock< utils::elFileMonitor > socketMonitor;

    size_t      eventQueueSize      = 10;
    units::Time monitorTimeout      = units::Time::MilliSeconds( 10 );
    size_t      maxSocketBufferSize = 1024;
};
} // namespace network
} // namespace el3D
