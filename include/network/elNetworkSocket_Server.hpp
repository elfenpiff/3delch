#pragma once

#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/elFiFo.hpp"
#include "network/elNetworkSocket_Base.hpp"
#include "network/elNetworkSocket_internal.hpp"

#include <cstdint>
#include <map>

namespace el3D
{
namespace network
{
class elNetworkSocket_Server : public elNetworkSocket_Base
{
  public:
    using connectCallback_t = std::function< void( const socketHandleType_t ) >;
    using receiveCallback_t = std::function< void( const socketHandleType_t ) >;

    elNetworkSocket_Server();
    ~elNetworkSocket_Server();

    bb::elExpected< uint16_t, elNetworkSocket_Error >
    Listen( const uint16_t port                      = 0,
            const bool     doAcceptOneConnectionOnly = false );
    void
    StopListen();
    std::vector< elNetworkSocket::internal::connection_t >
    GetConnections() const noexcept;
    std::optional< elNetworkSocket::internal::connection_t >
    GetConnectionInformation( const socketHandleType_t socket ) const noexcept;
    void
    CloseConnection( const socketHandleType_t socket );

    bb::elExpected< elNetworkSocket_Error >
    Send( const socketHandleType_t   destinationSocket,
          const utils::byteStream_t& message ) noexcept;
    std::optional< utils::byteStream_t >
    BlockingReceive( const socketHandleType_t handle,
                     const size_t             numberOfBytes = 0 );
    std::optional< utils::byteStream_t >
    TryReceive( const socketHandleType_t handle,
                const size_t             numberOfBytes = 0 );
    std::optional< utils::byteStream_t >
    TimedReceive( const socketHandleType_t handle, const units::Time timeout,
                  const size_t numberOfBytes = 0 );

    void
    SetOnConnectCallback( const connectCallback_t& callback ) noexcept;
    void
    SetOnReceiveCallback( const receiveCallback_t& callback ) noexcept;

  private:
    elNetworkSocket::internal::connection_t
    CreateConnectionInformation(
        const socketHandleType_t handle,
        const sockaddr_in&       clientAddress ) const noexcept;

    bb::elExpected< elNetworkSocket_Error >
    Open() override;
    bb::elExpected< elNetworkSocket_Error >
    SetSocketOptions( const socketHandleType_t fileDescriptor );

    void
    ListeningThread();

    bb::elExpected< elNetworkSocket_Error >
    AcceptIncomingConnection();
    bb::elExpected< elNetworkSocket_Error >
    ReceiveFromIncomingConnection( const socketHandleType_t socket );

  private:
    class ConnectionHandler_t
    {
      public:
        void
        Clear() noexcept;
        void
        Terminate() noexcept;
        void
        Add( const socketHandleType_t handle,
             const elNetworkSocket::internal::connection_t&
                 connection ) noexcept;
        void
        Remove( const socketHandleType_t handle ) noexcept;
        std::vector< elNetworkSocket::internal::connection_t >
        GetConnections() const noexcept;
        std::vector< network::socketHandleType_t >
        GetInactiveSockets() const noexcept;

        std::optional< elNetworkSocket::internal::connection_t >
        GetConnectionToSocket(
            const socketHandleType_t& handle ) const noexcept;

        void
        PushMessage( const socketHandleType_t   handle,
                     const utils::byteStream_t& byteStream ) noexcept;

        std::optional< std::shared_ptr< elNetworkSocket::internal::buffer_t > >
        GetBuffer( const socketHandleType_t handle ) noexcept;

      private:
        std::map< int, elNetworkSocket::internal::connectionBuffer_t >
                 connections;
        uint64_t connectionID{ 0 };
    };

    using messageFiFo_t = concurrent::elPuPo<
        bb::elFiFo< elNetworkSocket::internal::message_t > >;

    concurrent::elSmartLock< ConnectionHandler_t > connectionHandler;
    connectCallback_t                              onConnectCallback;
    receiveCallback_t                              onReceiveCallback;

    bool     doAcceptOneConnectionOnly{ false };
    int      backlog       = 5;
    uint16_t listeningPort = 0;
};
} // namespace network
} // namespace el3D
