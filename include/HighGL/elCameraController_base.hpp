#pragma once

#include "OpenGL/elCamera_Perspective.hpp"

namespace el3D
{
namespace HighGL
{
enum class elCameraController
{
    Base,
    FreeFly,
    Orbit,
    StickToObject
};

class elCameraController_base
{
  public:
    elCameraController_base( const elCameraController      type,
                             OpenGL::elCamera_Perspective& camera ) noexcept;
    virtual ~elCameraController_base() = default;

    bool
    AttachToCamera( OpenGL::elCamera_Perspective& camera ) noexcept;
    bool
    IsAttachedToCamera() const noexcept;
    OpenGL::elCamera_Perspective::state_t
    GetState() noexcept;
    elCameraController
    GetType() const noexcept;

  protected:
    virtual void
    CalculateState() noexcept = 0;

  protected:
    elCameraController                    type = elCameraController::Base;
    OpenGL::elCamera_Perspective::state_t state;
    bool                                  hasUpdate              = false;
    bool                                  hasContinuousUpdates   = false;
    OpenGL::elCamera_Perspective*         camera                 = nullptr;
    const OpenGL::elCamera_Perspective::state_t*     cameraState = nullptr;
    OpenGL::elCamera_Perspective::stateLoader_t      stateLoader;
    OpenGL::elCamera_Perspective::stateLoaderGuard_t stateLoaderGuard;
};
} // namespace HighGL
} // namespace el3D
