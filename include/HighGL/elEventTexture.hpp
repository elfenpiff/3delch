#pragma once

#include "HighGL/elEventColor.hpp"
#include "HighGL/elEventColorRange.hpp"
#include "buildingBlocks/elGenericFactory.hpp"

#include <glm/glm.hpp>
#include <queue>

namespace el3D
{
namespace RenderPipeline
{
class elRenderPipelineManager;
}

namespace HighGL
{
class elEventTexture
{
  public:
    bb::product_ptr< elEventColor >
    CreateEventColor() noexcept;

    bb::product_ptr< elEventColorRange >
    CreateEventColorRange( const uint32_t size ) noexcept;

    elEventColor
    GetCurrentEventColor() const noexcept;
    friend class RenderPipeline::elRenderPipelineManager;

  private:
    struct range_t
    {
        uint32_t start = 0;
        uint32_t size  = 0;
    };

    void
    SetCurrentEventColor( const glm::vec4& ) noexcept;
    void
    FreeEventColor( const uint32_t ) noexcept;
    void
    FreeEventColorRange( const range_t& range ) noexcept;

  private:
    uint32_t               currentColor = 0;
    std::queue< uint32_t > freeColor;
    std::vector< range_t > freeRanges;
    elEventColor           currentEventColor;
    // should be always last member / cleanup can access other members
    bb::elGenericFactory< elEventColor, elEventColorRange > factory;
};
} // namespace HighGL
} // namespace el3D
