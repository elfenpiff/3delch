#pragma once

#include "utils/byteStream_t.hpp"

#include <GL/glew.h>
#include <string>

namespace el3D
{
namespace HighGL
{
class elTerrainGenerator
{
  public:
    struct heightmap_t
    {
        uint64_t       dimension;
        uint64_t       x;
        uint64_t       y;
        utils::byte_t* data;
    };

    struct geometry_t
    {
        std::vector< GLfloat > vertices;
        std::vector< GLuint >  elements;
        std::vector< GLfloat > textureCoordinates;
        std::vector< GLfloat > normals;
        std::vector< GLfloat > tangents;
        std::vector< GLfloat > bitangents;
    };

    struct settings_t
    {
        float    widthScaling    = 0.1f;
        float    heightScaling   = 0.003f;
        uint64_t normalGridRange = 5u;
    };

    elTerrainGenerator( const heightmap_t& heightmap,
                        const settings_t&  settings ) noexcept;

    const geometry_t&
    GetGeometry() const noexcept;

  private:
    geometry_t geometry;
};
} // namespace HighGL
} // namespace el3D
