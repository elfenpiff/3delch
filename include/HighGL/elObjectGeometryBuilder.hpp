#pragma once

#include "OpenGL/types.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elObjectGeometryBuilder
{
  public:
    static elObjectGeometryBuilder
    CreateCube() noexcept;
    static elObjectGeometryBuilder
    CreateScreenQuad() noexcept;
    static elObjectGeometryBuilder
    CreateQuad() noexcept;
    static elObjectGeometryBuilder
    CreatePyramid() noexcept;
    static elObjectGeometryBuilder
    CreateHexagon() noexcept;
    static elObjectGeometryBuilder
    CreateHexagonTube() noexcept;
    static elObjectGeometryBuilder
    CreateSphere( const uint64_t n ) noexcept;
    static elObjectGeometryBuilder
    CreateCylinder( const uint64_t n ) noexcept;
    static elObjectGeometryBuilder
    CreatePlane( const float width, const float height,
                 const uint64_t numberOfElementsWidth,
                 const uint64_t numberOfElementsHeight ) noexcept;

    elObjectGeometryBuilder&
    ApplyPosition( const glm::vec3& position ) noexcept;
    elObjectGeometryBuilder&
    ApplyScaling( const glm::vec3& scaling ) noexcept;

    const OpenGL::objectGeometry_t&
    GetObjectGeometry() const noexcept;
    OpenGL::objectGeometry_t
    GetInnerObjectGeometry() const noexcept;

  private:
    OpenGL::objectGeometry_t objectGeometry;
};
} // namespace HighGL
} // namespace el3D
