#pragma once

#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/types.hpp"

#include <GL/glew.h>
#include <array>
#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elLight_base
{
  public:
    elLight_base() = default;
    elLight_base( const glm::vec3 &color, const GLfloat diffuseIntensity,
                  const GLfloat ambientIntensity ) noexcept;
    elLight_base( const elLight_base & ) = delete;
    elLight_base( elLight_base && )      = default;
    virtual ~elLight_base();

    elLight_base &
    operator=( const elLight_base & ) = delete;
    elLight_base &
    operator=( elLight_base && ) = default;

    bool
    HasFog() const noexcept;
    void
    SetHasFog( const bool v ) noexcept;
    void
    SetFogIntensity( const float v ) noexcept;
    GLfloat
    GetFogIntensity() const noexcept;

    void
    SetColor( const glm::vec3 &c ) noexcept;
    glm::vec3
    GetColor() const noexcept;

    void
    SetCastShadow( const bool v ) noexcept;
    bool
    DoesCastShadow() const noexcept;

    void
    SetDiffuseIntensity( const GLfloat v );
    void
    SetAmbientIntensity( const GLfloat v );

    GLfloat
    GetDiffuseIntensity() const noexcept;
    GLfloat
    GetAmbientIntensity() const noexcept;

    bool
    HasUpdatedSettings() noexcept;

    void
    SettingsUpdated() noexcept;

    void
    ConnectGeometricLightObject(
        OpenGL::elGeometricObject_ModelMatrix &modelMatrix ) noexcept;

    void
    DisconnectGeometricLightObject() noexcept;

  protected:
    virtual void
    UpdateGeometricObjectPosition() noexcept = 0;

  protected:
    enum buffer_t
    {
        BUFFER_VERTEX = 0,
        BUFFER_ELEMENT,
    };

    glm::vec3 color = glm::vec3( 0.0f );

    OpenGL::elGeometricObject_ModelMatrix *geometricObject = nullptr;
    GLfloat                                diffuseIntensity;
    GLfloat                                ambientIntensity;
    bool                                   hasUpdatedSettings = true;
    bool                                   doesCastShadow     = true;
    bool                                   hasFog             = false;
    float                                  fogIntensity       = 1.0f;
};

} // namespace HighGL
} // namespace el3D
