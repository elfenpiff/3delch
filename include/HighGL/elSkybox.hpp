#pragma once


#include "DesignPattern/Creation.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "animation/elAnimatable.hpp"
#include "buildingBlocks/product_ptr.hpp"

#include <GL/glew.h>
#include <functional>
#include <glm/glm.hpp>
#include <memory>

namespace el3D
{
namespace HighGL
{
enum class elSkybox_Error
{
    UnableToOpenShaderFile,
    UnableToCreateShaderProgram,
    UnableToOpenTextureFile,
    UndefinedError
};

class elSkybox : public animation::elAnimatable< elSkybox >,
                 public DesignPattern::Creation< elSkybox, elSkybox_Error >
{
  public:
    struct textureConfig_t
    {
        std::string positiveXFile;
        std::string negativeXFile;
        std::string positiveYFile;
        std::string negativeYFile;
        std::string positiveZFile;
        std::string negativeZFile;
    };

    elSkybox()                  = delete;
    elSkybox( const elSkybox& ) = delete;
    elSkybox( elSkybox&& )      = default;
    ~elSkybox()                 = default;

    elSkybox&
    operator=( const elSkybox& ) = delete;
    elSkybox&
    operator=( elSkybox&& ) = default;

    OpenGL::elGeometricObject_VertexObject&
    GetVertexObject() noexcept;

    void
    SetSize( const float size ) noexcept;
    void
    SetColor( const glm::vec4& color ) noexcept;

    OpenGL::elTexture_CubeMap&
    GetTexture() noexcept;

    friend struct DesignPattern::Creation< elSkybox, elSkybox_Error >;

  private:
    using vertexObject_t =
        bb::product_ptr< OpenGL::elGeometricObject_VertexObject >;

    elSkybox( vertexObject_t&& vertexObject, uint64_t* const vertexShaderId,
              const std::function< glm::vec3() >& positionGetter,
              const std::string& shaderFile, const int shaderVersion,
              const std::string& shaderGroup ) noexcept;

    elSkybox( vertexObject_t&& vertexObject, uint64_t* const vertexShaderId,
              const std::function< glm::vec3() >& positionGetter,
              const std::string& shaderFile, const int shaderVersion,
              const std::string&     shaderGroup,
              const textureConfig_t& textures ) noexcept;
    elSkybox( vertexObject_t&& vertexObject, uint64_t* const vertexShaderId,
              const std::function< glm::vec3() >& positionGetter,
              const std::string& shaderFile, const int shaderVersion,
              const std::string& shaderGroup, const glm::vec4& color ) noexcept;

    bool
    InitVertexObject( const std::string& shaderFile, const int shaderVersion,
                      const std::string& shaderGroup ) noexcept;
    void
    InitCubeTexture( const uint64_t size, const GLvoid* const positiveXData,
                     const GLvoid* const negativeXData,
                     const GLvoid* const positiveYData,
                     const GLvoid* const negativeYData,
                     const GLvoid* const positiveZData,
                     const GLvoid* const negativeZData ) noexcept;

  private:
    float                                        size = 10.0f;
    std::function< glm::vec3() >                 positionGetter;
    vertexObject_t                               vertexObject;
    std::unique_ptr< OpenGL::elShaderProgram >   shader;
    bb::product_ptr< OpenGL::elTexture_CubeMap > texture;
    uint64_t*                                    shaderId = nullptr;
};
} // namespace HighGL
} // namespace el3D
