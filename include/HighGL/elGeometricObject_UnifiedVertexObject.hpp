#pragma once

#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elTextureBufferObject.hpp"
#include "OpenGL/elTexture_2D_Array.hpp"
#include "buildingBlocks/elGenericFactory.hpp"
#include "buildingBlocks/elUniqueIDHandler.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "buildingBlocks/product_ptr.hpp"
#include "utils/elCallbackVector.hpp"
#include "utils/elRunOnce.hpp"

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <memory>
#include <tuple>

namespace el3D
{
namespace HighGL
{
enum class elGeometricObject_UnifiedVertexObject_Error
{
    TextureArraySizeExceeded,
    Undefined
};

class elGeometricObject_UnifiedVertexObject;
namespace UnifiedVertexObject
{
using floatBuffer_t = std::vector< GLfloat >;
using uintBuffer_t  = std::vector< GLuint >;

enum class VertexElimination
{
    None,
    CloseVertices
};

enum floatBufferList_t : uint64_t
{
    VERTEX = 0,
    TEX_COORD,
    NORMAL,
    TANGENT,
    BITANGENT,
    DIFFUSE_COLOR,
    SPECULAR_COLOR,
    SHININESS_AND_ROUGHNESS,
    EMISSION_COLOR,
    EVENT_COLOR,
    FB_END
};
enum uintBufferList_t : uint64_t
{
    ELEMENTS = 0,
    TEX_DIFFUSE,
    TEX_SPECULAR_COLOR,
    TEX_SHININESS_AND_ROUGHNESS,
    TEX_NORMAL,
    TEX_EMISSION,
    UB_END
};

constexpr char DEFAULT_TEXTURE_TAG[] = "default";

class Texture
{
  public:
    Texture( const uint64_t id ) noexcept;

    bool
    operator==( const Texture& rhs ) const noexcept;
    bool
    operator!=( const Texture& rhs ) const noexcept;

    GLuint
    GetId() const noexcept;

  private:
    uint64_t id = 0u;
};

class Object : public OpenGL::elGeometricObject_ModelMatrix
{
  public:
    Object( elGeometricObject_UnifiedVertexObject* uvo, const uint64_t id,
            const std::string& name = "" ) noexcept;
    Object( elGeometricObject_UnifiedVertexObject* uvo, const uint64_t id,
            const floatBuffer_t& vertex,
            const floatBuffer_t& textureCoordinates,
            const floatBuffer_t& normals, const floatBuffer_t& tangents,
            const floatBuffer_t& bitangents, const uintBuffer_t& elements,
            const std::string& name = "" ) noexcept;

    Object( Object&& )      = delete;
    Object( const Object& ) = delete;
    ~Object()               = default;

    Object&
    operator=( Object&& ) = delete;
    Object&
    operator=( const Object& ) = delete;

    bb::product_ptr< Object >
    Clone() const noexcept;

    uint64_t
    GetNumberOfVertices() const noexcept;

    void
    SetFloatBuffer( const floatBufferList_t bufferType,
                    const floatBuffer_t&    buffer ) noexcept;

    void
    SetUintBuffer( const uintBufferList_t bufferType,
                   const uintBuffer_t&    buffer ) noexcept;

    const floatBuffer_t&
    GetFloatBuffer( const floatBufferList_t ) const noexcept;
    const uintBuffer_t&
    GetUintBuffer( const uintBufferList_t ) const noexcept;

    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    CreateTexture( const GLvoid* pixels ) noexcept;
    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    CreateTexture( const std::string& uniqueIdentifier,
                   const GLvoid*      pixels ) noexcept;
    void
    AttachTexture( const bb::shared_product_ptr< UnifiedVertexObject::Texture >&
                       texture ) noexcept;
    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    GetDefaultTexture() noexcept;

    utils::CallbackGuard
    SetPreDrawCallback( const std::function< void() >& f ) noexcept;

    void
    EliminateCloseVerticesInElements( const VertexElimination vertexElimination,
                                      const float minimumDistance ) noexcept;
    const std::string&
    GetName() const noexcept;

    std::vector< bb::shared_product_ptr< Texture > > textures;

    friend class ::el3D::HighGL::elGeometricObject_UnifiedVertexObject;

  private:
    static constexpr uint64_t MAT4_LENGTH{ 16u };

    void
    UpdateModelMatrix() const noexcept;

    static constexpr uint64_t INVALID_ID =
        std::numeric_limits< uint64_t >::max();

    elGeometricObject_UnifiedVertexObject* parent = nullptr;
    uint64_t                               id     = INVALID_ID;
    std::array< floatBuffer_t, FB_END >    floatBuffer;
    std::array< uintBuffer_t, UB_END >     uintBuffer;
    std::function< void() >                preDrawCallback;
    VertexElimination vertexElimination            = VertexElimination::None;
    float             vertexEliminationMinDistance = -1.0f;
    std::string       name;
};

} // namespace UnifiedVertexObject

class elGeometricObject_UnifiedVertexObject
    : public OpenGL::elGeometricObject_base
{
  public:
    static constexpr GLuint INVALID_TEXTURE_ID = 1024;

    elGeometricObject_UnifiedVertexObject(
        const OpenGL::DrawMode drawMode, const uint64_t sizeX,
        const uint64_t sizeY, const uint64_t texArraySize ) noexcept;
    elGeometricObject_UnifiedVertexObject(
        const OpenGL::DrawMode drawMode, const OpenGL::elShaderProgram* shader,
        const uint64_t sizeX, const uint64_t sizeY,
        const uint64_t texArraySize ) noexcept;

    elGeometricObject_UnifiedVertexObject(
        const elGeometricObject_UnifiedVertexObject& ) = delete;
    elGeometricObject_UnifiedVertexObject(
        elGeometricObject_UnifiedVertexObject&& ) = delete;
    elGeometricObject_UnifiedVertexObject&
    operator=( const elGeometricObject_UnifiedVertexObject& ) = delete;
    elGeometricObject_UnifiedVertexObject&
    operator=( elGeometricObject_UnifiedVertexObject&& ) = delete;

    void
    ResizeTextureArray( const uint64_t sizeX, const uint64_t sizeY,
                        const uint64_t arraySize ) noexcept;
    OpenGL::elTexture_base::texSize_t
    GetTextureArraySize() const noexcept;

    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    CreateTexture( const GLvoid* const pixels ) noexcept;

    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    CreateTexture( const std::string&  uniqueIdentifier,
                   const GLvoid* const pixels ) noexcept;

    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    CreateTexture( const std::string&                  uniqueIdentifier,
                   const std::vector< utils::byte_t >& pixels ) noexcept;

    template < typename... T >
    bb::product_ptr< UnifiedVertexObject::Object >
    CreateObject( const T&... t ) noexcept;

    bb::shared_product_ptr< UnifiedVertexObject::Texture >
    GetDefaultTexture() const noexcept;


    friend class UnifiedVertexObject::Object;

  protected:
    virtual void
    DrawImplementation(
        const uint64_t          shaderID,
        const OpenGL::elCamera& camera ) const noexcept override;
    void
    RunOncePerFrame() const noexcept;

    void
    RefreshFloatBufferData() noexcept;
    void
    RefreshUintBufferData() noexcept;
    void
    Bind( const size_t shaderID ) const noexcept;
    void
    Unbind( const size_t shaderID ) const noexcept;

    void
    Refresh() noexcept;
    void
    RefreshBufferData() noexcept;
    void
    RefreshUniformBufferData() noexcept;
    size_t
    GetBufferID(
        const enum UnifiedVertexObject::floatBufferList_t ) const noexcept;
    size_t
    GetBufferID(
        const enum UnifiedVertexObject::uintBufferList_t ) const noexcept;
    virtual bb::elExpected< size_t, OpenGL::elShaderProgram_Error >
    AttachShader( const uint64_t shaderId ) noexcept override;
    uint64_t
    AddToTextureArray( const GLvoid* const pixels ) noexcept;
    void
    UpdateObjectModelMatrix() const noexcept;

  protected:
    enum runOnce_t
    {
        _Refresh,
        _RefreshBufferData,
        _RefreshUniformBufferData
    };
    enum uniform_t
    {
        UNIFORM_BUFFER          = 0,
        UNIFORM_TEXTURE_ARRAY   = 1,
        UNIFORM_CAMERA_POSITION = 2,
        UNIFORM_VIEW_PROJECTION = 3,
    };
    mutable utils::elRunOnce runOnce;

    struct uniformBuffer_t
    {
        static constexpr uint64_t MATRIX_SIZE = 16u;

        std::string            name = "uniformBuffer";
        std::vector< GLfloat > data;
    } uniformBuffer;

    static constexpr uint8_t DEFAULT_TEXTURE_GREY = 0xBB;
    static const std::vector< OpenGL::elShaderProgram::shaderAttribute_t >
        ATTRIBUTES;

    OpenGL::elTextureBufferObject                          tbo;
    std::map< std::string, UnifiedVertexObject::Texture* > identifierToTexture;
    OpenGL::elTexture_2D_Array                             textureArray;
    bb::elUniqueIDHandler< uint64_t >                      textureIds;
    bb::elUniqueIDHandler< uint64_t >                      objectIds;

    // should be always last member / cleanup can access other members
    bb::elGenericFactory< UnifiedVertexObject::Object,
                          UnifiedVertexObject::Texture >
        factory;
    // has to come after factory since it cleans up on it
    bb::shared_product_ptr< UnifiedVertexObject::Texture > defaultTexture;
};
} // namespace HighGL
} // namespace el3D

#include "HighGL/elGeometricObject_UnifiedVertexObject.inl"
