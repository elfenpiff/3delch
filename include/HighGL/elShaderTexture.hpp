#pragma once

#include "DesignPattern/Creation.hpp"
#include "OpenGL/elFrameBufferObject.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elTexture_base.hpp"

#include <GL/glew.h>
#include <functional>
#include <vector>

namespace el3D
{
namespace HighGL
{
enum class elShaderTexture_Error
{
    ZeroOutputTextures,
    UnableToCreateShader,
    AttributeForCoordinatesNotFound,
    AttributeForTextureCoordinatesNotFound,
    UnableToFindBackbufferUniform
};

class elShaderTexture
    : public DesignPattern::Creation< elShaderTexture, elShaderTexture_Error >
{
  public:
    using uniformSetter_t =
        std::function< void( const OpenGL::elShaderProgram *, const GLint ) >;

    elShaderTexture( const elShaderTexture & ) = delete;
    elShaderTexture( elShaderTexture && )      = default;

    elShaderTexture &
    operator=( const elShaderTexture & ) = delete;
    elShaderTexture &
    operator=( elShaderTexture && ) = default;

    void
    Bind( const size_t n ) const noexcept;
    void
    Bind( const size_t n, const GLenum textureUnit ) const noexcept;
    void
    Bind( const size_t n, const GLint uniform,
          const GLenum textureUnit ) const noexcept;

    void
    SetPreDrawFunction( const std::function< void() > & ) noexcept;
    void
    SetPostDrawFunction( const std::function< void() > & ) noexcept;

    bool
    RegisterUniform( const std::string     &uniformName,
                     const uniformSetter_t &uniformSetter ) noexcept;
    bool
    HasUniform( const std::string &uniformName ) const noexcept;

    void
    Refresh() const noexcept;
    OpenGL::elShaderProgram &
    GetShader() noexcept;

    bool
    AddInputTexture( const std::string &uniform,
                     const OpenGL::elTexture_base * ) noexcept;
    void
    RemoveInputTexture( const OpenGL::elTexture_base * ) noexcept;
    bool
    AddInputShaderTexture( const std::string     &uniform,
                           const elShaderTexture *texture,
                           const size_t           textureID,
                           const bool             isFlipped = false ) noexcept;
    void
    SetDoClearBeforeDraw( const bool ) noexcept;
    OpenGL::elTexture_2D &
    GetOutputTexture( const size_t i ) noexcept;
    void
    SetSize( const uint64_t x, const uint64_t y ) noexcept;
    void
    SetBlending( const bool enable, const GLenum blendEquation,
                 const GLenum blendFuncSFactor,
                 const GLenum blendFuncDFactor ) noexcept;
    void
    SetDrawingRepetition( const size_t ) noexcept;

    friend struct DesignPattern::Creation< elShaderTexture,
                                           elShaderTexture_Error >;

  private:
    elShaderTexture( const std::vector< OpenGL::textureProperties_t >
                                        outputTextureProperties,
                     const std::string &fragmentShaderCode,
                     const GLsizei sizeX = 512, const GLsizei sizeY = 512,
                     const bool isFlipFlop               = false,
                     const bool enableTextureCoordinates = true );

    // CTor for flipFlopTexture
    elShaderTexture( OpenGL::elShaderProgram *shader,
                     const std::vector< OpenGL::textureProperties_t >
                                   outputTextureProperties,
                     const GLsizei sizeX, const GLsizei sizeY );
    void
    CreateOutputTextures() noexcept;
    void
    PrepareDraw() const noexcept;
    void
    DrawQuad() const noexcept;

  private:
    enum buffer_t
    {
        BUFFER_VERTEX = 0,
        BUFFER_ELEMENT,
        BUFFER_TEXTURE_COORDINATES,
        BUFFER_END
    };

    mutable bool doFlip = false;
    struct blending_t
    {
        bool   enable{ true };
        GLenum blendEquation{ GL_FUNC_ADD };
        GLenum blendFuncSFactor{ GL_SRC_ALPHA };
        GLenum blendFuncDFactor{ GL_ONE_MINUS_SRC_ALPHA };
    } blending;

    bool                                       doClearBeforeDraw{ true };
    size_t                                     drawingRepetition{ 1 };
    std::unique_ptr< elShaderTexture >         flipFlopTexture;
    OpenGL::elObjectBuffer                     objectBuffer;
    OpenGL::elFrameBufferObject                fbo;
    OpenGL::elShaderProgram                   *shader;
    std::unique_ptr< OpenGL::elShaderProgram > shaderInternal;
    OpenGL::elShaderProgram::shaderAttribute_t coordAttrib;
    OpenGL::elShaderProgram::shaderAttribute_t texCoordAttrib;
    std::vector< std::unique_ptr< OpenGL::elTexture_2D > > outputTextures;
    std::vector< GLint >                                   backbufferUniforms;

    struct texture_t
    {
        GLint                         uniformID;
        const OpenGL::elTexture_base *texture;
    };
    std::vector< texture_t > inputTextures;

    struct shaderTexture_t
    {
        GLint                  uniformID;
        const elShaderTexture *texture;
        const size_t           textureID;
        const bool             isFlipped;
    };
    std::vector< shaderTexture_t > inputShaderTextures;

    struct uniform_t
    {
        GLint           uniformID;
        uniformSetter_t setter;
    };
    std::vector< uniform_t > uniforms;

    std::function< void() > preDrawCall;
    std::function< void() > postDrawCall;
};
} // namespace HighGL
} // namespace el3D
