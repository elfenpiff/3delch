#pragma once

#include "DesignPattern/Creation.hpp"
#include "GLAPI/elImage.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "OpenGL/elModelMatrix.hpp"

#include <assimp/Importer.hpp>
#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <string>
#include <vector>

/*!
    \code
        elImage img("somePic.jpg");
        elGeometricObject_UnifiedVertexObject uvo(512, 512);
        uvo.SetTexture(0, img.GetRaw());

        elMeshLoader newMesh("someObject.obj");
        newMesh.LoadIntoUnifiedVertexObject(uvo, 1); // 1 since we do not want
                                                     // to override somePic.jpg
    \endcode
*/
namespace el3D
{
namespace HighGL
{
enum class elMeshLoader_Error
{
    UnableToLoadFile,
    UVOTextureArrayTooSmall,
    InconsistentVertexIndicies,
    Undefined
};
class elMeshLoader
    : public DesignPattern::Creation< elMeshLoader, elMeshLoader_Error >
{
  private:
    struct material_t;

  public:
    elMeshLoader() = delete;

    elMeshLoader( const elMeshLoader & ) = delete;
    elMeshLoader( elMeshLoader && )      = default;
    ~elMeshLoader()                      = default;
    elMeshLoader &
    operator=( elMeshLoader && ) = default;
    elMeshLoader &
    operator=( const elMeshLoader & ) = delete;

    /*!
        \brief put the loaded object into an unified vertex object
        \param the textureOffset sets the starting point in the texture array
                of the unifiedVertexObject
      */
    std::vector< bb::product_ptr< UnifiedVertexObject::Object > >
    LoadIntoUnifiedVertexObject(
        elGeometricObject_UnifiedVertexObject & ) noexcept;

    /*!
        \brief return the number of textures used in the mesh
        \code
            elMeshLoader someMesh("someMesh.obj");
            uint64_t texArraySize = someMesh.GetTextureArraySize();
            elGeometricObject_UnifiedVertexObject uvo(256, 256, texArraySize);
            someMesh.LoadIntoUnifiedVertexObject(uvo, 0);
        \endcode
    */
    uint64_t
    GetTextureArraySize() const noexcept;

    friend struct DesignPattern::Creation< elMeshLoader, elMeshLoader_Error >;

  private:
    /*!
       \brief load a mesh from a file and store it in an unified vertex object
                if the file does not exists or is not valid then the object is
                invalid
       \param filename of the mesh
    */
    elMeshLoader( const std::string &fileName, const float scaling = 1.0f );

  private:
    enum class TextureConversion
    {
        None,
        HeightMapToNormalMap
    };

    uint64_t
    AddRawTexture( const std::string &     filePath,
                   const TextureConversion conversion ) noexcept;
    void
    SetTextureArraySize() noexcept;
    void
    GenerateMaterials() noexcept;
    void
    ExtractMaterial( const aiMaterial *material, const material_t &type,
                     const uint64_t     materialIndex,
                     const std::string &path ) noexcept;
    bb::elExpected< elMeshLoader_Error >
    GenerateVertexObjects();
    std::string
    GetDirectoryFromFilePath( const std::string &filePath ) const noexcept;
    std::string
    AdjustPathToPlatform( const std::string &filePath ) const noexcept;

    void
    AddToBufferFromVector( const aiVector3D *vec, const uint64_t offset,
                           const bool              useZeroVector,
                           std::vector< GLfloat > &buffer,
                           const uint64_t          bufferDim ) const noexcept;

    std::string
    TextureEnumToString( const aiTextureType textureType ) const noexcept;

  private:
    std::string    fileName;
    const aiScene *scene{ nullptr };
    aiMatrix4x4    transformation;
    float          scaling = 1.0f;
    uint64_t       textureArraySize{ 0u };
#ifdef _WIN32
    static constexpr char pathSeparator        = '\\';
    static constexpr char foreignPathSeparator = '/';
#else
    static constexpr char pathSeparator        = '/';
    static constexpr char foreignPathSeparator = '\\';
#endif

    struct
    {
        uint64_t vertex   = 3;
        uint64_t texCoord = 2;
    } dimension;

    struct rawTexture_t
    {
        std::string                       filePath;
        TextureConversion                 conversion;
        std::unique_ptr< GLAPI::elImage > image;
    };

    static constexpr uint64_t INVALID_ID =
        std::numeric_limits< uint64_t >::max();

    std::vector< std::vector< uint64_t > > objectMaterials;
    std::vector< rawTexture_t >            rawTextures;

    struct material_t
    {
        const int                                     id;
        const aiTextureType                           type;
        HighGL::UnifiedVertexObject::uintBufferList_t bufferID;
        material_t( const int id, const aiTextureType &t,
                    const HighGL::UnifiedVertexObject::uintBufferList_t b )
            : id( id ), type( t ), bufferID( b )
        {
        }
    };

    std::vector< material_t > materials = {
        { 0, aiTextureType_DIFFUSE, HighGL::UnifiedVertexObject::TEX_DIFFUSE },
        { 1, aiTextureType_NORMALS, HighGL::UnifiedVertexObject::TEX_NORMAL },
        { 1, aiTextureType_HEIGHT, HighGL::UnifiedVertexObject::TEX_NORMAL },
        { 2, aiTextureType_SPECULAR,
          HighGL::UnifiedVertexObject::TEX_SPECULAR_COLOR },
        //{ 2, aiTextureType_SHININESS,
        //  HighGL::elGeometricObject_UnifiedVertexObject::TEX_SHININESS },
        //{ 5, aiTextureType_EMISSION_COLOR,
        //  HighGL::elGeometricObject_UnifiedVertexObject::TEX_EMISSION },
        //{ 6, aiTextureType_METALNESS,
        //  HighGL::elGeometricObject_UnifiedVertexObject::TEX_SHININESS },
        //{ 7, aiTextureType_DIFFUSE_ROUGHNESS,
        //  HighGL::elGeometricObject_UnifiedVertexObject::TEX_ROUGHNESS },
    };

    struct object_t
    {
        std::vector< GLfloat > vertices;
        std::vector< GLfloat > textureCoordinates;
        std::vector< GLfloat > normals;
        std::vector< GLfloat > tangents;
        std::vector< GLfloat > bitangents;
        std::vector< GLuint >  elements;
        std::vector< GLuint >  materialIds;
        std::string            name;
    };
    std::vector< object_t > objects;
};
} // namespace HighGL
} // namespace el3D
