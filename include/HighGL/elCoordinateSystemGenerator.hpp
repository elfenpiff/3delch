#pragma once

#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elTexture_2D.hpp"

namespace el3D
{
namespace HighGL
{
class elCoordinateSystemGenerator
{
  public:
    enum class GridStyle
    {
        Lines,
        DotsAndCrosses
    };

    struct settings_t
    {
        glm::vec2 gridSize      = { 10, 10 };
        GridStyle gridStyle     = GridStyle::Lines;
        glm::vec3 position      = glm::vec3{ 0.0f, 0.0f, 0.0f };
        float     majorDrawSize = 2.0f;
        float     minorDrawSize = 1.0f;
        float     majorTileSize = 1.0f;
        float     minorTileSize = 0.1f;
    };

    struct objectGeometry_t
    {
        std::vector< GLfloat > majorLines;
        std::vector< GLfloat > minorLines;
    };

    elCoordinateSystemGenerator( const settings_t& settings ) noexcept;
    ~elCoordinateSystemGenerator();

    const objectGeometry_t&
    GetGeometry() const noexcept;

    void
    UpdateBuffers( const glm::vec2& offset ) noexcept;

  private:
    std::vector< GLfloat >
    CreateLineVertices( const GLfloat tileSize, const glm::vec2& offset,
                        const glm::vec2& gridSize,
                        GLfloat          skipLine = -1.0f ) const noexcept;
    std::vector< GLfloat >
    CreateDotVertices( const GLfloat tileSize, const glm::vec2& offset,
                       const glm::vec2& gridSize,
                       GLfloat          skipLine = -1.0f ) const noexcept;
    std::vector< GLfloat >
    CreateCrossVertices( const GLfloat tileSize, const GLfloat crossSize,
                         const glm::vec2& offset, const glm::vec2& gridSize,
                         GLfloat skipLine = -1.0f ) const noexcept;

  private:
    objectGeometry_t geometry;
    settings_t       settings;
};
} // namespace HighGL
} // namespace el3D
