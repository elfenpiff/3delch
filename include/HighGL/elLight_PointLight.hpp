#pragma once

#include "HighGL/elLight_base.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"

namespace el3D
{
namespace RenderPipeline
{
class elRenderer_Deferred;
}

namespace HighGL
{
class elLight_PointLight : public elLight_base
{
  public:
    elLight_PointLight() noexcept;
    elLight_PointLight( const glm::vec3 &color, const glm::vec3 &attenuation,
                        const glm::vec3 &position, const float diffuseIntensity,
                        const float ambientIntensity ) noexcept;
    elLight_PointLight( const elLight_PointLight & ) = delete;
    elLight_PointLight( elLight_PointLight && )      = default;
    virtual ~elLight_PointLight()                    = default;

    elLight_PointLight &
    operator=( const elLight_PointLight & ) = delete;
    elLight_PointLight &
    operator=( elLight_PointLight && ) = default;

    void
    Bind( const OpenGL::elShaderProgram::shaderAttribute_t &a ) noexcept;
    void
    Unbind( const OpenGL::elShaderProgram::shaderAttribute_t &a ) noexcept;

    void
    SetAttenuation( const glm::vec3 & ) noexcept;
    void
    SetPosition( const glm::vec3 & ) noexcept;

    glm::vec3
    GetAttenuation() const noexcept;
    glm::vec3
    GetPosition() const noexcept;
    float
    GetRadius() const noexcept;

    friend class RenderPipeline::elRenderer_Deferred;

  private:
    void
    SetCubeSize() noexcept;
    void
    UpdateGeometricObjectPosition() noexcept override;

  private:
    OpenGL::elGeometricObject_VertexObject cube;
    float                                  radius = 0.0f;
    // constant, linear, exponential
    glm::vec3 attenuation = glm::vec3( 0.0 );
    glm::vec3 position    = glm::vec3( 0.0 );
};
} // namespace HighGL
} // namespace el3D
