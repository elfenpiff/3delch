#pragma once

#include <cstdint>
#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
class elEventColorRange
{
  public:
    elEventColorRange() noexcept = default;
    elEventColorRange( const uint32_t colorIndex,
                       const uint32_t size ) noexcept;

    uint32_t
    GetSize() const noexcept;
    uint32_t
    GetColorIndex( const uint64_t index ) const noexcept;
    glm::vec4
    GetRGBAColor( const uint64_t index ) const noexcept;

  private:
    uint32_t colorIndex = 0;
    uint32_t size       = 0;
};
} // namespace HighGL
} // namespace el3D
