#pragma once

#include <algorithm>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <vector>

extern "C"
{
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
}

#include "logging/elLog.hpp"
#include "lua/CallableTemplates.hpp"
#include "luaBindings/types.hpp"
#include "utils/macros.hpp"

#ifdef ADD_LUA_BINDINGS
/// enable lua bindings by defining ADD_LUA_BINDINGS
#define LUA_R( s1 )                                                            \
    void       CONCAT( LUA_init_func, s1 )();                                  \
    inline int CONCAT( LUA_init_func_Temp, s1 ) =                              \
        lua::LuaRegistrator( CONCAT( LUA_init_func, s1 ), false )

#define LUA_F( s1 ) void CONCAT( LUA_init_func, s1 )()

#define LUA_INIT_PREDEF_FUNCTIONS lua::LuaRegistrator( nullptr, true )
#endif

namespace el3D
{
namespace lua
{
extern std::map< size_t, std::string > luaRegisteredTypes;
extern std::map< size_t, std::string > luaRegisteredTypesPtr;

int
LuaRegistrator( const std::function< void() > &f, const bool &doApply );

class elLuaScript_Exception : public std::runtime_error
{
  public:
    explicit elLuaScript_Exception( const std::string &msg )
        : std::runtime_error( msg )
    {
    }
    virtual ~elLuaScript_Exception()
    {
    }
};


// LuaCheck
template < typename T > // default
inline typename std::enable_if< !luaBindings::isLuaArray< T >::value &&
                                    !luaBindings::isLuaTable< T >::value &&
                                    !std::is_enum_v< T >,
                                T >::type
LuaCheck( lua_State *l, const int &stackPos )
{
    auto iter = luaRegisteredTypes.find( typeid( T ).hash_code() );
    if ( iter != luaRegisteredTypes.end() )
    {
        return **reinterpret_cast< T ** >(
            luaL_checkudata( l, stackPos, iter->second.c_str() ) );
    }
    else
    {
        using Type = std::remove_const_t< std::remove_pointer_t< T > > *;
        auto iter  = luaRegisteredTypesPtr.find( typeid( Type ).hash_code() );
        if ( iter != luaRegisteredTypesPtr.end() )
        {
            return *reinterpret_cast< T * >(
                luaL_checkudata( l, stackPos, iter->second.c_str() ) );
        }
    }
    return T();
}
template < typename T >
inline typename std::enable_if< luaBindings::isLuaArray< T >::value, T >::type
LuaCheck( lua_State *l, const int &stackPos )
{
    T retVal;
    if ( lua_istable( l, stackPos ) )
    {
        lua_pushnil( l );
        while ( lua_next( l, stackPos ) != 0 )
        {
            retVal.data.push_back(
                LuaCheck< typename T::value_type >( l, -1 ) );
            lua_pop( l, 1 );
        }
    }
    return retVal;
}
template < typename T >
inline typename std::enable_if< luaBindings::isLuaTable< T >::value, T >::type
LuaCheck( lua_State *l, const int &stackPos )
{
    T retVal;
    if ( lua_istable( l, stackPos ) )
    {
        lua_pushnil( l );
        while ( lua_next( l, stackPos ) != 0 )
        {
            retVal.data.insert( std::make_pair(
                LuaCheck< typename T::key_type >( l, -2 ),
                LuaCheck< typename T::mapped_type >( l, -1 ) ) );
            lua_pop( l, 1 );
        }
    }
    return retVal;
}
template < typename T > // enums
inline typename std::enable_if< std::is_enum_v< T >, T >::type
LuaCheck( lua_State *l, const int &stackPos )
{
    return static_cast< T >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline uint8_t
LuaCheck< uint8_t >( lua_State *l, const int &stackPos )
{
    return static_cast< uint8_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline uint16_t
LuaCheck< uint16_t >( lua_State *l, const int &stackPos )
{
    return static_cast< uint16_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline uint32_t
LuaCheck< uint32_t >( lua_State *l, const int &stackPos )
{
    return static_cast< uint32_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline uint64_t
LuaCheck< uint64_t >( lua_State *l, const int &stackPos )
{
    return static_cast< uint64_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline int8_t
LuaCheck< int8_t >( lua_State *l, const int &stackPos )
{
    return static_cast< int8_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline int16_t
LuaCheck< int16_t >( lua_State *l, const int &stackPos )
{
    return static_cast< int16_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline int32_t
LuaCheck< int32_t >( lua_State *l, const int &stackPos )
{
    return static_cast< int32_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline int64_t
LuaCheck< int64_t >( lua_State *l, const int &stackPos )
{
    return static_cast< int64_t >( luaL_checkinteger( l, stackPos ) );
}
template <>
inline bool
LuaCheck< bool >( lua_State *l, const int &stackPos )
{
    // i know it is not luaL_checkboolean but since it does not exist we
    // need to use lua_toboolean
    return lua_toboolean( l, stackPos );
}
template <>
inline float
LuaCheck< float >( lua_State *l, const int &stackPos )
{
    return static_cast< float >( luaL_checknumber( l, stackPos ) );
}
template <>
inline double
LuaCheck< double >( lua_State *l, const int &stackPos )
{
    return static_cast< double >( luaL_checknumber( l, stackPos ) );
}
template <>
inline long double
LuaCheck< long double >( lua_State *l, const int &stackPos )
{
    return static_cast< long double >( luaL_checknumber( l, stackPos ) );
}
template <>
inline std::string
LuaCheck< std::string >( lua_State *l, const int &stackPos )
{
    return luaL_checkstring( l, stackPos );
}
template <>
inline const char *
LuaCheck< const char * >( lua_State *l, const int &stackPos )
{
    return luaL_checkstring( l, stackPos );
}


// LuaPush
template < typename T > // default
inline typename std::enable_if< !luaBindings::isLuaArray< T >::value &&
                                    !luaBindings::isLuaTable< T >::value,
                                int >::type
LuaPush( lua_State *l, const T t )
{
    auto iter = luaRegisteredTypes.find( typeid( T ).hash_code() );
    if ( iter != luaRegisteredTypes.end() )
    {
        T **newClass =
            reinterpret_cast< T ** >( lua_newuserdata( l, sizeof( T * ) ) );
        *newClass = new T( t );
        luaL_getmetatable( l, iter->second.c_str() );
        lua_setmetatable( l, -2 );
    }
    else
    {
        T *newClass =
            reinterpret_cast< T * >( lua_newuserdata( l, sizeof( T ) ) );
        *newClass    = t;
        auto iterPtr = luaRegisteredTypesPtr.find( typeid( T ).hash_code() );
        if ( iterPtr != luaRegisteredTypesPtr.end() )
        {
            luaL_getmetatable( l, iterPtr->second.c_str() );
            lua_setmetatable( l, -2 );
        }
    }

    return 1;
}
template < typename T >
inline typename std::enable_if< luaBindings::isLuaArray< T >::value, int >::type
LuaPush( lua_State *l, const T t )
{
    lua_newtable( l );
    int i = 0;
    for ( auto value : t.data )
    {
        LuaPush( l, value );
        lua_rawseti( l, -2, i );
        i++;
    }
    return 1;
}
template < typename T >
inline typename std::enable_if< luaBindings::isLuaTable< T >::value, int >::type
LuaPush( lua_State *l, const T t )
{
    lua_newtable( l );
    std::stringstream ss;
    for ( auto entry : t.data )
    {
        ss << entry.first;
        LuaPush( l, entry.second );
        lua_setfield( l, -2, ss.str().c_str() );
        ss.str( "" );
    }
    return 1;
}
template <>
inline int
LuaPush< std::string >( lua_State *l, const std::string arg )
{
    lua_pushstring( l, arg.c_str() );
    return 1;
}
template <>
inline int
LuaPush< const char * >( lua_State *l, const char *arg )
{
    lua_pushstring( l, arg );
    return 1;
}
template <>
inline int
LuaPush< float >( lua_State *l, const float arg )
{
    lua_pushnumber( l, static_cast< lua_Number >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< double >( lua_State *l, const double arg )
{
    lua_pushnumber( l, static_cast< lua_Number >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< long double >( lua_State *l, const long double arg )
{
    lua_pushnumber( l, static_cast< lua_Number >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< uint8_t >( lua_State *l, const uint8_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< uint16_t >( lua_State *l, const uint16_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< uint32_t >( lua_State *l, const uint32_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< uint64_t >( lua_State *l, const uint64_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< int8_t >( lua_State *l, const int8_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< int16_t >( lua_State *l, const int16_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< int32_t >( lua_State *l, const int32_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< int64_t >( lua_State *l, const int64_t arg )
{
    lua_pushinteger( l, static_cast< lua_Integer >( arg ) );
    return 1;
}
template <>
inline int
LuaPush< bool >( lua_State *l, const bool arg )
{
    lua_pushboolean( l, static_cast< int >( arg ) );
    return 1;
}

// LuaTo
template < typename T >
inline T
LuaTo( lua_State *l, const int &stackPos )
{
    auto iter = luaRegisteredTypes.find( typeid( T ).hash_code() );
    if ( iter != luaRegisteredTypes.end() )
    {
        return **reinterpret_cast< T ** >( lua_touserdata( l, stackPos ) );
    }
    else
    {
        auto iter = luaRegisteredTypesPtr.find( typeid( T ).hash_code() );
        if ( iter != luaRegisteredTypesPtr.end() )
        {
            return *reinterpret_cast< T * >( lua_touserdata( l, stackPos ) );
        }
    }

    return reinterpret_cast< T >( lua_touserdata( l, stackPos ) );
}
template <>
inline lua_Integer
LuaTo< lua_Integer >( lua_State *l, const int &stackPos )
{
    return lua_tointeger( l, stackPos );
}
template <>
inline int
LuaTo< int >( lua_State *l, const int &stackPos )
{
    return static_cast< int >( lua_tointeger( l, stackPos ) );
}
template <>
inline lua_Number
LuaTo< lua_Number >( lua_State *l, const int &stackPos )
{
    return lua_tonumber( l, stackPos );
}
template <>
inline float
LuaTo< float >( lua_State *l, const int &stackPos )
{
    return static_cast< float >( lua_tonumber( l, stackPos ) );
}
template <>
inline bool
LuaTo< bool >( lua_State *l, const int &stackPos )
{
    return static_cast< bool >( lua_toboolean( l, stackPos ) );
}
template <>
inline std::string
LuaTo< std::string >( lua_State *l, const int &stackPos )
{
    if ( lua_isnumber( l, stackPos ) || lua_isboolean( l, stackPos ) )
        return std::to_string( lua_tonumber( l, stackPos ) );
    else if ( lua_isstring( l, stackPos ) )
        return lua_tostring( l, stackPos );
    else
        return std::string();
}

// CreateArgTuple
template < int N, typename Tuple_t >
inline void
SetArgTuple( lua_State *, const int &, Tuple_t & )
{
}

template < int N, typename Tuple_t, typename T, typename... Targs >
inline void
SetArgTuple( lua_State *l, const int &startPos, Tuple_t &argTuple )
{
    if ( lua_gettop( l ) <= N )
        throw elLuaScript_Exception( LOG_EX( 0, "wrong number of arguments" ) );
    std::get< N >( argTuple ) =
        LuaCheck< typename std::tuple_element< N, Tuple_t >::type >(
            l, N + 1 + startPos );

    SetArgTuple< N + 1, Tuple_t, Targs... >( l, startPos, argTuple );
}

template < typename... Targs >
inline void
CreateArgTuple( lua_State *l, const int &startPos,
                std::tuple< Targs... > &argTuple )
{
    SetArgTuple< 0, std::tuple< Targs... >, Targs... >( l, startPos, argTuple );
}

// class handling
template < typename T, typename... Targs >
inline int
LuaProxyClassCtor( lua_State *l )
{
    std::tuple< Targs... > t;
    CreateArgTuple( l, 0, t );
    if ( lua_gettop( l ) != std::tuple_size< decltype( t ) >::value )
        throw elLuaScript_Exception(
            LOG_EX( 0, "wrong number of constructor arguments" ) );

    std::string name = lua_tostring( l, lua_upvalueindex( 1 ) );
    T **        newClass =
        reinterpret_cast< T ** >( lua_newuserdata( l, sizeof( T * ) ) );
    *newClass = CallNew< T >( t );

    luaL_getmetatable( l, name.c_str() );
    lua_setmetatable( l, -2 );

    return 1;
}

template < typename T >
inline int
LuaProxyClassDtor( lua_State *l )
{
    T **t = LuaTo< T ** >( l, 1 );
    delete *t;

    return 0;
}


template < typename Func_t, typename ArgumentTuple_T >
inline std::pair< Func_t *, ArgumentTuple_T >
LuaProxyFuncCallerPrep( lua_State *l, const int &stackPos )
{
    Func_t *f = reinterpret_cast< Func_t * >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );
    if ( !f )
        throw elLuaScript_Exception(
            LOG_EX( 0, "could not cast function pointer from lua stack" ) );

    ArgumentTuple_T t;
    CreateArgTuple( l, stackPos, t );

    if ( lua_gettop( l ) !=
         static_cast< int >( std::tuple_size< ArgumentTuple_T >::value +
                             static_cast< size_t >( stackPos ) ) )
        throw elLuaScript_Exception(
            LOG_EX( 0, "wrong number of function arguments" ) );

    return std::make_pair( f, t );
}

template < typename T, typename Var_t, typename Type_t >
inline int
LuaProxyClassGet( lua_State *l )
{
    T **   t = LuaTo< T ** >( l, 1 );
    Var_t *f = reinterpret_cast< Var_t * >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );

    LuaPush< Type_t >( l, ( *t )->*std::forward< Var_t >( *f ) );
    return 1;
}

template < typename T, typename Var_t, typename Type_t >
inline int
LuaProxyClassSet( lua_State *l )
{
    T **   t        = LuaTo< T ** >( l, 1 );
    Type_t newValue = LuaCheck< Type_t >( l, 2 );
    Var_t *f        = reinterpret_cast< Var_t * >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );

    ( *t )->*std::forward< Var_t >( *f ) = newValue;
    return 0;
}

template < typename Var_t, typename Type_t >
inline int
LuaProxyClassGetStatic( lua_State *l )
{
    Type_t **f = reinterpret_cast< Type_t ** >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );
    LuaPush< Type_t >( l, **f );
    return 1;
}

template < typename Var_t, typename Type_t >
inline int
LuaProxyClassSetStatic( lua_State *l )
{
    Type_t   newValue = LuaCheck< Type_t >( l, 1 );
    Type_t **f        = reinterpret_cast< Type_t ** >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );
    **f = newValue;
    return 0;
}

template < typename T, typename Func_t, typename Ret_t,
           typename ArgumentTuple_t >
inline typename std::enable_if< !std::is_void< Ret_t >::value, int >::type
LuaProxyClassMethod( lua_State *l )
{
    T ** t    = LuaTo< T ** >( l, 1 );
    auto prep = LuaProxyFuncCallerPrep< Func_t, ArgumentTuple_t >( l, 1 );
    return LuaPush< Ret_t >(
        l, static_cast< Ret_t >( Call( *t, *prep.first, prep.second ) ) );
}

template < typename T, typename Func_t, typename Ret_t,
           typename ArgumentTuple_t >
inline typename std::enable_if< std::is_void< Ret_t >::value, int >::type
LuaProxyClassMethod( lua_State *l )
{
    T ** t    = LuaTo< T ** >( l, 1 );
    auto prep = LuaProxyFuncCallerPrep< Func_t, ArgumentTuple_t >( l, 1 );
    Call( *t, *prep.first, prep.second );

    return 0;
}

template < typename Func_t, typename Ret_t, typename Tuple_t >
inline typename std::enable_if< !std::is_void< Ret_t >::value, int >::type
LuaProxyClassStaticMethod( lua_State *l )
{
    auto prep = LuaProxyFuncCallerPrep< Func_t, Tuple_t >( l, 0 );
    return LuaPush< Ret_t >( l, Call( *prep.first, prep.second ) );
}

template < typename Func_t, typename Ret_t, typename Tuple_t >
inline typename std::enable_if< std::is_void< Ret_t >::value, int >::type
LuaProxyClassStaticMethod( lua_State *l )
{
    auto prep = LuaProxyFuncCallerPrep< Func_t, Tuple_t >( l, 0 );
    Call( *prep.first, prep.second );

    return 0;
}


// lua C function caller
template < typename Func_t, typename Ret_t, typename... Targs >
inline typename std::enable_if< !std::is_void< Ret_t >::value, int >::type
LuaProxyFuncCaller( lua_State *l )
{
    auto prep =
        LuaProxyFuncCallerPrep< Func_t, std::tuple< Targs... > >( l, 0 );
    return LuaPush< Ret_t >( l, Call( prep.first, prep.second ) );
}

template < typename Func_t, typename Ret_t, typename... Targs >
inline typename std::enable_if< std::is_void< Ret_t >::value, int >::type
LuaProxyFuncCaller( lua_State *l )
{
    auto prep =
        LuaProxyFuncCallerPrep< Func_t, std::tuple< Targs... > >( l, 0 );
    Call( prep.first, prep.second );
    return 0;
}

template < typename Func_t, typename... Targs >
inline void
LuaPushFunction( lua_State *l, const std::string &name, const Func_t f )
{
    lua_pushlightuserdata( l, reinterpret_cast< void * >( f ) );
    lua_pushcclosure(
        l,
        LuaProxyFuncCaller<
            Func_t, typename std::invoke_result< Func_t &, Targs... >::type,
            Targs... >,
        1 );
    lua_setglobal( l, name.c_str() );
}
} // namespace lua
} // namespace el3D
