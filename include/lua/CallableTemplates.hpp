#pragma once

#include <cstddef>
#include <tuple>
#include <type_traits>
#include <typeindex>
#include <utility>

/*!
    \code
        class mathStuff {
        public:
            int sum(int a, int b) { return a + b; }
        };

        int subst(int a, int b) { return a - b; }

        int main() {
            mathStuff b;
            std::tuple<int, int> t = std::make_tuple(10, 20);
            int c = extTuple::Call(&mathStuff::sum, &b, t); // contains 30
            int d = extTuple::Call(subst, t); // contains -10
        }
    \endcode
    \brief calls a class method or a function with a tuple as argument
*/
namespace el3D
{
namespace lua
{
using std::forward;

template < size_t N >
struct ApplyToFunction
{
    template < typename Func_t, typename Tuple_t, typename... Args >
    static auto
    call( Func_t&& f, Tuple_t&& t, Args&&... a )
        -> decltype( ApplyToFunction< N - 1 >::call(
            ::std::forward< Func_t >( f ), ::std::forward< Tuple_t >( t ),
            ::std::get< N - 1 >( ::std::forward< Tuple_t >( t ) ),
            ::std::forward< Args >( a )... ) )
    {
        return ApplyToFunction< N - 1 >::call(
            ::std::forward< Func_t >( f ), ::std::forward< Tuple_t >( t ),
            ::std::get< N - 1 >( ::std::forward< Tuple_t >( t ) ),
            ::std::forward< Args >( a )... );
    }
};

template <>
struct ApplyToFunction< 0 >
{
    template < typename Func_t, typename Tuple_t, typename... Args >
    static auto
    call( Func_t&& f, Tuple_t&&, Args&&... a ) -> decltype(
        ::std::forward< Func_t >( f )( ::std::forward< Args >( a )... ) )
    {
        return ::std::forward< Func_t >( f )( ::std::forward< Args >( a )... );
    }
};

template < typename Func_t, typename Tuple_t >
auto
Call( Func_t&& f, Tuple_t&& t ) -> decltype(
    ApplyToFunction<
        ::std::tuple_size< typename ::std::decay< Tuple_t >::type >::value >::
        call( ::std::forward< Func_t >( f ), ::std::forward< Tuple_t >( t ) ) )
{
    return ApplyToFunction<
        ::std::tuple_size< typename ::std::decay< Tuple_t >::type >::value >::
        call( ::std::forward< Func_t >( f ), ::std::forward< Tuple_t >( t ) );
}


template < size_t N >
struct ApplyToMethod
{
    template < typename Class_t, typename Method_t, typename T,
               typename... Args >
    static auto
    call( Class_t&& c, Method_t&& m, T&& t, Args&&... a )
        -> decltype( ApplyToMethod< N - 1 >::call(
            forward< Class_t >( c ), forward< Method_t >( m ),
            forward< T >( t ), std::get< N - 1 >( forward< T >( t ) ),
            forward< Args >( a )... ) )
    {
        return ApplyToMethod< N - 1 >::call(
            forward< Class_t >( c ), forward< Method_t >( m ),
            forward< T >( t ), std::get< N - 1 >( forward< T >( t ) ),
            forward< Args >( a )... );
    }
};

template <>
struct ApplyToMethod< 0 >
{
    template < typename Class_t, typename Method_t, typename T,
               typename... Args >
    static auto
    call( Class_t&& c, Method_t&& m, T&&, Args&&... a )
        -> decltype( ( forward< Class_t >( c )->*forward< Method_t >( m ) )(
            forward< Args >( a )... ) )
    {
        return ( forward< Class_t >( c )->*forward< Method_t >( m ) )(
            forward< Args >( a )... );
    }
};

template < typename Class_t, typename Method_t, typename T >
auto
Call( Class_t&& c, Method_t&& m, T&& t ) -> decltype(
    ApplyToMethod< std::tuple_size< typename std::decay< T >::type >::value >::
        call( forward< Class_t >( c ), forward< Method_t >( m ),
              forward< T >( t ) ) )
{
    return ApplyToMethod< std::tuple_size< typename std::decay< T >::type >::
                              value >::call( forward< Class_t >( c ),
                                             forward< Method_t >( m ),
                                             forward< T >( t ) );
}


template < size_t N, typename Class_t >
struct ApplyToCTor
{
    template < typename Tuple_t, typename... Args >
    static Class_t*
    call( Tuple_t&& t, Args&&... a )
    {
        return ApplyToCTor< N - 1, Class_t >::call(
            ::std::forward< Tuple_t >( t ),
            ::std::get< N - 1 >( ::std::forward< Tuple_t >( t ) ),
            ::std::forward< Args >( a )... );
    }
};

template < typename Class_t >
struct ApplyToCTor< 0, Class_t >
{
    template < typename Tuple_t, typename... Args >
    static Class_t*
    call( Tuple_t&&, Args&&... a )
    {
        return new Class_t( ::std::forward< Args >( a )... );
    }
};

template < typename Class_t, typename Tuple_t >
Class_t*
CallNew( Tuple_t&& t )
{
    return ApplyToCTor<
        ::std::tuple_size< typename ::std::decay< Tuple_t >::type >::value,
        Class_t >::call( ::std::forward< Tuple_t >( t ) );
}
} // namespace lua


} // namespace el3D
