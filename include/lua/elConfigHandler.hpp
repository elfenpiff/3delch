#pragma once
#include "DesignPattern/Creation.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "logging/elLog.hpp"
#include "lua/elLuaScript.hpp"
#include "utils/elFile.hpp"

#include <map>
#include <memory>
#include <set>
#include <string>

namespace el3D
{
namespace lua
{
enum class elConfigHandler_Error
{
    FileHasNoLuaSuffix,
    UnableToReadFile,
    IsNotADirectory,
    InvalidPath,
    UnableToFindEntry,
    UnableToCreateLuaScript,
};

class elConfigHandler
    : public DesignPattern::Creation< elConfigHandler, elConfigHandler_Error >
{
  public:
    elConfigHandler();
    elConfigHandler( elConfigHandler && )      = default;
    elConfigHandler( const elConfigHandler & ) = delete;
    ~elConfigHandler()                         = default;

    elConfigHandler &
    operator=( elConfigHandler && ) = default;
    elConfigHandler &
    operator=( const elConfigHandler & ) = delete;

    bb::elExpected< elConfigHandler_Error >
    SetConfig( const std::string &path,
               const bool         recursiveDirectorySearch = true );

    void
    ClearFiles() noexcept;

    template < typename T >
    std::vector< T >
    Get( const std::vector< std::string > &configPath,
         const std::vector< T > &          defaultValue = {} ) const noexcept;
    template < typename T, uint64_t ExpectedSize >
    std::vector< T >
    GetWithSizeRestriction( const std::vector< std::string > &   configPath,
                            const std::array< T, ExpectedSize > &fallbackValue =
                                {} ) const noexcept;

    bb::elExpected< elConfigHandler_Error >
    IsEntryAvailable(
        const std::vector< std::string > &configPath ) const noexcept;

    std::set< std::string >
    GetConfigFileList() const noexcept;

    friend struct DesignPattern::Creation< elConfigHandler,
                                           elConfigHandler_Error >;

  private:
    elConfigHandler( const std::string &path );

    bb::elExpected< elConfigHandler_Error >
    SetConfigDirectory( const std::string &dir, const bool recursive = true );
    bb::elExpected< elConfigHandler_Error >
    SetConfigFile( const std::string &file );
    std::string
    PathToString( const std::vector< std::string > &path ) const noexcept;

  private:
    std::map< std::string, std::unique_ptr< lua::elLuaScript > > configFiles;
    std::string configFileSuffix = ".lua";
};
} // namespace lua
} // namespace el3D
#include "lua/elConfigHandler.inl"
