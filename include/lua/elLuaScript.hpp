#pragma once

#include "DesignPattern/Creation.hpp"
#include "buildingBlocks/elExpected.hpp"
#include "buildingBlocks/type_traits_extended.hpp"
#include "lua/CallableTemplates.hpp"
#include "lua/elLuaScriptHelpers.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

extern std::map< size_t, std::string > luaRegisteredTypes;
extern std::map< size_t, std::string > luaRegisteredTypesPtr;

namespace el3D
{
namespace lua
{
enum class elLuaScript_Error
{
    UnableToCreateLuaState,
    InvalidSyntax,
    RuntimeError,
    MemoryAllocationFailed,
    UndefinedError,
    ClassIsNotRegistered,
    GlobalNotRegistered,
    NotAFunction,
    PackagePathGlobalNotFound
};

/*!
\code
    class Tester {
    public:
        Tester(float) {}
        ~Tester() {}
        void func1() { std::cout << "hello\n"; }
        float sum(int a, int b) { return a + b; }
        static double div(int a, int b) { return a / b; }

        std::string someName;
        static int  someInt;
    };

    LUA_R(Tester);
    LUA_R(stdVector);
    LUA_R(someFunc);

    LUA_F(Tester) {
        elLuaScript::class_t newClass("Tester");
        newClass.
            AddConstructor<Tester, float>().
            AddMethod("func1", &Tester::func1).
            AddMethod("sum", &Tester::sum).
            AddStaticMethod("div", &Tester::div).
            AddVariable("someName", &Tester::someName).
            AddStaticVariable("someInt", &GetSetTest::f);
        elLuaScript::RegisterLuaClass(newClass);
    }

    LUA_F(stdVector) { // register class std::vector
        elLuaScript::class_t newClass( "stdVec" );
        newClass.AddConstructor< std::vector< int >, int >()
            .AddMethod("size", &std::vector< int >::size )
            .AddMethod("clear", &std::vector< int >::clear )
            .AddVariable("someName", &Tester::someName);
        elLuaScript::RegisterLuaClass( newClass );
    }

    LUA_F(someFunc) {
        elLuaScript::RegisterFunction<decltype(someFunc), int, float>
            ("someFunc", someFunc, "someNameSpace");
    }

    int main() {
        LUA_INIT_PREDEF_FUNCTIONS;  // registers all LUA_F() functions to all
                                    // new elLuaScript

        elLuaScript script(), script2("print('Hello World')");
        script.SetGlobal<std::string>("helloVar", "HelloWorld");

        script.SetSource("function add(a, b) return a+b end");
        int res = script.Call<lua_integer>("add", 2, 3)[0];

        script2.Exec();
        script2.SetSource("function fuu() print('fuu') end");
        script2.CallVoid("fuu");

        script2.SetSource("a=5; b = {'asd', 'bef'}");
        int a = script2.GetGlobal<lua_Integer>("a")[0];
        std::vector<std::string> b = script2.GetGlobal<std::string>("b");

        script.SetSource("table1 = { entry1 = { subEntry1 = 5}, entry123 =
            { subEntry2 = 3, subEntry4 = 'asd'}}");
        a = script.GetGlobal<lua_Integer>({"table1", "entry1", "subEntry1"})[0];
        b = script.GetGlobal<std::string>({"table1", "entry123", "subEntry4"});

        std::vector<std::string> c = script.GetGlobalTableEntries({"table1"});
        // contains entry1, entry123

        c = script.GetGlobalTableEntries({"table1", "entry1"});
        // contains subEntry1

        script.RequireNamespace("someNameSpace");
        script.SetSource("function a() return someFunc(12.2) end");

        //access the lua script from within the script via "this"
        script.RequireClass("elLuaScript");
        script.SetGlobal<elLuaScript*>("this", &script);
        // lua script requires a namespace
        script.SetSource("this:RequireNamespace('someNameSpace')");

        enum Test_T {A, B, C};
        //enums can be declared with
        script.SetGlobal<int>("A", static_cast<int>(Test_T::A) );

    }
\endcode
**/
class elLuaScript
    : public DesignPattern::Creation< elLuaScript, elLuaScript_Error >
{
  public:
    using fVec_t =
        std::vector< std::tuple< std::string, lua_CFunction,
                                 std::function< void( lua_State * ) > > >;
    using fVar_t = std::vector< std::tuple<
        std::string, lua_CFunction, std::function< void( lua_State * ) >,
        lua_CFunction, std::function< void( lua_State * ) > > >;
    /*
        \code
            -- LUA CODE --
            classVar1 = someClass.new(arg) -- there is no need for delete, lua
                                           --  does it
            ...

            classPtr  = GetClassPtr() -- if you get a class pointer you maybe
                                      -- want do delete it later
            ...
            classPtr:delete()  -- delete the class
            ...
            classVar1:Set_fuu(12)   -- sets class variable fuu
            a = classVar1:Get_bla() -- gets class variable bla
        \endcode
        \brief if you have a pointer to a class the explicit delete method is
       added so
       that the pointer is deleteable via classVar:delete(); if the class is a
       non pointer
       you can construct a new class object via classVar.new(args)
    */

#ifdef ADD_LUA_BINDINGS
    class class_t
    {
      public:
        class NameAlreadyRegistered_e;
        class NoConstructorDefined_e;
        class MultipleClassTypesForOneClass_e;

        class_t() = delete;
        class_t( const std::string &className );
        class_t( class_t && )      = default;
        class_t( const class_t & ) = default;
        class_t &
        operator=( class_t && ) = default;
        class_t &
        operator=( const class_t & ) = default;

        /*!
            \brief Class type deduction does not work on non virtual inherited
                    methods or on virtual but non overridden methods. Therefore
                    we need to set the class type explicitly, otherwise the
                    class is casted to its parent with reinterpret cast which
                    can cause undefined behavior.
        */
        template < typename Class, typename Func_t >
        class_t &
        AddMethod( const std::string &name, const Func_t f );

        template < typename Func_t >
        class_t &
        AddStaticMethod( const std::string &name, const Func_t f );

        template < typename Var_t >
        class_t &
        AddVariable( const std::string &name, const Var_t var );

        template < typename Var_t >
        class_t &
        AddStaticVariable( const std::string &name, const Var_t var );

        template < typename Class, typename... CTorArgs >
        class_t &
        AddConstructor();

        template < typename Class >
        class_t &
        AddPrivateConstructor();

        friend elLuaScript;

      private:
        void
        ThrowExceptionIfAlreadyRegistered( const std::string &newName ) const;

        template < typename Class >
        void
        AddThisPointer( lua_State *l );

      private:
        std::string                           name;
        fVar_t                                methodsGetSetVar;
        fVar_t                                methodsGetSetStaticVar;
        fVec_t                                methods;
        fVec_t                                staticMethods;
        size_t                                typeHashCode;
        size_t                                typeHashCodePtr;
        lua_CFunction                         ctor = nullptr;
        lua_CFunction                         dtor = nullptr;
        bool                                  constructorDefined{ false };
        std::function< void( lua_State *l ) > thisPointerGetCreator;
    };
#endif

    elLuaScript( const elLuaScript & ) = delete;
    elLuaScript( elLuaScript && );

    ~elLuaScript();

    elLuaScript &
    operator=( const elLuaScript & ) = delete;
    elLuaScript &
    operator=( elLuaScript && );

    static bool
    HasLuaBindings() noexcept;

    /*!
        \brief adds this as a global to the lua script so that the this pointer
                can be accessed through the script
    */
    void
    AddThisPointer();

    /*!
        \brief set the source code of the lua script
    */
    bb::elExpected< elLuaScript_Error >
    SetSource( const std::string & );

    /*!
        \brief execute a lua script
    */
    bb::elExpected< elLuaScript_Error >
    Exec() const;

    /*!
        \code
            int a = file.Call<lua_integer>("add", 2, 3)[0];
        \endcode
        \param nresult = number of return value, func = functionName, arguments
        \brief calls a lua function and returns a given number of values
        \tparam lua_Integer, lua_Number, std::string
    */
    template < typename Tret, typename T, typename... Targs >
    bb::elExpected< std::vector< Tret >, elLuaScript_Error >
    Call( const std::string &func, const T &t, Targs... args ) const;

    template < typename Tret >
    bb::elExpected< std::vector< Tret >, elLuaScript_Error >
    Call( const std::string &func ) const;

    /*!
        \code
            script.CallVoid("printHello");
        \endcode
        \brief calls a lua function and returns a given number of values
       name type
       is not
       std::string
        \tparam lua_Integer, lua_Number, std::string
    */
    // TODO: why not merge with Call and try int n = lua_gettop decide for
    // himself
    template < typename T, typename... Targs >
    bb::elExpected< elLuaScript_Error >
    CallVoid( const std::string &func, const T &t, Targs... args ) const;

    bb::elExpected< elLuaScript_Error >
    CallVoid( const std::string &func ) const;

    std::function< void() >
    CreateCallback();

    template < typename ReturnValue, typename... Arguments >
    std::enable_if_t< std::is_same_v< ReturnValue, void >,
                      std::function< ReturnValue( Arguments... ) > >
    CreateCallbackToFunction( const std::string &fName );

    template < typename ReturnValue, typename... Arguments >
    std::enable_if_t< !std::is_same_v< ReturnValue, void >,
                      std::function< ReturnValue( Arguments... ) > >
    CreateCallbackToFunction( const std::string &fName );


    /*!
        \code
            script.SetGlobal<int>("glbVar", 42); // global variable in no
       namespace
            elLuaScript::SetGlobal<int>("fuba", 123, "stuff"); // global
       variable in
       namespace
       stuff
        \endcode
        \brief Sets a global variable in the lua script
    */
    template < typename T >
    void
    SetGlobal( const std::string &varName, const T value ) const noexcept;

    /*!
        \code
            elLuaScript::SetGlobalInNamespace<int>("fuba", 123, "stuff");
            ...
            luaScript.RequireNamespace("stuff"); // sets fuba = 123
        \endcode
        \brief Sets a global variable in a namespace
    */
    template < typename T >
    static void
    SetGlobalInNamespace( const std::string &varName, const T value,
                          const std::string nameSpace ) noexcept;

    /*!
        \code
            lua_Integer a = script.GetGlobal<lua_Integer>({"table1",
                    "entry1", "subEntry1"})[0];
            std::vector<lua_Number> = script.GetGlobal<lua_Number>({"table1",
                    "entry123", "subEntry4"});
        \endcode
        \brief returns the value of an entry in a table
        \tparam lua_Integer, lua_Number, std::string
    */
    template < typename T >
    std::vector< T >
    GetGlobal( const std::vector< std::string > &lst ) const noexcept;

    /*!
        \code
            if ( script.HasGlobal({"table1", "entry1", "subEntry1"}) )
                doStuff();
        \endcode
        \brief returns if a given global variable does exist or not
    */
    bool
    HasGlobal( const std::vector< std::string > &lst ) const noexcept;

    /*!
        \code
            std::vector<std::string>    = script.GetGlobalTableEntries("table1",
       "subtable4");
        \endcode
        \brief returns a list of all table keys at a specific point in the table
        \return the ordering of the entries is arbitrary and not in the same
       order of the
       declaration
    */
    std::vector< std::string >
    GetGlobalTableEntries(
        const std::vector< std::string > &lst ) const noexcept;

    /*!
        \brief returns a list of strings containing the names of all global
                variables
    */
    std::vector< std::string >
    GetGlobalNames() const noexcept;

    /*!
        \brief returns the type of a variable
        \return constants defined in lua.h:
            LUA_TNIL, LUA_TNUMBER, LUA_TBOOLEAN, LUA_TSTRING, LUA_TTABLE,
            LUA_TFUNCTION, LUA_TUSERDATA, LUA_TTHREAD, and LUA_TLIGHTUSERDATA
            or LUA_TNONE for a non-valid index
    */
    int
    GetGlobalType( const std::string &varName ) const noexcept;


    /*!
        \code
            elLuaScript::RegisterFunction("sum", sum, "math");
            script.RequireNamespace("math");
            script.SetSource("function a(b, c) return sum(a,b) end");
        \endcode
        \brief registers a c function in all lua objects by putting it in a
       namespace
       which must be
       required later by RequireNamespace
    */
    template < typename Func_t, typename... Targs >
    static void
    RegisterFunction( const std::string &name, const Func_t f,
                      const std::string &nameSpace ) noexcept;

#ifdef ADD_LUA_BINDINGS
    /*!
        \code
            elLuaScript::class_t newClass("ClassName");
            newClass.
                AddConstructor<ClassName, int>().
                ...

            elLuaScript::RegisterLuaClass(newClass);
            ...

        elLuaScript::RegisterLuaClass(newClass);
        ...

        script.RequireClass("ClassName");
        script.SetSource("a = ClassName.new(5)");
    \endcode
    \brief registers a class in lua so that it can be used later in the script;
    if you
    want to use
    it, the class must be required with RequireClass first
    */
    static void
    RegisterLuaClass( const elLuaScript::class_t &newClass,
                      const std::string &         nameSpace = "" );

    /*!
        \brief registers a class in lua so that it is usable in the script
    */
    bb::elExpected< elLuaScript_Error >
    RequireClass( const std::string & ) const;
#endif

    /*!
        \code
            enum SomeEnum {
                A = 1,
                B = 2
            };

            elLuaScript::RegisterGlobal<int>("SomeEnum", {{"A", A}, {"B", B}});
            ...
            script.RequireGlobal("SomeEnum");
        \endcode
    */
    template < typename T >
    static void
    RegisterGlobal( const std::string &              groupName,
                    const std::map< std::string, T > values,
                    const std::string &              nameSpace = "" );

    /*!
        \brief registers all functions globally in lua which are registered in a
       given
       namespace so
       that they are usable in the script
    */
    void
    RequireNamespace( const std::string & );

    /*!
        \brief registers a global in lua so that it is usable in the script
    */
    bb::elExpected< elLuaScript_Error >
    RequireGlobal( const std::string &groupName );

    /*!
        \brief adds a search path for lua modules which are used via require
    */
    bb::elExpected< elLuaScript_Error >
    AddModuleSearchPath( const std::string &path );

    friend struct DesignPattern::Creation< elLuaScript, elLuaScript_Error >;

  private:
    elLuaScript();
    explicit elLuaScript( const std::string &sourceCode );

    template < typename Func_t, typename Return_t, typename... Targs >
    static void
    RegisterFunctionHelper( const std::string &name,
                            Return_t ( *f )( Targs... ),
                            const std::string &nameSpace ) noexcept;

    bool
    IterateToEntryInTable( const std::string &entry ) const noexcept;

    void
    PopulateGlobalBlacklist() noexcept;
    bb::elExpected< elLuaScript_Error >
    GetGlobalFunc( const std::string & ) const;

    template < typename T >
    std::vector< T >
    LuaToVector( const size_t &nresult ) const noexcept;

    template < typename T >
    size_t
    CallLuaRecursion( const size_t &argNo, const T &t ) const noexcept;

    template < typename T, typename... Targs >
    size_t
    CallLuaRecursion( const size_t &argNo, const T &t,
                      Targs... args ) const noexcept;

    template < typename T >
    std::vector< T >
    GetGlobalByType() const noexcept;

#ifdef ADD_LUA_BINDINGS
    void
    CreateClassTable( const elLuaScript::class_t &,
                      const bool & ) const noexcept;
#endif

    bb::elExpected< elLuaScript_Error >
    ErrorHandler( const int & ) const;
    void
    SetField( const std::string & ) const noexcept;
    void
    Move( elLuaScript &&rhs ) noexcept;

  private:
    lua_State *             luaState         = nullptr;
    mutable bool            doExecBeforeCall = false;
    std::string             source;
    std::set< std::string > globalBlackList;
    mutable struct
    {
        std::set< std::string > namespaces;
        std::set< std::string > classes;
        std::map< std::string, std::function< void( lua_State * ) > > globals;
    } active;

    struct namespace_t
    {
        std::vector< std::function< void( lua_State * ) > >         functions;
        std::vector< std::function< void( const elLuaScript * ) > > variables;
        std::vector< std::function< void( const elLuaScript * ) > > classes;
        std::vector< std::function< void( elLuaScript * ) > >       constants;
    };

    static bool                                 preMainPhase;
    static std::map< std::string, namespace_t > nameSpaces;

#ifdef ADD_LUA_BINDINGS
    static std::map< std::string, elLuaScript::class_t > classes;
#endif

    static std::map< std::string, std::function< void( elLuaScript *const ) > >
        globalRegistrators;
};

#ifdef ADD_LUA_BINDINGS
LUA_R( elLuaScript );
#endif
} // namespace lua
} // namespace el3D

#include "lua/elLuaScript.inl"
