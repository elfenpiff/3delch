#pragma once

#include <algorithm>
#include <cstdlib>
#include <random>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace bb
{
template < typename T >
T
max( const T& arg1, const T& arg2 )
{
    return std::max( arg1, arg2 );
}

template < typename T, typename... Targs >
T
max( const T& arg, const Targs&... remainder )
{
    return std::max( arg, ::el3D::bb::max( remainder... ) );
}

template < typename T >
T
min( const T& arg1, const T& arg2 )
{
    return std::min( arg1, arg2 );
}

template < typename T, typename... Targs >
T
min( const T& arg, const Targs&... remainder )
{
    return std::min( arg, ::el3D::bb::min( remainder... ) );
}

template < typename T, typename Func >
void
for_each( T& container, const Func& func )
{
    std::for_each( container.begin(), container.end(), func );
}

template < typename T >
void
sort( T& container )
{
    std::sort( container.begin(), container.end() );
}

template < typename T, typename Func >
void
sort( T& container, const Func& func )
{
    std::sort( container.begin(), container.end(), func );
}

template < typename T, typename Value_T >
typename T::iterator
find( T& container, const Value_T& v )
{
    return std::find( container.begin(), container.end(), v );
}

template < typename T, typename Value_T >
typename T::const_iterator
find( const T& container, const Value_T& v )
{
    return std::find( container.begin(), container.end(), v );
}

template < typename T, typename Predicate_T >
typename T::iterator
find_if( T& container, const Predicate_T& p )
{
    return std::find_if( container.begin(), container.end(), p );
}

template < typename T, typename Predicate_T >
typename T::const_iterator
find_if( const T& container, const Predicate_T& p )
{
    return std::find_if( container.begin(), container.end(), p );
}

template < typename T, typename Func >
typename T::iterator
erase_if( T& container, const Func& func )
{
    auto iter = std::remove_if( container.begin(), container.end(), func );
    if ( iter == container.end() ) return container.end();
    return container.erase( iter );
}

template < typename T, typename Value >
typename T::iterator
erase( T& container, const Value& value )
{
    auto iter = std::remove( container.begin(), container.end(), value );
    if ( iter == container.end() ) return container.end();
    return container.erase( iter );
}

template < typename T, typename ValueType >
void
replace( T& container, const ValueType& oldValue, const ValueType& newValue )
{
    return std::replace( container.begin(), container.end(), oldValue,
                         newValue );
}

template < typename T >
typename T::iterator
min_element( T& container )
{
    return std::min_element( container.begin(), container.end() );
}

template < typename T >
typename T::iterator
max_element( T& container )
{
    return std::max_element( container.begin(), container.end() );
}

template < typename T >
std::pair< typename T::iterator, typename T::iterator >
minmax_element( T& container )
{
    return std::minmax_element( container.begin(), container.end() );
}

template < typename T >
T
randint( T a, T b = std::numeric_limits< T >::max() )
{
    std::random_device                 randomDevice;
    std::mt19937_64                    generator( randomDevice() );
    std::uniform_int_distribution< T > distribution( a, b );

    return distribution( generator );
}

} // namespace bb
} // namespace el3D
