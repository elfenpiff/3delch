#pragma once
#include <cstdint>
#include <errno.h>
#include <type_traits>

namespace el3D
{
namespace bb
{
using errno_t = std::remove_reference_t< decltype( errno ) >;
} // namespace bb
} // namespace el3D
