// macros and stuff which makes the code compileable in windows since the msvc
// compiler fucks some stuff up with weird macros
// pragma once or other include barriers must be avoided here otherwise the
// macros wont work

#ifdef _WIN32
#undef min
#undef max
#undef ERROR
#undef RegisterClass
#undef main
#undef CreateEvent
#undef interface

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef M_PI_2
#define M_PI_2 1.57079632679
#endif
#endif
