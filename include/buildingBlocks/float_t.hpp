#pragma once

#include <cstdint>

/// @brief provides the datatype template <uint8_t Size> float_t where you can
///         define which float length you would like to use. Supported should
///         be - depending on the compiler - the following types:
///             float_t<32>, float_t<64>, float_t<128>

namespace el3D
{
namespace bb
{
namespace internal
{
template < uint8_t Size, uint8_t FloatSize, uint8_t DoubleSize,
           uint8_t LongDoubleSize >
struct FloatSelector_t;

template < uint8_t Size, uint8_t DoubleSize, uint8_t LongDoubleSize >
struct FloatSelector_t< Size, Size, DoubleSize, LongDoubleSize >
{
    using type = float;
};

template < uint8_t Size, uint8_t FloatSize, uint8_t LongDoubleSize >
struct FloatSelector_t< Size, FloatSize, Size, LongDoubleSize >
{
    using type = double;
};

template < uint8_t Size, uint8_t FloatSize, uint8_t DoubleSize >
struct FloatSelector_t< Size, FloatSize, DoubleSize, Size >
{
    using type = long double;
};

constexpr uint8_t floatSize  = sizeof( float ) * 8u;
constexpr uint8_t doubleSize = ( sizeof( double ) * 8u != sizeof( float ) * 8u )
                                   ? sizeof( double ) * 8u
                                   : 0u;
constexpr uint8_t longDoubleSize =
    ( sizeof( long double ) * 8u != sizeof( double ) * 8u )
        ? sizeof( long double ) * 8u
        : 0xff;
} // namespace internal

template < uint8_t Size >
using float_t =
    typename internal::FloatSelector_t< Size, internal::floatSize,
                                        internal::doubleSize,
                                        internal::longDoubleSize >::type;
} // namespace bb
} // namespace el3D
