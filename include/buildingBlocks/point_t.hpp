#pragma once

namespace el3D
{
namespace bb
{
template < typename T, unsigned int N >
struct point_t;

template < typename T >
struct point_t< T, 1 >
{
    T x;

    auto
    operator<=>( const point_t& ) const = default;
};

template < typename T >
struct point_t< T, 2 >
{
    T x;
    T y;

    auto
    operator<=>( const point_t& ) const = default;
};

template < typename T >
struct point_t< T, 3 >
{
    T x;
    T y;
    T z;

    auto
    operator<=>( const point_t& ) const = default;
};

template < typename T >
struct point_t< T, 4 >
{
    T x;
    T y;
    T z;
    T w;

    auto
    operator<=>( const point_t& ) const = default;
};

} // namespace bb
} // namespace el3D
