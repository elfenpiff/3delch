#pragma once

#include <utility>
#include <variant>

namespace el3D
{
namespace bb
{
template < typename T = void >
struct Success
{
    Success( const T& t );

    T value;
};

template <>
struct Success< void >
{
};


template < typename T >
struct Error
{
    Error( const T& t );

    T value;
};


template < typename... >
class elExpected;

namespace internal
{
template < typename ErrorType, typename... ValueType >
class elExpectedBase
{
  protected:
    using storage_t = std::variant< ErrorType, ValueType... >;

    elExpectedBase( storage_t&& store, const bool hasError ) noexcept;

  public:
    elExpectedBase() noexcept = default;
    bool
    HasValue() const noexcept;
    bool
    HasError() const noexcept;

    explicit operator bool() const noexcept;

    const ErrorType&
    GetError() const& noexcept;
    ErrorType&
    GetError() & noexcept;
    const ErrorType&&
    GetError() const&& noexcept;
    ErrorType&&
    GetError() && noexcept;

    ErrorType*
    GetErrorPointer() noexcept;

    ErrorType
    GetErrorOr( const ErrorType& ) const noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        const elExpected< ValueType..., ErrorType >& >
    AndThen( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType >, void >,
        const elExpected< ValueType..., ErrorType > >
    AndThen( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType >& >
    AndThen( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType > >
    AndThen( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType >&& >
    AndThen( const CallableType& call ) && noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
        const elExpected< ValueType..., ErrorType >& >
    OrElse( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType, ErrorType >,
                         void >,
        const elExpected< ValueType..., ErrorType > >
    OrElse( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        const elExpected< ValueType..., ErrorType >& >
    OrElse( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType >, void >,
        const elExpected< ValueType..., ErrorType > >
    OrElse( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
        elExpected< ValueType..., ErrorType >& >
    OrElse( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType, ErrorType >,
                         void >,
        elExpected< ValueType..., ErrorType > >
    OrElse( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType >& >
    OrElse( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType > >
    OrElse( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
        elExpected< ValueType..., ErrorType >&& >
    OrElse( const CallableType& call ) && noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType >, void >,
        elExpected< ValueType..., ErrorType >&& >
    OrElse( const CallableType& call ) && noexcept;

  protected:
    storage_t store;
    bool      hasError{ false };
};

} // namespace internal

template < typename ErrorType >
class elExpected< ErrorType > : public internal::elExpectedBase< ErrorType >
{
  protected:
    using typename internal::elExpectedBase< ErrorType >::storage_t;

  public:
    using value_t = void;
    using error_t = ErrorType;

    elExpected()                    = default;
    elExpected( const elExpected& ) = default;
    elExpected( elExpected&& )      = default;
    ~elExpected()                   = default;

    elExpected&
    operator=( const elExpected& ) = default;
    elExpected&
    operator=( elExpected&& ) = default;

    elExpected( const Success< void >& );
    elExpected( const Error< ErrorType >& error );

    static elExpected
    CreateValue() noexcept;
    template < typename... Targs >
    static elExpected
    CreateError( Targs&&... args ) noexcept;

  private:
    elExpected( storage_t&& store, const bool hasError ) noexcept;
};

template < typename ValueType, typename ErrorType >
class elExpected< ValueType, ErrorType >
    : public internal::elExpectedBase< ErrorType, ValueType >
{
  protected:
    using parent_t = internal::elExpectedBase< ErrorType, ValueType >;
    using typename parent_t::storage_t;

  public:
    using value_t = ValueType;
    using error_t = ErrorType;

    using parent_t::AndThen;

    elExpected()                    = delete;
    elExpected( const elExpected& ) = default;
    elExpected( elExpected&& )      = default;
    ~elExpected()                   = default;

    elExpected&
    operator=( const elExpected& ) = default;
    elExpected&
    operator=( elExpected&& ) = default;

    elExpected( const Success< ValueType >& );
    elExpected( const Error< ErrorType >& error );

    template < typename... Targs >
    static elExpected
    CreateValue( Targs&&... args ) noexcept;
    template < typename... Targs >
    static elExpected
    CreateError( Targs&&... args ) noexcept;

    const ValueType&
    GetValue() const& noexcept;
    ValueType&
    GetValue() & noexcept;
    const ValueType&&
    GetValue() const&& noexcept;
    ValueType&&
    GetValue() && noexcept;
    ValueType*
    GetValuePointer() noexcept;

    ValueType
    GetValueOr( const ValueType& ) const noexcept;

    ValueType&
    operator*() noexcept;
    const ValueType&
    operator*() const noexcept;

    ValueType*
    operator->() noexcept;
    const ValueType*
    operator->() const noexcept;

    template < typename T >
    operator elExpected< T >() noexcept;
    template < typename T >
    operator elExpected< T >() const noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
        const elExpected< ValueType, ErrorType >& >
    AndThen( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType, ValueType >,
                         void >,
        const elExpected< ValueType, ErrorType > >
    AndThen( const CallableType& call ) const& noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
        elExpected< ValueType, ErrorType >& >
    AndThen( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        !std::is_same_v< std::invoke_result_t< CallableType, ValueType >,
                         void >,
        elExpected< ValueType, ErrorType > >
    AndThen( const CallableType& call ) & noexcept;

    template < typename CallableType >
    std::enable_if_t<
        std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
        elExpected< ValueType, ErrorType >&& >
    AndThen( const CallableType& call ) && noexcept;

  private:
    elExpected( storage_t&& store, const bool hasError ) noexcept;
};

template < typename ErrorType >
class elExpected< void, ErrorType > : public elExpected< ErrorType >
{
};

} // namespace bb
} // namespace el3D

#include "buildingBlocks/elExpected.inl"
