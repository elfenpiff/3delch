#pragma once

#include "buildingBlocks/algorithm_extended.hpp"
#include "buildingBlocks/elUniqueIndexVector.hpp"
#include "buildingBlocks/point_t.hpp"

#include <array>
#include <cstdint>
#include <map>
#include <span>
#include <vector>

namespace el3D
{
namespace bb
{
template < typename T >
class elVoxelVolume
{
  public:
    using position_t = point_t< uint64_t, 3 >;
    struct element_t
    {
        template < typename... Args >
        element_t( const position_t& p, Args&&... args ) noexcept;
        auto
        operator<=>( const element_t& rhs ) const noexcept;

        position_t position{ 0, 0, 0 };
        T          data;
    };

    template < typename... Args >
    bool
    Insert( const position_t& p, Args&&... args ) noexcept;
    void
    Remove( const position_t& p ) noexcept;

    T&
    Get( const position_t& p ) noexcept;
    const T&
    Get( const position_t& p ) const noexcept;

    bool
    DoesContainElementAt( const position_t& p ) const noexcept;

    const std::vector< element_t >&
    GetConstContents() const noexcept;
    std::span< element_t >
    GetContents() noexcept;

    void
    SortContents() noexcept;

  private:
    elUniqueIndexVector< element_t > contents;
    std::map< position_t, uint64_t > positionToData;
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/elVoxelVolume.inl"
