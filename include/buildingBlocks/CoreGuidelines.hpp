#pragma once

#include <exception>
#include <iostream>

namespace el3D
{
namespace bb
{
#define Expects( condition )                                                   \
    internal::Requires( "Expected", __FILE__, __LINE__, __FUNCTION__,          \
                        #condition, condition )
#define Ensures( condition )                                                   \
    internal::Requires( "Ensured", __FILE__, __LINE__, __FUNCTION__,           \
                        #condition, condition )
namespace internal
{
inline void
Requires( const char* type, const char* file, const int line,
          const char* function, const char* conditionName,
          const bool condition ) noexcept
{
    if ( !condition )
    {
        std::cerr << type << " condition \"" << conditionName << "\" failed in "
                  << file << ":" << line << " [" << function << "] "
                  << std::endl;
        std::terminate();
    }
}
} // namespace internal
} // namespace bb
} // namespace el3D
