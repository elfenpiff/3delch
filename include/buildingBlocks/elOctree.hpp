#include "buildingBlocks/algorithm_extended.hpp"
#include "math/functions.hpp"

#include <glm/glm.hpp>
#include <map>
#include <memory>
#include <set>
#include <vector>

namespace el3D
{
namespace bb
{
template < typename T >
class elOctree
{
  public:
    struct cube_t
    {
        glm::vec3 center         = glm::vec3( 0.0f, 0.0f, 0.0f );
        float     halfSideLength = 0.0f;
    };

    struct element_t
    {
        element_t( const glm::vec3& point, const T& value ) noexcept;
        glm::vec3 point;
        T         value;
    };

    elOctree( const float minimumHalfSideLength ) noexcept;

    void
    Insert( const glm::vec3& point, const T& value ) noexcept;
    bool
    DoesContain( const glm::vec3& point ) const noexcept;
    bool
    HasNeighbors( const glm::vec3& center,
                  const float      halfSideLength ) const noexcept;
    std::vector< element_t >
    GetNeighbors( const glm::vec3& center,
                  const float      halfSideLength ) const noexcept;

    std::vector< cube_t >
    GetTree() const noexcept;

  private:
    struct node_t
    {
        node_t( const glm::vec3& center, const float halfSideLength ) noexcept;

        bool
        DoesFitInNode( const glm::vec3& point ) const noexcept;

        uint64_t
        AcquireNodeIndexFor( const glm::vec3& point ) const noexcept;

        node_t&
        AcquireNodeFor( const glm::vec3& point ) noexcept;

        bool
        HasLeaves() const noexcept;

        std::vector< node_t* >
        GetOverlappingNodes( const glm::vec3& center,
                             const float      halfSideLength ) const noexcept;
        bool
        IsContainedIn( const glm::vec3& center,
                       const float      halfSideLength ) const noexcept;

        cube_t cube;
        std::map< glm::vec3, T,
                  bool ( * )( const glm::vec3&, const glm::vec3& ) >
            contents{ []( const glm::vec3& r, const glm::vec3& l ) {
                return ( r.x < l.x ) || ( r.x == l.x && r.x < l.x ) ||
                       ( r.x == l.x && r.y == l.y && r.z < l.z );
            } };
        std::unique_ptr< node_t > nodes[8];
    };

    static constexpr uint64_t INVALID_ID =
        std::numeric_limits< uint64_t >::max();

    std::unique_ptr< node_t > root;
    float                     minimumHalfSideLength = 0.0f;
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/elOctree.inl"
