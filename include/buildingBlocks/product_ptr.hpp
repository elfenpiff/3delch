#pragma once

#include <atomic>
#include <functional>

namespace el3D
{
namespace bb
{
template < typename T, typename ExtensionType >
struct CompositeProduct;

template < typename T >
class product_ptr
{
  public:
    using Deleter = std::function< void( T* ) >;

    product_ptr() noexcept = default;
    virtual ~product_ptr() noexcept;

    product_ptr( product_ptr&& rhs ) noexcept;
    product_ptr&
    operator=( product_ptr&& rhs ) noexcept;

    product_ptr( const product_ptr& ) = delete;
    product_ptr&
    operator=( const product_ptr& ) = delete;

    explicit operator bool() const noexcept;

    T&
    operator*() noexcept;

    const T&
    operator*() const noexcept;

    T*
    operator->() noexcept;

    const T*
    operator->() const noexcept;

    T*
    Get() noexcept;

    const T*
    Get() const noexcept;

    void
    Reset() noexcept;

    product_ptr&
    AndThen( const std::function< void( T& ) >& callback ) noexcept;
    const product_ptr&
    AndThen( const std::function< void( const T& ) >& callback ) const noexcept;

    const product_ptr&
    OrElse( const std::function< void() >& callback ) const noexcept;
    product_ptr&
    OrElse( const std::function< void() >& callback ) noexcept;

    template < typename >
    friend class product_ptr;
    template < typename >
    friend class product_guard;
    template < typename >
    friend class shared_product_ptr;
    template < typename... >
    friend class elGenericFactory;

  protected:
    product_ptr( T* const object, const Deleter& deleter ) noexcept;
    template < typename Other >
    product_ptr( product_ptr< Other >& linkedPtr,
                 const Deleter&        deleter ) noexcept;
    product_ptr( T* const object ) noexcept;


  protected:
    T*      data = nullptr;
    Deleter deleter;
};

template < typename T, typename ExtensionType >
class product_ptr< CompositeProduct< T, ExtensionType > >
    : public product_ptr< T >
{
  public:
    using Deleter = typename product_ptr< T >::Deleter;

    product_ptr() noexcept = default;
    product_ptr( T* const object ) noexcept;

    ExtensionType&
    Extension() noexcept;

    const ExtensionType&
    Extension() const noexcept;

    template < typename >
    friend class shared_product_ptr;
    template < typename >
    friend class product_ptr;
    template < typename... >
    friend class elGenericFactory;

  private:
    template < typename Other >
    product_ptr(
        product_ptr< CompositeProduct< Other, ExtensionType > >& linkedPtr,
        const Deleter& deleter ) noexcept;
    product_ptr< ExtensionType > extension;
};

template < typename T >
struct referenceHandle_t
{
    referenceHandle_t() = default;
    referenceHandle_t( const std::function< void( T* ) >& callback,
                       product_ptr< T >&                  ptr ) noexcept;

    std::atomic< uint64_t >     referenceCounter;
    void*                       productPtr;
    std::function< void( T* ) > callback;
};

template < typename T, typename ExtensionType >
struct referenceHandle_t< CompositeProduct< T, ExtensionType > >
{
    referenceHandle_t() = default;
    referenceHandle_t(
        const std::function< void( T* ) >&                   callback,
        product_ptr< CompositeProduct< T, ExtensionType > >& ptr ) noexcept;

    std::atomic< uint64_t >     referenceCounter;
    void*                       productPtr;
    std::function< void( T* ) > callback;
    ExtensionType*              extensionPtr;
};


template < typename T >
class shared_product_ptr
{
  public:
    using Deleter = std::function< void( T* ) >;

    shared_product_ptr() noexcept = default;
    shared_product_ptr( shared_product_ptr&& rhs ) noexcept;
    shared_product_ptr( const shared_product_ptr& rhs ) noexcept;
    ~shared_product_ptr() noexcept;

    shared_product_ptr&
    operator=( shared_product_ptr&& rhs ) noexcept;
    shared_product_ptr&
    operator=( const shared_product_ptr& rhs ) noexcept;

    explicit operator bool() const noexcept;

    T&
    operator*() noexcept;

    const T&
    operator*() const noexcept;

    T*
    operator->() noexcept;

    const T*
    operator->() const noexcept;

    T*
    Get() noexcept;

    const T*
    Get() const noexcept;

    void
    Reset() noexcept;

    shared_product_ptr&
    AndThen( const std::function< void( T& ) >& callback ) noexcept;
    const shared_product_ptr&
    AndThen( const std::function< void( const T& ) >& callback ) const noexcept;

    const shared_product_ptr&
    OrElse( const std::function< void() >& callback ) const noexcept;
    shared_product_ptr&
    OrElse( const std::function< void() >& callback ) noexcept;


    template < typename... >
    friend class elGenericFactory;

  protected:
    template < typename ChildType >
    shared_product_ptr( referenceHandle_t< ChildType >& referenceHandle,
                        const Deleter&                  deleter ) noexcept;

  protected:
    std::atomic< uint64_t >* referenceCounter = nullptr;
    T*                       data             = nullptr;
    Deleter                  deleter;
};

template < typename T, typename ExtensionType >
class shared_product_ptr< CompositeProduct< T, ExtensionType > >
{
  public:
    using Deleter = std::function< void( T* ) >;

    shared_product_ptr() noexcept = default;

    shared_product_ptr( shared_product_ptr&& rhs ) noexcept;
    shared_product_ptr( const shared_product_ptr& rhs ) noexcept;
    ~shared_product_ptr() noexcept;

    shared_product_ptr&
    operator=( shared_product_ptr&& rhs ) noexcept;
    shared_product_ptr&
    operator=( const shared_product_ptr& rhs ) noexcept;

    explicit operator bool() const noexcept;

    T&
    operator*() noexcept;

    const T&
    operator*() const noexcept;

    T*
    operator->() noexcept;

    const T*
    operator->() const noexcept;

    T*
    Get() noexcept;

    const T*
    Get() const noexcept;

    void
    Reset() noexcept;

    ExtensionType&
    Extension() noexcept;

    const ExtensionType&
    Extension() const noexcept;

    shared_product_ptr&
    AndThen( const std::function< void( T& ) >& callback ) noexcept;
    const shared_product_ptr&
    AndThen( const std::function< void( const T& ) >& callback ) const noexcept;

    const shared_product_ptr&
    OrElse( const std::function< void() >& callback ) const noexcept;
    shared_product_ptr&
    OrElse( const std::function< void() >& callback ) noexcept;


    template < typename... >
    friend class elGenericFactory;

  protected:
    template < typename ChildType >
    shared_product_ptr(
        referenceHandle_t< CompositeProduct< ChildType, ExtensionType > >&
                       referenceHandle,
        const Deleter& deleter ) noexcept;

  protected:
    std::atomic< uint64_t >* referenceCounter = nullptr;
    T*                       data             = nullptr;
    Deleter                  deleter;
    ExtensionType*           extension = nullptr;
};

template < typename T >
class product_guard : public product_ptr< T >
{
  public:
    using parent_t = product_ptr< T >;
    using parent_t::parent_t;
    product_guard( product_ptr< T >&& rhs ) noexcept;

  private:
    using parent_t::operator*;
    using parent_t::operator->;
    using parent_t::Get;
};
} // namespace bb
} // namespace el3D

#include "buildingBlocks/product_ptr.inl"
