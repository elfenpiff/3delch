#pragma once

#include "buildingBlocks/algorithm_extended.hpp"
#include "buildingBlocks/product_ptr.hpp"

#include <functional>
#include <map>
#include <memory>
#include <tuple>
#include <vector>

namespace el3D
{
namespace bb
{
namespace internal
{
template < typename T, typename T1, typename... Products >
struct ProductType;

template < typename T, typename SourceType >
std::enable_if_t< std::is_class_v< T >, T* >
CastTo( SourceType* ptr ) noexcept;

template < typename T, typename SourceType >
std::enable_if_t< !std::is_class_v< T >, T* >
CastTo( SourceType* ptr ) noexcept;
} // namespace internal

template < typename T, typename ExtensionType >
struct CompositeProduct;

template < typename... Products >
class elGenericFactory final
{
  public:
    using SelfType_t = elGenericFactory< Products... >;
    template < typename T >
    using Product_t =
        typename internal::ProductType< T, Products... >::Type::External;
    template < typename T >
    using BaseProduct_t =
        typename internal::ProductType< T, Products... >::Type::Internal;
    template < typename T >
    using UnderlyingProduct_t =
        product_ptr< Product_t< typename internal::ProductType<
            T, Products... >::Type::UnderlyingInternal > >;
    template < typename T >
    using UnderlyingUserProduct_t =
        typename internal::ProductType< T,
                                        Products... >::Type::UnderlyingInternal;

    elGenericFactory() noexcept;
    ~elGenericFactory() = default;

    elGenericFactory( const elGenericFactory& ) = delete;
    elGenericFactory( elGenericFactory&& )      = default;

    elGenericFactory&
    operator=( const elGenericFactory& ) = delete;
    elGenericFactory&
    operator=( elGenericFactory&& ) = default;

    static constexpr uint64_t END_POSITION =
        std::numeric_limits< uint64_t >::max();

    template < typename T >
    using UnderlyingInternalProduct_t =
        Product_t< typename internal::ProductType<
            T, Products... >::Type::UnderlyingInternal >;

    template < typename T >
    using OnRemoveCallback_t =
        std::function< void( UnderlyingUserProduct_t< T >* ) >;

    template < typename T, typename... Targs >
    product_ptr< Product_t< T > >
    CreateProductAtPosition( const uint64_t position,
                             Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    product_ptr< Product_t< T > >
    CreateProduct( Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    product_ptr< Product_t< T > >
    CreateProductAtPositionWithOnRemoveCallback(
        const OnRemoveCallback_t< T >& callback, const uint64_t position,
        Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    product_ptr< Product_t< T > >
    CreateProductWithOnRemoveCallback( const OnRemoveCallback_t< T >& callback,
                                       Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    shared_product_ptr< Product_t< T > >
    CreateSharedProduct( Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    shared_product_ptr< Product_t< T > >
    CreateSharedProductAtPosition( const uint64_t position,
                                   Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    shared_product_ptr< Product_t< T > >
    CreateSharedProductWithOnRemoveCallback(
        const OnRemoveCallback_t< T >& callback, Targs&&... args ) noexcept;

    template < typename T, typename... Targs >
    shared_product_ptr< Product_t< T > >
    CreateSharedProductAtPositionWithOnRemoveCallback(
        const OnRemoveCallback_t< T >& callback, const uint64_t position,
        Targs&&... args ) noexcept;


    template < typename T >
    shared_product_ptr< Product_t< T > >
    AcquireSharedProduct( const T& dataReference ) noexcept;

    template < typename T >
    std::vector< product_ptr< BaseProduct_t< T > > >&
    GetProducts() noexcept;

    template < typename T >
    const std::vector< product_ptr< BaseProduct_t< T > > >&
    GetProducts() const noexcept;

  private:
    template < typename T, typename... Targs >
    UnderlyingProduct_t< T >*
    CreateRawPointer( const uint64_t position, Targs&&... args ) noexcept;


  private:
    template < typename T >
    shared_product_ptr< Product_t< T > >
    CreateSharedProductPointer(
        referenceHandle_t< UnderlyingInternalProduct_t< T > >&
            referenceHandle ) noexcept;

    using productContainer_t =
        std::tuple< std::vector< product_ptr< Products > >... >;
    using referenceCounterContainer_t =
        std::tuple< std::map< void*, referenceHandle_t< Products > >... >;

    std::unique_ptr< referenceCounterContainer_t > referenceCounter;
    std::unique_ptr< productContainer_t >          products;
};

} // namespace bb
} // namespace el3D

#include "buildingBlocks/elGenericFactory.inl"
