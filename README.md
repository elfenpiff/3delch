# 3Delch
[CodeStyleConventions](CODESTYLE.md)
If you encounter problems please take a look into the [FAQ](https://gitlab.com/larry.robotics/larry.robotics/blob/master/FAQ.md).

## Building 3Delch
### Prerequisites
You have to install the following packages before compiling 3Delch.
 * cmake >= 3.13
 * lua >= 5.2
 * assimp
 * glew
 * glm
 * sdl2 >= 2.0.5
 * sdl2_image
 * sdl2_ttf
 * googletest
 * googlemock

#### Ubuntu
```
apt install cmake libassimp-dev liblua5.3-dev libglew-dev libglm-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libacl1-dev libgtest-dev libgmock-dev
```

### 3rd Party

Additionally, you need to clone the `3Delch-3rd-party` repository so that you have access to the required font, texture and model files.
```sh
cd 3Delch
git clone https://gitlab.com/el.chris/3delch-3rd-party.git
mv 3delch-3rd-party data
```
This repository has to be stored under ```data``` in the 3Delch root directory.

### Compilation

**Release Mode** If you would like to use it in a normal release build with all the examples and features, optimizied to maximum performance build it like
```sh
cmake -Bbuild -H. -DUSE_TCMALLOC=ON -DBUILD_EXAMPLES=ON -DCMAKE_BUILD_TYPE=Release
cd build
make -j16
```

**Debug Mode** When you would like to build it for developing and
debugging purposes, build it like
```sh
cmake -Bbuild -H. -DUSE_SANITIZER=ON -DBUILD_UNITTESTS=ON -DEXTENDED_CODE_ANALYSIS=ON -DBUILD_EXAMPLES=ON -DCMAKE_BUILD_TYPE=Debug
cd build
make -j16
```

**3Delch utils** If you are using only the utils parts of 3Delch which contain the following libraries
 * utils
 * units
 * lua
 * concurrent
 * logging
 * network

and are not having any third party dependencies you can build it like
```sh
cmake -Bbuild -H. -DONLY_UTILS=ON
cd build
make -j16
```

## Hints and HowTos
The documentation for 3Delch is at the moment very sparse but there does exist a doxygen file if you would like to generate the documentation on your own.
You can find working code examples in the examples folder where every
feature of 3Delch is somehow shown - but not with any kind of documentation (at the moment).
