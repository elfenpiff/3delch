# Bug List
 * Open GuiExample twice in 3Dscene and close the second - this leads to 
    segfault / undefined behavior in elWidget_Graph
 
 * gui when setting initial position with MoveTo instead of SetPosition the
    widget might be not showing up

 * theme if Classname like window_SomeClass does not exist it segfaults

 * if theme entry does not exist it segfaults
 
 * with dpi scaling != 1.0 dropbutton, listbox and table entry size is not
    correct

 * Entry pressing end scrolls not completely to the end

 * SegFault can happen if a guiWidget with a classname is searched in the
    guitheme lua file and this widget with classname is is not found
    > first happened with base_tree in tree. base_tree needs to be in a global
    > wrongly enforced base type - see settings, what if the widget is not
        in the enforced base type config

 * Windows buggy stuff
    > elWidget_Filebrowser
    > elBackTrace
    > elFileSystem (hasTitlebar)
    > HasTitlebar in lua theme and as a function leads to an abort

 * Textbox
    * mouse multi line selection and cursor multi line selection
        > lines flicker

 * Checkbutton is clickable outside of slider, elWidget_ShaderAnimation needs
    to set event texture in shaderTexture and not separately

 * slider button needs a minimum size

 * RenderPipeline deferred, forward, gui (if these renderer are
        initialized in a different order the deferred renderer stays black)

 * if inside windows is moved out out viewable range then the
    scrolling
         jumps, we need to recalculate the scrolling position if a windows is
         moved

 * when scrolling update the mouse hover, see listbox elements when
    scrolling

 * when deselecting the listbox all selected elements
             are in the state default

 * press mouse over table button and leave button leaves him in
         clicked state

 * when dpi scaling is between 1 and 2 (not of integral type) the
         scrollbar button moves faster then the mouse cursor
