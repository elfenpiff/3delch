#version 440

uniform samplerBuffer uniformBuffer;
uniform mat4 viewProjection;

layout(location=0)  in vec3 coord;
layout(location=1)  in vec3 normal;
layout(location=2)  in vec3 tangent;
layout(location=3)  in vec3 bitangent;
layout(location=4)  in vec2 texCoord;

layout(location=5) in vec4 diffuseColor;
layout(location=6) in vec4 specularColor;
layout(location=7) in vec2 shininessAndRoughness;
layout(location=8) in vec4 emissionColor;
layout(location=9) in vec4 eventColor;

layout(location=10) in int objectId;
layout(location=11) in int diffuseId;
layout(location=12) in int specularColorId;
layout(location=13) in int shininessAndRoughnessId;
layout(location=14) in int normalId;
layout(location=15) in int emissionId;

out VSData {
    vec3 position;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;

    vec4 diffuseColor;
    vec4 specularColor;
    vec2 shininessAndRoughness;
    vec4 emissionColor;
    vec4 eventColor;

    flat int diffuseId;
    flat int specularColorId;
    flat int shininessAndRoughnessId;
    flat int normalId;
    flat int emissionId;
} outData;

mat4 GetModelMatrix() {
    return (mat4( texelFetch(uniformBuffer, objectId * 4 + 0),
                  texelFetch(uniformBuffer, objectId * 4 + 1 ),
                  texelFetch(uniformBuffer, objectId * 4 + 2 ),
                  texelFetch(uniformBuffer, objectId * 4 + 3 )));
}

void main() {
    mat4 modelMatrix    = GetModelMatrix();
    outData.position           = (modelMatrix * vec4(coord, 1.0)).xyz;
    outData.texCoord           = texCoord;
    outData.normal             = normalize((vec4(normal, 1.0) * inverse(modelMatrix)).xyz);
    outData.tangent            = normalize((vec4(tangent, 1.0) * inverse(modelMatrix)).xyz);
    outData.bitangent          = normalize((vec4(bitangent, 1.0) * inverse(modelMatrix)).xyz);

    outData.diffuseId               = diffuseId;
    outData.specularColorId         = specularColorId;
    outData.shininessAndRoughnessId = shininessAndRoughnessId;
    outData.normalId                = normalId;
    outData.emissionId              = emissionId;

    outData.diffuseColor            = diffuseColor;
    outData.specularColor           = specularColor;
    outData.shininessAndRoughness   = shininessAndRoughness;
    outData.emissionColor           = emissionColor;
    outData.eventColor              = eventColor;

    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}
