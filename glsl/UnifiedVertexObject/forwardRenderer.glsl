//__geometry__
//!fragment
#version 440
// #define INVALID_TEXTURE_ID 1024 -- defined outside

uniform sampler2DArray textureArray;

in VSData {
    vec3 position;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;

    vec4 diffuseColor;
    vec4 specularColor;
    vec2 shininessAndRoughness;
    vec4 emissionColor;
    vec4 eventColor;

    flat int diffuseId;
    flat int specularColorId;
    flat int shininessAndRoughnessId;
    flat int normalId;
    flat int emissionId;
} inData;

layout(location=0) out vec4 diffuse;
layout(location=1) out vec4 glow;
layout(location=2) out vec3 normal;
layout(location=3) out vec3 position;
layout(location=4) out vec4 material;
layout(location=5) out vec4 event;

// #define MAX_LIGHTS 10
// layout(std140, binding = 0) uniform scene_t {
//     vec3    cameraPosition;
// } scene;
// 
// layout(std140, binding = 1) uniform light_t {
//     vec3    color[MAX_LIGHTS];
//     vec3    direction[MAX_LIGHTS];
//     float   ambientIntensity[MAX_LIGHTS];
//     float   diffuseIntensity[MAX_LIGHTS];
//     int     numberOfLights;
// } light;
// 
// layout(location = 0) out vec4 fragColor;
// layout(location = 1) out vec4 glow;
// layout(location = 2) out vec4 remainder;
// 
// uniform sampler2D diffuse;

vec3 GenerateNormalFromMap() {
    if ( inData.normalId == INVALID_TEXTURE_ID ) {
        return inData.normal;
    }

    // normal maps store value from -1.0 - 1.0 mapped to a range from 0.0 - 1.0
    // restore the normal value
    vec3 normalValue = texture(textureArray, vec3(inData.texCoord, inData.normalId) ).xyz;

    normalValue = normalValue * 2.0 - vec3(1.0);

    // tbn matrix * normal
    return normalize(mat3(inData.tangent, inData.bitangent, inData.normal) * normalValue);
}

void main() {
    diffuse     = (inData.diffuseId == INVALID_TEXTURE_ID) 
                        ? inData.diffuseColor 
                        : texture(textureArray, vec3(inData.texCoord, inData.diffuseId) );
    glow        = (inData.emissionId == INVALID_TEXTURE_ID) 
                       ? inData.emissionColor
                       : texture(textureArray, vec3(inData.texCoord, inData.emissionId));

    vec4 specularColor   = (inData.specularColorId == INVALID_TEXTURE_ID)
                        ? inData.specularColor 
                        : texture(textureArray, vec3(inData.texCoord, inData.specularColorId));

    float metallic  = specularColor.w;
    float roughness = (inData.shininessAndRoughnessId == INVALID_TEXTURE_ID) 
                        ? inData.shininessAndRoughness.y 
                        : texture(textureArray, vec3(inData.texCoord, inData.shininessAndRoughnessId)).y;
    float shininess = (inData.shininessAndRoughnessId == INVALID_TEXTURE_ID)
                        ? inData.shininessAndRoughness.x 
                        : texture(textureArray, vec3(inData.texCoord, inData.shininessAndRoughnessId)).x;

    material    = vec4(metallic, roughness, shininess, (diffuse.w == 0.0) ? 0.0 : 1.0);
    event       = inData.eventColor;

    normal      = GenerateNormalFromMap();
    position    = inData.position;


//    float shininess         = texture(textureArray, vec3(_texCoord, _shininessId)).x;
//    float metallic          = texture(textureArray, vec3(_texCoord, _specularColorId)).w;
//    vec3  specularColor     = texture(textureArray, vec3(_texCoord, _specularColorId)).xyz;
//
//    vec3 ambient = vec3(0.0), lighting = vec3(0.0), specular = vec3(0.0);
//    vec3 viewDirection = normalize(scene.cameraPosition - _position);
//
//    for(int n = 0; n < light.numberOfLights; ++n) {
//        ambient        += light.ambientIntensity[n] * light.color[n];
//
//        lighting       += max(dot(_normal, -light.direction[n]), 0.0) 
//                                * light.color[n];
//
//        vec3 reflectDirection = reflect(light.direction[n], _normal);
//        float specularFactor  = pow(max(dot(viewDirection, reflectDirection), 0.0), shininess);
//
//        specular         += metallic * specularFactor * light.color[n] * specularColor;
//    }
//
//    fragColor = vec4((ambient + lighting + specular), 1.0) * texture(textureArray, vec3(_texCoord, _diffuseId));
//    glow      = vec4(0.0);
//    remainder = vec4(_position, 0.0) + vec4(_normal, 0.0) + vec4(_tangent, 0.0) + vec4(_bitangent, 0.0);
}

//!vertex
//<<vertex.glsl
