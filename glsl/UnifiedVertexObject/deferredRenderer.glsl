//__geometry__
//!fragment
#version 440
// #define INVALID_TEXTURE_ID 1024 -- defined outside

uniform sampler2DArray textureArray;

in VSData {
    vec3 position;
    vec2 texCoord;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;

    vec4 diffuseColor;
    vec4 specularColor;
    vec2 shininessAndRoughness;
    vec4 emissionColor;
    vec4 eventColor;

    flat int diffuseId;
    flat int specularColorId;
    flat int shininessAndRoughnessId;
    flat int normalId;
    flat int emissionId;
} inData;

layout(location=0) out vec4 diffuse;
layout(location=1) out vec3 position;
layout(location=2) out vec3 normal;
layout(location=3) out vec4 material;
layout(location=4) out vec4 glow;
layout(location=5) out vec3 specular;
layout(location=6) out vec4 event;

vec3 GenerateNormalFromMap() {
    if ( inData.normalId == INVALID_TEXTURE_ID ) {
        return inData.normal;
    }

    // normal maps store value from -1.0 - 1.0 mapped to a range from 0.0 - 1.0
    // restore the normal value
    vec3 normalValue = texture(textureArray, vec3(inData.texCoord, inData.normalId) ).xyz;

    normalValue = normalValue * 2.0 - vec3(1.0);

    // tbn matrix * normal
    return normalize(mat3(inData.tangent, inData.bitangent, inData.normal) * normalValue);
}

void main() {
    diffuse     = (inData.diffuseId == INVALID_TEXTURE_ID) 
                        ? inData.diffuseColor 
                        : texture(textureArray, vec3(inData.texCoord, inData.diffuseId) );
    position    = inData.position;
    normal      = GenerateNormalFromMap();

    vec4 specularColor   = (inData.specularColorId == INVALID_TEXTURE_ID)
                        ? inData.specularColor 
                        : texture(textureArray, vec3(inData.texCoord, inData.specularColorId));

    float metallic  = specularColor.w;
    float roughness = (inData.shininessAndRoughnessId == INVALID_TEXTURE_ID) 
                        ? inData.shininessAndRoughness.y 
                        : texture(textureArray, vec3(inData.texCoord, inData.shininessAndRoughnessId)).y;
    float shininess = (inData.shininessAndRoughnessId == INVALID_TEXTURE_ID)
                        ? inData.shininessAndRoughness.x 
                        : texture(textureArray, vec3(inData.texCoord, inData.shininessAndRoughnessId)).x;

    material        = vec4(metallic, roughness, shininess, (diffuse.w == 0.0) ? 0.0 : 1.0);
    specular        = specularColor.xyz;
    event           = inData.eventColor;

    glow = (inData.emissionId == INVALID_TEXTURE_ID) 
                ? inData.emissionColor
                : texture(textureArray, vec3(inData.texCoord, inData.emissionId));
}

//!vertex
//<<vertex.glsl


//__directionalLightShadow__
//!fragment
#version 440

out vec4 directionalLight;

void main() {
    directionalLight = vec4( gl_FragCoord.z, 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform samplerBuffer uniformBuffer;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=5) in int objectId;

mat4 GetModelMatrix() {
    return (mat4( texelFetch(uniformBuffer, objectId * 4 + 0),
                  texelFetch(uniformBuffer, objectId * 4 + 1 ),
                  texelFetch(uniformBuffer, objectId * 4 + 2 ),
                  texelFetch(uniformBuffer, objectId * 4 + 3 )));
}

void main() {
    mat4 modelMatrix    = GetModelMatrix();
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}



//__pointLightShadow__
//!fragment
#version 440

uniform vec3 lightPosition;

in vec3 positionInWorld;
out vec4 pointLight;

void main() {
    pointLight = vec4(length(positionInWorld - lightPosition), 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform samplerBuffer uniformBuffer;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=5) in int objectId;

out vec3 positionInWorld;

mat4 GetModelMatrix() {
    return (mat4( texelFetch(uniformBuffer, objectId * 4 + 0),
                  texelFetch(uniformBuffer, objectId * 4 + 1 ),
                  texelFetch(uniformBuffer, objectId * 4 + 2 ),
                  texelFetch(uniformBuffer, objectId * 4 + 3 )));
}

void main() {
    mat4 modelMatrix    = GetModelMatrix();
    positionInWorld     = (modelMatrix * vec4(coord, 1.0)).xyz;
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}
