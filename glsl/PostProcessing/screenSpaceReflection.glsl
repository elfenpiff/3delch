//__default__
//!fragment
#version 440

//<<../Light/VogelDiskSample.glsl

uniform sampler2D finalImage;
uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D materialTexture;
uniform samplerCube skybox;

uniform mat4 invView;
uniform mat4 projection;
uniform mat4 view;
uniform int hasSkyboxAttached;
uniform vec3 cameraPosition;
uniform vec2 resolution;

noperspective in vec2 _texCoord;
vec2 texCoord = -_texCoord;

out vec4 outColor;

const float stepSize = 0.1;
const float minRayStepSize = 0.1;
const float maxNumberOfSteps = 30;
const int   binarySearchIterations = 5;
const float reflectionSpecularFalloffExponent = 3.0;
const int   ssrTexturePasses = 8;

// require screen resolution
const float roughnessSamplingFactor = max(1.0/resolution.x, 1.0/resolution.y) * 30.0;

vec2 ProjectionCoordinates(in vec3 positionInWorld) 
{
    vec4 projectedCoord = projection * vec4(positionInWorld, 1.0);
    projectedCoord.xy /= projectedCoord.w;
    projectedCoord.xy = projectedCoord.xy * 0.5 + 0.5;
    return projectedCoord.xy;
}

float GetDepth(in vec2 coord)
{
    return (view * texture(position, coord)).z;
}

vec2 BinarySearch(inout vec3 dir, inout vec3 hitCoord)
{
    float depth         = 0.0;
    vec2 projectedCoord = vec2(0.0);
 
    for(int i = 0; i < binarySearchIterations; ++i)
    {
        projectedCoord = ProjectionCoordinates(hitCoord); 

        depth = GetDepth(projectedCoord);

        dir *= 0.5;
        if(hitCoord.z - depth > 0.0)    hitCoord += dir;
        else                            hitCoord -= dir;
    }

    projectedCoord = ProjectionCoordinates(hitCoord); 
 
    return projectedCoord;
}
 
vec2 RayMarch(inout vec3 dir, inout vec3 hitCoord)
{
    dir *= stepSize;
 
    float depth         = 0.0;
    float dDepth        = 0.0;
    vec2 projectedCoord = vec2(0.0);
 
    for(int i = 0; i < maxNumberOfSteps; i++)
    {
        hitCoord += dir;
        projectedCoord = ProjectionCoordinates(hitCoord); 
 
        depth = GetDepth(projectedCoord);
        if(depth > 1000.0) continue;
 
        float dDepth = hitCoord.z - depth;

        if(dDepth <= 0.0 && (dir.z - dDepth) < 1.2)
        {
            return BinarySearch(dir, hitCoord);
        }
    }

    return projectedCoord;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

void main()
{
    float metallic = pow(texture(materialTexture, texCoord).x, 0.33);

    if ( metallic < 0.01 ) discard;

    vec3 viewNormal =   normalize((texture(normal, texCoord) * invView).xyz);
    vec3 viewPosition = (view * texture(position, texCoord)).xyz;

    vec3 albedo = texture(finalImage, texCoord).rgb;

    float roughness = texture(materialTexture, texCoord).y;

    vec3 F0 = mix(vec3(0.04), albedo, metallic);
    vec3 Fresnel = fresnelSchlick(max(dot(viewNormal, normalize(viewPosition)), 0.0), F0);

    // Reflection vector
    vec3 reflected = normalize(reflect(viewPosition, viewNormal));

    vec3 direction = reflected * max(minRayStepSize, -viewPosition.z);
    vec3 hitCoord = viewPosition;
    vec2 coords = RayMarch(direction, hitCoord);

    vec2 dCoords = smoothstep(0.2, 0.6, abs(vec2(0.5, 0.5) - coords));

    float screenEdgefactor = clamp(1.0 - (dCoords.x + dCoords.y), 0.0, 1.0);

    float ReflectionMultiplier = clamp(pow(metallic, reflectionSpecularFalloffExponent) * 
                screenEdgefactor * -reflected.z, 0.0, 0.9);


    vec3 I = normalize(texture(position, texCoord).xyz - cameraPosition);
    vec3 N = normalize((texture(normal, texCoord)).xyz);
    vec3 skyboxReflection = reflect(I, N);

    vec3 reflectedOrtho1 = vec3(skyboxReflection.z, -skyboxReflection.x, skyboxReflection.y);
    reflectedOrtho1 = normalize(reflectedOrtho1 - dot(skyboxReflection, reflectedOrtho1) / dot(skyboxReflection, skyboxReflection) * skyboxReflection);
    vec3 reflectedOrtho2 = normalize(cross(skyboxReflection, reflectedOrtho1));

    // Get color
    vec3 screenSpaceReflection = vec3(0.0);

    for(int i = 0; i < ssrTexturePasses; ++i) {
        vec2 vogelDiskSample = VogelDiskSample(i, ssrTexturePasses);
        vec2 onScreenCoordinates = coords + vogelDiskSample * roughness * roughnessSamplingFactor;
        screenSpaceReflection += texture(finalImage, onScreenCoordinates ).xyz * ReflectionMultiplier * Fresnel / float(ssrTexturePasses);

        if ( hasSkyboxAttached == 1 ) {
            vec3 r = skyboxReflection + (reflectedOrtho1 * vogelDiskSample.x + reflectedOrtho2 * vogelDiskSample.y) * roughness * roughnessSamplingFactor;
            screenSpaceReflection += texture(skybox, r).xyz * clamp(0.9 - 3.0 * screenEdgefactor * -reflected.z, 0.0, 1.0) / float(ssrTexturePasses);

        }
    }

    outColor = vec4(screenSpaceReflection, metallic);
}





