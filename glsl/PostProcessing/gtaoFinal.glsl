//__default__
//!fragment
#version 440

uniform sampler2D diffuse;
uniform sampler2D gtao;
uniform float scaling;

out vec4 fragColor;

vec3 MultiBounce(float ao, vec3 albedo)
{
    vec3 x = vec3(ao);

    vec3 a = 2.0404 * albedo - vec3(0.3324);
    vec3 b = -4.7951 * albedo + vec3(0.6417);
    vec3 c = 2.7552 * albedo + vec3(0.6903);

    return max(x, ((x * a + b) * x + c) * x);
}

void main()
{
    ivec2 loc = ivec2(gl_FragCoord.xy);
    ivec2 locao = ivec2(gl_FragCoord.xy * scaling);

    vec4 base = texelFetch(diffuse, loc, 0);
    float ao = texelFetch(gtao, locao, 0).r;

    fragColor.rgb = base.rgb * MultiBounce(ao, base.rgb);
    fragColor.a = base.a;
}
