//__default__
//!fragment
#version 440

uniform sampler2D gtao;
uniform sampler2D depth;

uniform vec2 invertedResolution;

out vec4 fragColor;

float BlurFunction(inout float totalWeight, vec2 uv, float depthAtCenter)
{
    float ao = texture(gtao, uv * invertedResolution).r;
    float depthAtUV = texture(depth, uv * invertedResolution).r;

    float weight = max(0.0, 0.1 - abs(depthAtUV - depthAtCenter)) * 30.0;
    totalWeight += weight;

    return ao * weight;
}

void main()
{
    vec2 center = gl_FragCoord.xy - vec2(2.0);
    ivec2 loc = ivec2(center);

    float totalao = texelFetch(gtao, loc, 0).r;
    float depthAtCenter = texelFetch(depth, loc, 0).r;
    float totalWeight = 1.0;

    totalao += BlurFunction(totalWeight, center + vec2(1, 0), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(2, 0), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(3, 0), depthAtCenter);

    totalao += BlurFunction(totalWeight, center + vec2(0, 1), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(1, 1), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(2, 1), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(3, 1), depthAtCenter);

    totalao += BlurFunction(totalWeight, center + vec2(0, 2), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(1, 2), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(2, 2), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(3, 2), depthAtCenter);

    totalao += BlurFunction(totalWeight, center + vec2(0, 3), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(1, 3), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(2, 3), depthAtCenter);
    totalao += BlurFunction(totalWeight, center + vec2(3, 3), depthAtCenter);

    fragColor = vec4(totalao / totalWeight, 0.0, 0.0, 1.0);
}
