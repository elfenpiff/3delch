//__default__
//!fragment
#version 440

#define PI                  3.141592
#define PI_HALF             1.570796

#define NUM_STEPS           8
#define RADIUS              0.2 * lengthOfOneMeter
#define FALLOFF_START2      0.006 * lengthOfOneMeter
#define FALLOFF_END2        0.4 * lengthOfOneMeter

uniform sampler2D gbufferDepth;
uniform sampler2D gbufferNormals;
uniform sampler2D noise;

uniform float lengthOfOneMeter;
uniform vec4 projInfo;
uniform vec4 clipInfo;
uniform vec2 invertedResolution;
uniform mat4 inverseView;

out vec4 fragColor;

in vec2 _texCoord;

int numberOfVariations = 1; // adjust quality by setting it from 1 to 6
float rotations[6] = {0.0, 180.0, 60.0, 120., 240., 300.};
float offsets[6]   = {0.833, 0.666, 0.5, 0.333, 0.166, 0.0};

float GetDepth(in vec2 uv) 
{
    float z_b = texture(gbufferDepth, uv).r;
    float z_n = 2.0 * z_b - 1.0;
    float z_e = 2.0 * clipInfo[0] * clipInfo[1] / (clipInfo[0] + clipInfo[1] - z_n * (clipInfo[1] - clipInfo[0]));

    float depth = z_e / (clipInfo[0] + clipInfo[1]);
    return depth;
}

vec4 GetViewPosition(vec2 uv)
{
    float d = GetDepth(uv * invertedResolution);
    vec4 ret = vec4(0.0, 0.0, 0.0, d);

    ret.z = clipInfo.x + d * (clipInfo.y - clipInfo.x);
    ret.xy = (uv * projInfo.xy + projInfo.zw) * ret.z;
    return ret;
}

float Falloff(float dist2, float cosh)
{
    return 2.0 * clamp((dist2 - FALLOFF_START2) / (FALLOFF_END2 - FALLOFF_START2), 0.0, 1.0);
}

void CalculateHorizonAngle(in vec2 uv, in vec4 vpos, in vec3 vdir, inout float h) {
    vec4 s = GetViewPosition(uv);
    vec3  ws = s.xyz - vpos.xyz;

    float dist2 = dot(ws, ws);
    float invdist = inversesqrt(dist2);
    float cosh = invdist * dot(ws, vdir);

    float falloff = Falloff(dist2, cosh);
    h = max(h, cosh - falloff);
}

void main()
{
    vec2 uv = (vec2(1.0) -_texCoord) / invertedResolution;
    ivec2 loc = ivec2(gl_FragCoord.xy);
    vec4 vpos = GetViewPosition(uv);

    vec4 normal = vec4(texelFetch(gbufferNormals, loc * int(clipInfo[3]), 0).xyz, 0.0);
    if (normal.xyz == vec3(0.0))
    {
        fragColor = vec4(1.0);
        return;
    }

    vec4 s;
    vec3 vnorm    = (normalize(normal) * inverseView).rgb;
    vec3 vdir    = normalize(-vpos.xyz);
    vec3 dir, ws;

    // calculation uses left handed system
    vnorm.z = -vnorm.z;

    vec2 noises    = texelFetch(noise, loc % 4, 0).rg;
    vec2 horizons = vec2(-1.0);

    float radius = (RADIUS * clipInfo.z) / vpos.z;

    radius = max(NUM_STEPS, radius);

    float stepsize    = radius / NUM_STEPS;
    float ao          = 0.0;
    float division    = noises.y * stepsize;

    for(int i = 0; i < numberOfVariations; ++i ) {
        float phi           = (rotations[i] + noises.x) * PI;
        float currstep      = 1.0 + division + 0.25 * stepsize * offsets[i];
        float dist2, invdist, falloff, cosh;

        dir = vec3(cos(phi), sin(phi), 0.0);
        horizons = vec2(-1.0);

        // calculate horizon angles
        for( int j = 0; j < NUM_STEPS; ++j ) {
            vec2 offset      = round(dir.xy * currstep);
            currstep        += stepsize;

            CalculateHorizonAngle(uv + offset, vpos, vdir, horizons.x );
            CalculateHorizonAngle(uv - offset, vpos, vdir, horizons.y );
        }

        horizons = acos(horizons);

        // calculate gamma
        vec3 bitangent      = normalize(cross(dir, vdir));
        vec3 tangent        = cross(vdir, bitangent);
        vec3 nx             = vnorm - bitangent * dot(vnorm, bitangent);

        float nnx           = length(nx);
        float invnnx        = 1.0 / (nnx + 1e-6);            // to avoid division with zero
        float cosxi         = dot(nx, tangent) * invnnx;    // xi = gamma + PI_HALF
        float gamma         = acos(cosxi) - PI_HALF;
        float cosgamma      = dot(nx, vdir) * invnnx;
        float singamma2     = -2.0 * cosxi;                    // cos(x + PI_HALF) = -sin(x)

        // clamp to normal hemisphere
        horizons.x = gamma + max(-horizons.x - gamma, -PI_HALF);
        horizons.y = gamma + min(horizons.y - gamma, PI_HALF);

        // Riemann integral is additive
        float factor = nnx * 0.25 * (
            (horizons.x * singamma2 + cosgamma - cos(2.0 * horizons.x - gamma)) +
            (horizons.y * singamma2 + cosgamma - cos(2.0 * horizons.y - gamma)));

        ao += factor;
    }

    ao = ao / numberOfVariations;

    fragColor = vec4(ao, 0.0, 0.0, 1.0);
}
