//__default__
//!fragment
#version 440

void main() {
}

//!vertex
#version 440

layout(location = 0) in vec3 coord;
uniform mat4 vp;

void main() {
    gl_Position = vp * vec4(coord, 1.0);
}
