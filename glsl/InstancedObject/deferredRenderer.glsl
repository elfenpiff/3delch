//__geometry__
//!fragment
#version 440

uniform sampler2D diffuse;

in vec3 _position;
in vec3 _normal;
in vec3 _tangent;
in vec3 _bitangent;
in vec2 _texCoord;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec3 position;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec2 texCoord;
layout(location = 4) out vec3 tangent;
layout(location = 5) out vec3 bitangent;

void main()
{
    fragColor   = texture(diffuse, _texCoord);
    position    = _position;
    normal      = _normal;
    texCoord    = _texCoord;
    tangent     = _tangent;
    bitangent   = _bitangent;
}

//!vertex
#version 440

uniform mat4 viewProjection;
layout (location = 0) in vec3 coord;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelMatrix;
layout (location = 4) in vec3 tangent;
layout (location = 5) in vec3 bitangent;

out vec3 _position;
out vec3 _normal;
out vec2 _texCoord;
out vec3 _tangent;
out vec3 _bitangent;

void main()
{
    _position   = (modelMatrix * vec4(coord, 1.0)).xyz;
    _texCoord   = texCoord;
    _normal     = normalize((vec4(normal, 1.0) * inverse(modelMatrix)).xyz);
    _tangent    = normalize((vec4(tangent, 1.0) * inverse(modelMatrix)).xyz);
    _bitangent  = normalize((vec4(bitangent, 1.0) * inverse(modelMatrix)).xyz);

    gl_Position = viewProjection * modelMatrix * vec4(coord, 1.0);
}


//__directionalLightShadow__
//!fragment
#version 440

out vec4 directionalLight;

void main() {
    directionalLight = vec4( gl_FragCoord.z, 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=3) in mat4 modelMatrix;

void main() {
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}


//__pointLightShadow__
//!fragment
#version 440

uniform vec3 lightPosition;

in vec3 positionInWorld;
out vec4 pointLight;

void main() {
    pointLight = vec4(length(positionInWorld - lightPosition), 0.0, 0.0, 1.0);
}

//!vertex
#version 440

uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=3) in mat4 modelMatrix;

out vec3 positionInWorld;

void main() {
    positionInWorld     = (modelMatrix * vec4(coord, 1.0)).xyz;
    gl_Position         = viewProjection * modelMatrix * vec4(coord, 1.0);
}
