//__default__
//!fragment
#version 440

uniform sampler2D diffuse;
uniform sampler2D gui;
uniform sampler2D guiEvent;
uniform sampler2D guiGlow;
uniform sampler2D glow;
uniform sampler2D screenSpaceReflection;

uniform sceneSettings_t {
    ivec2 resolution;
} sceneSettings;

out vec4 finalImage;
out vec4 event;

void main() {
    vec2 texCoord   = gl_FragCoord.xy / sceneSettings.resolution;

    // curved
    //vec2 guiCoord   = texCoord;
    //float factor    = (texCoord.x - 0.5) * 0.4;
    //guiCoord.y      = texCoord.y - factor * factor * (texCoord.y - 0.5);
    //guiCoord.y      = guiCoord.y * 1.04 - 0.02;
    //vec4 guiImage   = texture(gui, guiCoord) 
    //        * smoothstep(0.0, 0.002, guiCoord.y) 
    //        * smoothstep(0.0, 0.002, 1.0 - guiCoord.y);

    vec4 guiImage       = texture(gui, texCoord);
    vec4 guiGlowValue   = texture(guiGlow, texCoord);
    vec4 glowValue      = vec4(texture(glow, texCoord).xyz, 1.0);

    event               = texture(guiEvent, texCoord);
    vec4 diffuseColor   = texture(diffuse, texCoord);
    vec4 reflection     = texture(screenSpaceReflection, texCoord);
    diffuseColor        += reflection;
    finalImage          = (diffuseColor + glowValue) 
                            * ( 1.0 - guiImage.a) + guiImage + guiGlowValue;
//    finalImage = texture(screenSpaceReflection, texCoord);
}

//!vertex
#version 440

in vec3 coord;

void main() {
    gl_Position = vec4(coord, 1.0);
}

