//__default__
//!fragment
#version 440

uniform sampler2DArray textureArray;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;

in GSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} inData;

void main() {
    fragColor = inData.diffuseColor;
    glow      = inData.emissionColor;
}

//!vertex
#version 440

uniform samplerBuffer uniformBuffer;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=1) in vec3 normal;
layout(location=2) in vec3 tangent;
layout(location=3) in vec3 bitangent;
layout(location=4) in vec2 texCoord;
layout(location=5) in vec4 diffuseColor;
layout(location=8) in vec4 emissionColor;
layout(location=10) in int objectId;

out VSData {
    vec3 worldPosition;
    vec4 diffuseColor;
    vec4 emissionColor;
} outData;

mat4 GetModelMatrix() {
    return (mat4( texelFetch(uniformBuffer, objectId * 4 + 0),
                  texelFetch(uniformBuffer, objectId * 4 + 1 ),
                  texelFetch(uniformBuffer, objectId * 4 + 2 ),
                  texelFetch(uniformBuffer, objectId * 4 + 3 )));
}

void main() {
    mat4 modelMatrix        = GetModelMatrix();

    outData.worldPosition   = (modelMatrix * vec4(coord, 1.0)).xyz;
    outData.diffuseColor    = diffuseColor;
    outData.emissionColor   = emissionColor;
    gl_Position             = viewProjection * modelMatrix * vec4(coord, 1.0);
}

//!geometry
#version 440 

layout (triangles_adjacency) in;
layout (line_strip, max_vertices=6) out;

uniform vec3 cameraPosition;

in VSData {
    vec3 worldPosition;
    vec4 diffuseColor;
    vec4 emissionColor;
} inData[];

out GSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} outData;

void main()
{
    outData.diffuseColor = inData[0].diffuseColor;
    outData.emissionColor = inData[0].emissionColor;

    vec4 v0 = vec4(inData[0].worldPosition, 1.0);
    vec4 v1 = vec4(inData[1].worldPosition, 1.0);
    vec4 v2 = vec4(inData[2].worldPosition, 1.0);
    vec4 v3 = vec4(inData[3].worldPosition, 1.0);
    vec4 v4 = vec4(inData[4].worldPosition, 1.0);
    vec4 v5 = vec4(inData[5].worldPosition, 1.0);

    vec3 n042 = cross(v4.xyz - v0.xyz, v2.xyz - v0.xyz);

    vec3 viewDirection = cameraPosition - v0.xyz;

    if ( dot(viewDirection, n042) >= 0. ) {
        vec3 n021 = cross(v2.xyz - v0.xyz, v1.xyz - v0.xyz);
        vec3 n243 = cross(v4.xyz - v2.xyz, v3.xyz - v2.xyz);
        vec3 n405 = cross(v0.xyz - v4.xyz, v5.xyz - v4.xyz);

        viewDirection = cameraPosition - v1.xyz;
        if ( dot(viewDirection, n021) <= 0.) {
            gl_Position = gl_in[0].gl_Position;
            EmitVertex();
            gl_Position = gl_in[2].gl_Position;
            EmitVertex();
            EndPrimitive();
        }

        viewDirection = cameraPosition - v3.xyz;
        if ( dot(viewDirection, n243) <= 0.) {
            gl_Position = gl_in[2].gl_Position;
            EmitVertex();
            gl_Position = gl_in[4].gl_Position;
            EmitVertex();
            EndPrimitive();
        }
 
        viewDirection = cameraPosition - v5.xyz;
        if ( dot(viewDirection, n405) <= 0.) {
            gl_Position = gl_in[4].gl_Position;
            EmitVertex();
            gl_Position = gl_in[0].gl_Position;
            EmitVertex();
            EndPrimitive();
        }
    }
}
