//__default__
//!fragment
#version 440

uniform sampler2DArray textureArray;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;

in GSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} inData;

void main() {
    fragColor = inData.diffuseColor;
    glow      = inData.emissionColor;
}

//!vertex
#version 440

uniform samplerBuffer uniformBuffer;
uniform mat4 viewProjection;

layout(location=0) in vec3 coord;
layout(location=1) in vec3 normal;
layout(location=2) in vec3 tangent;
layout(location=3) in vec3 bitangent;
layout(location=4) in vec2 texCoord;
layout(location=5) in vec4 diffuseColor;
layout(location=8) in vec4 emissionColor;
layout(location=10) in int objectId;

out VSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} outData;

mat4 GetModelMatrix() {
    return (mat4( texelFetch(uniformBuffer, objectId * 4 + 0),
                  texelFetch(uniformBuffer, objectId * 4 + 1 ),
                  texelFetch(uniformBuffer, objectId * 4 + 2 ),
                  texelFetch(uniformBuffer, objectId * 4 + 3 )));
}

void main() {
    mat4 modelMatrix        = GetModelMatrix();

    outData.diffuseColor    = diffuseColor;
    outData.emissionColor   = emissionColor;
    gl_Position             = viewProjection * modelMatrix * vec4(coord, 1.0);
}

//!geometry
#version 440 

layout (triangles) in;
//layout (points, max_vertices=3) out;
layout (line_strip, max_vertices=3) out;

in VSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} inData[];

out GSData {
    vec4 diffuseColor;
    vec4 emissionColor;
} outData;

void main()
{
    outData.diffuseColor  = inData[0].diffuseColor;
    outData.emissionColor = inData[0].emissionColor;
    for (int i = 0; i < gl_in.length(); i++)
    {
        gl_Position     = gl_in[i].gl_Position; 
        EmitVertex();
    }
    EndPrimitive();
}
