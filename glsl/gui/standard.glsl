//__geometry__
//!vertex
#version 440

layout(location = 0) in vec3 coord;
out vec2 _coord;

//<<common.glsl

void main() {
    // needs to be super tight and absolutely fitting otherwise to much is 
    // drawn which is never be presented and this will cost performance
    vec2 xyCoord    = 2.0 * (coord.xy * setting.size * 0.5 + setting.position -
                        vec2(-setting.size.x * 0.5, setting.size.y * 0.5) ) - vec2(1.0);
    _coord          = (xyCoord + vec2(1.0)) / 2.0;
    gl_Position     = vec4(xyCoord, 0.0, 1.0);
}

//!fragment
#version 440

uniform sampler2D image;

layout (location=0) out vec4 fragColor;
layout (location=1) out vec4 event;

//<<common.glsl
in vec2 _coord;
//<<common_Fragment.glsl

void main() {
    if ( setting.isEmpty == 1 ) { discard; }
    RenderOnlyInViewableArea(_coord);

    fragColor   = vec4(0.0);
    event       = vec4(0.0);

    fragColor   = SetPositionDependingColors(setting.borders, _coord, setting.color, setting.colorAccent);
    event       = SetPositionDependingEventColors(setting.borders + setting.borderEventIncrease, _coord);

    if ( setting.doReadTexture != 0 ) {
        vec2 texturePosition = ( _coord - setting.position ) / setting.textureSize;
        if ( texturePosition.x < 1.0 && texturePosition.y > -1.0 ) {
            if ( setting.doInvertTextureCoordinates != 0 ) {
                fragColor += texture(image, vec2(1.0, 1.0) * texturePosition );
            } else {
                fragColor += texture(image, vec2(1.0, -1.0) * texturePosition );
            }
        }
    } 
}




//__glow__
//!vertex
#version 440

layout(location = 0) in vec3 coord;
out vec2 _coord;

//<<common.glsl

void main() {
    // needs to be super tight and absolutely fitting otherwise to much is 
    // drawn which is never be presented and this will cost performance
    vec2 xyCoord    = 2.0 * (coord.xy * setting.size * 0.5 + setting.position -
                        vec2(-setting.size.x * 0.5, setting.size.y * 0.5) ) - vec2(1.0);
    _coord          = (xyCoord + vec2(1.0)) / 2.0;
    gl_Position     = vec4(xyCoord, 0.0, 1.0);
}

//!fragment
#version 440

layout (location=0) out vec4 fragColor;

//<<common.glsl
in vec2 _coord;
//<<common_Fragment.glsl

void main() {
    if ( setting.isEmpty == 1 ) { discard; }
    RenderOnlyInViewableArea(_coord);

    fragColor   = SetScaledPositionDependingColors(setting.borders, setting.glowScaling, _coord, setting.glowColor, setting.glowColorAccent);
    fragColor  += vec4(0.0, 0.0, 0.0, 1.0);
}
