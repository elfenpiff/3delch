#define MAX_GRAPH_VALUES 4096

uniform graph_t 
{
    float values[MAX_GRAPH_VALUES];
    int numberOfValues;
    vec2 yRange;
}
graph;
