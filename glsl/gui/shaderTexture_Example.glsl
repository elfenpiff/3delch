//__default__
//!fragment
#version 440

//<<common.glsl

layout (location=0) out vec4 fragColor;
in vec2 _texCoord;

void main() {
    fragColor = vec4(_texCoord, 1.0, 1.0);
}
