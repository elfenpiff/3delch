#define WIDGET_OUTSIDE         -1
#define WIDGET_INNER            0
#define WIDGET_TOP              1
#define WIDGET_BOTTOM           2
#define WIDGET_LEFT             3
#define WIDGET_RIGHT            4
#define WIDGET_TOP_LEFT         5
#define WIDGET_TOP_RIGHT        6
#define WIDGET_BOTTOM_LEFT      7
#define WIDGET_BOTTOM_RIGHT     8

struct position_t {
    float top;
    float bottom;
    float left;
    float right;
    float topLeft;
    float topRight;
    float bottomLeft;
    float bottomRight;
    float inner;
};

// middle \in [small, large]
bool SetOf(in float small, in float middle, in float large) {
    return small <= middle && middle <= large;
}

float HasIncreasingValues(in float small, in float middle, in float large) {
    return step(small, middle) * step(middle, large);
}

void RenderOnlyInViewableArea(in vec2 uv) {
    if( !( SetOf(setting.viewPort.z, uv.x, setting.viewPort.w ) &&
           SetOf(setting.viewPort.y, uv.y, setting.viewPort.x ))) {
        discard;
    }
}

position_t GetPositions(vec4 adjustedBorder, in vec2 uv) {
    position_t value;

    value.top           = HasIncreasingValues(setting.position.y - adjustedBorder.x, uv.y, setting.position.y) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z, uv.x, setting.position.x + setting.size.x - adjustedBorder.w);

    value.bottom        = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z, uv.x, setting.position.x + setting.size.x - adjustedBorder.w);

    value.left          = HasIncreasingValues(setting.position.y - setting.size.y + adjustedBorder.y, uv.y, setting.position.y - adjustedBorder.x) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z);

    value.right         = HasIncreasingValues(setting.position.y - setting.size.y + adjustedBorder.y, uv.y, setting.position.y - adjustedBorder.x) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w, uv.x, setting.position.x + setting.size.x);

    value.topLeft       = HasIncreasingValues(setting.position.y - adjustedBorder.x, uv.y, setting.position.y ) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z);
    value.topRight      = HasIncreasingValues(setting.position.y - adjustedBorder.x, uv.y, setting.position.y ) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w, uv.x, setting.position.x + setting.size.x);
    value.bottomLeft    = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y ) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z);
    value.bottomRight   = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y ) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w, uv.x, setting.position.x + setting.size.x);
    value.inner         = HasIncreasingValues(setting.position.y + adjustedBorder.y - setting.size.y, uv.y, setting.position.y - adjustedBorder.x ) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z, uv.x, setting.position.x + setting.size.x - adjustedBorder.w);

    return value;
}

vec4 SetPositionDependingColors(vec4 adjustedBorder, in vec2 uv, in vec4 color, in vec4 accentColor)
{
    position_t pos = GetPositions(adjustedBorder, uv);

    vec4 widgetColor = (pos.top + pos.bottom + pos.left + pos.right + 
                        pos.topLeft + pos.topRight + pos.bottomLeft + pos.bottomRight ) 
                        * accentColor + pos.inner * color;
    widgetColor *= setting.doDrawColors;
    return widgetColor;
}

vec4 SetPositionDependingEventColors(vec4 adjustedBorder, in vec2 uv) {
    position_t pos = GetPositions(adjustedBorder, uv);

    vec4 widgetEventColor =  pos.top * setting.eventColorTop + pos.bottom * setting.eventColorBottom
                        + pos.left * setting.eventColorLeft + pos.right * setting.eventColorRight
                        + pos.topLeft * setting.eventColorTopLeft + pos.topRight * setting.eventColorTopRight
                        + pos.bottomLeft * setting.eventColorBottomLeft + pos.bottomRight * setting.eventColorBottomRight
                        + pos.inner * setting.eventColorInner;
    widgetEventColor *= (1.0 - setting.doDrawEventColor);
    return widgetEventColor;
}

position_t GetScaledPositions(vec4 adjustedBorder, in vec2 uv) {
    position_t value;
    value.top           = HasIncreasingValues(setting.position.y - adjustedBorder.x , uv.y, setting.position.y) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z, uv.x, setting.position.x + setting.size.x - adjustedBorder.w);

    value.bottom        = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y ) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z , uv.x, setting.position.x + setting.size.x - adjustedBorder.w );

    value.left          = HasIncreasingValues(setting.position.y - setting.size.y + adjustedBorder.y , uv.y, setting.position.y - adjustedBorder.x ) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z );

    value.right         = HasIncreasingValues(setting.position.y - setting.size.y + adjustedBorder.y , uv.y, setting.position.y - adjustedBorder.x ) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w , uv.x, setting.position.x + setting.size.x);

    value.topLeft       = HasIncreasingValues(setting.position.y - adjustedBorder.x , uv.y, setting.position.y ) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z );
    value.topRight      = HasIncreasingValues(setting.position.y - adjustedBorder.x , uv.y, setting.position.y ) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w , uv.x, setting.position.x + setting.size.x);
    value.bottomLeft    = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y  ) *
                          HasIncreasingValues(setting.position.x, uv.x, setting.position.x + adjustedBorder.z );
    value.bottomRight   = HasIncreasingValues(setting.position.y - setting.size.y, uv.y, setting.position.y - setting.size.y + adjustedBorder.y  ) *
                          HasIncreasingValues(setting.position.x + setting.size.x - adjustedBorder.w , uv.x, setting.position.x + setting.size.x);
    value.inner         = HasIncreasingValues(setting.position.y + adjustedBorder.y  - setting.size.y, uv.y, setting.position.y - adjustedBorder.x  ) *
                          HasIncreasingValues(setting.position.x + adjustedBorder.z , uv.x, setting.position.x + setting.size.x - adjustedBorder.w );

    return value;
}

vec4 SetScaledPositionDependingColors(vec4 border, float borderScaling, in vec2 uv, in vec4 color, in vec4 accentColor)
{
    vec2 borderMinimum = vec2( 1.0 / setting.resolution.y, 1.0 / setting.resolution.x );
    vec4 adjustedBorder = max(border, vec4(borderMinimum, borderMinimum));
    position_t pos = GetScaledPositions(adjustedBorder / borderScaling, uv);

    if ( pos.inner == 0.0 && accentColor.w == 0.0 ) discard;

    vec4 widgetColor = (pos.top + pos.bottom + pos.left + pos.right + 
                        pos.topLeft + pos.topRight + pos.bottomLeft + pos.bottomRight ) 
                        * accentColor + pos.inner * color;
    widgetColor *= setting.doDrawColors;
    return widgetColor;
}
