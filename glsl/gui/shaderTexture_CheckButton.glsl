//__default__
//!fragment
#version 440

//<<common.glsl

in vec2 _texCoord;
layout (location=0) out vec4 fragColor;

vec2  uv            = 1.0 - _texCoord * 2.0;
float ratio         = setting.resolution.x / setting.resolution.y;
float radius        = 1.0 / ratio;
const float PI      = 3.1415;

vec4
Button()
{
    float dist = distance(
        vec2( uv.x, uv.y / ratio ),
        vec2( sin( - PI / 2.0 - (PI * setting.time ) ) * ( 1.0 - radius ), 0.0 ) );
    float alpha = step( radius * 0.8, dist );
    return setting.color * ( 1.0 - alpha );
}

vec4
Slider()
{
    float dist1 =
        distance( vec2( uv.x, uv.y / ratio ), vec2( -1.0 + radius, 0.0 ) );
    float dist2 =
        distance( vec2( uv.x, uv.y / ratio ), vec2( 1.0 - radius, 0.0 ) );

    float alpha1 = step( radius, dist1 );
    float alpha2 = step( radius, dist2 );
    float alpha3 = step( uv.x, -1.0 + radius );
    float alpha4 = step( 1.0 - radius, uv.x );

    alpha4 = max( alpha3, alpha4 );


    return setting.colorAccent * ( 1.0 - min( alpha1, min( alpha2, alpha4 ) ) );
}

void
main()
{
    vec4 buttonColor = Button();
    if ( Button() != vec4(0.0) )
        fragColor = buttonColor;
    else 
        fragColor = Slider();
}
