//__default__
//!fragment
#version 440

//<<common.glsl

in vec2 _texCoord;
layout (location=0) out vec4 fragColor;

vec2  uv            = 1.0 - _texCoord * 2.0;
float ratio         = setting.resolution.x / setting.resolution.y;
float radius        = 1.0 / ratio;
const float PI      = 3.1415;

vec4 Background() {
    return setting.color;
}

// setting.size.y == 0.0 if not indifferent progress
// setting.size.x == setting.size.y == 1.0 if indifferent progress
vec4 Progress() {
    float alpha = step(uv.x, -1.0 + 2.0 * setting.size.x);
    alpha -= setting.size.y * (1.0 - abs(sin(uv.x + setting.time)));
    return setting.colorAccent * alpha;
}

void
main()
{
    fragColor = max( Background(), Progress() );
}
