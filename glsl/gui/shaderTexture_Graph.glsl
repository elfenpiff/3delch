//__default__
//!fragment
#version 440

//<<common.glsl
//<<common_Graph.glsl

layout( location = 0 ) out vec4 fragColor;
in vec2 _texCoord;

vec2  pixelSize           = 1.0 / setting.resolution;
float dataCoordResolution = 1.0 / float( graph.numberOfValues );

void
main()
{
    int positionInGraph = int((1.0 - _texCoord.x) / dataCoordResolution );
    float yPosition = mix(graph.yRange.y, graph.yRange.x, _texCoord.y);

    float d = smoothstep(
        distance( yPosition, graph.values[positionInGraph] ), 0.0, 3 * pixelSize.y );

    fragColor = mix(setting.colorAccent, setting.color, d);
}
