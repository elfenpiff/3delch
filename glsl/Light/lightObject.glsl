//__geometry__
//!fragment
#version 440

uniform sampler2DArray textureArray;

in vec3 _position;
in vec3 _normal;
in vec3 _tangent;
in vec3 _bitangent;
in vec2 _texCoord;
flat in int _diffuseId;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 glow;
layout(location = 2) out vec4 remainder;

uniform sampler2D diffuse;

void main() {
    fragColor = vec4(0.0);
    glow      = texture(textureArray, vec3(_texCoord, _diffuseId));
    remainder = vec4(_position, 0.0) + vec4(_normal, 0.0) + vec4(_tangent, 0.0) + vec4(_bitangent, 0.0);
}

//!vertex
//<<../UnifiedVertexObject/vertex.glsl
