//__directionalLight__
//!fragment
#version 440
// #define MAX_LIGHTS 10 -- defined outside

#define MAX_SHININESS 128.0
#define FOG_RAYTRACING_STEPS 16

uniform sampler2D normal;
uniform sampler2D diffuse;
uniform sampler2D position;
uniform sampler2DArray shadowMap;
uniform sampler2D material;
uniform sampler2D specular;

uniform light_t {
    vec3    color[MAX_LIGHTS];
    vec3    direction[MAX_LIGHTS];
    float   ambientIntensity[MAX_LIGHTS];
    float   diffuseIntensity[MAX_LIGHTS];
    mat4    biasedViewProjection[MAX_LIGHTS];
    int     numberOfLights;
    ivec2   resolution;
    float   shadowBorderSize;
    int     hasShadow[MAX_LIGHTS];
    vec3    cameraPosition;
    int     hasFog[MAX_LIGHTS];
    float   fogIntensity[MAX_LIGHTS];
} light;

out vec4 directionalLight;

//<<VogelDiskSample.glsl

float SmoothShadowFactor(in vec3 objectPosition, in int lightNumber) {
    if ( light.hasShadow[lightNumber] == 0 ) return 1.0;

    const float epsilon = 0.001;
    const int passes = 16;

    vec4 shadowCoordinates = light.biasedViewProjection[lightNumber] * vec4(objectPosition, 1);
    shadowCoordinates /= shadowCoordinates.w;

    if ( shadowCoordinates.x < 0.0 || shadowCoordinates.x > 1.0 ||
         shadowCoordinates.y < 0.0 || shadowCoordinates.y > 1.0 )
        return 1.0;

    float distanceFromCamera = shadowCoordinates.z - epsilon;
    float shadowFactor = 1.0;
    for(int i = 0; i < passes; ++i) {
        float distanceFromLight = texture(shadowMap, vec3(shadowCoordinates.xy + VogelDiskSample(i, passes) * light.shadowBorderSize, lightNumber)).r;

        if ( distanceFromLight < distanceFromCamera ) 
            shadowFactor -= 1. / passes;
    }

    return shadowFactor;
}

float ShadowFactor(in vec3 objectPosition, in int lightNumber) {
    if ( light.hasShadow[lightNumber] == 0 ) return 1.0;

    vec4 shadowCoordinates = light.biasedViewProjection[lightNumber] * vec4(objectPosition, 1);
    shadowCoordinates /= shadowCoordinates.w;

    float distanceFromCamera = shadowCoordinates.z;
    float distanceFromLight = texture(shadowMap, vec3(shadowCoordinates.xy, lightNumber)).r;

    if ( distanceFromLight < distanceFromCamera ) 
        return 0.0;

    return 1.0;
}

const float G_SCATTERING = 0.1;

float ComputeScattering(float lightDotView) {
    float result = 1.0 - G_SCATTERING * G_SCATTERING;
    result = result / (4.0 * 3.1415 * pow(1.0 + G_SCATTERING * G_SCATTERING - (2.0 * G_SCATTERING) * lightDotView, 1.5));
    return result;
}

void main() {
    vec2 texCoord           = gl_FragCoord.xy / light.resolution;
    vec3 objectNormal       = texture(normal, texCoord).xyz;
    vec3 objectPosition     = texture(position, texCoord).xyz;
    vec3 baseLight          = vec3(0.0);
    vec3 specularLighting   = vec3(0.0);
    vec3 specularColor      = texture(specular, texCoord).xyz;

    float shininess         = texture(material, texCoord).z;
    float metallic          = texture(material, texCoord).x;

    vec3 viewDirection = normalize(light.cameraPosition - objectPosition);
    float fogFactor = 0.0;

    for(int n = 0; n < light.numberOfLights; ++n) {
        float shadowFactor      = SmoothShadowFactor(objectPosition, n);
        float diffuseFactor     = max(0.0, dot(objectNormal, -light.direction[n]));
        float specularFactor    = 0.0;

        const float minimalShininess  = 2.0;
        if ( diffuseFactor > 0.0 && shininess > 0.0 ) {
            vec3 reflectDirection = reflect(light.direction[n], objectNormal);
            specularFactor = pow(max(dot(viewDirection, reflectDirection), 0.0), max(minimalShininess, shininess * MAX_SHININESS));
            specularLighting += metallic * specularFactor * specularColor;
        }

        baseLight += vec3(light.color[n]) 
                    * max(light.diffuseIntensity[n] * diffuseFactor * shadowFactor, 
                          light.ambientIntensity[n]);

        if ( light.hasFog[n] == 1 ) {
            const mat4 ditterPattern = mat4(vec4(0.0, 0.5, 0.125, 0.625),
                                      vec4(0.75, 0.22, 0.875, 0.375),
                                      vec4(0.1875, 0.6875, 0.0625, 0.5625),
                                      vec4(0.9375, 0.4375, 0.8125, 0.3125));

            vec3 rayVector = objectPosition - light.cameraPosition;

            float stepLength = length(rayVector) / float(FOG_RAYTRACING_STEPS);
            vec3 step = normalize(rayVector) * stepLength;
            vec3 currentPosition = light.cameraPosition;

            float lightFog = 0.0;
            for(int i = 0; i < FOG_RAYTRACING_STEPS; ++i) {
                float shadowFactor = ShadowFactor(currentPosition, n);
                lightFog += shadowFactor / float(FOG_RAYTRACING_STEPS) * ComputeScattering(dot(normalize(rayVector), light.direction[n]));
                float ditterValue = ditterPattern[int(gl_FragCoord.x) % 4][int(gl_FragCoord.y) % 4];
                ditterValue = (ditterValue + 11.0) / 12.0;
                currentPosition += step * ditterValue;
            }

            fogFactor += pow(lightFog, 1.0 / light.fogIntensity[n]);
        }
    }

    directionalLight = (1.0 - fogFactor) * texture(diffuse, texCoord) * vec4(baseLight + specularLighting, 1.0)
                        + fogFactor * vec4(light.color[0], 1.0);
}

//!vertex
#version 440

in vec3 coord;
void main() {
    gl_Position = vec4(coord, 1.0);
}

//__pointLight__
//!fragment
#version 440

#define MAX_SHININESS 128.0

uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D diffuse;
uniform samplerCube shadowMap;
uniform sampler2D material;
uniform sampler2D specular;

uniform light_t {
    vec3  color;
    vec3  position;
    vec3  attenuation;
    float intensity;
    float ambientIntensity;
    ivec2 resolution;
    int   hasShadow;
    vec3  cameraPosition;
} light;

out vec4 pointLight;

//<<VogelDiskSample.glsl
//
float CalculateShadowFactor(vec3 lightDirection, float distanceFromCamera) {
    if ( light.hasShadow == 0 ) return 1.0;

    float bias                  = 0.1;
    float shadowFactor          = 1.0;
    int   passes                = 16;
    float shadowBorderSize      = 0.02;

    for(int i = 0; i < passes; i++) {
        float distanceFromLight = texture(shadowMap, lightDirection + vec3(VogelDiskSample(i, passes), 1) * shadowBorderSize).r + bias;
        if ( distanceFromCamera > distanceFromLight ) {
            shadowFactor -= 1. / (passes );
        }
    }

    return shadowFactor;
}

void main() {
    vec2 texCoord               = gl_FragCoord.xy / light.resolution;
    vec3 objectPosition         = texture(position, texCoord).xyz;
    vec3 objectNormal           = normalize(texture(normal, texCoord).xyz);

    vec3  lightDirection        = objectPosition - light.position;
    float distanceFromCamera    = length(lightDirection);
    float shadowFactor          = CalculateShadowFactor(lightDirection, distanceFromCamera);
    lightDirection              = normalize(lightDirection);

    float diffuseFactor         = max(0.0, dot(objectNormal, -lightDirection));
    vec4 baseLight              = vec4(light.color, 1.0) 
                                    * max( diffuseFactor * shadowFactor * light.intensity, light.ambientIntensity);

    float shininess             = texture(material, texCoord).z;
    vec3  specularLighting      = vec3(0.0);

    if ( diffuseFactor > 0.0 && shininess > 0.0 ) {
        const float minimalShininess = 2.0;
        float metallic        = texture(material, texCoord).x;
        vec3 specularColor    = texture(specular, texCoord).xyz;
        vec3  viewDirection   = normalize(light.cameraPosition - objectPosition);
        vec3 reflectDirection = reflect(lightDirection, objectNormal);
        float specularFactor  = pow(max(dot(viewDirection, reflectDirection), 0.0), max(minimalShininess, shininess * MAX_SHININESS));
        specularLighting      = metallic * specularFactor * specularColor;
    }

    float attenuation    = light.attenuation.x +
                            light.attenuation.y * distanceFromCamera +
                            light.attenuation.z * distanceFromCamera * distanceFromCamera;

    attenuation          = max(1.0, attenuation);
    pointLight           = texture(diffuse, texCoord) * (baseLight + vec4(specularLighting, 1.0)) / attenuation;
}

//!vertex
#version 440

uniform mat4 vp;
in vec3 coord;

void main() {
    gl_Position = vp * vec4(coord, 1.0);
}



