#define PI_2 6.28318530717958647688

float InterleavedGradientNoise(vec2 uv)
{
  vec3 magic = vec3(0.06711056, 0.00583715, 52.9829189);
  return fract(magic.z * fract(dot(uv, magic.xy)));
}

vec2 VogelDiskSample(int sampleIndex, int samplesCount)
{
    float phi = InterleavedGradientNoise(gl_FragCoord.xy) * PI_2;
    float goldenAngle = 2.4;

    float r = sqrt(sampleIndex + 0.5) / sqrt(samplesCount);
    float theta = goldenAngle * sampleIndex + phi;

    return r * vec2(cos(theta), sin(theta));
}
