namespace el3D
{
namespace HighGL
{
template < typename... T >
inline bb::product_ptr< UnifiedVertexObject::Object >
elGeometricObject_UnifiedVertexObject::CreateObject( const T &...t ) noexcept
{
    uint64_t objectId = this->objectIds.RequestID();
    uniformBuffer.data.resize( ( objectId + 1 ) *
                               uniformBuffer_t::MATRIX_SIZE );

    this->runOnce.Call( _Refresh );
    auto newObject =
        this->factory
            .CreateProductWithOnRemoveCallback< UnifiedVertexObject::Object >(
                [this, objectId]( UnifiedVertexObject::Object * ) {
                    this->objectIds.FreeID( objectId );
                    this->runOnce.Call( _Refresh );
                },
                this, objectId, t... );
    return newObject;
}
} // namespace HighGL
} // namespace el3D
