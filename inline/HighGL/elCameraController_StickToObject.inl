namespace el3D
{
namespace HighGL
{
template < typename T >
inline elCameraController_StickToObject::elCameraController_StickToObject(
    OpenGL::elCamera_Perspective& camera, T& object,
    const glm::vec3& positionOffset, const glm::vec3& lookAtOffset ) noexcept
    : elCameraController_StickToObject(
          camera, [o = &object] { return o->GetPosition(); },
          [o = &object] { return o->GetRotation(); }, positionOffset,
          lookAtOffset )
{
}
} // namespace HighGL
} // namespace el3D

