namespace el3D
{
namespace bb
{
template < uint64_t N >
inline elCircularIndex< N >::elCircularIndex( const int64_t value )
    : value( value )
{
}

template < uint64_t N >
inline elCircularIndex< N >::operator uint64_t() const noexcept
{
    if ( this->value < 0 )
        return static_cast< uint64_t >(
            llabs( ( this->value - static_cast< int64_t >( N ) ) %
                   static_cast< int64_t >( N ) ) );
    else
        return static_cast< uint64_t >(
            llabs( this->value % static_cast< int64_t >( N ) ) );
}


template < uint64_t N >
inline int64_t
elCircularIndex< N >::GetValue() const noexcept
{
    return this->value;
}

template < uint64_t N >
inline elCircularIndex< N >
elCircularIndex< N >::operator++() noexcept
{
    return llabs( ++this->value % static_cast< int64_t >( N ) );
}

template < uint64_t N >
inline elCircularIndex< N >
elCircularIndex< N >::operator--() noexcept
{
    return llabs( --this->value % static_cast< int64_t >( N ) );
}

template < uint64_t N >
inline elCircularIndex< N >
elCircularIndex< N >::operator++( int ) noexcept
{
    return llabs( this->value++ % static_cast< int64_t >( N ) );
}

template < uint64_t N >
inline elCircularIndex< N >
elCircularIndex< N >::operator--( int ) noexcept
{
    return llabs( this->value-- % static_cast< int64_t >( N ) );
}

template < uint64_t N >
inline elCircularIndex< N >&
elCircularIndex< N >::operator+=( const int64_t rhs ) noexcept
{
    this->value = this->value + rhs;
    return *this;
}

template < uint64_t N >
inline elCircularIndex< N >&
elCircularIndex< N >::operator-=( const int64_t rhs ) noexcept
{
    this->value = this->value - rhs;
    return *this;
}

template < uint64_t N >
inline bool
elCircularIndex< N >::operator==( const elCircularIndex< N > v ) const noexcept
{
    return this->value == v.value;
}

template < uint64_t N >
inline bool
elCircularIndex< N >::operator!=( const elCircularIndex< N > v ) const noexcept
{
    return !this->operator==( v );
}
} // namespace bb
} // namespace el3D
