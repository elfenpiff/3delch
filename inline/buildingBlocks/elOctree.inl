
#include <list>

namespace el3D
{
namespace bb
{
namespace internal
{
float
MaximumNorm( const glm::vec3& a, const glm::vec3& b )
{
    return bb::max( fabsf( a.x - b.x ), fabsf( a.y - b.y ),
                    fabsf( a.z - b.z ) );
}
} // namespace internal

template < typename T >
inline elOctree< T >::elOctree( const float minimumHalfSideLength ) noexcept
    : minimumHalfSideLength{ minimumHalfSideLength }
{
}

template < typename T >
inline void
elOctree< T >::Insert( const glm::vec3& point, const T& value ) noexcept
{
    if ( !this->root )
    {
        this->root                  = std::make_unique< node_t >( point, 0.0f );
        this->root->contents[point] = value;
        return;
    }

    if ( !this->root->DoesFitInNode( point ) )
    {
        if ( this->root->cube.halfSideLength == 0.0f )
        {
            glm::vec3 midPoint = ( this->root->cube.center + point ) * 0.5f;
            this->root->cube.halfSideLength = bb::max(
                internal::MaximumNorm( this->root->cube.center, point ),
                this->minimumHalfSideLength );
            this->root->cube.center = midPoint;

            if ( this->minimumHalfSideLength <=
                 this->root->cube.halfSideLength / 2.0f )
            {
                auto      iter        = this->root->contents.begin();
                glm::vec3 storedPoint = iter->first;
                T         storedValue = iter->second;
                auto      node = &this->root->AcquireNodeFor( storedPoint );
                node->contents[storedPoint] = storedValue;
                this->root->contents.clear();
            }
        }
        else
        {
            for ( node_t* node                              = this->root.get();
                  !this->root->DoesFitInNode( point ); node = this->root.get() )
            {
                glm::vec3 direction{
                    ( point.x >= node->cube.center.x ) ? 1.0f : -1.0f,
                    ( point.y >= node->cube.center.y ) ? 1.0f : -1.0f,
                    ( point.z >= node->cube.center.z ) ? 1.0f : -1.0f };

                glm::vec3 center =
                    node->cube.center + direction * node->cube.halfSideLength;

                auto newRoot = std::make_unique< node_t >(
                    center, node->cube.halfSideLength * 2.0f );
                auto oldRootIndex =
                    newRoot->AcquireNodeIndexFor( this->root->cube.center );
                newRoot->nodes[oldRootIndex] = std::move( this->root );
                this->root                   = std::move( newRoot );
            }
        }
    }

    node_t* node = this->root.get();
    for ( ; this->minimumHalfSideLength <= node->cube.halfSideLength / 2.0f;
          node = &node->AcquireNodeFor( point ) )
    {
        if ( node->contents.empty() && !node->HasLeaves() )
        {
            node->contents[point] = value;
            return;
        }
    }

    node->contents[point] = value;
}

template < typename T >
inline bool
elOctree< T >::DoesContain( const glm::vec3& point ) const noexcept
{
    if ( !this->root->DoesFitInNode( point ) ) return false;

    auto* node = this->root.get();

    while ( node->contents.empty() )
    {
        auto id = node->AcquireNodeIndexFor( point );

        if ( !node->nodes[id] ) return false;
        node = node->nodes[id].get();
    }

    return node->contents.find( point ) != node->contents.end();
}

template < typename T >
inline std::vector< typename elOctree< T >::element_t >
elOctree< T >::GetNeighbors( const glm::vec3& center,
                             const float      halfSideLength ) const noexcept
{
    std::vector< element_t > retVal;
    if ( !this->root ) return retVal;

    std::list< const node_t* > nodesToVisit;

    auto visitNode = [&]( const node_t* node ) {
        if ( !node->contents.empty() )
        {
            for ( auto& n : node->contents )
                if ( internal::MaximumNorm( center, n.first ) < halfSideLength )
                    retVal.emplace_back( n.first, n.second );
        }
        else
        {
            auto overlappingNodes =
                node->GetOverlappingNodes( center, halfSideLength );
            for ( auto n : overlappingNodes )
                nodesToVisit.emplace_back( n );
        }
    };

    visitNode( this->root.get() );

    while ( !nodesToVisit.empty() )
    {
        auto firstNode = *nodesToVisit.begin();
        visitNode( firstNode );
        nodesToVisit.erase( nodesToVisit.begin() );
    }

    return retVal;
}

template < typename T >
inline bool
elOctree< T >::HasNeighbors( const glm::vec3& center,
                             const float      halfSideLength ) const noexcept
{
    if ( !this->root ) return false;

    std::list< const node_t* > nodesToVisit;

    auto visitNode = [&]( const node_t* node ) {
        if ( node->IsContainedIn( center, halfSideLength ) )
            return true;
        else if ( node->HasLeaves() )
            nodesToVisit.emplace_back( node );
        else if ( !node->contents.empty() )
            for ( auto& n : node->contents )
                if ( internal::MaximumNorm( center, n.first ) < halfSideLength )
                    return true;

        return false;
    };

    if ( visitNode( this->root.get() ) ) return true;

    while ( !nodesToVisit.empty() )
    {
        auto firstNode = *nodesToVisit.begin();
        auto overlappingNodes =
            firstNode->GetOverlappingNodes( center, halfSideLength );

        for ( auto node : overlappingNodes )
            if ( visitNode( node ) ) return true;

        nodesToVisit.erase( nodesToVisit.begin() );
    }

    return false;
}

template < typename T >
inline std::vector< typename elOctree< T >::cube_t >
elOctree< T >::GetTree() const noexcept
{
    std::vector< typename elOctree< T >::cube_t > retVal;
    std::vector< const node_t* >                  todoList;

    todoList.emplace_back( this->root.get() );

    while ( !todoList.empty() )
    {
        auto nextNode = todoList.back();
        todoList.pop_back();
        for ( uint64_t i = 0; i < 8; i++ )
            if ( nextNode->nodes[i] )
                todoList.emplace_back( nextNode->nodes[i].get() );
        retVal.emplace_back( nextNode->cube );
    }

    return retVal;
}

template < typename T >
inline elOctree< T >::node_t::node_t( const glm::vec3& center,
                                      const float      halfSideLength ) noexcept
    : cube{ center, halfSideLength }
{
}

template < typename T >
inline std::vector< typename elOctree< T >::node_t* >
elOctree< T >::node_t::GetOverlappingNodes(
    const glm::vec3& center, const float halfSideLength ) const noexcept
{
    std::vector< typename elOctree< T >::node_t* > overlappingNodes;

    for ( uint64_t i = 0; i < 8; ++i )
    {
        if ( this->nodes[i] )
        {
            auto distance =
                internal::MaximumNorm( this->nodes[i]->cube.center, center );
            if ( distance <
                 this->nodes[i]->cube.halfSideLength + halfSideLength )
                overlappingNodes.emplace_back( this->nodes[i].get() );
        }
    }

    return overlappingNodes;
}

template < typename T >
inline bool
elOctree< T >::node_t::IsContainedIn(
    const glm::vec3& center, const float halfSideLength ) const noexcept
{
    auto distance = internal::MaximumNorm( this->cube.center, center );
    return ( distance + this->cube.halfSideLength < halfSideLength );
}

template < typename T >
inline bool
elOctree< T >::node_t::DoesFitInNode( const glm::vec3& point ) const noexcept
{
    glm::vec3 min = this->cube.center - this->cube.halfSideLength;
    glm::vec3 max = this->cube.center + this->cube.halfSideLength;

    return ( min.x <= point.x && point.x <= max.x ) &&
           ( min.y <= point.y && point.y <= max.y ) &&
           ( min.z <= point.z && point.z <= max.z );
}

template < typename T >
inline typename elOctree< T >::node_t&
elOctree< T >::node_t::AcquireNodeFor( const glm::vec3& point ) noexcept
{
    auto index = this->AcquireNodeIndexFor( point );
    if ( !this->nodes[index] )
    {
        float     newHalfSideLength = this->cube.halfSideLength / 2.0f;
        glm::vec3 newCenter         = this->cube.center;

        for ( int i = 0; i < 3; ++i )
            newCenter[i] += ( this->cube.center[i] < point[i] )
                                ? newHalfSideLength
                                : -newHalfSideLength;

        this->nodes[index] =
            std::make_unique< node_t >( newCenter, newHalfSideLength );

        if ( !this->contents.empty() )
        {
            auto      iter             = this->contents.begin();
            glm::vec3 storedPoint      = iter->first;
            T         storedValue      = iter->second;
            auto&     node             = this->AcquireNodeFor( storedPoint );
            node.contents[storedPoint] = storedValue;
            this->contents.clear();
        }
    }

    return *this->nodes[index].get();
}

template < typename T >
inline uint64_t
elOctree< T >::node_t::AcquireNodeIndexFor(
    const glm::vec3& point ) const noexcept
{
    if ( this->cube.center.x < point.x && this->cube.center.y < point.y &&
         this->cube.center.z < point.z )
        return 0;
    else if ( this->cube.center.x < point.x && this->cube.center.y < point.y &&
              this->cube.center.z >= point.z )
        return 1;
    else if ( this->cube.center.x < point.x && this->cube.center.y >= point.y &&
              this->cube.center.z < point.z )
        return 2;
    else if ( this->cube.center.x >= point.x && this->cube.center.y < point.y &&
              this->cube.center.z < point.z )
        return 3;
    else if ( this->cube.center.x < point.x && this->cube.center.y >= point.y &&
              this->cube.center.z >= point.z )
        return 4;
    else if ( this->cube.center.x >= point.x && this->cube.center.y < point.y &&
              this->cube.center.z >= point.z )
        return 5;
    else if ( this->cube.center.x >= point.x &&
              this->cube.center.y >= point.y && this->cube.center.z < point.z )
        return 6;
    else if ( this->cube.center.x >= point.x &&
              this->cube.center.y >= point.y && this->cube.center.z >= point.z )
        return 7;

    return INVALID_ID;
}

template < typename T >
inline bool
elOctree< T >::node_t::HasLeaves() const noexcept
{
    bool hasLeaves = false;

    for ( uint64_t i = 0; i < 8; ++i )
        hasLeaves |= static_cast< bool >( this->nodes[i] );

    return hasLeaves;
}

template < typename T >
inline elOctree< T >::element_t::element_t( const glm::vec3& point,
                                            const T&         value ) noexcept
    : point{ point }, value{ value }
{
}


} // namespace bb
} // namespace el3D
