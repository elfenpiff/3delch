namespace el3D
{
namespace bb
{
template < typename T, typename ExtensionType >
struct CompositeProduct
{
};

namespace internal
{
template < typename T1, typename T2 >
struct TypeDef
{
    using External           = T1;
    using Internal           = T2;
    using UnderlyingExternal = T1;
    using UnderlyingInternal = T2;
};

template < typename T1, typename T2, typename T3, typename T4 >
struct TypeDef< CompositeProduct< T1, T2 >, CompositeProduct< T3, T4 > >
{
    using External           = CompositeProduct< T1, T2 >;
    using Internal           = CompositeProduct< T3, T4 >;
    using UnderlyingExternal = T1;
    using UnderlyingInternal = T3;
};

template < bool, typename... >
struct ProductTypeImpl;

template < typename T, typename T1, typename T2, typename... Products >
struct ProductTypeImpl< false, T, T1, T2, Products... >
{
    using Type = typename ProductTypeImpl< std::is_same_v< T, T2 > ||
                                               std::is_base_of_v< T2, T >,
                                           T, T2, Products... >::Type;
};

template < typename T, typename T1, typename T2, typename T3,
           typename... Products >
struct ProductTypeImpl< false, T, T1, CompositeProduct< T2, T3 >, Products... >
{
    using Type = typename ProductTypeImpl<
        std::is_same_v< T, T2 > || std::is_base_of_v< T2, T >, T,
        CompositeProduct< T2, T3 >, Products... >::Type;
};

template < typename T, typename T1, typename... Products >
struct ProductTypeImpl< true, T, T1, Products... >
{
    using Type = TypeDef< T, T1 >;
};

template < typename T, typename T1, typename T2, typename... Products >
struct ProductTypeImpl< true, T, CompositeProduct< T1, T2 >, Products... >
{
    using Type =
        TypeDef< CompositeProduct< T, T2 >, CompositeProduct< T1, T2 > >;
};

template < typename T, typename T1, typename... Products >
struct ProductType
{
    using Type = typename ProductTypeImpl< std::is_same_v< T, T1 > ||
                                               std::is_base_of_v< T1, T >,
                                           T, T1, Products... >::Type;
};

template < typename T, typename T1, typename T2, typename... Products >
struct ProductType< T, CompositeProduct< T1, T2 >, Products... >
{
    using Type = typename ProductTypeImpl<
        std::is_same_v< T, T1 > || std::is_base_of_v< T1, T >, T,
        CompositeProduct< T1, T2 >, Products... >::Type;
};


template < int N, typename... >
struct GetIndexOfImpl;

template < int N, typename T1, typename T2, typename... Targs >
struct GetIndexOfImpl< N, T1, T2, Targs... >
{
    static constexpr int value =
        ( std::is_same_v< T1, T2 > || std::is_base_of_v< T2, T1 > )
            ? N
            : GetIndexOfImpl< N + 1, T1, Targs... >::value;
};

template < int N, typename T >
struct GetIndexOfImpl< N, T >
{
    static constexpr int value = -1;
};

template < typename T, typename T1, typename... Targs >
struct GetIndexOf
{
    using Impl                 = GetIndexOfImpl< 0, T, T1, Targs... >;
    static constexpr int value = Impl::value;
};

template < int N, typename T, typename... Targs >
struct GetTypeAt
{
    using type = typename GetTypeAt< N - 1, Targs... >::type;
};

template < typename T, typename... Targs >
struct GetTypeAt< 0, T, Targs... >
{
    using type = T;
};

template < typename T, typename SourceType >
inline std::enable_if_t< std::is_class_v< T >, T* >
CastTo( SourceType* ptr ) noexcept
{
    return dynamic_cast< T* >( ptr );
}

template < typename T, typename SourceType >
inline std::enable_if_t< !std::is_class_v< T >, T* >
CastTo( SourceType* ptr ) noexcept
{
    return static_cast< T* >( ptr );
}
} // namespace internal

////////////////////
/// elGenericFactory
////////////////////
template < typename... Products >
inline elGenericFactory< Products... >::elGenericFactory() noexcept
    : referenceCounter{ std::make_unique< referenceCounterContainer_t >() },
      products{ std::make_unique< productContainer_t >() }
{
}

template < typename... Products >
template < typename T, typename... Targs >
inline product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateProduct( Targs&&... args ) noexcept
{
    return this->CreateProductAtPositionWithOnRemoveCallback< T >(
        OnRemoveCallback_t< T >(), END_POSITION,
        std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateProductWithOnRemoveCallback(
    const OnRemoveCallback_t< T >& callback, Targs&&... args ) noexcept
{
    return this->CreateProductAtPositionWithOnRemoveCallback< T >(
        callback, END_POSITION, std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateProductAtPosition(
    const uint64_t position, Targs&&... args ) noexcept
{
    return this->CreateProductAtPositionWithOnRemoveCallback< T >(
        OnRemoveCallback_t< T >(), position, std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateProductAtPositionWithOnRemoveCallback(
    const OnRemoveCallback_t< T >& callback, const uint64_t position,
    Targs&&... args ) noexcept
{
    using ExternalType =
        typename internal::ProductType< T, Products... >::Type::External;
    using Type =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< Type, Products... >;
    using UnderlyingType =
        typename internal::ProductType< T,
                                        Products... >::Type::UnderlyingInternal;

    UnderlyingProduct_t< T >* productPtr = this->CreateRawPointer< T >(
        position, std::forward< Targs >( args )... );

    return product_ptr< ExternalType >(
        *productPtr,
        [productsPtr = this->products.get(), callback]( T* ptr )
        {
            if ( callback ) callback( ptr );

            auto& productVector = std::get< TypeIndex::value >( *productsPtr );
            auto  iter          = bb::find_if(
                productVector,
                [&]( auto& uniquePtr ) {
                    return uniquePtr.Get() ==
                           internal::CastTo< UnderlyingType >( ptr );
                } );

            if ( iter != productVector.end() )
            {
                // a product could contain again multiple products, when the
                // destructor is called the dtor of the contained products is
                // called and removes the products from the productVector which
                // invalidates this iterator and a different element can be
                // removed
                //
                // to avoid this, we move the product out of the vector, remove
                // the moved element from the vector and then call the
                // destructor now the productVector can be changed by other
                // elements dtor
                auto ptr = std::move( *iter );
                productVector.erase( iter );
                ptr.Reset();
            }
        } );
}

template < typename... Products >
template < typename T, typename... Targs >
inline
    typename elGenericFactory< Products... >::template UnderlyingProduct_t< T >*
    elGenericFactory< Products... >::CreateRawPointer(
        const uint64_t position, Targs&&... args ) noexcept
{
    using Type =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using UnderlyingType =
        typename internal::ProductType< T,
                                        Products... >::Type::UnderlyingInternal;
    using TypeIndex = internal::GetIndexOf< Type, Products... >;

    static_assert( TypeIndex::value != -1,
                   "Type or a base of that type is not contained!" );
    static_assert( (std::is_base_of_v< UnderlyingType, T > &&
                    std::has_virtual_destructor_v< UnderlyingType >) ||
                       std::is_same_v< UnderlyingType, T >,
                   "The base type requires a virtual destructor!" );

    T* newObject = new T( std::forward< Targs >( args )... );

    auto& productsVector = std::get< TypeIndex::value >( *this->products );

    UnderlyingProduct_t< T >* productPtr = nullptr;
    if ( position == END_POSITION )
    {
        productsVector.emplace_back( UnderlyingProduct_t< T >( newObject ) );
        productPtr = &productsVector.back();
    }
    else
    {
        productsVector.emplace( productsVector.begin() +
                                    static_cast< int64_t >( position ),
                                UnderlyingProduct_t< T >( newObject ) );
        productPtr = &productsVector[position];
    }

    return productPtr;
}

template < typename... Products >
template < typename T, typename... Targs >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateSharedProduct( Targs&&... args ) noexcept
{
    return this->CreateSharedProductAtPositionWithOnRemoveCallback< T >(
        OnRemoveCallback_t< T >(), END_POSITION,
        std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateSharedProductAtPosition(
    const uint64_t position, Targs&&... args ) noexcept
{
    return this->CreateSharedProductAtPositionWithOnRemoveCallback< T >(
        OnRemoveCallback_t< T >(), position, std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateSharedProductWithOnRemoveCallback(
    const OnRemoveCallback_t< T >& callback, Targs&&... args ) noexcept
{
    return this->CreateSharedProductAtPositionWithOnRemoveCallback< T >(
        callback, END_POSITION, std::forward< Targs >( args )... );
}

template < typename... Products >
template < typename T, typename... Targs >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::
    CreateSharedProductAtPositionWithOnRemoveCallback(
        const OnRemoveCallback_t< T >& callback, const uint64_t position,
        Targs&&... args ) noexcept
{
    using Type =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< Type, Products... >;
    auto& referenceVector =
        std::get< TypeIndex::value >( *this->referenceCounter );

    UnderlyingProduct_t< T >* productPtr = this->CreateRawPointer< T >(
        position, std::forward< Targs >( args )... );

    auto reference = referenceVector.emplace(
        std::piecewise_construct, std::forward_as_tuple( productPtr->Get() ),
        std::forward_as_tuple( callback, *productPtr ) );

    return this->CreateSharedProductPointer< T >( reference.first->second );
}

template < typename... Products >
template < typename T >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::CreateSharedProductPointer(
    referenceHandle_t< UnderlyingInternalProduct_t< T > >&
        referenceHandle ) noexcept
{
    using Type =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< Type, Products... >;

    auto& referenceVector =
        std::get< TypeIndex::value >( *this->referenceCounter );

    return shared_product_ptr< Product_t< T > >(
        referenceHandle,
        [productsPtr = this->products.get(), referenceVector = &referenceVector,
         callback = referenceHandle.callback]( T* ptr )
        {
            if ( callback ) callback( ptr );

            auto& productVector = std::get< TypeIndex::value >( *productsPtr );
            auto  iter = bb::find_if( productVector, [&]( auto& uniquePtr )
                                     { return uniquePtr.Get() == ptr; } );

            if ( iter != productVector.end() ) productVector.erase( iter );

            auto referenceIter = referenceVector->find( ptr );
            if ( referenceIter != referenceVector->end() )
                referenceVector->erase( referenceIter );
        } );
}

template < typename... Products >
template < typename T >
inline shared_product_ptr<
    typename elGenericFactory< Products... >::template Product_t< T > >
elGenericFactory< Products... >::AcquireSharedProduct(
    const T& dataReference ) noexcept
{
    using Type =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< Type, Products... >;
    auto& referenceVector =
        std::get< TypeIndex::value >( *this->referenceCounter );

    auto referenceIter = referenceVector.find(
        const_cast< void* >( static_cast< const void* >( &dataReference ) ) );
    if ( referenceIter == referenceVector.end() )
        return shared_product_ptr< Product_t< T > >();

    return this->CreateSharedProductPointer< T >( referenceIter->second );
}

template < typename... Products >
template < typename T >
inline std::vector< product_ptr<
    typename elGenericFactory< Products... >::template BaseProduct_t< T > > >&
elGenericFactory< Products... >::GetProducts() noexcept
{
    using FactoryType =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< FactoryType, Products... >;
    return std::get< TypeIndex::value >( *this->products );
}

template < typename... Products >
template < typename T >
inline const std::vector< product_ptr<
    typename elGenericFactory< Products... >::template BaseProduct_t< T > > >&
elGenericFactory< Products... >::GetProducts() const noexcept
{
    using FactoryType =
        typename internal::ProductType< T, Products... >::Type::Internal;
    using TypeIndex = internal::GetIndexOf< FactoryType, Products... >;
    return std::get< TypeIndex::value >( *this->products );
}
} // namespace bb
} // namespace el3D
