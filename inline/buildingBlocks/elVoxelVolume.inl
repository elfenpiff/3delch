namespace el3D
{
namespace bb
{
template < typename T >
template < typename... Args >
inline elVoxelVolume< T >::element_t::element_t( const position_t& p,
                                                 Args&&... args ) noexcept
    : position{ p }, data{ std::forward< Args >( args )... }
{
}

template < typename T >
inline auto
elVoxelVolume< T >::element_t::operator<=>(
    const element_t& rhs ) const noexcept
{
    return this->position <=> rhs.position;
}

template < typename T >
template < typename... Args >
inline bool
elVoxelVolume< T >::Insert( const position_t& p, Args&&... args ) noexcept
{
    if ( this->DoesContainElementAt( p ) ) return false;
    this->positionToData[p] =
        this->contents.emplace_back( p, std::forward< Args >( args )... )
            .index();
    return true;
}

template < typename T >
inline void
elVoxelVolume< T >::Remove( const position_t& p ) noexcept
{
    auto iter = this->positionToData.find( p );
    if ( iter != this->positionToData.end() )
    {
        this->contents.erase( iter->second );
        this->positionToData.erase( iter );
    }
}

template < typename T >
inline T&
elVoxelVolume< T >::Get( const position_t& p ) noexcept
{
    return this->contents[this->positionToData[p]].data;
}

template < typename T >
inline const T&
elVoxelVolume< T >::Get( const position_t& p ) const noexcept
{
    auto iter = this->positionToData.find( p );
    return this->contents[iter->second].data;
}

template < typename T >
inline bool
elVoxelVolume< T >::DoesContainElementAt( const position_t& p ) const noexcept
{
    auto iter = this->positionToData.find( p );
    return iter != this->positionToData.end();
}

template < typename T >
inline const std::vector< typename elVoxelVolume< T >::element_t >&
elVoxelVolume< T >::GetConstContents() const noexcept
{
    return this->contents.get_const_contents();
}

template < typename T >
inline std::span< typename elVoxelVolume< T >::element_t >
elVoxelVolume< T >::GetContents() noexcept
{
    return this->contents.get_contents();
}

template < typename T >
inline void
elVoxelVolume< T >::SortContents() noexcept
{
    bb::sort( this->contents );
    this->positionToData.clear();
    for ( uint64_t i = 0, limit = this->contents.size(); i < limit; ++i )
        this->positionToData[this->contents[i].position] = i;
}
} // namespace bb
} // namespace el3D
