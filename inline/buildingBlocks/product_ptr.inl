namespace el3D
{
namespace bb
{
template < typename T >
template < typename Other >
inline product_ptr< T >::product_ptr( product_ptr< Other >& linkedPtr,
                                      const Deleter&        deleter ) noexcept
    : data{ static_cast< T* >( linkedPtr.data ) }, deleter{ deleter }
{
}

template < typename T >
inline product_ptr< T >::product_ptr( T* const       object,
                                      const Deleter& deleter ) noexcept
    : data{ object }, deleter{ deleter }
{
}

template < typename T >
inline product_ptr< T >::product_ptr( T* const object ) noexcept
    : data{ object }, deleter{ []( T* ptr ) { delete ptr; } }
{
}

template < typename T >
inline product_ptr< T >::~product_ptr() noexcept
{
    this->Reset();
}

template < typename T >
inline product_ptr< T >::product_ptr( product_ptr&& rhs ) noexcept
    : data{ std::move( rhs.data ) }, deleter{ std::move( rhs.deleter ) }
{
    rhs.data    = nullptr;
    rhs.deleter = Deleter();
}

template < typename T >
inline product_ptr< T >&
product_ptr< T >::operator=( product_ptr&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Reset();
        this->data    = std::move( rhs.data );
        this->deleter = std::move( rhs.deleter );
        rhs.data      = nullptr;
        rhs.deleter   = Deleter();
    }
    return *this;
}

template < typename T >
inline product_ptr< T >::operator bool() const noexcept
{
    return this->data != nullptr;
}

template < typename T >
inline T&
product_ptr< T >::operator*() noexcept
{
    return *this->data;
}

template < typename T >
inline const T&
product_ptr< T >::operator*() const noexcept
{
    return *this->data;
}

template < typename T >
inline T*
product_ptr< T >::operator->() noexcept
{
    return this->data;
}

template < typename T >
inline const T*
product_ptr< T >::operator->() const noexcept
{
    return this->data;
}

template < typename T >
inline T*
product_ptr< T >::Get() noexcept
{
    return this->data;
}

template < typename T >
inline const T*
product_ptr< T >::Get() const noexcept
{
    return this->data;
}

template < typename T >
inline void
product_ptr< T >::Reset() noexcept
{
    if ( this->data )
    {
        if ( this->deleter ) this->deleter( this->data );
        this->data    = nullptr;
        this->deleter = Deleter();
    }
}

template < typename T >
inline product_ptr< T >&
product_ptr< T >::AndThen(
    const std::function< void( T& ) >& callback ) noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T >
inline const product_ptr< T >&
product_ptr< T >::AndThen(
    const std::function< void( const T& ) >& callback ) const noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T >
inline product_ptr< T >&
product_ptr< T >::OrElse( const std::function< void() >& callback ) noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}

template < typename T >
inline const product_ptr< T >&
product_ptr< T >::OrElse(
    const std::function< void() >& callback ) const noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}

template < typename T, typename ExtensionType >
template < typename Other >
inline product_ptr< CompositeProduct< T, ExtensionType > >::product_ptr(
    product_ptr< CompositeProduct< Other, ExtensionType > >& linkedPtr,
    const Deleter&                                           deleter ) noexcept
    : product_ptr< T >( linkedPtr, deleter ),
      extension( &linkedPtr.Extension(), []( ExtensionType* ) {} )
{
}

template < typename T, typename ExtensionType >
inline product_ptr< CompositeProduct< T, ExtensionType > >::product_ptr(
    T* const object ) noexcept
    : product_ptr< T >( object, []( T* ptr ) { delete ptr; } ),
      extension( new ExtensionType(), []( ExtensionType* ptr ) { delete ptr; } )
{
}

template < typename T, typename ExtensionType >
inline ExtensionType&
product_ptr< CompositeProduct< T, ExtensionType > >::Extension() noexcept
{
    return *this->extension;
}

template < typename T, typename ExtensionType >
inline const ExtensionType&
product_ptr< CompositeProduct< T, ExtensionType > >::Extension() const noexcept
{
    return *this->extension;
}

//////////////////////////
/// reference handle
//////////////////////////

template < typename T >
inline referenceHandle_t< T >::referenceHandle_t(
    const std::function< void( T* ) >& callback,
    product_ptr< T >&                  ptr ) noexcept
    : referenceCounter{ 0u }, productPtr{ ptr.Get() }, callback{ callback }
{
}

template < typename T, typename ExtensionType >
inline referenceHandle_t< CompositeProduct< T, ExtensionType > >::
    referenceHandle_t(
        const std::function< void( T* ) >&                   callback,
        product_ptr< CompositeProduct< T, ExtensionType > >& ptr ) noexcept
    : referenceCounter{ 0u }, productPtr{ ptr.Get() }, callback{ callback },
      extensionPtr{ &ptr.Extension() }
{
}


//////////////////////////
/// shared_product_ptr
//////////////////////////

template < typename T >
template < typename ChildType >
inline shared_product_ptr< T >::shared_product_ptr(
    referenceHandle_t< ChildType >& referenceHandle,
    const Deleter&                  deleter ) noexcept
    : referenceCounter{ &referenceHandle.referenceCounter },
      data{ static_cast< T* >( referenceHandle.productPtr ) }, deleter{
                                                                   deleter }
{
    ( *this->referenceCounter )++;
}

template < typename T >
inline shared_product_ptr< T >::shared_product_ptr(
    shared_product_ptr&& rhs ) noexcept
    : referenceCounter{ rhs.referenceCounter }, data{ rhs.data },
      deleter{ std::move( rhs.deleter ) }
{
    rhs.referenceCounter = nullptr;
    rhs.data             = nullptr;
}

template < typename T >
inline shared_product_ptr< T >::shared_product_ptr(
    const shared_product_ptr& rhs ) noexcept
    : referenceCounter{ rhs.referenceCounter }, data{ rhs.data },
      deleter{ rhs.deleter }
{
    if ( this->referenceCounter ) ( *this->referenceCounter )++;
}

template < typename T >
inline shared_product_ptr< T >::~shared_product_ptr() noexcept
{
    this->Reset();
}

template < typename T >
inline shared_product_ptr< T >&
shared_product_ptr< T >::operator=( shared_product_ptr&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Reset();

        this->data             = rhs.data;
        this->referenceCounter = rhs.referenceCounter;
        this->deleter          = rhs.deleter;

        rhs.referenceCounter = nullptr;
        rhs.data             = nullptr;
    }
    return *this;
}

template < typename T >
inline shared_product_ptr< T >&
shared_product_ptr< T >::operator=( const shared_product_ptr& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Reset();

        this->data             = rhs.data;
        this->referenceCounter = rhs.referenceCounter;
        this->deleter          = rhs.deleter;

        if ( this->referenceCounter ) ( *this->referenceCounter )++;
    }
    return *this;
}

template < typename T >
inline void
shared_product_ptr< T >::Reset() noexcept
{
    if ( this->referenceCounter != nullptr )
    {
        if ( this->referenceCounter->fetch_sub( 1 ) == 1u )
        {
            if ( this->deleter ) this->deleter( this->data );
        }

        this->referenceCounter = nullptr;
        this->data             = nullptr;
    }
}

template < typename T >
inline shared_product_ptr< T >::operator bool() const noexcept
{
    return this->data != nullptr;
}

template < typename T >
inline T&
shared_product_ptr< T >::operator*() noexcept
{
    return *this->data;
}

template < typename T >
inline const T&
shared_product_ptr< T >::operator*() const noexcept
{
    return *this->data;
}

template < typename T >
inline T*
shared_product_ptr< T >::operator->() noexcept
{
    return this->data;
}

template < typename T >
inline const T*
shared_product_ptr< T >::operator->() const noexcept
{
    return this->data;
}

template < typename T >
inline T*
shared_product_ptr< T >::Get() noexcept
{
    return this->data;
}

template < typename T >
inline const T*
shared_product_ptr< T >::Get() const noexcept
{
    return this->data;
}

template < typename T >
inline shared_product_ptr< T >&
shared_product_ptr< T >::AndThen(
    const std::function< void( T& ) >& callback ) noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T >
inline const shared_product_ptr< T >&
shared_product_ptr< T >::AndThen(
    const std::function< void( const T& ) >& callback ) const noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T >
inline const shared_product_ptr< T >&
shared_product_ptr< T >::OrElse(
    const std::function< void() >& callback ) const noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}

template < typename T >
inline shared_product_ptr< T >&
shared_product_ptr< T >::OrElse(
    const std::function< void() >& callback ) noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}

template < typename T, typename ExtensionType >
template < typename ChildType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >::
    shared_product_ptr(
        referenceHandle_t< CompositeProduct< ChildType, ExtensionType > >&
                       referenceHandle,
        const Deleter& deleter ) noexcept
    : referenceCounter{ &referenceHandle.referenceCounter },
      data{ static_cast< T* >( referenceHandle.productPtr ) },
      deleter{ deleter }, extension( referenceHandle.extensionPtr )
{
    if ( this->referenceCounter ) ++( *this->referenceCounter );
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >::
    shared_product_ptr( shared_product_ptr&& rhs ) noexcept
    : referenceCounter{ std::move( rhs.referenceCounter ) }, data{ std::move(
                                                                 rhs.data ) },
      deleter{ std::move( rhs.deleter ) }, extension{
                                               std::move( rhs.extension ) }
{
    rhs.referenceCounter = nullptr;
    rhs.data             = nullptr;
    rhs.extension        = nullptr;
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >::
    shared_product_ptr( const shared_product_ptr& rhs ) noexcept
    : referenceCounter{ rhs.referenceCounter }, data{ rhs.data },
      deleter{ rhs.deleter }, extension{ rhs.extension }
{
    if ( this->referenceCounter ) ++( *this->referenceCounter );
}

template < typename T, typename ExtensionType >
inline shared_product_ptr<
    CompositeProduct< T, ExtensionType > >::~shared_product_ptr() noexcept
{
    this->Reset();
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator=(
    shared_product_ptr&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Reset();

        this->referenceCounter = std::move( rhs.referenceCounter );
        this->data             = std::move( rhs.data );
        this->deleter          = std::move( rhs.deleter );
        this->extension        = std::move( rhs.extension );

        rhs.referenceCounter = nullptr;
        rhs.data             = nullptr;
        rhs.extension        = nullptr;
    }
    return *this;
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator=(
    const shared_product_ptr& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->Reset();

        this->referenceCounter = rhs.referenceCounter;
        this->data             = rhs.data;
        this->deleter          = rhs.deleter;
        this->extension        = rhs.extension;

        if ( this->referenceCounter ) ++( *this->referenceCounter );
    }
    return *this;
}

template < typename T, typename ExtensionType >
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator bool()
    const noexcept
{
    return this->data != nullptr;
}

template < typename T, typename ExtensionType >
T&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator*() noexcept
{
    return *this->data;
}

template < typename T, typename ExtensionType >
const T&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator*()
    const noexcept
{
    return *this->data;
}

template < typename T, typename ExtensionType >
T*
                                            shared_product_ptr<
                                                CompositeProduct< T, ExtensionType > >::operator->() noexcept
{
    return this->data;
}

template < typename T, typename ExtensionType >
const T*
shared_product_ptr< CompositeProduct< T, ExtensionType > >::operator->()
    const noexcept
{
    return this->data;
}

template < typename T, typename ExtensionType >
T*
shared_product_ptr< CompositeProduct< T, ExtensionType > >::Get() noexcept
{
    return this->data;
}

template < typename T, typename ExtensionType >
const T*
shared_product_ptr< CompositeProduct< T, ExtensionType > >::Get() const noexcept
{
    return this->data;
}

template < typename T, typename ExtensionType >
void
shared_product_ptr< CompositeProduct< T, ExtensionType > >::Reset() noexcept
{
    if ( this->referenceCounter != nullptr )
    {
        if ( this->referenceCounter->fetch_sub( 1 ) == 1u )
        {
            if ( this->deleter ) this->deleter( this->data );
        }

        this->referenceCounter = nullptr;
        this->data             = nullptr;
        this->extension        = nullptr;
    }
}

template < typename T, typename ExtensionType >
inline ExtensionType&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::Extension() noexcept
{
    return *this->extension;
}

template < typename T, typename ExtensionType >
inline const ExtensionType&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::Extension()
    const noexcept
{
    return *this->extension;
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::AndThen(
    const std::function< void( T& ) >& callback ) noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T, typename ExtensionType >
inline const shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::AndThen(
    const std::function< void( const T& ) >& callback ) const noexcept
{
    if ( callback && this->data != nullptr ) callback( *this->data );
    return *this;
}

template < typename T, typename ExtensionType >
inline const shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::OrElse(
    const std::function< void() >& callback ) const noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}

template < typename T, typename ExtensionType >
inline shared_product_ptr< CompositeProduct< T, ExtensionType > >&
shared_product_ptr< CompositeProduct< T, ExtensionType > >::OrElse(
    const std::function< void() >& callback ) noexcept
{
    if ( callback && this->data == nullptr ) callback();
    return *this;
}


template < typename T >
inline product_guard< T >::product_guard( product_ptr< T >&& rhs ) noexcept
{
    this->data    = rhs.data;
    this->deleter = rhs.deleter;
    rhs.data      = nullptr;
}

} // namespace bb
} // namespace el3D
