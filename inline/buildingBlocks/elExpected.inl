namespace el3D
{
namespace bb
{
namespace internal
{
template < typename ErrorType, typename... ValueType >
inline elExpectedBase< ErrorType, ValueType... >::elExpectedBase(
    storage_t&& store, const bool hasError ) noexcept
    : store( std::move( store ) ), hasError( hasError )
{
}

template < typename ErrorType, typename... ValueType >
inline bool
elExpectedBase< ErrorType, ValueType... >::HasError() const noexcept
{
    return this->hasError;
}

template < typename ErrorType, typename... ValueType >
inline bool
elExpectedBase< ErrorType, ValueType... >::HasValue() const noexcept
{
    return !this->hasError;
}

template < typename ErrorType, typename... ValueType >
inline elExpectedBase< ErrorType, ValueType... >::operator bool() const noexcept
{
    return !this->hasError;
}

template < typename ErrorType, typename... ValueType >
inline ErrorType&
elExpectedBase< ErrorType, ValueType... >::GetError() & noexcept
{
    return std::get< 0 >( this->store );
}

template < typename ErrorType, typename... ValueType >
inline const ErrorType&
elExpectedBase< ErrorType, ValueType... >::GetError() const& noexcept
{
    return const_cast< elExpectedBase* >( this )->GetError();
}

template < typename ErrorType, typename... ValueType >
inline ErrorType&&
elExpectedBase< ErrorType, ValueType... >::GetError() && noexcept
{
    return std::move( std::get< 0 >( this->store ) );
}

template < typename ErrorType, typename... ValueType >
inline const ErrorType&&
elExpectedBase< ErrorType, ValueType... >::GetError() const&& noexcept
{
    return std::move( const_cast< elExpectedBase* >( this )->GetError() );
}

template < typename ErrorType, typename... ValueType >
inline ErrorType
elExpectedBase< ErrorType, ValueType... >::GetErrorOr(
    const ErrorType& error ) const noexcept
{
    return ( this->hasError ) ? std::get< 0 >( this->store ) : error;
}

template < typename ErrorType, typename... ValueType >
inline ErrorType*
elExpectedBase< ErrorType, ValueType... >::GetErrorPointer() noexcept
{
    return &this->GetError();
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType >, void >,
    const elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::AndThen(
    const CallableType& call ) const& noexcept
{
    if ( this->HasValue() ) return call();
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    const elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::AndThen(
    const CallableType& call ) const& noexcept
{
    if ( this->HasValue() ) call();
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::AndThen(
    const CallableType& call ) & noexcept
{
    if ( this->HasValue() ) return call();
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::AndThen(
    const CallableType& call ) & noexcept
{
    if ( this->HasValue() ) call();
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType >&& >
elExpectedBase< ErrorType, ValueType... >::AndThen(
    const CallableType& call ) && noexcept
{
    if ( this->HasValue() ) call();
    return std::move(
        *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this ) );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
    const elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) const& noexcept
{
    if ( this->HasError() ) call( this->GetError() );
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
    const elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) const& noexcept
{
    if ( this->HasError() ) return call( this->GetError() );
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    const elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) const& noexcept
{
    if ( this->HasError() ) call();
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType >, void >,
    const elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) const& noexcept
{
    if ( this->HasError() ) return call();
    return *reinterpret_cast< const elExpected< ValueType..., ErrorType >* >(
        this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
    elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) & noexcept
{
    if ( this->HasError() ) call( this->GetError() );
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
    elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) & noexcept
{
    if ( this->HasError() ) return call( this->GetError() );
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType >& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) & noexcept
{
    if ( this->HasError() ) call();
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType > >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) & noexcept
{
    if ( this->HasError() ) return call();
    return *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ErrorType >, void >,
    elExpected< ValueType..., ErrorType >&& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) && noexcept
{
    if ( this->HasError() ) call( this->GetError() );
    return std::move(
        *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this ) );
}

template < typename ErrorType, typename... ValueType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType >, void >,
    elExpected< ValueType..., ErrorType >&& >
elExpectedBase< ErrorType, ValueType... >::OrElse(
    const CallableType& call ) && noexcept
{
    if ( this->HasError() ) call();
    return std::move(
        *reinterpret_cast< elExpected< ValueType..., ErrorType >* >( this ) );
}

} // namespace internal

template < typename T >
inline Success< T >::Success( const T& t ) : value( t )
{
}

template < typename T >
inline Error< T >::Error( const T& t ) : value( t )
{
}

template < typename ValueType, typename ErrorType >
inline elExpected< ValueType, ErrorType >::elExpected(
    storage_t&& store, const bool hasError ) noexcept
    : internal::elExpectedBase< ErrorType, ValueType >( std::move( store ),
                                                        hasError )
{
}

template < typename ValueType, typename ErrorType >
inline elExpected< ValueType, ErrorType >::elExpected(
    const Success< ValueType >& success )
    : internal::elExpectedBase< ErrorType, ValueType >(
          storage_t( std::in_place_index_t< 1 >(), success.value ), false )
{
}

template < typename ValueType, typename ErrorType >
inline elExpected< ValueType, ErrorType >::elExpected(
    const Error< ErrorType >& error )
    : internal::elExpectedBase< ErrorType, ValueType >(
          storage_t( std::in_place_index_t< 0 >(), error.value ), true )
{
}

template < typename ValueType, typename ErrorType >
template < typename... Targs >
inline elExpected< ValueType, ErrorType >
elExpected< ValueType, ErrorType >::CreateValue( Targs&&... args ) noexcept
{
    elExpected< ValueType, ErrorType > returnValue(
        storage_t( std::in_place_index_t< 1 >(),
                   std::forward< Targs >( args )... ),
        false );

    return returnValue;
}

template < typename ValueType, typename ErrorType >
template < typename... Targs >
inline elExpected< ValueType, ErrorType >
elExpected< ValueType, ErrorType >::CreateError( Targs&&... args ) noexcept
{
    elExpected< ValueType, ErrorType > returnValue(
        storage_t( std::in_place_index_t< 0 >(),
                   std::forward< Targs >( args )... ),
        true );

    return returnValue;
}

template < typename ValueType, typename ErrorType >
inline ValueType&
elExpected< ValueType, ErrorType >::GetValue() & noexcept
{
    return std::get< 1 >( this->store );
}

template < typename ValueType, typename ErrorType >
inline const ValueType&
elExpected< ValueType, ErrorType >::GetValue() const& noexcept
{
    return const_cast< elExpected* >( this )->GetValue();
}

template < typename ValueType, typename ErrorType >
inline ValueType*
elExpected< ValueType, ErrorType >::GetValuePointer() noexcept
{
    return &this->GetValue();
}

template < typename ValueType, typename ErrorType >
inline ValueType&&
elExpected< ValueType, ErrorType >::GetValue() && noexcept
{
    return std::move( std::get< 1 >( this->store ) );
}

template < typename ValueType, typename ErrorType >
inline const ValueType&&
elExpected< ValueType, ErrorType >::GetValue() const&& noexcept
{
    return const_cast< elExpected* >( this )->GetValue();
}

template < typename ValueType, typename ErrorType >
inline ValueType
elExpected< ValueType, ErrorType >::GetValueOr(
    const ValueType& value ) const noexcept
{
    return ( this->HasValue() ) ? this->GetValue() : value;
}

template < typename ValueType, typename ErrorType >
inline ValueType*
elExpected< ValueType, ErrorType >::operator->() noexcept
{
    return &this->GetValue();
}

template < typename ValueType, typename ErrorType >
inline const ValueType*
elExpected< ValueType, ErrorType >::operator->() const noexcept
{
    return &this->GetValue();
}

template < typename ValueType, typename ErrorType >
inline ValueType&
elExpected< ValueType, ErrorType >::operator*() noexcept
{
    return this->GetValue();
}

template < typename ValueType, typename ErrorType >
inline const ValueType&
elExpected< ValueType, ErrorType >::operator*() const noexcept
{
    return const_cast< elExpected* >( this )->operator*();
}

template < typename ValueType, typename ErrorType >
template < typename T >
inline elExpected< ValueType, ErrorType >::operator elExpected< T >() noexcept
{
    if ( this->HasError() )
        return Error( this->GetError() );
    else
        return Success<>();
}

template < typename ValueType, typename ErrorType >
template < typename T >
inline elExpected< ValueType, ErrorType >::operator elExpected< T >()
    const noexcept
{
    return const_cast< elExpected* >( this )->operator elExpected< T >();
}

template < typename ValueType, typename ErrorType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
    const elExpected< ValueType, ErrorType >& >
elExpected< ValueType, ErrorType >::AndThen(
    const CallableType& call ) const& noexcept
{
    if ( this->HasValue() ) call( this->GetValue() );
    return *this;
}

template < typename ValueType, typename ErrorType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
    const elExpected< ValueType, ErrorType > >
elExpected< ValueType, ErrorType >::AndThen(
    const CallableType& call ) const& noexcept
{
    if ( this->HasValue() ) return call( this->GetValue() );
    return *this;
}

template < typename ValueType, typename ErrorType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
    elExpected< ValueType, ErrorType >& >
elExpected< ValueType, ErrorType >::AndThen(
    const CallableType& call ) & noexcept
{
    if ( this->HasValue() ) call( this->GetValue() );
    return *this;
}

template < typename ValueType, typename ErrorType >
template < typename CallableType >
inline std::enable_if_t<
    !std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
    elExpected< ValueType, ErrorType > >
elExpected< ValueType, ErrorType >::AndThen(
    const CallableType& call ) & noexcept
{
    if ( this->HasValue() ) return call( this->GetValue() );
    return *this;
}

template < typename ValueType, typename ErrorType >
template < typename CallableType >
inline std::enable_if_t<
    std::is_same_v< std::invoke_result_t< CallableType, ValueType >, void >,
    elExpected< ValueType, ErrorType >&& >
elExpected< ValueType, ErrorType >::AndThen(
    const CallableType& call ) && noexcept
{
    if ( this->HasValue() ) call( this->GetValue() );
    return std::move( *this );
}

// elExpected<ErrorType>

template < typename ErrorType >
inline elExpected< ErrorType >::elExpected( storage_t&& store,
                                            const bool  hasError ) noexcept
    : internal::elExpectedBase< ErrorType >( std::move( store ), hasError )
{
}

template < typename ErrorType >
inline elExpected< ErrorType >::elExpected( const Success< void >& )
    : internal::elExpectedBase< ErrorType >( storage_t(), false )
{
}

template < typename ErrorType >
inline elExpected< ErrorType >::elExpected( const Error< ErrorType >& error )
    : internal::elExpectedBase< ErrorType >( error.value, true )
{
}

template < typename ErrorType >
inline elExpected< ErrorType >
elExpected< ErrorType >::CreateValue() noexcept
{
    elExpected< ErrorType > returnValue( storage_t(), false );

    return returnValue;
}

template < typename ErrorType >
template < typename... Targs >
inline elExpected< ErrorType >
elExpected< ErrorType >::CreateError( Targs&&... args ) noexcept
{
    elExpected< ErrorType > returnValue(
        storage_t( std::forward< Targs >( args )... ), true );

    return returnValue;
}
} // namespace bb
} // namespace el3D
