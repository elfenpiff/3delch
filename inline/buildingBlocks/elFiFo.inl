namespace el3D
{
namespace bb
{
template < typename T >
template < typename... Targs >
inline void
elFiFo< T >::Push( Targs&&... args ) noexcept
{
    this->data.push( T( std::forward< Targs >( args )... ) );
}

template < typename T >
template < typename Allocator >
inline void
elFiFo< T >::MultiPush( const std::vector< T, Allocator >& values ) noexcept
{
    for ( auto& v : values )
        this->data.push( v );
}

template < typename T >
inline std::optional< T >
elFiFo< T >::Pop() noexcept
{
    if ( this->data.empty() ) return std::nullopt;

    std::optional< T > returnValue =
        std::make_optional< T >( this->data.front() );

    this->data.pop();
    return returnValue;
}

template < typename T >
template < typename OutputContainer >
inline std::optional< OutputContainer >
elFiFo< T >::MultiPop( const size_t n ) noexcept
{
    if ( this->Size() < n ) return std::nullopt;

    size_t limit = ( n == 0 ) ? this->Size() : n;

    std::optional< OutputContainer > returnValue =
        std::make_optional< OutputContainer >( limit );

    for ( size_t k = 0; k < limit; ++k )
    {
        returnValue->at( k ) = this->data.front();
        this->data.pop();
    }

    return returnValue;
}

template < typename T >
inline void
elFiFo< T >::Clear() noexcept
{
    for ( size_t k = 0, limit = this->Size(); k < limit; ++k )
        this->data.pop();
}

template < typename T >
inline bool
elFiFo< T >::IsEmpty() const noexcept
{
    return this->data.empty();
}

template < typename T >
inline uint64_t
elFiFo< T >::Size() const noexcept
{
    return this->data.size();
}
} // namespace bb
} // namespace el3D
