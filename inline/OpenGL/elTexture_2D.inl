namespace el3D
{
namespace OpenGL
{
template < typename T >
inline const elTexture_2D&
elTexture_2D::TexImage( const std::vector< T >& pixels ) const noexcept
{
    return this->TexImage( pixels.data() );
}

template < typename T >
inline const elTexture_2D&
elTexture_2D::TexSubImage( const GLint offsetX, const GLint offsetY,
                           const uint64_t width, const uint64_t height,
                           const std::vector< T >& pixels ) const noexcept
{
    return this->TexSubImage( offsetX, offsetY, static_cast< GLsizei >( width ),
                              static_cast< GLsizei >( height ), pixels.data() );
}


} // namespace OpenGL
} // namespace el3D
