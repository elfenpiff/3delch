namespace el3D
{
namespace OpenGL
{
template < typename T >
std::vector< T >
elFrameBufferObject::ReadPixels( const GLint x, const GLint y,
                                 const GLsizei width, const GLsizei height,
                                 const GLsizei valueSize,
                                 const size_t  attachment,
                                 const size_t  n ) noexcept
{
    elTexture_2D* tex = dynamic_cast< elTexture_2D* >(
        this->frameBuffer.data[n].textures[attachment] );
    std::vector< T > retVal(
        static_cast< size_t >( width * height * valueSize ) );
    this->Read( this->frameBuffer.data[n].attachments[attachment], n );
    gl( glReadPixels, x, y, width, height, tex->GetFormat(), tex->GetType(),
        retVal.data() );
    this->ReadStop();
    return retVal;
}
} // namespace OpenGL
} // namespace el3D
