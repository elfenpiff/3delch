namespace el3D
{
namespace OpenGL
{
template < typename F, typename... Fargs >
typename std::enable_if<
    !std::is_same< typename std::invoke_result< F, Fargs... >::type,
                   void >::value,
    typename std::invoke_result< F, Fargs... >::type >::type
__gl( const char *file, const int line, const char *func, const char *glFunc,
      F &&f, Fargs &&...fargs )
{
#ifdef ELCH_DEBUG
    internal::LastGLCall::file   = file;
    internal::LastGLCall::line   = line;
    internal::LastGLCall::func   = func;
    internal::LastGLCall::glFunc = glFunc;
#else
    (void)file;
    (void)line;
    (void)func;
    (void)glFunc;
#endif

    auto result = f( std::forward< Fargs >( fargs )... );

#ifdef ELCH_DEBUG
    internal::Call( file, line, func, glFunc );
#endif
    return result;
}

template < typename F, typename... Fargs >
typename std::enable_if<
    std::is_same< typename std::invoke_result< F, Fargs... >::type,
                  void >::value,
    void >::type
__gl( const char *file, const int line, const char *func, const char *glFunc,
      F &&f, Fargs &&...fargs )
{
#ifdef ELCH_DEBUG
    internal::LastGLCall::file   = file;
    internal::LastGLCall::line   = line;
    internal::LastGLCall::func   = func;
    internal::LastGLCall::glFunc = glFunc;
#else
    (void)file;
    (void)line;
    (void)func;
    (void)glFunc;
#endif

    f( std::forward< Fargs >( fargs )... );

#ifdef ELCH_DEBUG
    internal::Call( file, line, func, glFunc );
#endif
}
} // namespace OpenGL
} // namespace el3D
