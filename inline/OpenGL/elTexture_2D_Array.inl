namespace el3D
{
namespace OpenGL
{
template < typename T >
inline const elTexture_2D_Array&
elTexture_2D_Array::TexImage(
    const uint64_t                    index,
    const std::initializer_list< T >& pixels ) const noexcept
{
    return this->TexImage( index, std::vector< T >( pixels ).data() );
}

template < typename T >
inline const elTexture_2D_Array&
elTexture_2D_Array::TexSubImage(
    const uint64_t index, const GLint offsetX, const GLint offsetY,
    const uint64_t width, const uint64_t height,
    const std::initializer_list< T >& pixels ) const noexcept
{
    return this->TexSubImage( index, offsetX, offsetY, width, height,
                              std::vector< T >( pixels ).data() );
}
} // namespace OpenGL
} // namespace el3D
