namespace el3D
{
namespace OpenGL
{
template <>
struct DataTypeToEnum< GLbyte >
{
    static constexpr GLenum value = GL_BYTE;
};

template <>
struct DataTypeToEnum< GLubyte >
{
    static constexpr GLenum value = GL_UNSIGNED_BYTE;
};

template <>
struct DataTypeToEnum< GLshort >
{
    static constexpr GLenum value = GL_SHORT;
};

template <>
struct DataTypeToEnum< GLushort >
{
    static constexpr GLenum value = GL_UNSIGNED_SHORT;
};

template <>
struct DataTypeToEnum< GLint >
{
    static constexpr GLenum value = GL_INT;
};

template <>
struct DataTypeToEnum< GLuint >
{
    static constexpr GLenum value = GL_UNSIGNED_INT;
};

template <>
struct DataTypeToEnum< GLfloat >
{
    static constexpr GLenum value = GL_FLOAT;
};

template <>
struct DataTypeToEnum< GLdouble >
{
    static constexpr GLenum value = GL_DOUBLE;
};


template <>
struct EnumToDataType< GL_BYTE >
{
    using type = GLbyte;
};

template <>
struct EnumToDataType< GL_UNSIGNED_BYTE >
{
    using type = GLubyte;
};

template <>
struct EnumToDataType< GL_SHORT >
{
    using type = GLshort;
};

template <>
struct EnumToDataType< GL_UNSIGNED_SHORT >
{
    using type = GLushort;
};

template <>
struct EnumToDataType< GL_INT >
{
    using type = GLint;
};

template <>
struct EnumToDataType< GL_UNSIGNED_INT >
{
    using type = GLuint;
};

template <>
struct EnumToDataType< GL_FLOAT >
{
    using type = GLfloat;
};

template <>
struct EnumToDataType< GL_DOUBLE >
{
    using type = GLdouble;
};


} // namespace OpenGL
} // namespace el3D
