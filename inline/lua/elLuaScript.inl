namespace el3D
{
namespace lua
{

#ifdef ADD_LUA_BINDINGS
class elLuaScript::class_t::NameAlreadyRegistered_e : public std::runtime_error
{
  public:
    explicit NameAlreadyRegistered_e( const std::string &msg )
        : std::runtime_error( msg )
    {
    }
    virtual ~NameAlreadyRegistered_e()
    {
    }
};


class elLuaScript::class_t::MultipleClassTypesForOneClass_e
    : public std::runtime_error
{
  public:
    explicit MultipleClassTypesForOneClass_e( const std::string &msg )
        : std::runtime_error( msg )
    {
    }
    virtual ~MultipleClassTypesForOneClass_e()
    {
    }
};

class elLuaScript::class_t::NoConstructorDefined_e : public std::runtime_error
{
  public:
    explicit NoConstructorDefined_e( const std::string &msg )
        : std::runtime_error( msg )
    {
    }
    virtual ~NoConstructorDefined_e()
    {
    }
};


inline elLuaScript::class_t::class_t( const std::string &className )
    : name( className )
{
}

template < typename Class, typename Func_t >
inline elLuaScript::class_t &
elLuaScript::class_t::AddMethod( const std::string &name, const Func_t f )
{
    ThrowExceptionIfAlreadyRegistered( name );
    using methodInfo = bb::GetMethodInfo< Func_t >;

    methods.push_back( std::make_tuple(
        name,
        LuaProxyClassMethod< Class, Func_t, typename methodInfo::ReturnType,
                             typename methodInfo::ArgumentTuple >,
        std::bind( LuaPush< Func_t >, std::placeholders::_1, f ) ) );

    return *this;
}

template < typename Func_t >
inline elLuaScript::class_t &
elLuaScript::class_t::AddStaticMethod( const std::string &name, const Func_t f )
{
    ThrowExceptionIfAlreadyRegistered( name );
    using methodInfo = bb::GetStaticMethodInfo< Func_t >;

    staticMethods.push_back( std::make_tuple(
        name,
        LuaProxyClassStaticMethod< Func_t, typename methodInfo::ReturnType,
                                   typename methodInfo::ArgumentTuple >,
        std::bind( LuaPush< Func_t >, std::placeholders::_1, f ) ) );
    return *this;
}

template < typename Var_t >
inline elLuaScript::class_t &
elLuaScript::class_t::AddVariable( const std::string &name, const Var_t var )
{
    ThrowExceptionIfAlreadyRegistered( name );
    using varInfo = bb::GetMemberVariableInfo< Var_t >;

    methodsGetSetVar.push_back( std::make_tuple(
        name,
        LuaProxyClassSet< typename varInfo::ClassType, Var_t,
                          typename varInfo::VarType >,
        std::bind( LuaPush< Var_t >, std::placeholders::_1, var ),
        LuaProxyClassGet< typename varInfo::ClassType, Var_t,
                          typename varInfo::VarType >,
        std::bind( LuaPush< Var_t >, std::placeholders::_1, var ) ) );
    return *this;
}

template < typename Var_t >
inline elLuaScript::class_t &
elLuaScript::class_t::AddStaticVariable( const std::string &name,
                                         const Var_t        var )
{
    ThrowExceptionIfAlreadyRegistered( name );
    methodsGetSetStaticVar.push_back( std::make_tuple(
        name,
        LuaProxyClassSetStatic< Var_t,
                                typename std::remove_pointer< Var_t >::type >,
        std::bind( LuaPush< Var_t >, std::placeholders::_1, var ),
        LuaProxyClassGetStatic< Var_t,
                                typename std::remove_pointer< Var_t >::type >,
        std::bind( LuaPush< Var_t >, std::placeholders::_1, var ) ) );
    return *this;
}

template < typename Class >
inline int
LuaGetThisPointer( lua_State *l )
{
    Class **thisClass = LuaTo< Class ** >( l, 1 );
    // get storage for return value
    Class **value = reinterpret_cast< Class ** >(
        lua_touserdata( l, lua_upvalueindex( 1 ) ) );
    *value = *thisClass;

    LuaPush< Class * >( l, *value );
    return 1;
}

template < typename Class >
inline void
elLuaScript::class_t::AddThisPointer( lua_State *l )
{
    LuaPush< Class * >( l, nullptr ); // storage for the return value
    lua_pushcclosure( l, LuaGetThisPointer< Class >, 1 );
}

template < typename Class, typename... CTorArgs >
inline elLuaScript::class_t &
elLuaScript::class_t::AddConstructor()
{
    this->ctor = LuaProxyClassCtor< Class, CTorArgs... >;
    return this->AddPrivateConstructor< Class >();
}

template < typename Class >
inline elLuaScript::class_t &
elLuaScript::class_t::AddPrivateConstructor()
{
    if ( this->constructorDefined )
    {
        if ( this->typeHashCode != typeid( Class ).hash_code() ||
             this->typeHashCode != typeid( Class * ).hash_code() )
            throw MultipleClassTypesForOneClass_e(
                std::string( "There are multiple class type definitions for "
                             "the class " ) +
                name );
    }
    else
    {
        this->typeHashCode          = typeid( Class ).hash_code();
        this->typeHashCodePtr       = typeid( Class * ).hash_code();
        this->thisPointerGetCreator = [this]( lua_State *l ) {
            this->AddThisPointer< Class >( l );
        };
    }

    this->dtor               = LuaProxyClassDtor< Class >;
    this->constructorDefined = true;
    return *this;
}

inline void
elLuaScript::class_t::ThrowExceptionIfAlreadyRegistered(
    const std::string &newName ) const
{
    for ( auto &e : methodsGetSetVar )
        if ( std::get< 0 >( e ) == newName )
        {
            throw NameAlreadyRegistered_e(
                std::string( "A setter or getter method already exists with "
                             "the name \"" ) +
                this->name + "::" + newName +
                "\"! For variables Set and Get methods are automatically "
                "added." );
        }

    for ( auto &e : methodsGetSetStaticVar )
        if ( std::get< 0 >( e ) == newName )
        {
            throw NameAlreadyRegistered_e(
                std::string(
                    "A static setter or getter method already exists with "
                    "the name \"" ) +
                this->name + "::" + newName +
                "\"! For static variables static Set and Get methods are "
                "automatically "
                "added." );
        }

    for ( auto &e : methods )
        if ( std::get< 0 >( e ) == newName )
        {
            throw NameAlreadyRegistered_e(
                std::string( "A  method already exists with "
                             "the name \"" ) +
                this->name + "::" + newName + "\"" );
        }

    for ( auto &e : staticMethods )
        if ( std::get< 0 >( e ) == newName )
        {
            throw NameAlreadyRegistered_e(
                std::string( "A static method already exists with "
                             "the name \"" ) +
                this->name + "::" + newName + "\"" );
        }
}
#endif

template < typename Tret, typename T, typename... Targs >
inline bb::elExpected< std::vector< Tret >, elLuaScript_Error >
elLuaScript::Call( const std::string &func, const T &t, Targs... args ) const
{
    if ( this->doExecBeforeCall )
    {
        auto call = this->Exec();
        if ( call.HasError() ) return bb::Error( call.GetError() );
    }

    int  n    = lua_gettop( this->luaState );
    auto call = this->GetGlobalFunc( func ).AndThen( [&] {
        size_t s = CallLuaRecursion( 0ul, t, args... );
        return ErrorHandler( lua_pcall(
            this->luaState, static_cast< int >( s + 1ul ), LUA_MULTRET, 0 ) );
    } );

    n = lua_gettop( this->luaState ) - n;

    auto retVal = LuaToVector< Tret >( static_cast< size_t >( n ) );
    lua_pop( this->luaState, n );
    if ( call.HasError() ) return bb::Error( call.GetError() );
    return bb::Success( retVal );
}

template < typename Tret >
inline bb::elExpected< std::vector< Tret >, elLuaScript_Error >
elLuaScript::Call( const std::string &func ) const
{
    if ( this->doExecBeforeCall )
    {
        auto call = this->Exec();
        if ( call.HasError() ) return bb::Error( call.GetError() );
    }

    int  n         = lua_gettop( this->luaState );
    auto getGlobal = this->GetGlobalFunc( func );
    if ( getGlobal.HasError() ) return bb::Error( getGlobal.GetError() );

    auto errorHandler =
        this->ErrorHandler( lua_pcall( this->luaState, 0, LUA_MULTRET, 0 ) );
    if ( errorHandler.HasError() ) return bb::Error( errorHandler.GetError() );

    n = lua_gettop( this->luaState ) - n;

    auto retVal = LuaToVector< Tret >( static_cast< size_t >( n ) );
    lua_pop( this->luaState, n );
    return bb::Success( retVal );
}

template < typename T, typename... Targs >
inline bb::elExpected< elLuaScript_Error >
elLuaScript::CallVoid( const std::string &func, const T &t,
                       Targs... args ) const
{
    if ( this->doExecBeforeCall ) this->Exec();

    auto call = this->GetGlobalFunc( func ).AndThen( [&] {
        size_t s = CallLuaRecursion( 0, t, args... );
        return ErrorHandler( lua_pcall(
            this->luaState, static_cast< int >( s + 1ul ), LUA_MULTRET, 0 ) );
    } );
    if ( call.HasError() ) return bb::Error( call.GetError() );
    return bb::Success<>();
}

template < typename T >
inline void
elLuaScript::SetGlobal( const std::string &varName,
                        const T            value ) const noexcept
{
    LuaPush< T >( this->luaState, value );
    lua_setglobal( this->luaState, varName.c_str() );
    this->active.globals[varName] =
        std::bind( LuaPush< T >, std::placeholders::_1, value );
}

template < typename T >
inline void
elLuaScript::SetGlobalInNamespace( const std::string &varName, const T value,
                                   const std::string nameSpace ) noexcept
{
    elLuaScript::nameSpaces[nameSpace].variables.push_back( std::bind(
        &elLuaScript::SetGlobal< T >, std::placeholders::_1, varName, value ) );
}

template < typename T >
inline std::vector< T >
elLuaScript::GetGlobal( const std::vector< std::string > &lst ) const noexcept
{
    if ( this->doExecBeforeCall ) this->Exec();
    if ( lst.empty() ) return std::vector< T >();

    for ( auto entry = lst.begin(); entry != lst.end(); ++entry )
    {
        if ( entry == lst.begin() )
        { // get table name
            lua_getglobal( this->luaState, entry->c_str() );
            // not a table return var value
            if ( !lua_isnil( this->luaState, -1 ) &&
                 !lua_istable( this->luaState, -1 ) )
            {
                return GetGlobalByType< T >();
            }
            else if ( lua_isnil( this->luaState, -1 ) )
            {
                return std::vector< T >();
            }
        }
        else
        {
            if ( !this->IterateToEntryInTable( *entry ) )
            {
                lua_pop( this->luaState, lua_gettop( this->luaState ) );
                return std::vector< T >();
            }
        }
    }

    // we are at the right position in the table and return the entry
    auto retVal = GetGlobalByType< T >();
    return retVal;
}

template < typename Func_t, typename Return_t, typename... Targs >
inline void
elLuaScript::RegisterFunctionHelper( const std::string &name,
                                     Return_t( f )( Targs... ),
                                     const std::string &nameSpace ) noexcept
{
    elLuaScript::nameSpaces[nameSpace].functions.push_back( std::bind(
        LuaPushFunction< Func_t, Targs... >, std::placeholders::_1, name, f ) );
}

template < typename Func_t, typename... Targs >
inline void
elLuaScript::RegisterFunction( const std::string &name, const Func_t f,
                               const std::string &nameSpace ) noexcept
{
    elLuaScript::RegisterFunctionHelper<
        typename std::remove_pointer< Func_t >::type >( name, f, nameSpace );
}

template < typename T >
inline std::vector< T >
elLuaScript::LuaToVector( const size_t &nresult ) const noexcept
{
    std::vector< T > result( nresult );
    for ( size_t i = 0; i < nresult; i++ )
        result[nresult - i - 1] =
            LuaTo< T >( this->luaState, static_cast< int >( -( i + 1 ) ) );
    return result;
}

template < typename T >
inline size_t
elLuaScript::CallLuaRecursion( const size_t &argNo, const T &t ) const noexcept
{
    LuaPush< T >( this->luaState, t );
    return argNo;
}

template < typename T, typename... Targs >
inline size_t
elLuaScript::CallLuaRecursion( const size_t &argNo, const T &t,
                               Targs... args ) const noexcept
{
    CallLuaRecursion( argNo, t );
    return CallLuaRecursion( argNo + 1, args... );
}

template < typename T >
inline std::vector< T >
elLuaScript::GetGlobalByType() const noexcept
{
    std::vector< T > value;

    if ( lua_istable( this->luaState, -1 ) )
    { // lua var is array, iterate through it
        for ( lua_pushnil( this->luaState ); lua_next( this->luaState, -2 );
              lua_pop( this->luaState, 1 ) )
            value.push_back( LuaTo< T >( this->luaState, -1 ) );
    }
    else
    { // lua var is a simple var
        value.push_back( LuaTo< T >( this->luaState, -1 ) );
    }

    lua_pop( this->luaState, lua_gettop( this->luaState ) );
    return value;
}

template < typename T >
inline void
elLuaScript::RegisterGlobal( const std::string &              groupName,
                             const std::map< std::string, T > values,
                             const std::string &              nameSpace )
{
    globalRegistrators[groupName] = [=]( elLuaScript *const script ) {
        for ( auto v : values )
            script->SetGlobal< T >( v.first, v.second );
    };

    if ( !nameSpace.empty() )
        elLuaScript::nameSpaces[nameSpace].constants.push_back( std::bind(
            &elLuaScript::RequireGlobal, std::placeholders::_1, groupName ) );
}

template < typename ReturnValue, typename... Arguments >
std::enable_if_t< std::is_same_v< ReturnValue, void >,
                  std::function< ReturnValue( Arguments... ) > >
elLuaScript::CreateCallbackToFunction( const std::string &functionName )
{
    if ( functionName.empty() )
    {
        LOG_WARN( 0 ) << "empty function name returns empty std::function "
                         "callback";
        return std::function< ReturnValue( Arguments... ) >();
    }
    else
        return std::function< ReturnValue( Arguments... ) >(
            [this, functionName]( Arguments... args ) {
                this->CallVoid( functionName, args... );
            } );
}

template < typename ReturnValue, typename... Arguments >
std::enable_if_t< !std::is_same_v< ReturnValue, void >,
                  std::function< ReturnValue( Arguments... ) > >
elLuaScript::CreateCallbackToFunction( const std::string &functionName )
{
    if ( functionName.empty() )
    {
        LOG_WARN( 0 ) << "empty function name returns empty std::function "
                         "callback";
        return std::function< ReturnValue( Arguments... ) >();
    }
    else
    {
        return std::function< ReturnValue( Arguments... ) >(
            [this, functionName]( Arguments... args ) -> ReturnValue {
                auto result = this->Call< bool >( functionName, args... );
                if ( result.HasError() )
                {
                    LOG_ERROR( 0 ) << "an error occured during the "
                                   << functionName << " call";
                    return ReturnValue();
                }
                else
                {
                    if ( result.GetValue().empty() )
                    {
                        LOG_ERROR( 0 )
                            << "function " << functionName
                            << " should return something but returned "
                               "nothing";
                        return ReturnValue();
                    }
                    else if ( result.GetValue().size() != 1 )
                    {
                        LOG_WARN( 0 )
                            << "function " << functionName
                            << " returned more then one return value but "
                               "only single return values are supported. "
                               "Returning the first one!";
                        return result.GetValue()[0];
                    }
                    else
                    {
                        // we need to cast the value since vector<bool> returns
                        // proxy objects
                        ReturnValue retVal =
                            static_cast< ReturnValue >( result.GetValue()[0] );
                        return retVal;
                    }
                }
            } );
    }
}

} // namespace lua
} // namespace el3D
