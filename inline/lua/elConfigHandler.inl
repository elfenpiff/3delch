namespace el3D
{
namespace lua
{
template < typename T >
inline std::vector< T >
elConfigHandler::Get( const std::vector< std::string > &configPath,
                      const std::vector< T > &defaultValue ) const noexcept
{
    std::vector< T > retVal;
    for ( auto &f : configFiles )
    {
        if ( !f.second->HasGlobal( configPath ) ) continue;
        retVal = f.second->GetGlobal< T >( configPath );
        if ( !retVal.empty() ) return retVal;
    }

    return defaultValue;
}

template < typename T, uint64_t ExpectedSize >
inline std::vector< T >
elConfigHandler::GetWithSizeRestriction(
    const std::vector< std::string > &   configPath,
    const std::array< T, ExpectedSize > &defaultValue ) const noexcept
{
    std::vector< T > retVal;
    for ( auto &f : configFiles )
    {
        if ( !f.second->HasGlobal( configPath ) ) continue;
        retVal = f.second->GetGlobal< T >( configPath );
        if ( !retVal.empty() )
        {
            if ( retVal.size() != ExpectedSize )
            {
                std::cerr << "config entry \""
                          << this->PathToString( configPath )
                          << "\" has not the expected size of " << ExpectedSize
                          << ". Returning default value!\n";

                retVal.clear();
                for ( auto &val : defaultValue )
                    retVal.emplace_back( val );
            }
            return retVal;
        }
    }

    retVal.clear();
    for ( auto &val : defaultValue )
        retVal.emplace_back( val );
    return retVal;
}


} // namespace lua
} // namespace el3D
