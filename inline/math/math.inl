namespace el3D
{
namespace math
{
template < typename T >
inline NoGLMType< T >
Distance( const T& p1, const T& p2 ) noexcept
{
    return std::abs( p2 - p1 );
}

template < typename T >
inline typename GLMType< T >::value_type
Distance( const T& p1, const T& p2 ) noexcept
{
    return glm::distance( p1, p2 );
}

template < typename T >
inline NoGLMType< T >
Normalize( const T& value ) noexcept
{
    return value / value;
}

template < typename T >
inline GLMType< T >
Normalize( const T& value ) noexcept
{
    return glm::normalize( value );
}
} // namespace math
} // namespace el3D

