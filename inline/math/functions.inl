namespace el3D
{
namespace math
{
template < typename T >
inline constexpr T
Fract( const T t ) noexcept
{
    return fabs( t ) - floor( fabs( t ) );
}

template < typename T >
inline constexpr T
MapTo( const T t, const std::array< T, 2 >& oldRange,
       const std::array< T, 2 >& newRange ) noexcept
{
    T oldRangeLength = oldRange[1] - oldRange[0];
    T newRangeLength = newRange[1] - newRange[0];
    T rangeDistance  = newRange[0] - oldRange[0];

    return std::clamp( ( t + rangeDistance ) / oldRangeLength * newRangeLength,
                       newRange[0], newRange[1] );
}

template < typename T >
inline constexpr T
Fade( const T t ) noexcept
{
    return t * t * t * ( t * ( t * 6 - 15 ) + 10 );
}

template < typename T >
inline constexpr T
Lerp( const T t, const T a, const T b ) noexcept
{
    return a + t * ( b - a );
}

template < typename T >
struct NextPowerOfTwoImpl;

template <>
struct NextPowerOfTwoImpl< uint8_t >
{
    static constexpr uint8_t
    Do( const uint8_t t ) noexcept
    {
        uint8_t result = t - 1;
        result |= static_cast< uint8_t >( result >> 1 );
        result |= static_cast< uint8_t >( result >> 2 );
        result |= static_cast< uint8_t >( result >> 4 );
        return static_cast< uint8_t >( result + 1 + ( t == 0 ) );
    }
};

template <>
struct NextPowerOfTwoImpl< uint16_t >
{
    static constexpr uint16_t
    Do( const uint16_t t ) noexcept
    {
        uint16_t result = t - 1;
        result |= static_cast< uint16_t >( result >> 1 );
        result |= static_cast< uint16_t >( result >> 2 );
        result |= static_cast< uint16_t >( result >> 4 );
        result |= static_cast< uint16_t >( result >> 8 );
        return static_cast< uint16_t >( result + 1 + ( t == 0 ) );
    }
};

template <>
struct NextPowerOfTwoImpl< uint32_t >
{
    static constexpr uint32_t
    Do( const uint32_t t ) noexcept
    {
        uint32_t result = t - 1;
        result |= static_cast< uint32_t >( result >> 1u );
        result |= static_cast< uint32_t >( result >> 2u );
        result |= static_cast< uint32_t >( result >> 4u );
        result |= static_cast< uint32_t >( result >> 8u );
        result |= static_cast< uint32_t >( result >> 16u );
        return static_cast< uint32_t >( result + 1 + ( t == 0 ) );
    }
};

template <>
struct NextPowerOfTwoImpl< uint64_t >
{
    static constexpr uint64_t
    Do( const uint64_t t ) noexcept
    {
        uint64_t result = t - 1;
        result |= static_cast< uint64_t >( result >> 1u );
        result |= static_cast< uint64_t >( result >> 2u );
        result |= static_cast< uint64_t >( result >> 4u );
        result |= static_cast< uint64_t >( result >> 8u );
        result |= static_cast< uint64_t >( result >> 16u );
        result |= static_cast< uint64_t >( result >> 32u );
        return static_cast< uint64_t >( result + 1 + ( t == 0 ) );
    }
};

template < typename T >
inline constexpr typename std::make_unsigned< T >::type
NextPowerOfTwo( const T t ) noexcept
{
    static_assert( std::is_integral_v< T >, "only integral types are allowed" );
    using UnsignedT = typename std::make_unsigned< T >::type;
    return NextPowerOfTwoImpl< UnsignedT >::Do( static_cast< UnsignedT >( t ) );
}


} // namespace math
} // namespace el3D
