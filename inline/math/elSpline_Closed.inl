#include <cstdio>

namespace el3D
{
namespace math
{
template < typename T >
inline elSpline_Closed< T >::elSpline_Closed( const path_t< T >& path,
                                              const float factor ) noexcept
{
    uint64_t pathSize = path.size();

    T tangentStart = ( pathSize > 2 )
                         ? this->CalculateCatmullRomSplineTangent(
                               path[pathSize - 1], path[1], factor )
                         : T( 0 );
    T tangentEnd = ( pathSize > 2 ) ? this->CalculateCatmullRomSplineTangent(
                                          path[pathSize - 2], path[0], factor )
                                    : T( 0 );

    this->GenerateSpline( pathSize, path,
                          this->GenerateTangents( pathSize, path, factor,
                                                  tangentStart, tangentEnd ) );

    // add curve which closes the spline
    this->AddCurve(
        { path.back() * 2.0f - path.front() * 2.0f + tangentEnd + tangentStart,
          path.back() * ( -3.0f ) + path.front() * 3.0f +
              tangentEnd * ( -2.0f ) - tangentStart,
          tangentEnd, path.back() } );
}

} // namespace math
} // namespace el3D
