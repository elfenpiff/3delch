#include <stdio.h>

namespace el3D
{
namespace math
{
template < typename T >
inline float
elSpline_base< T >::Length() const noexcept
{
    return this->splineLength;
}

template < typename T >
inline T
elSpline_base< T >::operator()( const float t ) const noexcept
{
    auto curveParam = this->GetCurveNumberAndAdjustedT( t );

    auto& curve = this->curves[curveParam.curveNumber];

    return curve.value( curveParam.adjustedT / curve.length );
}

template < typename T >
inline std::vector< T >
elSpline_base< T >::GenerateTangents( const uint64_t     pathSize,
                                      const path_t< T >& path,
                                      const float factor, const T& tangentStart,
                                      const T& tangentEnd ) const noexcept
{
    std::vector< T > tangents;
    tangents.reserve( pathSize );
    tangents.emplace_back( tangentStart );
    for ( uint64_t i = 0; i + 2 < pathSize; ++i )
        tangents.emplace_back( this->CalculateCatmullRomSplineTangent(
            path[i], path[i + 2], factor ) );

    tangents.emplace_back( tangentEnd );
    return tangents;
}

template < typename T >
inline void
elSpline_base< T >::GenerateSpline( const uint64_t          pathSize,
                                    const path_t< T >&      path,
                                    const std::vector< T >& tangents ) noexcept
{
    // calc coefficients for p(t) = at^3 + bt^2 + ct + d
    for ( uint64_t i = 0; i + 1 < pathSize; ++i )
    {
        this->AddCurve( { path[i] * 2.0f - path[i + 1] * 2.0f + tangents[i] +
                              tangents[i + 1],
                          path[i] * ( -3.0f ) + path[i + 1] * 3.0f +
                              tangents[i] * ( -2.0f ) - tangents[i + 1],
                          tangents[i], path[i] } );
    }
}

template < typename T >
inline void
elSpline_base< T >::AddCurve( const elCubicCurve< T >& curve ) noexcept
{
    this->curves.emplace_back(
        curve_t{ curve, curve.Length( LENGTH_CALC_STEPS ) } );
    this->splineLength += this->curves.back().length;
    ++this->size;
}

template < typename T >
inline T
elSpline_base< T >::CalculateCatmullRomSplineTangent(
    const T& p0, const T& p2, const float t ) const noexcept
{
    return Normalize( ( p2 - p0 ) * t );
}


template < typename T >
inline typename elSpline_base< T >::curveParam_t
elSpline_base< T >::GetCurveNumberAndAdjustedT( const float t ) const noexcept
{
    float adjustedT =
        ( t < 0.0f )
            ? ( t +
                ( 1 + ( std::floor( std::abs( t ) / this->splineLength ) ) ) *
                    this->splineLength )
            : ( t - std::floor( t / this->splineLength ) * this->splineLength );

    float currentLength = 0.0f;
    for ( uint64_t i = 0; i < this->size; ++i )
    {
        currentLength += this->curves[i].length;
        if ( adjustedT < currentLength )
            return { i,
                     this->curves[i].length - ( currentLength - adjustedT ) };
    }

    return { 0, 0 };
}

} // namespace math
} // namespace el3D
