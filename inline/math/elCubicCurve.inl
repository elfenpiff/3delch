namespace el3D
{
namespace math
{
template < typename T >
inline elCubicCurve< T >::elCubicCurve( const T& a, const T& b, const T& c,
                                        const T& d ) noexcept
    : a( a ), b( b ), c( c ), d( d )
{
}

template < typename T >
inline T
elCubicCurve< T >::operator()( const float t ) const noexcept
{
    return this->a * t * t * t + this->b * t * t + this->c * t + this->d;
}

template < typename T >
inline T
elCubicCurve< T >::GetValueOfDerivate_1st( const float t ) const noexcept
{
    return 3.0f * this->a * t * t + 2.0f * this->b * t + this->c;
}

template < typename T >
inline T
elCubicCurve< T >::GetValueOfDerivate_2nd( const float t ) const noexcept
{
    return 6.0f * this->a * t + 2.0f * this->b;
}

template < typename T >
inline float
elCubicCurve< T >::Length( const uint64_t resolution ) const noexcept
{
    float stepSize = 1.0f / static_cast< float >( resolution );
    float length   = 0.0f;

    for ( float t_i = 0.0f; t_i + stepSize <= 1.0f; t_i += stepSize )
        length += Distance( ( *this )( t_i + stepSize ), ( *this )( t_i ) );

    return length;
}

} // namespace math
} // namespace el3D

