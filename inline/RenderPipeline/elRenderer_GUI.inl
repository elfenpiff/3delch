namespace el3D
{
namespace RenderPipeline
{
template < typename T >
inline std::enable_if_t< !std::is_same_v< T, GuiGL::elWidget_MouseCursor >,
                         GuiGL::widget_t< T > >
elRenderer_GUI::Create( const std::string& className )
{
    return this->windowLayer->Create< T >( className );
}
} // namespace RenderPipeline
} // namespace el3D
