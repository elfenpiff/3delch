namespace el3D
{
namespace RenderPipeline
{
template < typename T, typename... Targs >
inline Deferred::Object< T >
elRenderer_Deferred::CreateObject( Targs &&...args ) noexcept
{
    Deferred::Object< T > newObject;
    newObject =
        this->factory.CreateProduct< T >( std::forward< Targs >( args )... );
    newObject.Extension() = this->GenerateObjectSettings( *newObject );

    return newObject;
}
} // namespace RenderPipeline
} // namespace el3D
