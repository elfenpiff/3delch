namespace el3D
{
namespace network
{
template < typename T >
inline bb::elExpected< elNetworkSocket_Error >
elEasyTCPMessaging_Client::Send( const uint64_t         transmissionID,
                                 const etm::MessageType messageType,
                                 const T &              payload ) noexcept
{
    utils::elByteSerializer serialized;
    serialized.Serialize(
        utils::Endian::Big,
        etm::Transmission_t< T >( transmissionID, messageType, payload ) );

    return this->connection.Send( serialized.GetByteStream() );
}

template < typename T >
inline std::optional< etm::Transmission_t< T > >
elEasyTCPMessaging_Client::TryReceive() noexcept
{
    return this->InternalReceive< T >( [&]( uint64_t length ) {
        return this->connection.TryReceive( length );
    } );
}

template < typename T >
inline std::optional< etm::Transmission_t< T > >
elEasyTCPMessaging_Client::TimedReceive( const units::Time &timeout ) noexcept
{
    return this->InternalReceive< T >( [&]( uint64_t length ) {
        return this->connection.TimedReceive( timeout, length );
    } );
}

template < typename T >
inline std::optional< etm::Transmission_t< T > >
elEasyTCPMessaging_Client::BlockingReceive() noexcept
{
    return this->InternalReceive< T >( [&]( uint64_t length ) {
        return this->connection.BlockingReceive( length );
    } );
}

template < typename T >
inline std::optional< etm::Transmission_t< T > >
elEasyTCPMessaging_Client::InternalReceive(
    const std::function< std::optional< utils::byteStream_t >( uint64_t ) >
        &receiver ) noexcept
{
    if ( !this->currentPacketLength.has_value() )
    {
        this->currentPacketLength = receiver( 8UL );
        if ( !this->currentPacketLength.has_value() ) return std::nullopt;
    }
    else
    {
        LOG_DEBUG( 0 ) << "retrying to receive announced data";
    }

    auto extractedLength =
        utils::elByteDeserializer( *this->currentPacketLength )
            .Get< uint64_t >( utils::Endian::Big, 0 );
    if ( extractedLength.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to extract length from received package. "
                          "this should never happen!";
        return std::nullopt;
    }
    auto length = extractedLength.GetValue();

    auto rawMessage = receiver( length );
    if ( !rawMessage.has_value() )
    {
        LOG_DEBUG( 0 )
            << length
            << " bytes of data were announced but could not be received";
        return std::nullopt;
    }

    utils::byteStream_t finalRawMessage = *this->currentPacketLength;
    finalRawMessage.stream.insert( finalRawMessage.stream.end(),
                                   rawMessage.value().stream.begin(),
                                   rawMessage.value().stream.end() );

    etm::Transmission_t< T > receivedMessage;
    if ( utils::elByteDeserializer( finalRawMessage )
             .Extract( utils::Endian::Big, receivedMessage )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to create fitting Transmission_t<T> type "
                          "from received message ";
        return std::nullopt;
    }

    this->currentPacketLength = std::nullopt;
    return receivedMessage;
}

} // namespace network
} // namespace el3D
