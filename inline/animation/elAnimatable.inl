namespace el3D
{
namespace animation
{
template < typename Child >
template <
    template < typename, template < typename > class > class AnimationType,
    template < typename > class InterpolationPolicy, typename SetCallback >
inline AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >&
elAnimatable< Child >::Animate( const SetCallback setter ) noexcept
{
    return this
        ->Animate< AnimationType, InterpolationPolicy, SetCallback, Child >(
            setter, nullptr );
}

template < typename Child >
template < template < typename, template < typename > class >
           class AnimationType,
           template < typename > class InterpolationPolicy,
           typename SetCallback, typename ChildOverride >
inline AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >&
elAnimatable< Child >::Animate( const SetCallback    setter,
                                ChildOverride* const childPointer ) noexcept
{
    static_assert( HasOneArgument< SetCallback >,
                   "Only members with one argument are supported!" );
    static_assert( HasReturnTypeVoid< SetCallback >,
                   "Only members with a return type of void are supported!" );

    using value_t = ArgumentType< SetCallback >;
    using anim_t  = AnimationType< value_t, InterpolationPolicy >;

    auto id   = this->CallbackToId( setter );
    auto iter = bb::find_if( this->animations,
                             [&]( auto& e ) { return e.destinationId == id; } );

    if ( iter == this->animations.end() )
    {
        anim_t*    animation = new anim_t();
        setCall_t* setCall   = new setCall_t();

        *setCall = [setter]( void* const       thisPointer,
                             const void* const value ) {
            ( ( *reinterpret_cast< ChildOverride* >( thisPointer ) ).*
              setter )( *static_cast< const value_t* >( value ) );
        };

        animation->setCallback = [this, setCall,
                                  childPointer]( const void* const v ) {
            void* thisPointer = this;
            if ( !std::is_same_v< Child, ChildOverride > )
                thisPointer = childPointer;

            ( *setCall )( thisPointer, v );
        };

        this->AddAnimation(
            animation, setCall,
            []( const elAnimate_base* const source, setCall_t* const setCall,
                void* const thisPointer ) {
                anim_t* animation =
                    new anim_t( *static_cast< const anim_t* >( source ) );
                animation->setCallback = [=]( const void* const v ) {
                    ( *setCall )( thisPointer, v );
                };
                return animation;
            },
            id );
        return *animation;
    }
    else
    {
        return *dynamic_cast< anim_t* >( iter->animation.get() );
    }
}

template < typename Child >
template < template < typename, template < typename > class >
           class AnimationType,
           template < typename > class InterpolationPolicy, typename ValueType >
inline AnimationType< ValueType, InterpolationPolicy >&
elAnimatable< Child >::AnimateFreeFunction(
    const std::function< void( const ValueType& ) >& setter ) noexcept
{
    using anim_t = AnimationType< ValueType, InterpolationPolicy >;

    auto id = this->CallbackToId(
        setter.template target< void( const ValueType& ) >() );

    auto iter = bb::find_if( this->animations,
                             [&]( auto& e ) { return e.destinationId == id; } );

    if ( iter == this->animations.end() )
    {
        anim_t*    animation = new anim_t();
        setCall_t* setCall   = new setCall_t();

        *setCall = [=]( void* const, const void* const value ) {
            setter( *static_cast< const ValueType* >( value ) );
        };

        animation->setCallback = [this, setCall]( const void* const v ) {
            ( *setCall )( this, v );
        };
        this->AddAnimation(
            animation, setCall,
            []( const elAnimate_base* const source, setCall_t* const setCall,
                void* const thisPointer ) {
                anim_t* animation =
                    new anim_t( *static_cast< const anim_t* >( source ) );
                animation->setCallback = [=]( const void* const v ) {
                    ( *setCall )( thisPointer, v );
                };
                return animation;
            },
            id );
        return *animation;
    }
    else
    {
        return *dynamic_cast< anim_t* >( iter->animation.get() );
    }
}

template < typename Child >
template <
    template < typename, template < typename > class > class AnimationType,
    template < typename > class InterpolationPolicy, typename SetCallback >
inline const AnimationType< ArgumentType< SetCallback >, InterpolationPolicy >*
elAnimatable< Child >::GetAnimation( const SetCallback setter ) const noexcept
{
    static_assert( HasOneArgument< SetCallback >,
                   "Only members with one argument are supported!" );
    static_assert( HasReturnTypeVoid< SetCallback >,
                   "Only members with a return type of void are supported!" );

    using value_t = ArgumentType< SetCallback >;
    using anim_t  = AnimationType< value_t, InterpolationPolicy >;

    auto id   = this->CallbackToId( setter );
    auto iter = bb::find_if( this->animations, [&]( const auto& e ) {
        return e.destinationId == id;
    } );

    return ( iter == this->animations.end() )
               ? nullptr
               : dynamic_cast< anim_t* >( iter->animation.get() );
}

template < typename Child >
template < typename SetCallback >
inline void
elAnimatable< Child >::RemoveAnimation( const SetCallback setter ) noexcept
{
    auto id   = this->CallbackToId( setter );
    auto iter = bb::find_if( this->animations,
                             [&]( auto& e ) { return e.destinationId == id; } );

    if ( iter != this->animations.end() ) this->animations.erase( iter );
}

template < typename Child >
template < typename SetCallback >
inline elAnimatableBase::id_t
elAnimatable< Child >::CallbackToId( const SetCallback setter ) const noexcept
{
    id_t newId;
    newId.fill( 0x00 );
    for ( uint64_t i = 0u; i < sizeof( SetCallback ); ++i )
        newId[i] = reinterpret_cast< const uint8_t* >( &setter )[i];
    return newId;
}


} // namespace animation
} // namespace el3D
