namespace el3D
{
namespace animation
{
template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetVelocity(
    const velocity_t v ) noexcept
{
    this->velocity = v;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetVelocityType(
    const AnimationVelocity v ) noexcept
{
    this->velocityType = v;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetLoop(
    const bool v ) noexcept
{
    this->hasLoop = v;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetSource(
    const T& v ) noexcept
{
    this->hasToPerformUpdate = true;
    this->source             = v;
    this->currentPosition    = v;
    this->currentFactor      = 0.0;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetDestination(
    const T& v ) noexcept
{
    this->hasToPerformUpdate = true;
    this->destination        = v;
    this->pathType           = PathType::Destination;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::SetDirection(
    const T& v ) noexcept
{
    this->hasToPerformUpdate = true;
    this->direction          = v;
    this->pathType           = PathType::Direction;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline elAnimate_SourceDestination< T, Interpolation >&
elAnimate_SourceDestination< T, Interpolation >::JumpTo( const T& v ) noexcept
{
    this->destination        = v;
    this->currentPosition    = v;
    this->source             = v;
    this->currentFactor      = 0.0;
    this->hasToPerformUpdate = false;
    return *this;
}

template < typename T, template < typename > class Interpolation >
inline T
elAnimate_SourceDestination< T, Interpolation >::GetSource() const noexcept
{
    return this->source;
}

template < typename T, template < typename > class Interpolation >
inline T
elAnimate_SourceDestination< T, Interpolation >::GetDestination() const noexcept
{
    return this->destination;
}

template < typename T, template < typename > class Interpolation >
inline T
elAnimate_SourceDestination< T, Interpolation >::GetDirection() const noexcept
{
    return this->direction;
}

template < typename T, template < typename > class Interpolation >
inline bool
elAnimate_SourceDestination< T, Interpolation >::HasDestination() const noexcept
{
    return this->hasDestination;
}

template < typename T, template < typename > class Interpolation >
inline bool
elAnimate_SourceDestination< T, Interpolation >::HasLoop() const noexcept
{
    return this->hasLoop;
}

template < typename T, template < typename > class Interpolation >
inline bool
elAnimate_SourceDestination< T, Interpolation >::HasDirection() const noexcept
{
    return this->hasDirection;
}

template < typename T, template < typename > class Interpolation >
inline T
elAnimate_SourceDestination< T, Interpolation >::GetCurrentPosition()
    const noexcept
{
    return this->currentPosition;
}

template < typename T, template < typename > class Interpolation >
inline velocity_t
elAnimate_SourceDestination< T, Interpolation >::GetVelocity() const noexcept
{
    return this->velocity;
}

template < typename T, template < typename > class Interpolation >
inline void
elAnimate_SourceDestination< T, Interpolation >::PerformUpdate() noexcept
{
    switch ( this->pathType )
    {
        case PathType::Destination:
        {
            velocity_t adjustedVelocity =
                ( this->velocityType == AnimationVelocity::Relative )
                    ? this->velocity
                    : this->velocity /
                          static_cast< velocity_t >( math::Distance(
                              this->destination, this->source ) );

            this->currentFactor += std::clamp(
                static_cast< interpolation::factor_t >(
                    ( this->currentTime - *this->previousUpdateTime )
                        .GetSeconds() ) *
                    adjustedVelocity,
                static_cast< interpolation::factor_t >( 0.0 ),
                static_cast< interpolation::factor_t >( 1.0 ) );

            if ( this->currentFactor >=
                 static_cast< interpolation::factor_t >( 1.0 ) -
                     interpolation::ERROR_EPSILON )
            {
                if ( this->hasLoop )
                {
                    this->currentFactor =
                        static_cast< interpolation::factor_t >( 1.0 ) -
                        this->currentFactor;

                    std::swap( this->source, this->destination );
                    this->currentPosition = this->Interpolate(
                        this->currentFactor, this->source, this->destination );
                }
                else
                {
                    this->hasToPerformUpdate = false;
                    this->currentPosition    = this->destination;
                }
            }
            else
            {
                this->currentPosition = this->Interpolate(
                    this->currentFactor, this->source, this->destination );
            }
            break;
        }
        case PathType::Direction:
        {
            this->currentPosition +=
                this->direction *
                static_cast< velocity_t >(
                    ( this->currentTime - *this->previousUpdateTime )
                        .GetSeconds() *
                    this->velocity );
            break;
        }
        default:
            break;
    }
}

template < typename T, template < typename > class Interpolation >
inline void
elAnimate_SourceDestination< T, Interpolation >::Set() noexcept
{
    if ( this->setCallback ) this->setCallback( &this->currentPosition );
}

template < typename T, template < typename > class Interpolation >
inline void
elAnimate_SourceDestination< T, Interpolation >::Reset() noexcept
{
    elAnimate_base::Reset();
    this->currentFactor      = 0.0;
    this->hasToPerformUpdate = false;
    this->pathType           = PathType::Undefined;
}
} // namespace animation
} // namespace el3D
