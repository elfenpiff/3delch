namespace el3D
{
namespace animation
{
namespace interpolation
{
template < typename T >
inline T
Linear< T >::Interpolate( const factor_t factor, const T& source,
                          const T& destination ) noexcept
{
    return source * ( static_cast< factor_t >( 1.0 ) - factor ) +
           destination * factor;
}

template < typename T >
inline T
Hermite< T >::Interpolate( const factor_t factor, const T& source,
                           const T& destination ) noexcept
{
    factor_t hermiteFactor = factor * factor *
                             ( static_cast< factor_t >( 3.0 ) -
                               factor * static_cast< factor_t >( 2.0 ) );

    return source * ( static_cast< factor_t >( 1.0 ) - hermiteFactor ) +
           destination * hermiteFactor;
}
} // namespace interpolation
} // namespace animation
} // namespace el3D
