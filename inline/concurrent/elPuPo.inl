namespace el3D
{
namespace concurrent
{

template < typename Container >
inline elPuPo< Container >::~elPuPo()
{
    this->Terminate();
}


template < typename Container >
inline void
elPuPo< Container >::Terminate() noexcept
{
    {
        // we need to acquire the base guard since we are modifying the
        // condition variable predicate
        auto baseGuard    = this->base.GetLockGuard();
        this->doTerminate = true;
    }

    this->base.NotifyAll();
}

template < typename Container >
inline void
elPuPo< Container >::Push( const typename Container::ValueType& t )
{
    this->base->Push( t );
    this->base.NotifyOne();
}

template < typename Container >
template < typename Allocator >
inline void
elPuPo< Container >::MultiPush(
    const std::vector< typename Container::ValueType, Allocator >& t )
{
    this->base->MultiPush( t );
    this->base.NotifyAll();
}

template < typename Container >
inline std::optional< typename Container::ValueType >
elPuPo< Container >::TryPop()
{
    return this->base->Pop();
}

template < typename Container >
template < typename OutputContainer >
inline std::optional< OutputContainer >
elPuPo< Container >::TryMultiPop( const size_t n )
{
    return this->base->template MultiPop< OutputContainer >( n );
}

template < typename Container >
inline std::optional< typename Container::ValueType >
elPuPo< Container >::BlockingPop()
{
    size_t localClearID = 0;
    {
        auto baseGuard = this->base.GetLockGuard();
        localClearID   = this->clearID;
    }

    this->base.Wait( [&] {
        return !this->base.UnsafeAccess().IsEmpty() || this->doTerminate ||
               localClearID != this->clearID;
    } );
    if ( this->doTerminate ) return std::nullopt;

    return this->TryPop();
}

template < typename Container >
template < typename OutputContainer >
inline std::optional< OutputContainer >
elPuPo< Container >::BlockingMultiPop( const size_t n )
{
    size_t localClearID = 0;
    {
        auto baseGuard = this->base.GetLockGuard();
        localClearID   = this->clearID;
    }

    this->base.Wait( [&] {
        return ( n == 0 && !this->base.UnsafeAccess().IsEmpty() ) ||
               ( n != 0 && this->base.UnsafeAccess().Size() >= n ) ||
               this->doTerminate || localClearID != this->clearID;
    } );
    if ( this->doTerminate ) return std::nullopt;

    return this->template TryMultiPop< OutputContainer >( n );
}

template < typename Container >
inline std::optional< typename Container::ValueType >
elPuPo< Container >::TimedPop( const units::Time& timeout )
{
    size_t localClearID = 0;
    {
        auto baseGuard = this->base.GetLockGuard();
        localClearID   = this->clearID;
    }

    this->base.WaitFor( timeout, [&] {
        return !this->base.UnsafeAccess().IsEmpty() || this->doTerminate ||
               localClearID != this->clearID;
    } );
    if ( this->doTerminate ) return std::nullopt;

    return this->TryPop();
}

template < typename Container >
template < typename OutputContainer >
inline std::optional< OutputContainer >
elPuPo< Container >::TimedMultiPop( const units::Time& timeout, const size_t n )
{
    size_t localClearID = 0;
    {
        auto baseGuard = this->base.GetLockGuard();
        localClearID   = this->clearID;
    }

    if ( this->base.WaitFor( timeout, [&] {
             return ( n == 0 && !this->base.UnsafeAccess().IsEmpty() ) ||
                    ( n != 0 && this->base.UnsafeAccess().Size() >= n ) ||
                    this->doTerminate || localClearID != this->clearID;
         } ) == false )
    {
        return std::nullopt;
    }
    if ( this->doTerminate ) return std::nullopt;

    return this->template TryMultiPop< OutputContainer >( n );
}

template < typename Container >
inline bool
elPuPo< Container >::IsEmpty() const noexcept
{
    return this->base->IsEmpty();
}

template < typename Container >
inline size_t
elPuPo< Container >::Size() const noexcept
{
    return this->base->Size();
}

template < typename Container >
inline void
elPuPo< Container >::Clear() noexcept
{
    this->base->Clear();
    {
        auto baseGuard = this->base.GetLockGuard();
        this->clearID++;
    }

    this->base.NotifyAll();
}
} // namespace concurrent
} // namespace el3D
