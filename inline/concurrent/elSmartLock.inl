namespace el3D
{
namespace concurrent
{
template < typename T >
inline elSmartLock< T >::Guard::Guard( T* const          object,
                                       std::mutex* const mtx ) noexcept
    : object( object ), mtx( mtx )
{
    this->mtx->lock();
}

template < typename T >
inline elSmartLock< T >::Guard::~Guard() noexcept
{
    if ( this->mtx != nullptr ) this->mtx->unlock();
}

template < typename T >
inline elSmartLock< T >::Guard::Guard( Guard&& rhs )
    : object( rhs.object ), mtx( rhs.mtx )
{
    rhs.object = nullptr;
    rhs.mtx    = nullptr;
}

template < typename T >
inline typename elSmartLock< T >::Guard&
elSmartLock< T >::Guard::operator=( Guard&& rhs )
{
    if ( this != &rhs )
    {
        if ( this->mtx != nullptr ) this->mtx->unlock();
        this->object = rhs.object;
        this->mtx    = rhs.mtx;

        rhs.mtx    = nullptr;
        rhs.object = nullptr;
    }

    return *this;
}

template < typename T >
inline T*
elSmartLock< T >::Guard::operator->() noexcept
{
    return object;
}

template < typename T >
inline const T*
elSmartLock< T >::Guard::operator->() const noexcept
{
    return object;
}

template < typename T >
inline T&
elSmartLock< T >::Guard::operator*() noexcept
{
    return *object;
}

template < typename T >
inline const T&
elSmartLock< T >::Guard::operator*() const noexcept
{
    return *object;
}

template < typename T >
template < typename... Targs >
inline elSmartLock< T >
elSmartLock< T >::Create( Targs&&... args ) noexcept
{
    return elSmartLock(
        std::make_shared< T >( std::forward< Targs >( args )... ) );
}

template < typename T >
inline elSmartLock< T >::elSmartLock() noexcept
    : elSmartLock( std::make_shared< T >() )
{
}

template < typename T >
inline elSmartLock< T >::elSmartLock( const std::shared_ptr< T >& object )
    : object( object )
{
}

template < typename T >
inline elSmartLock< T >::elSmartLock( const elSmartLock& rhs )
{
    *this = rhs;
}

template < typename T >
inline elSmartLock< T >::elSmartLock( elSmartLock&& rhs ) noexcept
{
    *this = std::move( rhs );
}

template < typename T >
inline elSmartLock< T >&
elSmartLock< T >::operator=( const elSmartLock& rhs )
{
    if ( this != &rhs )
    {
        std::scoped_lock lock( this->mtx, rhs.mtx );
        if ( rhs.object )
            this->object = std::make_shared< T >( *rhs.object );
        else
            this->object.reset();
    }

    return *this;
}

template < typename T >
inline elSmartLock< T >&
elSmartLock< T >::operator=( elSmartLock&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        std::scoped_lock lock( this->mtx, rhs.mtx );
        this->object = std::move( rhs.object );
        rhs.object.reset();
    }
    return *this;
}

template < typename T >
inline typename elSmartLock< T >::Guard
elSmartLock< T >::operator->() noexcept
{
    return Guard( object.get(), &mtx );
}

template < typename T >
inline const typename elSmartLock< T >::Guard
elSmartLock< T >::operator->() const noexcept
{
    return Guard( object.get(), &mtx );
}

template < typename T >
inline typename elSmartLock< T >::Guard
elSmartLock< T >::GetLockGuard() noexcept
{
    return Guard( object.get(), &mtx );
}


template < typename T >
inline const typename elSmartLock< T >::Guard
elSmartLock< T >::GetLockGuard() const noexcept
{
    return Guard( object.get(), &mtx );
}

template < typename T >
inline T&
elSmartLock< T >::UnsafeAccess() noexcept
{
    return *this->object;
}

template < typename T >
inline const T&
elSmartLock< T >::UnsafeAccess() const noexcept
{
    return *this->object;
}

template < typename T >
inline T
elSmartLock< T >::GetCopy() const noexcept
{
    std::lock_guard< std::mutex > lock( this->mtx );
    T                             newCopy( *this->object );
    return newCopy;
}

template < typename T >
template < typename Predicate >
inline void
elSmartLock< T >::Wait( const Predicate& p ) noexcept
{
    std::unique_lock< std::mutex > lock( this->mtx );
    this->condition.wait( lock, p );
}

template < typename T >
template < typename Predicate >
inline bool
elSmartLock< T >::WaitFor( const units::Time& timeout,
                           const Predicate&   p ) noexcept
{
    std::unique_lock< std::mutex > lock( this->mtx );
    return this->condition.wait_for(
        lock, std::chrono::duration< double >( timeout ), p );
}

template < typename T >
inline void
elSmartLock< T >::NotifyOne() noexcept
{
    this->condition.notify_one();
}

template < typename T >
inline void
elSmartLock< T >::NotifyAll() noexcept
{
    this->condition.notify_all();
}

} // namespace concurrent
} // namespace el3D
