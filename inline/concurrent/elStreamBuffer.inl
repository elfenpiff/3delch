namespace el3D
{
namespace concurrent
{
template < typename T >
inline elStreamBuffer< T >::~elStreamBuffer()
{
    delete this->buffer.load();
}

template < typename T >
elStreamBuffer< T >::elStreamBuffer( const elStreamBuffer& rhs )
{
    *this = rhs;
}

template < typename T >
elStreamBuffer< T >::elStreamBuffer( elStreamBuffer&& rhs )
    : buffer( rhs.buffer.load() )
{
    rhs.buffer.store( nullptr );
}

template < typename T >
elStreamBuffer< T >&
elStreamBuffer< T >::operator=( const elStreamBuffer& rhs )
{
    if ( this != &rhs )
    {
        if ( rhs.buffer.load() == nullptr )
        {
            delete this->buffer.exchange( nullptr );
        }
        else if ( rhs.buffer.load() != nullptr &&
                  this->buffer.load() != nullptr )
        {
            *this->buffer.load() = *rhs.buffer.load();
        }
        else if ( rhs.buffer.load() != nullptr &&
                  this->buffer.load() == nullptr )
        {
            this->buffer.store( new T( *rhs.buffer.load() ) );
        }
    }

    return *this;
}

template < typename T >
elStreamBuffer< T >&
elStreamBuffer< T >::operator=( elStreamBuffer&& rhs )
{
    if ( this != &rhs )
    {
        delete this->buffer.exchange( rhs.buffer.exchange( nullptr ) );
    }

    return *this;
}

template < typename T >
inline void
elStreamBuffer< T >::Push( const T& t ) noexcept
{
    T* out = this->buffer.exchange( new T( t ) );
    if ( out != nullptr ) delete out;
}

template < typename T >
inline std::optional< T >
elStreamBuffer< T >::Pop() noexcept
{
    T* out = this->buffer.exchange( nullptr );
    if ( out == nullptr ) return std::nullopt;

    std::optional< T > retVal = std::make_optional( *out );
    delete out;

    return retVal;
}
} // namespace concurrent
} // namespace el3D
