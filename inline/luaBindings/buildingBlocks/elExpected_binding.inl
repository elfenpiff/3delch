namespace el3D
{
namespace luaBindings
{
namespace bb
{
template < typename ValueType, typename ErrorType >
inline void
AddLuaResultBinding( const std::string& luaClassName,
                     const std::string& nameSpace )
{
    using result_t = ::el3D::bb::elExpected< ValueType, ErrorType >;

    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddPrivateConstructor< result_t >();
    newClass.AddMethod< result_t >( "HasError", &result_t::HasError );
    newClass.AddMethod< result_t >( "GetErrorPointer",
                                    &result_t::GetErrorPointer );
    newClass.AddMethod< result_t >( "GetValuePointer",
                                    &result_t::GetValuePointer );
    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}

template < typename ErrorType >
inline void
AddLuaResultBinding( const std::string& luaClassName,
                     const std::string& nameSpace )
{
    using result_t = ::el3D::bb::elExpected< ErrorType >;

    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddPrivateConstructor< result_t >();
    newClass.AddMethod< result_t >( "HasError", &result_t::HasError );
    newClass.AddMethod< result_t >( "GetErrorPointer",
                                    &result_t::GetErrorPointer );
    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace bb
} // namespace luaBindings
} // namespace el3D
