namespace el3D
{
namespace luaBindings
{
namespace utils
{
template < typename T >
inline void
AddLuaRawPointerBinding( const std::string& luaClassName,
                         const std::string& nameSpace )
{
    using rawPointer_t = ::el3D::utils::elRawPointer< T >;

    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< rawPointer_t >();
    newClass.AddMethod< rawPointer_t >( "Get", &rawPointer_t::__Get__ );
    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace utils
} // namespace luaBindings
} // namespace el3D
