namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
template < typename ReturnValue, typename... Arguments >
class lua_function : public std::function< ReturnValue( Arguments... ) >
{
  public:
    using base_t = std::function< ReturnValue( Arguments... ) >;

    bool
    lua_is_valid()
    {
        return static_cast< bool >( *this );
    }

    ReturnValue
    lua_invoke( const Arguments... args )
    {
        return base_t::operator()( args... );
    }

    void
    lua_invoke_void( const Arguments... args )
    {
        base_t::operator()( args... );
    }
};
} // namespace internal

template < typename ReturnValue, typename... Arguments >
inline void
AddLuaFunctionBinding( const std::string& luaClassName,
                       const std::string& nameSpace )
{
    using stl_function = std::function< ReturnValue( Arguments... ) >;
    using lua_function = internal::lua_function< ReturnValue, Arguments... >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_function >();

    newClass.AddMethod< lua_function >( "is_valid",
                                        &lua_function::lua_is_valid );

    if ( std::is_same_v< ReturnValue, void > )
        newClass.AddMethod< lua_function >( "invoke",
                                            &lua_function::lua_invoke_void );
    else
        newClass.AddMethod< lua_function >( "invoke",
                                            &lua_function::lua_invoke );

    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace stl
} // namespace luaBindings
} // namespace el3D
