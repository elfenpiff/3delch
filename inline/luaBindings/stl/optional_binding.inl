namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
// we have to introduce this class since in C++ there seems to be no way on
// how to overcome the const overloaded function problem
template < typename T >
class lua_optional : public std::optional< T >
{
  public:
    void
    lua_emplace( const T& newValue )
    {
        this->emplace( newValue );
    }

    T
    lua_value()
    {
        return this->value();
    }

    T
    lua_value_or( const T& defaultValue )
    {
        return this->value_or( defaultValue );
    }

    void
    lua_reset()
    {
        this->reset();
    }
};
} // namespace internal

template < typename T >
inline void
AddLuaOptionalBinding( const std::string& luaClassName,
                       const std::string& nameSpace )
{
    using stl_optional = std::optional< T >;
    using lua_optional = internal::lua_optional< T >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_optional >();

    newClass.AddMethod< lua_optional >( "emplace", &lua_optional::lua_emplace );
    newClass.AddMethod< lua_optional >( "value", &lua_optional::lua_value );
    newClass.AddMethod< lua_optional >( "value_or",
                                        &lua_optional::lua_value_or );
    newClass.AddMethod< lua_optional >( "reset", &lua_optional::lua_reset );

    newClass.AddMethod< stl_optional >( "has_value", &stl_optional::has_value );

    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}

} // namespace stl
} // namespace luaBindings
} // namespace el3D
