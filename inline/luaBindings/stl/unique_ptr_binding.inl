namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
// we have to introduce this class since in C++ there seems to be no way on
// how to overcome the const overloaded function problem
template < typename T >
class lua_unique_ptr : public std::unique_ptr< T >
{
  public:
    void
    lua_reset()
    {
        return this->reset();
    }

    bool
    lua_has_value()
    {
        return static_cast< bool >( *this );
    }
};

} // namespace internal
template < typename T >
inline void
AddLuaUniquePtrBinding( const std::string& luaClassName,
                        const std::string& nameSpace )
{
    using stl_unique_ptr = std::unique_ptr< T >;
    using lua_unique_ptr = internal::lua_unique_ptr< T >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_unique_ptr >();

    newClass.AddMethod< stl_unique_ptr >( "release", &stl_unique_ptr::release );
    newClass.AddMethod< lua_unique_ptr >( "reset", &lua_unique_ptr::lua_reset );
    newClass.AddMethod< stl_unique_ptr >( "get", &stl_unique_ptr::get );
    newClass.AddMethod< lua_unique_ptr >( "has_value",
                                          &lua_unique_ptr::lua_has_value );

    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace stl
} // namespace luaBindings
} // namespace el3D
