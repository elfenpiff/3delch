namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
// we have to introduce this class since in C++ there seems to be no way on
// how to overcome the const overloaded function problem
template < typename T >
class lua_shared_future : public std::shared_future< T >
{
  public:
    T
    lua_get()
    {
        return this->get();
    }

    bool
    lua_wait_for( const units::Time& t )
    {
        return this->wait_for( static_cast< std::chrono::duration< double > >(
                   t ) ) == std::future_status::ready;
    }

    bool
    lua_value_available()
    {
        return this->wait_for( std::chrono::seconds( 0 ) ) ==
               std::future_status::ready;
    }
};
} // namespace internal

template < typename T >
inline void
AddLuaSharedFutureBinding( const std::string& luaClassName,
                           const std::string& nameSpace )
{
    using stl_shared_future = std::shared_future< T >;
    using lua_shared_future = internal::lua_shared_future< T >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_shared_future >();

    newClass.AddMethod< lua_shared_future >( "get",
                                             &lua_shared_future::lua_get );
    newClass.AddMethod< stl_shared_future >( "valid",
                                             &stl_shared_future::valid );
    newClass.AddMethod< stl_shared_future >( "wait", &stl_shared_future::wait );
    newClass.AddMethod< lua_shared_future >( "wait_for",
                                             &lua_shared_future::lua_wait_for );
    newClass.AddMethod< lua_shared_future >(
        "value_available", &lua_shared_future::lua_value_available );

    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}

} // namespace stl
} // namespace luaBindings
} // namespace el3D
