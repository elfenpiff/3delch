namespace el3D
{
namespace luaBindings
{
namespace stl
{
namespace internal
{
// we have to introduce this class since in C++ there seems to be no way on
// how to overcome the const overloaded function problem
template < typename T >
class lua_shared_ptr : public std::shared_ptr< T >
{
  public:
    void
    lua_reset()
    {
        return this->reset();
    }

    bool
    lua_has_value()
    {
        return static_cast< bool >( *this );
    }
};

} // namespace internal
template < typename T >
inline void
AddLuaSharedPtrBinding( const std::string& luaClassName,
                        const std::string& nameSpace )
{
    using stl_shared_ptr = std::shared_ptr< T >;
    using lua_shared_ptr = internal::lua_shared_ptr< T >;
    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddConstructor< stl_shared_ptr >();

    newClass.AddMethod< lua_shared_ptr >( "reset", &lua_shared_ptr::lua_reset );
    newClass.AddMethod< stl_shared_ptr >( "get", &stl_shared_ptr::get );
    newClass.AddMethod< stl_shared_ptr >( "unique", &stl_shared_ptr::unique );
    newClass.AddMethod< stl_shared_ptr >( "use_count",
                                          &stl_shared_ptr::use_count );
    newClass.AddMethod< lua_shared_ptr >( "has_value",
                                          &lua_shared_ptr::lua_has_value );

    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace stl
} // namespace luaBindings
} // namespace el3D
