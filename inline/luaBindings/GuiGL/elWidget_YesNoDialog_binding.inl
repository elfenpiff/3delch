namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_YesNoDialog, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_InfoMessage, ChildType >::Do( newClass );
    newClass.AddMethod< ChildType >( "SetYesNoCallback",
                                     &elWidget_YesNoDialog::SetYesNoCallback );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
