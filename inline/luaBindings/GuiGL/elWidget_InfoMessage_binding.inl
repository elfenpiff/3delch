namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_InfoMessage, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "InsertLine",
                                     &elWidget_InfoMessage::InsertLine );
    newClass.AddMethod< ChildType >( "ModifyLine",
                                     &elWidget_InfoMessage::ModifyLine );
    newClass.AddMethod< ChildType >( "RemoveLine",
                                     &elWidget_InfoMessage::RemoveLine );
    newClass.AddMethod< ChildType >(
        "SetCallbackOnClose", &elWidget_InfoMessage::SetCallbackOnClose );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
