namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
namespace handler
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaCallbackInterface( ::el3D::lua::elLuaScript::class_t& newClass )
{
    using namespace ::el3D::GuiGL::handler;

    newClass.AddMethod< ChildType >( "SetOnCloseCallback",
                                     &callback::SetOnCloseCallback );
}
#endif
} // namespace handler
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
