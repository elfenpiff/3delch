namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
namespace handler
{
#ifdef ADD_LUA_BINDINGS
template < typename BaseClass, typename ChildType >
inline void
AddLuaWidgetFactoryCreateInterface(
    ::el3D::lua::elLuaScript::class_t& newClass )
{
    using namespace ::el3D::GuiGL;
    newClass.AddMethod< ChildType >(
        "Create_Base", &BaseClass::template Create< elWidget_base > );
    newClass.AddMethod< ChildType >(
        "Create_Button", &BaseClass::template Create< elWidget_Button > );
    newClass.AddMethod< ChildType >(
        "Create_Checkbutton",
        &BaseClass::template Create< elWidget_Checkbutton > );
    newClass.AddMethod< ChildType >(
        "Create_Dropbutton",
        &BaseClass::template Create< elWidget_Dropbutton > );
    newClass.AddMethod< ChildType >(
        "Create_Entry", &BaseClass::template Create< elWidget_Entry > );
    newClass.AddMethod< ChildType >(
        "Create_FileBrowser",
        &BaseClass::template Create< elWidget_FileBrowser > );
    newClass.AddMethod< ChildType >(
        "Create_Image", &BaseClass::template Create< elWidget_Image > );
    newClass.AddMethod< ChildType >(
        "Create_InfoMessage",
        &BaseClass::template Create< elWidget_InfoMessage > );
    newClass.AddMethod< ChildType >(
        "Create_Label", &BaseClass::template Create< elWidget_Label > );
    newClass.AddMethod< ChildType >(
        "Create_Listbox", &BaseClass::template Create< elWidget_Listbox > );
    newClass.AddMethod< ChildType >(
        "Create_Menu", &BaseClass::template Create< elWidget_Menu > );
    newClass.AddMethod< ChildType >(
        "Create_Menubar", &BaseClass::template Create< elWidget_Menubar > );
    newClass.AddMethod< ChildType >(
        "Create_MouseCursor",
        &BaseClass::template Create< elWidget_MouseCursor > );
    newClass.AddMethod< ChildType >(
        "Create_PopupMenu", &BaseClass::template Create< elWidget_PopupMenu > );
    newClass.AddMethod< ChildType >(
        "Create_Progress", &BaseClass::template Create< elWidget_Progress > );
    newClass.AddMethod< ChildType >(
        "Create_ShaderAnimation",
        &BaseClass::template Create< elWidget_ShaderAnimation > );
    newClass.AddMethod< ChildType >(
        "Create_Slider", &BaseClass::template Create< elWidget_Slider > );
    newClass.AddMethod< ChildType >(
        "Create_TabbedWindow",
        &BaseClass::template Create< elWidget_TabbedWindow > );
    newClass.AddMethod< ChildType >(
        "Create_Table", &BaseClass::template Create< elWidget_Table > );
    newClass.AddMethod< ChildType >(
        "Create_Textbox", &BaseClass::template Create< elWidget_Textbox > );
    newClass.AddMethod< ChildType >(
        "Create_TextCursor",
        &BaseClass::template Create< elWidget_TextCursor > );
    newClass.AddMethod< ChildType >(
        "Create_Titlebar", &BaseClass::template Create< elWidget_Titlebar > );
    newClass.AddMethod< ChildType >(
        "Create_Tree", &BaseClass::template Create< elWidget_Tree > );
    newClass.AddMethod< ChildType >(
        "Create_Window", &BaseClass::template Create< elWidget_Window > );
    newClass.AddMethod< ChildType >(
        "Create_YesNoDialog",
        &BaseClass::template Create< elWidget_YesNoDialog > );
    newClass.AddMethod< ChildType >(
        "Create_Graph", &BaseClass::template Create< elWidget_Graph > );
}

template < typename ChildType >
inline void
AddLuaWidgetFactoryInterface( ::el3D::lua::elLuaScript::class_t& newClass )
{
    using namespace ::el3D::GuiGL::handler;
    AddLuaWidgetFactoryCreateInterface< widgetFactory, ChildType >( newClass );

    newClass.AddMethod< ChildType >( "GetTheme", &widgetFactory::GetTheme );
    newClass.AddMethod< ChildType >( "RemoveThis", &widgetFactory::RemoveThis );
    newClass.AddMethod< ChildType >( "GetTitlebar",
                                     &widgetFactory::GetTitlebar );
    newClass.AddMethod< ChildType >( "MoveWidgetToTop",
                                     &widgetFactory::MoveWidgetToTop );
}
#endif


} // namespace handler
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
