namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
namespace handler
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaSettingsInterface( ::el3D::lua::elLuaScript::class_t& newClass )
{
    using namespace ::el3D::GuiGL::handler;

    newClass.AddMethod< ChildType >( "MoveTo", &settings::MoveTo );
    newClass.AddMethod< ChildType >( "ResizeTo", &settings::ResizeTo );
    newClass.AddMethod< ChildType >( "GetAbsolutPosition",
                                     &settings::GetAbsolutPosition );
    newClass.AddMethod< ChildType >( "SetHasTitlebar",
                                     &settings::SetHasTitlebar );
    newClass.AddMethod< ChildType >( "GetHasTitlebar",
                                     &settings::GetHasTitlebar );
    newClass.AddMethod< ChildType >( "SetHasMultiSelection",
                                     &settings::SetHasMultiSelection );
    newClass.AddMethod< ChildType >( "GetHasMultiSelection",
                                     &settings::GetHasMultiSelection );
    newClass.AddMethod< ChildType >( "SetIsUnselectable",
                                     &settings::SetIsUnselectable );
    newClass.AddMethod< ChildType >( "GetIsUnselectable",
                                     &settings::GetIsUnselectable );
    newClass.AddMethod< ChildType >( "SetHasFixedPosition",
                                     &settings::SetHasFixedPosition );
    newClass.AddMethod< ChildType >( "SetStickToParentSize",
                                     &settings::SetStickToParentSize );
    newClass.AddMethod< ChildType >( "SetBaseColorForState",
                                     &settings::SetBaseColorForState );
    newClass.AddMethod< ChildType >( "SetAccentColorForState",
                                     &settings::SetAccentColorForState );
    newClass.AddMethod< ChildType >( "SetBaseGlowColorForState",
                                     &settings::SetBaseGlowColorForState );
    newClass.AddMethod< ChildType >( "SetAccentGlowColorForState",
                                     &settings::SetAccentGlowColorForState );
    newClass.AddMethod< ChildType >( "SetIsMovable", &settings::SetIsMovable );
    newClass.AddMethod< ChildType >( "SetIsResizable",
                                     &settings::SetIsResizable );
    newClass.AddMethod< ChildType >( "SetDoRenderWidget",
                                     &settings::SetDoRenderWidget );
    newClass.AddMethod< ChildType >( "DoRenderWidget",
                                     &settings::DoRenderWidget );
    newClass.AddMethod< ChildType >( "SetDisableEventHandling",
                                     &settings::SetDisableEventHandling );
    newClass.AddMethod< ChildType >( "GetHasDisabledEventHandling",
                                     &settings::GetHasDisabledEventHandling );
    newClass.AddMethod< ChildType >( "SetHasVerticalAlignment",
                                     &settings::SetHasVerticalAlignment );
    newClass.AddMethod< ChildType >( "GetHasVerticalAlignment",
                                     &settings::GetHasVerticalAlignment );
    newClass.AddMethod< ChildType >( "GetIsMovable", &settings::GetIsMovable );
    newClass.AddMethod< ChildType >( "GetIsResizable",
                                     &settings::GetIsResizable );
    newClass.AddMethod< ChildType >( "GetSize", &settings::GetSize );
    newClass.AddMethod< ChildType >( "GetBorderSize",
                                     &settings::GetBorderSize );
    newClass.AddMethod< ChildType >( "GetContentSize",
                                     &settings::GetContentSize );
    newClass.AddMethod< ChildType >( "GetPosition", &settings::GetPosition );
    newClass.AddMethod< ChildType >( "SetAlignedPositionWithWidget",
                                     &settings::SetAlignedPositionWithWidget );
    newClass.AddMethod< ChildType >( "SetButtonAlignment",
                                     &settings::SetButtonAlignment );
    newClass.AddMethod< ChildType >( "GetButtonAlignment",
                                     &settings::GetButtonAlignment );
    newClass.AddMethod< ChildType >( "GetWindowDimensions",
                                     &settings::GetWindowDimensions );
    newClass.AddMethod< ChildType >( "GetClassName", &settings::GetClassName );
    newClass.AddMethod< ChildType >( "GetAdjustSizeToFitChildren",
                                     &settings::GetAdjustSizeToFitChildren );
    newClass.AddMethod< ChildType >( "SetAdjustSizeToFitChildren",
                                     &settings::SetAdjustSizeToFitChildren );
    newClass.AddMethod< ChildType >( "SetPositionPointOfOrigin",
                                     &settings::SetPositionPointOfOrigin );
    newClass.AddMethod< ChildType >(
        "SetPositionPointOfOriginForChilds",
        &settings::SetPositionPointOfOriginForChilds );
    newClass.AddMethod< ChildType >(
        "GetPositionPointOfOriginForChilds",
        &settings::GetPositionPointOfOriginForChilds );
    newClass.AddMethod< ChildType >( "SetIsAlwaysOnTop",
                                     &settings::SetIsAlwaysOnTop );
    newClass.AddMethod< ChildType >( "SetIsAlwaysOnBottom",
                                     &settings::SetIsAlwaysOnBottom );
    newClass.AddMethod< ChildType >( "SetHasVerticalScrolling",
                                     &settings::SetHasVerticalScrolling );
    newClass.AddMethod< ChildType >( "SetHasHorizontalScrolling",
                                     &settings::SetHasHorizontalScrolling );
    newClass.AddMethod< ChildType >( "SetBorderSize",
                                     &settings::SetBorderSize );
    newClass.AddMethod< ChildType >( "SetSize", &settings::SetSize );
    newClass.AddMethod< ChildType >( "SetPosition", &settings::SetPosition );
    newClass.AddMethod< ChildType >( "SetDoInvertTextureCoordinates",
                                     &settings::SetDoInvertTextureCoordinates );
    newClass.AddMethod< ChildType >( "GetDoInvertTextureCoordinates",
                                     &settings::GetDoInvertTextureCoordinates );
}
#endif
} // namespace handler
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
