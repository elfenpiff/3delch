namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Tree, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_Window, ChildType >::Do( newClass );
    newClass.AddMethod< ChildType >( "AddEntries",
                                     &elWidget_Tree::Lua_AddEntries );
    newClass.AddMethod< ChildType >( "RemoveEntry",
                                     &elWidget_Tree::Lua_RemoveEntry );
    newClass.AddMethod< ChildType >( "GetEntry", &elWidget_Tree::Lua_GetEntry );
    newClass.AddMethod< ChildType >( "Clear", &elWidget_Tree::Clear );
    newClass.AddMethod< ChildType >( "SetClickOnEntryCallback",
                                     &elWidget_Tree::SetClickOnEntryCallback );
    newClass.AddMethod< ChildType >( "SetEntryOpenCallback",
                                     &elWidget_Tree::SetEntryOpenCallback );
    newClass.AddMethod< ChildType >( "SetEntryCloseCallback",
                                     &elWidget_Tree::SetEntryCloseCallback );
    newClass.AddMethod< ChildType >( "AllowDoubleEntries",
                                     &elWidget_Tree::AllowDoubleEntries );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
