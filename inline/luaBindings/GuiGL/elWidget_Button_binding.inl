namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Button, ChildType >::Do(
    lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetText", &elWidget_Button::SetText );
    newClass.AddMethod< ChildType >( "GetText", &elWidget_Button::GetText );
    newClass.AddMethod< ChildType >( "GetFontHeight",
                                     &elWidget_Button::GetFontHeight );
    newClass.AddMethod< ChildType >( "SetClickCallback",
                                     &elWidget_Button::SetClickCallback );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
