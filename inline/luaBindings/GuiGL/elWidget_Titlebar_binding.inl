namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Titlebar, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetTitleText",
                                     &elWidget_Titlebar::SetTitleText );
    newClass.AddMethod< ChildType >( "SetHasCloseButton",
                                     &elWidget_Titlebar::SetHasCloseButton );
    newClass.AddMethod< ChildType >( "GetCloseButton",
                                     &elWidget_Titlebar::GetCloseButton );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
