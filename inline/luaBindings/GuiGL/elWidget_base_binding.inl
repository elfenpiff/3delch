namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface< ::el3D::GuiGL::elWidget_base, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::handler::AddLuaWidgetFactoryInterface<
        ChildType >( newClass );
    ::el3D::luaBindings::GuiGL::handler::AddLuaSettingsInterface< ChildType >(
        newClass );
    ::el3D::luaBindings::GuiGL::handler::AddLuaCallbackInterface< ChildType >(
        newClass );

    newClass.AddMethod< ChildType >( "Reshape", &elWidget_base::Reshape );
    newClass.AddMethod< ChildType >( "Draw", &elWidget_base::Draw );
    newClass.AddMethod< ChildType >( "SetState", &elWidget_base::SetState );
    newClass.AddMethod< ChildType >( "GetType", &elWidget_base::GetType );
}

template < typename ChildType >
inline void
AddLuaWidgetBinding( const std::string &name )
{
    static_cast< void >( name );
    //::el3D::lua::elLuaScript::class_t newClass( name );
    // newClass.AddPrivateConstructor< ChildType >();
    // AddLuaWidgetInterface< ChildType, ChildType >::Do( newClass );
    //::el3D::lua::elLuaScript::RegisterLuaClass( newClass,
    //                                            ::el3D::GuiGL::NAMESPACE_NAME
    //                                            );
}

#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
