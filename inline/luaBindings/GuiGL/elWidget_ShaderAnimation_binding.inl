namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_ShaderAnimation, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );
    newClass.AddMethod< ChildType >( "SetUp",
                                     &elWidget_ShaderAnimation::SetUp );
    newClass.AddMethod< ChildType >(
        "GetShaderTexture", &elWidget_ShaderAnimation::GetShaderTexture );
    newClass.AddMethod< ChildType >(
        "SetTextureUniformBlockData",
        &elWidget_ShaderAnimation::SetTextureUniformBlockData );
    newClass.AddMethod< ChildType >(
        "SetDoInfinitLoop", &elWidget_ShaderAnimation::SetDoInfinitLoop );
    newClass.AddMethod< ChildType >( "Start",
                                     &elWidget_ShaderAnimation::Start );
    newClass.AddMethod< ChildType >( "ReverseStart",
                                     &elWidget_ShaderAnimation::ReverseStart );
    newClass.AddMethod< ChildType >( "Stop", &elWidget_ShaderAnimation::Stop );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
