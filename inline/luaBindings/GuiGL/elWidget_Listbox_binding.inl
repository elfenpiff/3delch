namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Listbox, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_Table, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "AddElement",
                                     &elWidget_Listbox::AddElement );
    newClass.AddMethod< ChildType >( "RemoveElements",
                                     &elWidget_Listbox::RemoveElements );
    newClass.AddMethod< ChildType >(
        "GetElementAtIndex",
        &elWidget_Listbox::GetElementAtIndex< elWidget_Button > );
    newClass.AddMethod< ChildType >( "Clear", &elWidget_Listbox::Clear );
    newClass.AddMethod< ChildType >( "GetSelectedWidgets",
                                     &elWidget_Listbox::GetSelectedWidgets );
    newClass.AddMethod< ChildType >(
        "SetElementSelectionCallback",
        &elWidget_Listbox::SetElementSelectionCallback );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
