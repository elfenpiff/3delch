namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Dropbutton, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    luaBindings::GuiGL::AddLuaWidgetInterface< elWidget_Button, ChildType >::Do(
        newClass );

    newClass.AddMethod< ChildType >( "AddElement",
                                     &elWidget_Dropbutton::AddElement );
    newClass.AddMethod< ChildType >( "GetSelection",
                                     &elWidget_Dropbutton::GetSelection );
    newClass.AddMethod< ChildType >( "SetMaxListHeight",
                                     &elWidget_Dropbutton::SetMaxListHeight );
    newClass.AddMethod< ChildType >(
        "SetCallbackOnSelection",
        &elWidget_Dropbutton::SetCallbackOnSelection );
    newClass.AddMethod< ChildType >( "RemoveElements",
                                     &elWidget_Dropbutton::RemoveElements );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
