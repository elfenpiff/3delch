namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface<::el3D::GuiGL::elWidget_Image, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );

    newClass.AddMethod< ChildType >( "SetImage", &elWidget_Image::SetImage );
    newClass.AddMethod< ChildType >( "SetImageFromTexture",
                                     &elWidget_Image::SetImageFromTexture );
    newClass.AddMethod< ChildType >( "SetImageFromByteStream",
                                     &elWidget_Image::SetImageFromByteStream );
    newClass.AddMethod< ChildType >( "SetImageFromBytePointer",
                                     &elWidget_Image::SetImageFromBytePointer );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
