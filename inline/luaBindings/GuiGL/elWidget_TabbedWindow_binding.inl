namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaWidgetInterface< ::el3D::GuiGL::elWidget_TabbedWindow, ChildType >::Do(
    ::el3D::lua::elLuaScript::class_t &newClass )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetInterface<
        elWidget_base, ChildType >::Do( newClass );
    // newClass.AddMethod< ChildType >(
    //    "CreateTabReturnID", &elWidget_TabbedWindow::CreateTabReturnID );
    // newClass.AddMethod< ChildType >( "GetTabButton",
    //                                 &elWidget_TabbedWindow::GetTabButton );
    // newClass.AddMethod< ChildType >( "GetTabWindow",
    //                                 &elWidget_TabbedWindow::GetTabWindow );
    // newClass.AddMethod< ChildType >( "RemoveTab",
    //                                 &elWidget_TabbedWindow::RemoveTab );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
