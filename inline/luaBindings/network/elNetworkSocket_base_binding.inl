namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
template < typename ChildType >
inline void
AddLuaNetworkSocket_baseInterface( lua::elLuaScript::class_t &newClass )
{
    newClass.AddMethod< ChildType >(
        "IsConnected", &::el3D::network::elNetworkSocket_Base::IsConnected );
}
#endif

} // namespace network
} // namespace luaBindings
} // namespace el3D
