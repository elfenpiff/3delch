namespace el3D
{
namespace luaBindings
{
namespace network
{
namespace messages
{
template < typename T, typename E >
inline void
AddLuaResponseBinding( const std::string& luaClassName,
                       const std::string& nameSpace )
{
    using type_t = ::network::messages::Response_t< T, E >;

    lua::elLuaScript::class_t newClass( luaClassName );
    newClass.AddPrivateConstructor< type_t >();
    newClass.AddMethod< type_t >( "GetRPCEnum", &type_t::GetRPCEnum );
    newClass.AddMethod< type_t >( "GetRPCData", &type_t::GetRPCData );
    newClass.AddMethod< type_t >( "GetTransmissionError",
                                  &type_t::GetTransmissionError );
    newClass.AddMethod< type_t >( "HasTransmissionError",
                                  &type_t::HasTransmissionError );
    lua::elLuaScript::RegisterLuaClass( newClass, nameSpace );
}
} // namespace messages
} // namespace network
} // namespace luaBindings
} // namespace el3D
