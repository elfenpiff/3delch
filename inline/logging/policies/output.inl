namespace el3D
{
namespace logging
{
namespace output
{
template < typename THREAD_POLICY >
void
ToTerminal< THREAD_POLICY >::Print( const std::string &msg ) noexcept
{
    this->Call( [=]() {
        fputs( ( msg + "\n" ).c_str(), stdout );
        fflush( stdout );
    } );
}

class Exception_FileOpen : public std::runtime_error
{
  public:
    explicit Exception_FileOpen( const std::string &msg )
        : std::runtime_error( msg )
    {
    }
};
template < typename THREAD_POLICY >
ToFile< THREAD_POLICY >::ToFile( const std::string &fileName )
    : fileName( fileName )
{
    this->file.open( this->fileName, std::ios::out | std::ios::app );
    if ( !this->file.is_open() )
        throw Exception_FileOpen( "could not open file \'" + fileName + "\'" );
}

template < typename THREAD_POLICY >
ToFile< THREAD_POLICY >::~ToFile()
{
    this->file.close();
}

template < typename THREAD_POLICY >
void
ToFile< THREAD_POLICY >::Print( const std::string &msg ) noexcept
{
    this->Call( [this, msg]() { this->file << msg << std::endl; } );
}
} // namespace output
} // namespace logging
} // namespace el3D
