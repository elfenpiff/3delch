namespace el3D
{
template < typename T >
std::string
logging::detail::toString( const T &msg ) noexcept
{
    std::stringstream ss;
    ss << msg;
    return ss.str();
}
} // namespace el3D
