namespace el3D
{
namespace world
{
template < typename T >
inline GuiGL::widget_t< T >
elGui::Create( const std::string& className ) noexcept
{
    return this->gui->GetRoot()->Create< T >( className );
}
} // namespace world
} // namespace el3D
