namespace el3D
{
namespace world
{
template < typename ShapeType, typename... Targs >
inline bb::product_ptr< elPhysics::Object >
elPhysics::AddCollisionShape( const physicalProperties_t&  physicalProperties,
                              const geometricProperties_t& geometry,
                              const setModelMatrix_t&      setModelMatrix,
                              const Targs&... args ) noexcept
{
    return this->factory.CreateProduct< Object >(
        *this, this->dynamicWorld,
        [=]() -> btCollisionShape* { return new ShapeType( args... ); },
        physicalProperties, geometry, setModelMatrix );
}

} // namespace world
} // namespace el3D
