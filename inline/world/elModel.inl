namespace el3D
{
namespace world
{
template < typename ShapeType, typename... ShapeTypeCTorArgs >
inline void
elModel::SetPhysicalProperties( const physicalProperties_t physicalProperties,
                                const ShapeTypeCTorArgs&... args ) noexcept
{
    this->physical = this->GetPhysics().AddCollisionShape< ShapeType >(
        physicalProperties,
        geometricProperties_t{ this->GetPosition(), this->GetScaling() },
        [this]( auto m ) { this->SetModelMatrix( m ); }, args... );
    this->physical->Reset();
}


} // namespace world
} // namespace el3D
