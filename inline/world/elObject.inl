namespace el3D
{
namespace world
{
template < OpenGL::RenderTarget Target, typename T, typename... Targs >
inline typename _3Delch::ObjectSettings< static_cast< int >( Target ), T >::Type
elObject::CreateCustomObject( Targs&&... args ) noexcept
{
    return this->settings.scene->CreateCustomObject< Target, T, Targs... >(
        std::forward< Targs >( args )... );
}

template < typename... Targs >
inline bb::elExpected< bb::product_ptr< HighGL::elSkybox >,
                       HighGL::elSkybox_Error >
elObject::CreateSkybox( Targs&&... args ) noexcept
{
    return this->settings.scene->CreateSkybox(
        std::forward< Targs >( args )... );
}
} // namespace world
} // namespace el3D

