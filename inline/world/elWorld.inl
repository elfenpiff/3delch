namespace el3D
{
namespace world
{
template < typename T, typename... Targs >
inline bb::product_ptr< T >
elWorld::Create( Targs&&... args ) noexcept
{
    return this->factory.CreateProduct< T >(
        objectSettings_t{ this, this->scene, this->uvo.Get(), &this->physics,
                          this->silhouette.uvo.Get(),
                          this->wireframe.uvo.Get() },
        std::forward< Targs >( args )... );
}

} // namespace world
} // namespace el3D
