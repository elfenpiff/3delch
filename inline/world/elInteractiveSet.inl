namespace el3D
{
namespace world
{
template < typename... Targs >
inline void
elInteractiveSet::SetupInteractiveMesh( interactiveMeshCallbacks_t callbacks,
                                        Targs&&... args ) noexcept
{
    this->interactiveMeshCallbacks = callbacks;
    this->interactiveMesh = this->GetWorld().Create< elInteractiveMesh >(
        std::forward< Targs >( args )... );
}
} // namespace world
} // namespace el3D
