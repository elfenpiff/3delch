namespace el3D
{
namespace _3Delch
{
template < OpenGL::RenderTarget Target, typename T, typename... Targs >
inline typename ObjectSettings< static_cast< int >( Target ), T >::Type
elScene::CreateCustomObject( Targs&&... args ) noexcept
{
    return this->GetRenderer< Target >().renderer->template CreateObject< T >(
        std::forward< Targs >( args )... );
}

template < OpenGL::RenderTarget Target >
inline typename std::tuple_element< static_cast< int >( Target ),
                                    elScene::renderTypeList_t >::type&
elScene::GetRenderer() noexcept
{
    return std::get< static_cast< int >( Target ) >( this->renderer );
}

template < typename... Targs >
inline bb::elExpected< bb::product_ptr< HighGL::elSkybox >,
                       HighGL::elSkybox_Error >
elScene::CreateSkybox( Targs&&... args ) noexcept
{
    if ( !this->factory.GetProducts< HighGL::elSkybox >().empty() )
    {
        LOG_ERROR( 0 ) << "skybox already created for scene "
                       << this->sceneName;
        return bb::Error( HighGL::elSkybox_Error::UndefinedError );
    }

    auto skyboxObject =
        this->pipeline->GetForwardRenderer()
            ->CreateObject< OpenGL::elGeometricObject_VertexObject >(
                nullptr, RenderPipeline::RenderOrder::BackgroundObject,
                OpenGL::DrawMode::Triangles,
                HighGL::elObjectGeometryBuilder::CreateCube()
                    .GetInnerObjectGeometry() );

    auto shaderId = &skyboxObject.Extension().geometricShader;

    auto result = HighGL::elSkybox::Create( std::move( skyboxObject ), shaderId,
                                            std::forward< Targs >( args )... );
    if ( result.HasError() ) return bb::Error( result.GetError() );

    auto skybox = bb::elExpected< bb::product_ptr< HighGL::elSkybox >,
                                  HighGL::elSkybox_Error >::
        CreateValue(
            this->factory.CreateProductWithOnRemoveCallback< HighGL::elSkybox >(
                [this]( auto ) { this->pipeline->ResetSkyboxTexture(); },
                std::move( **result ) ) );

    this->pipeline->RegisterSkyboxTexture( ( *skybox )->GetTexture() );

    return skybox;
}


} // namespace _3Delch
} // namespace el3D
