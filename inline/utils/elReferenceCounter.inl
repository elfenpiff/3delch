namespace el3D
{
namespace utils
{
template < typename T >
inline elReferenceCounter< T >::elReferenceCounter(
    const std::function< void() >& callWhenNoMoreReferencesExist ) noexcept
    : value{ new std::atomic< T >( 0 ) }, callWhenNoMoreReferencesExist{
                                              callWhenNoMoreReferencesExist }
{
    this->IncrementReferenceCounter();
}

template < typename T >
inline elReferenceCounter< T >::elReferenceCounter(
    const elReferenceCounter& rhs ) noexcept
    : value{ rhs.value }, callWhenNoMoreReferencesExist{
                              rhs.callWhenNoMoreReferencesExist }
{
    this->IncrementReferenceCounter();
}

template < typename T >
inline elReferenceCounter< T >::elReferenceCounter(
    elReferenceCounter&& rhs ) noexcept
    : value{ rhs.value }, callWhenNoMoreReferencesExist{
                              rhs.callWhenNoMoreReferencesExist }
{
    rhs.value                         = nullptr;
    rhs.callWhenNoMoreReferencesExist = std::function< void() >();
}

template < typename T >
inline elReferenceCounter< T >::~elReferenceCounter()
{
    this->DecrementReferenceCounter();
}

template < typename T >
inline elReferenceCounter< T >&
elReferenceCounter< T >::operator=( const elReferenceCounter& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->DecrementReferenceCounter();
        this->value                         = rhs.value;
        this->callWhenNoMoreReferencesExist = rhs.callWhenNoMoreReferencesExist;
        this->IncrementReferenceCounter();
    }

    return *this;
}

template < typename T >
inline elReferenceCounter< T >&
elReferenceCounter< T >::operator=( elReferenceCounter&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->DecrementReferenceCounter();
        this->value = std::move( rhs.value );
        this->callWhenNoMoreReferencesExist =
            std::move( rhs.callWhenNoMoreReferencesExist );
        rhs.value                         = nullptr;
        rhs.callWhenNoMoreReferencesExist = std::function< void() >();
    }

    return *this;
}

template < typename T >
inline void
elReferenceCounter< T >::IncrementReferenceCounter() noexcept
{
    if ( this->value == nullptr ) return;
    this->value->fetch_add( 1, std::memory_order_relaxed );
}

template < typename T >
inline void
elReferenceCounter< T >::DecrementReferenceCounter() noexcept
{
    if ( this->value == nullptr ) return;

    this->value->fetch_sub( 1, std::memory_order_relaxed );
    if ( *this->value == 0 )
    {
        if ( this->callWhenNoMoreReferencesExist )
            callWhenNoMoreReferencesExist();
        delete this->value;
        this->value                         = nullptr;
        this->callWhenNoMoreReferencesExist = std::function< void() >();
    }
}

template < typename T >
inline T
elReferenceCounter< T >::GetValue() const noexcept
{
    return ( this->value == nullptr )
               ? 0
               : this->value->load( std::memory_order_relaxed );
}
} // namespace utils
} // namespace el3D
