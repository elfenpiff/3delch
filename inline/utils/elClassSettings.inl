namespace el3D
{
namespace utils
{
template < typename T >
inline std::vector< T > &
elClassSettings::Get( const size_t entry ) noexcept
{
    return std::get< std::vector< T > >( this->data[entry].value );
}

template < typename T >
inline const std::vector< T > &
elClassSettings::Get( const size_t entry ) const noexcept
{
    return std::get< std::vector< T > >( this->data[entry].value );
}

template < typename T >
inline void
elClassSettings::Set( const size_t entry, const T &value,
                      const size_t offset ) noexcept
{
    for ( size_t k = 0, limit = value.size(); k < limit; ++k )
    {
        std::get< T >( this->data[entry].value )[k + offset] = value[k];
    }
}

template <>
inline void
elClassSettings::Set( const size_t entry, const std::vector< double > &value,
                      const size_t offset ) noexcept
{
    for ( size_t k = 0, limit = value.size(); k < limit; ++k )
    {
        std::get< std::vector< float > >(
            this->data[entry].value )[k + offset] =
            static_cast< float >( value[static_cast< size_t >( k )] );
    }
}

template <>
inline void
elClassSettings::Set( const size_t entry, const glm::vec4 &value,
                      const size_t offset ) noexcept
{
    for ( size_t k = 0; k < 4; ++k )
    {
        std::get< std::vector< float > >(
            this->data[entry].value )[k + offset] =
            value[static_cast< int >( k )];
    }
}

template <>
inline void
elClassSettings::Set( const size_t entry, const glm::vec3 &value,
                      const size_t offset ) noexcept
{
    for ( size_t k = 0; k < 3; ++k )
    {
        std::get< std::vector< float > >(
            this->data[entry].value )[k + offset] =
            value[static_cast< int >( k )];
    }
}

template <>
inline void
elClassSettings::Set( const size_t entry, const glm::vec2 &value,
                      const size_t offset ) noexcept
{
    for ( size_t k = 0; k < 2; ++k )
    {
        std::get< std::vector< float > >(
            this->data[entry].value )[k + offset] =
            value[static_cast< int >( k )];
    }
}

template <>
inline void
elClassSettings::Set( const size_t entry, const int &value,
                      const size_t offset ) noexcept
{
    std::get< std::vector< int > >( this->data[entry].value )[offset] = value;
}

template <>
inline void
elClassSettings::Set( const size_t entry, const float &value,
                      const size_t offset ) noexcept
{
    std::get< std::vector< float > >( this->data[entry].value )[offset] = value;
}

template <>
inline void
elClassSettings::Set( const size_t entry, const double &value,
                      const size_t offset ) noexcept
{
    std::get< std::vector< float > >( this->data[entry].value )[offset] =
        static_cast< float >( value );
}

template <>
inline void
elClassSettings::Set( const size_t entry, const std::string &value,
                      const size_t offset ) noexcept
{
    std::get< std::vector< std::string > >( this->data[entry].value )[offset] =
        value;
}

template < typename T >
elClassSettings::entry_t::entry_t( const std::string &name, const size_t size,
                                   const dataType_t        type,
                                   const std::vector< T > &defaultValue )
    : name( name ), size( size ), type( type ), defaultValue( defaultValue )
{
}
} // namespace utils
} // namespace el3D
