namespace el3D
{
namespace utils
{

// MemCpy
template < typename T >
inline MemCpyTo< T >
MemCpy::From( const T* const source ) noexcept
{
    return MemCpyTo< T >( source );
}

// MemCpyTo
template < typename T >
MemCpyTo< T >::MemCpyTo( const T* const source ) noexcept : source( source )
{
}

template < typename T >
template < typename U >
inline typename std::enable_if<
    std::is_same_v< T, U > && std::is_same_v< std::remove_const_t< T >, void >,
    MemCpySize< T > >::type
MemCpyTo< T >::To( U* const destination ) noexcept
{
    return MemCpySize( this->source, destination );
}

template < typename T >
template < typename U >
inline typename std::enable_if<
    std::is_same_v< T, U > && !std::is_same_v< std::remove_const_t< T >, void >,
    T* >::type
MemCpyTo< T >::To( U* const destination ) noexcept
{
    return MemCpySize< T >( this->source, destination ).Size( sizeof( T ) );
}

// MemCpySize
template < typename T >
MemCpySize< T >::MemCpySize( const T* const source,
                             T* const       destination ) noexcept
    : source( source ), destination( destination )
{
}

template < typename T >
inline T*
MemCpySize< T >::Size( size_t size ) noexcept
{
    using nonConstT = std::remove_const_t< T >;
    return static_cast< T* >( memcpy(
        static_cast< void* >( const_cast< nonConstT* >( destination ) ),
        static_cast< void* >( const_cast< nonConstT* >( source ) ), size ) );
}

} // namespace utils
} // namespace el3D
