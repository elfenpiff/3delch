namespace el3D
{
namespace utils
{
template < typename T >
inline elAllocator< T >::elAllocator() noexcept
    : elAllocator(
          []( uint64_t numberOfObjects ) {
              return static_cast< T* >(
                  malloc( sizeof( T ) * numberOfObjects ) );
          },
          []( T* memory, uint64_t numberOfObjects ) {
              for ( uint64_t k = 0; k < numberOfObjects; ++k )
                  memory[k].~T();
              free( memory );
          }

      )
{
    this->isDefaultConstructed = true;
}

template < typename T >
inline elAllocator< T >::elAllocator(
    const allocator_t& allocator, const deallocator_t& deallocator ) noexcept
    : allocator( allocator ), deallocator( deallocator )
{
    this->isDefaultConstructed = false;
}

template < typename T >
inline T*
elAllocator< T >::allocate( const uint64_t numberOfObjects ) noexcept
{
    return elAllocator::allocator( numberOfObjects );
}

template < typename T >
inline void
elAllocator< T >::deallocate( T*             memory,
                              const uint64_t numberOfObjects ) noexcept
{
    return elAllocator::deallocator( memory, numberOfObjects );
}

template < typename T >
inline bool
elAllocator< T >::operator==( const elAllocator& ) const noexcept
{
    return this->isDefaultConstructed;
}

template < typename T >
inline bool
elAllocator< T >::operator!=( const elAllocator& rhs ) const noexcept
{
    return !operator==( rhs );
}


} // namespace utils
} // namespace el3D
