namespace el3D
{
namespace utils
{
template < typename To_T, typename From_T >
inline To_T
ToGLM( const From_T& c, const size_t offset ) noexcept
{
    To_T newCon;

    for ( size_t i = 0, l = static_cast< size_t >( newCon.length() ); i < l;
          ++i )
        newCon[static_cast< int >( i )] = c[i + offset];

    return newCon;
}

template < typename GLMType >
inline std::vector< float >
SetVectorFrom( const uint64_t repetition, const GLMType& v ) noexcept
{
    const auto           vectorLength = v.length();
    std::vector< float > retVal;
    retVal.reserve( repetition * static_cast< uint64_t >( vectorLength ) );

    for ( uint64_t i = 0; i < repetition; ++i )
    {
        for ( int k = 0; k < vectorLength; ++k )
        {
            retVal.emplace_back( v[k] );
        }
    }

    return retVal;
}

template < typename T >
inline std::string
ToString( const T value, const uint64_t precision ) noexcept
{
    std::stringstream ss;
    ss << std::fixed << std::setprecision( static_cast< int >( precision ) )
       << value;
    return ss.str();
}

} // namespace utils
} // namespace el3D
