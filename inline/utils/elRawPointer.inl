namespace el3D
{
namespace utils
{
template < typename T >
constexpr inline elRawPointer< T >::elRawPointer() noexcept : t( nullptr )
{
}

template < typename T >
constexpr inline elRawPointer< T >::elRawPointer( T* t ) noexcept : t( t )
{
}

template < typename T >
constexpr inline elRawPointer< T >::elRawPointer( const T* t ) noexcept
    : t( const_cast< T* >( t ) )
{
}

template < typename T >
constexpr inline elRawPointer< T >::operator T*() noexcept
{
    return t;
}

template < typename T >
constexpr inline elRawPointer< T >::operator T*() const noexcept
{
    return t;
}

template < typename T >
constexpr inline T& elRawPointer< T >::operator->() noexcept
{
    return *t;
}

template < typename T >
constexpr inline const T& elRawPointer< T >::operator->() const noexcept
{
    return *t;
}

template < typename T >
constexpr inline T*
elRawPointer< T >::Get() noexcept
{
    return t;
}

template < typename T >
constexpr inline const T*
elRawPointer< T >::Get() const noexcept
{
    return t;
}

template < typename T >
constexpr inline T*
elRawPointer< T >::__Get__() noexcept
{
    return t;
}


} // namespace utils
} // namespace el3D
