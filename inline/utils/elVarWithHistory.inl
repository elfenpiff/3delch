namespace el3D
{
namespace utils
{
template < typename T >
elVarWithHistory< T >::elVarWithHistory( const T& initialValue )
    : value( initialValue ), previousValue( initialValue )
{
}

template < typename T >
template < typename... Targs >
void
elVarWithHistory< T >::Assign( Targs&&... t )
{
    this->previousValue = this->value;
    this->value         = T( std::forward< Targs >( t )... );
}

template < typename T >
elVarWithHistory< T >&
elVarWithHistory< T >::operator=( const T& rhs )
{
    this->previousValue = this->value;
    this->value         = rhs;
    return *this;
}

template < typename T >
elVarWithHistory< T >&
elVarWithHistory< T >::operator=( T&& rhs )
{
    this->previousValue = this->value;
    this->value         = rhs;
    return *this;
}

template < typename T >
elVarWithHistory< T >::operator T() const
{
    return this->value;
}

template < typename T >
elVarWithHistory< T >::operator T()
{
    return this->value;
}

template < typename T >
T
elVarWithHistory< T >::PreviousValue() const
{
    return this->previousValue;
}

template < typename T >
const T&
elVarWithHistory< T >::Get() const noexcept
{
    return this->value;
}

template < typename T >
void
elVarWithHistory< T >::ResetHistory() noexcept
{
    this->previousValue = this->value;
}
} // namespace utils
} // namespace el3D
