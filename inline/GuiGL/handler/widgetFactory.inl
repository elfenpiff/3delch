namespace el3D
{
namespace GuiGL
{
namespace handler
{
namespace helper
{
template < typename T >
struct GetTitlebar
{
    static T*
    Get( widgetFactory* )
    {
        return nullptr;
    }
};
} // namespace helper

template < typename T >
inline std::enable_if_t< !std::is_same_v< T, elWidget_Titlebar >,
                         widget_t< T > >
widgetFactory::Create(
    const std::string&                             className,
    const std::function< void( elWidget_base* ) >& customDeleter ) noexcept
{
    if ( this->removeNullptrFromWidgets )
    {
        bb::erase( this->widgets, nullptr );
        this->removeNullptrFromWidgets = false;
    }
    constructor_t constructor{ this->me,
                               this->shader,
                               this->glowShader,
                               this->eventTexture,
                               this->eventHandler,
                               this->stickyExclusiveEvent,
                               this->window,
                               this->fontCache,
                               this->mouseCursor,
                               this->shaderTextureRegistrator,
                               this->shaderTextureDeregistrator,
                               this->shaderVersion,
                               this->theme,
                               this->GetType< T >(),
                               this->screenWidth,
                               this->screenHeight,
                               className,
                               TYPE_END,
                               this->glowScaling,
                               *this->callbackContainer };
    T*            newWidget = new T( constructor );
    this->PrepareWidget( newWidget );
    this->widgets.emplace_back( newWidget );

    return widget_t< T >( newWidget,
                          [=]( elWidget_base* ptr )
                          {
                              if ( customDeleter ) customDeleter( ptr );
                              widgetFactory::RemoveFromParent( ptr );
                              widgetFactory::DeleteWidget( ptr );
                          } );
}


template <>
inline widgetType_t
widgetFactory::GetType< elWidget_base >()
{
    return TYPE_BASE;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Button >()
{
    return TYPE_BUTTON;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Entry >()
{
    return TYPE_ENTRY;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Image >()
{
    return TYPE_IMAGE;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Label >()
{
    return TYPE_LABEL;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_MouseCursor >()
{
    return TYPE_MOUSE_CURSOR;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Slider >()
{
    return TYPE_SLIDER;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_TextCursor >()
{
    return TYPE_TEXT_CURSOR;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Window >()
{
    return TYPE_WINDOW;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Table >()
{
    return TYPE_TABLE;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Listbox >()
{
    return TYPE_LISTBOX;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Dropbutton >()
{
    return TYPE_DROPBUTTON;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_ShaderAnimation >()
{
    return TYPE_SHADERANIMATION;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Checkbutton >()
{
    return TYPE_CHECKBUTTON;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Progress >()
{
    return TYPE_PROGRESS;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_TabbedWindow >()
{
    return TYPE_TABBEDWINDOW;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Menu >()
{
    return TYPE_MENU;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Menubar >()
{
    return TYPE_MENUBAR;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_PopupMenu >()
{
    return TYPE_POPUPMENU;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Textbox >()
{
    return TYPE_TEXTBOX;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_InfoMessage >()
{
    return TYPE_INFOMESSAGE;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_YesNoDialog >()
{
    return TYPE_YESNODIALOG;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Tree >()
{
    return TYPE_TREE;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_FileBrowser >()
{
    return TYPE_FILEBROWSER;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Titlebar >()
{
    return TYPE_TITLEBAR;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Graph >()
{
    return TYPE_GRAPH;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Selection >()
{
    return TYPE_SELECTION;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_KeyValuePair >()
{
    return TYPE_KEY_VALUE_PAIR;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Accordion >()
{
    return TYPE_ACCORDION;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_Grid >()
{
    return TYPE_GRID;
}

template <>
inline widgetType_t
widgetFactory::GetType< elWidget_ColorPicker >()
{
    return TYPE_COLOR_PICKER;
}
} // namespace handler
} // namespace GuiGL
} // namespace el3D
