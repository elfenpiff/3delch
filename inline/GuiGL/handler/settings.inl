namespace el3D
{
namespace GuiGL
{
namespace handler
{
template < typename T, typename getter_t >
inline std::pair< std::vector< std::string >, std::vector< T > >
settings::GetSettingsEntry( const size_t property, getter_t get,
                            const std::vector< T >& defaultValue ) noexcept
{
    auto&            prop  = widgetProperty[static_cast< size_t >( property )];
    std::vector< T > value = defaultValue;
    std::string      thisType = this->GetWidgetLine( 0 )[0];
    std::vector< std::string > cfgPath;

    for ( size_t i = 0; i < this->settingsLookupDepth; ++i )
    {
        cfgPath = this->GetWidgetLine( i );
        cfgPath.emplace_back( widgetProperty[property].name );
        value = get( cfgPath, value );
    }

    // check if the value size from the theme matches the required size
    if ( value.size() != prop.size )
    {
        LOG_FATAL( 0 ) << "The dimensions for " + prop.name + " in " +
                              thisType + " must be " +
                              std::to_string( prop.size ) + " but has size " +
                              std::to_string( value.size() );
        std::terminate();
    }

    cfgPath.pop_back();
    return std::make_pair( cfgPath, value );
}

template < typename Exception >
inline void
settings::ThrowIfPropertyIsUnset( const size_t property ) const
{
    if ( this->widgetSettings.Get< std::string >( property )[0].empty() )
        throw Exception(
            LOG_EX( 0, this->UnsetPropertyErrorMessage( property ) ) );
}

template < typename T >
inline T
settings::RoundVector( const T& value ) const noexcept
{
    T roundedValue = value;
    for ( auto i = 0, length = value.length(); i < length; ++i )
        roundedValue[i] = std::round( roundedValue[i] );

    return roundedValue;
}


} // namespace handler
} // namespace GuiGL
} // namespace el3D
