namespace el3D
{
namespace GuiGL
{
template < typename T >
T*
elWidget_Table::GetElement( const size_t col, const size_t row ) noexcept
{
    return static_cast< T* >( this->elements[row][col].Get() );
}

template < typename T >
const T*
elWidget_Table::GetElement( const size_t col, const size_t row ) const noexcept
{
    return static_cast< const T* >( this->elements[row][col].Get() );
}
} // namespace GuiGL
} // namespace el3D
