#include <iostream>

namespace el3D
{
namespace GuiGL
{
template < typename T, typename Factory >
inline widget_ptr< T, Factory >::widget_ptr(
    T* const                                       ptr,
    const std::function< void( elWidget_base* ) >& deleter ) noexcept
    : ptr( ptr ), deleter( deleter )
{
}

template < typename T, typename Factory >
template < typename Child >
inline widget_ptr< T, Factory >::widget_ptr(
    widget_ptr< Child, Factory >&& rhs ) noexcept
{
    *this = std::move( rhs );
}

template < typename T, typename Factory >
template < typename Child >
inline widget_ptr< T, Factory >&
widget_ptr< T, Factory >::operator=(
    widget_ptr< Child, Factory >&& rhs ) noexcept
{
    static_assert( std::is_base_of_v< T, Child >,
                   "T must be the base of the origin widget_ptr type" );
    if ( static_cast< void* >( this ) != static_cast< void* >( &rhs ) )
    {
        this->Reset();
        this->ptr     = rhs.ptr;
        this->deleter = std::move( rhs.deleter );
        rhs.ptr       = nullptr;
    }

    return *this;
}

template < typename T, typename Factory >
inline widget_ptr< T, Factory >::~widget_ptr()
{
    this->Reset();
}

template < typename T, typename Factory >
inline void
widget_ptr< T, Factory >::Reset() noexcept
{
    if ( this->ptr != nullptr && this->deleter ) this->deleter( this->ptr );

    this->ptr = nullptr;
}

template < typename T, typename Factory >
inline T*
widget_ptr< T, Factory >::operator->() noexcept
{
    return this->ptr;
}

template < typename T, typename Factory >
inline const T*
widget_ptr< T, Factory >::operator->() const noexcept
{
    return this->ptr;
}

template < typename T, typename Factory >
inline T*
widget_ptr< T, Factory >::Get() noexcept
{
    return this->ptr;
}

template < typename T, typename Factory >
inline const T*
widget_ptr< T, Factory >::Get() const noexcept
{
    return this->ptr;
}


template < typename T, typename Factory >
inline T&
widget_ptr< T, Factory >::operator*() noexcept
{
    return *this->ptr;
}

template < typename T, typename Factory >
inline const T&
widget_ptr< T, Factory >::operator*() const noexcept
{
    return *this->ptr;
}

template < typename T, typename Factory >
inline widget_ptr< T, Factory >::operator bool() const noexcept
{
    return this->ptr != nullptr;
}


} // namespace GuiGL
} // namespace el3D
