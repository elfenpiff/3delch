namespace el3D
{
namespace GuiGL
{
template < typename T >
inline std::enable_if_t< !std::is_same_v< T, elWidget_Titlebar >,
                         widget_t< T > >
elWidget_Grid::CreateInGrid(
    const glm::uvec2& position, Grid::elementProperties_t properties,
    const std::string&                             className,
    const std::function< void( elWidget_base* ) >& customDeleter ) noexcept
{
    auto widget = this->Create< T >( className,
                                     [=, this]( auto ptr )
                                     {
                                         if ( customDeleter )
                                             customDeleter( ptr );
                                         this->Remove( ptr );
                                     } );

    this->AdjustGridSizeTo( position );
    this->gridContents[position.x][position.y].ptr        = widget.Get();
    this->gridContents[position.x][position.y].properties = properties;
    this->runOnce.Call( _Method_Reshape_ );
    return widget;
}
} // namespace GuiGL
} // namespace el3D
