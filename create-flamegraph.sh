#!/bin/bash

CMD=$1
TIME=1
FLAMEGRAPH_PATH="/usr/bin/"

eval $CMD &
CMD_PID=$!

echo "Capturing output of \"$CMD\" for $TIME seconds"

perf record -F 99 -p $CMD_PID -g -- sleep $TIME
perf script > out.perf

$FLAMEGRAPH_PATH/stackcollapse-perf.pl out.perf > out.folded
$FLAMEGRAPH_PATH/flamegraph.pl out.folded > out.svg
