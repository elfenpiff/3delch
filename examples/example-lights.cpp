#include "3Delch/3Delch.hpp"
#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/GuiGL.hpp"
#include "HighGL/HighGL.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/OpenGL.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "utils/utils.hpp"
#include "world/elModel.hpp"
#include "world/elModelGroup.hpp"
#include "world/elPointLight.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->sky->UseForReflection( false );

            this->sun1 = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 1.0f, 0.7f, 0.4f ),
                .direction        = glm::vec3( -0.5f, -1.0f, 0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.2f,
                .doCastShadow     = true,
                .hasFog           = true,
                .fogIntensity     = 3.0f } );
            this->sun1->Move( units::Angle::Degree( 0.0 ),
                              units::Angle::Degree( 0.0 ),
                              glm::vec2( 1.0, 0.0 ), 0.1f );

            this->sun2 = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( 0.3f, -1.0f, -0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.0f,
                .doCastShadow     = true } );

            this->pointLight1 = this->Create< world::elPointLight >(
                world::pointLightProperties_t{
                    .color            = { 1.0f, 0.0f, 0.0f },
                    .attenuation      = { 0.1f, 0.1f, 0.1f },
                    .position         = { 2.0f, 3.0f, 0.0f },
                    .diffuseIntensity = 1.5f,
                    .ambientIntensity = 0.2f,
                    .doCastShadow     = true } );

            this->pointLight2 = this->Create< world::elPointLight >(
                world::pointLightProperties_t{
                    .color            = { 0.0f, 0.0f, 1.0f },
                    .attenuation      = { 0.1f, 0.1f, 0.1f },
                    .position         = { -4.0f, 3.0f, 0.0f },
                    .diffuseIntensity = 1.5f,
                    .ambientIntensity = 0.2f,
                    .doCastShadow     = true } );

            this->sphere = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateSphere( 100 )
                    .GetObjectGeometry() );

            this->sponza = this->Create< world::elModelGroup >(
                "data/demoModels/sponza/sponza.obj", 0.01f );
            this->rover = this->Create< world::elModelGroup >(
                "data/MarsRover-Mesh_v1.0.obj", 0.1f );
        }

        bb::product_ptr< world::elModelGroup > sponza;
        bb::product_ptr< world::elModelGroup > rover;
        bb::product_ptr< world::elSky >        sky;
        bb::product_ptr< world::elSun >        sun1;
        bb::product_ptr< world::elSun >        sun2;
        bb::product_ptr< world::elPointLight > pointLight1;
        bb::product_ptr< world::elPointLight > pointLight2;
        bb::product_ptr< world::elModel >      sphere;
    } world( *scene );


    world.pointLight2->MoveTo( world.pointLight2->GetProperties().position,
                               { 4.0f, 3.0f, 0.0f }, 0.4f, true );

    world.sphere->SetDiffuseColor( { 0x99, 0x99, 0x99, 0xff } );
    world.sphere->SetSpecularColor( { 0x00, 0x00, 0xff }, 0xff );
    world.sphere->SetShininessAndRoughness( 0x22, 0x00 );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
