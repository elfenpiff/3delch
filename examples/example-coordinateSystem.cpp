#include "3Delch/3Delch.hpp"
#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/GuiGL.hpp"
#include "HighGL/HighGL.hpp"
#include "HighGL/elCoordinateSystemGenerator.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/OpenGL.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "lua/elLuaScript.hpp"
#include "lua/lua.hpp"
#include "math/elPerlinNoise.hpp"
#include "units/Velocity.hpp"
#include "utils/utils.hpp"
#include "world/elCoordinateSystem.hpp"
#include "world/elModelGroup.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    class MyWorld : public world::elWorld
    {
      public:
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky = this->Create< world::elSky >(
                world::ColoredSkybox, glm::vec4{ glm::vec3{ 0.4f }, 1.0f } );

            this->sun = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( -0.3f, -1.0f, 0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.2f,
                .doCastShadow     = true } );
            this->coordinateSystem = this->Create< world::elCoordinateSystem >(
                world::CoordinateSystemProperties{
                    { 10, 10 },
                    { 0.0, 0.0, 0.0 },
                    HighGL::elCoordinateSystemGenerator::GridStyle::Lines,
                    world::CoordinateSystemStyle::MayorMinorPlane } );
            this->coordinateSystemFloat = this->Create<
                world::elCoordinateSystem >( world::CoordinateSystemProperties{
                { 10, 10 },
                { 0.0, 0.0, 0.0 },
                HighGL::elCoordinateSystemGenerator::GridStyle::DotsAndCrosses,
                world::CoordinateSystemStyle::MayorMinor } );
            this->rover = this->Create< world::elModelGroup >(
                "data/MarsRover-Mesh_v1.0.obj", 0.1f );
            this->rover->SetDiffuseColor( { 0x44, 0xcc, 0x66, 0xff } );
            this->rover->SetSpecularColor( { 0xff, 0x00, 0x00 }, 0x99 );
            this->rover->SetShininessAndRoughness( 0xff, 0x55 );
        }

        bb::product_ptr< world::elModelGroup >       rover;
        bb::product_ptr< world::elSky >              sky;
        bb::product_ptr< world::elCoordinateSystem > coordinateSystem;
        bb::product_ptr< world::elCoordinateSystem > coordinateSystemFloat;
        bb::product_ptr< world::elSun >              sun;
    } myWorld( *scene );

    // myWorld.coordinateSystem->SetRotation( { 1.0f, 0.0f, 0.0f, M_PI_2 } );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
