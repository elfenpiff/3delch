#include "math/elSpline_Closed.hpp"
#include "math/elSpline_Open.hpp"

#include <iostream>
#include <string>

using namespace el3D::math;

std::string
ToString( const glm::vec3& v ) noexcept
{
    return std::to_string( v.x ) + " " + std::to_string( v.y ) + " " +
           std::to_string( v.z );
}

int
main()
{
    path_t< glm::vec3 > path = { { 1.0, 0.5, 2.0 },
                                 { 2.0, 3.0, 4.0 },
                                 { 0.0, 0.2, 0.9 },
                                 { 0.5, 3.0, 0.4 },
                                 { 2.0, 0.0, 2.0 } };
    // elSpline_Open< glm::vec3 > spline( path, 0.5f, { 1.0, 1.0, 1.0 },
    //                                   { -1.0, -1.0, -1.0 } );
    elSpline_Closed< glm::vec3 > spline( path, 1.0f );

    for ( float i = -3.0f; i < 16.0f; i += 0.1f )
        std::cout << ToString( spline( i ) ) << std::endl;

    std::cout << "length " << spline.Length() << std::endl;

    return 0;
}
