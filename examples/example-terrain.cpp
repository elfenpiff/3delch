#include "3Delch/3Delch.hpp"
#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/GuiGL.hpp"
#include "HighGL/HighGL.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/OpenGL.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "lua/elLuaScript.hpp"
#include "lua/lua.hpp"
#include "math/elPerlinNoise.hpp"
#include "units/Velocity.hpp"
#include "utils/utils.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elTerrain.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    class MyWorld : public world::elWorld
    {
      public:
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );

            this->sun = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( -0.3f, -1.0f, 0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.2f,
                .doCastShadow     = true } );

            this->terrain = this->Create< world::elTerrain >(
                world::terrainProperties_t{ .frequency       = 6.0f,
                                            .octaves         = 32,
                                            .seed            = 19231,
                                            .dimension       = { 1024, 1024 },
                                            .widthScaling    = 0.1f,
                                            .heightScaling   = 0.03f,
                                            .normalGridRange = 5 } );
        }

        bb::product_ptr< world::elSky >     sky;
        bb::product_ptr< world::elSun >     sun;
        bb::product_ptr< world::elTerrain > terrain;
    } myWorld( *scene );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
