#include "3Delch/3Delch.hpp"
#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/GuiGL.hpp"
#include "HighGL/Conversion.hpp"
#include "HighGL/HighGL.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/OpenGL.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "lua/elLuaScript.hpp"
#include "lua/lua.hpp"
#include "math/elPerlinNoise.hpp"
#include "units/Velocity.hpp"
#include "utils/utils.hpp"
#include "world/elBoundingBox.hpp"
#include "world/elInteractiveMesh.hpp"
#include "world/elInteractiveSet.hpp"
#include "world/elModel.hpp"
#include "world/elModelEffect.hpp"
#include "world/elModelGroup.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elTerrain.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sphere = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateSphere( 100 )
                    .GetObjectGeometry() );
            this->sky =
                this->Create< world::elSky >( world::ColoredSkyboxFromConfig );
            this->sky->UseForReflection( false );
            this->sun = this->Create< world::elSun >( world::SunProperties{} );
            this->sponza = this->Create< world::elModelGroup >(
                "data/demoModels/sponza/sponza.obj", 0.01f );
            this->rover = this->Create< world::elModelGroup >(
                "data/MarsRover-Mesh_v1.0.obj", 0.1f );

            this->interactiveSet = this->Create< world::elInteractiveSet >();
        }

        bb::product_ptr< world::elModelGroup >     sponza;
        bb::product_ptr< world::elModelGroup >     rover;
        bb::product_ptr< world::elModel >          sphere;
        bb::product_ptr< world::elSky >            sky;
        bb::product_ptr< world::elSun >            sun;
        bb::product_ptr< world::elBoundingBox >    boundingBox;
        bb::product_ptr< world::elModelEffect >    silhouette;
        bb::product_ptr< world::elInteractiveSet > interactiveSet;
    } world( *scene );

    world.rover->Rotate( { 0.0, 1.0, 0.0 }, 1.0 );
    world.rover->MoveTo( { -10.0, 0.0, 0.0 }, 0.5 );

    world.sphere->MoveTo( { 10.0, 0.0, 0.0 }, 0.5 );

    world.interactiveSet->SetupInteractiveMesh(
        { .click = []( auto v ) { printf( "%f %f %f\n", v.x, v.y, v.z ); } },
        100.0f, 1000U );
    world.interactiveSet->GetInteractiveMesh().SetStickToCamera( true );


    world.sun->Move( units::Angle::Degree( 90 ), units::Angle::Degree( 0 ),
                     { 1.0f, 0.0f }, 0.01f );

    world.sky
        ->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Linear >( &world::elSky::SetColor )
        .SetSource( { 0.0, 0.0, 0.0, 1.0 } )
        .SetDestination( { 1.0, 1.0, 1.0, 1.0 } )
        .SetVelocity( 0.1f )
        .SetVelocityType( animation::AnimationVelocity::Absolute );

    auto normalMap =
        HighGL::ConvertHeightMapToNormalMap( "data/heightmap.png", 0.001f );

    world.sphere->SetDiffuseColor( { 0x99, 0x99, 0x99, 0xff } );
    world.sphere->SetTexture( world::TextureType::Normal, normalMap.width,
                              normalMap.height, normalMap.data.stream.data() );
    world.sphere->SetSpecularColor( { 0x00, 0x00, 0xff }, 0xff );
    world.sphere->SetShininessAndRoughness( 0x22, 0x00 );
    world.sphere
        ->Animate< animation::elAnimate_SourceDestination,
                   animation::interpolation::Hermite >(
            &world::elModel::SetPosition )
        .SetSource( { 0.0, 0.0, 0.0 } )
        .SetDestination( { 2.0, 0.0, 0.0 } )
        .SetVelocity( 1.0f )
        .SetVelocityType( animation::AnimationVelocity::Absolute )
        .SetLoop( true );

    world.sphere->SetEmissionColor( { 0x00, 0xff, 0xff, 0x39 } );

    world.interactiveSet->Attach( *world.sphere, {} );
    world.interactiveSet->Attach( *world.rover, {} );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
