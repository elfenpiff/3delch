#include "3Delch/3Delch.hpp"
#include "3Delch/elScene.hpp"
#include "world/elModel.hpp"
#include "world/elModelEffect.hpp"
#include "world/elModelGroup.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

#include <glm/gtx/rotate_vector.hpp>
using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->sun = this->Create< world::elSun >( world::SunProperties{} );
            this->sun->SetPosition( units::Angle::Degree( 45.0 ),
                                    units::Angle::Degree( -45.0 ) );

            this->ground = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateCube()
                    .GetObjectGeometry() );
            constexpr float WORLD_SIZE = 250.0f;
            this->ground->SetScaling( { WORLD_SIZE, 1.0, WORLD_SIZE } );
            this->ground->SetPosition( { 0.0, -5.0, 0.0 } );
            this->ground->SetDiffuseColor( { 0x88, 0x88, 0x88, 0xff } );

            this->ground->SetPhysicalProperties< btBoxShape >(
                world::physicalProperties_t{ .mass = 0.0f },
                btVector3( WORLD_SIZE, 1.0, WORLD_SIZE ) );
        }

        bb::product_ptr< world::elModel > ground;
        bb::product_ptr< world::elSky >   sky;
        bb::product_ptr< world::elSun >   sun;
    };

    MyWorld world( *scene );
    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
