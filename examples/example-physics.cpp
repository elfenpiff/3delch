#include "3Delch/3Delch.hpp"
#include "world/elModel.hpp"
#include "world/elPhysics.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;
using namespace ::el3D::animation;
using namespace ::el3D::units::literals;

int
main()
{
    auto& instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto& scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    class MyWorld : public world::elWorld
    {
      public:
        MyWorld( _3Delch::elScene& scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->box = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateCube()
                    .GetObjectGeometry() );
            this->box->SetScaling( { 50.0, 50.0, 50.0 } );
            this->box->SetPosition( { 0.0, -56.0, 0.0 } );
            this->box->SetDiffuseColor( { 0x88, 0x88, 0x88, 0xff } );
            this->box->SetPhysicalProperties< btBoxShape >(
                world::physicalProperties_t{ .mass = 0.0f },
                btVector3( 50.0, 50.0, 50.0 ) );

            this->sun = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( -0.3f, -1.0f, 0.1f ),
                .diffuseIntensity = 0.9f,
                .ambientIntensity = 0.5f,
                .doCastShadow     = true } );

            this->sphereTemplate = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateSphere( 20 )
                    .GetObjectGeometry() );
            this->sphereTemplate->SetPosition( { 2.0, 10.0, 0.0 } );
            this->sphereTemplate->SetDiffuseColor( { 0x33, 0x99, 0x11, 0xff } );
            this->sphereTemplate->SetPhysicalProperties< btSphereShape >(
                world::physicalProperties_t{ .mass = 10.0f }, 1.0f );

            this->boxTemplate = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateCube()
                    .GetObjectGeometry() );
            this->boxTemplate->SetPosition( { 3.0, 10.0, 0.0 } );
            this->boxTemplate->SetScaling( { 0.1, 1.0, 0.25 } );
            this->boxTemplate->SetDiffuseColor( { 0x99, 0x33, 0x11, 0xff } );
            this->boxTemplate->SetPhysicalProperties< btBoxShape >(
                world::physicalProperties_t{ .mass = 1.0f },
                btVector3( 0.1f, 1.0f, 0.25f ) );
        }

        void
        CreateSphere()
        {
            this->spheres.emplace_back( this->sphereTemplate->Clone() );
        }

        void
        CreateBox()
        {
            this->boxes.emplace_back( this->boxTemplate->Clone() );
        }


        bb::product_ptr< world::elSky >   sky;
        bb::product_ptr< world::elModel > box;

        bb::product_ptr< world::elModel > sphereTemplate;
        bb::product_ptr< world::elModel > boxTemplate;


        std::vector< bb::product_ptr< world::elModel > > spheres;
        std::vector< bb::product_ptr< world::elModel > > boxes;
        bb::product_ptr< world::elSun >                  sun;
    } myWorld( *scene );

    scene->SetupFreeFlyCamera();

    auto callback = _3Delch::CreateMainLoopCallback(
        [&]
        {
            double          time = instance.Window()->GetRuntime().GetSeconds();
            static double   previousTime = time;
            static uint64_t counter      = 0;

            if ( time - previousTime > 0.5 )
            {
                previousTime = time;
                ++counter;
                if ( counter % 2 == 0 )
                    myWorld.CreateSphere();
                else
                    myWorld.CreateBox();
            }
        } );

    instance.StartMainLoop();

    return 0;
}
