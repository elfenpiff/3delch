#include "3Delch/3Delch.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Checkbutton.hpp"
#include "GuiGL/elWidget_Dropbutton.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_FileBrowser.hpp"
#include "GuiGL/elWidget_Graph.hpp"
#include "GuiGL/elWidget_Image.hpp"
#include "GuiGL/elWidget_InfoMessage.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Listbox.hpp"
#include "GuiGL/elWidget_Menu.hpp"
#include "GuiGL/elWidget_Menubar.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_PopupMenu.hpp"
#include "GuiGL/elWidget_Progress.hpp"
#include "GuiGL/elWidget_Selection.hpp"
#include "GuiGL/elWidget_ShaderAnimation.hpp"
#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_TabbedWindow.hpp"
#include "GuiGL/elWidget_Table.hpp"
#include "GuiGL/elWidget_TextCursor.hpp"
#include "GuiGL/elWidget_Textbox.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "GuiGL/elWidget_Tree.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "GuiGL/elWidget_YesNoDialog.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "HighGL/elMeshLoader.hpp"
#include "OpenGL/OpenGL.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "network/network.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/utils.hpp"
#include "world/elGui.hpp"
#include "world/elModelGroup.hpp"
#include "world/elPointLight.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"


using namespace ::el3D;

std::vector< GLfloat > triangle  = { 0.0,  0.0,  0.0,  1.0f, 0.0f, 0.0f,
                                    1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };
std::vector< GLuint >  elements  = { 0, 1, 2, 2, 3, 0 };
std::vector< GLuint >  diffuse   = { 0, 0, 0, 0 };
std::vector< GLfloat > texCoords = { 0.0f, 0.0f, 1.0f, 0.0f,
                                     1.0f, 1.0f, 0.0f, 1.0f };

std::vector< GLfloat > triangle2  = { -1.0, -1.0, 2.0,  1.0f,  -1.0f, 2.0f,
                                     1.0f, 1.0f, 2.0f, -1.0f, 1.0f,  2.0f };
std::vector< GLuint >  diffuse2   = { 1, 1, 1, 1 };
std::vector< GLuint >  elements2  = { 0, 1, 2, 2, 3, 0 };
std::vector< GLfloat > texCoords2 = { 0.0f, 0.0f, 1.0f, 0.0f,
                                      1.0f, 1.0f, 0.0f, 1.0f };

GuiGL::elWidget_TextCursor* textCursor;

GLint timeLoc;

int
main()
{
    auto& instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto& scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    auto img2 = GLAPI::elImage::Create( GLAPI::FromFilePath, "data/dog.jpg",
                                        SDL_PIXELFORMAT_RGBA32 )
                    .GetValue();

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene& scene ) : world::elWorld( scene )
        {
            this->sponza = this->Create< world::elModelGroup >(
                "data/demoModels/sponza/sponza.obj", 0.01f );

            this->dirLight = this->Create< world::elSun >( world::SunProperties{
                .color            = glm::vec3( 0.9f, 0.9f, 1.0f ),
                .direction        = glm::vec3( -0.1f, -1.0f, -0.1f ),
                .diffuseIntensity = 0.8f,
                .ambientIntensity = 0.2f,
                .doCastShadow     = true } );

            this->pointLight = this->Create< world::elPointLight >(
                world::pointLightProperties_t{
                    .color            = { 1.0f, 0.0f, 0.0f },
                    .attenuation      = { 0.1f, 0.1f, 0.1f },
                    .position         = { 2.0f, 3.0f, 0.0f },
                    .diffuseIntensity = 1.5f,
                    .ambientIntensity = 0.2f,
                    .doCastShadow     = true } );
        }

        bb::product_ptr< world::elModelGroup > sponza;
        bb::product_ptr< world::elSun >        dirLight;
        bb::product_ptr< world::elPointLight > pointLight;
    } world( *scene );

    struct MyGui : public world::elGui
    {
        MyGui( _3Delch::elScene& scene ) : world::elGui( scene ), tex2( "" )
        {
            auto img =
                GLAPI::elImage::Create( GLAPI::FromFilePath, "data/cat.jpg",
                                        SDL_PIXELFORMAT_RGBA32 )
                    .GetValue();
            this->tex2 = OpenGL::elTexture_2D( "cat", *img.get() );
            this->img1 = this->Create< GuiGL::elWidget_Image >();
            img1->SetPosition( glm::vec2( 80, 80 ) );
            img1->SetSize( glm::vec2( 800, 800 ) );
            img1->SetBorderSize( glm::vec4( 40 ) );
            img1->SetImageFromTexture( &tex2 );
        }

        OpenGL::elTexture_2D      tex2;
        GuiGL::elWidget_Image_ptr img1;
    } gui( *scene );

    // GLAPI::elFont* font1(
    //    GLAPI::elFont::FromCache( "data/fonts/Ubuntu-R.ttf", 24, false ) );
    // font1->SetText( "hello world", {0, 0, 0, 0} );
    // auto dim = font1->GetTextureSize();
    // font1->SetText( "FLOP", {255, 255, 255, 255} );

    // tex2.SetSize( dim.width, dim.height );
    // tex2.TexImage( font1->GetBytePointer() );

    auto win = scene->GUI()->Create< GuiGL::elWidget_Window >();
    win->SetPosition( glm::vec2( 700, 10 ) );
    win->SetSize( glm::vec2( 350, 400 ) );
    win->SetBorderSize( glm::vec4( 10 ) );

    //
    //    elTexture_2D tex3;
    //    auto         img2w = win->Create< GuiGL::elWidget_Image >();
    //    img2w->SetPosition( glm::vec2( 50, 50 ) );
    //    img2w->SetSize( glm::vec2( 400, 400 ) );
    //    img2w->SetBorderSize( glm::vec4( 40 ) );
    //    img2w->SetImage( img2 );
    //
    auto win3 = win->Create< GuiGL::elWidget_Window >();
    win3->SetPosition( glm::vec2( 400, 100 ) );
    win3->SetSize( glm::vec2( 50, 50 ) );
    win3->SetBorderSize( glm::vec4( 5 ) );

    auto label = win->Create< GuiGL::elWidget_Label >();
    label->SetPosition( { 0, 0 } );
    label->SetText( "Hallo Welt!" );
    label->SetBaseColorForState( { 1.0, 0.0, 0.0, 0.0 }, GuiGL::STATE_DEFAULT );


    auto entry = win->Create< GuiGL::elWidget_Entry >();
    entry->SetPosition( { 0, 100 } );
    entry->SetSize( { 400, 100 } );
    entry->SetText( "Hallo WElt" );

    auto button = win->Create< GuiGL::elWidget_Button >();
    button->SetPosition( { 0, 250 } );
    button->SetSize( { 300, 40 } );
    button->SetText( "Click me" );
    button->SetClickCallback( []() { printf( "click!!!\n" ); } );

    auto slider = win->Create< GuiGL::elWidget_Slider >();
    slider->SetIsHorizontal( false );
    slider->SetPosition( { 300, 40 } );
    slider->SetSize( { 400, 300 } );
    slider->SetSliderPosition( 0.8f );

    auto slWin = scene->GUI()->Create< GuiGL::elWidget_Window >();
    slWin->SetSize( { 800, 500 } );
    slWin->SetBorderSize( { 20, 20, 20, 20 } );
    slWin->SetIsScrollbarEnabled( true, false, true, false );
    slWin->SetIsScrollbarEnabled( true, true, true, true );
    auto w1 = slWin->Create< GuiGL::elWidget_Window >();
    w1->SetPosition( { 50, 50 } );
    w1->SetPosition( { 0, 0 } );
    w1->SetSize( { 100, 100 } );
    w1->SetPositionPointOfOrigin( GuiGL::POSITION_BOTTOM_RIGHT );
    w1->SetBorderSize( { glm::vec4{ 25.0f } } );
    auto w2 = slWin->Create< GuiGL::elWidget_Window >();
    w2->SetPosition( { 400, 0 } );
    w2->SetSize( { 100, 100 } );
    w2->SetBorderSize( { glm::vec4{ 25.0f } } );

    auto dropButton = slWin->Create< GuiGL::elWidget_Dropbutton >();
    dropButton->SetText( "dropbutton" );
    dropButton->SetPosition( { 50, 50 } );
    dropButton->SetPositionPointOfOrigin( GuiGL::POSITION_BOTTOM_RIGHT );
    dropButton->AddElements( { "sel1", "asd123", "##10231", "120kpas",
                               "!@$#$^&", "zxc,qwe", "112" } );

    auto windowClass = scene->GUI()->Create< GuiGL::elWidget_Window >();
    windowClass->SetClassName( "SomeClass", true );
    windowClass->SetPosition( { 100, 100 } );

    auto listBox = scene->GUI()->Create< GuiGL::elWidget_Listbox >();
    listBox->AddElements( { "1asd", "12: fff", "1123123", "1111", "1XXX" } );
    listBox->AddElements( { "2asd", "22: fff" }, 4 );
    listBox->AddElements( { "3asd", "32: fff", "333AA", "3123123" }, 6 );
    for ( size_t i = 0; i < 50; ++i )
    {
        std::stringstream ss;
        ss << "index: " << i;
        listBox->AddElements( { ss.str() } );
    }
    listBox->RemoveElements( 4, 1 );
    listBox->SetElementSelectionCallback(
        [&]()
        {
            auto s = listBox->GetSelectedWidgets();
            for ( auto& e : s )
                printf(
                    "selected : %s\n",
                    listBox->GetElementAtIndex< GuiGL::elWidget_Button >( e )
                        ->GetText()
                        .c_str() );
        } );

    auto shaderAnimation = slWin->Create< GuiGL::elWidget_ShaderAnimation >();
    shaderAnimation->SetPosition( { 10, 10 } );
    shaderAnimation->SetSize( { 0, 0 } );

    auto checkButton = slWin->Create< GuiGL::elWidget_Checkbutton >();
    checkButton->SetPosition( { 310, 310 } );
    checkButton->SetSize( { 250, 50 } );


    auto widget = slWin->Create< GuiGL::elWidget_Window >();
    widget->SetSize( { 100, 100 } );
    widget->SetPosition( { 0, 0 } );
    widget->SetPositionPointOfOrigin( GuiGL::POSITION_CENTER );

    auto progress = slWin->Create< GuiGL::elWidget_Progress >();
    progress->SetPosition( { 300, 10 } );
    progress->SetSize( { 450, 250 } );
    progress->SetProgress( GuiGL::elWidget_Progress::PROGRESS_INDIFFERENT );
    progress->SetProgress( 0.3f );

    auto tabbedWindow = scene->GUI()->Create< GuiGL::elWidget_TabbedWindow >();
    tabbedWindow->SetPositionPointOfOrigin( GuiGL::POSITION_TOP_RIGHT );
    tabbedWindow->SetButtonAlignment( GuiGL::POSITION_RIGHT );
    tabbedWindow->SetPosition( { 10, 10 } );
    tabbedWindow->SetSize( { 450, 250 } );
    auto t1 = tabbedWindow->CreateTab( "tab1" );
    auto t2 = tabbedWindow->CreateTab( "SomeFee" );

    auto l1 = t1->window->Create< GuiGL::elWidget_Label >();
    l1->SetText( "Tab 1 Content" );
    auto b1 = t2->window->Create< GuiGL::elWidget_Button >();
    b1->SetText( "Tab1 Button" );
    b1->SetPosition( glm::vec2( 10.0f, 50.0f ) );
    auto l2 = t2->window->Create< GuiGL::elWidget_Label >();
    l2->SetText( "XXXXXTab 2 ContentXXXXXX" );

    auto callback = []() { printf( "callback\n" ); };
    auto menu     = scene->GUI()->Create< GuiGL::elWidget_Menu >();
    menu->SetPosition( { 200, 740 } );
    menu->SetSize( { 200, 100 } );

    menu->AddEntry( ",", "Aentry1" );
    menu->AddEntry( ",", "entry2" );
    menu->AddEntry( ",", "a1" );
    menu->AddEntry( ",", "a1,a2" );
    menu->AddEntry( ",", "a1,a3" );
    menu->AddEntry( ",", "b1,b2" );
    menu->AddEntry( ",", "b1,b3" );
    menu->AddEntry( ",", "c1,c2" );
    menu->AddEntry( ",", "c1,d1,d2" );
    menu->AddEntry( ",", "c1,d1,d3" );
    menu->AddEntry( ",", "c1,d1,d4" );
    menu->AddEntry( ",", "c1,c3" );
    menu->AddEntry( ",", "entry3" );
    menu->AddEntry( ",", "FuFu" );
    menu->AddEntry( ",", "FuFu,BlaFu1" );
    menu->AddEntry( ",", "FuFu,BlaFu2" );
    menu->AddEntry( ",", "FuFu,BlaFu2,Blala3" );
    menu->AddEntry( ",", "FuFu,BlaFu2,Blala3,fu1" );

    menu->SetHasVerticalAlignment( false );
    menu->GetEntryFromVector( { "Aentry1" } )->SetClickCallback( callback );
    menu->GetEntryFromVector( { "c1", "d1", "d3" } )
        ->SetClickCallback( callback );

    auto smarty = slWin->Create< GuiGL::elWidget_Label >();
    smarty->SetText( "asd" );

    auto menubar = slWin->Create< GuiGL::elWidget_Menubar >();
    menubar->SetEntries( { "entryX1",
                           "entry2",
                           { "a1", "a2", "a3" },
                           { "b1", "b2", "b3" },
                           { "cX1", "c2", { "dX1", "d2", "dX3", "d4" }, "c3" },
                           "entry3" } );
    menubar->SetButtonAlignment( GuiGL::POSITION_LEFT );
    menubar->GetEntry( ",", "entryX1" )->SetClickCallback( callback );
    menubar->GetEntry( ",", "cX1,dX1,dX3" )->SetClickCallback( callback );

    auto popupMenu = slWin->Create< GuiGL::elWidget_PopupMenu >();
    popupMenu->SetEntries(
        { "entry1",
          "entry2",
          { "a1", "a2", "a3" },
          { "b1", "b2", "b3" },
          { "c1", "c2", { "d1", "d2", "d3", "d4" }, "c3" } } );
    popupMenu->SetPosition( { 200, 50 } );

    popupMenu->GetEntryFromVector( { "entry1" } )->SetClickCallback( callback );
    popupMenu->GetEntryFromVector( { "c1", "d1", "d3" } )
        ->SetClickCallback( callback );

    auto infoBox = scene->GUI()->Create< GuiGL::elWidget_InfoMessage >();
    infoBox->SetText( { "hello there", "how are you today", "blalallalala!" } );

    auto yesNo = scene->GUI()->Create< GuiGL::elWidget_YesNoDialog >();
    yesNo->SetText( { "Do you really care?", "Really?" } );

    auto treeBox = scene->GUI()->Create< GuiGL::elWidget_Tree >();
    treeBox->AddEntries( {}, { "asdasd", "fasd", "faux" } );
    treeBox->AddEntries( { "asdasd" }, { "a1", "a2", "a3" } );
    treeBox->AddEntries( { "fasd" }, { "casa1", "sdaa2", "daa3" } );
    treeBox->AddEntries( { "fasd", "casa1" }, { "casa1", "sdaa2", "daa3" } );
    treeBox->AddEntries( { "asdasd", "a3" }, { "fuu", "a1", "a2", "a3" } );

    auto fileBrowser = scene->GUI()->Create< GuiGL::elWidget_FileBrowser >();
    fileBrowser->SetSize( { 1000, 600 } );
    fileBrowser->SetPosition( { 0, 0 } );
    fileBrowser->SetOpenCallback(
        [&]()
        {
            auto v = fileBrowser->GetSelection();
            for ( auto e : v )
                printf( "%s\n", e.c_str() );
        } );


    auto graph = scene->GUI()->Create< GuiGL::elWidget_Graph >();
    graph->SetSize( { 1200, 200 } );
    graph->SetUpGraphShader( "glsl/gui/shaderTexture_Graph.glsl" );


    graph->SetGraphDataCallback(
        [&]
        {
            std::vector< float > data;
            float                runtime = static_cast< float >(
                instance.Window()->GetRuntime().GetSeconds() );
            for ( float i = 0.0f; i < 9.0f; i = i + 0.1f )
            {
                float fourier = cosf( i + 0.4f * runtime ) +
                                0.3f * sinf( 2.5f * i + runtime ) +
                                0.2f * sinf( 4.0f * i + 3.2f * runtime ) +
                                0.1f * cosf( 8.2f * i - 0.9f * runtime );
                data.emplace_back( fourier *
                                       ( ( i < 8.0f ) ? i * 0.05f : 0.5f ) +
                                   ( ( i < 8.0f ) ? -0.5f : 0.5f ) );
            }
            graph->SetGraphData( data, glm::vec2{ -2.0f, 2.0f } );
        } );


    auto stickyCenter = scene->GUI()->Create< GuiGL::elWidget_Window >();
    stickyCenter->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT, false );
    auto stickyTop = scene->GUI()->Create< GuiGL::elWidget_Window >();
    stickyTop->SetSize( { 50, 50 } );
    stickyTop->SetStickToWidget(
        stickyCenter.Get(), GuiGL::verticalPosition_t::VPOSITION_TOP,
        GuiGL::horizontalPosition_t::HPOSITION_CENTER );

    auto stickyBottom = scene->GUI()->Create< GuiGL::elWidget_Window >();
    stickyBottom->SetSize( { 50, 50 } );
    stickyBottom->SetStickToWidget(
        stickyCenter.Get(), GuiGL::verticalPosition_t::VPOSITION_BOTTOM,
        GuiGL::horizontalPosition_t::HPOSITION_CENTER );

    auto stickyLeft = scene->GUI()->Create< GuiGL::elWidget_Window >();
    stickyLeft->SetSize( { 50, 50 } );
    stickyLeft->SetStickToWidget( stickyCenter.Get(),
                                  GuiGL::verticalPosition_t::VPOSITION_CENTER,
                                  GuiGL::horizontalPosition_t::HPOSITION_LEFT );

    auto stickyRight = scene->GUI()->Create< GuiGL::elWidget_Window >();
    stickyRight->SetSize( { 50, 50 } );
    stickyRight->SetStickToWidget(
        stickyCenter.Get(), GuiGL::verticalPosition_t::VPOSITION_CENTER,
        GuiGL::horizontalPosition_t::HPOSITION_RIGHT );

    auto listBoxSi = scene->GUI()->Create< GuiGL::elWidget_Listbox >();

    for ( uint64_t i = 0; i < 20; ++i )
        listBoxSi->AddElement( std::to_string( i ) + "easd", i );
    listBoxSi->Reshape();

    auto textBox = scene->GUI()->Create< GuiGL::elWidget_Textbox >();
    textBox->SetText( { "hello", "world", "you little foo!" } );
    textBox->InsertLine( "its the end" );
    textBox->InsertLine( "first line!", 0 );
    textBox->InsertLine( "", 2 );
    textBox->InsertLine( "second line!", 1 );
    for ( size_t i = 0; i < 40; ++i )
        textBox->InsertLine( "its the end" + std::to_string( i * 2 ) );
    textBox->SetIsEditable( true );

    textBox->ModifyLine( "XXX\nXX", 2 );

    scene->SetupFreeFlyCamera();
    instance.StartMainLoop();

    return 0;
}
