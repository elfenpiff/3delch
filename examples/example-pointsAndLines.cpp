#include "3Delch/3Delch.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "world/elGeometricModel.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

using namespace ::el3D;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();
    scene->GUI()->SetShowMouseCursor( false );

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    class MyWorld : public world::elWorld
    {
      public:
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky = this->Create< world::elSky >(
                world::ColoredSkybox, glm::vec4( 0.75, 0.75, 0.75, 1.0 ) );
            auto sphere = HighGL::elObjectGeometryBuilder::CreateSphere( 25 )
                              .GetObjectGeometry();

            this->points = this->Create< world::elGeometricModel >(
                world::geometricModelProperties_t{
                    .vertices       = sphere.vertices,
                    .colorPerVertex = std::vector< GLfloat >(),
                    .position       = { 4.0, 0.0, 2.0 },
                    .uniformColor   = { 0xff, 0x00, 0x00, 0xff },
                    .drawSize       = 4.0f,
                    .type           = world::GeometricType::Points,
                } );

            this->lines = this->Create< world::elGeometricModel >(
                world::geometricModelProperties_t{
                    .vertices       = sphere.vertices,
                    .colorPerVertex = std::vector< GLfloat >(),
                    .position       = { 4.0, 0.0, -2.0 },
                    .uniformColor   = { 0x00, 0xff, 0x00, 0xff },
                    .drawSize       = 4.0f,
                    .type           = world::GeometricType::LineStrip,
                } );
        }

        bb::product_ptr< world::elSky >            sky;
        bb::product_ptr< world::elGeometricModel > points;
        bb::product_ptr< world::elGeometricModel > lines;

    } myWorld( *scene );

    scene->SetupFreeFlyCamera();

    instance.StartMainLoop();

    return 0;
}
