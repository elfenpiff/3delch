#include "logging/elLog.hpp"

#include <chrono>

using namespace el3D;

std::atomic< bool > doStop{false};
void
f()
{
    while ( !doStop.load() )
        LOG_WARN( 0 ) << "asas";
}

void
Callback( const std::string &channelName, const logging::logLevel_t,
          const std::string &, const std::string &, const std::string &,
          const std::string &msg ) noexcept
{
    std::cout << "Hel.lo from callback " << channelName << " " << msg
              << std::endl;
}

int
main()
{
    // logging::elLog::EnableOutputRedirection();

    logging::output::ToTerminal< logging::threading::InSeparateThread > l1;
    l1.SetFormatting( logging::format::DetailledColored );
    l1.SetLogLevel( logging::DEBUG );

    logging::output::ToFile< logging::threading::InSeparateThread > l2(
        "logfile.log" );
    l2.SetLogLevel( logging::DEBUG );

    logging::output::ToTerminal< logging::threading::InSeparateThread > l3;
    l3.SetFormatting( logging::format::ShortColored );
    l3.SetLogLevel( logging::DEBUG );

    logging::elLog::CreateChannel( "funky" );
    logging::elLog::AddOutput( 1, &l3 );

    logging::elLog::AddCallback( 0, Callback );
    logging::elLog::AddOutput( 0, &l1 );
    logging::elLog::AddOutput( 0, &l2 );

    std::vector< std::thread > t;
    int                        l = 2;

    for ( int i = 0; i < l; i++ )
        t.emplace_back( f );

    std::chrono::microseconds timeSpan( 1 );
    std::this_thread::sleep_for( timeSpan );

    doStop = true;
    for ( auto &a : t )
        a.join();


    LOG_WARN( 0 ) << "hello world";
    LOG_FATAL( 0 ) << "upsoi";
    LOG_INFO( 1 ) << "funky channel output";
    std::clog << "fuu Hello WUU" << std::endl;
    std::cout << "fuu Hello WUU" << std::endl;
    std::cerr << "fuu Hello WUU" << std::endl;

    printf( "hello world" );

    LOG_FATAL( 0 ) << "last print before exit";
    exit( 0 );
}
