#include "3Delch/3Delch.hpp"
#include "3Delch/elIntegratedConsole.hpp"
#include "3Delch/elScene.hpp"
#include "GLAPI/GLAPI.hpp"
#include "GuiGL/GuiGL.hpp"
#include "HighGL/Conversion.hpp"
#include "HighGL/HighGL.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/OpenGL.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "OpenGL/elGeometricObject_NonVertexObject.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/RenderPipeline.hpp"
#include "lua/elLuaScript.hpp"
#include "lua/lua.hpp"
#include "math/elPerlinNoise.hpp"
#include "units/Velocity.hpp"
#include "utils/utils.hpp"
#include "world/elBoundingBox.hpp"
#include "world/elInteractiveMesh.hpp"
#include "world/elInteractiveSet.hpp"
#include "world/elModel.hpp"
#include "world/elModelEffect.hpp"
#include "world/elModelGroup.hpp"
#include "world/elSky.hpp"
#include "world/elSun.hpp"
#include "world/elTerrain.hpp"
#include "world/elVehicle.hpp"
#include "world/elWorld.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

#include <glm/gtx/rotate_vector.hpp>
using namespace ::el3D;
using namespace ::el3D::animation;

int
main()
{
    auto &instance = _3Delch::Init( "cfg/globalConfig.lua" );
    auto &scene    = instance.GetSceneManager().CreateScene( "main" ).value();

    auto exitEvent = _3Delch::CreateAndRegisterKeyPressEvent(
        GLAPI::KeyState::DOWN, SDLK_ESCAPE,
        [&] { instance.StopMainLoopAndDestroy(); } );

    struct MyWorld : public world::elWorld
    {
        MyWorld( _3Delch::elScene &scene ) : world::elWorld( scene )
        {
            this->sky =
                this->Create< world::elSky >( world::TexturedSkyboxFromConfig );
            this->sun = this->Create< world::elSun >( world::SunProperties{} );
            this->sun->SetPosition( units::Angle::Degree( 45.0 ),
                                    units::Angle::Degree( -45.0 ) );

            this->ground = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateCube()
                    .GetObjectGeometry() );
            constexpr float WORLD_SIZE = 250.0f;
            this->ground->SetScaling( { WORLD_SIZE, 1.0, WORLD_SIZE } );
            this->ground->SetPosition( { 0.0, -5.0, 0.0 } );
            this->ground->SetDiffuseColor( { 0x88, 0x88, 0x88, 0xff } );

            this->ground->SetPhysicalProperties< btBoxShape >(
                world::physicalProperties_t{ .mass = 0.0f },
                btVector3( WORLD_SIZE, 1.0, WORLD_SIZE ) );

            auto rover = this->Create< world::elModelGroup >(
                "data/MarsRover-Mesh_v1.0.obj", 1.0f );

            // for ( uint64_t i = 0; i < rover->models.size(); )
            //{
            //    auto name = rover->models[i]->GetName();
            //    if ( name.substr( 0, 6 ) == "Mantel" ||
            //         name.substr( 0, 5 ) == "Felge" )
            //    {
            //        rover->models.erase( rover->models.begin() +
            //                             static_cast< int64_t >( i ) );
            //    }
            //    else
            //        ++i;
            //}

            rover->SetDiffuseColor( { 0xaa, 0xaa, 0xaa, 0xff } );

            auto wheel =
                this->Create< world::elModelGroup >( "data/wheel.obj" );

            // rim
            wheel->models[0]->SetDiffuseColor( { 0xbb, 0xbb, 0xbb, 0xff } );
            wheel->models[0]->SetSpecularColor( { 0xff, 0xff, 0xff }, 0xff );

            // wheel
            wheel->models[1]->SetDiffuseColor( { 0x33, 0x33, 0x33, 0xff } );


            world::vehicleProperties_t vehicleProperties;
            vehicleProperties.vehicleBodyModel = std::move( rover );

            vehicleProperties.wheels.emplace_back( world::wheelProperties_t{
                .orientation      = { 0.0f, 0.0f, -1.0f, M_PI_2f32 },
                .maxSteeringValue = 0.6f,
                .relativePosition = { 1.0f, 1.0f, 1.0f },
                .isFrontWheel     = true,
                .wheelModel       = wheel->Clone() } );
            vehicleProperties.wheels.emplace_back( world::wheelProperties_t{
                .orientation      = { 0.0f, 0.0f, 1.0f, M_PI_2f32 },
                .maxSteeringValue = 0.6f,
                .relativePosition = { -1.0f, 1.0f, 1.0f },
                .isFrontWheel     = true,
                .wheelModel       = wheel->Clone() } );
            vehicleProperties.wheels.emplace_back( world::wheelProperties_t{
                .orientation      = { 0.0f, 0.0f, -1.0f, M_PI_2f32 },
                .maxSteeringValue = 0.3f,
                .relativePosition = { 1.0f, 1.0f, -1.0f },
                .isFrontWheel     = false,
                .wheelModel       = wheel->Clone() } );
            vehicleProperties.wheels.emplace_back( world::wheelProperties_t{
                .orientation      = { 0.0f, 0.0f, 1.0f, M_PI_2f32 },
                .maxSteeringValue = 0.6f,
                .relativePosition = { -1.0f, 1.0f, -1.0f },
                .isFrontWheel     = false,
                .wheelModel       = std::move( wheel ) } );

            this->vehicle = this->Create< world::elVehicle >(
                std::move( vehicleProperties ) );

            this->vehicleKeyboardEvent = _3Delch::CreateEvent();
            this->vehicleKeyboardEvent->SetCallback(
                [this]( const SDL_Event e )
                {
                    if ( e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_UP )
                    {
                        this->vehicle->Forward();
                    }
                    else if ( e.type == SDL_KEYUP &&
                              e.key.keysym.sym == SDLK_UP )
                    {
                        this->vehicle->RollOut();
                    }
                    else if ( e.type == SDL_KEYDOWN &&
                              e.key.keysym.sym == SDLK_DOWN )
                    {
                        this->vehicle->Backward();
                    }
                    else if ( e.type == SDL_KEYUP &&
                              e.key.keysym.sym == SDLK_DOWN )
                    {
                        this->vehicle->RollOut();
                    }
                    else if ( e.type == SDL_KEYDOWN &&
                              e.key.keysym.sym == SDLK_LEFT )
                    {
                        this->vehicle->SteerToLeft();
                    }
                    else if ( e.type == SDL_KEYUP &&
                              e.key.keysym.sym == SDLK_LEFT )
                    {
                        this->vehicle->SteerToCenter();
                    }
                    else if ( e.type == SDL_KEYDOWN &&
                              e.key.keysym.sym == SDLK_RIGHT )
                    {
                        this->vehicle->SteerToRight();
                    }
                    else if ( e.type == SDL_KEYUP &&
                              e.key.keysym.sym == SDLK_RIGHT )
                    {
                        this->vehicle->SteerToCenter();
                    }
                    else if ( e.type == SDL_KEYDOWN &&
                              e.key.keysym.sym == SDLK_SPACE )
                    {
                        this->vehicle->Brake();
                    }
                    else if ( e.type == SDL_KEYUP &&
                              e.key.keysym.sym == SDLK_SPACE )
                    {
                        this->vehicle->ReleaseBrake();
                    }
                    else if ( e.type == SDL_KEYDOWN &&
                              e.key.keysym.sym == SDLK_x )
                    {
                        this->bullets.emplace_back(
                            this->templateBullet->Clone() );
                        this->bullets.back()->SetPosition(
                            { 55.0f, 100.0f, 55.0f } );
                        this->bullets.back()->SetScaling(
                            { 1.0f, 1.0f, 1.0f } );
                        this->bullets.back()
                            ->SetPhysicalProperties< btSphereShape >(
                                world::physicalProperties_t{ .mass = 10000.0f },
                                1.0f );
                    }
                } );
            this->vehicleKeyboardEvent->Register();

            this->templateBullet = this->Create< world::elModel >(
                HighGL::elObjectGeometryBuilder::CreateSphere( 25 )
                    .GetObjectGeometry() );
            this->templateBullet->SetPosition( { 55.0f, 100.0f, 55.0f } );


            auto createStone = [&]( glm::vec3 p )
            {
                constexpr glm::vec3 STONE_DIM = glm::vec3{ 1.25f, 2.5f, 1.25f };
                this->stones.emplace_back( this->Create< world::elModel >(
                    HighGL::elObjectGeometryBuilder::CreateCube()
                        .GetObjectGeometry() ) );
                this->stones.back()->SetPosition( p );
                this->stones.back()->SetScaling( STONE_DIM );
                this->stones.back()->SetDiffuseColor(
                    { 0xcc, 0xdd, 0xcc, 0xff } );
                this->stones.back()->SetPhysicalProperties< btBoxShape >(
                    world::physicalProperties_t{ .mass = 100.0f },
                    btVector3( STONE_DIM.x, STONE_DIM.y, STONE_DIM.z ) );
            };

            auto createPlate = [&]( glm::vec3 p )
            {
                constexpr glm::vec3 STONE_DIM = glm::vec3{ 5.0f, 0.25f, 5.0f };
                this->stones.emplace_back( this->Create< world::elModel >(
                    HighGL::elObjectGeometryBuilder::CreateCube()
                        .GetObjectGeometry() ) );
                this->stones.back()->SetPosition( p );
                this->stones.back()->SetScaling( STONE_DIM );
                this->stones.back()->SetDiffuseColor(
                    { 0x88, 0x88, 0x88, 0xff } );
                this->stones.back()->SetPhysicalProperties< btBoxShape >(
                    world::physicalProperties_t{ .mass = 1000.0f },
                    btVector3( STONE_DIM.x, STONE_DIM.y, STONE_DIM.z ) );
            };

            for ( float yOffset = 0.0f; yOffset < 100.0f; yOffset += 13.0f )
            {
                for ( float xOffset = 0.0f; xOffset < 100.0f; xOffset += 13.0f )
                {
                    for ( float offset = 0.0f;
                          offset < 20.0f + ( xOffset + yOffset ) * 0.1;
                          offset += 5.25f )
                    {
                        createStone(
                            { 5.0 + xOffset, -5.0 + offset, 25.0 + yOffset } );
                        createStone(
                            { 5.0 + xOffset, -5.0 + offset, 30.0 + yOffset } );
                        createStone(
                            { 10.0 + xOffset, -5.0 + offset, 30.0 + yOffset } );
                        createStone(
                            { 10.0 + xOffset, -5.0 + offset, 25.0 + yOffset } );
                        createPlate( { 7.5 + xOffset, -2.5f + offset,
                                       27.5f + yOffset } );
                    }
                }
            }
        }

        bb::product_ptr< world::elModel >                ground;
        bb::product_ptr< world::elSky >                  sky;
        bb::product_ptr< world::elSun >                  sun;
        bb::product_ptr< world::elVehicle >              vehicle;
        std::vector< bb::product_ptr< world::elModel > > stones;
        std::vector< bb::product_ptr< world::elModel > > bullets;
        bb::product_ptr< world::elModel >                templateBullet;

        bb::product_ptr< GLAPI::elEvent > vehicleKeyboardEvent;
    };

    MyWorld world( *scene );

    world.vehicle->AttachCamera( scene->GetCamera(), { 0.0f, 5.5f, -16.0f },
                                 { 0.0f, -0.3f, 1.0f } );
    instance.StartMainLoop();

    return 0;
}
