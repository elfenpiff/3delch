this:RequireClass("_3Delch")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("GuiGL")
this:RequireNamespace("GLAPI")


this:RequireGlobal("sdlPixelFormat")

function AddMenuEntries(menu)
    menu:AddEntry(".", "menu1.submenu1.subsubmenu1")
    menu:AddEntry(".", "menu1.submenu1.subsubmenu2")
    menu:AddEntry(".", "menu1.submenu1.subsubmenu3")
    menu:AddEntry(".", "menu1.submenu1.subsubmenu4")
    menu:AddEntry(".", "menu1.submenu2")
    menu:AddEntry(".", "menu1.submenu3")
    menu:AddEntry(".", "menu1.submenu4.subsubmenu1")
    menu:AddEntry(".", "menu1.submenu4.subsubmenu2")
    menu:AddEntry(".", "menu2.submenu1")
    menu:AddEntry(".", "menu2.submenu2")
    menu:AddEntry(".", "menu3.submenu2")
    menu:AddEntry(".", "menu4")
end

function AddGraphData()
    graph:ResetGraphData()
    local runtime = _3Delch.GetInstance():Window():GetRuntime()
    for i = 0,10,0.005
    do
        graph:AddGraphDataValue(math.sin(i + runtime) + 
            0.1 * math.sin(8.2 * i + 3 * runtime) * math.sin(3.6 * i) +
            0.2 * math.sin(11.4 * i + 4 * runtime) * math.sin(2.2 * i)+
            0.3 * math.sin(4.2 * i + 2 * runtime) * math.sin(8.1 * i) +
            0.4 * math.sin(0.4 * i + 0.5 * runtime) * math.sin(12 * i ) +
            0.11 * math.sin(22 * i + 7 * runtime)
            )
    end
    graph:SetGraphDataYDrawRange(glm_vec2.new(-2.2, 2.2))
    graph:UpdateGraphData()
end

function CreateGUIExample(baseGUI)
    themeWindow = baseGUI:Create_TabbedWindow("")
    themeWindow:SetButtonAlignment(POSITION_TOP)
    themeWindow:SetSize(glm_vec2.new(1400, 800))
    themeWindow:SetPosition(glm_vec2.new(100, 100))
    themeWindow:CreateTabReturnID("Button")
    themeWindow:CreateTabReturnID("DataHandling")
    themeWindow:CreateTabReturnID("Menu")
    themeWindow:CreateTabReturnID("Window")
    themeWindow:CreateTabReturnID("Messages")
    themeWindow:CreateTabReturnID("Editing")
    themeWindow:CreateTabReturnID("Utils")
    themeWindow:SetHasTitlebar(true)
    local titlebar = themeWindow:GetTitlebar()
    titlebar:SetHasCloseButton(true)
    titlebar:SetTitleText("gui example")
    themeWindow:Reshape()

    local themeWindow_Button            = themeWindow:GetTabWindow(0)
    local themeWindow_DataHandling      = themeWindow:GetTabWindow(1)
    local themeWindow_Menu              = themeWindow:GetTabWindow(2)
    local themeWindow_Window            = themeWindow:GetTabWindow(3)
    local themeWindow_Messages          = themeWindow:GetTabWindow(4)
    local themeWindow_Editing           = themeWindow:GetTabWindow(5)
    local themeWindow_Utils             = themeWindow:GetTabWindow(6)


    -- windows
    local window = themeWindow_Window:Create_Window("")
    window:Reshape()
    local label = window:Create_Label("")
    label:SetText("window")
    window:SetSize(glm_vec2.new(150, 150))
    window:SetPosition(glm_vec2.new(20, 20))

    window = window:Create_Window("")
    window:SetSize(glm_vec2.new(100, 50))
    window:SetPosition(glm_vec2.new(20, 20))
    label = window:Create_Label("")
    label:SetText("miniwindow")

    window = themeWindow_Window:Create_Window("")
    label = window:Create_Label("")
    label:SetText("inactive")
    window:SetSize(glm_vec2.new(150, 150))
    window:SetPosition(glm_vec2.new(200, 20))
    window:SetState(STATE_INACTIVE)

    -- data collections
    local listbox = themeWindow_DataHandling:Create_Listbox("")
    listbox:SetSize(glm_vec2.new(200, 300))
    listbox:SetPosition(glm_vec2.new(20, 20))

    for i = 0,20,1
    do
        listbox:AddElement("listbox item 1" .. i, 0)
    end

    local table = themeWindow_DataHandling:Create_Table("")
    table:SetElementBaseType(TYPE_BUTTON)
    table:SetHoveringMode(TABLEMODE_ROW)
    table:SetSize(glm_vec2.new(300, 300))
    table:SetPosition(glm_vec2.new(250, 20))
    table:SetHasTitleRow(true)
    table:ResetNumberOfColsAndRows(3, 3)
    table:SetTitleRowButtonText(0, "row 0")
    table:SetTitleRowButtonText(1, "row 1")
    table:SetTitleRowButtonText(2, "row 2")
    for i = 0,2,1
    do
        for k = 0,2,1
        do
            table:GetElement_Button(i, k):SetText("fuu " .. i .. ", " .. k)
        end
    end
    table:Reshape()

    local tree = themeWindow_DataHandling:Create_Tree("")
    tree:SetPosition(glm_vec2.new(20, 350))
    tree:AddEntries(".", "", "bla.fuu.maeh")
    tree:AddEntries(".", "bla", "bla.fuu.maeh")
    tree:AddEntries(".", "bla.fuu", "bla.fuu.maeh")
    tree:AddEntries(".", "maeh", "bla.fuu.maeh")
    tree:GetEntry(".", "bla"):SetText("Xjaaj")

    graph = themeWindow_DataHandling:Create_Graph("")
    graph:SetSize(glm_vec2.new(750, 100))
    graph:SetPosition(glm_vec2.new(600, 10))
    graph:SetUpGraphShader("glsl/gui/shaderTexture_Graph.glsl", "default")
    graph:SetGraphDataCallback(this:CreateCallbackToFunction("AddGraphData"))

    -- buttons
    local button = themeWindow_Button:Create_Button("")
    button:SetText("button")
    button:SetPosition(glm_vec2.new(50, 50))

    local dropButton = themeWindow_Button:Create_Dropbutton("")
    dropButton:SetPosition(glm_vec2.new(50, 150))
    dropButton:SetText("dropButton")
    for i = 0,8,1
    do
        dropButton:AddElement("item " .. i, 0)
    end

    local checkButton = themeWindow_Button:Create_Checkbutton("") 
    checkButton:SetPosition(glm_vec2.new(300, 50))

    local slider = themeWindow_Button:Create_Slider("")
    slider:SetPosition(glm_vec2.new(300, 150))

    -- messages
    local infoMessage = themeWindow_Messages:Create_InfoMessage("")
    infoMessage:InsertLine("hello info message longy long klong bong", 0)
    infoMessage:InsertLine("11hello info", 1)
    infoMessage:InsertLine("bla", 2)
    infoMessage:SetSize(glm_vec2.new(300, 150))
    infoMessage:SetPosition(glm_vec2.new(20, 20))
    
    local yesNoDialog = themeWindow_Messages:Create_YesNoDialog("")
    yesNoDialog:InsertLine("hello yesno dialog", 0)
    yesNoDialog:InsertLine("11hello info", 1)
    yesNoDialog:InsertLine("X", 2)
    yesNoDialog:SetSize(glm_vec2.new(300, 150))
    yesNoDialog:SetPosition(glm_vec2.new(400, 20))

    -- menu
    local popupMenu = themeWindow_Menu:Create_PopupMenu("")
    popupMenu:AddEntry(".", "popupMenu.bla")
    AddMenuEntries(popupMenu)

    local menu = themeWindow_Menu:Create_Menu("")
    menu:SetPosition(glm_vec2.new(20, 60))
    AddMenuEntries(menu)

    local menubar = themeWindow_Menu:Create_Menubar("")
    menubar:AddEntry(".", "menubar.blala")
    AddMenuEntries(menubar)

    -- editing
    local textbox = themeWindow_Editing:Create_Textbox("")
    textbox:SetPosition(glm_vec2.new(20, 20))
    textbox:SetSize(glm_vec2.new(300, 500))
    textbox:SetIsEditable(true)
    for i = 0, 35, 1
    do
        textbox:InsertLine("Hello World >> " .. i .. " << bla fuh long text, lalalallalala XXX 123", 0)
    end

    local entry = themeWindow_Editing:Create_Entry("")
    entry:SetPosition(glm_vec2.new(350, 20))
    entry:SetSize(glm_vec2.new(200, 30))
    entry:SetText("i am an entry .... i veeeerry long entry ... its a meee entryman")
    
    -- utils
    local fileBrowser = themeWindow_Utils:Create_FileBrowser("")
    fileBrowser:SetPosition(glm_vec2.new(10, 400))
    fileBrowser:SetSize(glm_vec2.new(900, 500))

    local progress = themeWindow_Utils:Create_Progress("")
    progress:SetPosition(glm_vec2.new(10, 10))
    progress:SetSize(glm_vec2.new(300, 24))
    progress:SetProgress(0.4)

    local image = themeWindow_Utils:Create_Image("")
    image:SetPosition(glm_vec2.new(10, 50))
    image:SetSize(glm_vec2.new(200, 200))

    local imageData = elImage.CreateFromFile("data/dog.jpg", SDL_PIXELFORMAT_RGBA32)
    if not imageData:has_value() then
        print ("image not found")
    else
        image:SetImage(imageData:get())
    end

    local shaderAnimation = themeWindow_Utils:Create_ShaderAnimation("")
    shaderAnimation:SetPosition(glm_vec2.new(350, 10))
    shaderAnimation:SetSize(glm_vec2.new(100, 100))
    shaderAnimation:SetUp("glsl/gui/shaderTexture_Example.glsl", "default", true, 1, 512, 512, FALSE)
end
