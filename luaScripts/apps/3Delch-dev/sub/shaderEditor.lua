this:RequireClass("_3Delch")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("GuiGL")
this:RequireNamespace("GLAPI")
this:RequireNamespace("utils")
this:RequireNamespace("stl")
this:RequireNamespace("lua")
this:RequireNamespace("luaBindings")

function CreateShaderEditor(baseGUI)
    local defaultShader = "glsl/gui/shaderTexture_Example.glsl"

    window = baseGUI:Create_Window("")
    window:SetSize(glm_vec2.new(1300, 700))
    window:SetPosition(glm_vec2.new(100, 100))
    window:SetHasTitlebar(true)
    local titlebar = window:GetTitlebar()
    titlebar:SetHasCloseButton(true)
    titlebar:SetTitleText("shader editor")

    local code = window:Create_Textbox("")
    code:SetPosition(glm_vec2.new(0, 0))
    code:SetIsEditable(true)
    code:SetSize(glm_vec2.new(700, 1))
    code:SetStickToParentSize(glm_vec2.new(-1, 0))

    local file = elFile.new(defaultShader, false) 
    file:Read()
    local fileContent = file:GetContentAsVector()
    for i = 0, fileContent:size() - 1, 1
    do
        code:InsertLine(fileContent:at(i), i)
    end    

    local shaderAnimation = window:Create_ShaderAnimation("")
    shaderAnimation:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    shaderAnimation:SetPosition(glm_vec2.new(0, 0))
    shaderAnimation:SetStickToParentSize(glm_vec2.new(700, 0))
    shaderAnimation:SetUp(defaultShader, "default", true, 1, 512, 512, FALSE)
end

