this:RequireClass("_3Delch")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("GuiGL")
this:RequireNamespace("GLAPI")
this:RequireNamespace("utils")
this:RequireNamespace("OpenGL")
this:RequireNamespace("lua")

function TextureTargetToString(target)
   if target == GL_TEXTURE_1D then
       return "GL_TEXTURE_1D"
   elseif target == GL_TEXTURE_2D then
       return "GL_TEXTURE_2D"
   elseif target == GL_TEXTURE_3D then
       return "GL_TEXTURE_3D"
   elseif target == GL_TEXTURE_CUBE_MAP then
       return "GL_TEXTURE_CUBE_MAP"
   elseif target == GL_TEXTURE_RECTANGLE then
       return "GL_TEXTURE_RECTANGLE"
   elseif target == GL_TEXTURE_BUFFER then
       return "GL_TEXTURE_BUFFER"
   elseif target == GL_TEXTURE_1D_ARRAY then
       return "GL_TEXTURE_1D_ARRAY"
   elseif target == GL_TEXTURE_2D_ARRAY then
       return "GL_TEXTURE_2D_ARRAY"
   elseif target == GL_TEXTURE_CUBE_MAP_ARRAY then
       return "GL_TEXTURE_CUBE_MAP_ARRAY"
   elseif target == GL_TEXTURE_2D_MULTISAMPLE then
       return "GL_TEXTURE_2D_MULTISAMPLE"
   elseif target == GL_TEXTURE_2D_MULTISAMPLE_ARRAY then
       return "GL_TEXTURE_2D_MULTISAMPLE_ARRAY"
   else 
       return "UNKNOWN"
   end
end

function SelectTexture()
    local selection = textureTable:GetSelection()
    if not selection:empty() then
        local entry = selection:at(0):Get_y()
        local bindOnlyTexture = elTexture_base.new(textureList:at(entry):Get_texture(), textureList:at(entry):Get_target())
        preview:SetImageFromTexture(bindOnlyTexture:ThisPointer())
        textureInfoTarget:SetText(TextureTargetToString(bindOnlyTexture:GetTarget()))
    end
end

function CreateTextureBrowser(baseGUI)
    window = baseGUI:Create_Window("")
    window:SetSize(glm_vec2.new(1300, 700))
    window:SetPosition(glm_vec2.new(100, 100))
    window:SetHasTitlebar(true)
    local titlebar = window:GetTitlebar()
    titlebar:SetHasCloseButton(true)
    titlebar:SetTitleText("texture browser")

    textureTable = window:Create_Table("")
    textureTable:SetElementBaseType(TYPE_BUTTON)
    textureTable:SetHoveringMode(TABLEMODE_ROW)
    textureTable:SetSelectionMode(TABLEMODE_ROW)
    textureTable:SetPosition(glm_vec2.new(0, 0))
    textureTable:SetSize(glm_vec2.new(300, 1))
    textureTable:SetStickToParentSize(glm_vec2.new(-1, 0))

    local registry = elObjectRegistry.GetInstance()
    textureList = registry:GetRegisteredTextures()
    textureTable:ResetNumberOfColsAndRows(2, textureList:size())

    for i = 0, textureList:size() - 1, 1
    do
        local texture = textureList:at(i)
        textureTable:GetElement_Button(0, i):SetText(texture:Get_texture())
        textureTable:GetElement_Button(1, i):SetText(texture:Get_identifier())
        textureTable:SetRowHeight(i, 24)
    end
    textureTable:SetColumnWidth(0, 50)
    textureTable:SetColumnWidth(1, 248)
    textureTable:Reshape()
    textureTable:SetOnSelectionCallback(this:CreateCallbackToFunction("SelectTexture"))

    preview = window:Create_Image("")
    preview:SetIsMovable(false)
    preview:SetPositionPointOfOrigin(POSITION_TOP_RIGHT, false)
    preview:SetPosition(glm_vec2.new(0, 0))
    preview:SetStickToParentSize(glm_vec2.new(300, 0))
    preview:SetDoInvertTextureCoordinates(true)

    previewInfo = preview:Create_Base("")
    previewInfo:SetStickToParentSize(glm_vec2.new(0, -1))
    previewInfo:SetSize(glm_vec2.new(0, 30))
    previewInfo:SetIsMovable(false)
    previewInfo:SetIsResizable(false)
    previewInfo:SetBaseColorForState(glm_vec4.new( 0, 0, 0, 0.6), STATE_DEFAULT )
    textureInfoTarget = previewInfo:Create_Label("")
    textureInfoTarget:SetPosition(glm_vec2.new(5, 5))
end
