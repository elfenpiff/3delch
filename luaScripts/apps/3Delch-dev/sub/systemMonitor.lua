this:RequireClass("_3Delch")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("GuiGL")
this:RequireNamespace("GLAPI")
this:RequireNamespace("utils")
this:RequireNamespace("OpenGL")
this:RequireNamespace("lua")

function UpdateValues()
    fpsValueLabel:SetText(_3Delch.GetInstance():Window():GetFramesPerSecond())
end

function RemoveUpdateValuesCallback()
    _3Delch.GetInstance():RemoveMainLoopCallback(callbackID)
end

function CreateSystemMonitor(baseGUI) 
    window = baseGUI:Create_Window("")
    window:SetSize(glm_vec2.new(700, 700))
    window:SetHasTitlebar(true)
    window:GetTitlebar():SetTitleText("system monitor")
    window:GetTitlebar():SetHasCloseButton(true)
    window:SetPosition(glm_vec2.new(100, 100))

    local fpsLabel = window:Create_Label("")
    fpsLabel:SetText("FPS : ")
    fpsLabel:SetPosition(glm_vec2.new(10, 10))

    fpsValueLabel = window:Create_Label("")
    fpsValueLabel:SetPosition(glm_vec2.new(100, 10))

    callbackID = _3Delch.GetInstance():AddMainLoopCallback(this:CreateCallbackToFunction("UpdateValues"))
    window:SetOnCloseCallback(this:CreateCallbackToFunction("RemoveUpdateValuesCallback"))
end
