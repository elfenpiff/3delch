this:RequireNamespace("_3Delch")
this:RequireNamespace("GuiGL")
this:RequireNamespace("RenderPipeline")
this:RequireNamespace("utils")
this:RequireNamespace("lua")

require "luaScripts/apps/3Delch-dev/sub/textureBrowser"
require "luaScripts/apps/3Delch-dev/sub/shaderEditor"
require "luaScripts/apps/3Delch-dev/sub/guiExample"
require "luaScripts/apps/3Delch-dev/sub/systemMonitor"

function Exit3DElch()
    _3Delch.GetInstance():StopMainLoopAndDestroy()
end

function GuiExample()
    CreateGUIExample(gui)
end

function ShaderEditor()
    CreateShaderEditor(gui)
end

function TextureBrowser()
    CreateTextureBrowser(gui)
end

function SystemMonitor()
    CreateSystemMonitor(gui)
end

function CreateMainMenu(baseGUI)
    gui = baseGUI 

    local menubar = gui:Create_Menubar("")
    menubar:AddEntry(".", "main.gui example")
    menubar:AddEntry(".", "main.shader editor")
    menubar:AddEntry(".", "main.exit")

    menubar:AddEntry(".", "debug.system monitor")
    menubar:AddEntry(".", "debug.texture browser")

    menubar:GetEntry(".", "main.gui example"):SetClickCallback(this:CreateCallbackToFunction("GuiExample"))
    menubar:GetEntry(".", "main.shader editor"):SetClickCallback(this:CreateCallbackToFunction("ShaderEditor"))
    menubar:GetEntry(".", "debug.system monitor"):SetClickCallback(this:CreateCallbackToFunction("SystemMonitor"))
    menubar:GetEntry(".", "debug.texture browser"):SetClickCallback(this:CreateCallbackToFunction("TextureBrowser"))
    menubar:GetEntry(".", "main.exit"):SetClickCallback(this:CreateCallbackToFunction("Exit3DElch"))
end
