local StreamConnection      = require "classes/StreamConnection"
local RequestConnection     = require "classes/RequestConnection"

local LarryServiceHandler = {}
LarryServiceHandler.__index = LarryServiceHandler
setmetatable(LarryServiceHandler, {__call = function(cls, ...) return cls.new() end})

local function SetPrintForService(service, newPrint)
    if service.connection ~= nil then
        service.connection:SetPrint(newPrint)
    end
end

local function DisconnectService(service)
    if service.isEstablished then
        service.connection:Disconnect()
        service.isEstablished = false
    end
    service.connection = nil
end

local function HandleCameraConnection(camera)
    if camera.connection ~= nil then
        if not camera.connection:HasConnectionFailure() and
            not camera.connection:IsConnected() then
            camera.connection:SwitchToNextState()

        elseif camera.connection:IsConnected() then
            camera.currentFrame = camera.connection:GetNextFrame()
            if camera.currentFrame:has_value() then
                camera.isEstablished = true
            end
        end
    end
end

local function HandleDriveConnection(drive)
    if drive.connection ~= nil then
        if drive.connection:IsConnected() then
            drive.isEstablished = true
            if drive.wheelSpeed.previous.left ~= drive.wheelSpeed.current.left or
                drive.wheelSpeed.previous.right ~= drive.wheelSpeed.current.right then
                if drive.request == nil or
                    drive.request:value_available() then
                    local serializer = elByteSerializer_BigEndian.new()
                    if drive.wheelSpeed.current.left == 0 and drive.wheelSpeed.current.right == 0 then
                        serializer:AddUint32(3)
                    else 
                        serializer:AddUint32(2)
                        serializer:AddDouble(drive.wheelSpeed.current.left)
                        serializer:AddDouble(drive.wheelSpeed.current.right)
                    end
                    drive.request = drive.connection:SendCustomRequest(
                                        serializer:GetByteStream())
                end
            end
        end
    end
end

local function HandleSystemInfoConnection(systemInfo)
    if systemInfo.connection ~= nil then
        if systemInfo.connection:IsConnected() then
            systemInfo.isEstablished = true

            local serializer = elByteSerializer_BigEndian.new()
            serializer:AddUint32(systemInfo.tag.uptime)

            if systemInfo.request.uptime == nil or 
                systemInfo.request.uptime:value_available() then

                if systemInfo.request.uptime ~= nil then
                    local uptimeResult = systemInfo.request.uptime:get()
                    if not uptimeResult:HasError() then
                        --print("uptime success")
                        --local uptimeResponse = uptimeResult:GetValuePointer()
                        --print(uptimeResponse:GetRPCEnum())
                        --aaa = rpc_custom_response.new()
                        --print(aaa:GetRPCEnum())
                    end
                end

                systemInfo.request.uptime = systemInfo.connection:SendCustomRequest(
                    serializer:GetByteStream())
            end
        end
    end
end




------------------------------------
-- LarryServiceHandler
------------------------------------

function LarryServiceHandler.new()
    local self              = setmetatable({}, LarryServiceHandler)
    self.ipAddress          = "127.0.0.1"
    self.protocolVersion    = 0
    self.print              = print

    self.camera             = { port = 8995,
                                serviceName = "larry-camera",
                                isEstablished = false, 
                                connection = nil,
                                currentFrame = nil
                              }

    self.drive              = { port = 41682, 
                                serviceName = "larry-drive", 
                                isEstablished = false, 
                                connection = nil,
                                request    = nil,
                                wheelSpeed = {
                                    current  = {left = 0, right = 0},
                                    previous = {left = 0, right = 0}
                                },
                              }

    self.systemInfo         = { port = 26214, 
                                serviceName = "larry-system-info",
                                isEstablished = false, 
                                connection = nil,
                                tag = {
                                    uptime      = 2,
                                    cpu         = 3,
                                    memory      = 4,
                                    network     = 5,
                                    temperatur  = 6
                                },
                                request = {
                                    uptime      = nil,
                                    cpu         = nil,
                                    memory      = nil,
                                    network     = nil,
                                    temperatur  = nil
                                }
                              }

    return self
end

function LarryServiceHandler.SetIPAddress(self, ipAddress)
    self.ipAddress = ipAddress
end

function LarryServiceHandler.SetPrint(self, newPrint)
    self.print = newPrint
    SetPrintForService(self.camera,     newPrint)
    SetPrintForService(self.drive,      newPrint)
    SetPrintForService(self.systemInfo, newPrint)
end

function LarryServiceHandler.ConnectServices(self)
    if not self.camera.isEstablished then
        self.camera.connection = StreamConnection.new(self.ipAddress, 
            self.camera.port, self.camera.serviceName, self.protocolVersion)
        SetPrintForService(self.camera, self.print)
        self.camera.connection:SwitchToNextState()
    end

    if not self.drive.isEstablished then 
        self.drive.connection = RequestConnection.new(self.ipAddress,
            self.drive.port, self.drive.serviceName, self.protocolVersion)
        SetPrintForService(self.drive, self.print)
        self.drive.connection:Connect()
    end

    if not self.systemInfo.isEstablished then 
        self.systemInfo.connection = RequestConnection.new(self.ipAddress,
            self.systemInfo.port, self.systemInfo.serviceName, self.protocolVersion)
        SetPrintForService(self.systemInfo, self.print)
        self.systemInfo.connection:Connect()
    end
end

function LarryServiceHandler.IsConnectionEstablished(self)
    return { camera     = self.camera.isEstablished,
             drive      = self.drive.isEstablished,
             systemInfo = self.systemInfo.isEstablished
            }
end

function LarryServiceHandler.Disconnect(self)
    DisconnectService(self.camera)
    DisconnectService(self.drive)
    DisconnectService(self.systemInfo)

    self.drive.request = nil
    self.systemInfo.request.uptime = nil
end

function LarryServiceHandler.ConnectionLoop(self)
    HandleCameraConnection(self.camera)
    HandleDriveConnection(self.drive)
    HandleSystemInfoConnection(self.systemInfo)
end

function LarryServiceHandler.GetCurrentCameraFrame(self)
    return self.camera.currentFrame
end

function LarryServiceHandler.SetWheelSpeed(self, left, right)
    self.drive.wheelSpeed.previous.left     = self.drive.wheelSpeed.current.left
    self.drive.wheelSpeed.previous.right    = self.drive.wheelSpeed.current.right

    self.drive.wheelSpeed.current.left      = left
    self.drive.wheelSpeed.current.right     = right
end

return LarryServiceHandler
