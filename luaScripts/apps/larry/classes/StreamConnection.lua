StreamConnection = {}
StreamConnection.__index = StreamConnection
setmetatable(StreamConnection, {__call = function(cls, ...) return cls.new() end, })

local state = { connected = 0, disconnected = -999, init = 1, init_error = -1, establish = 2, establish_error = -2, stream = 3, stream_error = -3}

function StreamConnection.new(hostname, port, serviceName, serviceVersion)
    local self                  = setmetatable({}, StreamConnection)

    self.hostname               = hostname
    self.port                   = port
    self.serviceName            = serviceName
    self.serviceVersion         = serviceVersion
    self.currentState           = state.init 
    self.print                  = print
    self.hasReceivedFirstFrame  = false

    return self
end

function StreamConnection.SwitchToNextState(self)
    if self.currentState == state.init then
        self.initialConnection      = elEasyTCPMessaging_Client.new(self.serviceName, self.serviceVersion)
        self.initialConnectionState = self.initialConnection:ConnectTo(self.hostname, self.port)
        self:Log("initiating connection to " .. self.hostname .. ":" .. self.port) 
        self.currentState = state.establish
    elseif self.currentState == state.establish then 
        if self.initialConnectionState:value_available() then 
            if self.initialConnectionState:get() then
                self.streamConnectionState = self.initialConnection:EstablishStreamConnection()
                self:Log("connection established")
                self.currentState = state.stream
            else 
                self:Log("connection failed")
                self.currentState = state.establish_error
            end
        end
    elseif self.currentState == state.stream then
        if self.streamConnectionState:value_available() then 
            if self.streamConnectionState:get():has_value() then
                self.streamConnection = self.streamConnectionState:get()
                self:Log("stream established")
                self.currentState = state.connected
            else 
                self:Log("stream failed")
                self.currentState = state.stream_error
            end
        end
    end

    return self.currentState
end

function StreamConnection.GetCurrentState(self)
    return self.currentState
end

function StreamConnection.GetNextFrame(self)
    local frame = self.streamConnection:get():GetNextFrame()
    if not self.hasReceivedFirstFrame and frame:has_value() then 
        self.hasReceivedFirstFrame = true
        self:Log(self.hostname .. ":" .. self.port .. " start receiving")
    end
    return frame
end

function StreamConnection.Disconnect(self)
    self.streamConnection:get():Disconnect()
    self:Log(self.hostname .. ":" .. self.port .. " disconnected")
    self.currentState = state.disconnected
end

function StreamConnection.IsConnected(self)
    if self.currentState == state.connected then
        return true
    else 
        return false
    end
end

function StreamConnection.SetPrint(self, newPrint)
    self.print = newPrint
end

function StreamConnection.Log(self, message)
    self.print("[ " .. self.serviceName .. ":" .. self.serviceVersion .. " ] " .. message)
end

function StreamConnection.HasConnectionFailure(self)
    return self.currentState < 0
end

return StreamConnection
