local EventHandling = {}
EventHandling.__index = EventHandling
setmetatable(EventHandling, {__call = function(cls, ...) return cls.new() end})

local move = {x = 0, y = 0, z = 0}

function HasMovementEvent(sdlEvent)
    return HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_w) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_a) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_s) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_d) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_w) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_a) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_s) or
               HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_d)
end

function HandleMovementEvent(sdlEvent) 
    if HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_w) then
        move.z = 1
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_w) then
        move.z = 0
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_s) then
        move.z = -1
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_s) then
        move.z = 0
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_a) then
        move.x = -1
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_a) then
        move.x = 0
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYDOWN, SDLK_d) then
        move.x = 1
    elseif HasSpecifiedSDLEventOccured(sdlEvent, SDL_KEYUP, SDLK_d) then
        move.x = 0
    end
end

function EventHandling.new()
    local self = setmetatable({}, EventHandling)

    self.movementEvent = self:CreateAndRegisterMovementEvent()

    return self
end

function EventHandling.CreateAndRegisterMovementEvent(self)
    local event = _3Delch.GetInstance():Window():GetEventHandler():CreateEvent()
    local set = this:CreateCallbackToEventVerificationFunction(
                    "HasMovementEvent")
    local setCallback = this:CreateCallbackToEventCallbackFunction(
                            "HandleMovementEvent")
    event:Set(set)
    event:SetCallback(setCallback)
    event:Register()

    return event
end

function EventHandling.GetMovement(self)
    return move
end

return EventHandling
