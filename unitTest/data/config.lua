TRUE    = 1
FALSE   = 0
bla     = 123
fuu     = 12.012
var3    = "hello world"

array1  = {1, 2, 3}
array2  = {"halo", "fuf", "bar"}

table1 = {
    sub1 = {
        a = 1,
        b = 2,
        c = 3
    },
    sub2 = {
        fu = "asd",
        bla = "asd",
        bu = "aa"
    },
    a = { 1, 2, 3},
    sub3 = {
        a = { 'a', 'b', 'c'}
    }
}

global = {
    log = {
        file = "logfile.log",
        level = 3, -- 3 = error
    },

    window = {
        name = "3Delch",
        size = {1440, 800},
        openGLVersion = {4, 4},
        useOpenGLCoreProfile = TRUE,
        showMouseCursor = FALSE,
        relativeMouseMode = TRUE,
        windowGrabMouse = TRUE,
    },

    renderer = {
        deferred = {
            geometryPassShader = {
                file    = "glsl/deferredRenderer_UnifiedVertexObject.glsl",
                group   = "default",
            },
            directionalLightShader = {
                file    = "glsl/deferredRenderer_UnifiedVertexObject.glsl",
                group   = "directionalLight",
            },
            pointLightShader = {
                file    = "glsl/deferredRenderer_UnifiedVertexObject.glsl",
                group   = "pointLight",
            },
            passThroughShader = {
                file    = "glsl/passThrough.glsl",
                group   = "pointLightDeferred",
            },

            unifiedVertexObject = {
                textureSize = {512, 512, 28},
            },
        },
        
        forward = {
            defaultShader = {
                file    = "glsl/forwardRenderer_VertexObject.glsl",
                group   = "default",
            },
        },

        gui = {
            shader = {
                file  = "glsl/gui/standard.glsl",
                group = "default",
            },

            theme = "cfg/guiTheme.lua",
        },
    },
}
