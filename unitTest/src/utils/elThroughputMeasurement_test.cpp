#include "unitTest.hpp"
#include "units/Time.hpp"
#include "utils/elThroughputMeasurement.hpp"

using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::testing;

class elThroughputMeasurement_test : public Test
{
  public:
    elThroughputMeasurement< uint64_t > sut{ units::Time::MilliSeconds( 10.0 ),
                                             5u };
};

TEST_F( elThroughputMeasurement_test, Constructor )
{
    EXPECT_THAT( sut.Get(), Eq( 5u ) );
}

TEST_F( elThroughputMeasurement_test, IncrementWith )
{
    sut.IncrementWith( 1234u );
    EXPECT_THAT( sut.Get(), Eq( 1239u ) );
}

TEST_F( elThroughputMeasurement_test, GetThroughput )
{
    sut.IncrementWith( 200u );
    EXPECT_THAT( sut.GetThroughputPerSecond(), Eq( 0u ) );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    auto throughPut = sut.GetThroughputPerSecond();
    EXPECT_TRUE( static_cast< uint64_t >( 20000 * 0.8 ) <= throughPut &&
                 throughPut <= static_cast< uint64_t >( 20000 * 1.2 ) );
}
