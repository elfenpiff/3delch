#include "DesignPattern/Creation.hpp"
#include "unitTest.hpp"
#include "units/Time.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

using namespace ::testing;
using namespace ::el3D::utils;
using namespace ::el3D::units;
using namespace ::el3D;

class elByteDeserializer_test : public ::testing::Test
{
  public:
    void
    SetUp()
    {
    }
};

TEST_F( elByteDeserializer_test, ConvertDouble_BigEndian )
{
    double           value{ 223.4242 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< double >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertFloat_LittleEndian )
{
    float            value{ 42.1337f };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< float >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint8_BigEndian )
{
    uint8_t          value{ 223 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint8_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint8_LittleEndian )
{
    uint8_t          value{ 43 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint8_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint16_BigEndian )
{
    uint16_t         value{ 22213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint16_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint16_LittleEndian )
{
    uint16_t         value{ 4223 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint16_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint32_BigEndian )
{
    uint32_t         value{ 21212213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint32_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint32_LittleEndian )
{
    uint32_t         value{ 487223 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint32_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}


TEST_F( elByteDeserializer_test, ConvertUint64_BigEndian )
{
    uint64_t         value{ 29012231212213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint64_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint64_LittleEndian )
{
    uint64_t         value{ 48722138718823 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< uint64_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt8_BigEndian )
{
    int8_t           value{ -23 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int8_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt8_LittleEndian )
{
    int8_t           value{ -43 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int8_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt16_BigEndian )
{
    int16_t          value{ -22213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );


    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int16_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt16_LittleEndian )
{
    int16_t          value{ -4223 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int16_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt32_BigEndian )
{
    int32_t          value{ -21212213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int32_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt32_LittleEndian )
{
    int32_t          value{ -487223 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int32_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}


TEST_F( elByteDeserializer_test, ConvertInt64_BigEndian )
{
    int64_t          value{ -29012231212213 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int64_t >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt64_LittleEndian )
{
    int64_t          value{ -48722138718823 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto               deserialized = sut.Get< int64_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertArray )
{
    std::array< uint32_t, 3 > value{ 1123123, 928391, 289812 };
    elByteSerializer          serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto deserialized = sut.Get< std::array< uint32_t, 3 > >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInt64Vector_BigEndian )
{
    std::vector< int64_t > value{ 123123123, 928391, -1289812, 1231231 };
    elByteSerializer       serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto deserialized = sut.Get< std::vector< int64_t > >( Endian::Big, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertUint64Vector_LittleEndian )
{
    std::vector< uint64_t > value{ 1231231231, 92128391, 12161289812,
                                   128831231 };
    elByteSerializer        serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto deserialized = sut.Get< std::vector< uint64_t > >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertString )
{
    std::string      value{ "Hello World!" };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    auto deserialized = sut.Get< std::string >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( false ) );
    EXPECT_THAT( deserialized.GetValue(), Eq( value ) );
}

TEST_F( elByteDeserializer_test, ConvertInvalidString )
{
    std::string      value{ "Hello World!" };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    auto byteStream = serializer.GetByteStream();
    byteStream.stream.resize( 10 );

    elByteDeserializer sut( byteStream );
    auto deserialized = sut.Get< std::string >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( true ) );
}

TEST_F( elByteDeserializer_test, ConvertInvalidVector )
{
    std::vector< int32_t > value{ 1, 2, 3, 4, 5 };
    elByteSerializer       serializer;
    serializer.Serialize( Endian::Little, value );
    auto byteStream = serializer.GetByteStream();

    byteStream.stream.resize( 18 );
    elByteDeserializer sut( byteStream );
    auto deserialized = sut.Get< std::vector< int32_t > >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( true ) );
}

TEST_F( elByteDeserializer_test, ConvertInvalidInteger )
{
    uint64_t         value{ 123123 };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, value );

    auto byteStream = serializer.GetByteStream();
    byteStream.stream.resize( 7 );
    elByteDeserializer sut( byteStream );
    auto               deserialized = sut.Get< uint64_t >( Endian::Little, 0 );
    ASSERT_THAT( deserialized.HasError(), Eq( true ) );
}

TEST_F( elByteDeserializer_test, ExtractMultipleValues )
{
    uint64_t                   a{ 0xabc }, b{ 0xbcd }, c{ 0xaffe };
    std::string                d{ "hello" }, e{ "world" };
    std::vector< std::string > f{ "lama", "frodo", "blubb" };
    std::vector< uint16_t >    g{ 0x12, 0x23, 0x34, 0x45 };

    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, a, b, c, d, e, f, g );

    elByteDeserializer sut( serializer.GetByteStream() );

    uint64_t                   a1, b1, c1;
    std::string                d1, e1;
    std::vector< std::string > f1;
    std::vector< uint16_t >    g1;

    ASSERT_THAT(
        sut.Extract( Endian::Big, a1, b1, c1, d1, e1, f1, g1 ).HasError(),
        Eq( false ) );
    EXPECT_THAT( a1, Eq( a ) );
    EXPECT_THAT( b1, Eq( b ) );
    EXPECT_THAT( c1, Eq( c ) );
    EXPECT_THAT( d1, Eq( d ) );
    EXPECT_THAT( e1, Eq( e ) );

    ASSERT_THAT( f1.size(), Eq( f.size() ) );
    EXPECT_THAT( f1[0], Eq( f[0] ) );
    EXPECT_THAT( f1[1], Eq( f[1] ) );
    EXPECT_THAT( f1[2], Eq( f[2] ) );

    ASSERT_THAT( g1.size(), Eq( g.size() ) );
    EXPECT_THAT( g1[0], Eq( g[0] ) );
    EXPECT_THAT( g1[1], Eq( g[1] ) );
    EXPECT_THAT( g1[2], Eq( g[2] ) );
}

TEST_F( elByteDeserializer_test, ExtractSingleUint64_t_WithOffset )
{
    elByteSerializer serializer;
    int32_t          crap{ 123 };
    uint64_t         value{ 901231 };

    serializer.Serialize( Endian::Big, crap, value );

    elByteDeserializer sut( serializer.GetByteStream(), 4u );
    uint64_t           extractedValue;

    ASSERT_THAT( sut.Extract( Endian::Big, extractedValue ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( value, Eq( extractedValue ) );
}

TEST_F( elByteDeserializer_test, VariantUint )
{
    std::variant< uint32_t, std::string > testData{
        static_cast< uint32_t >( 1231u ) };
    elByteSerializer serializer;
    serializer.Serialize( Endian::Little, testData );

    elByteDeserializer                    sut( serializer.GetByteStream() );
    std::variant< uint32_t, std::string > result;

    ASSERT_THAT( sut.Extract( Endian::Little, result ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( std::get< 0 >( result ), Eq( std::get< 0 >( testData ) ) );
}

TEST_F( elByteDeserializer_test, VariantString )
{
    std::variant< uint32_t, std::string, int8_t > testData{ "dodo" };
    elByteSerializer                              serializer;
    serializer.Serialize( Endian::Big, testData );

    elByteDeserializer sut( serializer.GetByteStream() );
    std::variant< uint32_t, std::string, int8_t > result;

    ASSERT_THAT( sut.Extract( Endian::Big, result ).HasError(), Eq( false ) );
    EXPECT_THAT( std::get< 1 >( result ), Eq( std::get< 1 >( testData ) ) );
}

TEST_F( elByteDeserializer_test, Uint32AndThenByteStream )
{
    byteStream_t       stream{ 0x00, 0x00, 0x00, 0x05, 0xab, 0xbc, 0xcd, 0xde };
    elByteDeserializer sut( stream );

    uint32_t     number;
    byteStream_t byteStream;

    ASSERT_THAT( sut.Extract( Endian::Big, number, byteStream ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( number, Eq( 5 ) );
    ASSERT_THAT( byteStream.stream.size(), Eq( 4 ) );
    EXPECT_THAT( byteStream.stream[0], Eq( 0xab ) );
    EXPECT_THAT( byteStream.stream[1], Eq( 0xbc ) );
    EXPECT_THAT( byteStream.stream[2], Eq( 0xcd ) );
    EXPECT_THAT( byteStream.stream[3], Eq( 0xde ) );
}

TEST_F( elByteDeserializer_test, Uint32AndThenByteStreamWithOffset )
{
    byteStream_t stream{ 0xff, 0xaa, 0xbe, 0x00, 0x00, 0x00,
                         0x07, 0xa1, 0xb1, 0xc1, 0xd1 };

    elByteDeserializer sut( stream, 3 );

    uint32_t     number;
    byteStream_t byteStream;

    ASSERT_THAT( sut.Extract( Endian::Big, number, byteStream ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( number, Eq( 7 ) );
    EXPECT_THAT( byteStream.stream.size(), Eq( 4 ) );
    EXPECT_THAT( byteStream.stream[0], Eq( 0xa1 ) );
    EXPECT_THAT( byteStream.stream[1], Eq( 0xb1 ) );
    EXPECT_THAT( byteStream.stream[2], Eq( 0xc1 ) );
    EXPECT_THAT( byteStream.stream[3], Eq( 0xd1 ) );
}

TEST_F( elByteDeserializer_test, MultipleUint32WithOffset )
{
    byteStream_t stream{ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
                         0x07, 0x08, 0x09, 0x10, 0x11 };

    elByteDeserializer sut( stream, 3 );
    uint32_t           number1;
    uint32_t           number2;

    ASSERT_THAT( sut.Extract( Endian::Big, number1, number2 ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( number1, Eq( 0x04050607 ) );
    EXPECT_THAT( number2, Eq( 0x08091011 ) );
}

TEST_F( elByteDeserializer_test, DeserializeBasicUnit )
{
    units::Time      value = units::Time::Seconds( 123.456 );
    elByteSerializer serializer;
    serializer.Serialize( Endian::Big, value );

    elByteDeserializer sut( serializer.GetByteStream() );
    units::Time        extractedValue;

    ASSERT_THAT( sut.Extract( Endian::Big, extractedValue ).HasError(),
                 Eq( false ) );
    EXPECT_THAT( value, Eq( extractedValue ) );
}
