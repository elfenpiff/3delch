#include "unitTest.hpp"
#include "utils/memcpy_pp.hpp"

using namespace ::testing;
using namespace ::el3D::utils;

TEST( MemCpy_pp_test, NonVoidPtr )
{
    int  a    = 1234;
    int  b    = 4567;
    auto addr = MemCpy::From( &a ).To( &b );
    EXPECT_THAT( a, Eq( b ) );
    EXPECT_THAT( &b, Eq( addr ) );
}


TEST( MemCpy_pp_test, VoidPtr )
{
    int  a    = 1337;
    int  b    = 4242;
    auto addr = MemCpy::From( static_cast< void* >( &a ) )
                    .To( static_cast< void* >( &b ) )
                    .Size( sizeof( int ) );
    EXPECT_THAT( a, Eq( b ) );
    EXPECT_THAT( &b, Eq( addr ) );
}
