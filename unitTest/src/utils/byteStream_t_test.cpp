#include "unitTest.hpp"
#include "utils/byteStream_t.hpp"

using namespace ::testing;
using namespace ::el3D::utils;

TEST( byteStream_t_test, PreallocatedMemory )
{
    byte_t someBytes[100];
    {
        byteStream_t newStream( 10, someBytes );

        byte_t i = 0;
        for ( auto& e : newStream.stream )
        {
            e = i++;
        }
    }
    for ( byte_t i = 0; i < 10; ++i )
        EXPECT_THAT( someBytes[i], Eq( i ) );
}
