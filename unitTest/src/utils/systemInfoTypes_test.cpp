#include "unitTest.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/systemInfoTypes.hpp"

using namespace ::testing;
using namespace ::el3D::utils;
using namespace ::el3D::utils::internal;

template < typename T >
T
SerializationTest( T& sut, const Endian endianness )
{
    byteStream_t stream( sut.GetSerializedSize() );

    EXPECT_THAT( sut.Serialize( endianness, stream, 0 ),
                 Eq( sut.GetSerializedSize() ) );

    T deserializedSut;
    EXPECT_THAT(
        deserializedSut.Deserialize( endianness, stream, 0 ).HasError(),
        Eq( false ) );
    return deserializedSut;
}

TEST( systemInfoTypes, cpuCore_t_GetSerializedSize )
{
    ASSERT_THAT( cpuCore_t().GetSerializedSize(), Eq( sizeof( float ) * 2 ) );
}

TEST( systemInfoTypes, cpuCore_t_Serialize )
{
    cpuCore_t sut;
    sut.frequencyInMhz = 123.456f;
    sut.load           = 789.012f;

    cpuCore_t deserializedSut =
        SerializationTest< cpuCore_t >( sut, Endian::Big );
    ASSERT_THAT( deserializedSut.frequencyInMhz, Eq( 123.456f ) );
    ASSERT_THAT( deserializedSut.load, Eq( 789.012f ) );
}

TEST( systemInfoTypes, cpu_t_SerializedSize )
{
    cpu_t sut;
    sut.model = "1fuuu1";
    sut.cores.resize( 5 );
    ASSERT_THAT( sut.GetSerializedSize(),
                 Eq( 8u + 6u + 4u + 4u + 8u + 5u * ( 4u + 4u ) ) );
}

TEST( systemInfoTypes, cpu_t_Serialize )
{
    cpu_t sut;
    sut.model         = " abcd";
    sut.numberOfCores = 2;
    sut.load          = 5.54f;
    cpuCore_t cpu1, cpu2;
    cpu1.frequencyInMhz = 1.2f;
    cpu1.load           = 3.4f;
    cpu2.frequencyInMhz = 5.6f;
    cpu2.load           = 7.8f;

    sut.cores.emplace_back( cpu1 );
    sut.cores.emplace_back( cpu2 );

    cpu_t deserializedSut = SerializationTest< cpu_t >( sut, Endian::Big );
    ASSERT_THAT( deserializedSut.model, Eq( " abcd" ) );
    ASSERT_THAT( deserializedSut.numberOfCores, Eq( 2 ) );
    ASSERT_THAT( deserializedSut.load, Eq( 5.54f ) );
    ASSERT_THAT( deserializedSut.cores.size(), Eq( 2 ) );
    ASSERT_THAT( deserializedSut.cores[0].frequencyInMhz, Eq( 1.2f ) );
    ASSERT_THAT( deserializedSut.cores[0].load, Eq( 3.4f ) );
    ASSERT_THAT( deserializedSut.cores[1].frequencyInMhz, Eq( 5.6f ) );
    ASSERT_THAT( deserializedSut.cores[1].load, Eq( 7.8f ) );
}
