#include "GLAPI/elWindow.hpp"
#include "OpenGL/elBufferObject.hpp"
#include "unitTest.hpp"

using namespace ::el3D::GLAPI;
using namespace ::el3D::OpenGL;


class elBufferObject_test : public ::testing::Test
{
    virtual void
    SetUp()
    {
    }

    virtual void
    TearDown()
    {
    }

    std::unique_ptr< elWindow > window =
        elWindow::Create( 1, 1, "elBufferObject_test", 4, 4, true, 0u, true )
            .GetValue();
};

TEST_F( elBufferObject_test, UploadBufferData )
{
    testing::internal::CaptureStderr();

    elBufferObject b( 2 );

    std::vector< GLfloat > data( 100 );
    for ( size_t i = 0; i < 100; ++i )
        data[i] = static_cast< GLfloat >( static_cast< float >( i ) * 0.9 );

    b.BufferData( static_cast< GLsizeiptr >( sizeof( GLfloat ) * data.size() ),
                  data.data() );

    GLfloat* testData = new GLfloat[100]();

    b.GetBufferSubData( 0, sizeof( GLfloat ) * 100, testData );

    for ( size_t i = 0; i < 100; ++i )
        EXPECT_EQ( testData[i], data[i] );

    std::vector< GLfloat > data2( 20 );
    for ( size_t i = 0; i < 20; ++i )
        data2[i] = 55;

    b.BufferSubData( 80 * sizeof( GLfloat ), sizeof( GLfloat ) * 20,
                     data2.data() );
    b.GetBufferSubData( 0, sizeof( GLfloat ) * 100, testData );

    for ( size_t i = 0; i < 80; ++i )
        EXPECT_EQ( testData[i], data[i] );
    for ( size_t i = 80; i < 100; ++i )
        EXPECT_EQ( testData[i], 55 );


    b.BufferData( static_cast< GLsizeiptr >( sizeof( GLfloat ) * data.size() ),
                  data.data(), 1 );
    b.GetBufferSubData( 0, sizeof( GLfloat ) * 100, testData, 1 );

    for ( size_t i = 0; i < 100; ++i )
        EXPECT_EQ( testData[i], data[i] );

    delete[] testData;

    EXPECT_EQ( b.GetBufferParameteriv( GL_BUFFER_SIZE ),
               100 * sizeof( GLfloat ) );
    testing::internal::GetCapturedStderr();
}
