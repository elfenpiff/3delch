#include "concurrent/elStreamBuffer.hpp"
#include "unitTest.hpp"

#include <thread>

using namespace ::testing;
using namespace ::el3D::concurrent;

class elStreamBuffer_test : public Test
{
  public:
    elStreamBuffer< int > sut;
};

TEST_F( elStreamBuffer_test, EmptyPopWithNoPush )
{
    EXPECT_THAT( sut.Pop().has_value(), Eq( false ) );
}

TEST_F( elStreamBuffer_test, EmptyPopWhatsPushed )
{
    sut.Push( 4562 );
    auto value = sut.Pop();
    ASSERT_THAT( value.has_value(), Eq( true ) );
    EXPECT_THAT( *value, Eq( 4562 ) );
}

TEST_F( elStreamBuffer_test, DoublePopIsEmpty )
{
    sut.Push( 4921 );
    EXPECT_THAT( sut.Pop().has_value(), Eq( true ) );
    EXPECT_THAT( sut.Pop().has_value(), Eq( false ) );
}

TEST_F( elStreamBuffer_test, MultiPushPopsLatest )
{
    sut.Push( 111 );
    sut.Push( 222 );
    sut.Push( 3334 );

    auto value = sut.Pop();
    EXPECT_THAT( value.has_value(), Eq( true ) );
    EXPECT_THAT( *value, Eq( 3334 ) );
}

