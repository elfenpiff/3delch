#include "concurrent/elSmartLock.hpp"
#include "unitTest.hpp"

#include <vector>

using namespace ::testing;
using namespace ::el3D::concurrent;

namespace elSmartLock_test
{
struct NoDefaultCTor
{
};
} // namespace elSmartLock_test

TEST( elSmartLock_test, API_TEST_NonConstSimple )
{
    elSmartLock< std::vector< int > > threadSafeVector;
    threadSafeVector->push_back( 123 );
}

TEST( elSmartLock_test, API_TEST_NonConstSimpleCreate )
{
    auto threadSafeVector = elSmartLock< std::vector< int > >::Create();
    threadSafeVector->push_back( 123 );
}

TEST( elSmartLock_test, API_TEST_ConstSimple )
{
    auto threadSafeVector = elSmartLock< std::vector< int > >::Create();

    EXPECT_TRUE( ( *const_cast< const decltype( threadSafeVector )* >(
                       &threadSafeVector ) )
                     ->empty() );
}

TEST( elSmartLock_test, API_TEST_NonConstGetLockGuard )
{
    elSmartLock< std::vector< int > > threadSafeVector;

    auto guard = threadSafeVector.GetLockGuard();
    guard->push_back( 1 );
    guard->push_back( 1 );
}

TEST( elSmartLock_test, API_TEST_ConstGetLockGuard )
{
    elSmartLock< std::vector< int > > threadSafeVector;

    auto guard = ( *const_cast< const decltype( threadSafeVector )* >(
                       &threadSafeVector ) )
                     .GetLockGuard();
    EXPECT_TRUE( guard->empty() );
}
