#include "concurrent/elSemaphore.hpp"
#include "unitTest.hpp"

#ifndef _WIN32

using namespace ::testing;
using namespace ::el3D::concurrent;

class elSemaphore_test : public Test
{
  public:
    void
    SetUp()
    {
    }

    void
    TearDown()
    {
    }
};

TEST_F( elSemaphore_test, CTor )
{
    auto sut = elSemaphore::Create();
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( *( *sut )->GetValue(), Eq( 0 ) );
}

TEST_F( elSemaphore_test, CTorWithValue )
{
    auto sut = elSemaphore::Create( 42u );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    EXPECT_THAT( *( *sut )->GetValue(), Eq( 42 ) );
}

TEST_F( elSemaphore_test, MoveCTorWithValue )
{
    auto sut = elSemaphore::Create( 42u );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    elSemaphore::result_t sut2( std::move( sut ) );
    ASSERT_THAT( sut2.HasError(), Eq( false ) );
    EXPECT_THAT( *( *sut2 )->GetValue(), Eq( 42 ) );
}

TEST_F( elSemaphore_test, MoveAssignment )
{
    auto sut2 = elSemaphore::Create( 73u );
    ASSERT_THAT( sut2.HasError(), Eq( false ) );

    {
        auto sut = elSemaphore::Create( 42u );
        ASSERT_THAT( sut.HasError(), Eq( false ) );
        sut2 = std::move( sut );
    }

    EXPECT_THAT( *( *sut2 )->GetValue(), Eq( 42 ) );
}

TEST_F( elSemaphore_test, Post )
{
    auto sut = elSemaphore::Create( 3u );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    ( *sut )->Post();

    EXPECT_THAT( *( *sut )->GetValue(), Eq( 4 ) );
}

TEST_F( elSemaphore_test, Wait )
{
    auto sut = elSemaphore::Create( 3u );
    ASSERT_THAT( sut.HasError(), Eq( false ) );
    ( *sut )->Wait();

    EXPECT_THAT( *( *sut )->GetValue(), Eq( 2 ) );
}

#endif
