#include "concurrent/elTrigger.hpp"
#include "unitTest.hpp"

#include <chrono>
#include <thread>

using namespace ::testing;
using namespace ::el3D::concurrent;
using namespace ::el3D::units;
using namespace ::el3D::units::literals;
using namespace ::el3D::bb;

class elTrigger_test : public Test
{
  public:
    elTrigger< int > sut{123, []( const int& i ) { return i > 0; }};
};

TEST_F( elTrigger_test, SetInitialValue )
{
    elTrigger< int > sut{42, []( const int& ) { return true; }};
    EXPECT_THAT( sut.Get(), Eq( 42 ) );
}

TEST_F( elTrigger_test, SetAndGet )
{
    sut.Set( 1337 );
    EXPECT_THAT( sut.Get(), Eq( 1337 ) );
}

TEST_F( elTrigger_test, SetWithSetterAndGet )
{
    sut.SetWithSetter( []( const int& i ) { return i * 2; } );
    EXPECT_THAT( sut.Get(), Eq( 246 ) );
}

TEST_F( elTrigger_test, TryWaitWhenPredicateIsValidReturnsValue )
{
    auto value = sut.TryWait();
    ASSERT_THAT( value.has_value(), Eq( true ) );
    EXPECT_THAT( *value, Eq( 123 ) );
}

TEST_F( elTrigger_test, TryWaitWhenPredicateIsInvalidReturnsFalse )
{
    sut.Set( -12 );
    EXPECT_THAT( sut.TryWait().has_value(), Eq( false ) );
}

TEST_F( elTrigger_test, TryWaitWithCustomPredicate )
{
    sut.Set( -13 );
    EXPECT_THAT(
        sut.TryWait( []( const int& i ) { return i == -13; } ).has_value(),
        Eq( true ) );
}

TEST_F( elTrigger_test,
        BlockingWaitWhenPredicateIsValidReturnsValueImmidiately )
{
    auto value = sut.BlockingWait();
    ASSERT_THAT( value.has_value(), Eq( true ) );
    EXPECT_THAT( *value, Eq( 123 ) );
}

TEST_F( elTrigger_test, BlockingWaitWhenPredicateIsInvalidWaits )
{
    std::atomic_bool threadHasReturned = false;
    sut.Set( -1 );
    std::thread t( [&] {
        EXPECT_THAT( sut.BlockingWait().has_value(), Eq( true ) );
        threadHasReturned = true;
    } );
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( threadHasReturned.load(), Eq( false ) );
    sut.Set( 1 );
    sut.TriggerOne();
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( threadHasReturned.load(), Eq( true ) );
    t.join();
}

TEST_F( elTrigger_test, BlockingWaitWithCustomPredicate )
{
    sut.Set( -78 );
    EXPECT_THAT(
        sut.BlockingWait( []( const int& i ) { return i == -78; } ).has_value(),
        Eq( true ) );
}

TEST_F( elTrigger_test, TimedWaitWhenPredicateIsValidReturnsValueImmidiately )
{
    auto value = sut.TimedWait( Time::MilliSeconds( 10.0f ) );
    ASSERT_THAT( value.has_value(), Eq( true ) );
    EXPECT_THAT( *value, Eq( 123 ) );
}

TEST_F( elTrigger_test, TimedWaitReturnsFalseOnTimeout )
{
    sut.Set( -1 );
    EXPECT_THAT( sut.TimedWait( Time::MilliSeconds( 10.0f ) ).has_value(),
                 Eq( false ) );
}

TEST_F( elTrigger_test, TimedWaitWhenPredicateIsInvalidWaitsTillTimeout )
{
    std::atomic_bool threadHasReturned = false;
    sut.Set( -1 );
    std::thread t( [&] {
        EXPECT_THAT( sut.TimedWait( Time::MilliSeconds( 10.0f ) ).has_value(),
                     Eq( false ) );
        threadHasReturned = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 7 ) );
    EXPECT_THAT( threadHasReturned, Eq( false ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 7 ) );
    EXPECT_THAT( threadHasReturned, Eq( true ) );
    t.join();
}

TEST_F( elTrigger_test, TimedWaitWhenPredicateIsInvalidWaitsTillItsTriggered )
{
    std::atomic_bool threadHasReturned = false;
    sut.Set( -1 );
    std::thread t( [&] {
        EXPECT_THAT( sut.TimedWait( Time::MilliSeconds( 10.0f ) ).has_value(),
                     Eq( true ) );
        threadHasReturned = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 7 ) );
    EXPECT_THAT( threadHasReturned, Eq( false ) );
    sut.Set( 1 );
    sut.TriggerOne();
    std::this_thread::sleep_for( std::chrono::milliseconds( 7 ) );
    EXPECT_THAT( threadHasReturned, Eq( true ) );
    t.join();
}

TEST_F( elTrigger_test, TimedWaitWithCustomPredicate )
{
    sut.Set( -781 );
    EXPECT_THAT( sut.TimedWait( Time::Seconds( 1 ),
                                []( const int& i ) { return i == -781; } )
                     .has_value(),
                 Eq( true ) );
}

TEST_F( elTrigger_test, TriggerMultipleWaiter )
{
    constexpr size_t                                numberOfWaiters = 3;
    std::array< std::atomic_bool, numberOfWaiters > threadHasReturned{false};
    sut.Set( -1 );

    auto call = [&]( size_t i ) {
        sut.BlockingWait();
        threadHasReturned[i] = true;
    };

    std::vector< std::thread > threads;
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        threads.emplace_back( [&, i = i] { call( i ); } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        EXPECT_THAT( threadHasReturned[i].load(), Eq( false ) );

    sut.Set( 1 );
    sut.TriggerAll();

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        EXPECT_THAT( threadHasReturned[i].load(), Eq( true ) );

    for ( auto& t : threads )
        t.join();
}

TEST_F( elTrigger_test, TriggerOnlyOnOfMultipleWaiters )
{
    constexpr size_t                                numberOfWaiters = 3;
    std::array< std::atomic_bool, numberOfWaiters > threadHasReturned{false};
    sut.Set( -1 );
    auto call = [&]( size_t i ) {
        sut.BlockingWait();
        threadHasReturned[i] = true;
    };

    std::vector< std::thread > threads;
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        threads.emplace_back( [&, i = i] { call( i ); } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        EXPECT_THAT( threadHasReturned[i].load(), Eq( false ) );

    sut.Set( 1 );
    sut.TriggerOne();

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    int returnCounter = 0;
    for ( size_t i = 0; i < numberOfWaiters; ++i )
        if ( threadHasReturned[i].load() ) returnCounter++;

    EXPECT_THAT( returnCounter, Eq( 1 ) );

    sut.TriggerAll();

    for ( auto& t : threads )
        t.join();
}

TEST_F( elTrigger_test, BlockingWaitAbortsOnDestructorAndReturnsFalse )
{
    std::thread      t;
    std::atomic_bool threadHasReturned = false;

    {
        elTrigger< int > sut( -1, []( const int& i ) { return i > 0; } );
        t = std::thread( [&] {
            EXPECT_THAT( sut.BlockingWait().has_value(), Eq( false ) );
            threadHasReturned = true;
        } );

        std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
        EXPECT_THAT( threadHasReturned, Eq( false ) );
    }

    std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
    EXPECT_THAT( threadHasReturned, Eq( true ) );
    t.join();
}

TEST_F( elTrigger_test, TimedWaitAbortsOnDestructorAndReturnsFalse )
{
    std::thread      t;
    std::atomic_bool threadHasReturned = false;

    {
        elTrigger< int > sut( -1, []( const int& i ) { return i > 0; } );
        t = std::thread( [&] {
            EXPECT_THAT( sut.TimedWait( Time::Seconds( 10 ) ).has_value(),
                         Eq( false ) );
            threadHasReturned = true;
        } );

        std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
        EXPECT_THAT( threadHasReturned, Eq( false ) );
    }

    std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
    EXPECT_THAT( threadHasReturned, Eq( true ) );
    t.join();
}
