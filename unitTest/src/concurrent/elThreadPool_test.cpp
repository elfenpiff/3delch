#include "concurrent/elThreadPool.hpp"
#include "unitTest.hpp"

#include <chrono>

using namespace ::el3D::concurrent;
using namespace ::testing;

class elThreadPool_test : public Test
{
  public:
};

TEST_F( elThreadPool_test, DoesNotBlockOnDTor )
{
    elThreadPool pool( 4 );
    pool.AddTask( [] {
        std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    } );
}

TEST_F( elThreadPool_test, GetNumberOfThreads )
{
    elThreadPool pool( 4 );
    EXPECT_THAT( pool.GetNumberOfThreads(), Eq( 4 ) );
}
