#include "buildingBlocks/elCircularBuffer.hpp"
#include "buildingBlocks/elFiFo.hpp"
#include "concurrent/elPuPo.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::concurrent;
using namespace ::el3D::units;
using namespace ::el3D::units::literals;
using namespace ::el3D::bb;

template < typename PuPoContainer >
class elPuPo_test : public Test
{
  public:
    void
    SetUp()
    {
    }

    void
    TearDown()
    {
    }

    PuPoContainer sut;
};

using vec_t = std::vector< int >;

using Implementations =
    Types< elPuPo< elFiFo< int > >, elPuPo< elCircularBuffer< int, 10 > > >;

TYPED_TEST_CASE( elPuPo_test, Implementations );

TYPED_TEST( elPuPo_test, IsEmptyWhenCreated )
{
    EXPECT_THAT( this->sut.IsEmpty(), Eq( true ) );
}

TYPED_TEST( elPuPo_test, IsNotEmptyWhenPushed )
{
    this->sut.Push( 1 );
    EXPECT_THAT( this->sut.IsEmpty(), Eq( false ) );
}

TYPED_TEST( elPuPo_test, sizeEqualsZeroWhenCreated )
{
    EXPECT_THAT( this->sut.Size(), Eq( 0 ) );
}

TYPED_TEST( elPuPo_test, sizeWhenPushed )
{
    this->sut.Push( 1 );
    this->sut.Push( 1 );
    EXPECT_THAT( this->sut.Size(), Eq( 2 ) );
}

TYPED_TEST( elPuPo_test, pushTryPopSingleElement )
{
    this->sut.Push( 1 );
    auto val = this->sut.TryPop();
    ASSERT_THAT( val.has_value(), Eq( true ) );
    EXPECT_THAT( *val, Eq( 1 ) );
}

TYPED_TEST( elPuPo_test, pushTryPopMultipleElement )
{
    for ( int i = 0; i < 10; ++i )
        this->sut.Push( i );

    for ( int i = 0; i < 10; ++i )
    {
        auto val = this->sut.TryPop();
        ASSERT_THAT( val.has_value(), Eq( true ) );
        EXPECT_THAT( *val, Eq( i ) );
    }
}

TYPED_TEST( elPuPo_test, tryPopFailsWhenEmpty )
{
    EXPECT_THAT( this->sut.TryPop().has_value(), Eq( false ) );
}

TYPED_TEST( elPuPo_test, tryMultiPopSeveralItems )
{
    for ( int i = 0; i < 10; ++i )
        this->sut.Push( i );

    auto result = this->sut.template TryMultiPop< vec_t >( 5 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value().size(), Eq( 5 ) );

    for ( size_t i = 0; i < 5; ++i )
        EXPECT_THAT( result.value()[i], Eq( i ) );
}

TYPED_TEST( elPuPo_test, tryMultiPopAllItems )
{
    for ( int i = 0; i < 10; ++i )
        this->sut.Push( i );

    auto result = this->sut.template TryMultiPop< vec_t >();
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value().size(), Eq( 10 ) );

    for ( size_t i = 0; i < 10; ++i )
        EXPECT_THAT( result.value()[i], Eq( i ) );
}

TYPED_TEST( elPuPo_test, blockingPopNotBlockingWhenFull )
{
    this->sut.Push( 123 );

    auto result = this->sut.BlockingPop();
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value(), Eq( 123 ) );
}

TYPED_TEST( elPuPo_test, blockingPopBlockingWhenEmpty )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.BlockingPop();
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Push( 123 );
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedPopNotBlockingWhenFull )
{
    this->sut.Push( 123 );

    auto result = this->sut.TimedPop( 3600_s );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value(), Eq( 123 ) );
}

TYPED_TEST( elPuPo_test, timedPopBlockingWhenEmpty )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.TimedPop( 3600_s );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Push( 123 );
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedPopNonBlockingAfterTimeout )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.TimedPop( 10_ms );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 6 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 6 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, blockingMultiPopNotBlockingWhenFull )
{
    this->sut.Push( 123 );
    this->sut.Push( 456 );
    this->sut.Push( 789 );

    auto result = this->sut.template BlockingMultiPop< vec_t >( 2 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value().size(), Eq( 2 ) );
    ASSERT_THAT( result.value()[0], Eq( 123 ) );
    ASSERT_THAT( result.value()[1], Eq( 456 ) );
}

TYPED_TEST( elPuPo_test, blockingMultiPopBlockingWhenEmpty )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.template BlockingMultiPop< vec_t >( 2 );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Push( 123 );
    this->sut.Push( 123 );
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedMultiPopNotBlockingWhenFull )
{
    this->sut.Push( 123 );
    this->sut.Push( 456 );

    auto result = this->sut.template TimedMultiPop< vec_t >( 3600_s, 2 );

    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result.value().size(), Eq( 2 ) );
    ASSERT_THAT( result.value()[0], Eq( 123 ) );
    ASSERT_THAT( result.value()[1], Eq( 456 ) );
}

TYPED_TEST( elPuPo_test, timedMultiPopBlockingWhenEmpty )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.template TimedMultiPop< vec_t >( 3600_s, 2 );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Push( 123 );
    this->sut.Push( 123 );
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedMultiPopNonBlockingAfterTimeout )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.template TimedMultiPop< vec_t >( 10_ms, 44 );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 6 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 6 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, blockingPopUnblockingAfterTerminate )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.BlockingPop();
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Clear();
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, blockingMultiPopUnblockingAfterClear )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.template BlockingMultiPop< vec_t >( 123 );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Clear();
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedPopUnblockingAfterClear )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.TimedPop( 12345_s );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Clear();
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}

TYPED_TEST( elPuPo_test, timedMultiPopUnblockingAfterClear )
{
    std::atomic< bool > hasPopped{false};

    std::thread t( [&] {
        this->sut.template TimedMultiPop< vec_t >( 1231213_s, 123 );
        hasPopped = true;
    } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    EXPECT_THAT( hasPopped, Eq( false ) );
    this->sut.Clear();
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
    EXPECT_THAT( hasPopped, Eq( true ) );

    t.join();
}
