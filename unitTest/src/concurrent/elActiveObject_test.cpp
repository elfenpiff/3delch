#include "concurrent/elActiveObject.hpp"
#include "concurrent/elThreadPool.hpp"
#include "unitTest.hpp"

#include <chrono>

using namespace ::el3D::concurrent;
using namespace ::testing;

class elActiveObject_test : public Test
{
  public:
    elThreadPool   threadPool{4u};
    elActiveObject sut{threadPool};
};

TEST_F( elActiveObject_test, DTorBlocksTillEveryTaskIsFinished )
{
    bool hasValue = false;
    {
        elActiveObject sut2{threadPool};
        sut2.AddTask( [&] {
            std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
            hasValue = true;
        } );
    }

    EXPECT_THAT( hasValue, Eq( true ) );
}


TEST_F( elActiveObject_test, TaskWithReturn )
{
    bool taskWasExecuted = false;
    auto r               = sut.AddTaskWithReturn< int >( [&]() {
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
        taskWasExecuted = true;
        return 1337;
    } );
    EXPECT_THAT( taskWasExecuted, Eq( false ) );
    EXPECT_THAT( r->get(), Eq( 1337 ) );
    EXPECT_THAT( taskWasExecuted, Eq( true ) );
}
