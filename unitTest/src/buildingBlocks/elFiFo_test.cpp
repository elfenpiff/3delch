#include "buildingBlocks/elFiFo.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::bb;

class elFiFo_test : public Test
{
  public:
    elFiFo< int > sut;
};

using vec_t = std::vector< int >;

TEST_F( elFiFo_test, PushPopOne )
{
    sut.Push( 121 );
    auto result = sut.Pop();
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( *result, Eq( 121 ) );
}

TEST_F( elFiFo_test, MultiPushPop )
{
    sut.MultiPush( vec_t{111, 222, 333} );

    for ( size_t k = 1; k < 4; ++k )
    {
        auto result = sut.Pop();
        ASSERT_THAT( result.has_value(), Eq( true ) );
        EXPECT_THAT( *result, Eq( k * 111 ) );
    }
}

TEST_F( elFiFo_test, MultiPop )
{
    sut.Push( 121 );
    sut.Push( 12 );
    sut.Push( 1 );
    auto result = sut.MultiPop< vec_t >( 2 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 2 ) );
    EXPECT_THAT( result->at( 0 ), Eq( 121 ) );
    EXPECT_THAT( result->at( 1 ), Eq( 12 ) );
}

TEST_F( elFiFo_test, MultiPopTooLarge )
{
    sut.Push( 121 );
    sut.Push( 12 );
    sut.Push( 1 );
    auto result = sut.MultiPop< vec_t >( 4 );
    ASSERT_THAT( result.has_value(), Eq( false ) );
}

TEST_F( elFiFo_test, NoPopAfterClear )
{
    sut.Push( 121 );
    sut.Push( 12 );
    sut.Push( 1 );
    sut.Clear();
    auto result = sut.Pop();
    ASSERT_THAT( result.has_value(), Eq( false ) );
}

TEST_F( elFiFo_test, SizeZeroAfterClear )
{
    sut.Push( 121 );
    sut.Push( 12 );
    sut.Push( 1 );
    sut.Clear();
    ASSERT_THAT( sut.Size(), Eq( 0 ) );
}

TEST_F( elFiFo_test, IsEmptyAfterClear )
{
    sut.Push( 121 );
    sut.Push( 12 );
    sut.Clear();
    ASSERT_THAT( sut.IsEmpty(), Eq( true ) );
}

TEST_F( elFiFo_test, IsNotEmptyAfterPush )
{
    sut.Push( 121 );
    sut.Push( 12 );
    ASSERT_THAT( sut.IsEmpty(), Eq( false ) );
}
