#include "buildingBlocks/elCircularBuffer.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::bb;

class elCircularBuffer_test : public Test
{
  public:
    elCircularBuffer< int, 3 > sut;
};

using vec_t = std::vector< int >;

TEST_F( elCircularBuffer_test, Size )
{
    EXPECT_THAT( sut.Size(), Eq( 0u ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.Size(), Eq( 1u ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.Size(), Eq( 2u ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.Size(), Eq( 3u ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.Size(), Eq( 3u ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.Size(), Eq( 3u ) );
}

TEST_F( elCircularBuffer_test, IsEmpty )
{
    EXPECT_THAT( sut.IsEmpty(), Eq( true ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.IsEmpty(), Eq( false ) );
    sut.Push( 1 );
    EXPECT_THAT( sut.IsEmpty(), Eq( false ) );
    sut.Pop();
    sut.Pop();
    EXPECT_THAT( sut.IsEmpty(), Eq( true ) );
}

namespace elCircularBuffer_testing
{
size_t dtorCounter{0};
struct CTorDTor
{
    CTorDTor()
    {
        dtorCounter++;
    }
    CTorDTor( const CTorDTor& )
    {
        dtorCounter++;
    }
    ~CTorDTor()
    {
        dtorCounter--;
    }
};
} // namespace elCircularBuffer_testing

TEST_F( elCircularBuffer_test, Clear )
{
    elCircularBuffer< elCircularBuffer_testing::CTorDTor, 3 > sut2;
    sut2.Push();
    sut2.Push();
    sut2.Push();
    sut2.Push();
    sut2.Push();

    EXPECT_THAT( elCircularBuffer_testing::dtorCounter, Eq( 3 ) );
    sut2.Clear();
    EXPECT_THAT( elCircularBuffer_testing::dtorCounter, Eq( 0 ) );
}

TEST_F( elCircularBuffer_test, PushOnOverflow )
{
    for ( int i = 0; i < 3; ++i )
        EXPECT_THAT( sut.Push( i ).has_value(), Eq( false ) );

    auto result = sut.Push( 4 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( *result, Eq( 0 ) );

    result = sut.Push( 5 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( *result, Eq( 1 ) );
}

TEST_F( elCircularBuffer_test, EmplaceOnOverflow )
{
    for ( int i = 0; i < 3; ++i )
        EXPECT_THAT( sut.Push( i ).has_value(), Eq( false ) );

    auto result = sut.Push( 4 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( *result, Eq( 0 ) );

    result = sut.Push( 5 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( *result, Eq( 1 ) );
}

TEST_F( elCircularBuffer_test, Pop )
{
    for ( int i = 0; i < 3; ++i )
        EXPECT_THAT( sut.Push( i ).has_value(), Eq( false ) );

    for ( int i = 0; i < 3; ++i )
    {
        auto result = sut.Pop();
        ASSERT_THAT( result.has_value(), Eq( true ) );
        EXPECT_THAT( *result, Eq( i ) );
    }

    auto result = sut.Pop();
    ASSERT_THAT( result.has_value(), Eq( false ) );
}

TEST_F( elCircularBuffer_test, PopAfterOverflow )
{
    for ( int i = 0; i < 3; ++i )
        EXPECT_THAT( sut.Push( i ).has_value(), Eq( false ) );
    for ( int i = 3; i < 6; ++i )
        EXPECT_THAT( sut.Push( i ).has_value(), Eq( true ) );

    for ( int i = 0; i < 3; ++i )
    {
        auto result = sut.Pop();
        ASSERT_THAT( result.has_value(), Eq( true ) );
        EXPECT_THAT( *result, Eq( i + 3 ) );
    }

    auto result = sut.Pop();
    ASSERT_THAT( result.has_value(), Eq( false ) );
}

TEST_F( elCircularBuffer_test, MultipopZeroElementsReturnsAllElements )
{
    for ( int i = 0; i < 3; ++i )
        sut.Push( i );

    auto result = sut.MultiPop< vec_t >( 0 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 3 ) );
}

TEST_F( elCircularBuffer_test, MultipopOneElement )
{
    for ( int i = 0; i < 3; ++i )
        sut.Push( i );

    auto result = sut.MultiPop< vec_t >( 1 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 1 ) );
    EXPECT_THAT( result->at( 0 ), Eq( 0 ) );
}

TEST_F( elCircularBuffer_test, MultipopMultipleElement )
{
    for ( int i = 0; i < 3; ++i )
        sut.Push( i + 5 );

    auto result = sut.MultiPop< vec_t >( 2 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 2 ) );
    EXPECT_THAT( result->at( 0 ), Eq( 5 ) );
    EXPECT_THAT( result->at( 1 ), Eq( 6 ) );
}

TEST_F( elCircularBuffer_test, MultipopMultipleElementAfterOverflowWhenFull )
{
    for ( int i = 0; i < 4; ++i )
        sut.Push( i * 5 );

    auto result = sut.MultiPop< vec_t >( 3 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 3 ) );
    EXPECT_THAT( result->at( 0 ), Eq( 5 ) );
    EXPECT_THAT( result->at( 1 ), Eq( 10 ) );
    EXPECT_THAT( result->at( 2 ), Eq( 15 ) );
}

TEST_F( elCircularBuffer_test, MultipopMultipleElementAfterOverflowWhenNotFull )
{
    for ( int i = 0; i < 4; ++i )
        sut.Push( i + 7 );

    EXPECT_THAT( sut.Pop().value(), Eq( 8 ) );
    auto result = sut.MultiPop< vec_t >( 2 );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    EXPECT_THAT( result->size(), Eq( 2 ) );
    EXPECT_THAT( result->at( 0 ), Eq( 9 ) );
    EXPECT_THAT( result->at( 1 ), Eq( 10 ) );
}

TEST_F( elCircularBuffer_test, MultiPushWithSpaceLeft )
{
    EXPECT_THAT( sut.MultiPush( vec_t{1, 2} ).has_value(), Eq( false ) );
}

TEST_F( elCircularBuffer_test, MultiPushToFull )
{
    sut.Push( 0 );
    EXPECT_THAT( sut.MultiPush( vec_t{1, 2} ).has_value(), Eq( false ) );
}

TEST_F( elCircularBuffer_test, MultiPushOverflow )
{
    sut.Push( 0 );
    sut.Push( 1 );

    auto result = sut.MultiPush( vec_t{2, 3, 4} );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result->size(), Eq( 2 ) );
    ASSERT_THAT( result->at( 0 ), Eq( 0 ) );
    ASSERT_THAT( result->at( 1 ), Eq( 1 ) );
}

TEST_F( elCircularBuffer_test, MultiPushMoreThanCapacity )
{
    sut.Push( 0 );
    sut.Push( 1 );

    auto result = sut.MultiPush( vec_t{2, 3, 4, 5, 6, 7} );
    ASSERT_THAT( result.has_value(), Eq( true ) );
    ASSERT_THAT( result->size(), Eq( 5 ) );
    for ( int i = 0; i < 5; i++ )
        EXPECT_THAT( result->at( static_cast< size_t >( i ) ), Eq( i ) );
}

TEST_F( elCircularBuffer_test, GetContentWhenEmpty )
{
    EXPECT_THAT( sut.GetContent< vec_t >().size(), Eq( 0 ) );
}

TEST_F( elCircularBuffer_test, GetContentWithSomeElements )
{
    sut.Push( 141 );
    sut.Push( 782 );

    auto content = sut.GetContent< vec_t >();
    ASSERT_THAT( content.size(), Eq( 2 ) );
    EXPECT_THAT( content[0], Eq( 141 ) );
    EXPECT_THAT( content[1], Eq( 782 ) );
}

TEST_F( elCircularBuffer_test, GetContentWhenFull )
{
    sut.Push( 9141 );
    sut.Push( 9782 );
    sut.Push( 92 );

    auto content = sut.GetContent< vec_t >();
    ASSERT_THAT( content.size(), Eq( 3 ) );
    EXPECT_THAT( content[0], Eq( 9141 ) );
    EXPECT_THAT( content[1], Eq( 9782 ) );
    EXPECT_THAT( content[2], Eq( 92 ) );
}

TEST_F( elCircularBuffer_test, GetContentWhenOverflowing )
{
    sut.Push( 39141 );
    sut.Push( 39782 );
    sut.Push( 392 );
    sut.Push( 1392 );

    auto content = sut.GetContent< vec_t >();
    ASSERT_THAT( content.size(), Eq( 3 ) );
    EXPECT_THAT( content[0], Eq( 39782 ) );
    EXPECT_THAT( content[1], Eq( 392 ) );
    EXPECT_THAT( content[2], Eq( 1392 ) );
}
