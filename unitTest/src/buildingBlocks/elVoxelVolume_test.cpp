#include "buildingBlocks/elVoxelVolume.hpp"
#include "unitTest.hpp"

using namespace ::el3D::bb;
using namespace ::testing;

namespace
{
struct TestClass
{
    TestClass() = default;
    TestClass( const uint64_t a, const uint64_t b, const uint64_t c ) noexcept
        : a{ a }, b{ b }, c{ c }
    {
    }

    auto
    operator<=>( const TestClass& ) const noexcept = default;

    uint64_t a = 0, b = 0, c = 0;
};

class elVoxelVolume_test : public ::testing::Test
{
  public:
    elVoxelVolume< TestClass > sut;
    using position_t = typename elVoxelVolume< TestClass >::position_t;
};
} // namespace


TEST_F( elVoxelVolume_test, IsEmptyOnCreation )
{
    EXPECT_THAT( this->sut.GetConstContents().size(), Eq( 0 ) );
    EXPECT_THAT( this->sut.GetContents().size(), Eq( 0 ) );
}

TEST_F( elVoxelVolume_test, InsertOneElementWorks )
{
    this->sut.Insert( position_t{ 0, 5, 1 }, 1U, 2U, 3U );

    auto constContents = this->sut.GetConstContents();
    auto contents      = this->sut.GetContents();

    ASSERT_THAT( constContents.size(), Eq( 1 ) );
    ASSERT_THAT( contents.size(), Eq( 1 ) );

    EXPECT_THAT( constContents[0].data, Eq( TestClass{ 1U, 2U, 3U } ) );
    EXPECT_THAT( contents[0].data, Eq( TestClass{ 1U, 2U, 3U } ) );
    EXPECT_THAT( this->sut.Get( { 0, 5, 1 } ), Eq( TestClass{ 1, 2, 3 } ) );
}

TEST_F( elVoxelVolume_test, InsertMultipleElementWorks )
{
    constexpr uint64_t NUMBER_OF_ELEMENTS = 100;
    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; ++i )
        this->sut.Insert( { i + 1, i + 2, i + 3 }, i + 4, i + 5, i + 6 );

    auto constContents = this->sut.GetConstContents();
    auto contents      = this->sut.GetContents();

    ASSERT_THAT( constContents.size(), Eq( NUMBER_OF_ELEMENTS ) );
    ASSERT_THAT( contents.size(), Eq( NUMBER_OF_ELEMENTS ) );

    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; ++i )
    {
        EXPECT_THAT( constContents[i].data,
                     Eq( TestClass{ i + 4, i + 5, i + 6 } ) );
        EXPECT_THAT( contents[i].data, Eq( TestClass{ i + 4, i + 5, i + 6 } ) );
        EXPECT_THAT( this->sut.Get( { i + 1, i + 2, i + 3 } ),
                     Eq( TestClass{ i + 4, i + 5, i + 6 } ) );
    }
}

TEST_F( elVoxelVolume_test, RemoveRandomElementsWorks )
{
    constexpr uint64_t NUMBER_OF_ELEMENTS = 100;
    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; ++i )
        this->sut.Insert( { i + 1, i + 2, i + 3 }, i + 4, i + 5, i + 6 );

    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; i += 2 )
        this->sut.Remove( { i + 1, i + 2, i + 3 } );

    auto constContents = this->sut.GetConstContents();
    auto contents      = this->sut.GetContents();

    ASSERT_THAT( constContents.size(), Eq( NUMBER_OF_ELEMENTS / 2 ) );
    ASSERT_THAT( contents.size(), Eq( NUMBER_OF_ELEMENTS / 2 ) );

    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS / 2; ++i )
    {
        uint64_t adjustedI = 2 * i + 1;
        EXPECT_THAT(
            constContents[i].data,
            Eq( TestClass{ adjustedI + 4, adjustedI + 5, adjustedI + 6 } ) );
        EXPECT_THAT(
            contents[i].data,
            Eq( TestClass{ adjustedI + 4, adjustedI + 5, adjustedI + 6 } ) );
        EXPECT_THAT(
            this->sut.Get( { adjustedI + 1, adjustedI + 2, adjustedI + 3 } ),
            Eq( TestClass{ adjustedI + 4, adjustedI + 5, adjustedI + 6 } ) );
    }
}

TEST_F( elVoxelVolume_test, AccessAfterSortWorks )
{
    constexpr uint64_t NUMBER_OF_ELEMENTS = 100;
    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; ++i )
    {
        uint64_t n = NUMBER_OF_ELEMENTS - i + 6;
        this->sut.Insert( { n - 1, n - 2, n - 3 }, n - 4, n - 5, n - 6 );
    }

    this->sut.SortContents();

    for ( uint64_t i = 0; i < NUMBER_OF_ELEMENTS; ++i )
    {
        uint64_t n = NUMBER_OF_ELEMENTS - i + 6;
        EXPECT_THAT( this->sut.Get( { n - 1, n - 2, n - 3 } ),
                     Eq( TestClass( n - 4, n - 5, n - 6 ) ) );
    }
}
