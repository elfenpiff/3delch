#include "buildingBlocks/elOctree.hpp"
#include "unitTest.hpp"

#include <bits/stdint-uintn.h>

using namespace ::el3D::bb;
using namespace ::testing;

class elOctree_test : public ::testing::Test
{
  public:
    virtual void
    SetUp()
    {
    }

    bool
    TreeContains( const std::vector< elOctree< int >::cube_t >& tree,
                  const glm::vec3& center, const float halfSideLength )
    {
        for ( auto& c : tree )
        {
            if ( glm::distance( c.center, center ) < EPSILON &&
                 fabsf( c.halfSideLength - halfSideLength ) < EPSILON )
                return true;
        }

        return false;
    }

    bool
    Contains(
        const std::vector< typename elOctree< int >::element_t >& elements,
        const glm::vec3&                                          point )
    {
        for ( auto& e : elements )
        {
            if ( glm::distance( e.point, point ) < EPSILON ) return true;
        }
        return false;
    }

    static constexpr float EPSILON = 0.001f;

    elOctree< int > sut{ 0.1f };
};

TEST_F( elOctree_test, ElementContainedAfterInsert )
{
    glm::vec3 p{ 1.0f, 2.0f, 3.0f };
    sut.Insert( p, 0 );
    auto tree = sut.GetTree();

    EXPECT_TRUE( sut.DoesContain( p ) );
    EXPECT_THAT( tree.size(), Eq( 1 ) );
    EXPECT_TRUE( this->TreeContains( tree, p, 0.0f ) );
}

TEST_F( elOctree_test, AddingSecondElementPartsRootNote )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 1.0f, 2.0f, 3.0f }, 0 );

    EXPECT_TRUE( sut.DoesContain( { 1.0f, 1.0f, 1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 1.0f, 2.0f, 3.0f } ) );

    auto tree = sut.GetTree();
    EXPECT_THAT( tree.size(), Eq( 3 ) );

    // root node
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.0f, 1.5f, 2.0f }, 2.0f ) );
    // contains 1.0f, 1.0f, 1.0f
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.0f, 0.5f, 1.0f }, 1.0f ) );
    // contains 1.0f, 2.0f, 3.0f
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.0f, 2.5f, 3.0f }, 1.0f ) );
}

TEST_F( elOctree_test,
        InsertBroadLevelOfElementPartsRootNodeInCorrectAmountOfLeaves )
{
    std::vector< glm::vec3 > points{
        { 1.0f, 1.0f, 1.0f },   { -1.0f, -1.0f, -1.0f }, { -1.0f, 1.0f, 1.0f },
        { 1.0f, -1.0f, 1.0f },  { -1.0f, -1.0f, 1.0f },  { 1.0f, 1.0f, -1.0f },
        { -1.0f, 1.0f, -1.0f }, { 1.0f, -1.0f, -1.0f } };

    auto& p = points[0];
    sut.Insert( p, 0 );

    for ( uint64_t i = 1; i < points.size(); ++i )
    {
        auto& p = points[i];
        sut.Insert( p, 0 );

        for ( uint64_t k = 0; k <= i; ++k )
            EXPECT_TRUE( sut.DoesContain( points[k] ) );
        for ( uint64_t k = i + 1; k < points.size(); ++k )
            EXPECT_FALSE( sut.DoesContain( points[k] ) );

        auto tree = sut.GetTree();
        EXPECT_THAT( tree.size(), Eq( i + 2 ) );
        // root
        EXPECT_TRUE(
            this->TreeContains( tree, glm::vec3{ 0.0f, 0.0f, 0.0f }, 2.0f ) );

        // -1.0, -1.0, -1.0
        EXPECT_TRUE( this->TreeContains( tree, glm::vec3{ -1.0f, -1.0f, -1.0f },
                                         1.0f ) );

        if ( i > 1 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ -1.0f, 1.0f, 1.0f }, 1.0f ) );
        }
        if ( i > 2 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ 1.0f, -1.0f, 1.0f }, 1.0f ) );
        }
        if ( i > 3 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ -1.0f, -1.0f, 1.0f }, 1.0f ) );
        }
        if ( i > 4 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ 1.0f, 1.0f, -1.0f }, 1.0f ) );
        }
        if ( i > 5 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ -1.0f, 1.0f, -1.0f }, 1.0f ) );
        }
        if ( i > 6 )
        {
            EXPECT_TRUE( this->TreeContains(
                tree, glm::vec3{ 1.0f, -1.0f, -1.0f }, 1.0f ) );
        }
    }
}

TEST_F( elOctree_test,
        InsertDepthLevelOfElementPartsRootTreeInCorrectAmountOfLeaves )
{
    sut.Insert( { 10.0f, 10.0f, 10.0f }, 0 );
    sut.Insert( { -10.0f, -10.0f, -10.0f }, 0 );
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );

    EXPECT_TRUE( sut.DoesContain( { 10.0f, 10.0f, 10.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { -10.0f, -10.0f, -10.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 1.0f, 1.0f, 1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 2.0f, 2.0f, 2.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 2.0f, 2.0f, 1.0f } ) );

    auto tree = sut.GetTree();
    EXPECT_THAT( tree.size(), Eq( 10 ) );

    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.0f, 0.0f, 0.0f }, 20.0f ) );
    // contains -10.0, -10.0, -10.0
    EXPECT_TRUE( this->TreeContains( tree, glm::vec3{ -10.0f, -10.0f, -10.0f },
                                     10.0f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 10.0f, 10.0f, 10.0f }, 10.0f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 5.0f, 5.0f, 5.0f }, 5.0f ) );
    // contains 10.0, 10.0, 10.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 7.5f, 7.5f, 7.5f }, 2.5f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 2.5f, 2.5f, 2.5f }, 2.5f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.25f, 1.25f, 1.25f }, 1.25f ) );

    // contains {1.0, 1.0, 1.0}
    EXPECT_TRUE( this->TreeContains( tree, glm::vec3{ 0.625f, 0.625f, 0.625f },
                                     0.625f ) );
    // contains {2.0, 2.0, 2.0}
    EXPECT_TRUE( this->TreeContains( tree, glm::vec3{ 1.875f, 1.875f, 1.875f },
                                     0.625f ) );
    // contains {2.0, 2.0, 1.0}
    EXPECT_TRUE( this->TreeContains( tree, glm::vec3{ 1.875f, 1.875f, 0.625f },
                                     0.625f ) );
}

TEST_F(
    elOctree_test,
    InsertingElementsWithIncreasingDistanceCreatesNoNewRootNodesWhenInRange )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { -1.0f, -1.0f, -1.0f }, 0 );
    sut.Insert( { 1.5f, 1.5f, 1.5f }, 0 );

    EXPECT_TRUE( sut.DoesContain( { 1.0f, 1.0f, 1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { -1.0f, -1.0f, -1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 1.5f, 1.5f, 1.5f } ) );

    auto tree = sut.GetTree();
    EXPECT_THAT( tree.size(), Eq( 5 ) );

    // root node
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.0f, 0.0f, 0.0f }, 2.0f ) );

    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.0f, 1.0f, 1.0f }, 1.0f ) );

    // contains 1.0, 1.0, 1.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.5f, 0.5f, 0.5f }, 0.5f ) );

    // contains 1.5, 1.5, 1.5
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.5f, 1.5f, 1.5f }, 0.5f ) );

    // contains -1.0, -1.0, -1.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ -1.0f, -1.0f, -1.0f }, 1.0f ) );
}

TEST_F(
    elOctree_test,
    InsertingElementsWithLargerIncreasingDistanceCreatesMultipleNewRootNodes )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { -1.0f, -1.0f, -1.0f }, 0 );
    sut.Insert( { 9.0f, 9.0f, 0.0f }, 0 );

    EXPECT_TRUE( sut.DoesContain( { 1.0f, 1.0f, 1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { -1.0f, -1.0f, -1.0f } ) );
    EXPECT_TRUE( sut.DoesContain( { 9.0f, 9.0f, 0.0f } ) );

    auto tree = sut.GetTree();
    EXPECT_THAT( tree.size(), Eq( 6 ) );

    // root node
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 6.0f, 6.0f, -2.0f }, 8.0f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 2.0f, 2.0f, 2.0f }, 4.0f ) );
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 0.0f, 0.0f, 0.0f }, 2.0f ) );
    // contains -1.0, -1.0, -1.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ -1.0f, -1.0f, -1.0f }, 1.0f ) );
    // contains 1.0, 1.0, 1.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.0f, 1.0f, 1.0f }, 1.0f ) );

    // contains 9.0, 9.0, 0.0
    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 10.0f, 10.0f, 2.0f }, 4.0f ) );
}

TEST_F( elOctree_test, InsertingOnlyTwoIdenticalElements )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );

    EXPECT_TRUE( sut.DoesContain( { 1.0f, 1.0f, 1.0f } ) );

    auto tree = sut.GetTree();
    EXPECT_THAT( tree.size(), Eq( 1 ) );

    EXPECT_TRUE(
        this->TreeContains( tree, glm::vec3{ 1.0f, 1.0f, 1.0f }, 0.0f ) );
}

TEST_F( elOctree_test, HasNoNeighborsWhenEmpty )
{
    EXPECT_FALSE( sut.HasNeighbors( { 0.0f, 0.0f, 0.0f }, 10.0f ) );
}

TEST_F( elOctree_test, HasNeighborsWithSingleElementAndRangeContainsEverything )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    EXPECT_TRUE( sut.HasNeighbors( { 0.0f, 0.0f, 0.0f }, 10.0f ) );
}

TEST_F( elOctree_test,
        HasNeighborsWithMultipleElementAndRangeContainsEverything )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 1.0f, 1.0f, -1.0f }, 0 );
    EXPECT_TRUE( sut.HasNeighbors( { 0.0f, 0.0f, 0.0f }, 10.0f ) );
}

TEST_F( elOctree_test, HasNeighborsWithPartiallyOverlappingBox )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );

    EXPECT_TRUE( sut.HasNeighbors( { 2.2f, 2.2f, 1.2f }, 0.3f ) );
}

TEST_F( elOctree_test, HasNoNeighborsWhenBoxOverlapsButToFarAway )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );

    EXPECT_FALSE( sut.HasNeighbors( { 1.8f, 1.8f, 1.8f }, 0.15f ) );
}

TEST_F( elOctree_test, GetNeighborsRetrievesNothingWhenEmpty )
{
    EXPECT_TRUE( sut.GetNeighbors( { 0.0f, 0.0f, 0.0f }, 10.0f ).empty() );
}

TEST_F( elOctree_test, GetNeighborsRetrievesEverythingWhenGreatEnough )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );
    sut.Insert( { 9.0f, 7.0f, 1.0f }, 0 );
    sut.Insert( { -8.0f, 7.0f, 6.0f }, 0 );
    sut.Insert( { 1.0f, -3.0f, 1.0f }, 0 );

    auto neighbors = sut.GetNeighbors( { 0.0f, 0.0f, 0.0f }, 10.0f );

    EXPECT_THAT( neighbors.size(), Eq( 6 ) );
    EXPECT_TRUE( Contains( neighbors, { 1.0f, 1.0f, 1.0f } ) );
    EXPECT_TRUE( Contains( neighbors, { 2.0f, 2.0f, 2.0f } ) );
    EXPECT_TRUE( Contains( neighbors, { 2.0f, 2.0f, 1.0f } ) );
    EXPECT_TRUE( Contains( neighbors, { 9.0f, 7.0f, 1.0f } ) );
    EXPECT_TRUE( Contains( neighbors, { -8.0f, 7.0f, 6.0f } ) );
    EXPECT_TRUE( Contains( neighbors, { 1.0f, -3.0f, 1.0f } ) );
}

TEST_F( elOctree_test, GetNeighborsRetrievesNothingWhenToSmall )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );
    sut.Insert( { 9.0f, 7.0f, 1.0f }, 0 );
    sut.Insert( { -8.0f, 7.0f, 6.0f }, 0 );
    sut.Insert( { 1.0f, -3.0f, 1.0f }, 0 );

    auto neighbors = sut.GetNeighbors( { 0.5f, 0.5f, 0.5f }, 0.4f );

    EXPECT_THAT( neighbors.size(), Eq( 0 ) );
}

TEST_F( elOctree_test, GetNeighborsRetrievesEverythingInLMAXNormRadius )
{
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );
    sut.Insert( { 9.0f, 7.0f, 1.0f }, 0 );
    sut.Insert( { -8.0f, 7.0f, 6.0f }, 0 );
    sut.Insert( { 1.0f, -3.0f, 1.0f }, 0 );

    auto neighbors = sut.GetNeighbors( { 1.0f, 1.0f, 1.0f }, 1.0f + EPSILON );

    EXPECT_THAT( neighbors.size(), Eq( 3 ) );
    sut.Insert( { 1.0f, 1.0f, 1.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 2.0f }, 0 );
    sut.Insert( { 2.0f, 2.0f, 1.0f }, 0 );
}
