#include "lua/elConfigHandler.hpp"
#include "unitTest.hpp"

using namespace ::el3D::lua;
class elConfigHandler_test : public ::testing::Test
{
    virtual void
    SetUp()
    {
    }
};

TEST_F( elConfigHandler_test, SetConfigFile )
{
    elConfigHandler cfg;
    testing::internal::CaptureStderr();
    EXPECT_TRUE( cfg.SetConfig( "a2.lua" ).HasError() );
    EXPECT_TRUE( cfg.SetConfig( "a3.fuu" ).HasError() );
    std::string output = testing::internal::GetCapturedStderr();
}

TEST_F( elConfigHandler_test, SetConfigDirectory )
{
    elConfigHandler cfg;
    testing::internal::CaptureStderr();
    ASSERT_FALSE(
        cfg.SetConfig( std::string( UNIT_TEST_DIR ) + "cfg/" ).HasError() );
    testing::internal::GetCapturedStderr();

    auto                    s   = cfg.GetConfigFileList();
    std::set< std::string > fuu = {
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "fuu" +
            PATH_SEPARATOR + "fuu1.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "fuu" +
            PATH_SEPARATOR + "fuu2.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "fuu2" +
            PATH_SEPARATOR + "fuu3.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "fuu2" +
            PATH_SEPARATOR + "fuu4.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test1.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test2.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test3.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test4.lua"};

    for ( auto e : fuu )
        EXPECT_NE( s.find( e ), s.end() );

    cfg.ClearFiles();
    testing::internal::CaptureStderr();
    ASSERT_FALSE( cfg.SetConfig( std::string( UNIT_TEST_DIR ) + "cfg/", false )
                      .HasError() );
    testing::internal::GetCapturedStderr();

    std::set< std::string > fuu2 = {
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test1.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test2.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test3.lua",
        std::string( UNIT_TEST_DIR ) + "cfg" + PATH_SEPARATOR + "test4.lua"};

    s = cfg.GetConfigFileList();
    for ( auto e : fuu2 )
        EXPECT_NE( s.find( e ), s.end() );

    testing::internal::CaptureStderr();
    EXPECT_TRUE( cfg.SetConfig( "/some/crap" ).HasError() );
    testing::internal::GetCapturedStderr();
}

TEST_F( elConfigHandler_test, Get )
{
    elConfigHandler cfg;
    testing::internal::CaptureStderr();
    ASSERT_FALSE(
        cfg.SetConfig( std::string( UNIT_TEST_DIR ) + "cfg/" ).HasError() );
    ASSERT_FALSE( cfg.SetConfig( std::string( UNIT_TEST_DIR ) + "/config.lua" )
                      .HasError() );
    testing::internal::GetCapturedStderr();

    EXPECT_EQ( cfg.Get< int >( {"t1Var1"}, {0} )[0], 123 );
    EXPECT_EQ( cfg.Get< bool >( {"t1Var2"}, {true} )[0], true );
    EXPECT_EQ( cfg.Get< double >( {"t1Var3"}, {0.0} )[0], 123.123 );
    EXPECT_EQ( cfg.Get< std::string >( {"t1Var4"}, {"hello"} )[0], "hello" );

    EXPECT_EQ( cfg.Get< std::string >( {"global", "renderer", "deferred",
                                        "passThroughShader", "file"} )[0],
               "glsl/passThrough.glsl" );
    EXPECT_EQ( cfg.Get< std::string >( {"global", "renderer", "deferred",
                                        "passThroughShader", "group"} )[0],
               "pointLightDeferred" );
}
