#include "3Delch/elScene.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::_3Delch;
using namespace ::el3D;

class elScene_test : public Test
{
  public:
    virtual void
    SetUp()
    {
    }

    virtual void
    TearDown()
    {
    }

    std::string                             name          = "main";
    int                                     shaderVersion = 440;
    std::unique_ptr< lua::elConfigHandler > config =
        lua::elConfigHandler::Create( std::string( UNIT_TEST_DIR ) +
                                      "elSceneConfig.lua" )
            .GetValue();
    std::unique_ptr< GLAPI::elWindow > window =
        GLAPI::elWindow::Create( 1, 1, "elScene_test", 4, 4, true, 0u, true )
            .GetValue();
    RenderPipeline::elRenderPipelineManager pipelineManager{ window.get(), 1.0,
                                                             config.get() };
    GLAPI::elFontCache                      fontCache;
    GLAPI::elEventHandler                   eventHandler;

    elScene sut{ name, config.get(), window.get(), &pipelineManager,
                 &fontCache };
};

TEST_F( elScene_test, GetTextureArraySize )
{
    // auto texSize = sut.GetTextureArraySize< RenderTarget::Forward >(
    //     OpenGL::DrawMode::Triangles );
    // EXPECT_NE( texSize.arraySize, 0 );
}
