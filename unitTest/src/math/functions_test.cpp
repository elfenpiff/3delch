#include "math/functions.hpp"
#include "unitTest.hpp"

using namespace ::el3D;
using namespace ::el3D::math;
using namespace ::testing;

TEST( math_functions_test, NextPowerOfTwo )
{
    EXPECT_THAT( NextPowerOfTwo( 0 ), Eq( 1 ) );
    EXPECT_THAT( NextPowerOfTwo( 1 ), Eq( 1 ) );
    EXPECT_THAT( NextPowerOfTwo( 2 ), Eq( 2 ) );
    EXPECT_THAT( NextPowerOfTwo( 3 ), Eq( 4 ) );
    EXPECT_THAT( NextPowerOfTwo( 4 ), Eq( 4 ) );
    EXPECT_THAT( NextPowerOfTwo( 5 ), Eq( 8 ) );
    EXPECT_THAT( NextPowerOfTwo( 6 ), Eq( 8 ) );
    EXPECT_THAT( NextPowerOfTwo( 7 ), Eq( 8 ) );
    EXPECT_THAT( NextPowerOfTwo( 8 ), Eq( 8 ) );

    EXPECT_THAT( NextPowerOfTwo( 1073741826 ), Eq( 2147483648 ) );
    EXPECT_THAT( NextPowerOfTwo( 2147483648 ), Eq( 2147483648 ) );

    EXPECT_THAT( NextPowerOfTwo( 34359738378 ), Eq( 68719476736 ) );
    EXPECT_THAT( NextPowerOfTwo( 68719476736 ), Eq( 68719476736 ) );
}
