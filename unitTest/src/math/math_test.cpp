#include "math/math.hpp"
#include "unitTest.hpp"

using namespace ::el3D;
using namespace ::el3D::math;
using namespace ::testing;

TEST( math_math_test, ClosestPoint )
{
    auto point = ClosestPoint( { 1.0f, 1.0f, 1.0f }, { 5.0f, 0.0f, 0.0f },
                               { 4.0f, 4.0f, 1.0f } );

    EXPECT_THAT( point.x, FloatEq( 4.0f ) );
    EXPECT_THAT( point.y, FloatEq( 1.0f ) );
    EXPECT_THAT( point.z, FloatEq( 1.0f ) );
}
