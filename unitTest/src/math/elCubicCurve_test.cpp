#include "math/elCubicCurve.hpp"
#include "unitTest.hpp"

#include <glm/glm.hpp>

using namespace ::el3D;
using namespace ::el3D::math;
using namespace ::testing;

template < typename T >
class elCubicCurve_test : public Test
{
  public:
    using Type    = T;
    using SutType = elCubicCurve< T >;
};

using Implementations = Types< glm::vec3, glm::vec2, glm::vec4, float >;
TYPED_TEST_SUITE( elCubicCurve_test, Implementations );

TYPED_TEST( elCubicCurve_test, Evaluate )
{
    typename TestFixture::Type    a( 1 ), b( 2 ), c( 3 ), d( 4 );
    typename TestFixture::SutType sut( a, b, c, d );
    EXPECT_EQ( sut( 0 ), d );
    EXPECT_EQ( sut( 1 ), a + b + c + d );
}

TYPED_TEST( elCubicCurve_test, EvaluateDerivate )
{
    typename TestFixture::Type    a( 1 ), b( 2 ), c( 3 ), d( 4 );
    typename TestFixture::SutType sut( a, b, c, d );
    EXPECT_EQ( sut.GetValueOfDerivate_1st( 0 ), c );
    EXPECT_EQ( sut.GetValueOfDerivate_1st( 1 ), 3.0f * a + 2.0f * b + c );

    EXPECT_EQ( sut.GetValueOfDerivate_2nd( 0 ), 2.0f * b );
    EXPECT_EQ( sut.GetValueOfDerivate_2nd( 1 ), 6.0f * a + 2.0f * b );
}
