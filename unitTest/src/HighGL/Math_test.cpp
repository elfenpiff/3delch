#include "HighGL/Math.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "unitTest.hpp"

using namespace ::el3D;
using namespace ::el3D::HighGL;

TEST( Math_test, GenerateAdjacencyListForTetraeder )
{
    const std::vector< GLfloat > tetraederVertices{
        -1.0,  -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,  -1.0f };

    const std::vector< GLuint > tetraederElements{ 0, 1, 2, 0, 1, 3,
                                                   1, 2, 3, 2, 0, 3 };

    auto                  list = GenerateAdjacencyList( tetraederElements );
    std::vector< GLuint > result{ 0, 3, 1, 3, 2, 3, 0, 2, 1, 2, 3, 2,
                                  1, 0, 2, 0, 3, 0, 2, 1, 0, 1, 3, 1 };

    EXPECT_EQ( list, result );
}

TEST( Math_test, GenerateAdjacencyListForPyramid )
{
    auto list =
        GenerateAdjacencyList( HighGL::elObjectGeometryBuilder::CreatePyramid()
                                   .GetObjectGeometry()
                                   .elements );

    std::vector< GLuint > result{ 0,  3, 2,  6, 1,  6, 2,  1, 0,  6, 3,  6,
                                  4,  2, 5,  2, 6,  3, 7,  0, 8,  3, 9,  0,
                                  10, 0, 11, 0, 12, 1, 13, 2, 14, 1, 15, 2 };

    EXPECT_EQ( list, result );
}

TEST( Math_test, Benchmark )
{
    long double totalTime  = 0.0;
    uint64_t    repetition = 5;

    auto pyramid =
        HighGL::elObjectGeometryBuilder::CreatePyramid().GetObjectGeometry();

    for ( uint64_t i = 0u; i < repetition; ++i )
    {
        uint64_t numberOfElements    = 1200u;
        uint64_t pyramidVerticesSize = pyramid.vertices.size();

        std::vector< GLfloat > vertices( numberOfElements *
                                         pyramidVerticesSize );
        std::vector< GLuint >  elements( numberOfElements *
                                         pyramid.elements.size() );

        for ( uint64_t i = 0; i < pyramidVerticesSize; ++i )
            for ( uint64_t n = 0; n < numberOfElements; ++n )
                vertices[i + pyramidVerticesSize * n] =
                    pyramid.vertices[i] +
                    static_cast< float >( pyramidVerticesSize * n * 10 );

        for ( uint64_t i = 0, limit = pyramid.elements.size(); i < limit; ++i )
            for ( uint64_t n = 0; n < numberOfElements; ++n )
                elements[i + limit * n] =
                    pyramid.elements[i] +
                    static_cast< GLuint >( n * pyramidVerticesSize / 3 );

        auto start = units::Time::TimeSinceEpoch();
        std::cout << "vertices: " << vertices.size()
                  << "   elements: " << elements.size() << std::endl;
        auto list     = GenerateAdjacencyList( elements );
        auto duration = units::Time::TimeSinceEpoch() - start;
        totalTime += duration.GetSeconds();
        std::cout << duration << std::endl;
    }

    std::cout << "average " << totalTime / repetition << "s\n";
}
