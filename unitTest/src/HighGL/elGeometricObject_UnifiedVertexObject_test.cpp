#include "GLAPI/elWindow.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "unitTest.hpp"

using namespace ::el3D;
using namespace ::el3D::HighGL;
using namespace ::el3D::OpenGL;
using namespace ::el3D::GLAPI;
using namespace ::testing;

std::string fragShader( R"(
#version 440
uniform sampler2DArray textureArray;
flat in int _diffuse;
in vec2 _texCoord;
in vec3 _fuu;

out vec4 color1;
void main() {
    color1 = texture(textureArray, vec3(_texCoord, _diffuse) + _fuu);
}
)" );

std::string vertexShader( R"(
#version 440
in vec3 coord;
in vec3 normal;
in vec3 tangent;
in vec3 bitangent;
in vec2 texCoord;
in int objectID;
in int diffuse;

flat out int _diffuse;
out vec2 _texCoord;
out vec3 _fuu;
uniform samplerBuffer uniformBuffer;

void main() {
    vec4 fuu = texelFetch(uniformBuffer, objectID);
    gl_Position = vec4(coord, 1.0 + fuu.x);
    _texCoord = texCoord;
    _diffuse  = diffuse;
    _fuu = normal + tangent + bitangent;
}
)" );

class elGeometricObject_UnifiedVertexObject_test : public ::testing::Test
{
  public:
    class SUT : public ::el3D::HighGL::elGeometricObject_UnifiedVertexObject
    {
      public:
        SUT( const OpenGL::elShaderProgram* shader, const GLsizei sizeX,
             const GLsizei sizeY, const GLsizei texArraySize )
        noexcept
            : elGeometricObject_UnifiedVertexObject(
                  OpenGL::DrawMode::Triangles, shader,
                  static_cast< uint64_t >( sizeX ),
                  static_cast< uint64_t >( sizeY ),
                  static_cast< uint64_t >( texArraySize ) )
        {
        }

        void
        RunOncePerFrame() const noexcept
        {
            this->UpdateObjectModelMatrix();
            elGeometricObject_UnifiedVertexObject::RunOncePerFrame();
        }

        size_t
        GetBufferID(
            const enum UnifiedVertexObject::floatBufferList_t v ) const noexcept
        {
            return elGeometricObject_UnifiedVertexObject::GetBufferID( v );
        }

        size_t
        GetBufferID(
            const enum UnifiedVertexObject::uintBufferList_t v ) const noexcept
        {
            return elGeometricObject_UnifiedVertexObject::GetBufferID( v );
        }

        OpenGL::elObjectBuffer&
        GetObjectBuffer()
        {
            return this->objectBuffer;
        }

        uniformBuffer_t&
        GetUniformBuffer()
        {
            return this->uniformBuffer;
        }
    };

    virtual void
    SetUp()
    {
    }

    virtual void
    TearDown()
    {
    }

    template < typename T >
    std::vector< T >
    Data( const uint64_t numberOfData = 0, const T value = 0 )
    {
        return std::vector< T >( numberOfData, value );
    }

    glm::mat4
    Matrix( const float value )
    {
        glm::mat4 m;
        for ( int i = 0; i < 4; ++i )
            for ( int k = 0; k < 4; ++k )
                m[i][k] = value;
        return m;
    }

    std::unique_ptr< elWindow > window =
        elWindow::Create( 1, 1, "elBufferObject_test", 4, 4, true, 0u, true )
            .GetValue();
    std::unique_ptr< elShaderProgram > shader =
        elShaderProgram::Create( OpenGL::shaderFromSource, vertexShader,
                                 fragShader )
            .GetValue();

    SUT sut{ shader.get(), 1, 1, 1 };
};

TEST_F( elGeometricObject_UnifiedVertexObject_test,
        ComplexCreateAndResetInReverseOrder )
{
    glm::mat4              m1 = Matrix( 1.0f );
    glm::mat4              m2 = Matrix( 2.0f );
    std::vector< GLfloat > empty;
    auto object1 = sut.CreateObject( Data( 12, 1.0f ), empty, empty, empty,
                                     empty, Data< GLuint >( 10, 1 ) );
    object1->SetModelMatrix( m1 );
    sut.RunOncePerFrame();
    auto buffer = sut.GetUniformBuffer().data;
    ASSERT_THAT( buffer.size(), Ge( 16 ) );

    auto object2 = sut.CreateObject( Data( 12, 2.0f ), empty, empty, empty,
                                     empty, Data< GLuint >( 10, 2 ) );
    object2->SetModelMatrix( m2 );
    sut.RunOncePerFrame();
    buffer = sut.GetUniformBuffer().data;
    ASSERT_THAT( buffer.size(), Ge( 32 ) );

    object1.Reset();
    sut.RunOncePerFrame();
    object2.Reset();
    sut.RunOncePerFrame();

    object2 = sut.CreateObject( Data( 12, 2.0f ), empty, empty, empty, empty,
                                Data< GLuint >( 10, 2 ) );
    object2->SetModelMatrix( m2 );
    sut.RunOncePerFrame();

    EXPECT_THAT( sut.GetObjectBuffer().GetNumberOfElements(), Eq( 10 ) );
    buffer = sut.GetUniformBuffer().data;
    ASSERT_THAT( buffer.size(), Ge( 32 ) );
    for ( uint64_t i = 0; i < 16; ++i )
        EXPECT_THAT( buffer[i], Eq( 1.0f ) );
    for ( uint64_t i = 16; i < 32; ++i )
        EXPECT_THAT( buffer[i], Eq( 2.0f ) );
}
