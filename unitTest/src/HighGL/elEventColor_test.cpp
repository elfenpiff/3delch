#include "GLAPI/elWindow.hpp"
#include "HighGL/elEventColor.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D::HighGL;

class elEventColor_test : public Test
{
  public:
    virtual void
    SetUp()
    {
    }

    virtual void
    TearDown()
    {
    }

    bool
    VEq( const glm::vec4& lhs, const glm::vec4& rhs,
         const float epsilon = 1.0f / 255.0f )
    {
        for ( int i = 0; i < 4; ++i )
            if ( abs( lhs[i] - rhs[i] ) > epsilon ) return false;
        return true;
    }
};

TEST_F( elEventColor_test, ConvertINT2RGBA_Consistency )
{
    uint32_t  a1 = 123, a2 = 123456, a3 = 12356789, a4 = 13456790;
    glm::vec4 v1 = elEventColor( a1 ).GetRGBAColor();
    glm::vec4 v2 = elEventColor( a2 ).GetRGBAColor();
    glm::vec4 v3 = elEventColor( a3 ).GetRGBAColor();
    glm::vec4 v4 = elEventColor( a4 ).GetRGBAColor();

    EXPECT_THAT( elEventColor( v1 ).GetIndex(), Eq( a1 ) );
    EXPECT_THAT( elEventColor( v2 ).GetIndex(), Eq( a2 ) );
    EXPECT_THAT( elEventColor( v3 ).GetIndex(), Eq( a3 ) );
    EXPECT_THAT( elEventColor( v4 ).GetIndex(), Eq( a4 ) );
}

TEST_F( elEventColor_test, ConvertRGBA2INT_Consistency )
{
    glm::vec4 v1 = { 0.5f, 0.25f, 0.125f, 1.0f };
    glm::vec4 v2 = { 0.25f, 0.5f, 0.75f, 1.0f };
    glm::vec4 v3 = { 0.125f, 0.5f, 0.75f, 1.0f };
    glm::vec4 v4 = { 0.75f, 0.125f, 0.25f, 1.0f };

    uint32_t a1 = elEventColor( v1 ).GetIndex();
    uint32_t a2 = elEventColor( v2 ).GetIndex();
    uint32_t a3 = elEventColor( v3 ).GetIndex();
    uint32_t a4 = elEventColor( v4 ).GetIndex();

    EXPECT_THAT( VEq( elEventColor( a1 ).GetRGBAColor(), v1 ), Eq( true ) );
    EXPECT_THAT( VEq( elEventColor( a2 ).GetRGBAColor(), v2 ), Eq( true ) );
    EXPECT_THAT( VEq( elEventColor( a3 ).GetRGBAColor(), v3 ), Eq( true ) );
    EXPECT_THAT( VEq( elEventColor( a4 ).GetRGBAColor(), v4 ), Eq( true ) );
}
