#include "unitTest.hpp"

#include "logging/elLog.hpp"

using namespace ::el3D;

int
main( int argc, char* argv[] )
{
    ::testing::InitGoogleTest( &argc, argv );

    logging::output::ToFile< logging::threading::InSeparateThread > fileLog(
        "unittests.log" );
    fileLog.SetLogLevel( logging::logLevel_t::DEBUG );
    fileLog.SetFormatting( logging::format::Detailled );


    logging::elLog::AddOutput( 0, &fileLog );


    return RUN_ALL_TESTS();
}
