#include "network/elEasyTCPMessaging_Client.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/etm/Connect_t.hpp"
#include "network/etm/Identify_t.hpp"
#include "network/etm/Request_t.hpp"
#include "network/etm/Transmission_t.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::network;
using namespace ::el3D::network::etm;

class elEasyTCPMessaging_Client_test : public Test
{
  public:
    static constexpr char     SERVICE[] = "testService";
    static constexpr uint32_t VERSION   = 121;
    static constexpr uint16_t PORT      = 8121;

    elEasyTCPMessaging_Client_test() : sut(), server( SERVICE, VERSION )
    {
    }

    void
    SetUp()
    {
        ASSERT_THAT( server.Listen( PORT ).HasError(), Eq( false ) );
        std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    }

    elEasyTCPMessaging_Client sut;
    elEasyTCPMessaging_Server server;
};

TEST_F( elEasyTCPMessaging_Client_test, GetServerIdentification )
{
    auto identity = sut.GetServerIdentification( "127.0.0.1", PORT );
    ASSERT_THAT( identity.has_value(), Eq( true ) );
    EXPECT_THAT( identity->protocolVersion, 0 );
    EXPECT_THAT( identity->serviceID, Eq( std::string( SERVICE ) ) );
    EXPECT_THAT( identity->serviceVersion, Eq( VERSION ) );
}

TEST_F( elEasyTCPMessaging_Client_test, GetServerIdentificationServerTimeout )
{
    elNetworkSocket_Server rawServer;
    rawServer.Listen( 12345 );
    sut.SetTimeout( units::Time::MilliSeconds( 100 ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto identity = sut.GetServerIdentification( "127.0.0.1", 12345 );
    ASSERT_THAT( identity.has_value(), Eq( false ) );
}

TEST_F( elEasyTCPMessaging_Client_test, GetServerIdentificationUnableToConnect )
{
    auto identity = sut.GetServerIdentification( "127.0.0.1", 12345 );
    ASSERT_THAT( identity.has_value(), Eq( false ) );
}

TEST_F( elEasyTCPMessaging_Client_test, GetServerIdentificationReceiveGarbage )
{
    elNetworkSocket_Server rawServer;
    rawServer.Listen( 12345 );
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    std::thread t1( [&] {
        auto identity = sut.GetServerIdentification( "127.0.0.1", 12345 );
        ASSERT_THAT( identity.has_value(), Eq( false ) );
    } );
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto                conn = rawServer.GetConnections();
    utils::byteStream_t byteStream( {9u, 9u, 9u, 4u, 1u} );

    rawServer.Send( conn[0].socket, byteStream );
    t1.join();
}

TEST_F( elEasyTCPMessaging_Client_test, ConnectToServerAndSendData )
{
    ASSERT_THAT( sut.Connect( "127.0.0.1", PORT ).get(), Eq( true ) );
    ASSERT_THAT(
        sut.Send< std::string >( 123, MessageType::Request, "hello world!" )
            .HasError(),
        Eq( false ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto rawMessage = server.TryReceiveMessages();

    ASSERT_THAT( rawMessage.empty(), Eq( false ) );
    Transmission_t< std::string > message;

    ASSERT_THAT( utils::elByteDeserializer( rawMessage[0].message )
                     .Extract( utils::Endian::Big, message )
                     .HasError(),
                 Eq( false ) );
    EXPECT_THAT( message.payload.payload, Eq( "hello world!" ) );
}

TEST_F( elEasyTCPMessaging_Client_test, ConnectToServerAndReceiveData )
{
    ASSERT_THAT( sut.Connect( "127.0.0.1", PORT ).get(), Eq( true ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto conn = server.RefreshAndReturnConnections();
    ASSERT_THAT( conn.empty(), Eq( false ) );
    server.SendMessage< std::string >( 555, MessageType::Response, "hey froggy",
                                       conn[0] );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto message = sut.TryReceive< std::string >();
    ASSERT_THAT( message.has_value(), Eq( true ) );
    EXPECT_THAT( message->payload.payload, Eq( "hey froggy" ) );
}

TEST_F( elEasyTCPMessaging_Client_test, ConnectToSendAndReceiveData )
{
    ASSERT_THAT( sut.Connect( "127.0.0.1", PORT ).get(), Eq( true ) );
    ASSERT_THAT( sut.Send< int >( 123, MessageType::Request, 1234 ).HasError(),
                 Eq( false ) );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto rawMessage = server.TryReceiveMessages();
    ASSERT_THAT( rawMessage.empty(), Eq( false ) );
    Transmission_t< int > message;
    ASSERT_THAT( utils::elByteDeserializer( rawMessage[0].message )
                     .Extract( utils::Endian::Big, message )
                     .HasError(),
                 Eq( false ) );
    EXPECT_THAT( message.payload.payload, Eq( 1234 ) );

    server.SendMessage< int >( 555, MessageType::Response, 5678,
                               rawMessage[0].connectionID );
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    auto response = sut.TryReceive< int >();
    ASSERT_THAT( response.has_value(), Eq( true ) );
    EXPECT_THAT( response->payload.payload, Eq( 5678 ) );
}
