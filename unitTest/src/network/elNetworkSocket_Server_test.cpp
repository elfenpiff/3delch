#include "network/elNetworkSocket_Client.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::network::elNetworkSocket::internal;
using namespace ::el3D::network;

class elNetworkSocket_Server_test : public Test
{
  public:
    elNetworkSocket_Server_test()
    {
    }

    virtual void
    SetUp()
    {
    }

    virtual void
    TearDown()
    {
    }

    elNetworkSocket_Server server;
    elNetworkSocket_Client client;
};

TEST_F( elNetworkSocket_Server_test, SingleConnection )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    EXPECT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );
}

TEST_F( elNetworkSocket_Server_test, MultiConnection )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    EXPECT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    elNetworkSocket_Client client2;
    EXPECT_THAT( client2.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );
}

TEST_F( elNetworkSocket_Server_test, SingleConnectionReceive )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );
    client.Send( byteStream_t{'1'} );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto msg = server.BlockingReceive( server.GetConnections()[0].socket );
    EXPECT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg.value().stream[0], Eq( '1' ) );
}

TEST_F( elNetworkSocket_Server_test, MultiConnectionReceive )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    elNetworkSocket_Client client2;
    ASSERT_THAT( client2.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    client.Send( byteStream_t{'1'} );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto msg = server.BlockingReceive( server.GetConnections()[0].socket );
    EXPECT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg.value().stream[0], Eq( '1' ) );

    client2.Send( byteStream_t{'2'} );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto msg2 = server.BlockingReceive( server.GetConnections()[1].socket );
    EXPECT_THAT( msg2.has_value(), Eq( true ) );
    EXPECT_THAT( msg2.value().stream[0], Eq( '2' ) );
}

TEST_F( elNetworkSocket_Server_test, SingleConnectionSend )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    auto conn = server.GetConnections();
    server.Send( conn[0].socket, utils::byteStream_t{'a', 'b'} );

    auto msg = client.BlockingReceive();
    ASSERT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg->stream.at( 0 ), Eq( 'a' ) );
    EXPECT_THAT( msg->stream.at( 1 ), Eq( 'b' ) );
}

TEST_F( elNetworkSocket_Server_test, MultiConnectionSend )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    elNetworkSocket_Client client2;
    ASSERT_THAT( client2.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto conn = server.GetConnections();
    server.Send( conn[0].socket, utils::byteStream_t{'a', 'b'} );
    server.Send( conn[1].socket, utils::byteStream_t{'a', 'b'} );

    auto msg = client.BlockingReceive();
    ASSERT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg->stream.at( 0 ), Eq( 'a' ) );
    EXPECT_THAT( msg->stream.at( 1 ), Eq( 'b' ) );

    auto msg2 = client2.BlockingReceive();
    ASSERT_THAT( msg2.has_value(), Eq( true ) );
    EXPECT_THAT( msg2->stream.at( 0 ), Eq( 'a' ) );
    EXPECT_THAT( msg2->stream.at( 1 ), Eq( 'b' ) );
}

TEST_F( elNetworkSocket_Server_test, UnableToSendWhenDisconnected )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto conn = server.GetConnections();
    client.Disconnect();
    std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );

    ASSERT_THAT( conn.empty(), Eq( false ) );
    EXPECT_THAT(
        server.Send( conn[0].socket, utils::byteStream_t{'a', 'b'} ).HasError(),
        Eq( true ) );
}
