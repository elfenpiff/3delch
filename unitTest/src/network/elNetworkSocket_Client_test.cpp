#include "network/elNetworkSocket_Client.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "unitTest.hpp"

#include <chrono>

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::network;

class elNetworkSocket_Client_test : public Test
{
  public:
    virtual void
    SetUp()
    {
    }

    elNetworkSocket_Client client;
    elNetworkSocket_Server server;
};

TEST_F( elNetworkSocket_Client_test, ConnectViaHostnameToValidTarget )
{
    server.Listen( 12345 );
    EXPECT_THAT( client.IsConnected(), Eq( false ) );
    EXPECT_THAT( client.Connect( "localhost", 12345 ).get().HasError(),
                 Eq( false ) );
    EXPECT_THAT( client.IsConnected(), Eq( true ) );
}

TEST_F( elNetworkSocket_Client_test, ConnectViaHostnameToInvalidTarget )
{
    EXPECT_THAT( client.IsConnected(), Eq( false ) );
    EXPECT_THAT( client.Connect( "localfuu", 12345 ).get().HasError(),
                 Eq( true ) );
    EXPECT_THAT( client.IsConnected(), Eq( false ) );
}

TEST_F( elNetworkSocket_Client_test, ConnectViaIPToValidTarget )
{
    server.Listen( 12345 );
    EXPECT_THAT( client.IsConnected(), Eq( false ) );
    EXPECT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );
    EXPECT_THAT( client.IsConnected(), Eq( true ) );
}

TEST_F( elNetworkSocket_Client_test, Disconnect )
{
    server.Listen( 12345 );
    ASSERT_THAT( client.Connect( "127.0.0.1", 12345 ).get().HasError(),
                 Eq( false ) );
    EXPECT_THAT( client.IsConnected(), Eq( true ) );
    client.Disconnect();
    EXPECT_THAT( client.IsConnected(), Eq( false ) );
}

TEST_F( elNetworkSocket_Client_test, SendMessage )
{
    ASSERT_THAT( server.Listen( 12345 ).HasError(), Eq( false ) );
    client.Connect( "127.0.0.1", 12345 ).get();
    client.Send( utils::byteStream_t{'h', 'i'} );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto msg = server.BlockingReceive( server.GetConnections()[0].socket );
    EXPECT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg.value().stream[0], Eq( 'h' ) );
    EXPECT_THAT( msg.value().stream[1], Eq( 'i' ) );
}

TEST_F( elNetworkSocket_Client_test, UnableToSendMessageAfterDisconnected )
{
    server.Listen( 12345 );
    client.Connect( "127.0.0.1", 12345 ).get();
    client.Disconnect();
    EXPECT_THAT( client.Send( utils::byteStream_t{'h', 'i'} ).HasError(),
                 Eq( true ) );
}

TEST_F( elNetworkSocket_Client_test, ReconnectWithSecondServer )
{
    server.Listen( 12345 );
    client.Connect( "127.0.0.1", 12345 ).get();

    elNetworkSocket_Server s2;
    s2.Listen( 7773 );
    client.Connect( "127.0.0.1", 7773 ).get();

    client.Send( utils::byteStream_t{'A'} );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
    auto msg = s2.BlockingReceive( s2.GetConnections()[0].socket );
    EXPECT_THAT( msg.has_value(), Eq( true ) );
    EXPECT_THAT( msg.value().stream[0], Eq( 'A' ) );
}

TEST_F( elNetworkSocket_Client_test, IsPortOpenWithClosedPort )
{
    EXPECT_THAT( client.IsPortOpen( "127.0.0.1", 55556 ), Eq( false ) );
}

TEST_F( elNetworkSocket_Client_test, IsPortOpenWithOpenPort )
{
    server.Listen( 55556 );
    EXPECT_THAT( client.IsPortOpen( "127.0.0.1", 55556 ), Eq( true ) );
}
