#include "network/elNetworkSocket_Client.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "unitTest.hpp"

#include <thread>

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::utils;
using namespace ::el3D::network;

class elNetworkSocket_Base_test : public Test
{
  public:
    virtual void
    SetUp()
    {
    }

    elNetworkSocket_Client client;
    elNetworkSocket_Server server;
};

TEST_F( elNetworkSocket_Base_test, ClientReceiveNonBlockingAfterDisconnect )
{
    server.Listen( 6661 );
    client.Connect( "127.0.0.1", 6661 ).get();
    std::thread t( [&] { client.BlockingReceive(); } );
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    client.Disconnect();
    t.join();
}

TEST_F( elNetworkSocket_Base_test, ServerReceiveNonBlockingAfterStopListening )
{
    server.Listen( 6661 );
    client.Connect( "127.0.0.1", 6661 ).get();

    std::thread t(
        [&] { server.BlockingReceive( server.GetConnections()[0].socket ); } );

    std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );

    server.StopListen();
    t.join();
}

