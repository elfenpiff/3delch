#include "animation/elAnimate_SourceDestination.hpp"
#include "unitTest.hpp"

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::animation;

class elAnimation_SourceDestination_test : public Test
{
  public:
    void
    SetUp()
    {
        sut.SetVelocity( 1.0 ).SetSource( 2.0 ).SetDestination( 4.0 );
        sut.SetCustomGetTimeCallback( [&] { return currentTime; } );
    }

    units::Time currentTime = units::Time::Seconds( 1.0 );
    elAnimate_SourceDestination< float, interpolation::Linear > sut;
};

TEST_F( elAnimation_SourceDestination_test,
        InitialUpdateLeavesCurrentPositionUnchanged )
{
    sut.Update();
    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 2.0f );
}

TEST_F( elAnimation_SourceDestination_test,
        LinearRelativeUpdateCurrentPositionAfterUpdateCall )
{
    sut.Update();
    currentTime = units::Time::Seconds( 1.5 );
    sut.Update();

    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 3.0f );
}

TEST_F( elAnimation_SourceDestination_test,
        LinearAbsoluteUpdateCurrentPositionAfterUpdateCall )
{
    sut.SetVelocityType( AnimationVelocity::Absolute );

    sut.Update();
    currentTime = units::Time::Seconds( 2.0 );
    sut.Update();

    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 3.0f );
}

TEST_F( elAnimation_SourceDestination_test,
        FinishedUpdatingAfterArrivingAtDestination )
{
    sut.Update();
    currentTime = units::Time::Seconds( 3.0 );
    sut.Update();

    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 4.0f );
    EXPECT_FALSE( sut.HasToPerformUpdate() );
}

TEST_F( elAnimation_SourceDestination_test, NoOverShooting )
{
    sut.Update();
    currentTime = units::Time::Seconds( 30.0 );
    sut.Update();

    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 4.0f );
    EXPECT_FALSE( sut.HasToPerformUpdate() );
}

TEST_F( elAnimation_SourceDestination_test, JumpToFinishesAnimation )
{
    sut.Update();
    sut.JumpTo( 10.0 );

    EXPECT_FLOAT_EQ( sut.GetCurrentPosition(), 10.0f );
    EXPECT_FALSE( sut.HasToPerformUpdate() );
}

TEST_F( elAnimation_SourceDestination_test, UnboundedAnimationIsUnbounded )
{
    elAnimate_SourceDestination< float, interpolation::Linear > animation;
    animation.SetCustomGetTimeCallback( [&] { return currentTime; } );
    animation.SetVelocity( 1.0 ).SetSource( 2.0 ).SetDirection( 1.0 );

    animation.Update();
    currentTime = units::Time::Seconds( 11.0 );
    animation.Update();

    EXPECT_FLOAT_EQ( animation.GetCurrentPosition(), 12.0f );
}

TEST_F( elAnimation_SourceDestination_test, HermiteInterpolation )
{
    elAnimate_SourceDestination< float, interpolation::Hermite > animation;
    animation.SetCustomGetTimeCallback( [&] { return currentTime; } );
    animation.SetVelocity( 1.0 ).SetSource( 2.0 ).SetDestination( 4.0 );

    animation.Update();
    currentTime = units::Time::Seconds( 1.25 );
    animation.Update();

    interpolation::factor_t hermiteFactor = 0.25 * 0.25 * ( 3.0 - 0.25 * 2.0 );

    EXPECT_FLOAT_EQ( animation.GetCurrentPosition(),
                     2.0f + hermiteFactor * 2.0f );
}

TEST_F( elAnimation_SourceDestination_test, VectorInterpolation )
{
    elAnimate_SourceDestination< glm::vec2, interpolation::Linear > animation;
    animation.SetCustomGetTimeCallback( [&] { return currentTime; } );
    animation.SetVelocity( 1.0 )
        .SetSource( { 2.0, 2.0 } )
        .SetDestination( { 4.0, 6.0 } );

    animation.Update();
    currentTime = units::Time::Seconds( 1.5 );
    animation.Update();

    EXPECT_FLOAT_EQ( animation.GetCurrentPosition().x, 3.0 );
    EXPECT_FLOAT_EQ( animation.GetCurrentPosition().y, 4.0 );
}

TEST_F( elAnimation_SourceDestination_test,
        OnUpdateCallbackIsNotCalledOnFirstUpdate )
{
    bool callbackCalled = false;
    sut.SetOnUpdateCallback( [&] { callbackCalled = true; } );

    sut.Update();
    EXPECT_FALSE( callbackCalled );
}

TEST_F( elAnimation_SourceDestination_test,
        OnUpdateCallbackIsCalledAfterFirstUpdate )
{
    bool callbackCalled = false;
    sut.SetOnUpdateCallback( [&] { callbackCalled = true; } );

    sut.Update();
    sut.Update();
    EXPECT_TRUE( callbackCalled );
}

TEST_F( elAnimation_SourceDestination_test,
        OnUpdateCallbackIsCalledAfterFinishUpdate )
{
    bool callbackCalled = false;
    sut.SetOnUpdateCallback( [&] { callbackCalled = true; } );

    sut.Update();
    currentTime = units::Time::Seconds( 10.0 );
    sut.Update();
    EXPECT_TRUE( callbackCalled );
}

TEST_F( elAnimation_SourceDestination_test, OnArrivalCallbackIsCalledOnArrival )
{
    bool callbackCalled = false;
    sut.SetOnArrivalCallback( [&] { callbackCalled = true; } );

    sut.Update();
    EXPECT_FALSE( callbackCalled );

    currentTime = units::Time::Seconds( 10.0 );
    sut.Update();
    EXPECT_TRUE( callbackCalled );
}

TEST_F( elAnimation_SourceDestination_test, AbsoluteVelocity )
{
    currentTime = units::Time::Seconds( 0.0 );

    elAnimate_SourceDestination< float, interpolation::Linear > sut2;
    sut2.SetVelocity( 2.0 )
        .SetSource( 6.0 )
        .SetDestination( 12.0 )
        .SetVelocityType( AnimationVelocity::Absolute );
    sut2.SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut2.Update();
    currentTime = units::Time::Seconds( 1.0 );
    sut2.Update();

    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition(), 8.0f );
}

TEST_F( elAnimation_SourceDestination_test, RelativeVelocity )
{
    currentTime = units::Time::Seconds( 0.0 );

    elAnimate_SourceDestination< float, interpolation::Linear > sut2;
    sut2.SetVelocity( 2.0 )
        .SetSource( 6.0 )
        .SetDestination( 12.0 )
        .SetVelocityType( AnimationVelocity::Relative );
    sut2.SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut2.Update();
    currentTime = units::Time::Seconds( 0.25 );
    sut2.Update();

    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition(), 9.0 );
}

TEST_F( elAnimation_SourceDestination_test, GlmVec4 )
{
    currentTime = units::Time::Seconds( 0.0 );

    elAnimate_SourceDestination< glm::vec4, interpolation::Linear > sut2;
    sut2.SetVelocity( 1.0 )
        .SetSource( { 1.0, 2.0, 3.0, 4.0 } )
        .SetDestination( { 5.0, 6.0, 7.0, 8.0 } )
        .SetVelocityType( AnimationVelocity::Relative );
    sut2.SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut2.Update();
    currentTime = units::Time::Seconds( 0.25 );
    sut2.Update();

    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition().x, 2.0f );
    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition().y, 3.0f );
    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition().z, 4.0f );
    EXPECT_FLOAT_EQ( sut2.GetCurrentPosition().w, 5.0f );
}

