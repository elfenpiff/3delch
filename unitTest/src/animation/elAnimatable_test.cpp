#include "animation/elAnimatable.hpp"
#include "animation/elAnimate_SourceDestination.hpp"
#include "unitTest.hpp"
#include "units/Time.hpp"

#include <glm/glm.hpp>

using namespace ::testing;
using namespace ::el3D;
using namespace ::el3D::animation;

class SomeClass : public elAnimatable< SomeClass >
{
  public:
    void
    SetA( float v )
    {
        a = v;
    }
    float
    GetA()
    {
        return a;
    }

    void
    SetB( glm::vec4 v )
    {
        b = v;
    }
    glm::vec4
    GetB()
    {
        return b;
    }

    using elAnimatableBase::HasActiveAnimations;
    using elAnimatableBase::UpdateAnimations;

    float     a{ 0.0f };
    glm::vec4 b{ 0.0 };
};

class elAnimatable_test : public Test
{
  public:
    void
    SetUp()
    {
        sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
               &SomeClass::SetA )
            .SetDestination( 12.0f )
            .SetSource( 6.0f )
            .SetVelocity( 2.0f )
            .SetVelocityType( AnimationVelocity::Absolute )
            .SetCustomGetTimeCallback( [&] { return currentTime; } );
    }

    SomeClass   sut;
    units::Time currentTime = units::Time::Seconds( 0.0 );
};

TEST_F( elAnimatable_test, BeforeUpdateOriginalValueIsSet )
{
    EXPECT_FLOAT_EQ( sut.a, 0.0f );
}

TEST_F( elAnimatable_test, FirstUpdateSetsValueToSource )
{
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 6.0f );

    currentTime = units::Time::Seconds( 1.0 );
    sut.UpdateAnimations();
}

TEST_F( elAnimatable_test, UpdateAfterSomeTimeAdjustsValue )
{
    sut.UpdateAnimations();
    currentTime = units::Time::Seconds( 1.0 );
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 8.0f );
}

TEST_F( elAnimatable_test, AnimateComplexVector )
{
    SomeClass sut2;
    sut2.Animate< elAnimate_SourceDestination, interpolation::Linear >(
            &SomeClass::SetB )
        .SetDestination( { 5.0f, 6.0f, 7.0f, 8.0f } )
        .SetSource( { 1.0f, 2.0f, 3.0f, 4.0f } )
        .SetVelocity( 1.0f )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut2.UpdateAnimations();

    EXPECT_FLOAT_EQ(
        glm::distance( sut2.b, glm::vec4{ 1.0f, 2.0f, 3.0f, 4.0f } ), 0.0f );

    currentTime = units::Time::Seconds( 1.0 );
    sut2.UpdateAnimations();

    EXPECT_FLOAT_EQ(
        glm::distance( sut2.b, glm::vec4{ 1.5f, 2.5f, 3.5f, 4.5f } ), 0.0f );
}

TEST_F( elAnimatable_test, UpdateAnimationVelocity )
{
    sut.UpdateAnimations(); // 6.0f

    currentTime = units::Time::Seconds( 1.0 );
    sut.UpdateAnimations(); // 8.0f

    sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
           &SomeClass::SetA )
        .SetVelocity( 1.0f );

    currentTime = units::Time::Seconds( 2.0 );
    sut.UpdateAnimations();
    EXPECT_THAT( sut.a, 9.0f );
}

TEST_F( elAnimatable_test, RemoveAnimation )
{
    sut.UpdateAnimations();

    sut.RemoveAnimation( &SomeClass::SetA );

    currentTime = units::Time::Seconds( 2.0 );
    sut.UpdateAnimations();
    EXPECT_THAT( sut.a, 6.0f );
}

TEST_F( elAnimatable_test, HasActiveAnimationsWhenThereIsOne )
{
    EXPECT_THAT( sut.HasActiveAnimations(), Eq( true ) );
}

TEST_F( elAnimatable_test, HasNoActiveAnimationsAfterRemove )
{
    sut.RemoveAnimation( &SomeClass::SetA );
    EXPECT_THAT( sut.HasActiveAnimations(), Eq( false ) );
}

TEST_F( elAnimatable_test,
        HasNoActiveAnimationsAfterAnimationHasReachedDestination )
{
    sut.UpdateAnimations();
    currentTime = units::Time::Seconds( 200.0 );
    sut.UpdateAnimations();
    EXPECT_THAT( sut.HasActiveAnimations(), Eq( false ) );
}

TEST_F( elAnimatable_test, FreeFunctionIsAnimatable )
{
    double value       = 0.0;
    auto   valueSetter = [&]( const double& v ) { value = v; };
    sut.AnimateFreeFunction< elAnimate_SourceDestination, interpolation::Linear,
                             double >( valueSetter )
        .SetVelocity( 1.0f )
        .SetSource( 2.0 )
        .SetDestination( 7.0 )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } );

    EXPECT_DOUBLE_EQ( value, 0.0 );
    sut.UpdateAnimations();
    EXPECT_DOUBLE_EQ( value, 2.0 );
    currentTime = units::Time::Seconds( 1.0 );
    sut.UpdateAnimations();

    double epsilon = 0.0001;

    EXPECT_GE( value, 3.0 - epsilon );
    EXPECT_LE( value, 3.0 + epsilon );
}

TEST_F( elAnimatable_test, FreeFunctionIsNotAddedMultipleTimes )
{
    double value       = 0.0;
    auto   valueSetter = [&]( const double& ) { value += 1.0; };
    sut.AnimateFreeFunction< elAnimate_SourceDestination, interpolation::Linear,
                             double >( valueSetter )
        .SetVelocity( 1.0f )
        .SetSource( 2.0 )
        .SetDestination( 7.0 )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut.UpdateAnimations();

    sut.AnimateFreeFunction< elAnimate_SourceDestination, interpolation::Linear,
                             double >( valueSetter )
        .SetVelocity( 1.0f )
        .SetSource( 2.0 )
        .SetDestination( 7.0 )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } );

    sut.UpdateAnimations();
    EXPECT_DOUBLE_EQ( value, 2.0 );
    sut.UpdateAnimations();
    EXPECT_DOUBLE_EQ( value, 3.0 );
}

TEST_F( elAnimatable_test, CopyConstructor )
{
    SomeClass sutCopy( sut );

    sut.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 6.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 0.0f ) );

    sutCopy.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 6.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 6.0f ) );

    currentTime = units::Time::Seconds( 1.0 );

    sutCopy.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 6.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 8.0f ) );

    sut.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 8.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 8.0f ) );
}

TEST_F( elAnimatable_test, CopyAssignment )
{
    SomeClass sutCopy;

    sutCopy
        .Animate< elAnimate_SourceDestination, interpolation::Linear >(
            &SomeClass::SetA )
        .SetDestination( 36.0f )
        .SetSource( 24.0f )
        .SetVelocity( 2.0f )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } );

    sutCopy.UpdateAnimations();
    sut.UpdateAnimations();

    sutCopy = sut;

    EXPECT_THAT( sut.a, Eq( 6.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 6.0f ) );

    currentTime = units::Time::Seconds( 1.0 );
    sutCopy.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 6.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 8.0f ) );

    sut.UpdateAnimations();
    EXPECT_THAT( sut.a, Eq( 8.0f ) );
    EXPECT_THAT( sutCopy.a, Eq( 8.0f ) );
}

TEST_F( elAnimatable_test, FollowUpAnimationInCallbackWorks )
{
    int onArrivalCallbackCalled = 0;
    sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
           &SomeClass::SetA )
        .SetDestination( 12.0f )
        .SetSource( 6.0f )
        .SetVelocity( 2.0f )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } )
        .SetOnArrivalCallback( [&] {
            ++onArrivalCallbackCalled;
            sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
                   &SomeClass::SetA )
                .SetSource( 12.0f )
                .SetDestination( 20.0f )
                .SetVelocity( 1.0f )
                .SetVelocityType( AnimationVelocity::Absolute )
                .SetCustomGetTimeCallback( [&] { return currentTime; } );
        } );

    sut.UpdateAnimations();
    currentTime = units::Time::Seconds( 3.0 );
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 12.0f );
    EXPECT_EQ( onArrivalCallbackCalled, 1 );
    currentTime = units::Time::Seconds( 4.0 );
    sut.UpdateAnimations();
    EXPECT_EQ( onArrivalCallbackCalled, 1 );
    EXPECT_FLOAT_EQ( sut.a, 13.0f );
    currentTime = units::Time::Seconds( 20.0 );
    sut.UpdateAnimations();
    EXPECT_EQ( onArrivalCallbackCalled, 1 );
    EXPECT_FLOAT_EQ( sut.a, 20.0f );
}

TEST_F( elAnimatable_test, FollowUpAnimationInCallbackWithDifferentSourceWorks )
{
    sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
           &SomeClass::SetA )
        .SetDestination( 12.0f )
        .SetSource( 6.0f )
        .SetVelocity( 2.0f )
        .SetVelocityType( AnimationVelocity::Absolute )
        .SetCustomGetTimeCallback( [&] { return currentTime; } )
        .SetOnArrivalCallback( [&] {
            sut.Animate< elAnimate_SourceDestination, interpolation::Linear >(
                   &SomeClass::SetA )
                .SetSource( 16.0f )
                .SetDestination( 24.0f )
                .SetVelocity( 1.0f )
                .SetVelocityType( AnimationVelocity::Absolute )
                .SetCustomGetTimeCallback( [&] { return currentTime; } );
        } );

    sut.UpdateAnimations();
    currentTime = units::Time::Seconds( 3.0 );
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 16.0f );
    currentTime = units::Time::Seconds( 4.0 );
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 17.0f );
    currentTime = units::Time::Seconds( 20.0 );
    sut.UpdateAnimations();
    EXPECT_FLOAT_EQ( sut.a, 24.0f );
}
