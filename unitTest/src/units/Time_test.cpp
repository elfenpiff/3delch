#include "unitTest.hpp"
#include "units/Time.hpp"

using namespace ::el3D::units;
using namespace ::el3D::units::literals;
using namespace ::testing;

TEST( Time_test, OperatorPlus )
{
    Time sut = 2_s + 3_s;
    EXPECT_THAT( sut.GetSeconds(), Eq( 5 ) );
}

TEST( Time_test, OperatorMinus )
{
    Time sut = 7_s - 6_s;
    EXPECT_THAT( sut.GetSeconds(), Eq( 1 ) );
}

TEST( Time_test, PlusAssignment )
{
    Time sut = 4_s;
    sut += 5_s;
    EXPECT_THAT( sut.GetSeconds(), Eq( 9 ) );
}

TEST( Time_test, MinusAssignment )
{
    Time sut = 8_s;
    sut -= 1_s;
    EXPECT_THAT( sut.GetSeconds(), Eq( 7 ) );
}
