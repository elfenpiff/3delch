#include "unitTest.hpp"
#include "units/DataSize.hpp"


TEST( DataSize_test, StreamOperator )
{
    el3D::units::DataSize sut = el3D::units::DataSize::MegaBytes( 2.34 );
    std::stringstream     ss;
    ss << sut;
    EXPECT_THAT( sut.ToString( 2 ), testing::Eq( "2.34mb" ) );
}

using namespace ::el3D::units;
using namespace ::el3D::units::literals;
using namespace ::testing;

TEST( DataSize_test, ToStringInMegaByte )
{
    DataSize sut = DataSize::MegaBytes( 2.34 );
    EXPECT_THAT( sut.ToString( 2 ), Eq( "2.34mb" ) );
}

