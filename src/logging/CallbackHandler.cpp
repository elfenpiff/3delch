#include "logging/CallbackHandler.hpp"

namespace el3D
{
namespace logging
{
size_t
CallbackHandler::Add( const callback_t& callback ) noexcept
{
    std::lock_guard< std::mutex > lock( this->mutex );
    return this->callbacks.emplace_back( callback ).index();
}

void
CallbackHandler::Remove( const size_t callbackID ) noexcept
{
    std::lock_guard< std::mutex > lock( this->mutex );
    this->callbacks.erase( callbackID );
}

void
CallbackHandler::Call( const std::string& channelName,
                       const logLevel_t logLevel, const std::string& file,
                       const std::string& line, const std::string& func,
                       const std::string& msg ) const noexcept
{
    // this step is required since a callback can call Add or Remove and
    // then this would dead lock. Additionally the callback vector would
    // be unusable within a range based loop after a modification
    bb::elUniqueIndexVector< callback_t > callbackCopy;
    {
        std::lock_guard< std::mutex > lock( this->mutex );
        callbackCopy = this->callbacks;
    }

    for ( auto& c : callbackCopy )
        c( channelName, logLevel, file, line, func, msg );
}
} // namespace logging
} // namespace el3D
