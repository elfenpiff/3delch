#include "logging/elLog.hpp"

#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/LoggingRedirection.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace logging
{
bb::elUniqueIndexVector< elLog::channel_t > elLog::channels = {
    { DEFAULT_CHANNEL_NAME } };


elLog::channel_t::channel_t( const std::string& name ) noexcept
    : name( name ),
      callbacks( std::shared_ptr< CallbackHandler >( new CallbackHandler ) )
{
}

elLog::Proxy::Proxy( const std::vector< output::Base* >* output,
                     const CallbackHandler*              callback,
                     const std::string& channelName, const logLevel_t l,
                     const std::string& file, const int line,
                     const std::string& func ) noexcept
    : logLevel( l ), output( output ),
      callback( callback ), source{ channelName, file, std::to_string( line ),
                                    func }
{
}

elLog::Proxy::~Proxy()
{
    for ( auto out : *this->output )
    {
        out->Out( source.channelName, logLevel, source.file, source.line,
                  source.func, msg );
    }

    callback->Call( source.channelName, logLevel, source.file, source.line,
                    source.func, msg );
}

void
elLog::AddOutput( const size_t channel, output::Base* l )
{
    elLog::channels[channel].output.emplace_back( l );
}

void
elLog::RemoveOutput( const size_t channel, output::Base* entry )
{
    auto& list = elLog::channels[channel].output;
    auto  iter = bb::find( list, entry );
    if ( iter != list.end() ) list.erase( iter );
}

std::string
elLog::LogEx( const std::string& file, const int line, const std::string& func,
              const std::string& msg, const size_t channel ) noexcept
{
    Proxy( &elLog::channels[channel].output,
           elLog::channels[channel].callbacks.get(),
           elLog::channels[channel].name, WARN, file, line, func )
        << msg;
    return msg;
}

const elLog::Proxy
elLog::Log( const logLevel_t l, const std::string& file, const int line,
            const std::string& func, const size_t channel ) noexcept
{
    return Proxy( &elLog::channels[channel].output,
                  elLog::channels[channel].callbacks.get(),
                  elLog::channels[channel].name, l, file, line, func );
}

void
elLog::EnableOutputRedirection()
{
    using namespace redirection;
    static LogBuffer         clogBuffer( DEBUG, "std::clog" );
    static LogBuffer         coutBuffer( INFO, "std::cout" );
    static LogBuffer         cerrBuffer( ERROR, "std::cerr" );
    static OutputRedirection clogRedirection( std::clog, &clogBuffer );
    static OutputRedirection coutRedirection( std::cout, &coutBuffer );
    static OutputRedirection cerrRedirection( std::cerr, &cerrBuffer );
}

size_t
elLog::CreateChannel( const std::string& name ) noexcept
{
    for ( size_t k = 0, limit = elLog::channels.size(); k < limit; ++k )
        if ( elLog::channels[k].name == name ) return k;

    return elLog::channels.emplace_back( channel_t{ name } ).index();
}

void
elLog::RemoveChannel( const size_t channel ) noexcept
{
    elLog::channels.erase( channel );
}

size_t
elLog::AddCallback( const size_t channel, const callback_t& callback ) noexcept
{
    return elLog::channels[channel].callbacks->Add( callback );
}

void
elLog::RemoveCallback( const size_t channel, const size_t callbackID ) noexcept
{
    elLog::channels[channel].callbacks->Remove( callbackID );
}


} // namespace logging
} // namespace el3D
