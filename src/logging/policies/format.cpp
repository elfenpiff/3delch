#include "logging/policies/format.hpp"

#include "logging/policies/detail.hpp"

#include <sstream>

static char white[]      = "\033[1;37m";
static char lightgrey[]  = "\033[0;37m";
static char darkgrey[]   = "\033[2;37m";
static char red[]        = "\033[0;91m";
static char brightred[]  = "\033[1;91m";
static char green[]      = "\033[0;92m";
static char yellow[]     = "\033[0;93m";
static char brightcyan[] = "\033[0;96m";
static char brightblue[] = "\033[0;94m";
static char reset[]      = "\033[0m";

namespace el3D
{
static std::string
LogLevelToColor( const logging::logLevel_t logLevel )
{
    switch ( logLevel )
    {
        case logging::DEBUG:
            return darkgrey;
        case logging::WARN:
            return yellow;
        case logging::INFO:
            return green;
        case logging::ERROR:
            return red;
        case logging::FATAL:
            return brightred;
        default:
            return lightgrey;
    };
}

namespace logging
{
namespace format
{
std::string
Default( const std::string &channelName, const logLevel_t logLevel,
         const std::string &, const std::string &, const std::string &func,
         const std::string &msg ) noexcept
{
    std::stringstream ss;
    std::string       logLevelName(
        logLevelToString[static_cast< size_t >( logLevel )] );
    logLevelName.resize( 5, ' ' );
    ss << "< " << channelName << " > " << detail::GetCurrentUnixTime() << " "
       << "[ " << logLevelName << " ] ( " << func << " ) : " << msg;
    return ss.str();
}

std::string
DefaultColored( const std::string &channelName, const logLevel_t logLevel,
                const std::string &, const std::string &,
                const std::string &func, const std::string &msg ) noexcept
{
    std::stringstream ss;
    std::string       logLevelColor( LogLevelToColor( logLevel ) );
    std::string       logLevelName(
        logLevelToString[static_cast< size_t >( logLevel )] );
    logLevelName.resize( 5, ' ' );

    ss << darkgrey << "< " << brightcyan << channelName << darkgrey << " > "
       << darkgrey << detail::GetCurrentUnixTime() << lightgrey << " "
       << "[ " << logLevelColor << logLevelName << lightgrey << " ] "
       << lightgrey << " ( " << brightblue << func << lightgrey
       << " ) : " << white << msg << reset;

    return ss.str();
}

std::string
Detailled( const std::string &channelName, const logLevel_t logLevel,
           const std::string &file, const std::string &line,
           const std::string &func, const std::string &msg ) noexcept
{
    std::stringstream ss;
    std::string       logLevelName(
        logLevelToString[static_cast< size_t >( logLevel )] );
    logLevelName.resize( 5, ' ' );
    ss << "< " << channelName << " > " << detail::GetCurrentUnixTime() << " "
       << "[ " << logLevelName << " ] " << file << ":" << line << " ( " << func
       << " ) "
       << " : " << msg;
    return ss.str();
}

std::string
DetailledColored( const std::string &channelName, const logLevel_t logLevel,
                  const std::string &file, const std::string &line,
                  const std::string &func, const std::string &msg ) noexcept
{
    std::stringstream ss;
    std::string       logLevelColor( LogLevelToColor( logLevel ) );
    std::string       logLevelName(
        logLevelToString[static_cast< size_t >( logLevel )] );
    logLevelName.resize( 5, ' ' );

    ss << darkgrey << "< " << brightcyan << channelName << darkgrey << " > "
       << darkgrey << detail::GetCurrentUnixTime() << lightgrey << " "
       << "[ " << logLevelColor << logLevelName << lightgrey << " ] " << file
       << lightgrey << ":" << line << lightgrey << " ( " << brightblue << func
       << lightgrey << " )"
       << " : " << white << msg << reset;

    return ss.str();
}

std::string
MinimalColored( const std::string &, const logLevel_t logLevel,
                const std::string &, const std::string &, const std::string &,
                const std::string &msg ) noexcept
{
    std::stringstream ss;
    ss << LogLevelToColor( logLevel ) << msg << reset;
    return ss.str();
}

std::string
Minimal( const std::string &, const logLevel_t, const std::string &,
         const std::string &, const std::string &,
         const std::string &msg ) noexcept
{
    std::stringstream ss;
    ss << msg;
    return ss.str();
}

std::string
ShortColored( const std::string &, const logLevel_t logLevel,
              const std::string &, const std::string &, const std::string &func,
              const std::string &msg ) noexcept
{
    std::stringstream ss;
    ss << brightblue << func << " " << LogLevelToColor( logLevel ) << msg
       << reset;
    return ss.str();
}

std::string
Short( const std::string &, const logLevel_t, const std::string &,
       const std::string &, const std::string &func,
       const std::string &msg ) noexcept
{
    std::stringstream ss;
    ss << func << " " << msg;
    return ss.str();
}
} // namespace format
} // namespace logging
} // namespace el3D
