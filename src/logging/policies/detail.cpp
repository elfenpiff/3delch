#include "logging/policies/detail.hpp"

#include <chrono>

namespace el3D
{
namespace logging
{
namespace detail
{
std::string
GetCurrentUnixTime() noexcept
{
    return std::to_string(
        std::chrono::duration_cast< std::chrono::milliseconds >(
            std::chrono::system_clock::now().time_since_epoch() )
            .count() );
}
} // namespace detail
} // namespace logging
} // namespace el3D
