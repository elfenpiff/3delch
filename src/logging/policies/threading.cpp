#include "logging/policies/threading.hpp"

#include <chrono>
#include <cstdint>
#include <iostream>

namespace el3D
{
namespace logging
{
namespace threading
{
void
Disable::Call( const std::function< void() > &f ) const
{
    f();
}

void
ThreadSafe::Call( const std::function< void() > &f ) const
{
    std::lock_guard< std::mutex > lock( this->mtx );
    f();
}

InSeparateThread::InSeparateThread() noexcept
    : callThread( &InSeparateThread::BackgroundThread, this )
{
}

InSeparateThread::~InSeparateThread()
{
    while ( !this->callQueue.IsEmpty() )
        std::this_thread::sleep_for(
            std::chrono::nanoseconds( static_cast< uint64_t >(
                this->logClearTimeout.GetNanoSeconds() ) ) );

    this->callQueue.Terminate();
    this->callThread.join();
}

void
InSeparateThread::Call( const std::function< void() > &f ) const
{
    this->callQueue.Push( f );
}

void
InSeparateThread::BackgroundThread() const noexcept
{
    while ( true )
    {
        auto call = this->callQueue.BlockingPop();
        if ( !call.has_value() ) break;
        call.value()();
    }
}
} // namespace threading
} // namespace logging
} // namespace el3D
