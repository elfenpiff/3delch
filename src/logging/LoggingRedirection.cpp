#include "logging/elLog.hpp"

/*
int printf( const char* format, ... )
{
    va_list args;
    va_start( args, format );
    char buffer[256];
    vsnprintf( buffer, 256, format, args );
    va_end( args );

    elLog::Log( logging::INFO, "", 0, "printf" ) << buffer;
    return static_cast< int >( strlen( buffer ) );
}

int fprintf( FILE* stream, const char* format, ... )
{
    va_list args;
    va_start( args, format );
    char buffer[256];
    vsnprintf( buffer, 256, format, args );
    va_end( args );

    if ( stream == stdout )
        elLog::Log( logging::INFO, "", 0, "fprintf" ) << buffer;
    else if ( stream == stderr )
        elLog::Log( logging::ERROR, "", 0, "fprintf" ) << buffer;
    return static_cast< int >( strlen( buffer ) );
}
*/

namespace el3D
{
namespace logging
{
namespace redirection
{
LogBuffer::LogBuffer( const logging::logLevel_t logLevel,
                      const std::string&        source )
    : logLevel( logLevel ), source( source )
{
}

int
LogBuffer::sync()
{
    std::string temp = std::stringbuf::str();
    if ( temp.empty() ) return std::stringbuf::sync();

    for ( uint64_t pos = temp.find_first_of( "\n" ); pos != std::string::npos;
          temp = temp.substr( pos + 1 ), pos = temp.find_first_of( "\n" ) )
    {
        elLog::Log( this->logLevel, "undefined", 0, this->source,
                    elLog::DEFAULT_CHANNEL_ID )
            << buffer + temp.substr( 0, pos );
        buffer.clear();
    }

    buffer += temp;
    std::stringbuf::str( "" );
    return std::stringbuf::sync();
}

OutputRedirection::OutputRedirection( std::ostream&   out,
                                      std::streambuf* redirectTo )
    : out( out ), original( out.rdbuf( redirectTo ) )
{
}

OutputRedirection::~OutputRedirection()
{
    this->out.rdbuf( original );
}
} // namespace redirection


} // namespace logging
} // namespace el3D
