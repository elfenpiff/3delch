#include "OpenGL/elCamera.hpp"

#include <GL/glew.h>

namespace el3D
{
namespace OpenGL
{
elCamera::elCamera( const elCameraType type ) noexcept : type( type )
{
}

const elCamera::matrix_t&
elCamera::GetMatrices() const noexcept
{
    if ( this->hasUpdate || this->hasContinuousUpdate )
    {
        this->Update();
        this->hasUpdate = false;
    }

    return this->matrix;
}

bool
elCamera::HasUpdatedState() const noexcept
{
    return this->hasUpdate;
}

elCameraType
elCamera::GetType() const noexcept
{
    return this->type;
}

elCamera::zFactor_t
elCamera::GetZFactor() const noexcept
{
    return this->zFactor;
}

void
elCamera::SetZFactor( const units::Length zNear,
                      const units::Length zFar ) noexcept
{
    this->zFactor.zNear = zNear;
    this->zFactor.zFar  = zFar;
}


} // namespace OpenGL
} // namespace el3D
