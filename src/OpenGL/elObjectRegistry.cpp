#include "OpenGL/elObjectRegistry.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

namespace el3D
{
namespace OpenGL
{

static elObjectRegistry* ptr{ nullptr };


bool
elObjectRegistry::texture_t::operator==( const texture_t& rhs ) const noexcept
{
    return this->texture == rhs.texture && this->identifier == rhs.identifier &&
           this->target == rhs.target;
}

elObjectRegistry*
elObjectRegistry::GetInstance()
{
    static elObjectRegistry instance;
    return ptr;
}

elObjectRegistry::elObjectRegistry()
{
    ptr = this;
}

elObjectRegistry::~elObjectRegistry()
{
    ptr = nullptr;
}

uint64_t
elObjectRegistry::GetLastUpdateToken() const noexcept
{
    return this->lastUpdateToken;
}

void
elObjectRegistry::RegisterTexture( const std::string& identifier,
                                   const GLuint texture, const GLenum target )
{
    if ( identifier.empty() ) return;

    auto iter = bb::find_if( this->textureRegistry,
                             [&]( auto& v ) { return v.texture == texture; } );
    if ( iter != this->textureRegistry.end() ) return;

    this->textureRegistry.emplace_back(
        texture_t{ identifier, texture, target } );
    ++this->lastUpdateToken;
}

void
elObjectRegistry::UnregisterTexture( const GLuint texture )
{
    auto iter = bb::find_if( this->textureRegistry,
                             [&]( auto& v ) { return v.texture == texture; } );
    if ( iter == this->textureRegistry.end() ) return;
    this->textureRegistry.erase( iter );
    ++this->lastUpdateToken;
}

elObjectRegistry::textureRegistry_t
elObjectRegistry::GetRegisteredTextures() const
{
    return this->textureRegistry;
}

} // namespace OpenGL
} // namespace el3D
