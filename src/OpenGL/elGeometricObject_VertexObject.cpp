#include "OpenGL/elGeometricObject_VertexObject.hpp"

#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"

#include <cmath>
#include <initializer_list>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
const std::vector< elShaderProgram::shaderAttribute_t >
    elGeometricObject_VertexObject::ATTRIBUTES = {
        { "coord", 3 },        { "", 0, 0, 0, GL_UNSIGNED_INT },
        { "texCoord", 2 },     { "normal", 3 },
        { "tangent", 3 },      { "bitangent", 3 },
        { "diffuse", 4 },      { "eventColor", 4 },
        { "emissionColor", 4 } };


elGeometricObject_VertexObject::elGeometricObject_VertexObject(
    const DrawMode drawMode, const OpenGL::objectGeometry_t &objectGeometry,
    const std::vector< GLfloat > &diffuse,
    const std::vector< GLfloat > &eventColor,
    const std::vector< GLfloat > &emissionColor )
    : elGeometricObject_base(
          drawMode, 3, elGeometricObject_Type::VertexObject,
          objectGeometry.vertices, objectGeometry.elements, 2u,
          objectGeometry.textureCoordinates, 3u, objectGeometry.normals, 3u,
          objectGeometry.tangents, 3u, objectGeometry.bitangents, 4u, diffuse,
          4u, eventColor, 4u, emissionColor )
{
}

void
elGeometricObject_VertexObject::DrawImplementation(
    const uint64_t shaderID, const elCamera &camera ) const noexcept
{
    this->CallPreDrawCallback();

    this->Bind( shaderID, camera.GetMatrices().viewProjection );
    OpenGL::gl( glDrawElements, this->GetDrawModeForOpenGL(),
                static_cast< GLsizei >( this->GetNumberOfElements() ),
                static_cast< GLenum >( GL_UNSIGNED_INT ),
                static_cast< GLvoid * >( nullptr ) );
    this->Unbind( shaderID );
    this->CallPostDrawCallback();
}

bb::elExpected< size_t, elShaderProgram_Error >
elGeometricObject_VertexObject::AttachShader( const uint64_t shaderId ) noexcept
{
    auto &shader = this->shaders[shaderId];
    shader.RegisterAttributes( ATTRIBUTES );
    if ( !shader.attributes[BUFFER_VERTEX] )
    {
        LOG_ERROR( 0 ) << "the shader attribute "
                       << ATTRIBUTES[BUFFER_VERTEX].identifier
                       << " is required for the vertex object but not present "
                          "in the shader "
                       << shader.shader->GetFileAndGroupName();
        this->RemoveShader( shaderId );
        return bb::Error(
            elShaderProgram_Error::UnableToFindAttributeLocation );
    }

    shader.RegisterUniforms( { MODEL_MATRIX_ATTRIBUTE_NAME } );
    shader.RegisterUniforms( { VIEW_PROJECTION_ATTRIBUTE_NAME } );

    return bb::Success( shaderId );
}

void
elGeometricObject_VertexObject::Bind(
    const size_t shaderID, const glm::mat4 &viewProjection ) const noexcept
{
    auto &shader = this->shaders[shaderID];
    shader.shader->Bind();
    if ( shader.uniforms[UNIFORM_MODEL_MATRIX].id != -1 )
        shader.shader->SetUniform( shader.uniforms[UNIFORM_MODEL_MATRIX].id,
                                   this->GetModelMatrix() );
    if ( shader.uniforms[UNIFORM_VIEW_PROJECTION].id != -1 )
        shader.shader->SetUniform( shader.uniforms[UNIFORM_VIEW_PROJECTION].id,
                                   viewProjection );

    shader.BindTextures();

    this->objectBuffer.Bind();
    this->objectBuffer.Enable( { BUFFER_VERTEX, BUFFER_ELEMENT },
                               shader.attributes[BUFFER_VERTEX] );
    for ( size_t i = 0; i < BUFFER_END; ++i )
    {
        if ( i == BUFFER_VERTEX || i == BUFFER_ELEMENT ) continue;
        this->objectBuffer.Enable( { i }, shader.attributes[i] );
    }
}

void
elGeometricObject_VertexObject::Unbind( const size_t shaderID ) const noexcept
{
    this->objectBuffer.Disable( this->shaders[shaderID].attributes );
}

size_t
elGeometricObject_VertexObject::GetNumberOfElements() const noexcept
{
    return this->objectBuffer.GetNumberOfElements();
}

} // namespace OpenGL
} // namespace el3D
