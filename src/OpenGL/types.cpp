#include "OpenGL/types.hpp"


namespace el3D
{
namespace OpenGL
{
uint64_t
SizeOfType( const GLenum type ) noexcept
{
    switch ( type )
    {
        case GL_BYTE:
            return sizeof( EnumToDataType< GL_BYTE >::type );
        case GL_UNSIGNED_BYTE:
            return sizeof( EnumToDataType< GL_UNSIGNED_BYTE >::type );
        case GL_SHORT:
            return sizeof( EnumToDataType< GL_SHORT >::type );
        case GL_UNSIGNED_SHORT:
            return sizeof( EnumToDataType< GL_UNSIGNED_SHORT >::type );
        case GL_INT:
            return sizeof( EnumToDataType< GL_INT >::type );
        case GL_UNSIGNED_INT:
            return sizeof( EnumToDataType< GL_UNSIGNED_INT >::type );
        case GL_FLOAT:
            return sizeof( EnumToDataType< GL_FLOAT >::type );
        case GL_DOUBLE:
            return sizeof( EnumToDataType< GL_DOUBLE >::type );
        default:
            return 0;
    }
}
} // namespace OpenGL
} // namespace el3D
