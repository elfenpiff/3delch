#include "OpenGL/elTexture_2D.hpp"

#include "OpenGL/gl_Call.hpp"

namespace el3D
{
namespace OpenGL
{
elTexture_2D::elTexture_2D( const std::string& debugIdentifier,
                            const uint64_t sizeX, const uint64_t sizeY,
                            const GLint internalFormat, const GLenum format,
                            const GLenum type, const GLenum target ) noexcept
    : elTexture_base( debugIdentifier, internalFormat, format, type, target )
{
    this->SetSize( sizeX, sizeY );
}

elTexture_2D::elTexture_2D( const std::string&    debugIdentifier,
                            const GLAPI::elImage& image ) noexcept
    : elTexture_2D( debugIdentifier, image.GetDimensions().width,
                    image.GetDimensions().height )
{
    this->TexImage( image.GetBytePointer() );
}

elTexture_2D::elTexture_2D( const std::string& debugIdentifier,
                            const uint64_t sizeX, const uint64_t sizeY,
                            const GLint internalFormat, const GLenum format,
                            const GLenum type ) noexcept
    : elTexture_2D( debugIdentifier, sizeX, sizeY, internalFormat, format, type,
                    GL_TEXTURE_2D )
{
}

elTexture_2D::elTexture_2D( elTexture_2D&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elTexture_2D&
elTexture_2D::operator=( elTexture_2D&& rhs ) noexcept
{
    elTexture_base::operator=( std::move( rhs ) );
    return *this;
}

const elTexture_2D&
elTexture_2D::TexImage( const GLvoid* pixels ) const noexcept
{
    this->Bind();
    gl( glTexImage2D, this->target, 0, this->internalFormat,
        static_cast< GLsizei >( this->size.x ),
        static_cast< GLsizei >( this->size.y ), 0, this->format, this->type,
        pixels );
    return *this;
}

const elTexture_2D&
elTexture_2D::TexSubImage( const GLint offsetX, const GLint offsetY,
                           const uint64_t width, const uint64_t height,
                           const GLvoid* pixels ) const noexcept
{
    this->Bind();
    gl( glTexSubImage2D, this->target, 0, offsetX, offsetY,
        static_cast< GLsizei >( width ), static_cast< GLsizei >( height ),
        this->format, this->type, pixels );
    return *this;
}
} // namespace OpenGL
} // namespace el3D
