#include "OpenGL/elTexture_base.hpp"

#include "OpenGL/elObjectRegistry.hpp"
#include "OpenGL/gl_Call.hpp"
#include "logging/elLog.hpp"

#include <algorithm>
#include <cstdlib>
#include <ctime>

namespace el3D
{
namespace OpenGL
{
elTexture_base::elTexture_base( const GLuint id, const GLenum target ) noexcept
    : id( id ), target( target ), hasOwnerShip( false )
{
}

elTexture_base::elTexture_base() noexcept
    : elTexture_base( "loanedTexture", GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE,
                      GL_TEXTURE_2D )
{
}

elTexture_base::elTexture_base( const std::string& debugIdentifier,
                                const GLint internalFormat, const GLenum format,
                                const GLenum type,
                                const GLenum target ) noexcept
    : target{ target }, format{ format },
      internalFormat{ internalFormat }, type{ type }
{
    gl( glGenTextures, 1, &this->id );

#ifdef ELCH_DEBUG
    auto objectRegistry = elObjectRegistry::GetInstance();
    if ( objectRegistry )
        objectRegistry->RegisterTexture( debugIdentifier, this->id, target );
#else
    (void)debugIdentifier;
#endif

    this->SetMinMagFilter( GL_LINEAR, GL_LINEAR );
}

elTexture_base::~elTexture_base()
{
#ifdef ELCH_DEBUG
    if ( this->id != INVALID_ID && this->hasOwnerShip )
    {
        auto objectRegistry = elObjectRegistry::GetInstance();
        if ( objectRegistry ) objectRegistry->UnregisterTexture( this->id );
    }
#endif
    this->DeleteTexture();
}

elTexture_base::elTexture_base( elTexture_base&& rhs ) noexcept
    : id( rhs.id ), target( rhs.target ), size( rhs.size ),
      format( rhs.format ), internalFormat( rhs.internalFormat ),
      type( rhs.type )
{
    rhs.id = INVALID_ID;
}

elTexture_base&
elTexture_base::operator=( elTexture_base&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->DeleteTexture();
        this->id             = rhs.id;
        this->target         = rhs.target;
        this->size           = rhs.size;
        this->format         = rhs.format;
        this->internalFormat = rhs.internalFormat;
        this->type           = rhs.type;
        rhs.id               = INVALID_ID;
    }
    return *this;
}

void
elTexture_base::DeleteTexture() noexcept
{
    if ( this->id == INVALID_ID || !this->hasOwnerShip ) return;
    gl( glDeleteTextures, 1, &this->id );
#ifdef ELCH_DEBUG
    auto objectRegistry = elObjectRegistry::GetInstance();
    if ( objectRegistry ) objectRegistry->UnregisterTexture( this->id );
#endif
    this->id = INVALID_ID;
}

const elTexture_base*
elTexture_base::Bind() const noexcept
{
    gl( glBindTexture, this->target, this->id );
    return this;
}

void
elTexture_base::GenerateMipMap() const noexcept
{
    gl( glGenerateTextureMipmap, this->id );
}

const elTexture_base*
elTexture_base::BindAndActivate( const GLenum textureUnit ) const noexcept
{
    gl( glActiveTexture, textureUnit );
    gl( glBindTexture, this->target, this->id );
    return this;
}

const elTexture_base*
elTexture_base::BindActivateAndSetUniform(
    const GLint uniform, const GLenum textureUnit ) const noexcept
{
    gl( glActiveTexture, textureUnit );
    gl( glBindTexture, this->target, this->id );
    gl( glUniform1i, uniform,
        static_cast< GLint >( textureUnit - GL_TEXTURE0 ) );
    return this;
}

const elTexture_base*
elTexture_base::Unbind() const noexcept
{
    gl( glBindTexture, this->target, 0u );
    return this;
}

const elTexture_base*
elTexture_base::TexParameteri( const GLenum entry,
                               const GLint  value ) const noexcept
{
    this->Bind();
    gl( glTexParameteri, this->target, entry, value );
    return this;
}

const elTexture_base*
elTexture_base::TexParameterfv( const GLenum   entry,
                                const GLfloat* value ) const noexcept
{
    this->Bind();
    gl( glTexParameterfv, this->target, entry, value );
    return this;
}

const elTexture_base*
elTexture_base::TexParameterf( const GLenum  entry,
                               const GLfloat value ) const noexcept
{
    this->Bind();
    gl( glTexParameterf, this->target, entry, value );
    return this;
}

const elTexture_base*
elTexture_base::PixelStorei( const GLenum entry,
                             const GLint  value ) const noexcept
{
    this->Bind();
    gl( glPixelStorei, entry, value );
    return this;
}

std::unique_ptr< uint8_t[] >
elTexture_base::GetTexImage() const noexcept
{
    auto retVal = std::make_unique< uint8_t[] >(
        static_cast< size_t >( this->size.sizeInBytes ) );
    this->Bind();
    gl( glGetTexImage, this->target, 0, this->format, this->type,
        retVal.get() );

    return retVal;
}

elTexture_base&
elTexture_base::SetFormat( const GLenum value ) noexcept
{
    this->format = value;
    return *this;
}

elTexture_base&
elTexture_base::SetInternalFormat( const GLint value ) noexcept
{
    this->internalFormat = value;
    return *this;
}

elTexture_base&
elTexture_base::SetType( const GLenum value ) noexcept
{
    this->type = value;
    return *this;
}

elTexture_base::texSize_t
elTexture_base::GetSize() const noexcept
{
    return this->size;
}

elTexture_base&
elTexture_base::SetSize( const uint64_t x, const uint64_t y, const uint64_t z,
                         const uint64_t arraySize ) noexcept
{
    uint64_t adjustedX = x, adjustedY = y, adjustedZ = z,
             adjustedArraySize = arraySize;

    if ( this->target == GL_TEXTURE_2D || this->target == GL_TEXTURE_2D_ARRAY ||
         this->target == GL_TEXTURE_CUBE_MAP )
    {
        uint64_t maxTextureSize = static_cast< uint64_t >(
            Get< GLint >( ( this->target == GL_TEXTURE_CUBE_MAP )
                              ? GL_MAX_CUBE_MAP_TEXTURE_SIZE
                              : GL_MAX_TEXTURE_SIZE ) );

        if ( adjustedX > maxTextureSize || adjustedY > maxTextureSize )
        {
            LOG_ERROR( 0 ) << "The maximum supported texture size is "
                           << maxTextureSize << "x" << maxTextureSize
                           << " but the requested texture size is " << adjustedX
                           << "x" << adjustedY
                           << ". Cropping texture to maximum supported size.";

            adjustedX = std::min( maxTextureSize, adjustedX );
            adjustedY = std::min( maxTextureSize, adjustedY );
        }

        if ( adjustedZ != 0 ) adjustedZ = 0;
    }

    if ( this->target == GL_TEXTURE_2D_ARRAY )
    {
        uint64_t maxArraySize = static_cast< uint64_t >(
            Get< GLint >( GL_MAX_ARRAY_TEXTURE_LAYERS ) );
        if ( adjustedArraySize > maxArraySize )
        {
            LOG_ERROR( 0 )
                << "The maximum supported texture array size is "
                << maxArraySize << " but the requested array size of "
                << adjustedArraySize
                << " exceeds it. Cropping array to maximum supported size.";
            adjustedArraySize = maxArraySize;
        }
    }

    uint64_t sizeMinimum = 1;
    uint64_t sizeInBytes = std::max( sizeMinimum, adjustedX ) *
                           std::max( sizeMinimum, adjustedY ) *
                           std::max( sizeMinimum, adjustedZ ) *
                           std::max( sizeMinimum, adjustedArraySize );
    this->size = texSize_t{ adjustedX, adjustedY, adjustedZ, adjustedArraySize,
                            sizeInBytes };
    this->Reshape();
    return *this;
}

elTexture_base&
elTexture_base::SetWrap( const GLint wrapS, const GLint wrapT,
                         const GLint wrapR ) noexcept
{
    this->TexParameteri( GL_TEXTURE_WRAP_S, wrapS );
    this->TexParameteri( GL_TEXTURE_WRAP_T, wrapT );
    this->TexParameteri( GL_TEXTURE_WRAP_R, wrapR );
    return *this;
}

elTexture_base&
elTexture_base::SetMinMagFilter( const GLint minFilter,
                                 const GLint magFilter ) noexcept
{
    this->TexParameteri( GL_TEXTURE_MIN_FILTER, minFilter );
    this->TexParameteri( GL_TEXTURE_MAG_FILTER, magFilter );
    return *this;
}

elTexture_base&
elTexture_base::SetCompareParameters( const GLint compareMode,
                                      const GLint compareFunc ) noexcept
{
    this->TexParameteri( GL_TEXTURE_COMPARE_MODE, compareMode );
    this->TexParameteri( GL_TEXTURE_COMPARE_FUNC, compareFunc );
    return *this;
}

GLenum
elTexture_base::GetType() const noexcept
{
    return type;
}

GLint
elTexture_base::GetInternalFormat() const noexcept
{
    return internalFormat;
}

GLenum
elTexture_base::GetFormat() const noexcept
{
    return format;
}

GLuint
elTexture_base::GetID() const noexcept
{
    return id;
}

GLenum
elTexture_base::GetTarget() const noexcept
{
    return target;
}

void
elTexture_base::Reshape() const noexcept
{
    this->Bind();
    switch ( this->target )
    {
        case GL_TEXTURE_CUBE_MAP:
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_X ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_X ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_Y ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_Z ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            gl( glTexImage2D,
                static_cast< GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z ), 0,
                this->internalFormat, static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            break;
        case GL_TEXTURE_2D:
            gl( glTexImage2D, this->target, 0, this->internalFormat,
                static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ), 0, this->format,
                this->type, nullptr );
            break;
        case GL_TEXTURE_2D_ARRAY:
            gl( glTexImage3D, this->target, 0, this->internalFormat,
                static_cast< GLsizei >( this->size.x ),
                static_cast< GLsizei >( this->size.y ),
                static_cast< GLsizei >( this->size.arraySize ), 0, this->format,
                this->type, nullptr );
            break;
    }
}

std::vector< utils::byte_t >
elTexture_base::GenerateRandomTexture( const size_t size ) noexcept
{
    static unsigned int seed = static_cast< unsigned int >( time( 0 ) );
    srand( seed );

    std::vector< utils::byte_t > noise( size );
    for ( size_t i = 0; i < size; ++i )
        noise[i] = static_cast< utils::byte_t >(
            1 + rand() / static_cast< int >( ( RAND_MAX + 1u ) / 255 ) );
    seed = static_cast< unsigned int >( rand() );

    return noise;
}


} // namespace OpenGL
} // namespace el3D
