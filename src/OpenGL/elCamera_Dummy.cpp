#include "OpenGL/elCamera_Dummy.hpp"

namespace el3D
{
namespace OpenGL
{
elCamera_Dummy::elCamera_Dummy() noexcept : elCamera( elCameraType::base )
{
    this->hasUpdate = false;
}

elCamera_Dummy::elCamera_Dummy( const glm::mat4& viewProjection ) noexcept
    : elCamera_Dummy()
{
    this->matrix.viewProjection = viewProjection;
}

void
elCamera_Dummy::SetProjection( const glm::mat4& value ) noexcept
{
    this->matrix.projection = value;
}

void
elCamera_Dummy::SetView( const glm::mat4& value ) noexcept
{
    this->matrix.view = value;
}

void
elCamera_Dummy::SetViewProjection( const glm::mat4& value ) noexcept
{
    this->matrix.viewProjection = value;
}

void
elCamera_Dummy::Update() const noexcept
{
}

} // namespace OpenGL
} // namespace el3D
