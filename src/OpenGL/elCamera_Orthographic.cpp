#include "OpenGL/elCamera_Orthographic.hpp"

namespace el3D
{
namespace OpenGL
{
elCamera_Orthographic::elCamera_Orthographic() noexcept
    : elCamera( elCameraType::Orthographic )
{
}

elCamera_Orthographic::elCamera_Orthographic( const frustum_t& frustum,
                                              const zFactor_t& zFactor,
                                              const lookAt_t&  lookAt ) noexcept
    : elCamera( elCameraType::Orthographic ), frustum{ frustum },
      lookAt{ lookAt }, hasUpdate{ true }
{
    this->zFactor = zFactor;
}

void
elCamera_Orthographic::SetLookAt( const lookAt_t& value ) noexcept
{
    this->lookAt    = value;
    this->hasUpdate = true;
}

void
elCamera_Orthographic::SetFrustum( const frustum_t& value ) noexcept
{
    this->frustum   = value;
    this->hasUpdate = true;
}

void
elCamera_Orthographic::Update() const noexcept
{
    this->matrix.projection =
        glm::ortho( this->frustum.left, this->frustum.right,
                    this->frustum.bottom, this->frustum.top,
                    static_cast< float >( this->zFactor.zNear.GetMeter() ),
                    static_cast< float >( this->zFactor.zFar.GetMeter() ) );
    this->matrix.view =
        glm::lookAt( this->lookAt.eye, this->lookAt.center, this->lookAt.up );
    this->matrix.viewProjection = this->matrix.projection * this->matrix.view;

    this->hasUpdate = false;
}

} // namespace OpenGL
} // namespace el3D
