#include "OpenGL/elTexture_2D_Array.hpp"

#include "OpenGL/gl_Call.hpp"

namespace el3D
{
namespace OpenGL
{
elTexture_2D_Array::elTexture_2D_Array(
    const std::string& debugIdentifier, const uint64_t sizeX,
    const uint64_t sizeY, const uint64_t arraySize, const GLint internalFormat,
    const GLenum format, const GLenum type ) noexcept
    : elTexture_2D( debugIdentifier, sizeX, sizeY, internalFormat, format, type,
                    GL_TEXTURE_2D_ARRAY )
{
    this->SetSize( sizeX, sizeY, 0, arraySize );
}

elTexture_2D_Array::elTexture_2D_Array( elTexture_2D_Array&& rhs ) noexcept
    : elTexture_2D( "", rhs.size.x, rhs.size.y, rhs.internalFormat, rhs.format,
                    rhs.type, rhs.target )
{
    *this = std::move( rhs );
}

elTexture_2D_Array&
elTexture_2D_Array::operator=( elTexture_2D_Array&& rhs ) noexcept
{
    elTexture_base::operator=( std::move( rhs ) );
    return *this;
}

const elTexture_2D_Array&
elTexture_2D_Array::TexImage( const uint64_t index,
                              const GLvoid*  pixels ) const noexcept
{
    this->Bind();
    gl( glTexSubImage3D, this->target, 0, 0, 0, static_cast< GLsizei >( index ),
        static_cast< GLsizei >( this->size.x ),
        static_cast< GLsizei >( this->size.y ), 1, this->format, this->type,
        pixels );

    return *this;
}

const elTexture_2D_Array&
elTexture_2D_Array::TexSubImage( const uint64_t index, const GLint offsetX,
                                 const GLint offsetY, const uint64_t width,
                                 const uint64_t height,
                                 const GLvoid*  pixels ) const noexcept
{
    this->Bind();
    gl( glTexSubImage3D, this->target, 0, offsetX, offsetY,
        static_cast< GLsizei >( index ), static_cast< GLsizei >( width ),
        static_cast< GLsizei >( height ), static_cast< GLsizei >( index ),
        this->format, this->type, pixels );
    return *this;
}

} // namespace OpenGL
} // namespace el3D
