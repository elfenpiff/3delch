#include "OpenGL/elTextureBufferObject.hpp"

#include "OpenGL/gl_Call.hpp"

namespace el3D
{
namespace OpenGL
{
elTextureBufferObject::elTextureBufferObject( const size_t n,
                                              const GLenum internalFormat,
                                              const GLenum usage ) noexcept
    : elBufferObject( n, GL_TEXTURE_BUFFER, usage ), textureIDs( n ),
      internalFormat( internalFormat )
{
    gl( glGenTextures, static_cast< GLsizei >( n ), this->textureIDs.data() );
}

elTextureBufferObject::elTextureBufferObject(
    elTextureBufferObject&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elTextureBufferObject::~elTextureBufferObject()
{
    if ( this->textureIDs.size() > 0 )
        gl( glDeleteTextures, static_cast< GLsizei >( this->textureIDs.size() ),
            this->textureIDs.data() );
}

void
elTextureBufferObject::Bind( const GLint uniform, const GLenum textureUnit,
                             const size_t n ) const noexcept
{
    if ( uniform == -1 ) return;
    gl( glBindBuffer, this->bufferTarget[n], this->bufferIDs[n] );
    gl( glActiveTexture, textureUnit );
    gl( glBindTexture, static_cast< GLenum >( GL_TEXTURE_BUFFER ),
        this->textureIDs[n] );
    gl( glTexBuffer, static_cast< GLenum >( GL_TEXTURE_BUFFER ),
        this->internalFormat, this->bufferIDs[n] );
    gl( glUniform1i, uniform,
        static_cast< GLint >( textureUnit - GL_TEXTURE0 ) );
}

void
elTextureBufferObject::Bind( const size_t n ) const noexcept
{
    elBufferObject::Bind( n );
}

void
elTextureBufferObject::Unbind( const size_t n ) const noexcept
{
    gl( glBindBuffer, this->bufferTarget[n], 0u );
    gl( glBindTexture, static_cast< GLenum >( GL_TEXTURE_BUFFER ), 0u );
}

elTextureBufferObject&
elTextureBufferObject::operator=( elTextureBufferObject&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        *static_cast< elBufferObject* >( this ) =
            std::move( *static_cast< elBufferObject* >( &rhs ) );

        if ( this->textureIDs.size() > 0 )
            gl( glDeleteTextures,
                static_cast< GLsizei >( this->textureIDs.size() ),
                this->textureIDs.data() );

        this->internalFormat = std::move( rhs.internalFormat );
        this->textureIDs     = std::move( rhs.textureIDs );
    }
    return *this;
}
} // namespace OpenGL
} // namespace el3D
