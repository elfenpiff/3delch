#include "OpenGL/elShaderProgram.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/gl_Call.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"

#include <iterator>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on


namespace el3D
{
namespace OpenGL
{
GLuint elShaderProgram::activeShader = 0;

bb::elExpected< elShaderProgram_Error >
elShaderProgram::Init( const std::string &vertSrc, const std::string &fragSrc,
                       const std::string &geomSrc, const std::string &tCtrlSrc,
                       const std::string &tEvalSrc, const std::string &compSrc )
{
    this->id = glVoid( glCreateProgram );

    bool                  hasFailed = false;
    elShaderProgram_Error error;

    if ( vertSrc.size() != 0 && !this->SetSource( vertSrc, GL_VERTEX_SHADER ) )
    {
        hasFailed = true;
        error     = elShaderProgram_Error::CouldNotCompile_VertexShader;
        LOG_ERROR( 0 ) << "could not compile GL_VERTEX_SHADER";
    }

    if ( !hasFailed && fragSrc.size() != 0 &&
         !this->SetSource( fragSrc, GL_FRAGMENT_SHADER ) )
    {
        hasFailed = true;
        error     = elShaderProgram_Error::CouldNotCompile_FragmentShader;
        LOG_ERROR( 0 ) << "could not compile GL_FRAGMENT_SHADER";
    }

    if ( !hasFailed && geomSrc.size() != 0 &&
         !this->SetSource( geomSrc, GL_GEOMETRY_SHADER ) )
    {
        hasFailed = true;
        error     = elShaderProgram_Error::CouldNotCompile_GeometryShader;
        LOG_ERROR( 0 ) << "could not compile GL_GEOMETRY_SHADER";
    }

    if ( !hasFailed && tCtrlSrc.size() != 0 &&
         !this->SetSource( tCtrlSrc, GL_TESS_CONTROL_SHADER ) )
    {
        hasFailed = true;
        error =
            elShaderProgram_Error::CouldNotCompile_TessellationControlShader;
        LOG_ERROR( 0 ) << "could not compile GL_TESS_CONTROL_SHADER";
    }

    if ( !hasFailed && tEvalSrc.size() != 0 &&
         !this->SetSource( tEvalSrc, GL_TESS_EVALUATION_SHADER ) )
    {
        hasFailed = true;
        error =
            elShaderProgram_Error::CouldNotCompile_TessellationEvaluationShader;
        LOG_ERROR( 0 ) << "could not compile GL_TESS_EVALUATION_SHADER";
    }

    if ( !hasFailed && compSrc.size() != 0 &&
         !this->SetSource( compSrc, GL_COMPUTE_SHADER ) )
    {
        hasFailed = true;
        error     = elShaderProgram_Error::CouldNotCompile_ComputeShader;
        LOG_ERROR( 0 ) << "could not compile GL_COMPUTE_SHADER";
    }

    if ( hasFailed || !this->Link() )
    {
        for ( auto shader : this->attachedShader )
        {
            gl( glDeleteShader, shader.first );
        }
        gl( glDeleteProgram, this->id );

        return bb::Error( error );
    }

    return bb::Success<>();
}

elShaderProgram::elShaderProgram( const shaderFromSource_t,
                                  const std::string &vertSrc,
                                  const std::string &fragSrc,
                                  const std::string &geomSrc,
                                  const std::string &tCtrlSrc,
                                  const std::string &tEvalSrc,
                                  const std::string &compSrc ) noexcept
{
    this->Init( vertSrc, fragSrc, geomSrc, tCtrlSrc, tEvalSrc, compSrc )
        .AndThen( [&] { this->create_isInitialized = true; } )
        .OrElse( [&]( const auto &r ) {
            this->create_isInitialized = false;
            this->create_error         = r;
        } );
}

elShaderProgram::elShaderProgram(
    const shaderFromFile_t, const std::string &fileName,
    const std::string &group, const int shaderVersion,
    const elGLSLParser::staticDefines_t &defines ) noexcept
    : fileName( fileName ), groupName( group )
{
    auto shaderSource =
        OpenGL::elGLSLParser::Create( fileName, shaderVersion, defines );
    if ( shaderSource.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to open shader file \"" << fileName << "\"";
        this->create_isInitialized = false;
        this->create_error =
            elShaderProgram_Error::UnableToOpenShaderSourceFile;
        return;
    }

    auto &content = ( *shaderSource )->GetContent();
    auto  iter    = content.find( group );
    if ( iter == content.end() )
    {
        LOG_ERROR( 0 ) << "could not find source group: " + group +
                              " in shader " + fileName;

        this->create_isInitialized = false;
        this->create_error = elShaderProgram_Error::CouldNotFindSourceGroup;
        return;
    }

    std::vector< GLenum > shaderType = {
        GL_VERTEX_SHADER,       GL_FRAGMENT_SHADER,        GL_GEOMETRY_SHADER,
        GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_COMPUTE_SHADER };
    std::vector< std::string > code( shaderType.size() );
    for ( size_t i = 0, l = code.size(); i < l; ++i )
    {
        auto iterCode = iter->second.find( shaderType[i] );
        if ( iterCode != iter->second.end() ) code[i] = iterCode->second.code;
    }

    this->fileName  = ( *shaderSource )->shaderFile.GetPath();
    this->groupName = group;
    this->Init( code[0], code[1], code[2], code[3], code[4], code[5] )
        .AndThen( [&] { this->create_isInitialized = true; } )
        .OrElse(
            [&]( const auto &r ) {
                LOG_ERROR( 0 )
                    << "error in file: " << fileName << ":" << group
                    << ", failed to compile the following shader code: \n"
                    << code[0] << "\n"
                    << code[1] << "\n"
                    << code[2] << "\n"
                    << code[3] << "\n"
                    << code[4] << "\n"
                    << code[5];

                this->create_isInitialized = false;
                this->create_error         = r;
            } );
}

elShaderProgram::elShaderProgram( elShaderProgram &&rhs )
{
    this->id             = std::move( rhs.id );
    this->attachedShader = std::move( rhs.attachedShader );
    rhs.id               = 0;
}

elShaderProgram::~elShaderProgram()
{
    for ( auto shader : this->attachedShader )
    {
        gl( glDeleteShader, shader.first );
    }

    if ( this->id != 0 )
    {
        gl( glDeleteProgram, this->id );
    }
}

elShaderProgram &
elShaderProgram::operator=( elShaderProgram &&rhs )
{
    if ( this != &rhs )
    {
        this->id             = std::move( rhs.id );
        this->attachedShader = std::move( rhs.attachedShader );
        rhs.id               = 0;
    }

    return *this;
}

bb::elExpected< elShaderProgram_Error >
elShaderProgram::SetSource( const std::string &_source,
                            const GLenum       type ) noexcept
{
    const GLchar *source   = _source.c_str();
    GLuint        shaderID = gl( glCreateShader, type );
    LOG_DEBUG( 0 ) << "setting source for " << OpenGL::EnumToString( type )
                   << " [program-id: " << this->id << "]";
    if ( shaderID == 0 )
    {
        LOG_ERROR( 0 ) << "could not create " << OpenGL::EnumToString( type )
                       << " from source [program-id: " << this->id << "]";
        return bb::Error( elShaderProgram_Error::UnableToCreateShader );
    }

    gl( glShaderSource, shaderID, 1, &source, nullptr );
    gl( glCompileShader, shaderID );

    GLint isCompiled = GL_FALSE;
    gl( glGetShaderiv, shaderID, static_cast< GLenum >( GL_COMPILE_STATUS ),
        &isCompiled );
    if ( isCompiled == GL_FALSE )
    {
        LOG_ERROR( 0 ) << "could not compile " << this->fileName << ":"
                       << this->groupName << " [shader-id: " << shaderID << "] "
                       << OpenGL::EnumToString( type )
                       << " [program-id: " << this->id
                       << "]: " << this->GetInfoLog( shaderID );
        return bb::Error( elShaderProgram_Error::UnableToCompileShader );
    }

    this->attachedShader.push_back( std::make_pair( shaderID, type ) );
    return bb::Success<>();
}

void
elShaderProgram::SetFileName( const std::string &v ) noexcept
{
    this->fileName = v;
}

std::string
elShaderProgram::GetFileAndGroupName() const noexcept
{
    return ( ( this->fileName.empty() ) ? "file=no-file-source"
                                        : this->fileName ) +
           ":" +
           ( ( this->groupName.empty() ) ? "group=no-group-source"
                                         : this->groupName );
}

bb::elExpected< elShaderProgram_Error >
elShaderProgram::Link() const noexcept
{
    LOG_DEBUG( 0 ) << "linking shader [program-id: " << this->id << "]";
    for ( auto shader : this->attachedShader )
    {
        LOG_DEBUG( 0 ) << "attaching " << OpenGL::EnumToString( shader.second )
                       << " to [program-id: " << this->id << "]";
        gl( glAttachShader, this->id, shader.first );
    }

    GLint isLinked;
    gl( glLinkProgram, this->id );
    gl( glGetProgramiv, this->id, static_cast< GLenum >( GL_LINK_STATUS ),
        &isLinked );

    if ( isLinked == GL_FALSE )
    {
        LOG_ERROR( 0 ) << "could not link shader program [program-id: "
                       << this->id << "]: " << this->GetInfoLog( this->id );
        return bb::Error( elShaderProgram_Error::UnableToLinkShader );
    }

    for ( auto shader : this->attachedShader )
        gl( glDetachShader, this->id, shader.first );

    return bb::Success<>();
}

std::string
elShaderProgram::GetInfoLog( const GLuint shaderID ) const noexcept
{
    GLsizei maxLogLength = 1024;
    GLsizei logLength    = 0;
    GLchar *log          = nullptr;

    if ( glIsShader( shaderID ) )
    {
        gl( glGetShaderiv, shaderID,
            static_cast< GLenum >( GL_INFO_LOG_LENGTH ), &maxLogLength );
        log = (GLchar *)malloc( static_cast< size_t >(
            sizeof( GLchar ) * static_cast< size_t >( maxLogLength ) ) );
        gl( glGetShaderInfoLog, shaderID, maxLogLength, &logLength, log );
    }
    else if ( glIsProgram( shaderID ) )
    {
        gl( glGetProgramiv, shaderID,
            static_cast< GLenum >( GL_INFO_LOG_LENGTH ), &maxLogLength );
        log = (GLchar *)malloc( static_cast< size_t >(
            sizeof( GLchar ) * static_cast< size_t >( maxLogLength ) ) );
        gl( glGetProgramInfoLog, shaderID, maxLogLength, &logLength, log );
    }
    else
    {
        LOG_WARN( 0 ) << shaderID
                      << " is neither a program nor a shader [program-id:"
                      << this->id << "]";
    }

    std::string retVal;
    if ( log != nullptr )
    {
        if ( logLength > 0 ) retVal = log;
        free( log );
    }

    return retVal;
}

GLuint
elShaderProgram::GetShaderID() const noexcept
{
    return this->id;
}

void
elShaderProgram::Bind() const noexcept
{
    if ( this->id == elShaderProgram::activeShader ) return;

    this->BindNoCallback();
    if ( this->bindCallback ) this->bindCallback( *this );
}

void
elShaderProgram::BindNoCallback() const noexcept
{
    if ( this->id == elShaderProgram::activeShader ) return;

    gl( glUseProgram, this->id );
    elShaderProgram::activeShader = this->id;
}

void
elShaderProgram::Unbind() const noexcept
{
    gl( glUseProgram, 0u );
    elShaderProgram::activeShader = 0;
}

bool
elShaderProgram::ValidateProgram() const noexcept
{
    GLint isValid;
    gl( glValidateProgram, this->id );
    gl( glGetProgramiv, this->id, static_cast< GLenum >( GL_VALIDATE_STATUS ),
        &isValid );
    if ( isValid == GL_FALSE )
    {
        LOG_WARN( 0 ) << "could not validate shader program [program-id: "
                      << this->id << "]: " << this->GetInfoLog( this->id );
        return false;
    }
    return true;
}

void
elShaderProgram::SetBindCallback(
    const std::function< void( const elShaderProgram & ) > &f ) noexcept
{
    this->bindCallback = f;
}

bb::elExpected< elShaderProgram::shaderAttribute_t, elShaderProgram_Error >
elShaderProgram::GetAttribute( const std::string &identifier, const GLint size,
                               const GLuint dimension, const GLuint divisor,
                               const GLenum dataType ) const
{
    bb::Expects( 1 <= size && size <= 4 && 1 <= dimension && dimension <= 4 );
    auto attribLocation = this->GetAttributeLocation( identifier );
    if ( attribLocation.HasError() )
        return bb::Error( attribLocation.GetError() );

    return bb::Success< shaderAttribute_t >(
        { static_cast< GLuint >( attribLocation.GetValue() ), identifier, size,
          dimension, dataType, divisor } );
}

bb::elExpected< GLint, elShaderProgram_Error >
elShaderProgram::GetAttributeLocation( const std::string &name ) const
{
    GLint retVal = gl( glGetAttribLocation, this->id, name.c_str() );
    if ( retVal == -1 )
    {
        LOG_ERROR( 0 ) << "could not find shader attribute " << name
                       << " [program-id: " << std::to_string( this->id )
                       << " ] " << this->fileName << ":" << this->groupName;
        return bb::Error(
            elShaderProgram_Error::UnableToFindAttributeLocation );
    }

    return bb::Success( retVal );
}

bool
elShaderProgram::HasAttribute( const std::string &name ) const
{
    GLint retVal = gl( glGetAttribLocation, this->id, name.c_str() );
    return retVal != -1;
}

bb::elExpected< GLint, elShaderProgram_Error >
elShaderProgram::GetUniformLocation( const std::string &name ) const
{
    GLint retVal = gl( glGetUniformLocation, this->id, name.c_str() );
    if ( retVal == -1 )
    {
        LOG_ERROR( 0 ) << "could not find shader uniform " << name
                       << " [program-id: " << std::to_string( this->id ) << "] "
                       << this->fileName << ":" << this->groupName;
        return bb::Error( elShaderProgram_Error::UnableToFindUniformLocation );
    }

    return bb::Success( retVal );
}

bool
elShaderProgram::HasUniform( const std::string &name ) const
{
    GLint retVal = gl( glGetUniformLocation, this->id, name.c_str() );
    return retVal != -1;
}

bool
elShaderProgram::HasUniformBlock( const std::string &name ) const
{
    GLuint retVal = gl( glGetUniformBlockIndex, this->id, name.c_str() );
    return retVal != GL_INVALID_INDEX;
}

bb::elExpected< GLuint, elShaderProgram_Error >
elShaderProgram::GetUniformBlockIndex( const std::string &name ) const
{
    GLuint retVal = gl( glGetUniformBlockIndex, this->id, name.c_str() );
    if ( retVal == GL_INVALID_INDEX )
    {
        LOG_ERROR( 0 ) << "could not find uniform block " << name
                       << " [program-id: " << std::to_string( this->id ) << "] "
                       << this->fileName << ":" << this->groupName;
        return bb::Error(
            elShaderProgram_Error::UnableToFindUniformBlockIndex );
    }

    return bb::Success( retVal );
}

GLint
elShaderProgram::GetActiveUniformBlockiv( const GLuint uniformBlockIndex,
                                          const GLenum pname ) const noexcept
{
    GLint retVal;
    gl( glGetActiveUniformBlockiv, this->id, uniformBlockIndex, pname,
        &retVal );
    return retVal;
}

bb::elExpected< std::vector< GLuint >, elShaderProgram_Error >
elShaderProgram::GetUniformIndices(
    const std::vector< std::string > &uniformNames ) const
{
    std::vector< GLuint >   retVal( uniformNames.size() );
    std::vector< GLchar * > convertedUniformNames( uniformNames.size() );

    for ( size_t k = 0, l = uniformNames.size(); k < l; ++k )
        convertedUniformNames[k] =
            const_cast< GLchar * >( uniformNames[k].c_str() );

    gl( glGetUniformIndices, this->id,
        static_cast< GLsizei >( uniformNames.size() ),
        convertedUniformNames.data(), retVal.data() );
    for ( size_t k = 0, l = retVal.size(); k < l; ++k )
        if ( retVal[k] == GL_INVALID_INDEX )
        {
            LOG_ERROR( 0 ) << "could not find uniform block variable "
                           << uniformNames[k]
                           << " [program-id:" << std::to_string( this->id )
                           << "] " << this->fileName << ":" << this->groupName;
            return bb::Error(
                elShaderProgram_Error::UnableToFindUniformIndices );
        }

    return bb::Success( retVal );
}

std::vector< GLint >
elShaderProgram::GetActiveUniformsiv(
    const std::vector< GLuint > &uniformIndices,
    const GLenum                 pname ) const noexcept
{
    std::vector< GLint > retVal( uniformIndices.size() );
    gl( glGetActiveUniformsiv, this->id,
        static_cast< GLsizei >( uniformIndices.size() ), uniformIndices.data(),
        pname, retVal.data() );
    return retVal;
}

elShaderProgram::shaderAttribute_t::shaderAttribute_t(
    const GLuint id, const std::string &identifier, const GLint size,
    const GLuint dimension, const GLenum dataType,
    const GLuint divisor ) noexcept
    : id( id ), identifier( identifier ), size( size ), dimension( dimension ),
      dataType( dataType ), divisor( divisor )
{
}

elShaderProgram::shaderAttribute_t::shaderAttribute_t(
    const std::string &identifier, const GLint size, const GLuint dimension,
    const GLuint divisor, const GLenum dataType ) noexcept
    : shaderAttribute_t( INVALID_ID, identifier, size, dimension, dataType,
                         divisor )
{
}

elShaderProgram::shaderAttribute_t::operator bool() const noexcept
{
    return this->id != INVALID_ID;
}

// 1 dimension
template <>
void
elShaderProgram::SetUniform( const GLint    uniformID,
                             const GLfloat &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform1f, uniformID, value );
}

template <>
void
elShaderProgram::SetUniform( const GLint     uniformID,
                             const GLdouble &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform1d, uniformID, value );
}

template <>
void
elShaderProgram::SetUniform( const GLint  uniformID,
                             const GLint &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform1i, uniformID, value );
}

template <>
void
elShaderProgram::SetUniform( const GLint uniformID,
                             const bool &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform1i, uniformID, ( value ) ? 1 : 0 );
}

template <>
void
elShaderProgram::SetUniform( const GLint   uniformID,
                             const GLuint &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform1ui, uniformID, value );
}

// 2 dimension
template <>
void
elShaderProgram::SetUniform( const GLint      uniformID,
                             const glm::vec2 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform2f, uniformID, value.x, value.y );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::dvec2 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform2d, uniformID, value.x, value.y );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::ivec2 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform2i, uniformID, value.x, value.y );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::uvec2 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform2ui, uniformID, value.x, value.y );
}

// 3 dimension
template <>
void
elShaderProgram::SetUniform( const GLint      uniformID,
                             const glm::vec3 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform3f, uniformID, value.x, value.y, value.z );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::dvec3 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform3d, uniformID, value.x, value.y, value.z );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::ivec3 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform3i, uniformID, value.x, value.y, value.z );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::uvec3 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform3ui, uniformID, value.x, value.y, value.z );
}

// 4 dimension
template <>
void
elShaderProgram::SetUniform( const GLint      uniformID,
                             const glm::vec4 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform4f, uniformID, value.x, value.y, value.z, value.w );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::dvec4 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform4d, uniformID, value.x, value.y, value.z, value.w );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::ivec4 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform4i, uniformID, value.x, value.y, value.z, value.w );
}

template <>
void
elShaderProgram::SetUniform( const GLint       uniformID,
                             const glm::uvec4 &value ) const noexcept
{
    this->BindNoCallback();
    gl( glUniform4ui, uniformID, value.x, value.y, value.z, value.w );
}

//  MATRIX
// 2xN
template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat2x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2dv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat2x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2x3dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat2x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2x4dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat2x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2fv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat2x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2x3fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat2x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix2x4fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

// 3xN
template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat3x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3x2dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat3x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3dv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat3x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3x4dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat3x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3x2fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat3x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3fv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat3x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix3x4fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

// 4xN
template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat4x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4x2dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat4x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4x3dv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint         uniformID,
                             const glm::dmat4x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4dv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat4x2 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4x2fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat4x3 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4x3fv, uniformID, 1, transpose,
        glm::value_ptr( value ) );
}

template <>
void
elShaderProgram::SetUniform( const GLint        uniformID,
                             const glm::mat4x4 &value ) const noexcept
{
    GLboolean transpose = GL_FALSE;
    this->BindNoCallback();
    gl( glUniformMatrix4fv, uniformID, 1, transpose, glm::value_ptr( value ) );
}

template <>
GLfloat
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value;
    gl( glGetUniformfv, this->id, uniformID, &value );
    return value;
}

template <>
GLint
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLint value;
    gl( glGetUniformiv, this->id, uniformID, &value );
    return value;
}

template <>
GLuint
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLuint value;
    gl( glGetUniformuiv, this->id, uniformID, &value );
    return value;
}

template <>
GLdouble
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value;
    gl( glGetUniformdv, this->id, uniformID, &value );
    return value;
}

// float
template <>
glm::vec2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[2];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 2 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_vec2( &( value[0] ) );
}

template <>
glm::vec3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[3];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 3 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_vec3( &( value[0] ) );
}

template <>
glm::vec4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[4];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_vec4( &( value[0] ) );
}

template <>
glm::mat2x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[4];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat2x2( &( value[0] ) );
}

template <>
glm::mat2x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[6];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 6 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat2x3( &( value[0] ) );
}

template <>
glm::mat2x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[8];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 8 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat2x4( &( value[0] ) );
}

template <>
glm::mat3x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[6];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 6 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat3x2( &( value[0] ) );
}

template <>
glm::mat3x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[9];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 9 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat3x3( &( value[0] ) );
}

template <>
glm::mat3x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[12];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 12 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat3x4( &( value[0] ) );
}

template <>
glm::mat4x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[8];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 8 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat4x2( &( value[0] ) );
}

template <>
glm::mat4x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[12];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 12 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat4x3( &( value[0] ) );
}

template <>
glm::mat4x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLfloat value[16];
    gl( glGetnUniformfv, this->id, uniformID,
        static_cast< GLsizei >( 16 * sizeof( GLfloat ) ), &( value[0] ) );
    return glm::make_mat4x4( &( value[0] ) );
}

// double
template <>
glm::dvec2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[2];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 2 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_vec2( &( value[0] ) );
}

template <>
glm::dvec3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[3];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 3 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_vec3( &( value[0] ) );
}

template <>
glm::dvec4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[4];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_vec4( &( value[0] ) );
}

template <>
glm::dmat2x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[4];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat2x2( &( value[0] ) );
}

template <>
glm::dmat2x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[6];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 6 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat2x3( &( value[0] ) );
}

template <>
glm::dmat2x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[8];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 8 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat2x4( &( value[0] ) );
}

template <>
glm::dmat3x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[6];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 6 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat3x2( &( value[0] ) );
}

template <>
glm::dmat3x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[9];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 9 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat3x3( &( value[0] ) );
}

template <>
glm::dmat3x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[12];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 12 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat3x4( &( value[0] ) );
}

template <>
glm::dmat4x2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[8];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 8 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat4x2( &( value[0] ) );
}

template <>
glm::dmat4x3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[12];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 12 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat4x3( &( value[0] ) );
}

template <>
glm::dmat4x4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLdouble value[16];
    gl( glGetnUniformdv, this->id, uniformID,
        static_cast< GLsizei >( 16 * sizeof( GLdouble ) ), &( value[0] ) );
    return glm::make_mat4x4( &( value[0] ) );
}

// int
template <>
glm::ivec2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLint value[2];
    gl( glGetnUniformiv, this->id, uniformID,
        static_cast< GLsizei >( 2 * sizeof( GLint ) ), &( value[0] ) );
    return glm::make_vec2( &( value[0] ) );
}

template <>
glm::ivec3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLint value[3];
    gl( glGetnUniformiv, this->id, uniformID,
        static_cast< GLsizei >( 3 * sizeof( GLint ) ), &( value[0] ) );
    return glm::make_vec3( &( value[0] ) );
}

template <>
glm::ivec4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLint value[4];
    gl( glGetnUniformiv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLint ) ), &( value[0] ) );
    return glm::make_vec4( &( value[0] ) );
}

template <>
glm::uvec2
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLuint value[2];
    gl( glGetnUniformuiv, this->id, uniformID,
        static_cast< GLsizei >( 2 * sizeof( GLuint ) ), &( value[0] ) );
    return glm::make_vec2( &( value[0] ) );
}

template <>
glm::uvec3
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLuint value[3];
    gl( glGetnUniformuiv, this->id, uniformID,
        static_cast< GLsizei >( 3 * sizeof( GLuint ) ), &( value[0] ) );
    return glm::make_vec3( &( value[0] ) );
}

template <>
glm::uvec4
elShaderProgram::GetUniform( const GLint uniformID ) const noexcept
{
    GLuint value[4];
    gl( glGetnUniformuiv, this->id, uniformID,
        static_cast< GLsizei >( 4 * sizeof( GLuint ) ), &( value[0] ) );
    return glm::make_vec4( &( value[0] ) );
}
} // namespace OpenGL
} // namespace el3D
