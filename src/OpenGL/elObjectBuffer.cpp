#include "OpenGL/elObjectBuffer.hpp"

namespace el3D
{
namespace OpenGL
{
elObjectBuffer::elObjectBuffer( const uint64_t dimension,
                                const uint64_t numberOfBuffers,
                                const uint64_t elementBufferNumber ) noexcept
    : vao( 1 ), vbo( numberOfBuffers ), dimension( dimension )
{
    if ( numberOfBuffers != 0 )
        this->vbo.SetBufferTarget( GL_ELEMENT_ARRAY_BUFFER,
                                   elementBufferNumber );
}

uint64_t
elObjectBuffer::GetNumberOfElements() const noexcept
{
    return this->numberOfElements;
}

uint64_t
elObjectBuffer::GetNumberOfVertices() const noexcept
{
    return this->numberOfVertices;
}

void
elObjectBuffer::SetNumberOfElements( const uint64_t v ) noexcept
{
    this->numberOfElements = v;
}

void
elObjectBuffer::Bind() const noexcept
{
    this->vao.Bind();
}

void
elObjectBuffer::Enable(
    const std::vector< size_t > &             buffer,
    const elShaderProgram::shaderAttribute_t &attribute ) const noexcept
{
    if ( !attribute ) return;

    for ( auto b : buffer )
        this->vbo.Bind( b );
    this->vao.EnableVertexAttribArray( attribute );
}

void
elObjectBuffer::Disable( const std::vector< elShaderProgram::shaderAttribute_t >
                             &attributes ) const noexcept
{
    for ( auto a : attributes )
        if ( a ) this->vao.DisableVertexAttribArray( a );
}

elBufferObject &
elObjectBuffer::GetBuffer() noexcept
{
    return this->vbo;
}

const elBufferObject &
elObjectBuffer::GetBuffer() const noexcept
{
    return this->vbo;
}

void
elObjectBuffer::InitialUpdateBuffer( const size_t ) noexcept
{
}

void
elObjectBuffer::UpdateVertexBuffer(
    const std::vector< GLfloat > &vertices ) noexcept
{
    uint64_t vertexVectorSize = vertices.size();
    if ( vertexVectorSize % this->dimension != 0 )
    {
        LOG_WARN( 0 ) << "insufficient vertex data (" << vertexVectorSize
                      << ") for dimension " << this->dimension
                      << ". Truncating data, object will be incomplete!";

        std::vector< float > truncatedVertices = vertices;
        truncatedVertices.resize( vertexVectorSize -
                                  vertexVectorSize % this->dimension );
        vbo.BufferData( static_cast< GLsizeiptr >( truncatedVertices.size() *
                                                   sizeof( GLfloat ) ),
                        truncatedVertices.data(), 0 );
        this->numberOfVertices = truncatedVertices.size();
    }
    else
    {
        vbo.BufferData(
            static_cast< GLsizeiptr >( vertices.size() * sizeof( GLfloat ) ),
            vertices.data(), 0 );
        this->numberOfVertices = vertexVectorSize / 3;
    }
}

} // namespace OpenGL
} // namespace el3D
