#include "OpenGL/elGeometricObject_InstancedObject.hpp"

#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elObjectBuffer.hpp"
#include "OpenGL/elShaderProgram.hpp"


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
const std::vector< elShaderProgram::shaderAttribute_t >
    elGeometricObject_InstancedObject::ATTRIBUTES = {
        { "coord", 3 },
        { "", 0, 0, 0, GL_UNSIGNED_INT },
        { "texCoord", 2 },
        { "normal", 3 },
        { "tangent", 3 },
        { "bitangent", 3 },
        { "modelMatrix", 4, 4, 1 } };


elGeometricObject_InstancedObject::elGeometricObject_InstancedObject(
    const DrawMode drawMode, const objectGeometry_t &objectGeometry )
    : elGeometricObject_base(
          drawMode, 3, elGeometricObject_Type::InstancedObject,
          objectGeometry.vertices, objectGeometry.elements, 2U,
          objectGeometry.textureCoordinates, 3U, objectGeometry.normals, 3U,
          objectGeometry.tangents, 3U, objectGeometry.bitangents, 16U,
          std::vector< GLfloat >() )
{
}

void
elGeometricObject_InstancedObject::DrawImplementation(
    const uint64_t shaderID, const elCamera &camera ) const noexcept
{
    static_cast< void >( shaderID );

    this->CallPreDrawCallback();

    // uploading model matrices
    if ( this->hasUpdatedGeometry )
    {
        this->objectBuffer.GetBuffer().BufferData(
            static_cast< GLsizeiptr >( 4 * 4 * this->modelMatrixVector.size() *
                                       sizeof( float ) ),
            glm::value_ptr( this->modelMatrixVector.data()[0] ), BUFFER_MVP );
        this->hasUpdatedGeometry = false;
    }

    // activate and bind everything
    auto &shader = this->shaders[shaderID];
    shader.shader->Bind();
    if ( shader.uniforms[UNIFORM_VP].id != -1 )
        shader.shader->SetUniform( shader.uniforms[UNIFORM_VP].id,
                                   camera.GetMatrices().viewProjection );

    shader.BindTextures();

    this->objectBuffer.Bind();
    this->objectBuffer.Enable( { BUFFER_VERTEX, BUFFER_ELEMENT },
                               shader.attributes[BUFFER_VERTEX] );

    for ( size_t i = 0; i < BUFFER_END; ++i )
    {
        if ( i == BUFFER_VERTEX || i == BUFFER_ELEMENT ) continue;
        this->objectBuffer.Enable( { i }, shader.attributes[i] );
    }

    // draw
    OpenGL::gl(
        glDrawElementsInstanced, this->GetDrawModeForOpenGL(),
        static_cast< GLsizei >( this->objectBuffer.GetNumberOfElements() ),
        static_cast< GLenum >( GL_UNSIGNED_INT ),
        static_cast< GLvoid * >( nullptr ),
        static_cast< int >( this->numberOfInstances ) );

    this->objectBuffer.Disable( shader.attributes );

    this->CallPostDrawCallback();
}

bb::elExpected< size_t, elShaderProgram_Error >
elGeometricObject_InstancedObject::AttachShader(
    const uint64_t shaderId ) noexcept
{
    auto &shader = this->shaders[shaderId];
    shader.RegisterAttributes( ATTRIBUTES );
    if ( !shader.attributes[BUFFER_VERTEX] )
    {
        LOG_ERROR( 0 ) << "the shader attribute "
                       << ATTRIBUTES[BUFFER_VERTEX].identifier
                       << " is required for the instanced object but not "
                          "present in the shader "
                       << shader.shader->GetFileAndGroupName();
        this->RemoveShader( shaderId );
        return bb::Error(
            elShaderProgram_Error::UnableToFindAttributeLocation );
    }

    shader.RegisterUniforms( { VP_ATTRIBUTE_NAME } );

    return bb::Success( shaderId );
}

void
elGeometricObject_InstancedObject::UpdateGeometry(
    const std::vector< instanceGeometry_t > &geometry ) noexcept
{
    this->numberOfInstances = geometry.size();
    this->modelMatrixVector.resize( this->numberOfInstances );
    this->modelMatrix.resize( this->numberOfInstances );

    for ( uint64_t i = 0; i < this->numberOfInstances; ++i )
    {
        this->modelMatrix[i].SetPosition( geometry[i].position );
        this->modelMatrix[i].SetScaling( geometry[i].scaling );
        this->modelMatrix[i].SetRotation( geometry[i].rotation );
        this->modelMatrixVector[i] = this->modelMatrix[i].GetModelMatrix();
    }

    this->hasUpdatedGeometry = true;
}

} // namespace OpenGL
} // namespace el3D
