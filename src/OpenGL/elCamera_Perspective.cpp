#include "OpenGL/elCamera_Perspective.hpp"

#include "logging/elLog.hpp"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace el3D
{
namespace OpenGL
{
elCamera_Perspective::elCamera_Perspective() noexcept
    : elCamera( elCameraType::Perspective )
{
}

void
elCamera_Perspective::SetState( const state_t& v ) noexcept
{
    this->state = v;
}


const elCamera_Perspective::state_t&
elCamera_Perspective::GetState() const noexcept
{
    return this->state;
}

glm::vec2
elCamera_Perspective::GetViewPortSize() const noexcept
{
    return this->viewPortSize;
}

void
elCamera_Perspective::SetViewPortSize( const glm::vec2& size ) noexcept
{
    this->viewPortSize = size;
    this->hasUpdate    = true;
}

void
elCamera_Perspective::SetLookAt( const glm::vec3& value ) noexcept
{
    this->state.lookAt = value;
    this->hasUpdate    = true;
}

void
elCamera_Perspective::SetPosition( const glm::vec3& value ) noexcept
{
    this->state.position = value;
    this->hasUpdate      = true;
}

void
elCamera_Perspective::SetUp( const glm::vec3& value ) noexcept
{
    this->state.up  = value;
    this->hasUpdate = true;
}

void
elCamera_Perspective::SetFov( const units::Angle value ) noexcept
{
    this->state.fov = value;
    this->hasUpdate = true;
}

void
elCamera_Perspective::Update() const noexcept
{
    if ( this->stateLoader ) this->state = this->stateLoader();
    this->GenerateMatrices();
}

void
elCamera_Perspective::GenerateMatrices() const noexcept
{
    this->matrix.projection = glm::perspective(
        static_cast< float >( this->state.fov.GetDegree() ),
        this->viewPortSize.x / this->viewPortSize.y,
        static_cast< float >( this->zFactor.zNear.GetMeter() ),
        static_cast< float >( this->zFactor.zFar.GetMeter() ) );
    this->matrix.view = glm::lookAt( this->state.position,
                                     this->state.position + this->state.lookAt,
                                     this->state.up );

    this->matrix.viewProjection = this->matrix.projection * this->matrix.view;
}

elCamera_Perspective::stateLoaderGuard_t
elCamera_Perspective::SetStateLoader( const stateLoader_t& loader ) noexcept
{
    if ( !this->stateLoader )
    {
        this->stateLoader         = loader;
        this->hasContinuousUpdate = true;

        return utils::elGenericRAII( [this] { this->ResetStateLoader(); } );
    }

    LOG_ERROR( 0 ) << "state loader already attached at camera";
    return utils::elGenericRAII();
}

bool
elCamera_Perspective::HasStateLoader() const noexcept
{
    return static_cast< bool >( !this->stateLoader );
}


void
elCamera_Perspective::ResetStateLoader()
{
    this->stateLoader         = stateLoader_t();
    this->hasContinuousUpdate = false;
}


} // namespace OpenGL
} // namespace el3D

