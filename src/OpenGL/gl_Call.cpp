#include "OpenGL/gl_Call.hpp"

#include "GLAPI/elWindow.hpp"
#include "logging/elLog.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
namespace internal
{

std::string LastGLCall::file;
int         LastGLCall::line;
std::string LastGLCall::func;
std::string LastGLCall::glFunc;


template < typename T >
struct glGetCall;
template < typename T >
struct glGetIndexCall;

template <>
struct glGetCall< GLboolean >
{
    static GLboolean
    Do( const GLenum pname )
    {
        GLboolean data;
        gl( glGetBooleanv, pname, &data );
        return data;
    }
};

template <>
struct glGetCall< GLdouble >
{
    static GLdouble
    Do( const GLenum pname )
    {
        GLdouble data;
        gl( glGetDoublev, pname, &data );
        return data;
    }
};

template <>
struct glGetCall< GLfloat >
{
    static GLfloat
    Do( const GLenum pname )
    {
        GLfloat data;
        gl( glGetFloatv, pname, &data );
        return data;
    }
};

template <>
struct glGetCall< GLint >
{
    static GLint
    Do( const GLenum pname )
    {
        GLint data;
        gl( glGetIntegerv, pname, &data );
        return data;
    }
};

template <>
struct glGetCall< GLint64 >
{
    static GLint64
    Do( const GLenum pname )
    {
        GLint64 data;
        gl( glGetInteger64v, pname, &data );
        return data;
    }
};

template <>
struct glGetIndexCall< GLboolean >
{
    static GLboolean
    Do( const GLenum pname, const GLuint index )
    {
        GLboolean data;
        gl( glGetBooleani_v, pname, index, &data );
        return data;
    }
};

template <>
struct glGetIndexCall< GLdouble >
{
    static GLdouble
    Do( const GLenum pname, const GLuint index )
    {
        GLdouble data;
        gl( glGetDoublei_v, pname, index, &data );
        return data;
    }
};

template <>
struct glGetIndexCall< GLfloat >
{
    static GLfloat
    Do( const GLenum pname, const GLuint index )
    {
        GLfloat data;
        gl( glGetFloati_v, pname, index, &data );
        return data;
    }
};

template <>
struct glGetIndexCall< GLint >
{
    static GLint
    Do( const GLenum pname, const GLuint index )
    {
        GLint data;
        gl( glGetIntegeri_v, pname, index, &data );
        return data;
    }
};

template <>
struct glGetIndexCall< GLint64 >
{
    static GLint64
    Do( const GLenum pname, const GLuint index )
    {
        GLint64 data;
        gl( glGetInteger64i_v, pname, index, &data );
        return data;
    }
};

void
Call( const char *file, const int line, const char *func, const char *glFunc )
{
    GLenum error = glGetError();
    if ( error != GL_NO_ERROR )
    {
        logging::elLog::Log( logging::ERROR, file, line, func, 0 )
            << "call of function '" << glFunc
            << "' caused the following error : " << EnumToString( error );
        throw std::runtime_error( "OpenGL error occurred" );
    }
}
} // namespace internal


std::string
EnumToString( GLenum value )
{
    switch ( value )
    {
        case GL_RGBA:
            return "GL_RGBA";
        // glGetError
        case GL_NO_ERROR:
            return "GL_NO_ERROR";
        case GL_INVALID_ENUM:
            return "GL_INVALID_ENUM";
        case GL_INVALID_VALUE:
            return "GL_INVALID_VALUE";
        case GL_INVALID_OPERATION:
            return "GL_INVALID_OPERATION";
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            return "GL_INVALID_FRAMEBUFFER_OPERATION";
        case GL_OUT_OF_MEMORY:
            return "GL_OUT_OF_MEMORY";
        case GL_STACK_UNDERFLOW:
            return "GL_STACK_UNDERFLOW";
        case GL_STACK_OVERFLOW:
            return "GL_STACK_OVERFLOW";
        // glDebugMessageCallback
        case GL_DEBUG_TYPE_ERROR:
            return "GL_DEBUG_TYPE_ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PORTABILITY:
            return "GL_DEBUG_TYPE_PORTABILITY";
        case GL_DEBUG_TYPE_PERFORMANCE:
            return "GL_DEBUG_TYPE_PERFORMANCE";
        case GL_DEBUG_TYPE_MARKER:
            return "GL_DEBUG_TYPE_MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:
            return "GL_DEBUG_TYPE_PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:
            return "GL_DEBUG_TYPE_POP_GROUP";
        case GL_DEBUG_TYPE_OTHER:
            return "GL_DEBUG_TYPE_OTHER";
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            return "GL_DEBUG_SEVERITY_NOTIFICATION";
        case GL_DEBUG_SEVERITY_LOW:
            return "GL_DEBUG_SEVERITY_LOW";
        case GL_DEBUG_SEVERITY_MEDIUM:
            return "GL_DEBUG_SEVERITY_MEDIUM";
        case GL_DEBUG_SEVERITY_HIGH:
            return "GL_DEBUG_SEVERITY_HIGH";
        case GL_DEBUG_SOURCE_API:
            return "GL_DEBUG_SOURCE_API";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            return "GL_DEBUG_SOURCE_SHADER_COMPILER";
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            return "GL_DEBUG_SOURCE_THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:
            return "GL_DEBUG_SOURCE_APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:
            return "GL_SOURCE_OTHER";
        // glShader
        case GL_FRAGMENT_SHADER:
            return "GL_FRAGMENT_SHADER";
        case GL_VERTEX_SHADER:
            return "GL_VERTEX_SHADER";
        case GL_GEOMETRY_SHADER:
            return "GL_GEOMETRY_SHADER";
        case GL_TESS_CONTROL_SHADER:
            return "GL_TESS_CONTROL_SHADER";
        case GL_TESS_EVALUATION_SHADER:
            return "GL_TESS_EVALUATION_SHADER";
        case GL_COMPUTE_SHADER:
            return "GL_COMPUTE_SHADER";
        // GL_TEXTURE
        case GL_TEXTURE_1D:
            return "GL_TEXTURE_1D";
        case GL_TEXTURE_2D:
            return "GL_TEXTURE_2D";
        case GL_TEXTURE_3D:
            return "GL_TEXTURE_3D";
        case GL_TEXTURE_1D_ARRAY:
            return "GL_TEXTURE_1D_ARRAY";
        case GL_TEXTURE_2D_ARRAY:
            return "GL_TEXTURE_2D_ARRAY";
        case GL_TEXTURE_CUBE_MAP:
            return "GL_TEXTURE_CUBE_MAP";
        case GL_TEXTURE_CUBE_MAP_ARRAY:
            return "GL_TEXTURE_CUBE_MAP_ARRAY";
        case GL_TEXTURE_BUFFER:
            return "GL_TEXTURE_BUFFER";
        case GL_TEXTURE0:
            return "GL_TEXTURE0";
        case GL_TEXTURE1:
            return "GL_TEXTURE1";
        case GL_TEXTURE2:
            return "GL_TEXTURE2";
        case GL_TEXTURE3:
            return "GL_TEXTURE3";
        case GL_TEXTURE4:
            return "GL_TEXTURE4";
        case GL_TEXTURE5:
            return "GL_TEXTURE5";
        case GL_TEXTURE6:
            return "GL_TEXTURE6";
        case GL_TEXTURE7:
            return "GL_TEXTURE7";
        case GL_TEXTURE8:
            return "GL_TEXTURE8";
        case GL_TEXTURE9:
            return "GL_TEXTURE9";
        case GL_TEXTURE10:
            return "GL_TEXTURE10";
        case GL_TEXTURE11:
            return "GL_TEXTURE11";
        case GL_TEXTURE12:
            return "GL_TEXTURE12";
        case GL_TEXTURE13:
            return "GL_TEXTURE13";
        case GL_TEXTURE14:
            return "GL_TEXTURE14";
        case GL_TEXTURE15:
            return "GL_TEXTURE15";
        case GL_TEXTURE16:
            return "GL_TEXTURE16";
        case GL_TEXTURE17:
            return "GL_TEXTURE17";
        case GL_TEXTURE18:
            return "GL_TEXTURE18";
        case GL_TEXTURE19:
            return "GL_TEXTURE19";
        default:
            return "GL_UNKNOWN_ENUM";
    }
}

GLenum
ColorAttachmentFromOffset( const uint64_t offset ) noexcept
{
    static GLint maxColorAttachments = Get< GLint >( GL_MAX_COLOR_ATTACHMENTS );
    if ( static_cast< uint64_t >( maxColorAttachments ) <= offset )
    {
        LOG_ERROR( 0 )
            << "offset of " << offset
            << " exceeds the maximum number of color attachments which is "
            << maxColorAttachments;
    }

    switch ( offset )
    {
        case 0:
            return GL_COLOR_ATTACHMENT0;
        case 1:
            return GL_COLOR_ATTACHMENT1;
        case 2:
            return GL_COLOR_ATTACHMENT2;
        case 3:
            return GL_COLOR_ATTACHMENT3;
        case 4:
            return GL_COLOR_ATTACHMENT4;
        case 5:
            return GL_COLOR_ATTACHMENT5;
        case 6:
            return GL_COLOR_ATTACHMENT6;
        case 7:
            return GL_COLOR_ATTACHMENT7;
        case 8:
            return GL_COLOR_ATTACHMENT8;
        case 9:
            return GL_COLOR_ATTACHMENT9;
        case 10:
            return GL_COLOR_ATTACHMENT10;
        case 11:
            return GL_COLOR_ATTACHMENT11;
        case 12:
            return GL_COLOR_ATTACHMENT12;
        case 13:
            return GL_COLOR_ATTACHMENT13;
        default:
            LOG_ERROR( 0 ) << "The color attachment offset must be in the "
                              "range [0, 13]. Returning GL_COLOR_ATTACHMENT0";
    }
    return GL_COLOR_ATTACHMENT0;
}

void
DebugMessageCallback( GLenum source, GLenum type, GLuint id, GLenum severity,
                      GLsizei, const GLchar *message, const void * )
{
    logging::logLevel_t level;
    if ( severity == GL_DEBUG_SEVERITY_NOTIFICATION )
        level = logging::DEBUG;
    else if ( severity == GL_DEBUG_SEVERITY_LOW )
        level = logging::DEBUG;
    else if ( severity == GL_DEBUG_SEVERITY_MEDIUM )
        level = logging::WARN;
    else
        level = logging::ERROR;

    LOG( 0, level ) << "OpenGL: [ " << EnumToString( type ) << " ]("
                    << EnumToString( severity )
                    << "), trace = " << internal::LastGLCall::file << ":"
                    << internal::LastGLCall::line << ":"
                    << internal::LastGLCall::func << "{"
                    << internal::LastGLCall::glFunc << "}"
                    << ", source = " << EnumToString( source )
                    << ", id = " << id << ", " << message;
}

bool
IsEnabled( const GLenum cap ) noexcept
{
    bool isEnabled = gl( glIsEnabled, cap );
    return isEnabled;
}


void
Enable( const GLenum cap ) noexcept
{
    gl( glEnable, cap );
}

void
Disable( const GLenum cap ) noexcept
{
    gl( glDisable, cap );
}

template < typename T >
T
Get( const GLenum pname ) noexcept
{
    return internal::glGetCall< T >::Do( pname );
}

template < typename T >
T
GetIndex( const GLenum target, const GLuint index ) noexcept
{
    return internal::glGetIndexCall< T >::Do( target, index );
}

void
SetDepthTest( const DepthTest depthTest ) noexcept
{
    if ( depthTest == DepthTest::Default ) return;

    if ( depthTest == DepthTest::Enable )
        Enable( GL_DEPTH_TEST );
    else
        Disable( GL_DEPTH_TEST );
}

DepthTest
GetDepthTestState() noexcept
{
    return IsEnabled( GL_DEPTH_TEST ) ? DepthTest::Enable : DepthTest::Disable;
}

void
SetCullFace( const CullFace cullFace ) noexcept
{
    if ( cullFace == CullFace::Default ) return;
    if ( cullFace == CullFace::Disable )
    {
        Disable( GL_CULL_FACE );
        return;
    }

    Enable( GL_CULL_FACE );
    if ( cullFace == CullFace::Front )
        gl( glCullFace, static_cast< GLenum >( GL_FRONT ) );
    else if ( cullFace == CullFace::Back )
        gl( glCullFace, static_cast< GLenum >( GL_BACK ) );
    else if ( cullFace == CullFace::FrontAndBack )
        gl( glCullFace, static_cast< GLenum >( GL_FRONT_AND_BACK ) );
}

CullFace
GetCullFaceState() noexcept
{
    if ( !IsEnabled( GL_CULL_FACE ) ) return CullFace::Disable;

    auto mode = static_cast< GLenum >( Get< GLint >( GL_CULL_FACE_MODE ) );
    if ( mode == GL_BACK )
        return CullFace::Back;
    else if ( mode == GL_FRONT )
        return CullFace::Front;
    else if ( mode == GL_FRONT_AND_BACK )
        return CullFace::FrontAndBack;

    LOG_ERROR( 0 ) << "unknown cull face mode returned";
    return CullFace::Default;
}


template GLboolean
Get< GLboolean >( const GLenum pname ) noexcept;
template GLdouble
Get< GLdouble >( const GLenum pname ) noexcept;
template GLfloat
Get< GLfloat >( const GLenum pname ) noexcept;
template GLint
Get< GLint >( const GLenum pname ) noexcept;
template GLint64
Get< GLint64 >( const GLenum pname ) noexcept;
template GLboolean
GetIndex< GLboolean >( const GLenum pname, const GLuint index ) noexcept;
template GLdouble
GetIndex< GLdouble >( const GLenum pname, const GLuint index ) noexcept;
template GLfloat
GetIndex< GLfloat >( const GLenum pname, const GLuint index ) noexcept;
template GLint
GetIndex< GLint >( const GLenum pname, const GLuint index ) noexcept;
template GLint64
GetIndex< GLint64 >( const GLenum pname, const GLuint index ) noexcept;


} // namespace OpenGL
} // namespace el3D
