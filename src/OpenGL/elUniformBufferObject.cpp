#include "OpenGL/elUniformBufferObject.hpp"

#include "OpenGL/gl_Call.hpp"


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace OpenGL
{
elUniformBufferObject::elUniformBufferObject(
    const std::string &name, const std::vector< std::string > &varNames,
    const elShaderProgram *shader )
    : elBufferObject( 1, GL_UNIFORM_BUFFER, GL_DYNAMIC_DRAW ), name( name ),
      bufferIndex( 0 )
{
    auto addShaderResult = this->AddShader( shader );
    if ( addShaderResult.HasError() )
    {
        this->create_isInitialized = false;
        this->create_error         = addShaderResult.GetError();
        return;
    }

    this->size = shader->GetActiveUniformBlockiv( this->indices[0],
                                                  GL_UNIFORM_BLOCK_DATA_SIZE );
    this->data.resize( static_cast< size_t >( this->size ) );

    std::vector< GLuint > varIndices;
    if ( shader->GetUniformIndices( varNames )
             .AndThen( [&]( const auto &r ) { varIndices = r; } )
             .HasError() )
    {
        std::string varNamesString;
        for ( auto &v : varNames )
            varNamesString += v + ", ";
        if ( !varNamesString.empty() )
            varNamesString.resize( varNamesString.size() - 2 );

        LOG_ERROR( 0 ) << "unable to find uniform index for one of: "
                       << varNamesString;
        this->create_isInitialized = false;
        this->create_error =
            elUniformBufferObject_Error::UnableToFindUniformIndex;
        return;
    }

    this->varOffsets =
        shader->GetActiveUniformsiv( varIndices, GL_UNIFORM_OFFSET );

    std::vector< GLint > vecStride =
        shader->GetActiveUniformsiv( varIndices, GL_UNIFORM_ARRAY_STRIDE );
    std::vector< GLint > matStride =
        shader->GetActiveUniformsiv( varIndices, GL_UNIFORM_ARRAY_STRIDE );

    this->varStride.resize( vecStride.size() );
    for ( size_t k = 0, l = vecStride.size(); k < l; ++k )
        this->varStride[k] = std::max( vecStride[k], matStride[k] );

    this->create_isInitialized = true;
}

elUniformBufferObject::~elUniformBufferObject()
{
}

void
elUniformBufferObject::BindToShader( const size_t shaderId,
                                     const size_t n ) const noexcept
{
    gl( glBindBufferBase, this->bufferTarget[n], this->indices[shaderId],
        this->bufferIDs[n] );
}

void
elUniformBufferObject::UnbindFromShader( const size_t shaderId,
                                         const size_t n ) const noexcept
{
    gl( glBindBufferBase, this->bufferTarget[n], this->indices[shaderId], 0u );
}

void
elUniformBufferObject::SetVariableData( const size_t arrayIndex,
                                        const size_t varIndex,
                                        const void * varData,
                                        const size_t dataSize )
{
    size_t stride = ( this->varStride[varIndex] == 0 )
                        ? dataSize
                        : static_cast< size_t >( this->varStride[varIndex] );

    memcpy( this->data.data() + arrayIndex * stride +
                this->varOffsets[varIndex],
            varData, dataSize );

    this->BufferData( this->size, this->data.data(), this->bufferIndex );
}

bb::elExpected< size_t, elUniformBufferObject_Error >
elUniformBufferObject::AddShader( const elShaderProgram *const shader ) noexcept
{
    auto result = shader->GetUniformBlockIndex( this->name );
    if ( result.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to find uniform block index \"" << this->name
                       << "\"";
        return bb::Error(
            elUniformBufferObject_Error::UnableToFindUniformBlockIndex );
    }

    gl( glUniformBlockBinding, shader->GetShaderID(), *result, *result );

    return bb::Success( this->indices.emplace_back( *result ).index() );
}

void
elUniformBufferObject::RemoveShader( const size_t shaderId ) noexcept
{
    this->indices.erase( shaderId );
}


} // namespace OpenGL
} // namespace el3D
