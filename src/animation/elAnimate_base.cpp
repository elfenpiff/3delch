#include "animation/elAnimate_base.hpp"

namespace el3D
{
namespace animation
{
getTimeCallback_t elAnimate_base::globalGetTimeCallback;

elAnimate_base::elAnimate_base() noexcept
    : getTimeCallback{ ( globalGetTimeCallback ) ? globalGetTimeCallback : [] {
          return units::Time::TimeSinceEpoch();
      } }
{
}

elAnimate_base&
elAnimate_base::SetOnUpdateCallback( const onUpdateCallback_t& v ) noexcept
{
    this->onUpdateCallback = v;
    return *this;
}

elAnimate_base&
elAnimate_base::SetOnArrivalCallback( const onArrivalCallback_t& v ) noexcept
{
    this->onArrivalCallback = v;
    return *this;
}

elAnimate_base&
elAnimate_base::SetCustomGetTimeCallback( const getTimeCallback_t& v ) noexcept
{
    if ( v ) this->getTimeCallback = v;
    return *this;
}

bool
elAnimate_base::HasToPerformUpdate() const noexcept
{
    return this->hasToPerformUpdate;
}

void
elAnimate_base::Update() noexcept
{
    if ( !this->previousUpdateTime )
    {
        this->previousUpdateTime.emplace( this->getTimeCallback() );
        this->currentTime = *this->previousUpdateTime;
    }
    else
    {
        this->previousUpdateTime = this->currentTime;
        this->currentTime        = this->getTimeCallback();

        this->PerformUpdate();

        if ( this->onUpdateCallback ) this->onUpdateCallback();

        if ( !this->hasToPerformUpdate )
        {
            auto callback = this->onArrivalCallback;
            this->Reset();
            if ( callback ) callback();
        }
    }

    this->Set();
}

void
elAnimate_base::Reset() noexcept
{
    this->SetOnArrivalCallback( onArrivalCallback_t() );
    this->SetOnUpdateCallback( onUpdateCallback_t() );
    this->SetCustomGetTimeCallback( getTimeCallback_t() );
}

} // namespace animation
} // namespace el3D
