#include "animation/elAnimatable.hpp"

#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/elLog.hpp"

namespace el3D
{
namespace animation
{
utils::elCallbackVector* elAnimatableBase::globalCallbackVector = nullptr;

elAnimatableBase::elAnimatableBase( const elAnimatableBase& rhs ) noexcept
{
    *this = rhs;
}

elAnimatableBase::elAnimatableBase( elAnimatableBase&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elAnimatableBase::~elAnimatableBase()
{
    this->DeregisterUpdateAnimationCallback();
}

elAnimatableBase&
elAnimatableBase::operator=( const elAnimatableBase& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->RemoveAllAnimations();

        for ( auto& a : rhs.animations )
        {
            setCall_t* setCall = new setCall_t( *a.setCall );
            this->animations.emplace_back( animate_t{
                std::unique_ptr< elAnimate_base >(
                    a.animationFactory( a.animation.get(), setCall, this ) ),
                std::unique_ptr< setCall_t >( setCall ), a.animationFactory,
                a.destinationId } );
        }
        this->updateCallback = rhs.updateCallback;

        if ( rhs.callback ) this->RegisterUpdateAnimationCallback();
    }
    return *this;
}

elAnimatableBase&
elAnimatableBase::operator=( elAnimatableBase&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->RemoveAllAnimations();

        if ( rhs.callback ) this->RegisterUpdateAnimationCallback();

        this->animations     = std::move( rhs.animations );
        this->updateCallback = std::move( rhs.updateCallback );

        rhs.DeregisterUpdateAnimationCallback();
    }
    return *this;
}

void
elAnimatableBase::SetGlobalCallbackVectorForUpdateAnimation(
    utils::elCallbackVector& value ) noexcept
{
    elAnimatableBase::globalCallbackVector = &value;
}

void
elAnimatableBase::UpdateAnimations() noexcept
{
    std::vector< elAnimate_base* > animationsToRemove;
    for ( auto& anim : this->animations )
    {
        anim.animation->Update();

        if ( !anim.animation->HasToPerformUpdate() )
            animationsToRemove.emplace_back( anim.animation.get() );
    }

    for ( auto ptr : animationsToRemove )
        this->RemoveAnimation( ptr );

    if ( this->updateCallback ) this->updateCallback();
}

void
elAnimatableBase::RemoveAnimation( elAnimate_base* const animationPtr ) noexcept
{
    auto iter = bb::find_if( this->animations, [=]( auto& e )
                             { return e.animation.get() == animationPtr; } );
    if ( iter != this->animations.end() ) this->animations.erase( iter );

    if ( this->animations.empty() ) this->DeregisterUpdateAnimationCallback();
}

void
elAnimatableBase::RemoveAllAnimations() noexcept
{
    this->animations.clear();
    this->DeregisterUpdateAnimationCallback();
}

void
elAnimatableBase::DeregisterUpdateAnimationCallback() noexcept
{
    this->callback.Reset();
}

void
elAnimatableBase::RegisterUpdateAnimationCallback() noexcept
{
    if ( !this->callback )
    {
        if ( elAnimatableBase::globalCallbackVector != nullptr )
            this->callback =
                elAnimatableBase::globalCallbackVector->CreateCallback(
                    [this] { this->UpdateAnimations(); } );
        else
            LOG_ERROR( 0 ) << "Set a globalCallbackVector first with "
                              "elAnimatableBase::"
                              "SetGlobalCallbackVectorForUpdateAnimation "
                              "before creating animations";
    }
}

void
elAnimatableBase::AddAnimation( elAnimate_base* const     animationPtr,
                                setCall_t* const          setCall,
                                const animationFactory_t& animationFactory,
                                const id_t destinationId ) noexcept
{
    this->animations.emplace_back(
        animate_t{ std::unique_ptr< elAnimate_base >( animationPtr ),
                   std::unique_ptr< setCall_t >( setCall ), animationFactory,
                   destinationId } );

    this->RegisterUpdateAnimationCallback();
}

bool
elAnimatableBase::HasActiveAnimations() const noexcept
{
    return !this->animations.empty();
}

void
elAnimatableBase::SetUpdateCallback(
    const std::function< void() >& value ) noexcept
{
    this->updateCallback = value;
}


} // namespace animation
} // namespace el3D
