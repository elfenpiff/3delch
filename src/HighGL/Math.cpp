#include "HighGL/Math.hpp"

#include "buildingBlocks/algorithm_extended.hpp"
#include "buildingBlocks/elOctree.hpp"

#include <array>
#include <glm/glm.hpp>
#include <iostream>
#include <limits>
#include <map>
#include <set>


namespace el3D
{
namespace HighGL
{
namespace internal
{
struct vertex_t
{
    vertex_t( const glm::vec3 &vertex ) noexcept : vertex{ vertex }
    {
    }

    bool
    operator<( const vertex_t &rhs ) const noexcept
    {
        return ( ( this->vertex.x < rhs.vertex.x ) ||
                 ( this->vertex.x == rhs.vertex.x &&
                   this->vertex.y < rhs.vertex.y ) ||
                 ( this->vertex.x == rhs.vertex.x &&
                   this->vertex.y == rhs.vertex.y &&
                   this->vertex.z < rhs.vertex.z ) );
    }

    static constexpr float EPSILON = 0.001f;
    glm::vec3              vertex;
};

struct edge_t
{
    edge_t( const GLuint a, const GLuint b )
        : a{ std::min( a, b ) }, b{ std::max( a, b ) }
    {
    }

    bool
    operator<( const edge_t &rhs ) const noexcept
    {
        return ( this->a < rhs.a ) || ( this->a == rhs.a && this->b < rhs.b );
    }

    GLuint a;
    GLuint b;
};

std::map< vertex_t, GLuint >::const_iterator
Contains( const std::map< vertex_t, GLuint > &m, const vertex_t &v ) noexcept
{
    return m.find( v );
}
} // namespace internal

std::vector< GLuint >
EliminateCloseVerticesInElements( const std::vector< GLfloat > &vertices,
                                  const std::vector< GLuint > & elements,
                                  const GLfloat minimumDistance ) noexcept
{
    // merge same vertices and adjust elements
    bb::elOctree< GLuint > adjustedVertices( minimumDistance );
    std::vector< GLuint >  adjustedElements = elements;
    for ( uint64_t i = 0, verticesSize = vertices.size(); i < verticesSize;
          i += 3 )
    {
        glm::vec3 newVertex{ vertices[i], vertices[i + 1], vertices[i + 2] };

        auto neighbors =
            adjustedVertices.GetNeighbors( newVertex, minimumDistance );
        GLuint elementIndex = static_cast< GLuint >( i ) / 3;

        if ( neighbors.empty() )
            adjustedVertices.Insert( newVertex, elementIndex );
        else
            bb::replace( adjustedElements, elementIndex, neighbors[0].value );
    }

    return adjustedElements;
}

std::vector< GLuint >
GenerateAdjacencyList( const std::vector< GLuint > &elements ) noexcept
{
    uint64_t elementsSize = elements.size();

    // add edges to edgeMap
    std::map< internal::edge_t, std::vector< GLuint > > edgeMap;
    for ( uint64_t i = 0; i < elementsSize; i += 3 )
    {
        for ( uint64_t k = 0; k < 3; ++k )
        {
            internal::edge_t newEdge{ elements[i + k],
                                      elements[i + ( k + 1 ) % 3] };

            edgeMap[newEdge].emplace_back( elements[i + ( k + 2 ) % 3] );
        }
    }

    // generate adjacency elements
    std::vector< GLuint > adjacencyElements;
    for ( uint64_t i = 0; i < elementsSize; i += 3 )
    {
        for ( uint64_t k = 0; k < 3; ++k )
        {
            GLuint v1 = elements[i + k], v2 = elements[i + ( k + 1 ) % 3],
                   v3 = elements[i + ( k + 2 ) % 3];
            internal::edge_t edge{ v1, v2 };

            adjacencyElements.emplace_back( v1 );

            auto &adjacentElementVector = edgeMap[edge];
            bool  hasElementAdded       = false;
            for ( auto element : adjacentElementVector )
                if ( element != v3 )
                {
                    adjacencyElements.emplace_back( element );
                    hasElementAdded = true;
                    break;
                }

            if ( hasElementAdded == false )
                adjacencyElements.emplace_back( v3 );
        }
    }

    return adjacencyElements;
}


} // namespace HighGL
} // namespace el3D
