#include "GLAPI/elImage.hpp"
#include "HighGL/elTerrainGenerator.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace HighGL
{
elTerrainGenerator::elTerrainGenerator( const heightmap_t& heightmap,
                                        const settings_t&  settings ) noexcept
{
    /// vertices
    this->geometry.vertices.reserve( heightmap.x * heightmap.y * 3 );

    for ( uint64_t i = 0, x = 0, y = 0;
          i < heightmap.dimension * heightmap.x * heightmap.y;
          i += heightmap.dimension, ++x )
    {
        if ( i != 0 && i % ( heightmap.x * heightmap.dimension ) == 0 )
        {
            ++y;
            x = 0;
        }

        this->geometry.vertices.emplace_back( settings.widthScaling *
                                              static_cast< GLfloat >( y ) );
        this->geometry.vertices.emplace_back(
            settings.heightScaling *
            static_cast< GLfloat >( heightmap.data[i] ) );
        this->geometry.vertices.emplace_back( settings.widthScaling *
                                              static_cast< GLfloat >( x ) );
    }


    /// normals, tangents, bitangents
    this->geometry.normals.reserve( heightmap.x * heightmap.y * 3 );
    this->geometry.tangents.reserve( heightmap.x * heightmap.y * 3 );
    this->geometry.bitangents.reserve( heightmap.x * heightmap.y * 3 );

    for ( uint64_t x = 0, y = 0;; ++x )
    {
        if ( x == heightmap.x ) x = 0, ++y;
        if ( y == heightmap.y ) break;

        auto getVertexAt = [&, this, heightmap]( glm::uvec2 pos ) -> glm::vec3 {
            uint64_t n = 3 * ( heightmap.x * pos.y + pos.x );
            return glm::vec3( this->geometry.vertices[n],
                              this->geometry.vertices[n + 1],
                              this->geometry.vertices[n + 2] );
        };

        glm::uvec2 xPair{ ( x + settings.normalGridRange < heightmap.x )
                              ? x
                              : x - settings.normalGridRange,
                          ( x + settings.normalGridRange < heightmap.x )
                              ? x + settings.normalGridRange
                              : x };
        glm::uvec2 yPair{ ( y + settings.normalGridRange < heightmap.y )
                              ? y
                              : y - settings.normalGridRange,
                          ( y + settings.normalGridRange < heightmap.y )
                              ? y + settings.normalGridRange
                              : y };

        glm::vec3 averageXHeight{ 0.0f, 0.0f, 0.0f };
        for ( uint64_t n = xPair[0] + 1; n <= xPair[1]; ++n )
            averageXHeight += getVertexAt( { n, y } );
        averageXHeight /= ( xPair[1] - xPair[0] );

        glm::vec3 averageYHeight{ 0.0f, 0.0f, 0.0f };
        for ( uint64_t n = yPair[0] + 1; n <= yPair[1]; ++n )
            averageYHeight += getVertexAt( { x, n } );
        averageYHeight /= ( yPair[1] - yPair[0] );


        glm::vec3 tangent   = averageXHeight - getVertexAt( { xPair[0], y } );
        glm::vec3 bitangent = averageYHeight - getVertexAt( { x, yPair[0] } );

        glm::vec3 normal = glm::normalize( glm::cross( tangent, bitangent ) );

        this->geometry.normals.emplace_back( normal.y );
        this->geometry.normals.emplace_back( normal.z );
        this->geometry.normals.emplace_back( normal.x );

        this->geometry.tangents.emplace_back( tangent.y );
        this->geometry.tangents.emplace_back( tangent.z );
        this->geometry.tangents.emplace_back( tangent.x );

        this->geometry.bitangents.emplace_back( bitangent.y );
        this->geometry.bitangents.emplace_back( bitangent.z );
        this->geometry.bitangents.emplace_back( bitangent.x );
    }


    /// elements
    uint64_t numberOfElements = ( heightmap.y - 1 ) * heightmap.x - 1;
    this->geometry.elements.reserve( 6 * numberOfElements );

    for ( GLuint i = 0; i < numberOfElements; ++i )
    {
        if ( i != 0 && ( i + 1 ) % heightmap.x == 0 ) continue;

        this->geometry.elements.emplace_back( i );
        this->geometry.elements.emplace_back( heightmap.x + i );
        this->geometry.elements.emplace_back( i + 1 );

        this->geometry.elements.emplace_back( heightmap.x + i );
        this->geometry.elements.emplace_back( heightmap.x + i + 1 );
        this->geometry.elements.emplace_back( i + 1 );
    }


    /// texture coordinates
    this->geometry.textureCoordinates.reserve( heightmap.x * heightmap.y * 2 );
    for ( uint64_t x = 0, y = 0;; ++x )
    {
        if ( x == heightmap.x ) x = 0, ++y;
        if ( y == heightmap.y ) break;

        this->geometry.textureCoordinates.emplace_back(
            static_cast< GLfloat >( x ) /
            static_cast< GLfloat >( heightmap.x ) );
        this->geometry.textureCoordinates.emplace_back(
            static_cast< GLfloat >( y ) /
            static_cast< GLfloat >( heightmap.y ) );
    }
}

const elTerrainGenerator::geometry_t&
elTerrainGenerator::GetGeometry() const noexcept
{
    return this->geometry;
}

} // namespace HighGL
} // namespace el3D
