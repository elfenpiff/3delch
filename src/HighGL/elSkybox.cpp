#include "HighGL/elSkybox.hpp"

#include "GLAPI/elImage.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elTexture_CubeMap.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

namespace el3D
{
namespace HighGL
{
elSkybox::elSkybox( vertexObject_t&&                    vertexObject,
                    uint64_t* const                     vertexShaderId,
                    const std::function< glm::vec3() >& positionGetter,
                    const std::string& shaderFile, const int shaderVersion,
                    const std::string& shaderGroup ) noexcept
    : positionGetter( positionGetter ),
      vertexObject( std::move( vertexObject ) ), shaderId( vertexShaderId )
{
    if ( !this->InitVertexObject( shaderFile, shaderVersion, shaderGroup ) )
        return;
}

elSkybox::elSkybox( vertexObject_t&&                    vertexObject,
                    uint64_t* const                     vertexShaderId,
                    const std::function< glm::vec3() >& positionGetter,
                    const std::string& shaderFile, const int shaderVersion,
                    const std::string& shaderGroup,
                    const glm::vec4&   color ) noexcept
    : elSkybox( std::move( vertexObject ), vertexShaderId, positionGetter,
                shaderFile, shaderVersion, shaderGroup )
{
    this->SetColor( color );
}

elSkybox::elSkybox( vertexObject_t&&                    vertexObject,
                    uint64_t* const                     vertexShaderId,
                    const std::function< glm::vec3() >& positionGetter,
                    const std::string& shaderFile, const int shaderVersion,
                    const std::string&     shaderGroup,
                    const textureConfig_t& textures ) noexcept
    : elSkybox( std::move( vertexObject ), vertexShaderId, positionGetter,
                shaderFile, shaderVersion, shaderGroup )
{
    std::vector< GLAPI::elImage::result_t > images;
    images.reserve( 6 );
    auto openImage = [&]( size_t k, const std::string& file )
    {
        if ( this->create_isInitialized )
        {
            images.emplace_back( GLAPI::elImage::Create(
                GLAPI::FromFilePath, file, SDL_PIXELFORMAT_RGBA32 ) );
            if ( images[k].HasError() )
            {
                this->create_isInitialized = false;
                this->create_error = elSkybox_Error::UnableToOpenTextureFile;
                LOG_ERROR( 0 ) << "unable to open skybox texture file " << file;
            }
        }
    };

    openImage( 0, textures.positiveXFile );
    openImage( 1, textures.negativeXFile );
    openImage( 2, textures.positiveYFile );
    openImage( 3, textures.negativeYFile );
    openImage( 4, textures.positiveZFile );
    openImage( 5, textures.negativeZFile );
    if ( !this->create_isInitialized ) return;

    uint64_t resizeTo = 1;
    for ( size_t k = 0; k < 6; ++k )
    {
        auto dim = images[k].GetValue()->GetDimensions();
        resizeTo = bb::max( resizeTo, dim.width, dim.height );
    }

    bb::for_each( images, [&]( auto& img )
                  { img.GetValue()->Resize( resizeTo, resizeTo ); } );

    this->InitCubeTexture( resizeTo, images[0].GetValue()->GetBytePointer(),
                           images[1].GetValue()->GetBytePointer(),
                           images[2].GetValue()->GetBytePointer(),
                           images[3].GetValue()->GetBytePointer(),
                           images[4].GetValue()->GetBytePointer(),
                           images[5].GetValue()->GetBytePointer() );
}

OpenGL::elTexture_CubeMap&
elSkybox::GetTexture() noexcept
{
    return *this->texture;
}


void
elSkybox::SetColor( const glm::vec4& color ) noexcept
{
    GLubyte texture[4] = { static_cast< GLubyte >( 255u * color.x ),
                           static_cast< GLubyte >( 255u * color.y ),
                           static_cast< GLubyte >( 255u * color.z ),
                           static_cast< GLubyte >( 255u * color.w ) };

    this->InitCubeTexture( 1, &( texture[0] ), &( texture[0] ), &( texture[0] ),
                           &( texture[0] ), &( texture[0] ), &( texture[0] ) );
}

void
elSkybox::InitCubeTexture( const uint64_t      size,
                           const GLvoid* const positiveXData,
                           const GLvoid* const negativeXData,
                           const GLvoid* const positiveYData,
                           const GLvoid* const negativeYData,
                           const GLvoid* const positiveZData,
                           const GLvoid* const negativeZData ) noexcept
{
    if ( !this->texture || this->texture->GetSize().x != size )
        this->texture =
            this->vertexObject->CreateTexture< OpenGL::elTexture_CubeMap >(
                *this->shaderId, "diffuse", GL_TEXTURE0, "", size, size );
    this->texture->TexImage( positiveXData, negativeXData, positiveYData,
                             negativeYData, positiveZData, negativeZData );
}

bool
elSkybox::InitVertexObject( const std::string& shaderFile,
                            const int          shaderVersion,
                            const std::string& shaderGroup ) noexcept
{
    this->create_isInitialized = true;
    OpenGL::Enable( GL_TEXTURE_CUBE_MAP_SEAMLESS );
    this->SetSize( this->size );

    auto shaderResult = OpenGL::elShaderProgram::Create(
        OpenGL::shaderFromFile, shaderFile, shaderGroup, shaderVersion );
    if ( shaderResult.HasError() )
    {
        this->create_isInitialized = false;
        this->create_error         = elSkybox_Error::UnableToOpenShaderFile;
        LOG_ERROR( 0 ) << "unable to create skybox shader program from "
                       << shaderFile;
        return this->create_isInitialized;
    }

    this->shader    = std::move( shaderResult.GetValue() );
    *this->shaderId = this->vertexObject->AddShader( this->shader.get() )
                          .OrElse(
                              []
                              {
                                  LOG_FATAL( 0 )
                                      << "unable to attach skybox shader";
                                  std::terminate();
                              } )
                          .GetValue();

    this->vertexObject->SetPreDrawCallback(
        [vertexObjectPtr = this->vertexObject.Get(),
         positionGetter  = positionGetter]
        { vertexObjectPtr->SetPosition( positionGetter() ); } );

    return this->create_isInitialized;
}

OpenGL::elGeometricObject_VertexObject&
elSkybox::GetVertexObject() noexcept
{
    return *this->vertexObject;
}

void
elSkybox::SetSize( const float size ) noexcept
{
    this->size = size;
    this->vertexObject->SetScaling( { size, size, size } );
}


} // namespace HighGL
} // namespace el3D
