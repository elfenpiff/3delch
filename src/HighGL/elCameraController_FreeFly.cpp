#include "HighGL/elCameraController_FreeFly.hpp"

#include <cmath>
#include <glm/gtx/rotate_vector.hpp>

namespace el3D
{
namespace HighGL
{

elCameraController_FreeFly::elCameraController_FreeFly(
    OpenGL::elCamera_Perspective&         camera,
    const std::function< units::Time() >& getDeltaTime,
    const settings_t&                     s ) noexcept
    : elCameraController_base( elCameraController::FreeFly, camera ),
      settings{ s }, getDeltaTime{ getDeltaTime }
{
    this->hasContinuousUpdates = true;
}

const elCameraController_FreeFly::settings_t&
elCameraController_FreeFly::GetSettings() const noexcept
{
    return this->settings;
}

void
elCameraController_FreeFly::SetSensitivity( const float v ) noexcept
{
    this->settings.sensitivity = v;
}

void
elCameraController_FreeFly::SetVelocity( const float v ) noexcept
{
    this->settings.velocity = v;
}

void
elCameraController_FreeFly::SetRotationDirection( const glm::vec3& v ) noexcept
{
    this->settings.rotationDirection = v;
}

void
elCameraController_FreeFly::SetRotationStep( const glm::vec3& v ) noexcept
{
    this->rotationStep = v;
}

void
elCameraController_FreeFly::SetMovementDirection( const glm::vec3& v ) noexcept
{
    this->settings.movementDirection = v;
}

void
elCameraController_FreeFly::CalculateState() noexcept
{
    float deltaTime = static_cast< float >( this->getDeltaTime().GetSeconds() );

    this->rotation +=
        ( this->settings.rotationDirection + this->rotationStep ) * deltaTime *
        this->settings.sensitivity;
    this->rotationStep = glm::vec3( 0.0f );

    this->state.position +=
        ( this->settings.movementDirection.x * this->right +
          this->settings.movementDirection.y * this->state.up +
          this->settings.movementDirection.z * this->state.lookAt ) *
        deltaTime * this->settings.velocity;

    this->state.lookAt = glm::normalize( glm::vec3{
        std::cos( this->rotation.y ) * std::cos( this->rotation.x ),
        std::sin( this->rotation.x ),
        std::sin( this->rotation.y ) * std::cos( this->rotation.x ) } );

    this->right =
        glm::normalize( glm::cross( this->state.lookAt, this->state.up ) );

    this->state.up  = glm::rotate( glm::vec3( 0.0f, 1.0f, 0.0f ),
                                   this->rotation.z, this->state.lookAt );
    this->state.fov = this->cameraState->fov;
}
} // namespace HighGL
} // namespace el3D
