#include "HighGL/elEventTexture.hpp"

namespace el3D
{
namespace HighGL
{
bb::product_ptr< elEventColor >
elEventTexture::CreateEventColor() noexcept
{
    uint32_t colorIndex = 0;
    if ( !this->freeColor.empty() )
    {
        colorIndex = this->freeColor.front();
        this->freeColor.pop();
    }
    else
    {
        colorIndex = this->currentColor++;
    }

    return this->factory.CreateProductWithOnRemoveCallback< elEventColor >(
        [this, colorIndex]( auto ) { this->FreeEventColor( colorIndex ); },
        colorIndex );
}

bb::product_ptr< elEventColorRange >
elEventTexture::CreateEventColorRange( const uint32_t size ) noexcept
{
    if ( size == 0 ) return bb::product_ptr< elEventColorRange >();

    bool    hasRangeFound = false;
    range_t newRange{ .start = 0, .size = size };

    for ( auto iter = this->freeRanges.begin(); iter != this->freeRanges.end();
          ++iter )
    {
        if ( iter->size == size )
        {
            newRange.start = iter->start;
            this->freeRanges.erase( iter );
            hasRangeFound = true;
            break;
        }
    }

    if ( !hasRangeFound )
    {
        newRange.start = this->currentColor;
        this->currentColor += size;
    }

    return this->factory.CreateProductWithOnRemoveCallback< elEventColorRange >(
        [this, newRange]( auto ) { this->FreeEventColorRange( newRange ); },
        newRange.start, newRange.size );
}

void
elEventTexture::FreeEventColor( const uint32_t v ) noexcept
{
    this->freeColor.push( v );
}

void
elEventTexture::FreeEventColorRange( const range_t &range ) noexcept
{
    this->freeRanges.emplace_back( range );
}

elEventColor
elEventTexture::GetCurrentEventColor() const noexcept
{
    return this->currentEventColor;
}

void
elEventTexture::SetCurrentEventColor( const glm::vec4 &e ) noexcept
{
    this->currentEventColor = elEventColor( e );
}
} // namespace HighGL
} // namespace el3D
