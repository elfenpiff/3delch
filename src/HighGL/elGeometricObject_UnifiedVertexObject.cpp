#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"

#include "HighGL/Math.hpp"
#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

namespace el3D
{
namespace HighGL
{
using namespace UnifiedVertexObject;

// IMPORTANT requires the same order as floatBufferList_t/uintBufferList_t
const std::vector< OpenGL::elShaderProgram::shaderAttribute_t >
    elGeometricObject_UnifiedVertexObject::ATTRIBUTES{
        { "coord", 3 },
        { "texCoord", 2 },
        { "normal", 3 },
        { "tangent", 3 },
        { "bitangent", 3 },
        { "diffuseColor", 4 },
        { "specularColor", 4 },
        { "shininessAndRoughness", 2 },
        { "emissionColor", 4 },
        { "eventColor", 4 },
        { "", 3, 1, 0, GL_UNSIGNED_INT },
        { "diffuseId", 1, 1, 0, GL_UNSIGNED_INT },
        { "specularColorId", 1, 1, 0, GL_UNSIGNED_INT },
        { "shininessAndRoughnessId", 1, 1, 0, GL_UNSIGNED_INT },
        { "normalId", 1, 1, 0, GL_UNSIGNED_INT },
        { "emissionId", 1, 1, 0, GL_UNSIGNED_INT },
        { "objectId", 1, 1, 0, GL_UNSIGNED_INT },
    };

elGeometricObject_UnifiedVertexObject::elGeometricObject_UnifiedVertexObject(
    const OpenGL::DrawMode drawMode, const uint64_t sizeX, const uint64_t sizeY,
    const uint64_t texArraySize ) noexcept
    : OpenGL::elGeometricObject_base(
          drawMode, 3, OpenGL::elGeometricObject_Type::UnifiedVertexObject,
          1ul + FB_END + UB_END,
          static_cast< uint64_t >( FB_END ) +
              static_cast< uint64_t >( ELEMENTS ) ),
      runOnce(
          { std::bind( &elGeometricObject_UnifiedVertexObject::Refresh, this ),
            std::bind(
                &elGeometricObject_UnifiedVertexObject::RefreshBufferData,
                this ),
            std::bind( &elGeometricObject_UnifiedVertexObject::
                           RefreshUniformBufferData,
                       this ) } ),
      tbo( 1, GL_RGBA32F ), textureArray( "", sizeX, sizeY, texArraySize )
{
    std::vector< uint8_t > greyTexture(
        static_cast< size_t >( sizeX * sizeY * 4 ), DEFAULT_TEXTURE_GREY );

    this->defaultTexture = this->CreateTexture(
        UnifiedVertexObject::DEFAULT_TEXTURE_TAG, greyTexture.data() );

    if ( !this->defaultTexture )
    {
        LOG_FATAL( 0 ) << "unable to store the default texture";
        std::terminate();
    }
}

elGeometricObject_UnifiedVertexObject::elGeometricObject_UnifiedVertexObject(
    const OpenGL::DrawMode drawMode, const OpenGL::elShaderProgram* shader,
    const uint64_t sizeX, const uint64_t sizeY,
    const uint64_t texArraySize ) noexcept
    : elGeometricObject_UnifiedVertexObject( drawMode, sizeX, sizeY,
                                             texArraySize )
{
    this->AddShader( shader );
}

void
elGeometricObject_UnifiedVertexObject::Refresh() noexcept
{
    this->runOnce.Call( _RefreshBufferData );
    this->runOnce.Call( _RefreshUniformBufferData );
}

void
elGeometricObject_UnifiedVertexObject::RefreshFloatBufferData() noexcept
{
    uintBuffer_t objectIDs;

    for ( size_t i = 0; i < FB_END; ++i )
    {
        floatBuffer_t bufferData;
        for ( auto& o : this->factory.GetProducts< Object >() )
        {
            bufferData.insert( bufferData.end(), o->floatBuffer[i].begin(),
                               o->floatBuffer[i].end() );

            if ( i == VERTEX )
            {
                size_t begin = objectIDs.size(),
                       end   = begin + o->floatBuffer[i].size() /
                                         static_cast< size_t >(
                                             ATTRIBUTES[VERTEX].size );
                objectIDs.resize( end );
                std::fill( objectIDs.begin() + static_cast< long >( begin ),
                           objectIDs.begin() + static_cast< long >( end ),
                           o->id );
            }
        }

        this->objectBuffer.GetBuffer().BufferData(
            static_cast< GLsizeiptr >( sizeof( GLfloat ) * bufferData.size() ),
            bufferData.data(), i );

        // upload objectID
        if ( i == VERTEX )
            this->objectBuffer.GetBuffer().BufferData(
                static_cast< GLsizeiptr >( sizeof( GLuint ) *
                                           objectIDs.size() ),
                objectIDs.data(),
                static_cast< int >( FB_END ) + static_cast< int >( UB_END ) );
    }
}

void
elGeometricObject_UnifiedVertexObject::RefreshUintBufferData() noexcept
{
    size_t vertexCounter = 0;
    for ( size_t i = 0; i < UB_END; ++i )
    {
        uintBuffer_t bufferData;
        for ( auto& o : this->factory.GetProducts< Object >() )
        {
            if ( i == ELEMENTS )
            {
                uint64_t bufferLength = 0u;
                if ( this->GetDrawMode() ==
                     OpenGL::DrawMode::TrianglesWithAdjacency )
                {
                    std::vector< GLuint > adjacencyList;
                    if ( o->vertexElimination ==
                         VertexElimination::CloseVertices )
                    {
                        auto adjustedElements =
                            EliminateCloseVerticesInElements(
                                o->floatBuffer[VERTEX], o->uintBuffer[ELEMENTS],
                                o->vertexEliminationMinDistance );
                        adjacencyList =
                            GenerateAdjacencyList( adjustedElements );
                    }
                    else
                        adjacencyList =
                            GenerateAdjacencyList( o->uintBuffer[ELEMENTS] );

                    bufferLength = adjacencyList.size();
                    bufferData.insert( bufferData.end(), adjacencyList.begin(),
                                       adjacencyList.end() );
                }
                else
                {
                    bufferLength = o->uintBuffer[i].size();
                    bufferData.insert( bufferData.end(),
                                       o->uintBuffer[i].begin(),
                                       o->uintBuffer[i].end() );
                }

                std::for_each(
                    bufferData.end() - static_cast< long >( bufferLength ),
                    bufferData.end(),
                    [&]( GLuint& k )
                    { k += static_cast< GLuint >( vertexCounter ); } );
                vertexCounter +=
                    o->floatBuffer[VERTEX].size() /
                    static_cast< size_t >( ATTRIBUTES[VERTEX].size );
            }
            else
            {
                bufferData.insert( bufferData.end(), o->uintBuffer[i].begin(),
                                   o->uintBuffer[i].end() );
            }
        }

        this->objectBuffer.GetBuffer().BufferData(
            static_cast< GLsizeiptr >( sizeof( GLuint ) * bufferData.size() ),
            bufferData.data(), FB_END + i );

        if ( i == ELEMENTS )
            this->objectBuffer.SetNumberOfElements( bufferData.size() );
    }
}

void
elGeometricObject_UnifiedVertexObject::RefreshBufferData() noexcept
{
    this->RefreshFloatBufferData();
    this->RefreshUintBufferData();
}

void
elGeometricObject_UnifiedVertexObject::RefreshUniformBufferData() noexcept
{
    this->tbo.BufferData(
        static_cast< GLsizeiptr >( sizeof( GLfloat ) *
                                   this->uniformBuffer.data.size() ),
        this->uniformBuffer.data.data(), 0 );
}

void
elGeometricObject_UnifiedVertexObject::Bind(
    const size_t shaderID ) const noexcept
{
    this->shaders[shaderID].shader->Bind();
    if ( this->shaders[shaderID].uniforms[UNIFORM_TEXTURE_ARRAY].id != -1 )
        this->textureArray.BindActivateAndSetUniform(
            this->shaders[shaderID].uniforms[UNIFORM_TEXTURE_ARRAY].id,
            GL_TEXTURE1 );

    this->objectBuffer.Bind();
    this->objectBuffer.Enable(
        { this->GetBufferID( VERTEX ), this->GetBufferID( ELEMENTS ) },
        this->shaders[shaderID].attributes[this->GetBufferID( VERTEX )] );

    for ( size_t k = 0, l = this->shaders[shaderID].attributes.size(); k < l;
          ++k )
    {
        auto& e = this->shaders[shaderID].attributes[k];
        if ( k == this->GetBufferID( VERTEX ) ||
             k == this->GetBufferID( ELEMENTS ) )
            continue;

        this->objectBuffer.Enable( { k }, e );
    }

    this->tbo.Bind( this->shaders[shaderID].uniforms[UNIFORM_BUFFER].id,
                    GL_TEXTURE0, 0 );
}

void
elGeometricObject_UnifiedVertexObject::Unbind(
    const size_t shaderID ) const noexcept
{
    this->objectBuffer.Disable( this->shaders[shaderID].attributes );
}

bb::elExpected< size_t, OpenGL::elShaderProgram_Error >
elGeometricObject_UnifiedVertexObject::AttachShader(
    const uint64_t shaderId ) noexcept
{
    auto& shader = this->shaders[shaderId];
    shader.RegisterUniforms( { this->uniformBuffer.name, "textureArray",
                               "cameraPosition", "viewProjection" } );

    if ( shader.uniforms[0].id == -1 )
    {
        LOG_ERROR( 0 ) << "unable to find required uniform "
                       << this->uniformBuffer.name << " in shader "
                       << shader.shader->GetFileAndGroupName();
        return bb::Error(
            OpenGL::elShaderProgram_Error::UnableToFindUniformLocation );
    }

    shader.RegisterAttributes( ATTRIBUTES );

    return bb::Success( shaderId );
}

void
elGeometricObject_UnifiedVertexObject::ResizeTextureArray(
    const uint64_t sizeX, const uint64_t sizeY,
    const uint64_t arraySize ) noexcept
{
    this->textureArray.SetSize( sizeX, sizeY, 0, arraySize );
}

uint64_t
elGeometricObject_UnifiedVertexObject::AddToTextureArray(
    const GLvoid* const pixels ) noexcept
{
    uint64_t id = this->textureIds.RequestID();
    if ( id >= this->GetTextureArraySize().arraySize )
    {
        LOG_ERROR( 0 )
            << "unable to add additional texture since the maximum size of "
            << this->GetTextureArraySize().arraySize
            << " of the texture array is reached!\n";
        this->textureIds.FreeID( id );
        return INVALID_TEXTURE_ID;
    }

    this->textureArray.TexImage( id, pixels );

    return id;
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
elGeometricObject_UnifiedVertexObject::CreateTexture(
    const GLvoid* const pixels ) noexcept
{
    uint64_t id = this->AddToTextureArray( pixels );
    if ( id == INVALID_TEXTURE_ID )
        return bb::shared_product_ptr< UnifiedVertexObject::Texture >();

    return this->factory.CreateSharedProductWithOnRemoveCallback<
        UnifiedVertexObject::Texture >(
        [this, id]( auto ) { this->textureIds.FreeID( id ); }, id );
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
elGeometricObject_UnifiedVertexObject::CreateTexture(
    const std::string& uniqueIdentifier, const GLvoid* pixels ) noexcept
{
    if ( uniqueIdentifier.empty() ) return this->CreateTexture( pixels );

    uint64_t id = 0u;

    auto iter = this->identifierToTexture.find( uniqueIdentifier );
    if ( iter != this->identifierToTexture.end() )
    {
        LOG_DEBUG( 0 ) << "updating texture '" << iter->first
                       << "' with new data";
        id = iter->second->GetId();
        this->textureArray.TexImage( id, pixels );
        return this->factory.AcquireSharedProduct( *iter->second );
    }
    else
    {
        id = this->AddToTextureArray( pixels );
        if ( id == INVALID_TEXTURE_ID )
            return bb::shared_product_ptr< UnifiedVertexObject::Texture >();
    }

    auto newTexture = this->factory.CreateSharedProductWithOnRemoveCallback<
        UnifiedVertexObject::Texture >(
        [this, id]( auto )
        {
            for ( auto iter = this->identifierToTexture.begin();
                  iter != this->identifierToTexture.end(); ++iter )
                if ( iter->second->GetId() == id )
                {
                    this->identifierToTexture.erase( iter );
                    break;
                }
            this->textureIds.FreeID( id );
        },
        id );

    this->identifierToTexture.insert(
        std::make_pair( uniqueIdentifier, newTexture.Get() ) );
    return newTexture;
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
elGeometricObject_UnifiedVertexObject::CreateTexture(
    const std::string&                  uniqueIdentifier,
    const std::vector< utils::byte_t >& pixels ) noexcept
{
    return this->CreateTexture( uniqueIdentifier, pixels.data() );
}

void
elGeometricObject_UnifiedVertexObject::RunOncePerFrame() const noexcept
{
    this->runOnce.Run();
}

size_t
elGeometricObject_UnifiedVertexObject::GetBufferID(
    const enum floatBufferList_t b ) const noexcept
{
    return b;
}

size_t
elGeometricObject_UnifiedVertexObject::GetBufferID(
    const enum uintBufferList_t b ) const noexcept
{
    return static_cast< size_t >( FB_END ) + static_cast< size_t >( b );
}

OpenGL::elTexture_base::texSize_t
elGeometricObject_UnifiedVertexObject::GetTextureArraySize() const noexcept
{
    return this->textureArray.GetSize();
}

void
elGeometricObject_UnifiedVertexObject::DrawImplementation(
    const uint64_t shaderID, const OpenGL::elCamera& camera ) const noexcept
{
    this->UpdateObjectModelMatrix();

    this->RunOncePerFrame();

    this->Bind( shaderID );

    auto& shader                = this->shaders[shaderID];
    auto  cameraPositionUniform = shader.uniforms[UNIFORM_CAMERA_POSITION].id;

    if ( cameraPositionUniform != -1 &&
         camera.GetType() == OpenGL::elCameraType::Perspective )
        shader.shader->SetUniform(
            cameraPositionUniform,
            dynamic_cast< const OpenGL::elCamera_Perspective& >( camera )
                .GetState()
                .position );

    auto vpUniform = shader.uniforms[UNIFORM_VIEW_PROJECTION].id;
    if ( vpUniform != -1 )
        shader.shader->SetUniform( vpUniform,
                                   camera.GetMatrices().viewProjection );

    OpenGL::gl(
        glDrawElements, this->GetDrawModeForOpenGL(),
        static_cast< GLsizei >( this->objectBuffer.GetNumberOfElements() ),
        static_cast< GLenum >( GL_UNSIGNED_INT ),
        static_cast< GLvoid* >( nullptr ) );
    this->Unbind( shaderID );
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
elGeometricObject_UnifiedVertexObject::GetDefaultTexture() const noexcept
{
    return this->defaultTexture;
}

void
elGeometricObject_UnifiedVertexObject::UpdateObjectModelMatrix() const noexcept
{
    bb::for_each( this->factory.GetProducts< Object >(),
                  [&]( auto& o ) { o->UpdateModelMatrix(); } );
}

namespace UnifiedVertexObject
{
//////////////
// Object //
//////////////
Object::Object( elGeometricObject_UnifiedVertexObject* parent,
                const uint64_t id, const std::string& name ) noexcept
    : parent( parent ), id( id ), name( name )
{
}

Object::Object( elGeometricObject_UnifiedVertexObject* parent,
                const uint64_t id, const floatBuffer_t& vertex,
                const floatBuffer_t& textureCoordinates,
                const floatBuffer_t& normals, const floatBuffer_t& tangents,
                const floatBuffer_t& bitangents, const uintBuffer_t& elements,
                const std::string& name ) noexcept
    : Object( parent, id, name )
{
    auto numberOfVertices = vertex.size() / 3;
    auto setFloatBuffer   = [&]( uint64_t bufferId, uint64_t size,
                               const floatBuffer_t& buffer = floatBuffer_t() )
    {
        this->floatBuffer[bufferId] = buffer;
        this->floatBuffer[bufferId].resize( size, 0.0f );
    };

    setFloatBuffer( VERTEX, numberOfVertices * 3, vertex );
    setFloatBuffer( TEX_COORD, numberOfVertices * 2, textureCoordinates );
    setFloatBuffer( NORMAL, numberOfVertices * 3, normals );
    setFloatBuffer( TANGENT, numberOfVertices * 3, tangents );
    setFloatBuffer( BITANGENT, numberOfVertices * 3, bitangents );

    setFloatBuffer( DIFFUSE_COLOR, numberOfVertices * 4 );
    setFloatBuffer( SPECULAR_COLOR, numberOfVertices * 4 );
    setFloatBuffer( SHININESS_AND_ROUGHNESS, numberOfVertices * 2 );
    setFloatBuffer( EMISSION_COLOR, numberOfVertices * 4 );
    setFloatBuffer( EVENT_COLOR, numberOfVertices * 4 );

    this->uintBuffer[ELEMENTS] = elements;

    auto setUintBuffer = [&]( uint64_t bufferId, uint64_t size )
    {
        this->uintBuffer[bufferId].resize(
            size, elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID );
    };

    setUintBuffer( TEX_DIFFUSE, numberOfVertices );
    setUintBuffer( TEX_NORMAL, numberOfVertices );
    setUintBuffer( TEX_SPECULAR_COLOR, numberOfVertices );
    setUintBuffer( TEX_SHININESS_AND_ROUGHNESS, numberOfVertices );
    setUintBuffer( TEX_EMISSION, numberOfVertices );
}

bb::product_ptr< Object >
Object::Clone() const noexcept
{
    auto clone      = this->parent->CreateObject( this->GetName() );
    clone->textures = this->textures;
    clone->SetPreDrawCallback( this->preDrawCallback );
    clone->EliminateCloseVerticesInElements(
        this->vertexElimination, this->vertexEliminationMinDistance );

    for ( uint64_t i = 0; i < floatBufferList_t::FB_END; ++i )
        clone->SetFloatBuffer(
            static_cast< floatBufferList_t >( i ),
            this->GetFloatBuffer( static_cast< floatBufferList_t >( i ) ) );

    for ( uint64_t i = 0; i < uintBufferList_t::UB_END; ++i )
        clone->SetUintBuffer(
            static_cast< uintBufferList_t >( i ),
            this->GetUintBuffer( static_cast< uintBufferList_t >( i ) ) );

    clone->SetPosition( this->GetPosition() );
    clone->SetScaling( this->GetScaling() );
    clone->SetRotation(
        glm::vec4( this->GetRotationAxis(), this->GetRotationDegree() ) );
    clone->SetModelMatrix( this->GetModelMatrix() );

    return clone;
}

const std::string&
Object::GetName() const noexcept
{
    return this->name;
}

uint64_t
Object::GetNumberOfVertices() const noexcept
{
    return this->floatBuffer[VERTEX].size() / 3;
}

void
Object::SetFloatBuffer( const floatBufferList_t bufferType,
                        const floatBuffer_t&    buffer ) noexcept
{
    this->floatBuffer[bufferType] = buffer;
    this->parent->runOnce.Call(
        elGeometricObject_UnifiedVertexObject::_Refresh );
}

void
Object::SetUintBuffer( const uintBufferList_t bufferType,
                       const uintBuffer_t&    buffer ) noexcept
{
    this->uintBuffer[bufferType] = buffer;
    this->parent->runOnce.Call(
        elGeometricObject_UnifiedVertexObject::_Refresh );
}

const floatBuffer_t&
Object::GetFloatBuffer( const floatBufferList_t b ) const noexcept
{
    return this->floatBuffer[b];
}

const uintBuffer_t&
Object::GetUintBuffer( const uintBufferList_t b ) const noexcept
{
    return this->uintBuffer[b];
}

utils::CallbackGuard
Object::SetPreDrawCallback( const std::function< void() >& f ) noexcept
{
    if ( !f ) return utils::CallbackGuard();
    this->preDrawCallback = f;

    return { [] {},
             [this] { this->preDrawCallback = std::function< void() >(); } };
}

void
Object::EliminateCloseVerticesInElements(
    const VertexElimination vertexElimination,
    const float             minimumDistance ) noexcept
{
    this->vertexElimination            = vertexElimination;
    this->vertexEliminationMinDistance = minimumDistance;
    this->parent->runOnce.Call(
        elGeometricObject_UnifiedVertexObject::_Refresh );
}

void
Object::UpdateModelMatrix() const noexcept
{
    if ( this->preDrawCallback ) this->preDrawCallback();

    if ( !this->hasUpdatedValues ) return;

    glm::mat4 modelMatrix = this->GetModelMatrix();
    float*    data        = glm::value_ptr( modelMatrix );

    for ( uint64_t k = 0; k < MAT4_LENGTH; ++k )
    {
        this->parent->uniformBuffer
            .data[this->id * elGeometricObject_UnifiedVertexObject::
                                 uniformBuffer_t::MATRIX_SIZE +
                  k] = data[k];
    }
    this->parent->runOnce.Call(
        elGeometricObject_UnifiedVertexObject::_RefreshUniformBufferData );
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
Object::CreateTexture( const std::string& uniqueIdentifier,
                       const GLvoid*      pixels ) noexcept
{
    auto texture = this->parent->CreateTexture( uniqueIdentifier, pixels );

    this->textures.emplace_back( texture );
    return texture;
}

bb::shared_product_ptr< UnifiedVertexObject::Texture >
Object::CreateTexture( const GLvoid* pixels ) noexcept
{
    auto texture = this->parent->CreateTexture( pixels );

    this->textures.emplace_back( texture );
    return texture;
}

void
Object::AttachTexture(
    const bb::shared_product_ptr< UnifiedVertexObject::Texture >&
        texture ) noexcept
{
    this->textures.emplace_back( texture );
}


bb::shared_product_ptr< UnifiedVertexObject::Texture >
Object::GetDefaultTexture() noexcept
{
    return this->parent->defaultTexture;
}

///////////////////////////
/// Texture
//////////////////////////

Texture::Texture( const uint64_t id ) noexcept : id{ id }
{
}

bool
Texture::operator==( const Texture& rhs ) const noexcept
{
    return this->id == rhs.id;
}

bool
Texture::operator!=( const Texture& rhs ) const noexcept
{
    return !this->operator==( rhs );
}

GLuint
Texture::GetId() const noexcept
{
    return static_cast< GLuint >( this->id );
}


} // namespace UnifiedVertexObject
} // namespace HighGL
} // namespace el3D
