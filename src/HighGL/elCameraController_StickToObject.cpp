#include "HighGL/elCameraController_StickToObject.hpp"

#include "logging/elLog.hpp"
#include "math/math.hpp"

#include <glm/gtx/quaternion.hpp>

namespace el3D
{
namespace HighGL
{
elCameraController_StickToObject::elCameraController_StickToObject(
    OpenGL::elCamera_Perspective&       camera,
    const std::function< glm::vec3() >& getPosition,
    const std::function< glm::quat() >& getRotationQuaternion,
    const glm::vec3& positionOffset, const glm::vec3& lookAtOffset ) noexcept
    : elCameraController_base( elCameraController::StickToObject, camera ),
      getPosition{ getPosition }, getRotationQuaternion{ getRotationQuaternion }
{
    this->hasContinuousUpdates = true;
    if ( !this->getRotationQuaternion || !this->getPosition )
    {
        LOG_FATAL( 0 )
            << "either the getRotationQuaternion or the getPosition is empty";
        std::terminate();
    }

    this->SetPositionOffset( positionOffset );
    this->SetLookAtOffset( lookAtOffset );
}

glm::vec3
elCameraController_StickToObject::GetPositionOffset() const noexcept
{
    return this->positionOffset;
}

glm::vec3
elCameraController_StickToObject::GetLookAtOffset() const noexcept
{
    return this->lookAtOffset;
}

void
elCameraController_StickToObject::SetPositionOffset(
    const glm::vec3& v ) noexcept
{
    this->positionOffset = v;
}

void
elCameraController_StickToObject::SetLookAtOffset( const glm::vec3& v ) noexcept
{
    this->lookAtOffset = v;
}

void
elCameraController_StickToObject::CalculateState() noexcept
{
    this->state.position =
        this->getPosition() +
        math::Rotate( this->positionOffset, this->getRotationQuaternion() );
    this->state.up = this->cameraState->up;
    this->state.lookAt =
        math::Rotate( this->lookAtOffset, this->getRotationQuaternion() );
    this->state.fov = this->cameraState->fov;
}
} // namespace HighGL
} // namespace el3D

