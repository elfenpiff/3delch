#include "HighGL/elBoundingBoxGenerator.hpp"

#include "buildingBlocks/algorithm_extended.hpp"

#include <iostream>

namespace el3D
{
namespace HighGL
{
elBoundingBoxGenerator::elBoundingBoxGenerator(
    const std::vector< float >& vertices,
    const float                 relativeCornerLineLength ) noexcept
    : relativeCornerLineLength( relativeCornerLineLength )
{
    this->SetMinMax( vertices );
    this->SetBorderLines();
}

const elBoundingBoxGenerator::objectGeometry_t&
elBoundingBoxGenerator::GetGeometry() const noexcept
{
    return this->geometry;
}

void
elBoundingBoxGenerator::SetBorderLines() noexcept
{
    // cornerLines
    // top
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->max.z },
                   { this->max.x, this->max.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->min.z },
                   { this->max.x, this->max.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->min.z },
                   { this->max.x - this->absoluteCornerLineLength, this->max.y,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->min.z },
                   { this->min.x + this->absoluteCornerLineLength, this->max.y,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->max.z },
                   { this->min.x, this->max.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->min.z },
                   { this->min.x, this->max.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->max.z },
                   { this->min.x + this->absoluteCornerLineLength, this->max.y,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->max.z },
                   { this->max.x - this->absoluteCornerLineLength, this->max.y,
                     this->max.z } );

    // bottom
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->max.z },
                   { this->max.x, this->min.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->min.z },
                   { this->max.x, this->min.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->min.z },
                   { this->max.x - this->absoluteCornerLineLength, this->min.y,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->min.z },
                   { this->min.x + this->absoluteCornerLineLength, this->min.y,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->max.z },
                   { this->min.x, this->min.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->min.z },
                   { this->min.x, this->min.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->max.z },
                   { this->max.x - this->absoluteCornerLineLength, this->min.y,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->max.z },
                   { this->min.x + this->absoluteCornerLineLength, this->min.y,
                     this->max.z } );

    // top to bottom
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->max.z },
                   { this->max.x, this->max.y - this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->max.z },
                   { this->max.x, this->min.y + this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->max.z },
                   { this->min.x, this->max.y - this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->max.z },
                   { this->min.x, this->min.y + this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->max.y, this->min.z },
                   { this->min.x, this->max.y - this->absoluteCornerLineLength,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->min.x, this->min.y, this->min.z },
                   { this->min.x, this->min.y + this->absoluteCornerLineLength,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->max.y, this->min.z },
                   { this->max.x, this->max.y - this->absoluteCornerLineLength,
                     this->min.z } );
    this->AddLine( this->geometry.cornerLines,
                   { this->max.x, this->min.y, this->min.z },
                   { this->max.x, this->min.y + this->absoluteCornerLineLength,
                     this->min.z } );

    // borderLines
    // top
    this->AddLine( this->geometry.borderLines,
                   { this->max.x, this->max.y,
                     this->max.z - this->absoluteCornerLineLength },
                   { this->max.x, this->max.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.borderLines,
                   { this->max.x - this->absoluteCornerLineLength, this->max.y,
                     this->min.z },
                   { this->min.x + this->absoluteCornerLineLength, this->max.y,
                     this->min.z } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x, this->max.y,
                     this->min.z + this->absoluteCornerLineLength },
                   { this->min.x, this->max.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x + this->absoluteCornerLineLength, this->max.y,
                     this->max.z },
                   { this->max.x - this->absoluteCornerLineLength, this->max.y,
                     this->max.z } );

    // bottom
    this->AddLine( this->geometry.borderLines,
                   { this->max.x, this->min.y,
                     this->max.z - this->absoluteCornerLineLength },
                   { this->max.x, this->min.y,
                     this->min.z + this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.borderLines,
                   { this->max.x - this->absoluteCornerLineLength, this->min.y,
                     this->min.z },
                   { this->min.x + this->absoluteCornerLineLength, this->min.y,
                     this->min.z } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x, this->min.y,
                     this->min.z + this->absoluteCornerLineLength },
                   { this->min.x, this->min.y,
                     this->max.z - this->absoluteCornerLineLength } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x + this->absoluteCornerLineLength, this->min.y,
                     this->max.z },
                   { this->max.x - this->absoluteCornerLineLength, this->min.y,
                     this->max.z } );

    // top to bottom
    this->AddLine( this->geometry.borderLines,
                   { this->max.x, this->max.y - this->absoluteCornerLineLength,
                     this->max.z },
                   { this->max.x, this->min.y + this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x, this->max.y - this->absoluteCornerLineLength,
                     this->max.z },
                   { this->min.x, this->min.y + this->absoluteCornerLineLength,
                     this->max.z } );
    this->AddLine( this->geometry.borderLines,
                   { this->min.x, this->max.y - this->absoluteCornerLineLength,
                     this->min.z },
                   { this->min.x, this->min.y + this->absoluteCornerLineLength,
                     this->min.z } );
    this->AddLine( this->geometry.borderLines,
                   { this->max.x, this->max.y - this->absoluteCornerLineLength,
                     this->min.z },
                   { this->max.x, this->min.y + this->absoluteCornerLineLength,
                     this->min.z } );
}

void
elBoundingBoxGenerator::AddLine( std::vector< GLfloat >& lineVertices,
                                 const glm::vec3&        start,
                                 const glm::vec3&        end ) noexcept
{
    lineVertices.emplace_back( start.x );
    lineVertices.emplace_back( start.y );
    lineVertices.emplace_back( start.z );

    lineVertices.emplace_back( end.x );
    lineVertices.emplace_back( end.y );
    lineVertices.emplace_back( end.z );
}

void
elBoundingBoxGenerator::SetMinMax(
    const std::vector< float >& vertices ) noexcept
{
    for ( uint64_t i = 0, limit = vertices.size(); i < limit; i += 3 )
    {
        glm::vec3 v{ vertices[i], vertices[i + 1], vertices[i + 2] };

        if ( i == 0 )
        {
            this->min = v;
            this->max = v;
        }
        else
        {
            this->min.x = std::min( v.x, this->min.x );
            this->min.y = std::min( v.y, this->min.y );
            this->min.z = std::min( v.z, this->min.z );

            this->max.x = std::max( v.x, this->max.x );
            this->max.y = std::max( v.y, this->max.y );
            this->max.z = std::max( v.z, this->max.z );
        }
    }

    auto dimension = this->max - this->min;
    this->absoluteCornerLineLength =
        this->relativeCornerLineLength * bb::min( fabsf( dimension.x ),
                                                  fabsf( dimension.y ),
                                                  fabsf( dimension.z ) );
}

} // namespace HighGL
} // namespace el3D
