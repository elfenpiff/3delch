#include "HighGL/elMeshLoader.hpp"

#include "HighGL/Conversion.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "buildingBlocks/product_ptr.hpp"

namespace el3D
{
namespace HighGL
{
elMeshLoader::elMeshLoader( const std::string& fileName, const float scaling )
    : fileName( fileName ), scaling( scaling )
{

    Assimp::Importer importer;
    this->scene = importer.ReadFile(
        fileName.c_str(),
        aiProcess_Triangulate | aiProcess_FlipUVs |
            aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals |
            aiProcess_CalcTangentSpace | aiProcess_FlipWindingOrder );

    if ( !this->scene )
    {
        LOG_ERROR( 0 ) << "Failed to load mesh from file " << fileName << ". "
                       << importer.GetErrorString();
        this->create_isInitialized = false;
        this->create_error         = elMeshLoader_Error::UnableToLoadFile;
    }
    else
    {
        this->objectMaterials.resize( this->scene->mNumMaterials );
        aiIdentityMatrix4( &this->transformation );
        aiMultiplyMatrix4( &this->transformation,
                           &this->scene->mRootNode->mTransformation );

        this->SetTextureArraySize();
        this->GenerateMaterials();

        this->GenerateVertexObjects()
            .AndThen( [&] { this->create_isInitialized = true; } )
            .OrElse( [&] { this->create_isInitialized = false; } );
    }
}

void
elMeshLoader::ExtractMaterial( const aiMaterial*  material,
                               const material_t&  type,
                               const uint64_t     materialIndex,
                               const std::string& path ) noexcept
{
    if ( type.type == aiTextureType_NONE ) return;

    if ( material->GetTextureCount( type.type ) > 0 )
    {
        aiString textureFile;
        if ( material->GetTexture( type.type, 0, &textureFile, nullptr, nullptr,
                                   nullptr, nullptr, nullptr ) == AI_SUCCESS )
        {
            std::string filePath = this->AdjustPathToPlatform(
                path + std::string( textureFile.C_Str() ) );

            this->objectMaterials[materialIndex]
                                 [static_cast< uint64_t >( type.id )] =
                this->AddRawTexture(
                    filePath, ( type.type == aiTextureType_HEIGHT )
                                  ? TextureConversion::HeightMapToNormalMap
                                  : TextureConversion::None );

            return;
        }
    }

    this->objectMaterials[materialIndex][static_cast< uint64_t >( type.id )] =
        INVALID_ID;
}

uint64_t
elMeshLoader::AddRawTexture( const std::string&      filePath,
                             const TextureConversion conversion ) noexcept
{
    for ( uint64_t k = 0, l = this->rawTextures.size(); k < l; ++k )
        if ( this->rawTextures[k].filePath == filePath ) return k;

    auto image = GLAPI::elImage::Create( GLAPI::FromFilePath, filePath,
                                         SDL_PIXELFORMAT_RGBA32 );
    if ( image.HasError() )
    {
        LOG_WARN( 0 ) << "unable to load texture file " << filePath
                      << " for mesh file " << this->fileName;
    }

    this->rawTextures.emplace_back(
        rawTexture_t{ filePath, conversion, std::move( image.GetValue() ) } );

    return rawTextures.size() - 1;
}

void
elMeshLoader::SetTextureArraySize() noexcept
{
    this->textureArraySize = 0;
    for ( uint64_t i = 0; i < this->scene->mNumMaterials; ++i )
        for ( auto& m : this->materials )
            this->textureArraySize +=
                this->scene->mMaterials[i]->GetTextureCount( m.type );
}

void
elMeshLoader::GenerateMaterials() noexcept
{
    std::string filePath = this->GetDirectoryFromFilePath( this->fileName );
    for ( uint64_t i = 0; i < this->scene->mNumMaterials; ++i )
    {
        this->objectMaterials[i].resize( this->materials.size(), 0u );
        for ( auto& m : this->materials )
            this->ExtractMaterial( this->scene->mMaterials[i], m, i, filePath );
    }
}

bb::elExpected< elMeshLoader_Error >
elMeshLoader::GenerateVertexObjects()
{
    for ( uint64_t i = 0; i < this->scene->mNumMeshes; ++i )
    {
        aiMesh* mesh         = this->scene->mMeshes[i];
        auto    vertexSize   = mesh->mNumVertices * this->dimension.vertex;
        auto    elementSize  = mesh->mNumFaces * this->dimension.vertex;
        auto    texCoordSize = mesh->mNumVertices * this->dimension.texCoord;

        object_t newObject;

        newObject.name = mesh->mName.C_Str();

        newObject.vertices.resize( vertexSize, 0.0f );
        newObject.textureCoordinates.resize( texCoordSize, 0.0f );
        newObject.normals.resize( vertexSize, 0.0f );
        newObject.tangents.resize( vertexSize, 0.0f );
        newObject.bitangents.resize( vertexSize, 0.0f );
        newObject.elements.resize( elementSize, 0u );
        newObject.materialIds.resize( mesh->mNumVertices, 0u );

        for ( uint64_t k = 0; k < mesh->mNumVertices; ++k )
        {
            if ( mesh->mVertices != nullptr )
                this->AddToBufferFromVector( &( mesh->mVertices[k] ), k, false,
                                             newObject.vertices,
                                             dimension.vertex );

            if ( mesh->mTextureCoords[0] != nullptr )
                this->AddToBufferFromVector( &( mesh->mTextureCoords[0][k] ), k,
                                             !mesh->HasTextureCoords( 0 ),
                                             newObject.textureCoordinates,
                                             dimension.texCoord );

            if ( mesh->mNormals != nullptr )
                this->AddToBufferFromVector(
                    &( mesh->mNormals[k] ), k, !mesh->HasNormals(),
                    newObject.normals, dimension.vertex );

            if ( mesh->mTangents != nullptr )
                this->AddToBufferFromVector( &( mesh->mTangents[k] ), k,
                                             !mesh->HasTangentsAndBitangents(),
                                             newObject.tangents,
                                             dimension.vertex );

            if ( mesh->mBitangents != nullptr )
                this->AddToBufferFromVector( &( mesh->mBitangents[k] ), k,
                                             !mesh->HasTangentsAndBitangents(),
                                             newObject.bitangents,
                                             dimension.vertex );
        }

        bb::for_each( newObject.vertices,
                      [&]( auto& v ) { v *= this->scaling; } );

        for ( uint64_t k = 0; k < mesh->mNumFaces; ++k )
        {
            const aiFace& face = mesh->mFaces[k];
            if ( face.mNumIndices != this->dimension.vertex )
            {
                LOG_ERROR( 0 )
                    << "could not load mesh file: " << this->fileName
                    << " since one element has not the correct number of "
                       "vertex "
                    << "indices (" << std::to_string( face.mNumIndices )
                    << " != "
                    << std::to_string( this->dimension.vertex ) + " )";
                return bb::Error(
                    elMeshLoader_Error::InconsistentVertexIndicies );
            }

            for ( uint64_t m = 0; m < face.mNumIndices; ++m )
                newObject.elements[k * this->dimension.vertex + m] =
                    face.mIndices[m];
        }

        for ( uint64_t k = 0; k < mesh->mNumVertices; ++k )
            newObject.materialIds[k] = mesh->mMaterialIndex;

        this->objects.emplace_back( std::move( newObject ) );
    }

    return bb::Success<>();
}

void
elMeshLoader::AddToBufferFromVector( const aiVector3D*       vec,
                                     const uint64_t          offset,
                                     const bool              useZeroVector,
                                     std::vector< GLfloat >& buffer,
                                     const uint64_t bufferDim ) const noexcept
{
    aiVector3D temp = ( useZeroVector ) ? aiVector3D( 0.0f, 0.0f, 0.0f ) : *vec;
    aiTransformVecByMatrix4( &temp, &this->transformation );

    for ( uint64_t i = 0; i < bufferDim; ++i )
        buffer[offset * bufferDim + i] = temp[static_cast< unsigned int >( i )];
}

uint64_t
elMeshLoader::GetTextureArraySize() const noexcept
{
    return this->textureArraySize;
}

std::string
elMeshLoader::GetDirectoryFromFilePath(
    const std::string& filePath ) const noexcept
{
    uint64_t pos = filePath.find_last_of( this->pathSeparator );
#ifdef _WIN32
    if ( pos == std::string::npos )
        pos = filePath.find_last_of( this->foreignPathSeparator );
#endif
    if ( pos == std::string::npos )
        return "./";
    else
        return filePath.substr( 0, pos + 1 );
}

std::vector< bb::product_ptr< UnifiedVertexObject::Object > >
elMeshLoader::LoadIntoUnifiedVertexObject(
    elGeometricObject_UnifiedVertexObject& uvo ) noexcept
{
    std::vector< bb::product_ptr< UnifiedVertexObject::Object > > uvoObjects;
    std::vector< bb::shared_product_ptr< UnifiedVertexObject::Texture > >
         loadedTextures;
    auto texSize = uvo.GetTextureArraySize();
    for ( auto& t : this->rawTextures )
    {
        bb::shared_product_ptr< UnifiedVertexObject::Texture > newTexture;
        if ( t.image )
        {
            switch ( t.conversion )
            {
                case TextureConversion::HeightMapToNormalMap:
                {
                    float normalScaling = 0.01f;
                    t.image->Resize( texSize.x + 2, texSize.y + 2 );
                    newTexture = uvo.CreateTexture(
                        t.filePath,
                        ConvertHeightMapToNormalMap(
                            texSize.x + 2, texSize.y + 2,
                            t.image->GetByteStream(), normalScaling )
                            .data.stream.data() );
                    break;
                }
                default:
                {
                    t.image->Resize( texSize.x, texSize.y );
                    newTexture = uvo.CreateTexture( t.filePath,
                                                    t.image->GetBytePointer() );
                }
            }

            if ( !newTexture )
            {
                LOG_ERROR( 0 )
                    << "unable to load texture " << t.filePath << " for model "
                    << this->fileName
                    << " since the unified vertex object texture array size is "
                       "too small. Loading default texture instead!";
                newTexture = uvo.GetDefaultTexture();
            }
        }
        else
        {
            newTexture = uvo.GetDefaultTexture();
        }

        loadedTextures.emplace_back( newTexture );
    }

    for ( auto& o : this->objects )
    {
        uint64_t              numberOfVertices = o.materialIds.size();
        std::vector< GLuint > diffuseTextureIds(
            numberOfVertices,
            elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID );
        std::vector< GLuint > specularTextureIds(
            numberOfVertices,
            elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID );
        std::vector< GLuint > normalTextureIds(
            numberOfVertices,
            elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID );

        uvoObjects.emplace_back(
            uvo.CreateObject( o.vertices, o.textureCoordinates, o.normals,
                              o.tangents, o.bitangents, o.elements, o.name ) );
        auto& newObject = uvoObjects.back();

        for ( auto& material : this->materials )
        {
            auto fillTextureIds = [&]( std::vector< GLuint >& ids,
                                       aiTextureType type, uint64_t typeIndex )
            {
                if ( material.type != type ) return;

                for ( uint64_t k = 0, limit = o.materialIds.size(); k < limit;
                      ++k )
                {
                    auto index =
                        this->objectMaterials[o.materialIds[k]][typeIndex];

                    if ( index != INVALID_ID )
                    {
                        newObject->AttachTexture( loadedTextures[index] );

                        ids[k] = loadedTextures[index]->GetId();
                    }
                }
            };

            fillTextureIds( diffuseTextureIds, aiTextureType_DIFFUSE, 0 );
            fillTextureIds( normalTextureIds, aiTextureType_HEIGHT, 1 );
            fillTextureIds( specularTextureIds, aiTextureType_SPECULAR, 2 );
            fillTextureIds( normalTextureIds, aiTextureType_NORMALS, 1 );
        }

        newObject->SetUintBuffer( UnifiedVertexObject::TEX_DIFFUSE,
                                  diffuseTextureIds );
        newObject->SetUintBuffer( UnifiedVertexObject::TEX_NORMAL,
                                  normalTextureIds );
        newObject->SetUintBuffer( UnifiedVertexObject::TEX_SPECULAR_COLOR,
                                  specularTextureIds );
    }

    return uvoObjects;
}

std::string
elMeshLoader::AdjustPathToPlatform( const std::string& filePath ) const noexcept
{
    std::string retVal = filePath;
    for ( auto& c : retVal )
        if ( c == foreignPathSeparator ) c = pathSeparator;
    return retVal;
}

std::string
elMeshLoader::TextureEnumToString(
    const aiTextureType textureType ) const noexcept
{
    switch ( textureType )
    {
        case aiTextureType_NONE:
            return "TextureType::None";
        case aiTextureType_DIFFUSE:
            return "TextureType::Diffuse";
        case aiTextureType_SPECULAR:
            return "TextureType::Specular";
        case aiTextureType_AMBIENT:
            return "TextureType::Ambient";
        case aiTextureType_EMISSIVE:
            return "TextureType::Emissive";
        case aiTextureType_HEIGHT:
            return "TextureType::Height";
        case aiTextureType_NORMALS:
            return "TextureType::Normals";
        case aiTextureType_SHININESS:
            return "TextureType::Shininess";
        case aiTextureType_OPACITY:
            return "TextureType::Opacity";
        case aiTextureType_DISPLACEMENT:
            return "TextureType::Displacement";
        case aiTextureType_LIGHTMAP:
            return "TextureType::Lightmap";
        case aiTextureType_REFLECTION:
            return "TextureType::Reflection";
        case aiTextureType_BASE_COLOR:
            return "TextureType::BaseColor";
        case aiTextureType_NORMAL_CAMERA:
            return "TextureType::NormalCamera";
        case aiTextureType_EMISSION_COLOR:
            return "TextureType::EmissionColor";
        case aiTextureType_METALNESS:
            return "TextureType::Metalness";
        case aiTextureType_DIFFUSE_ROUGHNESS:
            return "TextureType::DiffuseRoughness";
        case aiTextureType_AMBIENT_OCCLUSION:
            return "TextureType::AmbientOcclusion";
        default:
            return "TextureType::Unknown";
    }
}
} // namespace HighGL
} // namespace el3D
