#include "HighGL/elCoordinateSystemGenerator.hpp"

#include "3Delch/3Delch.hpp"
#include "GuiGL/common.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"

namespace el3D
{
namespace HighGL
{
elCoordinateSystemGenerator::elCoordinateSystemGenerator(
    const settings_t& settings ) noexcept
    : settings{ settings }
{
    switch ( this->settings.gridStyle )
    {
        case GridStyle::Lines:
            this->geometry.majorLines =
                this->CreateLineVertices( this->settings.majorTileSize,
                                          { 0, 0 }, this->settings.gridSize );
            this->geometry.minorLines = this->CreateLineVertices(
                this->settings.minorTileSize, { 0, 0 }, this->settings.gridSize,
                this->settings.majorTileSize );

            break;

        case GridStyle::DotsAndCrosses:
            this->geometry.majorLines =
                this->CreateCrossVertices( this->settings.majorTileSize,
                                           this->settings.minorTileSize / 2.0f,
                                           { 0, 0 }, this->settings.gridSize );

            this->geometry.minorLines = this->CreateDotVertices(
                this->settings.minorTileSize, { 0, 0 }, this->settings.gridSize,
                this->settings.majorTileSize );
            break;
    }
}

elCoordinateSystemGenerator::~elCoordinateSystemGenerator()
{
}

const elCoordinateSystemGenerator::objectGeometry_t&
elCoordinateSystemGenerator::GetGeometry() const noexcept
{
    return this->geometry;
}

std::vector< GLfloat >
elCoordinateSystemGenerator::CreateDotVertices(
    const GLfloat tileSize, const glm::vec2& offset, const glm::vec2& gridSize,
    const GLfloat skipLine ) const noexcept
{
    const GLfloat          epsilon = tileSize / 2.0f;
    std::vector< GLfloat > gridVertices;

    auto doSkipLine = [&]( const GLfloat position ) -> bool
    {
        if ( skipLine <= 0.0f ) return false;
        GLfloat remainder = position / skipLine;
        return ( abs( remainder - round( remainder ) ) <= epsilon );
    };

    auto ceilVec = []( const glm::vec2 v )
    { return glm::vec2( ceil( v.x ), ceil( v.y ) ); };
    auto floorVec = []( const glm::vec2 v )
    { return glm::vec2( floor( v.x ), floor( v.y ) ); };


    glm::vec2 start = ceilVec( ( offset - gridSize ) / tileSize ) * tileSize;
    glm::vec2 end   = floorVec( ( offset + gridSize ) / tileSize ) * tileSize;

    for ( GLfloat i = start.x; i < end.x + epsilon; i += tileSize )
    {
        for ( GLfloat n = start.y; n < end.y + epsilon; n += tileSize )
        {
            if ( doSkipLine( n ) && doSkipLine( i ) ) continue;

            gridVertices.emplace_back( i + this->settings.position.x );
            gridVertices.emplace_back( 0.0f + this->settings.position.y );
            gridVertices.emplace_back( n + this->settings.position.z );
        }
    }

    return gridVertices;
}

std::vector< GLfloat >
elCoordinateSystemGenerator::CreateCrossVertices(
    const GLfloat tileSize, const GLfloat crossSize, const glm::vec2& offset,
    const glm::vec2& gridSize, const GLfloat skipLine ) const noexcept
{
    const GLfloat          epsilon = tileSize / 2.0f;
    std::vector< GLfloat > gridVertices;

    auto doSkipLine = [&]( const GLfloat position ) -> bool
    {
        if ( skipLine <= 0.0f ) return false;
        GLfloat remainder = position / skipLine;
        return ( abs( remainder - round( remainder ) ) <= epsilon );
    };

    auto ceilVec = []( const glm::vec2 v )
    { return glm::vec2( ceil( v.x ), ceil( v.y ) ); };
    auto floorVec = []( const glm::vec2 v )
    { return glm::vec2( floor( v.x ), floor( v.y ) ); };


    glm::vec2 start = ceilVec( ( offset - gridSize ) / tileSize ) * tileSize;
    glm::vec2 end   = floorVec( ( offset + gridSize ) / tileSize ) * tileSize;

    for ( GLfloat i = start.x; i < end.x + epsilon; i += tileSize )
    {
        for ( GLfloat n = start.y; n < end.y + epsilon; n += tileSize )
        {
            if ( doSkipLine( n ) && doSkipLine( i ) ) continue;

            gridVertices.emplace_back( i - crossSize +
                                       this->settings.position.x );
            gridVertices.emplace_back( 0.0f + this->settings.position.y );
            gridVertices.emplace_back( n + this->settings.position.z );

            gridVertices.emplace_back( i + crossSize +
                                       this->settings.position.x );
            gridVertices.emplace_back( 0.0f + this->settings.position.y );
            gridVertices.emplace_back( n + this->settings.position.z );

            gridVertices.emplace_back( i + this->settings.position.x );
            gridVertices.emplace_back( 0.0f + this->settings.position.y );
            gridVertices.emplace_back( n - crossSize +
                                       this->settings.position.z );

            gridVertices.emplace_back( i + this->settings.position.x );
            gridVertices.emplace_back( 0.0f + this->settings.position.y );
            gridVertices.emplace_back( n + crossSize +
                                       this->settings.position.z );
        }
    }

    return gridVertices;
}


std::vector< GLfloat >
elCoordinateSystemGenerator::CreateLineVertices(
    const GLfloat tileSize, const glm::vec2& offset, const glm::vec2& gridSize,
    const GLfloat skipLine ) const noexcept
{
    const GLfloat          epsilon = tileSize / 2.0f;
    std::vector< GLfloat > gridVertices;

    auto doSkipLine = [&]( const GLfloat position ) -> bool
    {
        if ( skipLine <= 0.0f ) return false;
        GLfloat remainder = position / skipLine;
        return ( abs( remainder - round( remainder ) ) <= epsilon );
    };

    auto ceilVec = []( const glm::vec2 v )
    { return glm::vec2( ceil( v.x ), ceil( v.y ) ); };
    auto floorVec = []( const glm::vec2 v )
    { return glm::vec2( floor( v.x ), floor( v.y ) ); };


    glm::vec2 start = ceilVec( ( offset - gridSize ) / tileSize ) * tileSize;
    glm::vec2 end   = floorVec( ( offset + gridSize ) / tileSize ) * tileSize;

    for ( GLfloat i = start.x; i < end.x + epsilon; i += tileSize )
    {
        if ( doSkipLine( i ) ) continue;

        gridVertices.emplace_back( i + this->settings.position.x );
        gridVertices.emplace_back( this->settings.position.y );
        gridVertices.emplace_back( -gridSize.y + offset.y +
                                   this->settings.position.z );

        gridVertices.emplace_back( i + this->settings.position.x );
        gridVertices.emplace_back( this->settings.position.y );
        gridVertices.emplace_back( gridSize.y + offset.y +
                                   this->settings.position.z );
    }

    for ( GLfloat i = start.y; i < end.y + epsilon; i += tileSize )
    {
        if ( doSkipLine( i ) ) continue;

        gridVertices.emplace_back( -gridSize.x + offset.x +
                                   this->settings.position.x );
        gridVertices.emplace_back( this->settings.position.y );
        gridVertices.emplace_back( i + this->settings.position.z );

        gridVertices.emplace_back( gridSize.x + offset.x +
                                   this->settings.position.x );
        gridVertices.emplace_back( this->settings.position.y );
        gridVertices.emplace_back( i + this->settings.position.z );
    }

    return gridVertices;
}

void
elCoordinateSystemGenerator::UpdateBuffers( const glm::vec2& offset ) noexcept
{
    switch ( this->settings.gridStyle )
    {
        case GridStyle::Lines:
            this->geometry.majorLines = this->CreateLineVertices(
                this->settings.majorTileSize, offset, this->settings.gridSize );
            this->geometry.minorLines = this->CreateLineVertices(
                this->settings.minorTileSize, offset, this->settings.gridSize,
                this->settings.majorTileSize );
            break;
        case GridStyle::DotsAndCrosses:
            this->geometry.majorLines =
                this->CreateCrossVertices( this->settings.majorTileSize,
                                           this->settings.minorTileSize / 2.0f,
                                           offset, this->settings.gridSize );
            this->geometry.minorLines = this->CreateDotVertices(
                this->settings.minorTileSize, offset, this->settings.gridSize,
                this->settings.majorTileSize );
            break;
    }
}

} // namespace HighGL
} // namespace el3D
