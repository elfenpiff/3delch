#include "HighGL/elLight_base.hpp"

namespace el3D
{
namespace HighGL
{
elLight_base::elLight_base( const glm::vec3 &color,
                            const GLfloat    diffuseIntensity,
                            const GLfloat    ambientIntensity ) noexcept
    : color( color ), diffuseIntensity( diffuseIntensity ),
      ambientIntensity( ambientIntensity )
{
}

elLight_base::~elLight_base()
{
}

bool
elLight_base::HasFog() const noexcept
{
    return this->hasFog;
}

void
elLight_base::SetFogIntensity( const float v ) noexcept
{
    this->fogIntensity       = v;
    this->hasUpdatedSettings = true;
}

GLfloat
elLight_base::GetFogIntensity() const noexcept
{
    return this->fogIntensity;
}

void
elLight_base::SetHasFog( const bool v ) noexcept
{
    this->hasFog             = v;
    this->hasUpdatedSettings = true;
}

void
elLight_base::SetColor( const glm::vec3 &c ) noexcept
{
    this->color              = c;
    this->hasUpdatedSettings = true;
}

glm::vec3
elLight_base::GetColor() const noexcept
{
    return this->color;
}

bool
elLight_base::HasUpdatedSettings() noexcept
{
    return this->hasUpdatedSettings;
}

void
elLight_base::SettingsUpdated() noexcept
{
    this->hasUpdatedSettings = false;
}

void
elLight_base::SetCastShadow( const bool v ) noexcept
{
    this->doesCastShadow     = v;
    this->hasUpdatedSettings = true;
}

bool
elLight_base::DoesCastShadow() const noexcept
{
    return this->doesCastShadow;
}

void
elLight_base::SetDiffuseIntensity( const GLfloat v )
{
    this->diffuseIntensity   = v;
    this->hasUpdatedSettings = true;
}

void
elLight_base::SetAmbientIntensity( const GLfloat v )
{
    this->ambientIntensity   = v;
    this->hasUpdatedSettings = true;
}

GLfloat
elLight_base::GetDiffuseIntensity() const noexcept
{
    return this->diffuseIntensity;
}

GLfloat
elLight_base::GetAmbientIntensity() const noexcept
{
    return this->ambientIntensity;
}

void
elLight_base::ConnectGeometricLightObject(
    OpenGL::elGeometricObject_ModelMatrix &modelMatrix ) noexcept
{
    this->geometricObject = &modelMatrix;
}

void
elLight_base::DisconnectGeometricLightObject() noexcept
{
    this->geometricObject = nullptr;
}


} // namespace HighGL
} // namespace el3D
