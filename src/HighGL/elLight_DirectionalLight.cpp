#include "HighGL/elLight_DirectionalLight.hpp"

#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elObjectBuffer.hpp"

namespace el3D
{
namespace HighGL
{
static std::unique_ptr< OpenGL::elObjectBuffer > onScreenQuad;


elLight_DirectionalLight::elLight_DirectionalLight()
    : elLight_DirectionalLight( glm::vec3( 0.0f ), glm::vec3( 0.0f ), 0.0f,
                                0.0f )
{
}

elLight_DirectionalLight::elLight_DirectionalLight(
    const glm::vec3 &color, const glm::vec3 &direction,
    const GLfloat diffuseIntensity, const GLfloat ambientIntensity )
    : elLight_base( color, diffuseIntensity, ambientIntensity )
{
    this->SetDirection( direction );
    if ( !onScreenQuad )
    {
        auto geometry =
            elObjectGeometryBuilder::CreateScreenQuad().GetObjectGeometry();
        onScreenQuad = std::make_unique< OpenGL::elObjectBuffer >(
            3, geometry.vertices, geometry.innerElements );
    }
}

OpenGL::elCamera_Orthographic
elLight_DirectionalLight::GetOrthographicCamera() const noexcept
{
    return { this->GetFrustum(), this->GetZFactor(), this->GetLookAt() };
}

OpenGL::elCamera_Orthographic::frustum_t
elLight_DirectionalLight::GetFrustum() const noexcept
{
    OpenGL::elCamera_Orthographic::frustum_t frustum;
    float frustumRange = this->zFar * this->frustumRangeScaling;
    frustum.left       = -frustumRange;
    frustum.right      = frustumRange;
    frustum.top        = -frustumRange;
    frustum.bottom     = frustumRange;
    return frustum;
}

OpenGL::elCamera::zFactor_t
elLight_DirectionalLight::GetZFactor() const noexcept
{
    OpenGL::elCamera::zFactor_t zFactor;
    float                       frustumRange = this->zFar;
    zFactor.zNear = units::Length::Meter( -frustumRange );
    zFactor.zFar  = units::Length::Meter( frustumRange );
    return zFactor;
}

OpenGL::elCamera_Orthographic::lookAt_t
elLight_DirectionalLight::GetLookAt() const noexcept
{
    OpenGL::elCamera_Orthographic::lookAt_t lookAt;

    lookAt.eye    = this->position - this->GetDirection();
    lookAt.up     = ( abs( glm::dot( this->GetDirection(),
                                     { 0.0f, 1.0f, 0.0f } ) ) >= 0.25f )
                        ? glm::vec3{ 1.0f, 0.0f, 0.0f }
                        : glm::vec3{ 0.0f, 1.0f, 0.0f };
    lookAt.center = this->position;
    return lookAt;
}

void
elLight_DirectionalLight::UpdateCameraParameters( const glm::vec3 &position,
                                                  const float zFar ) noexcept
{
    this->position = position;
    this->zFar     = zFar;

    if ( !this->geometricObject ) return;
    this->geometricObject->SetPosition( -this->direction * this->zFar * 0.95f +
                                        this->position );
}

void
elLight_DirectionalLight::UpdateGeometricObjectPosition() noexcept
{
    this->UpdateCameraParameters( this->position, this->zFar );
}

void
elLight_DirectionalLight::Bind(
    const OpenGL::elShaderProgram::shaderAttribute_t &a )
{
    onScreenQuad->Bind();
    onScreenQuad->Enable( { BUFFER_VERTEX, BUFFER_ELEMENT }, a );
}

void
elLight_DirectionalLight::Unbind(
    const OpenGL::elShaderProgram::shaderAttribute_t &a )
{
    onScreenQuad->Disable( { a } );
}

void
elLight_DirectionalLight::SetDirection( const glm::vec3 &v )
{
    this->direction = glm::normalize( v );
    this->UpdateGeometricObjectPosition();
    this->hasUpdatedSettings = true;
}

glm::vec3
elLight_DirectionalLight::GetDirection() const noexcept
{
    return this->direction;
}

void
elLight_DirectionalLight::SetFrustumRangeScaling( const float v ) noexcept
{
    this->frustumRangeScaling = v;
}

} // namespace HighGL
} // namespace el3D
