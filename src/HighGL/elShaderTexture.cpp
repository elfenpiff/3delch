#include "HighGL/elShaderTexture.hpp"

#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "OpenGL/gl_Call.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"

#include <iterator>


namespace el3D
{
namespace HighGL
{
elShaderTexture::elShaderTexture(
    const std::vector< OpenGL::textureProperties_t > outputTextureProperties,
    const std::string &fragmentShaderCode, const GLsizei sizeX,
    const GLsizei sizeY, const bool isFlipFlop,
    const bool enableTextureCoordinates )
    : objectBuffer( 3,
                    elObjectGeometryBuilder::CreateScreenQuad()
                        .GetObjectGeometry()
                        .vertices,
                    elObjectGeometryBuilder::CreateScreenQuad()
                        .GetObjectGeometry()
                        .innerElements,
                    2u,
                    elObjectGeometryBuilder::CreateScreenQuad()
                        .GetObjectGeometry()
                        .textureCoordinates ),
      fbo( 1 )
{
    this->fbo.SetSize( static_cast< uint64_t >( sizeX ),
                       static_cast< uint64_t >( sizeY ), 0 );
    if ( outputTextureProperties.empty() )
    {
        this->create_isInitialized = false;
        this->create_error         = elShaderTexture_Error::ZeroOutputTextures;
        LOG_ERROR( 0 )
            << "shader texture with zero output textures is not allowed";
        return;
    }

    for ( auto &p : outputTextureProperties )
    {
        this->outputTextures.emplace_back(
            std::make_unique< OpenGL::elTexture_2D >( p.identifier, sizeX,
                                                      sizeY, p.internalFormat,
                                                      p.format, p.type ) );
        this->outputTextures.back()->SetWrap( p.wrapS, p.wrapT );
    }

    // generate shader code
    std::string shaderSource;
    shaderSource += "#version 440\n";
    shaderSource += "in vec3 coord;\n";
    if ( enableTextureCoordinates )
    {
        shaderSource += "in vec2 texCoord;\n";
        shaderSource += "out vec2 _texCoord;\n";
    }
    shaderSource += "void main(void) {\n";
    shaderSource += "gl_Position = vec4(coord, 1.0);\n";
    if ( enableTextureCoordinates ) shaderSource += "_texCoord = texCoord;\n";
    shaderSource += "}\n";

    // create shader
    auto shaderProgram = OpenGL::elShaderProgram::Create(
        OpenGL::shaderFromSource, shaderSource, fragmentShaderCode );

    if ( shaderProgram.HasError() )
    {
        this->create_isInitialized = false;
        this->create_error = elShaderTexture_Error::UnableToCreateShader;
        LOG_ERROR( 0 ) << "unable to create shader for shader texture";
        return;
    }

    this->shaderInternal = std::move( shaderProgram.GetValue() );
    this->shader         = this->shaderInternal.get();

    if ( this->shader->GetAttribute( "coord", 3 )
             .AndThen( [&]( const auto &r ) { this->coordAttrib = r; } )
             .HasError() )
    {
        this->create_isInitialized = false;
        this->create_error =
            elShaderTexture_Error::AttributeForCoordinatesNotFound;
        LOG_ERROR( 0 ) << "attribute \"coord\" not found in shadertexture";
        return;
    }

    if ( enableTextureCoordinates )
    {
        if ( this->shader->GetAttribute( "texCoord", 2 )
                 .AndThen( [&]( const auto &r ) { this->texCoordAttrib = r; } )
                 .HasError() )
        {
            this->create_isInitialized = false;
            this->create_error =
                elShaderTexture_Error::AttributeForTextureCoordinatesNotFound;
            LOG_ERROR( 0 )
                << "attribute \"texCoord\" not found in shadertexture";
            return;
        }
    }

    if ( isFlipFlop )
        this->flipFlopTexture =
            std::unique_ptr< elShaderTexture >( new elShaderTexture(
                this->shader, outputTextureProperties, sizeX, sizeY ) );

    this->CreateOutputTextures();

    if ( isFlipFlop )
        for ( size_t i = 0, limit = this->outputTextures.size(); i < limit;
              ++i )
        {
            if ( this->shader
                     ->GetUniformLocation( "backbuffer" + std::to_string( i ) )
                     .AndThen( [&]( const auto &r )
                               { this->backbufferUniforms[i] = r; } )
                     .HasError() )
            {
                this->create_isInitialized = false;
                this->create_error =
                    elShaderTexture_Error::UnableToFindBackbufferUniform;
                LOG_ERROR( 0 )
                    << "unable to find uniform location for backbuffer "
                       "\"backbuffer"
                    << i << "\"";
                return;
            }
        }

    this->create_isInitialized = true;
}

elShaderTexture::elShaderTexture(
    OpenGL::elShaderProgram                         *shader,
    const std::vector< OpenGL::textureProperties_t > outputTextureProperties,
    const GLsizei sizeX, const GLsizei sizeY )
    : objectBuffer( 3, 0, 0 ), fbo( 1 ), shader( shader )
{
    this->fbo.SetSize( static_cast< uint64_t >( sizeX ),
                       static_cast< uint64_t >( sizeY ), 0 );
    this->create_isInitialized = true;
    for ( auto &p : outputTextureProperties )
        this->outputTextures.emplace_back(
            std::make_unique< OpenGL::elTexture_2D >( p.identifier, sizeX,
                                                      sizeY, p.internalFormat,
                                                      p.format, p.type ) );
    this->CreateOutputTextures();
}

void
elShaderTexture::SetPreDrawFunction(
    const std::function< void() > &func ) noexcept
{
    this->preDrawCall = func;
}

void
elShaderTexture::SetPostDrawFunction(
    const std::function< void() > &func ) noexcept
{
    this->postDrawCall = func;
}

void
elShaderTexture::CreateOutputTextures() noexcept
{
    for ( size_t i = 0, limit = this->outputTextures.size(); i < limit; ++i )
    {
        this->fbo.AddFramebufferTexture(
            this->outputTextures[i].get(),
            static_cast< GLenum >( GL_COLOR_ATTACHMENT0 + i ), 0 );
    }
    this->fbo.Clear();
}

void
elShaderTexture::Refresh() const noexcept
{
    if ( this->preDrawCall ) this->preDrawCall();
    this->PrepareDraw();
    if ( this->postDrawCall ) this->postDrawCall();
}

void
elShaderTexture::PrepareDraw() const noexcept
{
    // refresh input shader Textures
    for ( auto tex : this->inputShaderTextures )
        tex.texture->Refresh();

    this->fbo.Bind( 0 );
    this->shader->Bind();

    for ( auto &u : this->uniforms )
        u.setter( this->shader, u.uniformID );

    this->objectBuffer.Bind();
    this->objectBuffer.Enable( { BUFFER_VERTEX, BUFFER_ELEMENT },
                               this->coordAttrib );
    this->objectBuffer.Enable( { BUFFER_TEXTURE_COORDINATES },
                               this->texCoordAttrib );

    size_t k = 0;
    // activate backbuffer textures
    if ( this->flipFlopTexture )
    {
        const std::vector< std::unique_ptr< OpenGL::elTexture_2D > > *texVec;
        if ( !this->doFlip )
            texVec = &this->flipFlopTexture->outputTextures;
        else
            texVec = &this->outputTextures;

        for ( size_t i = 0, limit = texVec->size(); i < limit; ++i, ++k )
            ( *texVec )[i]->BindActivateAndSetUniform(
                backbufferUniforms[i],
                static_cast< GLenum >( GL_TEXTURE0 + k ) );
    }

    // activate input textures
    for ( auto tex : this->inputTextures )
    {
        tex.texture->BindActivateAndSetUniform(
            tex.uniformID, static_cast< GLenum >( GL_TEXTURE0 + k ) );
        ++k;
    }

    // activate input shaderTextures
    for ( auto tex : this->inputShaderTextures )
    {
        if ( !tex.texture->flipFlopTexture )
        {
            tex.texture->outputTextures[tex.textureID]
                ->BindActivateAndSetUniform(
                    tex.uniformID, static_cast< GLenum >( GL_TEXTURE0 + k ) );
        }
        else
        {
            if ( ( this->doFlip && !tex.isFlipped ) ||
                 ( !this->doFlip && !tex.isFlipped ) )
            {
                tex.texture->flipFlopTexture->outputTextures[tex.textureID]
                    ->BindActivateAndSetUniform(
                        tex.uniformID,
                        static_cast< GLenum >( GL_TEXTURE0 + k ) );
            }
            else
            {
                tex.texture->outputTextures[tex.textureID]
                    ->BindActivateAndSetUniform(
                        tex.uniformID,
                        static_cast< GLenum >( GL_TEXTURE0 + k ) );
            }
        }
        ++k;
    }

    if ( this->blending.enable )
    {
        OpenGL::Enable( GL_BLEND );
        OpenGL::gl( glBlendEquation, this->blending.blendEquation );
        OpenGL::gl( glBlendFunc, this->blending.blendFuncSFactor,
                    this->blending.blendFuncDFactor );
    }
    else
    {
        OpenGL::Disable( GL_BLEND );
    }

    for ( size_t i = 0; i < this->drawingRepetition; ++i )
    {
        if ( this->flipFlopTexture && this->doFlip )
            this->flipFlopTexture->DrawQuad();
        else
            this->DrawQuad();
        this->doFlip = !this->doFlip;
    }

    this->objectBuffer.Disable( { this->coordAttrib, this->texCoordAttrib } );
    this->fbo.Unbind();
}

void
elShaderTexture::DrawQuad() const noexcept
{
    if ( this->doClearBeforeDraw )
    {
        OpenGL::gl( glClearColor, 0.0f, 0.0f, 0.0f, 0.0f );
        OpenGL::gl( glClear, static_cast< GLbitfield >( GL_COLOR_BUFFER_BIT |
                                                        GL_DEPTH_BUFFER_BIT ) );
    }

    OpenGL::gl( glDrawElements, static_cast< GLenum >( GL_TRIANGLES ), 6,
                static_cast< GLenum >( GL_UNSIGNED_INT ),
                static_cast< GLvoid * >( nullptr ) );
}

void
elShaderTexture::RemoveInputTexture( const OpenGL::elTexture_base *v ) noexcept
{
    auto iter = bb::find_if( this->inputTextures,
                             [&]( auto &r ) { return r.texture == v; } );
    if ( iter != this->inputTextures.end() ) this->inputTextures.erase( iter );
}

bool
elShaderTexture::AddInputTexture( const std::string            &uniform,
                                  const OpenGL::elTexture_base *tex ) noexcept
{
    if ( this->shader->HasUniform( uniform ) )
    {
        if (
            this->shader->GetUniformLocation( uniform )
                .AndThen(
                    [&]( const auto &r ) {
                        this->inputTextures.emplace_back( texture_t{ r, tex } );
                    } )
                .HasError() )
        {
            LOG_WARN( 0 )
                << "unable to get texture uniform location of" << uniform
                << " even though the uniform is present in the shader texture  "
                << this->shader->GetFileAndGroupName();
            return false;
        }

        return true;
    }
    else
    {
        LOG_WARN( 0 ) << "unable to add texture to shader texture since the "
                         "texture uniform "
                      << uniform << " is not present "
                      << this->shader->GetFileAndGroupName();
    }

    return false;
}

bool
elShaderTexture::AddInputShaderTexture( const std::string     &uniform,
                                        const elShaderTexture *texture,
                                        const size_t           textureID,
                                        const bool isFlipped ) noexcept
{
    if ( this->shader->HasUniform( uniform ) )
    {
        if ( this->shader->GetUniformLocation( uniform )
                 .AndThen(
                     [&]( const auto &r )
                     {
                         this->inputShaderTextures.emplace_back(
                             shaderTexture_t{ r, texture, textureID,
                                              isFlipped } );
                     } )
                 .HasError() )
        {
            LOG_WARN( 0 )
                << "unable to get shader texture uniform location of" << uniform
                << " even though the uniform is present in the shader texture  "
                << this->shader->GetFileAndGroupName();
            return false;
        }

        return true;
    }
    else
    {
        LOG_WARN( 0 )
            << "unable to add shader texture to shader texture since the "
               "texture uniform "
            << uniform << " is not present "
            << this->shader->GetFileAndGroupName();
    }

    return false;
}

void
elShaderTexture::SetDoClearBeforeDraw( const bool b ) noexcept
{
    this->doClearBeforeDraw = b;
    if ( this->flipFlopTexture )
        this->flipFlopTexture->SetDoClearBeforeDraw( b );
}

void
elShaderTexture::Bind( const size_t n ) const noexcept
{
    if ( this->flipFlopTexture && this->doFlip )
        this->flipFlopTexture->outputTextures[n]->Bind();
    else
        this->outputTextures[n]->Bind();
}

void
elShaderTexture::Bind( const size_t n, const GLenum textureUnit ) const noexcept
{
    if ( this->flipFlopTexture && this->doFlip )
        this->flipFlopTexture->outputTextures[n]->BindAndActivate(
            textureUnit );
    else
        this->outputTextures[n]->BindAndActivate( textureUnit );
}

void
elShaderTexture::Bind( const size_t n, const GLint uniform,
                       const GLenum textureUnit ) const noexcept
{
    if ( this->flipFlopTexture && this->doFlip )
        this->flipFlopTexture->outputTextures[n]->BindActivateAndSetUniform(
            uniform, textureUnit );
    else
        this->outputTextures[n]->BindActivateAndSetUniform( uniform,
                                                            textureUnit );
}

OpenGL::elTexture_2D &
elShaderTexture::GetOutputTexture( const size_t i ) noexcept
{
    return *this->outputTextures[i];
}

void
elShaderTexture::SetSize( const uint64_t x, const uint64_t y ) noexcept
{
    this->fbo.SetSize( x, y, 0 );
    if ( this->flipFlopTexture ) this->flipFlopTexture->fbo.SetSize( x, y, 0 );
}

void
elShaderTexture::SetBlending( const bool enable, const GLenum blendEquation,
                              const GLenum blendFuncSFactor,
                              const GLenum blendFuncDFactor ) noexcept
{
    this->blending =
        blending_t{ enable, blendEquation, blendFuncSFactor, blendFuncDFactor };
}

void
elShaderTexture::SetDrawingRepetition( const size_t v ) noexcept
{
    this->drawingRepetition = v;
}

OpenGL::elShaderProgram &
elShaderTexture::GetShader() noexcept
{
    return *shader;
}

bool
elShaderTexture::RegisterUniform(
    const std::string     &uniformName,
    const uniformSetter_t &uniformSetter ) noexcept
{
    bb::Expects( static_cast< bool >( uniformSetter ) );

    if ( !this->shader->HasUniform( uniformName ) )
    {
        LOG_ERROR( 0 ) << "unable to register uniform " << uniformName
                       << " in shaderTexture";
        return false;
    }

    return !this->shader->GetUniformLocation( uniformName )
                .AndThen(
                    [&]( const auto &r ) {
                        this->uniforms.emplace_back(
                            uniform_t{ r, uniformSetter } );
                    } )
                .HasError();
}

bool
elShaderTexture::HasUniform( const std::string &uniformName ) const noexcept
{
    return this->shader->HasUniform( uniformName );
}


} // namespace HighGL
} // namespace el3D
