#include "HighGL/elEventColor.hpp"

namespace el3D
{
namespace HighGL
{
elEventColor::elEventColor( const uint32_t& colorIndex ) noexcept
    : colorIndex{ colorIndex }, color{ elEventColor::ConvertINT2RGBA(
                                    colorIndex ) }
{
}

elEventColor::elEventColor( const glm::vec4& color ) noexcept
    : colorIndex{ elEventColor::ConvertRGBA2INT( color ) }, color{ color }
{
}

uint32_t
elEventColor::GetIndex() const noexcept
{
    return this->colorIndex;
}

glm::vec4
elEventColor::GetRGBAColor() const noexcept
{
    return this->color;
}

glm::vec4
elEventColor::ConvertINT2RGBA( const uint32_t i ) noexcept
{
    uint32_t r = ( i & 0x00FF0000 ) >> 16;
    uint32_t g = ( i & 0x0000FF00 ) >> 8;
    uint32_t b = ( i & 0x000000FF ) >> 0;

    return glm::vec4( static_cast< float >( r ) / 255.0f,
                      static_cast< float >( g ) / 255.0f,
                      static_cast< float >( b ) / 255.0f, 1.0f );
}

uint32_t
elEventColor::ConvertRGBA2INT( const glm::vec4& v ) noexcept
{
    uint32_t r = static_cast< uint32_t >( v.x * 255 ) << 16;
    uint32_t g = static_cast< uint32_t >( v.y * 255 ) << 8;
    uint32_t b = static_cast< uint32_t >( v.z * 255 ) << 0;

    return r | g | b;
}
} // namespace HighGL
} // namespace el3D
