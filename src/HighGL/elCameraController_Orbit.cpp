#include "HighGL/elCameraController_Orbit.hpp"

#include <cmath>

namespace el3D
{
namespace HighGL
{
elCameraController_Orbit::elCameraController_Orbit(
    OpenGL::elCamera_Perspective&         camera,
    const std::function< units::Time() >& getDeltaTime,
    const settings_t&                     s ) noexcept
    : elCameraController_base( elCameraController::Orbit, camera ),
      settings{ s }, getDeltaTime{ getDeltaTime }
{
    this->hasContinuousUpdates = true;
}

glm::vec3
elCameraController_Orbit::GetCenter() const noexcept
{
    return this->settings.center;
}

glm::vec2
elCameraController_Orbit::GetAngle() const noexcept
{
    return this->settings.angle;
}

float
elCameraController_Orbit::GetDistance() const noexcept
{
    return this->settings.distance;
}

void
elCameraController_Orbit::SetCenter( const glm::vec3& v ) noexcept
{
    this->settings.center = v;
}

void
elCameraController_Orbit::SetAngle( const glm::vec2& v ) noexcept
{
    this->settings.angle = v;
}

void
elCameraController_Orbit::SetDistance( const float v ) noexcept
{
    this->settings.distance = v;
}

void
elCameraController_Orbit::SetZoom( const Zoom v ) noexcept
{
    this->zoom = v;
}

void
elCameraController_Orbit::CalculateState() noexcept
{
    float deltaTime = static_cast< float >( this->getDeltaTime().GetSeconds() );
    this->settings.distance += static_cast< float >( this->zoom ) * deltaTime *
                               this->settings.velocity;

    this->state.position.x = this->settings.distance *
                             -std::sin( this->settings.angle.x ) *
                             std::cos( this->settings.angle.y );
    this->state.position.y =
        this->settings.distance * -std::sin( this->settings.angle.y );
    this->state.position.z = this->settings.distance *
                             -std::cos( this->settings.angle.x ) *
                             std::cos( this->settings.angle.y );
    this->state.position += this->settings.center;

    this->state.up     = this->cameraState->up,
    this->state.lookAt = this->settings.center - this->state.position,
    this->state.fov    = this->cameraState->fov;
}
} // namespace HighGL
} // namespace el3D

