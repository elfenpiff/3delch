#include "HighGL/elLight_PointLight.hpp"

#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elGeometricObject_VertexObject.hpp"
#include "OpenGL/elObjectBuffer.hpp"

#include <algorithm>

namespace el3D
{
namespace HighGL
{
elLight_PointLight::elLight_PointLight() noexcept
    : elLight_PointLight( glm::vec3( 1.0f ), glm::vec3( 1.0f ),
                          glm::vec3( 1.0f ), 1.0f, 0.0f )
{
}

elLight_PointLight::elLight_PointLight( const glm::vec3& color,
                                        const glm::vec3& attenuation,
                                        const glm::vec3& position,
                                        const float      diffuseIntensity,
                                        const float ambientIntensity ) noexcept
    : elLight_base( color, diffuseIntensity, ambientIntensity ),
      cube( OpenGL::DrawMode::Triangles,
            elObjectGeometryBuilder::CreateCube().GetInnerObjectGeometry() ),
      attenuation( attenuation ), position( position )
{
    this->cube.SetPosition( this->position );
    this->SetCubeSize();
}

void
elLight_PointLight::UpdateGeometricObjectPosition() noexcept
{
    if ( !this->geometricObject ) return;
    this->geometricObject->SetPosition( this->position );
}

void
elLight_PointLight::SetAttenuation( const glm::vec3& v ) noexcept
{
    this->attenuation        = v;
    this->hasUpdatedSettings = true;
    this->SetCubeSize();
}

void
elLight_PointLight::SetPosition( const glm::vec3& v ) noexcept
{
    this->position = v;
    this->cube.SetPosition( v );
    this->UpdateGeometricObjectPosition();
    this->hasUpdatedSettings = true;
}

void
elLight_PointLight::Bind(
    const OpenGL::elShaderProgram::shaderAttribute_t& a ) noexcept
{
    this->cube.objectBuffer.Bind();
    this->cube.objectBuffer.Enable( { BUFFER_VERTEX, BUFFER_ELEMENT }, a );
}

void
elLight_PointLight::Unbind(
    const OpenGL::elShaderProgram::shaderAttribute_t& a ) noexcept
{
    this->cube.objectBuffer.Disable( { a } );
}

void
elLight_PointLight::SetCubeSize() noexcept
{
    float colorMax =
        fmaxf( fmaxf( this->color.x, this->color.y ), this->color.z );
    this->radius =
        ( -this->attenuation.x +
          sqrtf( fabsf( this->attenuation.y * this->attenuation.y -
                        4.0f * this->attenuation.z *
                            ( this->attenuation.x - 256.0f * colorMax ) *
                            this->diffuseIntensity ) ) ) /
        ( 2.0f * this->attenuation.z );

    this->cube.SetScaling( glm::vec3( this->radius / 2.0f ) );
}

glm::vec3
elLight_PointLight::GetAttenuation() const noexcept
{
    return this->attenuation;
}

glm::vec3
elLight_PointLight::GetPosition() const noexcept
{
    return this->position;
}

float
elLight_PointLight::GetRadius() const noexcept
{
    return this->radius;
}
} // namespace HighGL
} // namespace el3D
