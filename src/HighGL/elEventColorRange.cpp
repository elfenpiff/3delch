#include "HighGL/elEventColorRange.hpp"

#include "HighGL/elEventColor.hpp"

namespace el3D
{
namespace HighGL
{
elEventColorRange::elEventColorRange( const uint32_t colorIndex,
                                      const uint32_t size ) noexcept
    : colorIndex{ colorIndex }, size{ size }
{
}

uint32_t
elEventColorRange::GetSize() const noexcept
{
    return this->size;
}

uint32_t
elEventColorRange::GetColorIndex( const uint64_t index ) const noexcept
{
    return static_cast< uint32_t >( this->colorIndex + index );
}

glm::vec4
elEventColorRange::GetRGBAColor( const uint64_t index ) const noexcept
{
    return elEventColor::ConvertINT2RGBA( this->GetColorIndex( index ) );
}
} // namespace HighGL
} // namespace el3D
