#include "3Delch/elScene.hpp"

#include "3Delch/3Delch.hpp"
#include "GLAPI/sdlHelper.hpp"
#include "HighGL/elCameraController_FreeFly.hpp"
#include "HighGL/elCameraController_Orbit.hpp"
#include "OpenGL/elGLSLParser.hpp"
#include "OpenGL/elGeometricObject_base.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "RenderPipeline/elRenderer_Forward.hpp"
#include "lua/elConfigHandler.hpp"
#include "utils/elFileSystem.hpp"

namespace el3D
{
namespace _3Delch
{
elScene::elScene(
    const std::string& name, const lua::elConfigHandler* const config,
    GLAPI::elWindow* const                         window,
    RenderPipeline::elRenderPipelineManager* const pipelineManager,
    GLAPI::elFontCache* const                      fontCache ) noexcept
    : sceneName( name ), config( config ), pipelineManager( pipelineManager ),
      cleanupPipeline(
          [] {},
          [&] { this->pipelineManager->RemovePipeline( this->pipelineID ); } ),
      eventHandler( window->GetEventHandler() )
{
    auto pipelineMgmt = this->pipelineManager->CreatePipeline(
        this->sceneName, fontCache, this->eventHandler );

    this->pipelineID = pipelineMgmt.index();
    this->pipeline   = pipelineMgmt.value().get();
    this->GetRenderer< OpenGL::RenderTarget::Forward >().renderer =
        this->pipeline->GetForwardRenderer();
    this->GetRenderer< OpenGL::RenderTarget::Deferred >().renderer =
        this->pipeline->GetDeferredRenderer();
}

void
elScene::UseSkyboxForReflections( const bool v ) noexcept
{
    this->pipeline->UseSkyboxForReflection( v );
}

bb::product_ptr< HighGL::elEventColor >
elScene::AcquireEventColor() noexcept
{
    return this->pipelineManager->AcquireEventColor();
}

bb::product_ptr< HighGL::elEventColorRange >
elScene::AcquireEventColorRange( const uint32_t size ) noexcept
{
    return this->pipelineManager->AcquireEventColorRange( size );
}

const std::string&
elScene::GetSceneName() const noexcept
{
    return this->sceneName;
}

const OpenGL::elCamera_Perspective&
elScene::GetCamera() const noexcept
{
    return this->pipeline->GetCamera();
}

OpenGL::elCamera_Perspective&
elScene::GetCamera() noexcept
{
    return this->pipeline->GetCamera();
}

bool
elScene::HasStaticCamera() const noexcept
{
    return !( this->cameraEvent && this->cameraEvent->IsRegistered() );
}

uint32_t
elScene::GetCurrentEventColorIndex() const noexcept
{
    return this->pipelineManager->GetCurrentEventColorIndex();
}

void
elScene::EnableInteractiveCamera() noexcept
{
    if ( this->cameraEvent ) this->cameraEvent->Register();
}

void
elScene::DisableInteractiveCamera() noexcept
{
    if ( this->cameraEvent ) this->cameraEvent->Unregister();
}

template < typename T, typename... Targs >
bool
elScene::SetupCameraController( const std::string& source,
                                Targs&&... args ) noexcept
{
    this->cameraEvent      = CreateEvent();
    this->cameraController = std::make_unique< T >(
        this->GetCamera(), std::forward< Targs >( args )... );
    this->cameraControllerState = cameraControllerState_t();

    if ( !this->cameraController->IsAttachedToCamera() )
    {
        this->cameraController.reset();
        LOG_ERROR( 0 ) << "Unable to " << source
                       << " since someone else is "
                          "already attached to the camera";
        return false;
    }
    return true;
}

bool
elScene::SetupOrbitCamera( const glm::vec3 center, const float distance,
                           const glm::vec2& angle ) noexcept
{
    bool hasPerformedControllerSetup = false;

    float cameraSensitivity = this->config->Get< float >(
        { "global", "camera", "sensitivity" }, { 1.0f } )[0];
    float cameraSpeed = this->config->Get< float >(
        { "global", "camera", "speedInMeterPerSecond" }, { 1.0f } )[0];

    if ( !this->cameraController || this->cameraController->GetType() !=
                                        HighGL::elCameraController::Orbit )
    {
        if ( !this->SetupCameraController< HighGL::elCameraController_Orbit >(
                 "SetupOrbitCamera", GetDeltaTime,
                 HighGL::elCameraController_Orbit::settings_t{
                     .center      = center,
                     .angle       = angle,
                     .distance    = distance,
                     .sensitivity = cameraSensitivity,
                     .velocity    = cameraSpeed } ) )
            return false;


        hasPerformedControllerSetup = true;
    }

    if ( hasPerformedControllerSetup )
    {
        SDL_Keycode toggleRotate =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "orbitKeys", "toggleRotate" },
                { "g" } )[0] );
        SDL_Keycode zoomIn =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "orbitKeys", "zoomIn" }, { "r" } )[0] );
        SDL_Keycode zoomOut =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "orbitKeys", "zoomOut" }, { "f" } )[0] );

        this->cameraEvent->SetCallback(
            [=, this]( const SDL_Event e )
            {
                HighGL::elCameraController_Orbit* c =
                    dynamic_cast< HighGL::elCameraController_Orbit* >(
                        this->cameraController.get() );

                float     distance = c->GetDistance();
                glm::vec2 angle    = c->GetAngle();

                if ( e.type == SDL_KEYDOWN )
                {
                    if ( e.key.keysym.sym == zoomIn )
                        c->SetZoom(
                            HighGL::elCameraController_Orbit::Zoom::In );
                    else if ( e.key.keysym.sym == zoomOut )
                        c->SetZoom(
                            HighGL::elCameraController_Orbit::Zoom::Out );
                    else if ( e.key.keysym.sym == toggleRotate )
                        this->cameraControllerState.isInRotationMode = true;
                }
                else if ( e.type == SDL_KEYUP )
                {
                    if ( e.key.keysym.sym == zoomIn )
                        c->SetZoom(
                            HighGL::elCameraController_Orbit::Zoom::Stop );
                    else if ( e.key.keysym.sym == zoomOut )
                        c->SetZoom(
                            HighGL::elCameraController_Orbit::Zoom::Stop );
                    else if ( e.key.keysym.sym == toggleRotate )
                        this->cameraControllerState.isInRotationMode = false;
                }

                if ( e.type == SDL_MOUSEWHEEL )
                    distance += static_cast< float >( e.wheel.y ) * cameraSpeed;

                constexpr float MOUSE_SPEED_COMPENSATION = 0.0625f;
                if ( e.type == SDL_MOUSEMOTION &&
                     ( this->cameraControllerState.isInRotationMode ||
                       e.button.button == SDL_BUTTON_MIDDLE ) )
                    angle += this->GUI()
                                 ->GetMouseCursor()
                                 ->GetRelativeCursorPosition() *
                             MOUSE_SPEED_COMPENSATION * cameraSensitivity;

                c->SetDistance( distance );
                c->SetAngle( angle );
            } );
    }
    this->cameraEvent->Register();

    return true;
}

bool
elScene::SetupFreeFlyCamera() noexcept
{
    bool hasPerformedControllerSetup = false;
    if ( !this->cameraController || this->cameraController->GetType() !=
                                        HighGL::elCameraController::FreeFly )
    {
        if ( !this->SetupCameraController< HighGL::elCameraController_FreeFly >(
                 "SetupFreeFlyCamera", GetDeltaTime,
                 HighGL::elCameraController_FreeFly::settings_t{} ) )
            return false;

        HighGL::elCameraController_FreeFly* c =
            dynamic_cast< HighGL::elCameraController_FreeFly* >(
                this->cameraController.get() );

        float cameraSensitivity = this->config->Get< float >(
            { "global", "camera", "sensitivity" }, { 1.0f } )[0];
        auto cameraSpeed = this->config->Get< float >(
            { "global", "camera", "speedInMeterPerSecond" }, { 1.0f } )[0];

        c->SetVelocity( cameraSpeed );
        c->SetSensitivity( cameraSensitivity );
        hasPerformedControllerSetup = true;
    }

    if ( hasPerformedControllerSetup )
    {
        SDL_Keycode keyForward =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "forward" },
                { "w" } )[0] );
        SDL_Keycode keyBackward =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "backward" },
                { "s" } )[0] );
        SDL_Keycode keyLeft =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "left" }, { "a" } )[0] );
        SDL_Keycode keyRight =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "right" }, { "d" } )[0] );
        SDL_Keycode keyUp =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "up" }, { "r" } )[0] );
        SDL_Keycode keyDown =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "down" }, { "f" } )[0] );
        SDL_Keycode keyRollLeft =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "rollLeft" },
                { "f" } )[0] );
        SDL_Keycode keyRollRight =
            GLAPI::StringToSDLKeyCode( this->config->Get< std::string >(
                { "global", "camera", "freeFlyKeys", "rollRight" },
                { "f" } )[0] );

        this->cameraEvent->SetCallback(
            [=, this]( const SDL_Event e )
            {
                HighGL::elCameraController_FreeFly* c =
                    dynamic_cast< HighGL::elCameraController_FreeFly* >(
                        this->cameraController.get() );

                glm::vec3 movement = c->GetSettings().movementDirection;
                glm::vec3 rotation = { 0.0f, 0.0f,
                                       c->GetSettings().rotationDirection.z };
                glm::vec3 rotationStep( 0.0f );

                auto addKeyBinding = [&]( const SDL_Keycode key,
                                          float&            destination,
                                          const float       value )
                {
                    if ( e.type == SDL_KEYDOWN && e.key.keysym.sym == key )
                        destination = value;
                    else if ( e.type == SDL_KEYUP && e.key.keysym.sym == key )
                        destination = 0.0f;
                };

                addKeyBinding( keyForward, movement.z, 1.0f );
                addKeyBinding( keyBackward, movement.z, -1.0f );
                addKeyBinding( keyLeft, movement.x, -1.0f );
                addKeyBinding( keyRight, movement.x, 1.0f );
                addKeyBinding( keyUp, movement.y, 1.0f );
                addKeyBinding( keyDown, movement.y, -1.0f );
                addKeyBinding( keyRollLeft, rotation.z, -1.0f );
                addKeyBinding( keyRollRight, rotation.z, 1.0f );

                if ( e.type == SDL_MOUSEMOTION )
                {
                    constexpr float MOUSE_SPEED_COMPENSATION = 5.0f;
                    rotationStep.x = static_cast< float >( -e.motion.yrel );
                    rotationStep.y = static_cast< float >( e.motion.xrel );
                    rotationStep *= MOUSE_SPEED_COMPENSATION;
                }

                c->SetRotationStep( rotationStep );
                c->SetRotationDirection( rotation );
                c->SetMovementDirection( movement );
            } );
    }
    this->cameraEvent->Register();
    return true;
}

RenderPipeline::elRenderer_GUI*
elScene::GUI() noexcept
{
    return this->pipeline->GetGUIRenderer();
}

bb::product_ptr< HighGL::elLight_PointLight >
elScene::CreatePointLight( const glm::vec3& color, const glm::vec3& attenuation,
                           const glm::vec3& position,
                           const float      diffuseIntensity,
                           const float      ambientIntensity ) noexcept
{
    return this->pipeline->CreatePointLight(
        color, attenuation, position, diffuseIntensity, ambientIntensity );
}

bb::product_ptr< HighGL::elLight_DirectionalLight >
elScene::CreateDirectionalLight( const glm::vec3& color,
                                 const glm::vec3& direction,
                                 const GLfloat    diffuseIntensity,
                                 const GLfloat    ambientIntensity ) noexcept
{
    return this->pipeline->CreateDirectionalLight(
        color, direction, diffuseIntensity, ambientIntensity );
}

size_t
elScene::RegisterShaderTexture(
    const HighGL::elShaderTexture* const v ) noexcept
{
    return this->pipeline->RegisterShaderTexture( v );
}

void
elScene::DeregisterShaderTexture( const size_t id ) noexcept
{
    this->pipeline->DeregisterShaderTexture( id );
}

void
elScene::SetLengthOfOneMeter( const float v ) noexcept
{
    this->GetRenderer< OpenGL::RenderTarget::Deferred >()
        .renderer->SetLengthOfOneMeter( v );
}


} // namespace _3Delch
} // namespace el3D
