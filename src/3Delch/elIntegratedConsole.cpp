#include "3Delch/elIntegratedConsole.hpp"

#include "3Delch/3Delch.hpp"
#include "GuiGL/common.hpp"
#include "OpenGL/elObjectRegistry.hpp"
#include "units/DataSize.hpp"

namespace el3D
{
namespace _3Delch
{
elIntegratedConsole::elIntegratedConsole(
    elScene* const scene, GLAPI::elEventHandler* const eventHandler ) noexcept
    : scene( scene ), parent( scene->GUI()->GetRoot() ),
      eventHandler( eventHandler )
{
    this->mainWindow =
        this->parent->Create< GuiGL::elWidget_base >( CLASS_NAME );
    this->mainWindow->SetDoRenderWidget( this->isConsoleActive );
    this->mainWindowSize = this->mainWindow->GetSize();

    this->selector = this->mainWindow->Create< GuiGL::elWidget_Dropbutton >();
    this->selector->AddElements( { "general", "textures" } );
    this->selector->SetIsAlwaysOnTop( true );
    this->selector->SetCallbackOnSelection(
        [&]
        {
            this->activeWindow->SetDoRenderWidget( false );
            switch ( this->selector->GetSelection() )
            {
                case 0:
                    this->general.window->SetDoRenderWidget( true );
                    this->activeWindow = this->general.window.Get();
                    break;
                case 1:
                    this->textures.window->SetDoRenderWidget( true );
                    this->activeWindow = this->textures.window.Get();
                    break;
            }
        } );

    this->freeCamera = this->mainWindow->Create< GuiGL::elWidget_Button >();
    this->freeCamera->SetText( "free camera" );
    this->freeCamera->SetIsAlwaysOnTop( true );
    this->freeCamera->SetPosition( { this->selector->GetSize().x + 1,
                                     this->freeCamera->GetPosition().y } );
    this->freeCamera->SetClickCallback(
        [&]
        {
            if ( this->scene->HasStaticCamera() )
            {
                this->scene->EnableInteractiveCamera();
                this->freeCamera->SetText( "static camera" );
            }
            else
            {
                this->scene->DisableInteractiveCamera();
                this->freeCamera->SetText( "free camera" );
            }
        } );

    auto setupWindow = [&]( GuiGL::elWidget_Window_ptr& window )
    {
        window = this->mainWindow->Create< GuiGL::elWidget_Window >();
        for ( uint64_t state = 0; state < GuiGL::STATE_END; ++state )
        {
            window->SetBaseColorForState(
                { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( state ) );
            window->SetAccentColorForState(
                { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( state ) );
            window->SetBaseGlowColorForState(
                { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( state ) );
            window->SetAccentGlowColorForState(
                { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( state ) );
        }

        window->SetBorderSize( { 0, 0, 0, 0 } );
        window->SetIsResizable( false );
        window->SetIsMovable( false );
        window->SetStickToParentSize( { 0, 0 } );
        window->SetIsScrollbarEnabled( false, false, false, true );
    };

    setupWindow( this->general.window );
    this->activeWindow = this->general.window.Get();

    setupWindow( this->textures.window );
    this->textures.window->SetDoRenderWidget( false );
    this->textures.window->SetIsScrollbarEnabled( false, false, false, false );

    // setup general window
    this->general.fps =
        this->general.window->Create< GuiGL::elWidget_KeyValuePair >();
    this->general.fps->SetKeyText( "fps" );
    this->general.fps->SetValueOffset( { general_t::VALUE_OFFSET, 0.0f } );
    this->general.fps->SetPosition( { 10, this->selector->GetSize().y + 10 } );

    this->CreateKeyValuePair( this->general.position, this->general.fps,
                              general_t::VALUE_OFFSET, "position" );
    this->CreateKeyValuePair( this->general.lookAt, this->general.position,
                              general_t::VALUE_OFFSET, "lookAt" );
    this->CreateKeyValuePair( this->general.zNearFar, this->general.lookAt,
                              general_t::VALUE_OFFSET, "zNear / zFar" );
    this->CreateKeyValuePair( this->general.fov, this->general.zNearFar,
                              general_t::VALUE_OFFSET, "fov" );
    this->CreateKeyValuePair( this->general.viewPortSize, this->general.fov,
                              general_t::VALUE_OFFSET, "viewPortSize" );
    this->CreateKeyValuePair( this->general.dpiScaling,
                              this->general.viewPortSize,
                              general_t::VALUE_OFFSET, "dpiScaling" );

    this->CreateKeyValuePair( this->general.processID, this->general.dpiScaling,
                              general_t::VALUE_OFFSET, "process id" );
    this->general.processID->SetPosition(
        this->general.processID->GetPosition() + glm::vec2( 0.0, 10.0 ) );
    this->CreateKeyValuePair( this->general.threads, this->general.processID,
                              general_t::VALUE_OFFSET, "threads" );
    this->CreateKeyValuePair( this->general.cpuUsage, this->general.threads,
                              general_t::VALUE_OFFSET, "cpu usage" );
    this->CreateKeyValuePair( this->general.memoryUsage, this->general.cpuUsage,
                              general_t::VALUE_OFFSET, "memory usage" );

    // general cpu col
    this->general.processor =
        this->general.window->Create< GuiGL::elWidget_KeyValuePair >();
    this->general.processor->SetKeyText( "cpu" );
    this->general.processor->SetValueOffset(
        { general_t::VALUE_OFFSET / 2.0f, 0.0f } );
    this->general.processor->SetPosition(
        { 470, this->general.fps->GetPosition().y } );

    // general memory col
    this->general.memoryTotal =
        this->general.window->Create< GuiGL::elWidget_KeyValuePair >();
    this->general.memoryTotal->SetKeyText( "memory total" );
    this->general.memoryTotal->SetValueOffset(
        { general_t::VALUE_OFFSET, 0.0f } );
    this->general.memoryTotal->SetPosition(
        { 910, this->general.fps->GetPosition().y } );

    this->CreateKeyValuePair( this->general.memoryUsed,
                              this->general.memoryTotal,
                              general_t::VALUE_OFFSET, "memory used" );


    // setup texture browser
    this->textures.textureList =
        this->textures.window->Create< GuiGL::elWidget_Listbox >();
    this->textures.textureList->SetSize( { 400, 0 } );
    this->textures.textureList->SetPositionPointOfOrigin(
        GuiGL::POSITION_BOTTOM_LEFT );
    this->textures.textureList->SetStickToParentSize(
        { -1, this->selector->GetSize().y } );
    this->textures.textureList->SetTextAlignment( GuiGL::POSITION_CENTER_LEFT,
                                                  glm::vec2( 10.0, 0.0 ) );
    this->textures.textureList->SetElementSelectionCallback(
        [&]
        {
            auto selection = this->textures.textureList->GetSelectedWidgets();
            if ( selection.empty() ) return;

            uint64_t id =
                this->textures.registeredTextures.size() - selection[0] - 1;
            auto& tex = this->textures.registeredTextures[id];
            this->textures.previewTexture.emplace( tex.first, tex.second );
            this->textures.preview->SetImageFromTexture(
                &*textures.previewTexture );
            this->textures.textureType->SetText(
                OpenGL::EnumToString( tex.second ) );
        } );

    this->textures.preview =
        this->textures.window->Create< GuiGL::elWidget_Image >();
    this->textures.preview->SetPositionPointOfOrigin(
        GuiGL::POSITION_BOTTOM_RIGHT );
    this->textures.preview->SetStickToParentSize(
        { this->textures.textureList->GetSize().x,
          this->selector->GetSize().y } );
    this->textures.preview->SetDoInvertTextureCoordinates( true );
    this->textures.preview->SetBorderSize( { 0, 0, 0, 0 } );
    this->textures.preview->SetIsMovable( false );
    this->textures.preview->SetIsResizable( false );

    this->textures.footer =
        this->textures.preview->Create< GuiGL::elWidget_base >();
    this->textures.footer->SetDisableEventHandling( true );
    this->textures.footer->SetBaseColorForState( { 0.0, 0.0, 0.0, 0.4 },
                                                 GuiGL::STATE_DEFAULT );
    this->textures.footer->SetStickToParentSize( { 0, -1 } );
    this->textures.footer->SetPositionPointOfOrigin(
        GuiGL::POSITION_BOTTOM_LEFT );
    this->textures.footer->SetSize( { 0, 30 } );
    this->textures.textureType =
        this->textures.footer->Create< GuiGL::elWidget_Label >();
    this->textures.textureType->SetPosition( { 10, 5 } );


    // setup callbacks and events
    this->callback =
        GetInstance().CreateMainLoopCallback( [&] { this->RefreshValues(); } );

    this->showEvent = this->eventHandler->CreateEvent();
    this->showEvent->SetCallback(
        [this]( const SDL_Event e )
        {
            if ( !( e.key.type == SDL_KEYDOWN &&
                    e.key.keysym.sym == SDLK_F12 ) )
                return;

            this->isConsoleActive = !this->isConsoleActive;
            if ( this->isConsoleActive )
            {
                this->mainWindow->SetDoRenderWidget( true );
                this->mainWindow->ResizeTo( this->mainWindowSize );
                this->hasStaticCamera = this->scene->HasStaticCamera();
                this->hasMouseCursor =
                    this->scene->GUI()->DoesShowMouseCursor();

                this->scene->GUI()->SetShowMouseCursor( true );
                this->scene->DisableInteractiveCamera();
                this->freeCamera->SetText( "free camera" );
            }
            else
            {
                this->mainWindow->ResizeTo(
                    { this->mainWindowSize.x, 0.0f },
                    [&] { this->mainWindow->SetDoRenderWidget( false ); } );
                if ( !this->hasStaticCamera )
                    this->scene->EnableInteractiveCamera();
                this->scene->GUI()->SetShowMouseCursor( this->hasMouseCursor );
            }
        } );
    this->showEvent->Register();
}

void
elIntegratedConsole::CreateKeyValuePair(
    GuiGL::elWidget_KeyValuePair_ptr&       store,
    const GuiGL::elWidget_KeyValuePair_ptr& previous, const float valueOffset,
    const std::string& key ) noexcept
{
    store = this->general.window->Create< GuiGL::elWidget_KeyValuePair >();
    store->SetKeyText( key );
    store->SetValueOffset( { valueOffset, 0.0f } );
    store->SetPosition(
        { previous->GetPosition().x,
          previous->GetPosition().y +
              static_cast< float >( previous->GetFontHeight() ) } );
}

void
elIntegratedConsole::RefreshValues() noexcept
{
    if ( !this->isConsoleActive ) return;

    // refresh general
    auto& instance = GetInstance();
    this->general.fps->SetValueText(
        std::to_string( instance.Window()->GetFramesPerSecond() ) );

    auto& camera = instance.GetSceneManager().GetMainScene().GetCamera();

    auto& position = camera.GetState().position;
    this->general.position->SetValueText( std::to_string( position.x ) + ", " +
                                          std::to_string( position.y ) + ", " +
                                          std::to_string( position.z ) );

    auto& lookAt = camera.GetState().lookAt;
    this->general.lookAt->SetValueText( std::to_string( lookAt.x ) + ", " +
                                        std::to_string( lookAt.y ) + ", " +
                                        std::to_string( lookAt.z ) );

    std::stringstream ss;
    ss << camera.GetZFactor().zNear << " / " << camera.GetZFactor().zFar;

    this->general.zNearFar->SetValueText( ss.str() );


    ss.str( "" );
    ss << camera.GetState().fov;
    this->general.fov->SetValueText( ss.str() );

    glm::ivec2 viewPortSize = camera.GetViewPortSize();
    this->general.viewPortSize->SetValueText(
        std::to_string( viewPortSize.x ) + " x " +
        std::to_string( viewPortSize.y ) );

    this->general.dpiScaling->SetValueText(
        std::to_string( GuiGL::GetDPIScaling() ) );

    // refresh textures
    auto registry = OpenGL::elObjectRegistry::GetInstance();
    if ( registry->GetLastUpdateToken() !=
         this->textures.lastTextureUpdateToken )
    {
        this->textures.cache = registry->GetRegisteredTextures();

        this->textures.registeredTextures.clear();
        this->textures.textureList->Clear();

        for ( auto& tex : this->textures.cache )
        {
            this->textures.textureList->AddElement( tex.identifier, 0 );
            this->textures.registeredTextures.emplace_back(
                std::make_pair( tex.texture, tex.target ) );
        }
        this->textures.lastTextureUpdateToken = registry->GetLastUpdateToken();
    }

    if ( this->systemInfo.GetCurrentIteration() != this->systemInfoIteration )
    {
        this->systemInfoIteration = this->systemInfo.GetCurrentIteration();

        auto sysInfo = this->systemInfo.GetSystemInfo();
        this->general.processID->SetValueText(
            std::to_string( sysInfo.thisProcess.pid ) );
        this->general.threads->SetValueText(
            std::to_string( sysInfo.thisProcess.numberOfThreads ) );

        float totalLoad = sysInfo.thisProcess.cpuLoad;
        auto  threads   = sysInfo.thisProcess.threads;
        for ( auto thread : threads )
            totalLoad += thread.cpuLoad;

        this->general.cpuUsage->SetValueText( std::to_string( totalLoad ) );

        uint64_t memoryUsage = sysInfo.thisProcess.memoryUsage;
        for ( auto thread : threads )
            memoryUsage += thread.memoryUsage;

        ss.str( "" );
        ss << units::DataSize::Bytes(
            static_cast< units::DataSize::float_t >( memoryUsage ) );
        this->general.memoryUsage->SetValueText( ss.str() );


        // refresh general cpu
        this->general.processor->SetValueText(
            std::to_string( static_cast< uint64_t >( sysInfo.cpu.load ) ) );

        auto numberOfCores = sysInfo.cpu.numberOfCores;
        if ( this->general.cores.empty() )
        {
            this->general.cores.reserve( numberOfCores );
            auto* left  = &this->general.processor;
            auto* right = left;
            for ( auto i = 0u; i < numberOfCores; ++i )
            {
                this->general.cores.emplace_back();
                if ( i % 2 == 0 )
                {
                    this->CreateKeyValuePair( this->general.cores.back(), *left,
                                              general_t::VALUE_OFFSET / 2.0f,
                                              "core " +
                                                  std::to_string( i + 1 ) );
                    left = &this->general.cores.back();
                }
                else
                {
                    this->CreateKeyValuePair(
                        this->general.cores.back(), *right,
                        general_t::VALUE_OFFSET / 2.0f,
                        "core " + std::to_string( i + 1 ) );
                    if ( i == 1 )
                        this->general.cores.back()->SetPosition(
                            this->general.cores.back()->GetPosition() +
                            glm::vec2( 220.0, 0.0 ) );
                    right = &this->general.cores.back();
                }
            }
        }

        for ( auto i = 0u; i < numberOfCores; ++i )
            this->general.cores[i]->SetValueText(
                std::to_string(
                    static_cast< uint64_t >( sysInfo.cpu.cores[i].load ) ) +
                " [" +
                std::to_string( static_cast< uint64_t >(
                    sysInfo.cpu.cores[i].frequencyInMhz ) ) +
                "MHz] " );


        // refresh memory
        ss.str( "" );
        auto memoryTotal = units::DataSize::Bytes(
            static_cast< float >( sysInfo.memory.total ) );
        ss << memoryTotal;
        this->general.memoryTotal->SetValueText( ss.str() );

        ss.str( "" );
        auto memoryUsed = units::DataSize::Bytes(
            static_cast< float >( sysInfo.memory.used ) );
        ss << memoryUsed;
        this->general.memoryUsed->SetValueText( ss.str() );
    }
}

} // namespace _3Delch
} // namespace el3D
