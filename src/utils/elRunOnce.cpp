#include "utils/elRunOnce.hpp"

namespace el3D
{
namespace utils
{
elRunOnce::elRunOnce(
    const std::vector< std::function< void() > > &func ) noexcept
    : func( func ), doCallFunc( func.size() ), funcSize( func.size() )
{
    std::fill( this->doCallFunc.begin(), this->doCallFunc.end(), false );
}

void
elRunOnce::Run() noexcept
{
    for ( size_t k = 0; k < this->funcSize; ++k )
    {
        if ( this->doCallFunc[k] )
        {
            this->func[k]();
            this->doCallFunc[k] = false;
        }
    }
}

void
elRunOnce::Call( const size_t i ) noexcept
{
    this->doCallFunc[i] = true;
}

void
elRunOnce::Reset( const size_t i ) noexcept
{
    this->doCallFunc[i] = false;
}
} // namespace utils
} // namespace el3D
