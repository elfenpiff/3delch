#include "utils/byteStream_t.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{

namespace utils
{
byteStream_t::byteStream_t( const stream_t& value ) : stream( value )
{
}

byteStream_t::byteStream_t( stream_t&& value ) : stream( std::move( value ) )
{
}

byteStream_t::byteStream_t( const size_t size, void* const preallocatedMemory )
    : stream( size,
              ( preallocatedMemory != nullptr )
                  ? elAllocator< byte_t >(
                        [=]( uint64_t ) {
                            return static_cast< byte_t* >( preallocatedMemory );
                        },
                        []( byte_t*, uint64_t ) {} )
                  : elAllocator< byte_t >() )
{
}

byteStream_t::byteStream_t( const std::initializer_list< byte_t >& values )
    : stream( values )
{
}

std::string
byteStream_t::GetHumanReadableString( const size_t offset ) const noexcept
{
    std::stringstream ss;
    ss << " [ offset = " << offset << " ]  ";

    for ( auto& v : this->stream )
        ss << "0x" << std::hex << std::setw( 2 ) << std::setfill( '0' )
           << static_cast< uint32_t >( v ) << " ";
    return ss.str();
}

void
byteStream_t::ByteStreamErrorToStderr( const std::string&     source,
                                       const ByteStreamError& error,
                                       const byteStream_t&    stream,
                                       const uint64_t         offset ) noexcept
{
    switch ( error )
    {
        case utils::ByteStreamError::InvalidContent:
        {
            std::cerr << "invalid byte stream content in \"" << source << "\""
                      << std::endl;
            break;
        }
        case utils::ByteStreamError::InvalidSequence:
        {
            std::cerr << "invalid byte stream sequence in \"" << source << "\""
                      << std::endl;
            break;
        }
        case utils::ByteStreamError::LengthInformationMismatch:
        {
            std::cerr << "invalid length information in byte stream in \""
                      << source << "\"" << std::endl;
            break;
        }
        case utils::ByteStreamError::TooShort:
        {
            std::cerr << "byte stream is too short in \"" << source << "\""
                      << std::endl;
            break;
        }
        default:
        {
            std::cerr << "undefined error in byte stream occurred in \""
                      << source << "\"" << std::endl;
            break;
        }
    };

    std::cerr << stream.GetHumanReadableString( offset ) << std::endl;
}
} // namespace utils
} // namespace el3D
