#include "utils/elClassSettings.hpp"

namespace el3D
{
namespace utils
{
elClassSettings::elClassSettings( const std::vector< entry_t >& properties )
{
    for ( auto& v : properties )
        this->data.emplace_back( settingsEntry_t{v.name, v.defaultValue} );
}

elClassSettings::entry_t::entry_t( const std::string& name, const size_t size,
                                   const dataType_t type )
    : name( name ), size( size ), type( type )
{
    switch ( this->type )
    {
        case dataType_t::FLOAT:
            defaultValue = std::vector< float >( this->size );
            break;
        case dataType_t::STRING:
            defaultValue = std::vector< std::string >( this->size );
            break;
        case dataType_t::INT:
            defaultValue = std::vector< int >( this->size );
            break;
    }
}
} // namespace utils
} // namespace el3D
