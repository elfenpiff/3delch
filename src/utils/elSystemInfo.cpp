#include "utils/elSystemInfo.hpp"

#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/elLog.hpp"
#include "utils/elFile.hpp"
#include "utils/elFileSystem.hpp"
#include "utils/elGenericRAII.hpp"


#ifndef _WIN32
#include <arpa/inet.h>
#include <grp.h>
#include <net/if.h>
#include <pwd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>
#endif

#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>


namespace el3D
{
namespace utils
{
elSystemInfo::elSystemInfo( const units::Time intervalTime ) noexcept
    : intervalTime( intervalTime ),
      updateLoopThread( [this] { this->UpdateLoop(); } )
{
    this->systemInfoMutex.lock();
}

elSystemInfo::~elSystemInfo()
{
    this->keepRunning.store( false );
    this->updateLoopThread.join();
}

units::Time
elSystemInfo::GetUpdateIntervalTime() const noexcept
{
    return this->intervalTime;
}

uint64_t
elSystemInfo::GetCurrentIteration() const noexcept
{
    return this->currentIteration;
}

void
elSystemInfo::UpdateLoop() noexcept
{
    while ( this->keepRunning )
    {
        this->Update();

        if ( this->firstCycle )
        {
            this->UpdateOnce();
            this->systemInfo = this->updateThreadSystemInfo;
            this->firstCycle = false;
            this->systemInfoMutex.unlock();
        }
        else
        {
            std::lock_guard< std::mutex > lock( this->systemInfoMutex );
            this->systemInfo = this->updateThreadSystemInfo;
        }

        ++this->currentIteration;
        std::this_thread::sleep_for( std::chrono::milliseconds(
            static_cast< size_t >( this->intervalTime.GetMilliSeconds() ) ) );
    }
}

void
elSystemInfo::Update() noexcept
{
    this->UpdateUptime();
    this->UpdateCPU();
    this->UpdateMemory();
    this->UpdateNetwork();

    this->UpdateNetworkConnections();
    this->UpdateProcesses();
}

void
elSystemInfo::UpdateOnce() noexcept
{
    this->UpdateHostname();
    this->UpdateKernelVersion();
    this->UpdateThisProcessID();
}

void
elSystemInfo::UpdateProcesses() noexcept
{
#ifndef _WIN32
    auto procContent        = elFileSystem::GetDirectoryContents( "/proc/" );
    auto isProcessDirectory = []( const std::string& d ) -> bool
    {
        if ( d.find_first_not_of( "0123456789" ) != std::string::npos )
            return false;
        return true;
    };

    std::map< uint32_t, internal::process_t > internalProcessList;
    this->updateThreadSystemInfo.process.processList.clear();
    for ( auto& dir : procContent )
    {
        if ( dir.type == std::filesystem::file_type::directory )
        {
            if ( isProcessDirectory( dir.path.filename().string() ) &&
                 std::filesystem::exists( dir.path ) )
            {
                // get pid
                internal::process_t newProcess;
                newProcess.pid = static_cast< uint32_t >(
                    std::stoi( dir.path.filename().string(), nullptr, 10 ) );

                // get owner
                struct stat ownerID;
                if ( stat( dir.path.string().c_str(), &ownerID ) == -1 )
                {
                    std::cerr << "unable to get owner of process "
                              << newProcess.pid << std::endl;
                }
                else
                {
                    struct passwd* username = getpwuid( ownerID.st_uid );
                    if ( username != nullptr )
                    {
                        newProcess.user = username->pw_name;
                    }
                    else
                    {
                        newProcess.user = std::to_string( ownerID.st_uid );
                    }

                    struct group* groupname = getgrgid( ownerID.st_gid );
                    if ( groupname != nullptr )
                    {
                        newProcess.group = groupname->gr_name;
                    }
                    else
                    {
                        newProcess.group = std::to_string( ownerID.st_gid );
                    }
                }

                this->ExtractStatProcessInformation( newProcess );

                internalProcessList[newProcess.pid] = newProcess;
                if ( newProcess.pid ==
                     this->updateThreadSystemInfo.thisProcess.pid )
                    this->updateThreadSystemInfo.thisProcess = newProcess;
            }
        }
    }

    this->updateThreadSystemInfo.process.processList =
        this->PutThreadsIntoProcess( internalProcessList );

    this->updateThreadSystemInfo.process.numberOfProcesses =
        static_cast< uint32_t >(
            this->updateThreadSystemInfo.process.processList.size() );
#endif
}

std::vector< internal::process_t >
elSystemInfo::PutThreadsIntoProcess(
    const std::map< uint32_t, internal::process_t >& raw ) const noexcept
{
    std::map< uint32_t, internal::process_t > temp = raw;
    std::vector< internal::process_t >        returnValue;
    auto getParent = [&]( uint32_t pid, const std::string& comm ) -> uint32_t
    {
        while ( true )
        {
            auto iter = temp.find( pid );
            if ( iter == temp.end() ) return pid;
            uint32_t ppid = temp.at( pid ).ppid;
            if ( iter->second.ppid == 0 || iter->second.ppid == 1 ) return pid;
            if ( iter->second.command != comm ) return pid;

            iter = temp.find( ppid );
            if ( iter == temp.end() ) return pid;
            pid = iter->second.pid;
        }
    };

    /// move threads into processes
    for ( auto& p : temp )
    {
        uint32_t parent = getParent( p.first, p.second.command );
        if ( parent != p.first )
        {
            internal::thread_t thread;
            thread.tid         = p.second.pid;
            thread.cpuLoad     = p.second.cpuLoad;
            thread.memoryUsage = p.second.memoryUsage;
            temp[parent].threads.emplace_back( thread );
        }
    }

    /// add processes in vector
    for ( auto& p : temp )
    {
        if ( getParent( p.first, p.second.command ) == p.first )
            returnValue.emplace_back( p.second );
    }

    return returnValue;
}

void
elSystemInfo::ExtractStatProcessInformation(
    internal::process_t& process ) const noexcept
{
#ifndef _WIN32
    for ( auto& line :
          this->ReadFile( "/proc/" + std::to_string( process.pid ) + "/stat",
                          "unable to read status information for process " +
                              std::to_string( process.pid ) + " from " ) )
    {
        char                   command[256];
        int                    ppid;
        long unsigned int      utime, stime;
        long int               cutime, cstime;
        long long unsigned int starttime;
        long unsigned          vsize;
        long int               numberOfThreads;
        const int              numberOfMatchedElements = 9;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
        if ( sscanf(
                 line.c_str(),
                 "%*d %s %*c %d %*d %*d %*d %*d %*u %*lu %*lu %*lu %*lu "
                 "%lu "
                 "%lu "
                 "%ld %ld %*ld %*ld %ld %*ld %llu %lu %*ld %*lu %*lu %*lu "
                 "%*lu "
                 "%*lu %*lu %*lu %*lu %*lu %*lu %*lu %*lu %*lu %*d %*d %*u %*u "
                 "%*llu %*lu %*ld %*lu %*lu %*lu %*lu %*lu %*lu %*lu %*d",
                 command, &ppid, &utime, &stime, &cutime, &cstime,
                 &numberOfThreads, &starttime,
                 &vsize ) != numberOfMatchedElements )
        {
            return;
        }
#pragma GCC diagnostic pop

        /// calculate cpu load
        long double clockTicks =
            static_cast< long double >( sysconf( _SC_CLK_TCK ) );
        long double totalTime = static_cast< long double >(
            utime + stime + static_cast< long unsigned int >( cstime ) );
        long double seconds =
            this->updateThreadSystemInfo.uptime -
            ( static_cast< long double >( starttime ) / clockTicks );

        process.memoryUsage     = vsize;
        process.ppid            = static_cast< uint32_t >( ppid );
        process.numberOfThreads = static_cast< uint32_t >( numberOfThreads );
        process.cpuLoad         = static_cast< float >(
            100.0 * ( totalTime / clockTicks ) / seconds );
        process.command.assign( &( command[1] ) );
        if ( !process.command.empty() )
            process.command.resize( process.command.size() - 1 );
    }
#endif
}

void
elSystemInfo::UpdateHostname() noexcept
{
#ifndef _WIN32
    for ( auto& line :
          this->ReadFile( "/etc/hostname", "unable to read hostname from " ) )
    {
        this->updateThreadSystemInfo.hostname = line;
        return;
    }
#endif
}

void
elSystemInfo::UpdateKernelVersion() noexcept
{
#ifndef _WIN32
    for ( auto& line :
          this->ReadFile( "/proc/version",
                          "unable to read linux version information from " ) )
    {
        std::string temp = line.substr( line.find_first_of( ' ' ) + 1 );
        temp             = temp.substr( temp.find_first_of( ' ' ) + 1 );
        temp             = temp.substr( 0, temp.find_first_of( ' ' ) + 1 );
        this->updateThreadSystemInfo.kernelVersion = temp;
        return;
    }
#endif
}

void
elSystemInfo::UpdateThisProcessID() noexcept
{
    this->updateThreadSystemInfo.thisProcess.pid =
        static_cast< uint32_t >( getpid() );
}

void
elSystemInfo::UpdateCPU() noexcept
{
#ifdef _WIN32
    if ( this->updateThreadSystemInfo.cpu.numberOfCores == 0 )
    {
        SYSTEM_INFO systemInfo;
        GetNativeSystemInfo( &systemInfo );
        this->updateThreadSystemInfo.cpu.numberOfCores =
            static_cast< uint32_t >( systemInfo.dwNumberOfProcessors );

        /// allocate cpu core status memory
        this->updateThreadSystemInfo.cpu.cores.resize(
            this->updateThreadSystemInfo.cpu.numberOfCores );
        this->cpuDeltaTimes.resize(
            this->updateThreadSystemInfo.cpu.numberOfCores + 1 );
    }
#endif

#ifndef _WIN32
    if ( this->updateThreadSystemInfo.cpu.numberOfCores == 0 )
    {
        auto fileContent = this->ReadFile(
            "/proc/cpuinfo", "unable to read cpu informations from " );

        size_t cpuCounter = 0;
        for ( auto& line : fileContent )
        {
            if ( line.find( "model name" ) == 0 )
            {
                auto valueStart = line.find_first_of( ':' );
                this->updateThreadSystemInfo.cpu.model =
                    line.substr( valueStart + 2 );
            }
            else if ( line.find( "processor" ) == 0 )
            {
                cpuCounter++;
            }
        }

        this->updateThreadSystemInfo.cpu.numberOfCores =
            static_cast< uint32_t >( cpuCounter );

        /// allocate cpu core status memory
        this->updateThreadSystemInfo.cpu.cores.resize( cpuCounter );
        this->cpuDeltaTimes.resize( cpuCounter + 1 );
    }


    for ( size_t currentCore = 0;
          currentCore < this->updateThreadSystemInfo.cpu.numberOfCores;
          ++currentCore )
    {
        for ( auto& line :
              this->ReadFile( "/sys/devices/system/cpu/cpu" +
                                  std::to_string( currentCore ) +
                                  "/cpufreq/scaling_cur_freq",
                              "unable to read cpu core frequency from " ) )
        {
            this->updateThreadSystemInfo.cpu.cores[currentCore].frequencyInMhz =
                static_cast< float >(
                    std::strtoul( line.c_str(), nullptr, 10 ) ) /
                1000.0f;
            break;
        }
    }
#endif

    this->UpdateCPULoad();
}

void
elSystemInfo::UpdateCPULoad() noexcept
{
#ifdef _WIN32
    constexpr uint8_t     ProcessorPerformanceInformation = 0x8;
    constexpr const char* GetCPUInfoLibrarySymbol = "NtQuerySystemInformation";
    constexpr const char* GetCPUInfoDLLFile       = "NTDLL.DLL";

    struct ProcessorInformation_t
    {
        LARGE_INTEGER IdleTime;
        LARGE_INTEGER KernelTime;
        LARGE_INTEGER UserTime;
        LARGE_INTEGER Reserved1[2];
        ULONG         Reserved2;
    };

    using GetCPUInfo_t = DWORD( WINAPI* )( DWORD, void*, DWORD, DWORD* );

    static GetCPUInfo_t GetCPUInfo =
        reinterpret_cast< GetCPUInfo_t >( GetProcAddress(
            GetModuleHandle( GetCPUInfoDLLFile ), GetCPUInfoLibrarySymbol ) );

    std::vector< ProcessorInformation_t > rawProcessorInfo;
    rawProcessorInfo.resize( this->updateThreadSystemInfo.cpu.numberOfCores +
                             1 );

    DWORD returnLength;
    GetCPUInfo( ProcessorPerformanceInformation, rawProcessorInfo.data(),
                sizeof( ProcessorInformation_t ) *
                    this->updateThreadSystemInfo.cpu.numberOfCores,
                &returnLength );

    float cpuLoad = 0.0f;
    for ( uint64_t i = 0u; i < this->updateThreadSystemInfo.cpu.numberOfCores;
          ++i )
    {
        if ( !this->firstCycle )
        {
            uint64_t kernelTime = ( rawProcessorInfo[i].KernelTime.QuadPart -
                                    this->cpuDeltaTimes[i][0] );
            uint64_t userTime   = ( rawProcessorInfo[i].UserTime.QuadPart -
                                  this->cpuDeltaTimes[i][1] );
            uint64_t idleTime   = ( rawProcessorInfo[i].IdleTime.QuadPart -
                                  this->cpuDeltaTimes[i][2] );

            float coreLoad =
                100.0f *
                static_cast< float >( kernelTime + userTime - idleTime ) /
                static_cast< float >( kernelTime + userTime );

            this->updateThreadSystemInfo.cpu.cores[i].load = coreLoad;
            cpuLoad +=
                coreLoad / static_cast< float >(
                               this->updateThreadSystemInfo.cpu.numberOfCores );
        }

        this->cpuDeltaTimes[i][0] = rawProcessorInfo[i].KernelTime.QuadPart;
        this->cpuDeltaTimes[i][1] = rawProcessorInfo[i].UserTime.QuadPart;
        this->cpuDeltaTimes[i][2] = rawProcessorInfo[i].IdleTime.QuadPart;
    }
    this->updateThreadSystemInfo.cpu.load = cpuLoad;

#endif

#ifndef _WIN32
    std::vector< std::vector< size_t > > cpuTimes(
        this->updateThreadSystemInfo.cpu.numberOfCores + 1 );

    size_t currentCPU  = 0;
    auto   statContent = this->ReadFile(
        "/proc/stat", "unable to read cpu load informations from " );
    if ( statContent.empty() ) return;

    for ( auto& line : statContent )
    {
        if ( line.find( "cpu" ) == 0 )
        {
            size_t startPos  = line.find_first_of( ' ' );
            line             = line.substr( startPos + 1 );
            startPos         = line.find_first_not_of( ' ' );
            std::string temp = line.substr( startPos );
            while ( true )
            {
                startPos = temp.find_first_of( ' ' );
                if ( startPos == std::string::npos ) break;

                cpuTimes[currentCPU].emplace_back(
                    strtol( temp.substr( 0, startPos ).c_str(), nullptr, 10 ) );
                temp = temp.substr( startPos + 1 );
            }
        }
        currentCPU++;
    }

    currentCPU = 0;
    for ( auto& c : cpuTimes )
    {
        this->cpuDeltaTimes[currentCPU][0] = this->cpuDeltaTimes[currentCPU][1];
        this->cpuDeltaTimes[currentCPU][1] = c[3];
        this->cpuDeltaTimes[currentCPU][2] = this->cpuDeltaTimes[currentCPU][3];
        this->cpuDeltaTimes[currentCPU][3] =
            std::accumulate( c.begin(), c.end(), 0u );
        currentCPU++;
    }

    if ( !this->firstCycle )
    {
        size_t currentCPU = 0;
        for ( auto& t : this->cpuDeltaTimes )
        {
            size_t idleDelta  = t[1] - t[0];
            size_t totalDelta = t[3] - t[2];
            float  load =
                100.0f * ( 1.0f - static_cast< float >( idleDelta ) /
                                      static_cast< float >( totalDelta ) );
            if ( currentCPU == 0 )
            {
                this->updateThreadSystemInfo.cpu.load = load;
            }
            else
            {
                this->updateThreadSystemInfo.cpu.cores[currentCPU - 1].load =
                    load;
            }
            currentCPU++;
        }
    }
#endif
} // namespace utils

void
elSystemInfo::UpdateMemory() noexcept
{
#ifdef _WIN32
    MEMORYSTATUSEX memoryInfo;
    memoryInfo.dwLength = sizeof( MEMORYSTATUSEX );
    GlobalMemoryStatusEx( &memoryInfo );

    this->updateThreadSystemInfo.memory.total =
        static_cast< uint64_t >( memoryInfo.ullTotalPhys );
    this->updateThreadSystemInfo.memory.used =
        static_cast< uint64_t >( memoryInfo.ullAvailPhys );
    this->updateThreadSystemInfo.memory.swapTotal =
        static_cast< uint64_t >( memoryInfo.ullTotalPageFile );
    this->updateThreadSystemInfo.memory.swapUsed = static_cast< uint64_t >(
        memoryInfo.ullTotalPageFile - memoryInfo.ullAvailPageFile );
#endif

#ifndef _WIN32
    auto extractMemoryEntry = []( std::string rawEntry )
    {
        rawEntry = rawEntry.substr( rawEntry.find_first_of( ' ' ) );
        rawEntry = rawEntry.substr( rawEntry.find_first_not_of( ' ' ) );
        rawEntry = rawEntry.substr( 0, rawEntry.find_first_of( ' ' ) );
        return rawEntry;
    };

    for ( auto& line :
          this->ReadFile( "/proc/meminfo",
                          "unable to read memory usage informations from " ) )
    {
        if ( line.find( "MemTotal:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.total =
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                1024u;
        }
        else if ( line.find( "MemAvailable:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.used =
                this->updateThreadSystemInfo.memory.total -
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                    1024u;
        }
        else if ( line.find( "Buffers:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.buffer =
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                1024u;
        }
        else if ( line.find( "Cached:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.cached =
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                1024u;
        }
        else if ( line.find( "SwapTotal:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.swapTotal =
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                1024u;
        }
        else if ( line.find( "SwapFree:" ) == 0 )
        {
            this->updateThreadSystemInfo.memory.swapUsed =
                this->updateThreadSystemInfo.memory.swapTotal -
                strtoull( extractMemoryEntry( line ).c_str(), nullptr, 10 ) *
                    1024u;
        }
    }
#endif
}

void
elSystemInfo::UpdateUptime() noexcept
{
#ifndef _WIN32
    for ( auto& line :
          this->ReadFile( "/proc/uptime", "unable to read uptime from " ) )
    {
        this->updateThreadSystemInfo.uptime = strtof(
            line.substr( 0, line.find_first_of( ' ' ) ).c_str(), nullptr );
        return;
    }
#endif
}

void
elSystemInfo::UpdateNetwork() noexcept
{
#ifndef _WIN32
    auto devicesOverview = this->ReadFile(
        "/proc/net/dev", "unable to read network device informations from " );

    /// fetch available network device names
    if ( !devicesOverview.empty() )
    {
        this->updateThreadSystemInfo.network.interfaces.clear();
        for ( size_t k = 2, limit = devicesOverview.size() - 1; k < limit; ++k )
        {
            std::string temp = devicesOverview[k];
            temp             = temp.substr( temp.find_first_not_of( ' ' ) );
            temp             = temp.substr( 0, temp.find_first_of( ':' ) );
            this->updateThreadSystemInfo.network.interfaces.emplace_back();
            this->updateThreadSystemInfo.network.interfaces.back()
                .interfaceName = temp;
        }
    }

    /// get dns server
    this->updateThreadSystemInfo.network.dns.clear();
    for ( auto& line : this->ReadFile(
              "/etc/resolv.conf", "unable to read dns informations from " ) )
    {
        size_t start = line.find( "nameserver" );
        if ( start != std::string::npos )
        {
            std::string temp = line.substr( start + 10 );
            start            = temp.find_first_not_of( ' ' );
            if ( start != std::string::npos ) temp = temp.substr( start );
            start = temp.find_first_of( ' ' );
            if ( start != std::string::npos ) temp = temp.substr( 0, start );
            this->updateThreadSystemInfo.network.dns.emplace_back( temp );
        }
    }

    /// fill device informations
    for ( auto& device : this->updateThreadSystemInfo.network.interfaces )
    {
        for ( std::string pre : { "rx", "tx" } )
        {
            auto iface = ( pre == "rx" ) ? &device.incoming : &device.outgoing;
            auto content =
                this->ReadFile( "/sys/class/net/" + device.interfaceName +
                                    "/statistics/" + pre + "_bytes",
                                "unable to read network device statistics " );
            if ( !content.empty() )
                iface->bytes = strtoull( content[0].c_str(), nullptr, 10 );

            content =
                this->ReadFile( "/sys/class/net/" + device.interfaceName +
                                    "/statistics/" + pre + "_packets",
                                "unable to read network device statistics " );
            if ( !content.empty() )
                iface->packets = strtoull( content[0].c_str(), nullptr, 10 );

            content =
                this->ReadFile( "/sys/class/net/" + device.interfaceName +
                                    "/statistics/" + pre + "_errors",
                                "unable to read network device statistics " );
            if ( !content.empty() )
                iface->errors = strtoull( content[0].c_str(), nullptr, 10 );
        }

        /// update network load
        auto iter =
            bb::find_if( this->systemInfo.network.interfaces,
                         [&]( auto& v ) -> bool
                         { return v.interfaceName == device.interfaceName; } );

        if ( iter != this->systemInfo.network.interfaces.end() )
        {
            device.incoming.byteLoad =
                static_cast< float >( device.incoming.bytes -
                                      iter->incoming.bytes ) *
                1000.0f /
                static_cast< float >( this->intervalTime.GetMilliSeconds() );
            device.incoming.packetLoad =
                static_cast< float >( device.incoming.packets -
                                      iter->incoming.packets ) *
                1000.0f /
                static_cast< float >( this->intervalTime.GetMilliSeconds() );
            device.outgoing.byteLoad =
                static_cast< float >( device.outgoing.bytes -
                                      iter->outgoing.bytes ) *
                1000.0f /
                static_cast< float >( this->intervalTime.GetMilliSeconds() );
            device.outgoing.packetLoad =
                static_cast< float >( device.outgoing.packets -
                                      iter->outgoing.packets ) *
                1000.0f /
                static_cast< float >( this->intervalTime.GetMilliSeconds() );
        }

        this->GetIPAddressOfInterface( device );

        /// get mac address
        for ( auto& line : this->ReadFile(
                  "/sys/class/net/" + device.interfaceName + "/address",
                  "unable to read mac address from " ) )
        {
            device.macAddress = line;
            break;
        }
    }
#endif
}

void
elSystemInfo::GetIPAddressOfInterface(
    internal::networkInterface_t& interface ) const noexcept
{
#ifndef _WIN32
    int sock = socket( PF_INET, SOCK_DGRAM, IPPROTO_IP );
    if ( sock < 0 )
    {
        std::cerr << "unable to open socket to ip address of interface "
                  << interface.interfaceName << std::endl;
        return;
    }
    utils::elGenericRAII socketGuard( [] {}, [&] { close( sock ); } );

    // acquire ip address
    struct ifreq ifr;
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy( ifr.ifr_name, interface.interfaceName.c_str(), IFNAMSIZ - 1 );

    if ( ioctl( sock, SIOCGIFADDR, &ifr ) == -1 )
    {
        /// interface is down therefore it is alright that we are unable to
        /// acquire the ip address
        return;
    }

    std::string         ipAddress( 16, ' ' );
    struct sockaddr_in* ipRaw =
        reinterpret_cast< sockaddr_in* >( &ifr.ifr_addr );

    if ( inet_ntop( AF_INET, &ipRaw->sin_addr, ipAddress.data(),
                    static_cast< socklen_t >( ipAddress.size() ) ) == nullptr )
    {
        std::cerr << "unable to transform raw ipaddress into human readable "
                     "format for interface "
                  << interface.interfaceName << std::endl;
        return;
    }

    interface.ipAddress = ipAddress;

    /// acquire broadcast address
    if ( ioctl( sock, SIOCGIFBRDADDR, &ifr ) == -1 )
    {
        std::cerr << "unable to read broadcast address of interface "
                  << interface.interfaceName << std::endl;
        return;
    }

    ipAddress = std::string( 16, ' ' );
    ipRaw     = reinterpret_cast< struct sockaddr_in* >( &ifr.ifr_broadaddr );
    if ( inet_ntop( AF_INET, &ipRaw->sin_addr, ipAddress.data(),
                    static_cast< socklen_t >( ipAddress.size() ) ) == nullptr )
    {
        std::cerr << "unable to translate raw broadcast address into human "
                     "readable format for interface "
                  << interface.interfaceName << std::endl;
        return;
    }

    interface.broadCastAddress = ipAddress;


    /// acquire netmask
    if ( ioctl( sock, SIOCGIFNETMASK, &ifr ) == -1 )
    {
        std::cerr << "unable to read network mask of interface "
                  << interface.interfaceName << std::endl;
        return;
    }

    ipAddress = std::string( 16, ' ' );
    ipRaw     = reinterpret_cast< struct sockaddr_in* >( &ifr.ifr_broadaddr );
    if ( inet_ntop( AF_INET, &ipRaw->sin_addr, ipAddress.data(),
                    static_cast< socklen_t >( ipAddress.size() ) ) == nullptr )
    {
        std::cerr << "unable to translate network mask into human "
                     "readable format for interface "
                  << interface.interfaceName << std::endl;
        return;
    }

    interface.networkMask = ipAddress;
#endif
}

void
elSystemInfo::UpdateNetworkConnections() noexcept
{
#ifndef _WIN32
    auto content = this->ReadFile(
        "/proc/net/tcp",
        "unable to read network connection informations from " );
    if ( content.empty() ) return;

    this->updateThreadSystemInfo.network.connections.clear();
    for ( size_t k = 1, limit = content.size() - 1; k < limit; ++k )
    {
        std::string temp = content[k];
        temp             = temp.substr( temp.find_first_of( ':' ) + 1 );
        temp             = temp.substr( temp.find_first_not_of( ' ' ) );

        size_t pos           = 0;
        pos                  = temp.find_first_of( ':' );
        std::string sourceIP = temp.substr( 0, pos );
        temp                 = temp.substr( pos + 1 );

        pos                    = temp.find_first_of( ' ' );
        std::string sourcePort = temp.substr( 0, pos );
        temp                   = temp.substr( pos + 1 );

        pos                       = temp.find_first_of( ':' );
        std::string destinationIP = temp.substr( 0, pos );
        temp                      = temp.substr( pos + 1 );

        pos                         = temp.find_first_of( ' ' );
        std::string destinationPort = temp.substr( 0, pos );

        internal::connection_t newConnection;
        newConnection.source.port =
            static_cast< uint16_t >( std::stoi( sourcePort, nullptr, 16 ) );
        newConnection.destination.port = static_cast< uint16_t >(
            std::stoi( destinationPort, nullptr, 16 ) );

        auto getIPNumber = []( std::string v, size_t offset ) -> std::string
        {
            std::string temp( 2, ' ' );
            temp[0] = v[0 + offset];
            temp[1] = v[1 + offset];
            return std::to_string( std::stoi( temp, nullptr, 16 ) );
        };

        newConnection.source.ipAddress =
            getIPNumber( sourceIP, 6 ) + "." + getIPNumber( sourceIP, 4 ) +
            "." + getIPNumber( sourceIP, 2 ) + "." + getIPNumber( sourceIP, 0 );

        newConnection.destination.ipAddress =
            getIPNumber( destinationIP, 6 ) + "." +
            getIPNumber( destinationIP, 4 ) + "." +
            getIPNumber( destinationIP, 2 ) + "." +
            getIPNumber( destinationIP, 0 );

        this->updateThreadSystemInfo.network.connections.emplace_back(
            newConnection );
    }
#endif
}

std::vector< std::string >
elSystemInfo::ReadFile( const std::string& fileName,
                        const std::string& errorMessage ) const noexcept
{
    utils::elFile file( fileName );
    if ( !file.Read() )
    {
        std::cerr << errorMessage << fileName << std::endl;
        return std::vector< std::string >();
    }
    return file.GetContentAsVector();
}


systemInfo_t
elSystemInfo::GetSystemInfo() const noexcept
{
    std::lock_guard< std::mutex > lock( this->systemInfoMutex );
    return this->systemInfo;
}
} // namespace utils
} // namespace el3D
