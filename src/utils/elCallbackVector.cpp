#include "utils/elCallbackVector.hpp"

namespace el3D
{
namespace utils
{
CallbackGuard
elCallbackVector::CreateCallback(
    const std::function< void() >& callback ) noexcept
{
    std::lock_guard< std::recursive_mutex > lock( this->callbacksMutex );
    auto id = this->callbacks.emplace_back( callback ).index();
    this->updateCallbackCopy.store( true );
    return { [] {}, [id, this] { this->RemoveCallback( id ); } };
}

void
elCallbackVector::RemoveCallback( const uint64_t id ) noexcept
{
    std::lock_guard< std::recursive_mutex > lock( this->callbacksMutex );
    this->callbacks.erase( id );
    this->updateCallbackCopy.store( true );
}

void
elCallbackVector::Execute() noexcept
{
    if ( this->updateCallbackCopy.load() )
    {
        std::lock_guard< std::recursive_mutex > lock( this->callbacksMutex );
        this->callbackCopy = this->callbacks.get_const_contents();
        this->updateCallbackCopy.store( false );
    }

    for ( auto& callback : this->callbackCopy )
        callback();
}
} // namespace utils
} // namespace el3D
