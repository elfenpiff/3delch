#include "utils/elStackTrace.hpp"

#include "cuipron/SystemCommand.hpp"
#include "utils/terminalColors.hpp"

#ifndef WIN32
#include <cxxabi.h>
#include <execinfo.h>
#include <unistd.h>
#endif

#include <csignal>
#include <iostream>

void
SignalHandler( int signal )
{
    std::string signalString;
    switch ( signal )
    {
        case SIGABRT:
            signalString = "SIGABRT";
            break;
        case SIGSEGV:
            signalString = "SIGSEGV";
            break;
        case SIGILL:
            signalString = "SIGILL";
            break;
        case SIGFPE:
            signalString = "SIGFPE";
            break;
#ifndef _WIN32
        case SIGCHLD:
            signalString = "SIGCHLD";
            break;
#endif
        default:
            signalString = "SIGUNDEF";
    }

    el3D::utils::elStackTrace().Print();
    fprintf( stdout, "\n   received signal ::: %s[ %s%s%s ]%s\n\n",
             el3D::CONSOLE_FG_COLOR_LIGHT_RED, el3D::CONSOLE_FG_COLOR_RED,
             signalString.c_str(), el3D::CONSOLE_FG_COLOR_LIGHT_RED,
             el3D::CONSOLE_FG_COLOR_DEFAULT );
    exit( -1 );
}

namespace el3D
{
namespace utils
{
elStackTrace::stackEntry_t::stackEntry_t( const std::string &symbol,
                                          const std::string &function,
                                          const std::string &offset,
                                          const std::string &address )
    : symbol( symbol ), function( function ), offset( offset ),
      address( address )
{
#ifndef WIN32
    auto binaryName = this->GetBinaryName();
    if ( !binaryName.empty() )
    {
        cp::SystemCommand addr2line(
            std::string( "/usr/bin/addr2line -C -e " ) + this->GetBinaryName() +
            " -f -i " + this->address );
        auto result = addr2line.ExecuteAndGet();
        if ( result.size() > 1 )
        {
            this->internalSymbol = result[0];
            this->file           = result[1];
        }
    }
#endif
}

std::string
elStackTrace::stackEntry_t::GetBinaryName() const noexcept
{
    char buffer[1024];
#ifndef WIN32
    ssize_t readSize = readlink( "/proc/self/exe", buffer, sizeof( buffer ) );
    if ( readSize == -1 )
    {
        std::cerr << "could not read /proc/self/exe" << std::endl;
        buffer[0] = '\0';
    }
    else
    {
        buffer[readSize] = '\0';
    }
#endif

    return &( buffer[0] );
}

elStackTrace::stackAddressList_t::stackAddressList_t()
#ifndef WIN32
    : depth( backtrace( this->addresses,
                        sizeof( this->addresses ) / sizeof( void * ) ) )
#endif
{
}

elStackTrace::elStackTrace()
{
    this->GenerateStackTrace( stackAddressList_t() );
}

void
elStackTrace::GenerateStackTrace( const stackAddressList_t &addressList )
{
#ifndef WIN32
    char **symbolList =
        backtrace_symbols( addressList.addresses, addressList.depth );

    for ( size_t k = 0; k < static_cast< size_t >( addressList.depth ); ++k )
        this->AddRawEntryToStackTrace( symbolList[k] );

    free( symbolList );
#endif
}

void
elStackTrace::AddRawEntryToStackTrace( const std::string &rawEntry )
{
    std::string temp = rawEntry;

    // symbol
    size_t pos = temp.find_first_of( "(" );
    if ( pos == std::string::npos ) return;
    std::string symbol = temp.substr( 0, pos );
    temp               = temp.substr( pos + 1 );

    // function
    pos = temp.find_first_of( "+" );
    if ( pos == std::string::npos ) return;
    std::string function = temp.substr( 0, pos );
    temp                 = temp.substr( pos + 1 );

    // offset
    pos = temp.find_first_of( ")" );
    if ( pos == std::string::npos ) return;
    std::string offset = temp.substr( 0, pos );
    temp               = temp.substr( pos + 1 );

    // address
    pos = temp.find_first_of( "[" );
    if ( pos == std::string::npos ) return;
    temp = temp.substr( pos + 1 );
    pos  = temp.find_first_of( "]" );
    if ( pos == std::string::npos ) return;
    std::string address = temp.substr( 0, pos );

    this->stackTrace.emplace_back( symbol, function, offset, address );
}

std::vector< elStackTrace::stackEntry_t >
elStackTrace::Get() const noexcept
{
    return this->stackTrace;
}

void
elStackTrace::RegisterInSignalHandler() noexcept
{
    std::signal( SIGSEGV, SignalHandler );
    std::signal( SIGABRT, SignalHandler );
    std::signal( SIGILL, SignalHandler );
    std::signal( SIGFPE, SignalHandler );
#ifndef _WIN32
    std::signal( SIGCHLD, SignalHandler );
#endif
}

void
elStackTrace::Print() const noexcept
{
    printf( "%s----------------------------------------------------------------"
            "----------",
            CONSOLE_FG_COLOR_DARK_GRAY );
    printf( "\n%s     StackTrace\n", CONSOLE_FG_COLOR_LIGHT_GREEN );
    printf( "%s----------------------------------------------------------------"
            "----------\n",
            CONSOLE_FG_COLOR_DARK_GRAY );
    for ( size_t k = 0; k < this->stackTrace.size(); ++k )
    {
        auto &e = this->stackTrace[k];
        printf( "%s%3lu ", CONSOLE_FG_COLOR_DARK_GRAY, k );
        printf( "%s [ %s%s%s ] ", CONSOLE_FG_COLOR_DEFAULT,
                CONSOLE_FG_COLOR_LIGHT_GRAY, e.internalSymbol.c_str(),
                CONSOLE_FG_COLOR_DEFAULT );
        printf( "%s%s ", CONSOLE_FG_COLOR_LIGHT_WHITE, e.function.c_str() );
        printf( "%s%s ", CONSOLE_FG_COLOR_LIGHT_YELLOW, e.file.c_str() );
        printf( "%s{%s%s ", CONSOLE_FG_COLOR_DEFAULT,
                CONSOLE_FG_COLOR_DARK_GRAY, e.address.c_str() );
        printf( "+%s%s}", e.offset.c_str(), CONSOLE_FG_COLOR_DEFAULT );
        printf( "%s\n", CONSOLE_FG_COLOR_DEFAULT );
    }
    printf( "%s----------------------------------------------------------------"
            "---------"
            "-\n%s",
            CONSOLE_FG_COLOR_DARK_GRAY, CONSOLE_FG_COLOR_DEFAULT );
}

} // namespace utils
} // namespace el3D
