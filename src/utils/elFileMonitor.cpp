#ifdef _WIN32
#define NOMINMAX
#endif

#include "utils/elFileMonitor.hpp"

#include <algorithm>
#include <cstring>
#ifndef _WIN32
#include <sys/select.h>
#else
#include <winsock2.h>
#endif

namespace el3D
{
namespace utils
{
elFileMonitor::elFileDescriptorSet::elFileDescriptorSet()
{
    FD_ZERO( &this->fileDescriptorSet );
}

elFileMonitor::elFileDescriptorSet::elFileDescriptorSet(
    const std::initializer_list< int >& fileDescriptorList )
    : elFileDescriptorSet()
{
    for ( auto& fileDescriptor : fileDescriptorList )
        this->Add( fileDescriptor );
}

bool
elFileMonitor::elFileDescriptorSet::Add( const int fileDescriptor )
{
    if ( this->size >= FD_SETSIZE ) return false;

    this->size++;
    FD_SET( fileDescriptor, &this->fileDescriptorSet );
    this->highestFileDescriptor =
        std::max( fileDescriptor, this->highestFileDescriptor );
    return true;
}

bool
elFileMonitor::elFileDescriptorSet::Remove( const int fileDescriptor )
{
    if ( !this->Contains( fileDescriptor ) ) return false;

    this->size--;
    FD_CLR( fileDescriptor, &this->fileDescriptorSet );
    return true;
}

bool
elFileMonitor::elFileDescriptorSet::Contains( const int fileDescriptor ) const
{
    return FD_ISSET( fileDescriptor, &this->fileDescriptorSet );
}

fd_set*
elFileMonitor::elFileDescriptorSet::GetHandle()
{
    if ( this->size != 0 )
        return &this->fileDescriptorSet;
    else
        return nullptr;
}

int
elFileMonitor::elFileDescriptorSet::GetHighestFileDescriptor() const
{
    return this->highestFileDescriptor;
}

size_t
elFileMonitor::elFileDescriptorSet::GetSize() const
{
    return this->size;
}

void
elFileMonitor::AdjustHighestFileDescriptor()
{
    this->highestFileDescriptor = 0;

    if ( !this->descriptorSet.read.empty() )
        this->highestFileDescriptor = std::max(
            this->highestFileDescriptor, *this->descriptorSet.read.rbegin() );

    if ( !this->descriptorSet.write.empty() )
        this->highestFileDescriptor = std::max(
            this->highestFileDescriptor, *this->descriptorSet.write.rbegin() );

    if ( !this->descriptorSet.except.empty() )
        this->highestFileDescriptor = std::max(
            this->highestFileDescriptor, *this->descriptorSet.except.rbegin() );

    this->highestFileDescriptor += 1;
}

void
elFileMonitor::AddFile( const FileType type, const int descriptor )
{
    switch ( type )
    {
        case FileType::Read:
        {
            this->descriptorSet.read.insert( descriptor );
            break;
        }
        case FileType::Write:
        {
            this->descriptorSet.write.insert( descriptor );
            break;
        }
        case FileType::Except:
        {
            this->descriptorSet.except.insert( descriptor );
            break;
        }
    }

    this->AdjustHighestFileDescriptor();
}

void
elFileMonitor::RemoveFile( const FileType type, const int descriptor )
{
    auto removeFromSet = []( std::set< int >& s, int d ) {
        auto iter = s.find( d );
        if ( iter != s.end() ) s.erase( iter );
    };

    switch ( type )
    {
        case FileType::Read:
        {
            removeFromSet( this->descriptorSet.read, descriptor );
            break;
        }
        case FileType::Write:
        {
            removeFromSet( this->descriptorSet.write, descriptor );
            break;
        }
        case FileType::Except:
        {
            removeFromSet( this->descriptorSet.except, descriptor );
            break;
        }
    }
}

elFileMonitor::descriptorSet_t< elFileMonitor::elFileDescriptorSet >
elFileMonitor::CreateFileDescriptorSet() const
{
    descriptorSet_t< elFileDescriptorSet > newSet;

    for ( auto d : this->descriptorSet.read )
        newSet.read.Add( d );

    for ( auto d : this->descriptorSet.write )
        newSet.write.Add( d );

    for ( auto d : this->descriptorSet.except )
        newSet.except.Add( d );

    return newSet;
}

elFileMonitor::descriptorSet_t< elFileMonitor::elFileDescriptorSet >
elFileMonitor::Monitor( const units::Time& timeout )
{
    auto fdSet = this->CreateFileDescriptorSet();

    struct timeval tv = timeout;
    int            value =
        ::select( this->highestFileDescriptor, fdSet.read.GetHandle(),
                  fdSet.write.GetHandle(), fdSet.except.GetHandle(), &tv );

    if ( value == -1 )
    {
        std::cerr << "select call failed ::: " << strerror( errno )
                  << std::endl;
    }

    fdSet.doesContainBadFileDescriptor = ( value == -1 );

    return fdSet;
}

void
elFileMonitor::Clear()
{
    this->descriptorSet.read.clear();
    this->descriptorSet.write.clear();
    this->descriptorSet.except.clear();
}
} // namespace utils
} // namespace el3D
