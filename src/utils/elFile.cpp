#include "utils/elFile.hpp"

#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace utils
{
bool
elFile::Read()
{
    try
    {
        std::ifstream file( path,
                            ( isBinary ) ? std::ios::binary : std::ios::in );

        if ( !file.is_open() )
        {
            std::cerr << "could not read file \"" << path << "\"" << std::endl;
            return false;
        }

        this->content.clear();
        this->content.insert( this->content.begin(),
                              std::istreambuf_iterator< char >( file ),
                              std::istreambuf_iterator< char >() );

        file.close();
        return true;
    }
    catch ( const std::exception& e )
    {
        std::cerr << "exception occurred while reading file \"" << path
                  << "\" ::: " << e.what() << std::endl;
        return false;
    }
}

bool
elFile::Write()
{
    try
    {
        std::ofstream file( path );

        if ( !file.is_open() )
        {
            std::cerr << "could not write file \"" << path << "\"" << std::endl;
            return false;
        }

        for ( auto iter : content )
            file << iter;

        file.close();
        return true;
    }
    catch ( const std::exception& e )
    {
        std::cerr << "exception occurred writing file \"" << path
                  << "\" ::: " << e.what() << std::endl;
        return false;
    }
}

std::string
elFile::GetContentAsString() const noexcept
{
    std::string retVal;
    retVal.resize( content.size() );

    for ( size_t i = 0, limit = retVal.size(); i < limit; ++i )
        retVal[i] = content[i];

    return retVal;
}

std::vector< std::string >
elFile::GetContentAsVector() const noexcept
{
    std::vector< std::string > retVal;

    for ( size_t i = 0, limit = content.size(); i < limit; ++i )
    {
        if ( i == 0 || content[i] == 10 ) // 10 = '\n'
            retVal.push_back( "" );

        if ( content[i] == 10 ) continue;

        retVal.back().push_back( content[i] );
    }

    return retVal;
}

void
elFile::SetContentFromString( const std::string& val ) noexcept
{
    content.resize( val.size() );
    for ( size_t i = 0, limit = val.size(); i < limit; ++i )
        content[i] = val[i];
}

void
elFile::SetContentFromVector( const std::vector< std::string >& val ) noexcept
{
    content.clear();

    for ( size_t k = 0, limitK = val.size(); k < limitK; ++k )
    {
        size_t pos = content.size();
        content.resize( ( k != limitK - 1 ) ? content.size() + val[k].size() + 1
                                            : content.size() + val[k].size() );

        for ( size_t i = 0, limitI = val[k].size(); i < limitI; ++i )
            content[pos + i] = val[k][i];

        if ( k == limitK - 1 ) break;
        content[pos + val[k].size()] = '\n';
    }
}

std::string
elFile::GetPath() const noexcept
{
    return this->path;
}
} // namespace utils
} // namespace el3D
