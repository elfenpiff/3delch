#include "GLAPI/common/SDLCall.hpp"

#include "logging/elLog.hpp"



// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace GLAPI
{
void
__SDLErrorMessage( const std::string& message ) // NOLINT
{
    LOG_ERROR( 0 ) << message << " ::: " << SDL_GetError();
    SDL_ClearError();
}
} // namespace GLAPI
} // namespace el3D
