#include "GLAPI/elWindow.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/common/SDLCall.hpp"
#include "OpenGL/gl_Call.hpp"

#include <algorithm>


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace GLAPI
{
elWindow::elWindow( const int32_t x, const int32_t y, const std::string &name,
                    const int glMajor, const int glMinor,
                    const bool useCoreProfile, const uint32_t flags,
                    const bool enableVSync )
    : x{ x }, y{ y }, glMajor{ glMajor }, glMinor{ glMinor }, name{ name },
      eventHandler( std::make_unique< elEventHandler >() ),
      windowEvent{ this->eventHandler->CreateEvent() }
{
    if ( SDLCall(
             SDL_Init, []( auto v ) { return v < 0; }, SDL_INIT_VIDEO )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to init sdl2";
        this->create_error         = elWindow_Error::UnableToInitSDL2;
        this->create_isInitialized = false;
        return;
    }

    this->sdlQuit.SetDestructor( [] { SDL_Quit(); } );

    if ( SDLCall(
             SDL_GL_SetAttribute, []( auto v ) { return v != 0; },
             SDL_GL_CONTEXT_PROFILE_MASK,
             ( useCoreProfile ) ? SDL_GL_CONTEXT_PROFILE_CORE
                                : SDL_GL_CONTEXT_PROFILE_COMPATIBILITY )
             .HasError() )
    {
        this->create_isInitialized = false;
        if ( useCoreProfile )
        {
            LOG_ERROR( 0 ) << "unable to initialize OpenGL Core profile";
            this->create_error = elWindow_Error::OpenGLCoreProfileNotSupported;
        }
        else
        {
            this->create_error =
                elWindow_Error::OpenGLCompatibilityProfileNotSupported;
            LOG_ERROR( 0 )
                << "unable to initialize OpenGL Compatibility profile";
        }
        return;
    }

    if ( SDLCall(
             SDL_GL_SetAttribute, []( auto v ) { return v == -1; },
             SDL_GL_CONTEXT_MAJOR_VERSION, glMajor )
             .HasError() ||
         SDLCall(
             SDL_GL_SetAttribute, []( auto v ) { return v == -1; },
             SDL_GL_CONTEXT_MINOR_VERSION, glMinor )
             .HasError() )
    {
        this->create_isInitialized = false;
        this->create_error         = elWindow_Error::OpenGLVersionNotSupported;
        std::string profile = ( useCoreProfile ) ? "core" : "compatibility";

        LOG_ERROR( 0 ) << "unable to init OpenGL Version " << glMajor << "."
                       << glMinor << " [ " << profile << " profile ]";
        return;
    }

    if ( SDLCall(
             SDL_CreateWindow, []( auto v ) { return v == nullptr; },
             this->name.c_str(), static_cast< int >( SDL_WINDOWPOS_CENTERED ),
             static_cast< int >( SDL_WINDOWPOS_CENTERED ), this->x, this->y,
             SDL_WINDOW_OPENGL | flags )
             .AndThen( [&]( const auto &r ) {
                 this->window = std::shared_ptr< SDL_Window >(
                     r, []( SDL_Window *v ) { SDL_DestroyWindow( v ); } );
             } )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to create window";
        this->create_isInitialized = false;
        this->create_error         = elWindow_Error::UnableToCreateWindow;
        return;
    }

    this->SetWindowIsResizable( true );

    if ( SDLCall(
             SDL_GL_CreateContext, []( auto v ) { return v == nullptr; },
             this->window.get() )
             .AndThen( [&]( const auto &r ) {
                 this->glContext = decltype( this->glContext )(
                     r, []( SDL_GLContext v ) { SDL_GL_DeleteContext( v ); } );
             } )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to initialize GL context";
        this->create_isInitialized = false;
        this->create_error = elWindow_Error::UnableToInitializeGLContext;
        return;
    }

    if ( SDLCall( TTF_Init, []( auto v ) { return v == -1; } )
             // TODO: can be changed to AndThen([&] {}) - clang compile error
             // when missing (const auto)
             .AndThen( [&]( const auto ) {
                 this->ttfQuit.SetDestructor( [] { TTF_Quit(); } );
             } )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to initialize TTF fonts";
        this->create_isInitialized = false;
        this->create_error         = elWindow_Error::UnableToInitializeTTFfonts;
        return;
    }

    int profileMask;
    if ( SDLCall(
             SDL_GL_GetAttribute, []( auto v ) { return v == -1; },
             SDL_GL_CONTEXT_PROFILE_MASK, &profileMask )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "could not read GL_CONTEXT_PROFILE_MASK";
        this->create_isInitialized = false;
        this->create_error         = elWindow_Error::InternalError;
        return;
    }

    if ( useCoreProfile && profileMask != SDL_GL_CONTEXT_PROFILE_CORE )
    {
        LOG_WARN( 0 ) << "could not init core profile";
    }
    else if ( !useCoreProfile &&
              profileMask != SDL_GL_CONTEXT_PROFILE_COMPATIBILITY )
    {
        LOG_WARN( 0 ) << "could not init compatibility profile";
    }

    GLint major, minor;
    OpenGL::gl( glGetIntegerv, static_cast< GLenum >( GL_MAJOR_VERSION ),
                &major );
    OpenGL::gl( glGetIntegerv, static_cast< GLenum >( GL_MINOR_VERSION ),
                &minor );

    if ( major != this->glMajor || minor != this->glMinor )
    {
        LOG_WARN( 0 ) << "could not init request OpenGL " << this->glMajor
                      << "." << this->glMinor << ", OpenGL " << major << "."
                      << minor << " was initialized instead";
        this->glMajor = major;
        this->glMinor = minor;
    }

    SDLCall(
        SDL_GL_SetAttribute, []( auto v ) { return v == -1; },
        SDL_GL_DOUBLEBUFFER, 1 )
        .OrElse( [] { LOG_WARN( 0 ) << "could not init double buffer"; } );

    SDLCall(
        SDL_GL_SetSwapInterval, []( auto v ) { return v == -1; }, enableVSync )
        .OrElse(
            [] {
                LOG_WARN( 0 )
                    << "could not set OpenGL swap interval, vsync disabled";
            } );

    glewExperimental = GL_TRUE;

    GLenum error = glewInit();
    if ( error != GLEW_OK )
    {
        LOG_ERROR( 0 ) << "could not init glew : "
                       << std::string( reinterpret_cast< const char * >(
                              glewGetErrorString( error ) ) );

        this->create_isInitialized = false;
        this->create_error         = elWindow_Error::InternalError;
        return;
    }

    SDL_GL_SwapWindow( this->window.get() );

    if ( this->glMajor == 4 && this->glMinor >= 3 )
    {
        OpenGL::Enable( GL_DEBUG_OUTPUT );
        OpenGL::Enable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
#ifdef ELCH_DEBUG
#ifndef _WIN32
        OpenGL::gl( glDebugMessageCallback, OpenGL::DebugMessageCallback,
                    nullptr );
        GLenum dontCare = GL_DONT_CARE;
        OpenGL::gl( glDebugMessageControl, dontCare, dontCare, dontCare, 0,
                    nullptr, true );
#endif
#endif
    }

    this->windowEvent->SetCallback( [this]( const SDL_Event &e ) {
        if ( e.type == SDL_WINDOWEVENT &&
             e.window.event == SDL_WINDOWEVENT_RESIZED )
            this->Reshape();
    } );
    this->windowEvent->Register();

    this->SetShaderVersion();
    this->Reshape();
    this->PrintInfo();
    this->create_isInitialized = true;
}

elWindow::~elWindow()
{
}

void
elWindow::SetWindowIsResizable( const bool v ) noexcept
{
    SDL_SetWindowResizable( this->window.get(), static_cast< SDL_bool >( v ) );
}

void
elWindow::Reshape() noexcept
{
    SDL_GetWindowSize( this->window.get(), &this->x, &this->y );
    OpenGL::gl( glViewport, 0, 0, this->x, this->y );
    if ( this->reshapeCallback ) this->reshapeCallback( this->x, this->y );
}

windowSize_t
elWindow::GetWindowSize() const noexcept
{
    return { static_cast< size_t >( this->x ),
             static_cast< size_t >( this->y ) };
}

void
elWindow::SetWindowSize( const int x, const int y ) noexcept
{
    this->x = x;
    this->y = y;
    SDL_SetWindowSize( this->window.get(), x, y );
}

void
elWindow::PrintInfo() const noexcept
{
    std::string openGLProfile;

    int profile;
    SDLCall(
        SDL_GL_GetAttribute, []( auto v ) { return v == -1; },
        SDL_GL_CONTEXT_PROFILE_MASK, &profile )
        // TODO: can be changed to AndThen([&] {}) - clang compile error
        // when missing (const auto)
        .AndThen( [&]( const auto ) {
            if ( profile == SDL_GL_CONTEXT_PROFILE_CORE )
                openGLProfile = "(core)";
            else if ( profile == SDL_GL_CONTEXT_PROFILE_ES )
                openGLProfile = "(es)";
            else if ( profile == SDL_GL_CONTEXT_PROFILE_COMPATIBILITY )
                openGLProfile = "(compatibility)";
            else
                openGLProfile = "(unknown)";

            LOG_INFO( 0 ) << "Initializied OpenGL " << this->glMajor << "."
                          << this->glMinor << " " << openGLProfile;

            LOG_INFO( 0 ) << "Vendor             : "
                          << glGetString( GL_VENDOR );
            LOG_INFO( 0 ) << "Renderer           : "
                          << glGetString( GL_RENDERER );
            LOG_INFO( 0 ) << "Version            : "
                          << glGetString( GL_VERSION );
            LOG_INFO( 0 ) << "Shader             : " << this->shaderVersion;
        } );
}

void
elWindow::MainLoop() noexcept
{
    constexpr uint32_t NO_EVENT_COLOR_INDEX =
        std::numeric_limits< uint32_t >::max();
    uint32_t currentEventColorIndex = 0;
    if ( this->reshapeCallback ) this->reshapeCallback( this->x, this->y );

    while ( !this->exitMainLoop )
    {
        if ( this->mainLoopCallback )
            currentEventColorIndex = this->mainLoopCallback();
        else
            currentEventColorIndex = NO_EVENT_COLOR_INDEX;

        this->eventHandler->PollCurrentEvents( currentEventColorIndex );

        SDL_GL_SwapWindow( this->window.get() );
        this->CalculateDeltaTime();

        ++this->frameCounter;
    }
    LOG_INFO( 0 ) << "Exiting MainLoop ...";
    if ( this->afterExitCallback ) this->afterExitCallback();
}

float
elWindow::GetFramesPerSecond() const noexcept
{
    return static_cast< float >( this->frameCounter.GetThroughputPerSecond() );
}

units::Time
elWindow::GetRuntime() const noexcept
{
    return this->runtime;
}

units::Time
elWindow::GetDeltaTime() const noexcept
{
    return this->deltaTime;
}

void
elWindow::CalculateDeltaTime() noexcept
{
    uint64_t currentTick = SDL_GetPerformanceCounter();

    if ( this->lastTick != 0 )
        this->deltaTime = units::Time::Seconds(
            static_cast< double >( currentTick - this->lastTick ) /
            this->performanceFrequency );

    this->runtime = units::Time::Seconds(
        static_cast< double >( currentTick - this->firstTick ) /
        this->performanceFrequency );

    this->lastTick = currentTick;
}

void
elWindow::ExitMainLoop(
    const std::function< void() > &afterExitCallbackValue ) noexcept
{
    this->afterExitCallback = afterExitCallbackValue;
    this->exitMainLoop      = true;
}

void
elWindow::SetMainLoopCallback( const std::function< uint32_t() > &f ) noexcept
{
    this->mainLoopCallback = f;
}

void
elWindow::SetReshapeCallback( const reshapeCallback_t &f ) noexcept
{
    this->reshapeCallback = f;
}

bb::elExpected< elWindow_Error >
elWindow::SetCaptureMouse( const bool b ) const
{
    if ( SDLCall(
             SDL_CaptureMouse, []( auto v ) { return v == -1; },
             static_cast< SDL_bool >( b ) )
             .HasError() )
    {
        std::string v = ( b ) ? "true" : "false";
        LOG_ERROR( 0 ) << "unable to set CaptureMouse to " << v;
        return bb::Error( elWindow_Error::CaptureMouse );
    }

    return bb::Success<>();
}

bb::elExpected< elWindow_Error >
elWindow::SetRelativeMouseMode( const bool b ) const
{
    if ( SDLCall(
             SDL_SetRelativeMouseMode, []( auto v ) { return v == -1; },
             static_cast< SDL_bool >( b ) )
             .HasError() )
    {
        std::string v = ( b ) ? "true" : "false";
        LOG_ERROR( 0 ) << "unable to set RelativeMouseMode to " << v;
        return bb::Error( elWindow_Error::RelativeMouseMode );
    }

    return bb::Success<>();
}

bb::elExpected< elWindow_Error >
elWindow::SetShowCursor( const bool b ) const
{
    if ( SDLCall(
             SDL_ShowCursor, []( auto v ) { return v < 0; },
             static_cast< SDL_bool >( b ) )
             .HasError() )
    {
        std::string v = ( b ) ? "true" : "false";
        LOG_ERROR( 0 ) << "unable to set RelativeMouseMode to " << v;
        return bb::Error( elWindow_Error::ShowCursor );
    }

    return bb::Success<>();
}

void
elWindow::SetWindowGrab( const bool b ) const noexcept
{
    SDL_SetWindowGrab( this->window.get(), static_cast< SDL_bool >( b ) );
}

bb::elExpected< elWindow_Error >
elWindow::SetHintWithPriority( const char *name, const char *value,
                               const SDL_HintPriority priority ) const
{
    if ( SDLCall(
             SDL_SetHintWithPriority, []( auto v ) { return v == GL_FALSE; },
             name, value, priority )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "could not set HintWithPriority for name=\"" << name
                       << "\" with value=\"" << value << "\"";
        return bb::Error( elWindow_Error::UnableToSetHintPriority );
    }

    return bb::Success<>();
}

size_t
elWindow::GetFrameCounter() const noexcept
{
    return this->frameCounter.Get();
}

glVersion_t
elWindow::GetOpenGLVersion() const noexcept
{
    return { this->glMajor, this->glMinor };
}

float
elWindow::GetDisplayDPI() const noexcept
{
    float diagonalDPI;
    float horizontalDPI;
    float verticalDPI;

    SDL_GetDisplayDPI( 0, &diagonalDPI, &horizontalDPI, &verticalDPI );

    return diagonalDPI;
}

void
elWindow::SetShaderVersion() noexcept
{
    std::string temp = reinterpret_cast< const char * >(
        glGetString( GL_SHADING_LANGUAGE_VERSION ) );

    size_t spacePos = temp.find_first_of( " " );
    if ( spacePos != std::string::npos ) temp = temp.substr( 0, spacePos );

    size_t pointPos = temp.find_first_of( "." );
    if ( pointPos != std::string::npos ) temp.erase( pointPos, 1 );

    this->shaderVersion =
        static_cast< int >( std::strtol( temp.c_str(), nullptr, 10 ) );
}

int
elWindow::GetShaderVersion() const noexcept
{
    return this->shaderVersion;
}

elEventHandler *
elWindow::GetEventHandler() noexcept
{
    return this->eventHandler.get();
}
} // namespace GLAPI
} // namespace el3D
