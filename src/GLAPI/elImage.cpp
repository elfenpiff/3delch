#include "GLAPI/elImage.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/common/SDLCall.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace GLAPI
{
elImage::elImage()
{
}

elImage::elImage( FromSurface_t, SDL_Surface *s )
    : surface( elImage::CreateRGBSurface( elImage::CreateSurfacePointer( s ) ) )
{
    this->create_isInitialized = ( this->surface != nullptr );
    if ( !this->create_isInitialized )
        LOG_ERROR( 0 ) << "image created from empty surface";
}

elImage::elImage( FromFileInByteStream_t, const utils::byteStream_t &byteStream,
                  const uint32_t format )
{
    this->surface = this->ConvertRawSurface(
        elImage::CreateSurfaceFromByteStream( byteStream ), format );

    this->create_isInitialized = ( this->surface != nullptr );
    if ( !this->create_isInitialized )
        LOG_ERROR( 0 ) << "could not load image from byteStream ";
}

elImage::elImage( FromFileInMemory_t, void *const rawMemory,
                  const uint64_t rawMemorySize, const uint32_t format )
{
    this->surface = this->ConvertRawSurface(
        elImage::CreateSurfaceFromRawMemory( rawMemory, rawMemorySize ),
        format );

    this->create_isInitialized = ( this->surface != nullptr );
    if ( !this->create_isInitialized )
        LOG_ERROR( 0 ) << "could not load image from raw memory ";
}

elImage::elImage( FromFilePath_t, const std::string &path,
                  const uint32_t format )
{
    this->surface = this->ConvertRawSurface(
        elImage::CreateSurfaceFromFile( path ), format );
    this->create_isInitialized = ( this->surface != nullptr );

    if ( !this->create_isInitialized )
        LOG_ERROR( 0 ) << "could not load image from file " << path;
}

elImage::elImage( FromArray_t, const uint64_t width, const uint64_t height,
                  const utils::byte_t *const pixels ) noexcept
{
    this->surface = this->CreateSurfaceFromByteArray( width, height, pixels );
    this->create_isInitialized = ( this->surface != nullptr );
    if ( !this->create_isInitialized )
        LOG_ERROR( 0 ) << "unable to create image from array";
}


elImage::surface_t
elImage::CreateSurfaceFromByteArray( const uint64_t       width,
                                     const uint64_t       height,
                                     const utils::byte_t *pixels ) noexcept
{
    int    depth   = 32;
    int    pitch   = 4 * static_cast< int >( width );
    Uint32 rmask   = 0x000000ff;
    Uint32 gmask   = 0x0000ff00;
    Uint32 bmask   = 0x00ff0000;
    Uint32 amask   = 0xff000000;
    auto   surface = elImage::CreateSurfacePointer( SDL_CreateRGBSurfaceFrom(
        const_cast< void * >( static_cast< const void * >( pixels ) ),
        static_cast< int >( width ), static_cast< int >( height ), depth, pitch,
        rmask, gmask, bmask, amask ) );
    if ( surface == nullptr )
        LOG_ERROR( 0 ) << "unable to load surface from byte array : " +
                              elImage::GetIMGError();
    return surface;
}

elImage::surface_t
elImage::CreateSurfaceFromByteStream(
    const utils::byteStream_t &byteStream ) noexcept
{
    auto surface = elImage::CreateSurfacePointer( IMG_Load_RW(
        SDL_RWFromMem( const_cast< void * >( static_cast< const void * >(
                           byteStream.stream.data() ) ),
                       static_cast< int >( byteStream.stream.size() ) ),
        1 ) );

    if ( surface == nullptr )
        LOG_ERROR( 0 ) << "could not load bytestream : " +
                              elImage::GetIMGError();

    return surface;
}

elImage::surface_t
elImage::CreateSurfaceFromRawMemory( void *const    rawMemory,
                                     const uint64_t size ) noexcept
{
    auto surface = elImage::CreateSurfacePointer( IMG_Load_RW(
        SDL_RWFromMem( rawMemory, static_cast< int >( size ) ), 1 ) );

    if ( surface == nullptr )
        LOG_ERROR( 0 ) << "could not load rawMemory : " +
                              elImage::GetIMGError();

    return surface;
}

elImage::surface_t
elImage::CreateSurfaceFromFile( const std::string &filePath ) noexcept
{
    auto surface =
        elImage::CreateSurfacePointer( IMG_Load( filePath.c_str() ) );

    if ( surface == nullptr )
        LOG_ERROR( 0 ) << "could not load file : " + elImage::GetIMGError();

    return surface;
}

utils::elRawPointer< utils::byte_t >
elImage::GetBytePointer() const noexcept
{
    return static_cast< utils::byte_t * >( this->surface->pixels );
}

utils::byteStream_t
elImage::GetByteStream() const noexcept
{
    size_t byteStreamSize =
        static_cast< size_t >( this->surface->w * this->surface->h *
                               this->surface->format->BytesPerPixel );
    utils::byteStream_t raw( static_cast< size_t >( byteStreamSize ) );
    utils::byte_t *     pixels =
        static_cast< utils::byte_t * >( this->surface->pixels );

    for ( size_t k = 0; k < byteStreamSize; ++k )
        raw.stream[k] = pixels[k];

    return raw;
}

dimension_t
elImage::GetDimensions() const noexcept
{
    return { static_cast< uint64_t >( std::max( 0, this->surface->w ) ),
             static_cast< uint64_t >( std::max( 0, this->surface->h ) ) };
}

Uint8
elImage::GetBytesPerPixel() const noexcept
{
    return this->surface->format->BytesPerPixel;
}

Uint32
elImage::GetFormat() const noexcept
{
    return this->surface->format->format;
}

void
elImage::Resize( const uint64_t x, const uint64_t y ) noexcept
{
    if ( this->surface->w == static_cast< int >( x ) &&
         this->surface->h == static_cast< int >( y ) )
        return;
    this->surface = elImage::ResizeSurface( this->surface, x, y );
}

void
elImage::CanvasResize( const uint64_t x, const uint64_t y ) noexcept
{
    this->surface = elImage::CanvasResizeSurface( this->surface, x, y );
}

elImage::surface_t
elImage::ResizeSurface( const surface_t origin, const uint64_t x,
                        const uint64_t y ) noexcept
{
    if ( origin.get() == nullptr ) return origin;

    auto createCall = SDLCall(
        SDL_CreateRGBSurface, []( auto v ) { return v == nullptr; },
        origin->flags, static_cast< int >( x ), static_cast< int >( y ),
        origin->format->BitsPerPixel, origin->format->Rmask,
        origin->format->Gmask, origin->format->Bmask, origin->format->Amask );

    if ( createCall.HasError() ) return nullptr;

    auto newSurface = elImage::CreateSurfacePointer( createCall.GetValue() );

    auto blitCall = SDLCall(
        SDL_BlitScaled, []( auto v ) { return v == -1; }, origin.get(), nullptr,
        newSurface.get(), nullptr );

    if ( blitCall.HasError() ) return nullptr;

    return newSurface;
}

elImage::surface_t
elImage::CanvasResizeSurface( const surface_t origin, const uint64_t x,
                              const uint64_t y ) noexcept
{
    if ( origin.get() == nullptr ) return origin;

    SDL_Rect offset;
    offset.x = 0;
    offset.y = 0;

    auto createCall = SDLCall(
        SDL_CreateRGBSurface, []( auto v ) { return v == nullptr; },
        origin->flags, static_cast< int >( x ), static_cast< int >( y ),
        origin->format->BitsPerPixel, origin->format->Rmask,
        origin->format->Gmask, origin->format->Bmask, origin->format->Amask );

    if ( createCall.HasError() ) return nullptr;

    auto newSurface = elImage::CreateSurfacePointer( createCall.GetValue() );

    auto blitCall = SDLCall(
        SDL_BlitSurface, []( auto v ) { return v == -1; }, origin.get(),
        nullptr, newSurface.get(), &offset );

    if ( blitCall.HasError() ) return nullptr;

    return newSurface;
}

elImage::surface_t
elImage::CreateRGBSurface( const surface_t origin ) noexcept
{
    if ( origin.get() == nullptr ) return origin;

    auto createCall = SDLCall(
        SDL_CreateRGBSurface, []( auto *v ) { return v == nullptr; },
        origin->flags, origin->w, origin->h, origin->format->BitsPerPixel,
        origin->format->Rmask, origin->format->Gmask, origin->format->Bmask,
        origin->format->Amask );

    if ( createCall.HasError() ) return nullptr;

    auto newSurface = elImage::CreateSurfacePointer( createCall.GetValue() );

    auto blitCall = SDLCall(
        SDL_BlitSurface, []( auto v ) { return v == -1; }, origin.get(),
        nullptr, newSurface.get(), nullptr );

    if ( blitCall.HasError() ) return nullptr;

    return newSurface;
}

elImage::surface_t
elImage::ConvertRawSurface( const surface_t origin,
                            const uint32_t  format ) noexcept
{
    if ( origin.get() == nullptr ) return origin;

    auto result = elImage::CreateSurfacePointer(
        SDL_ConvertSurfaceFormat( origin.get(), format, 0 ) );
    if ( result.get() == nullptr )
        LOG_ERROR( 0 ) << "could not convert surface : "
                       << elImage::GetIMGError();
    return result;
}

std::string
elImage::GetIMGError() noexcept
{
    std::string error = IMG_GetError();
    SDL_ClearError();
    return error;
}

elImage::surface_t
elImage::CreateSurfacePointer( SDL_Surface *raw ) noexcept
{
    return std::shared_ptr< SDL_Surface >(
        raw, []( SDL_Surface *s ) { SDL_FreeSurface( s ); } );
}

} // namespace GLAPI
} // namespace el3D
