#include "GLAPI/elFontCache.hpp"

namespace el3D
{
namespace GLAPI
{
elFontCache::elFontCache()
{
}

bb::elExpected< elFont*, bool >
elFontCache::Get( const std::string& fontFile, const uint64_t fontSize,
                  const bool fastRenderer ) noexcept
{
    auto iter =
        this->cache.find( property_t{ fontFile, fontSize, fastRenderer } );

    if ( iter != this->cache.end() )
        return bb::Success( iter->second );
    else
    {
        elFont* newFont = new elFont( fontFile, fontSize, fastRenderer );
        if ( newFont->font == nullptr ) return bb::Error( false );
        this->cache[property_t( fontFile, fontSize, fastRenderer )] = newFont;

        return bb::Success( newFont );
    }
}

elFontCache::~elFontCache()
{
    this->ClearCache();
}

void
elFontCache::ClearCache() noexcept
{
    for ( auto& e : this->cache )
        delete e.second;

    this->cache.clear();
}

elFontCache::elFontCache( elFontCache&& rhs )
{
    *this = std::move( rhs );
}

elFontCache&
elFontCache::operator=( elFontCache&& rhs )
{
    if ( this != &rhs )
    {
        this->ClearCache();
        this->cache = std::move( rhs.cache );
        rhs.cache.clear();
    }
    return *this;
}
} // namespace GLAPI
} // namespace el3D
