#include "GLAPI/elFont.hpp"

#include "GuiGL/common.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace GLAPI
{
elFont::elFont( const std::string &fontFile, const uint64_t fontSize,
                const bool fastRenderer )
    : font( TTF_OpenFont( fontFile.c_str(),
                          static_cast< int >( static_cast< float >( fontSize ) *
                                              GuiGL::GetDPIScaling() ) ) ),
      fastRenderer( fastRenderer )
{
    if ( this->font == nullptr )
    {
        LOG_ERROR( 0 ) << "could not initialize font " << fontFile << ":"
                       << fontSize << " " << std::string( TTF_GetError() );
    }
}

elFont::~elFont() noexcept
{
    if ( this->font != nullptr ) TTF_CloseFont( this->font );
}

elFont::elFont( elFont &&rhs )
    : surface( std::move( rhs.surface ) ), font( std::move( rhs.font ) ),
      fastRenderer( std::move( rhs.fastRenderer ) )
{
    rhs.font = nullptr;
}

elFont &
elFont::operator=( elFont &&rhs )
{
    if ( this != &rhs )
    {
        this->surface      = std::move( rhs.surface );
        this->font         = std::move( rhs.font );
        this->fastRenderer = rhs.fastRenderer;

        rhs.font = nullptr;
    }
    return *this;
}

void
elFont::SetText( const std::string &text, const SDL_Color &color )
{
    std::string  renderedText( ( text.size() != 0 ) ? text : " " );
    SDL_Surface *newSurface;

    if ( this->fastRenderer )
        newSurface =
            TTF_RenderUTF8_Solid( this->font, renderedText.c_str(), color );
    else
        newSurface =
            TTF_RenderUTF8_Blended( this->font, renderedText.c_str(), color );

    if ( newSurface == nullptr )
        LOG_ERROR( 0 ) << "could not init font surface with the text: "
                       << std::string( TTF_GetError() );

    this->surface = elImage::Create( FromSurface, newSurface ).GetValue();
}

utils::byte_t *
elFont::GetBytePointer() const noexcept
{
    return static_cast< Uint8 * >( this->surface->GetBytePointer() );
}

void
elFont::CanvasResize( const uint64_t x, const uint64_t y ) noexcept
{
    this->surface->CanvasResize( x, y );
}

void
elFont::Resize( const uint64_t x, const uint64_t y ) noexcept
{
    this->surface->Resize( x, y );
}

dimension_t
elFont::GetTextureSize() const noexcept
{
    return this->surface->GetDimensions();
}

dimension_t
elFont::GetTextSize( const std::string &text ) const noexcept
{
    int width, height;
    TTF_SizeUTF8( this->font, text.c_str(), &width, &height );
    return { static_cast< uint64_t >( static_cast< float >( width ) /
                                      GuiGL::GetDPIScaling() ),
             static_cast< uint64_t >( static_cast< float >( height ) /
                                      GuiGL::GetDPIScaling() ) };
}

uint64_t
elFont::GetFontHeight() const noexcept
{
    return static_cast< uint64_t >(
        static_cast< float >( TTF_FontHeight( this->font ) ) /
        GuiGL::GetDPIScaling() );
}
} // namespace GLAPI
} // namespace el3D
