#include "GLAPI/elEventHandler.hpp"

#include "GLAPI/elEvent.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

namespace el3D
{
namespace GLAPI
{
bool
elEventHandler::IsActive() const noexcept
{
    return this->isActive;
}

void
elEventHandler::Activate() noexcept
{
    if ( this->isActive ) return;
    this->isActive = true;
}

void
elEventHandler::Deactivate() noexcept
{
    if ( !this->isActive ) return;
    this->isActive = false;
}

bb::product_ptr< elEvent >
elEventHandler::CreateEvent(
    const std::function< void( elEvent * ) > &cleanupCallback ) noexcept
{
    return this->factory.CreateProductWithOnRemoveCallback< elEvent >(
        [this, cleanupCallback]( auto event )
        {
            if ( cleanupCallback ) cleanupCallback( event );
            this->UnregisterEvent( event );
        },
        this );
}

bb::product_ptr< elEvent >
elEventHandler::CreateEventForColorIndex( const uint32_t colorIndex ) noexcept
{
    if ( this->eventsWithColorIndexSize <= colorIndex )
    {
        this->eventsWithColorIndex.resize( colorIndex + 1 );
        this->eventsWithColorIndexSize = colorIndex + 1;
    }

    auto newEvent = this->factory.CreateProductWithOnRemoveCallback< elEvent >(
        [colorIndex, this]( auto eventPtr )
        {
            if ( this->temporaryColorIndex == colorIndex )
            {
                auto tempIter =
                    bb::find( this->temporaryEventVector, eventPtr );
                if ( tempIter != this->temporaryEventVector.end() )
                    *tempIter = nullptr;
            }

            auto iter =
                bb::find( this->eventsWithColorIndex[colorIndex], eventPtr );
            if ( iter != this->eventsWithColorIndex[colorIndex].end() )
                this->eventsWithColorIndex[colorIndex].erase( iter );
        },
        this );

    this->eventsWithColorIndex[colorIndex].emplace_back( newEvent.Get() );
    return newEvent;
}

uint64_t
elEventHandler::GetCurrentEventLoopCycle() const noexcept
{
    return this->currentEventLoopCycle;
}

void
elEventHandler::PollCurrentEvents(
    const uint32_t currentEventColorIndex ) noexcept
{
    this->currentEventLoopCycle++;
    SDL_Event event;
    while ( SDL_PollEvent( &event ) )
        this->HandleEvents( event, currentEventColorIndex );
}

void
elEventHandler::RegisterEvent( const elEvent *const event ) noexcept
{
    this->registerUnregisterCallbacks.emplace_back(
        [this, event]() {
            this->registeredEvents.insert( { event, false } );
        } );
}

void
elEventHandler::UnregisterEvent( const elEvent *const event ) noexcept
{
    auto iter = this->registeredEvents.find( { event, false } );
    if ( iter != this->registeredEvents.end() ) iter->isRemoved = true;

    // callback needs to be created event when the registeredEvents is not found
    // since UnregisterEvent could be called before RegisterEvent (can happen
    // when a gui widget is set up outside the main loop)
    this->registerUnregisterCallbacks.emplace_back(
        [this, event]() {
            this->registeredEvents.erase( { event, false } );
        } );
}

void
elEventHandler::HandleEvents( const SDL_Event sdlEvent,
                              const uint32_t  currentEventColorIndex ) noexcept
{
    constexpr uint32_t INVALID_COLOR_INDEX =
        std::numeric_limits< uint32_t >::max();

    if ( this->isActive )
    {
        if ( currentEventColorIndex != INVALID_COLOR_INDEX &&
             currentEventColorIndex < this->eventsWithColorIndexSize )
        {
            this->temporaryColorIndex = currentEventColorIndex;
            this->temporaryEventVector =
                this->eventsWithColorIndex[currentEventColorIndex];

            for ( auto &event : this->temporaryEventVector )
                if ( event != nullptr && event->callback )
                    event->callback( sdlEvent );
        }

        for ( auto &e : this->registeredEvents )
        {
            auto &event = e.event;
            if ( !e.isRemoved && event )
            {
                if ( event->callback ) event->callback( sdlEvent );

                if ( event->dtorComparator &&
                     event->dtorComparator( sdlEvent ) )
                {
                    this->UnregisterEvent( event );
                    if ( event->afterRemovalCallback )
                        event->afterRemovalCallback( sdlEvent );
                    if ( event->automaticDeRegistrationCallback )
                        event->automaticDeRegistrationCallback();
                }
            }
        }
    }

    for ( auto &callback : this->registerUnregisterCallbacks )
        callback();

    this->registerUnregisterCallbacks.clear();
}

bool
elEventHandler::event_t::operator==( const elEvent *const event ) const noexcept
{
    return this->event == event;
}

bool
elEventHandler::event_t::operator==( const event_t &rhs ) const noexcept
{
    return this->event == rhs.event;
}

bool
elEventHandler::event_t::operator<( const event_t &rhs ) const noexcept
{
    return this->event < rhs.event;
}


} // namespace GLAPI
} // namespace el3D
