#include "luaBindings/units/Time_binding.hpp"

#include "units/Time.hpp"
#include "units/units_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace units
{
#ifdef ADD_LUA_BINDINGS
LUA_F( Time )
{
    using namespace ::el3D::units;
    lua::elLuaScript::class_t newClass( "Time" );
    newClass.AddPrivateConstructor< Time >()
        .AddMethod< Time >( "GetNanoSeconds", &Time::GetNanoSeconds )
        .AddMethod< Time >( "GetMicroSeconds", &Time::GetMicroSeconds )
        .AddMethod< Time >( "GetMilliSeconds", &Time::GetMilliSeconds )
        .AddMethod< Time >( "GetSeconds", &Time::GetSeconds )
        .AddMethod< Time >( "GetMinutes", &Time::GetMinutes )
        .AddMethod< Time >( "GetHours", &Time::GetHours )
        .AddMethod< Time >( "GetDays", &Time::GetDays )
        .AddStaticMethod( "NanoSeconds", Time::NanoSeconds )
        .AddStaticMethod( "MicroSeconds", Time::MicroSeconds )
        .AddStaticMethod( "MilliSeconds", Time::MilliSeconds )
        .AddStaticMethod( "Seconds", Time::Seconds )
        .AddStaticMethod( "Minutes", Time::Minutes )
        .AddStaticMethod( "Hours", Time::Hours )
        .AddStaticMethod( "Days", Time::Days );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace units
} // namespace luaBindings
} // namespace el3D
