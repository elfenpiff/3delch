#include "luaBindings/stl_specific/vector_binding.hpp"

#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elObjectRegistry.hpp"
#include "luaBindings/stl/vector_binding.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elAllocator.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

#include <glm/glm.hpp>

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_F( vector )
{
    std::string nameSpaceName = "stl";
    luaBindings::stl::AddLuaVectorBinding<
        utils::byte_t, utils::elAllocator< utils::byte_t > >(
        "vector_byte_t", utils::NAMESPACE_NAME );
    luaBindings::stl::AddLuaVectorBinding< std::string >( "vector_string",
                                                          nameSpaceName );

    luaBindings::stl::AddLuaVectorBinding< glm::uvec2 >(
        "vector_uvec2", GuiGL::NAMESPACE_NAME );
    luaBindings::stl::AddLuaVectorBinding<
        OpenGL::elObjectRegistry::texture_t >( "vector_texture_t",
                                               OpenGL::NAMESPACE_NAME );
}
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
