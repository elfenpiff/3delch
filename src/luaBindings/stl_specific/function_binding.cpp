#include "luaBindings/stl_specific/function_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "luaBindings/stl/function_binding.hpp"

#include <SDL.h>

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_F( function )
{
    std::string nameSpaceName = "stl";

    stl::AddLuaFunctionBinding< void >( "void_function_void", nameSpaceName );
    stl::AddLuaFunctionBinding< bool, SDL_Event >(
        "bool_function_SDL_Event", ::el3D::GLAPI::NAMESPACE_NAME );
    stl::AddLuaFunctionBinding< void, SDL_Event >(
        "void_function_SDL_Event", ::el3D::GLAPI::NAMESPACE_NAME );
}
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
