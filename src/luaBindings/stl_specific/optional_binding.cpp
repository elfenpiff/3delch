#include "luaBindings/stl_specific/optional_binding.hpp"

#include "luaBindings/stl/optional_binding.hpp"
#include "network/elNetworkSocket_internal.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_F( optional )
{
    stl::AddLuaOptionalBinding<
        ::el3D::network::elNetworkSocket::internal::message_t >(
        "optional_message_t", ::el3D::network::NAMESPACE_NAME );
}
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
