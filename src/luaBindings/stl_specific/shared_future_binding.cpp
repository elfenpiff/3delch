#include "luaBindings/stl_specific/shared_future_binding.hpp"

#include "luaBindings/stl/shared_future_binding.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace stl_specific
{
#ifdef ADD_LUA_BINDINGS
LUA_F( shared_future )
{
    std::string nameSpaceName = "stl";

    luaBindings::stl::AddLuaSharedFutureBinding< bool >( "shared_future_bool",
                                                         nameSpaceName );
}
#endif
} // namespace stl_specific
} // namespace luaBindings
} // namespace el3D
