#include "luaBindings/GuiGL/elWidget_PopupMenu_binding.hpp"

#include "luaBindings/GuiGL/elWidget_base_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elWidget_PopupMenu )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetBinding< elWidget_PopupMenu >(
        "elWidget_PopupMenu" );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
