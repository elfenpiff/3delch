#include "luaBindings/GuiGL/elWidget_Slider_binding.hpp"

#include "luaBindings/GuiGL/elWidget_base_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elWidget_Slider )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetBinding< elWidget_Slider >(
        "elWidget_Slider" );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
