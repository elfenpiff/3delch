#include "luaBindings/GuiGL/common_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( GUI_Common )
{
    using namespace ::el3D::GuiGL;
    lua::elLuaScript::RegisterGlobal< int >(
        "widgetStates",
        {{"STATE_DEFAULT", STATE_DEFAULT},
         {"STATE_INACTIVE", STATE_INACTIVE},
         {"STATE_HOVER", STATE_HOVER},
         {"STATE_CLICKED", STATE_CLICKED},
         {"STATE_SELECTED", STATE_SELECTED}},
        NAMESPACE_NAME );

    lua::elLuaScript::RegisterGlobal< int >(
        "positionCorner",
        {{"POSITION_TOP_LEFT", POSITION_TOP_LEFT},
         {"POSITION_TOP_RIGHT", POSITION_TOP_RIGHT},
         {"POSITION_BOTTOM_LEFT", POSITION_BOTTOM_LEFT},
         {"POSITION_BOTTOM_RIGHT", POSITION_BOTTOM_RIGHT},
         {"POSITION_CENTER", POSITION_CENTER},
         {"POSITION_TOP_CENTER", POSITION_TOP_CENTER},
         {"POSITION_BOTTOM_CENTER", POSITION_BOTTOM_CENTER},
         {"POSITION_CENTER_LEFT", POSITION_CENTER_LEFT},
         {"POSITION_CENTER_RIGHT", POSITION_CENTER_RIGHT}},
        NAMESPACE_NAME );

    lua::elLuaScript::RegisterGlobal< int >(
        "position",
        {{"POSITION_TOP", POSITION_TOP},
         {"POSITION_BOTTOM", POSITION_BOTTOM},
         {"POSITION_LEFT", POSITION_LEFT},
         {"POSITION_RIGHT", POSITION_RIGHT},
         {"POSITION_MIDDLE", POSITION_MIDDLE}},
        NAMESPACE_NAME );

    lua::elLuaScript::RegisterGlobal< int >(
        "widgetType",
        {{"TYPE_BASE", TYPE_BASE},
         {"TYPE_EMPTY", TYPE_EMPTY},
         {"TYPE_WINDOW", TYPE_WINDOW},
         {"TYPE_MOUSE_CURSOR", TYPE_MOUSE_CURSOR},
         {"TYPE_IMAGE", TYPE_IMAGE},
         {"TYPE_TEXT_CURSOR", TYPE_TEXT_CURSOR},
         {"TYPE_LABEL", TYPE_LABEL},
         {"TYPE_ENTRY", TYPE_ENTRY},
         {"TYPE_BUTTON", TYPE_BUTTON},
         {"TYPE_SLIDER", TYPE_SLIDER},
         {"TYPE_TABLE", TYPE_TABLE},
         {"TYPE_LISTBOX", TYPE_LISTBOX},
         {"TYPE_DROPBUTTON", TYPE_DROPBUTTON},
         {"TYPE_SHADERANIMATION", TYPE_SHADERANIMATION},
         {"TYPE_CHECKBUTTON", TYPE_CHECKBUTTON},
         {"TYPE_PROGRESS", TYPE_PROGRESS},
         {"TYPE_TABBEDWINDOW", TYPE_TABBEDWINDOW},
         {"TYPE_MENU", TYPE_MENU},
         {"TYPE_MENUBAR", TYPE_MENUBAR},
         {"TYPE_POPUPMENU", TYPE_POPUPMENU},
         {"TYPE_TEXTBOX", TYPE_TEXTBOX},
         {"TYPE_INFOMESSAGE", TYPE_INFOMESSAGE},
         {"TYPE_YESNODIALOG", TYPE_YESNODIALOG},
         {"TYPE_TREE", TYPE_TREE},
         {"TYPE_FILEBROWSER", TYPE_FILEBROWSER},
         {"TYPE_TITLEBAR", TYPE_TITLEBAR},
         {"TYPE_GRAPH", TYPE_GRAPH}},
        NAMESPACE_NAME );

    AddCTorAndRegisterLuaClass< glm::vec3, float, float, float >( "glm_vec3" );
    AddCTorAndRegisterLuaClass< glm::vec4, float, float, float, float >(
        "glm_vec4" );

    lua::elLuaScript::class_t glm_vec2( "glm_vec2" ); // NOLINT
    glm_vec2.AddConstructor< glm::vec2, float, float >();
    glm_vec2.AddVariable( "x", &glm::vec2::x );
    glm_vec2.AddVariable( "y", &glm::vec2::y );
    lua::elLuaScript::RegisterLuaClass( glm_vec2, NAMESPACE_NAME );

    lua::elLuaScript::class_t glm_uvec2( "glm_uvec2" ); // NOLINT
    glm_uvec2.AddConstructor< glm::uvec2, int, int >();
    glm_uvec2.AddVariable( "x", &glm::uvec2::x );
    glm_uvec2.AddVariable( "y", &glm::uvec2::y );
    lua::elLuaScript::RegisterLuaClass( glm_uvec2, NAMESPACE_NAME );
}
#endif

} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
