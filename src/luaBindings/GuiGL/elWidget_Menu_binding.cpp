#include "luaBindings/GuiGL/elWidget_Menu_binding.hpp"

#include "luaBindings/GuiGL/elWidget_base_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GuiGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elWidget_Menu )
{
    using namespace ::el3D::GuiGL;
    ::el3D::luaBindings::GuiGL::AddLuaWidgetBinding< elWidget_Menu >(
        "elWidget_Menu" );
}
#endif
} // namespace GuiGL
} // namespace luaBindings
} // namespace el3D
