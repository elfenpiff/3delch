#include "luaBindings/GLAPI/elImage_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/elImage.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GLAPI
{
using namespace ::el3D::GLAPI;
#ifdef ADD_LUA_BINDINGS
LUA_F( elImage )
{
    lua::elLuaScript::RegisterGlobal< int >(
        "sdlPixelFormat",
        { { "SDL_PIXELFORMAT_RGBA32", SDL_PIXELFORMAT_RGBA32 } },
        NAMESPACE_NAME );

    lua::elLuaScript::class_t newClass( "elImage" );
    // newClass.AddConstructor< elImage >()
    //    .AddStaticMethod( "CreateFromFile",
    //                      &elImage::Lua_Create< std::string, uint32_t > )
    //    .AddStaticMethod(
    //        "CreateFromByteStream",
    //        &elImage::Lua_Create< utils::byteStream_t, uint32_t > )
    //    .AddMethod< elImage >( "Resize", &elImage::Resize )
    //    .AddMethod< elImage >( "CanvasResize", &elImage::CanvasResize )
    //    .AddMethod< elImage >( "GetBytePointer", &elImage::GetBytePointer )
    //    .AddMethod< elImage >( "GetByteStream", &elImage::GetByteStream )
    //    .AddMethod< elImage >( "GetDimensions", &elImage::GetDimensions )
    //    .AddMethod< elImage >( "GetBytesPerPixel", &elImage::GetBytesPerPixel
    //    ) .AddMethod< elImage >( "GetFormat", &elImage::GetFormat );
    // lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif

} // namespace GLAPI
} // namespace luaBindings
} // namespace el3D
