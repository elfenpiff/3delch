#include "luaBindings/GLAPI/elEventHandler_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/elEventHandler.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GLAPI
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elEventHandler )
{
    lua::elLuaScript::class_t newClass( "::GLAPI::elEventHandler" );
    newClass.AddPrivateConstructor<::el3D::GLAPI::elEventHandler >();
    /// newClass.AddMethod<::GLAPI::elEventHandler >(
    ///     "CreateEvent", &::GLAPI::elEventHandler::CreateEvent );
    lua::elLuaScript::RegisterLuaClass( newClass,
                                        ::el3D::GLAPI::NAMESPACE_NAME );
}
#endif
} // namespace GLAPI
} // namespace luaBindings
} // namespace el3D
