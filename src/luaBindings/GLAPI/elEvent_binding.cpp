#include "luaBindings/GLAPI/elEvent_binding.hpp"

#include "GLAPI/GLAPI_NAMESPACE_NAME.hpp"
#include "GLAPI/elEvent.hpp"

namespace el3D
{
namespace luaBindings
{
namespace GLAPI
{
using namespace ::el3D::GLAPI;

#ifdef ADD_LUA_BINDINGS
LUA_F( elEvent )
{
    lua::elLuaScript::class_t newClass( "elEvent" );
    newClass.AddPrivateConstructor< elEvent >();
#if 0
    newClass.AddMethod< elEvent >( "Unregister", &elEvent::Unregister );
    newClass.AddMethod< elEvent >( "Register", &elEvent::Register );
    newClass.AddMethod< elEvent >( "Reset", &elEvent::Reset );
    newClass.AddMethod< elEvent >( "SetRemovalCondition",
                                   &elEvent::SetRemovalCondition );
    newClass.AddMethod< elEvent >( "SetCallback", &elEvent::SetCallback );
    newClass.AddMethod< elEvent >( "SetAfterRemovalCallback",
                                   &elEvent::SetAfterRemovalCallback );
    newClass.AddMethod< elEvent >( "HasDestructionCondition",
                                   &elEvent::HasDestructionCondition );
    newClass.AddStaticMethod( "StartTextInput", elEvent::StartTextInput );
    newClass.AddStaticMethod( "StopTextInput", elEvent::StopTextInput );
    newClass.AddStaticMethod( "HasTextInput", elEvent::HasTextInput );
    newClass.AddStaticMethod( "MultiInstanceStartTextInput",
                              elEvent::MultiInstanceStartTextInput );
    newClass.AddStaticMethod( "MultiInstanceStopTextInput",
                              elEvent::MultiInstanceStopTextInput );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
#endif
}
#endif
} // namespace GLAPI
} // namespace luaBindings
} // namespace el3D
