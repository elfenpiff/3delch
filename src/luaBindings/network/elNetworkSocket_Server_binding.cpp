#include "luaBindings/network/elNetworkSocket_Server_binding.hpp"

#include "luaBindings/network/elNetworkSocket_base_binding.hpp"
#include "network/elNetworkSocket_Server.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elNetworkSocket_Server )
{
    using namespace ::el3D::network;

    lua::elLuaScript::class_t newClass( "elNetworkSocket_Server" );
    newClass.AddConstructor< elNetworkSocket_Server >()
        .AddMethod< elNetworkSocket_Server >( "Send",
                                              &elNetworkSocket_Server::Send )
        .AddMethod< elNetworkSocket_Server >( "Listen",
                                              &elNetworkSocket_Server::Listen )
        .AddMethod< elNetworkSocket_Server >(
            "BlockingReceive", &elNetworkSocket_Server::BlockingReceive )
        .AddMethod< elNetworkSocket_Server >(
            "StopListen", &elNetworkSocket_Server::StopListen )
        .AddMethod< elNetworkSocket_Server >(
            "GetConnections", &elNetworkSocket_Server::GetConnections );

    luaBindings::network::AddLuaNetworkSocket_baseInterface<
        elNetworkSocket_Server >( newClass );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace network
} // namespace luaBindings
} // namespace el3D
