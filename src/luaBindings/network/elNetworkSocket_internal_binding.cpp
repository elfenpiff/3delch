#include "luaBindings/network/elNetworkSocket_internal_binding.hpp"

#include "network/elNetworkSocket_internal.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elNetworkSocket_internal )
{
    using namespace ::el3D::network::elNetworkSocket::internal;
    using namespace ::el3D::network;

    lua::elLuaScript::class_t connection( "connection_t" );
    connection.AddConstructor< connection_t >()
        .AddVariable( "socket", &connection_t::socket )
        .AddVariable( "connectionID", &connection_t::connectionID )
        .AddVariable( "hostIP", &connection_t::hostIP )
        .AddVariable( "hostName", &connection_t::hostName );
    lua::elLuaScript::RegisterLuaClass( connection, NAMESPACE_NAME );

    lua::elLuaScript::class_t message( "message_t" );
    message.AddConstructor< message_t >()
        .AddVariable( "connection", &message_t::connection )
        .AddVariable( "message", &message_t::message );
    lua::elLuaScript::RegisterLuaClass( message, NAMESPACE_NAME );
}
#endif
} // namespace network
} // namespace luaBindings
} // namespace el3D
