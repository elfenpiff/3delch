#include "luaBindings/network/elNetworkSocket_Client_binding.hpp"

#include "luaBindings/network/elNetworkSocket_base_binding.hpp"
#include "network/elNetworkSocket_Client.hpp"
#include "network/network_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace network
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elNetworkSocket_Client )
{
    using namespace ::el3D::network;

    lua::elLuaScript::class_t newClass( "elNetworkSocket_Client" );
    newClass.AddConstructor< elNetworkSocket_Client >()
        .AddMethod< elNetworkSocket_Client >( "Send",
                                              &elNetworkSocket_Client::Send )
        .AddMethod< elNetworkSocket_Client >( "Connect",
                                              &elNetworkSocket_Client::Connect )
        .AddMethod< elNetworkSocket_Client >(
            "TimedReceive", &elNetworkSocket_Client::TimedReceive )
        .AddMethod< elNetworkSocket_Client >(
            "TryReceive", &elNetworkSocket_Client::TryReceive )
        .AddMethod< elNetworkSocket_Client >(
            "BlockingReceive", &elNetworkSocket_Client::BlockingReceive )
        .AddMethod< elNetworkSocket_Client >(
            "Disconnect", &elNetworkSocket_Client::Disconnect )
        .AddMethod< elNetworkSocket_Client >(
            "GetConnectionInformation",
            &elNetworkSocket_Client::GetConnectionInformation );
    luaBindings::network::AddLuaNetworkSocket_baseInterface<
        elNetworkSocket_Client >( newClass );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace network
} // namespace luaBindings
} // namespace el3D
