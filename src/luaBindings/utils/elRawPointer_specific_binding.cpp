#include "luaBindings/utils/elRawPointer_specific_binding.hpp"

#include "luaBindings/utils/elRawPointer_binding.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elRawPointer_specific )
{
    AddLuaRawPointerBinding<::el3D::utils::byte_t >(
        "byte_ptr", ::el3D::utils::NAMESPACE_NAME );
}
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
