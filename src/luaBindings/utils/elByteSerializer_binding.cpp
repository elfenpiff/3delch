#include "luaBindings/utils/elByteSerializer_binding.hpp"

#include "utils/elByteSerializer.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
#ifdef ADD_LUA_BINDINGS

template < typename T >
void
Register( const std::string &name )
{
    lua::elLuaScript::class_t newClass( name );
    newClass.AddConstructor< T >();
    lua::elLuaScript::RegisterLuaClass( newClass,
                                        ::el3D::utils::NAMESPACE_NAME );
}

LUA_F( elByteSerializer )
{
}
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
