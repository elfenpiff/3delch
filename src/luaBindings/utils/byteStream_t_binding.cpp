#include "luaBindings/utils/byteStream_t_binding.hpp"

#include "utils/byteStream_t.hpp"
#include "utils/utils_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace luaBindings
{
namespace utils
{
#ifdef ADD_LUA_BINDINGS
LUA_F( byteStream_t )
{
    using namespace ::el3D::utils;
    lua::elLuaScript::class_t newClass( "byteStream_t" );
    newClass.AddConstructor< byteStream_t >();
    newClass.AddVariable( "stream", &byteStream_t::stream );
    newClass.AddMethod< byteStream_t >( "GetHumanReadableString",
                                        &byteStream_t::GetHumanReadableString );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace utils
} // namespace luaBindings
} // namespace el3D
