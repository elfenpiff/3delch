#include "luaBindings/3Delch/3Delch_binding.hpp"

namespace el3D
{
namespace luaBindings
{
#ifdef ADD_LUA_BINDINGS
LUA_F( _3Delch )
{
    lua::elLuaScript::class_t newClass( "_3Delch" );
    newClass.AddPrivateConstructor< _3Delch::_3Delch >()
        .AddMethod< _3Delch::_3Delch >( "Window", &_3Delch::_3Delch::Window )
        .AddMethod< _3Delch::_3Delch >( "StartMainLoop",
                                        &_3Delch::_3Delch::StartMainLoop )
        .AddMethod< _3Delch::_3Delch >(
            "StopMainLoopAndDestroy",
            &_3Delch::_3Delch::StopMainLoopAndDestroy )
        .AddMethod< _3Delch::_3Delch >( "GetWindowSize",
                                        &_3Delch::_3Delch::GetWindowSize )
        .AddMethod< _3Delch::_3Delch >(
            "SetMultiSamplingFactor",
            &_3Delch::_3Delch::SetMultiSamplingFactor );
    lua::elLuaScript::RegisterLuaClass( newClass, "_3Delch" );

    lua::elLuaScript::class_t skyboxTextureConfig( "elSkybox_textureConfig" );
    skyboxTextureConfig
        .AddConstructor< ::el3D::HighGL::elSkybox::textureConfig_t >();
    skyboxTextureConfig.AddVariable(
        "positiveXFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::positiveXFile );
    skyboxTextureConfig.AddVariable(
        "negativeXFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::negativeXFile );
    skyboxTextureConfig.AddVariable(
        "positiveYFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::positiveYFile );
    skyboxTextureConfig.AddVariable(
        "negativeYFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::negativeYFile );
    skyboxTextureConfig.AddVariable(
        "positiveZFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::positiveZFile );
    skyboxTextureConfig.AddVariable(
        "negativeZFile",
        &::el3D::HighGL::elSkybox::textureConfig_t::negativeZFile );
    lua::elLuaScript::RegisterLuaClass( skyboxTextureConfig, "_3Delch" );
}
#endif
} // namespace luaBindings
} // namespace el3D
