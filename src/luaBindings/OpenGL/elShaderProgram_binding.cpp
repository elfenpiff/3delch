#include "luaBindings/OpenGL/elShaderProgram_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elShaderProgram.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elShaderProgram )
{
    lua::elLuaScript::class_t newStruct( "elShaderProgram::shaderAttribute_t" );
    using namespace ::el3D::OpenGL;
    newStruct.AddConstructor< elShaderProgram::shaderAttribute_t >()
        .AddVariable( "id", &elShaderProgram::shaderAttribute_t::id )
        .AddVariable( "identifier",
                      &elShaderProgram::shaderAttribute_t::identifier )
        .AddVariable( "size", &elShaderProgram::shaderAttribute_t::size )
        .AddVariable( "dataType",
                      &elShaderProgram::shaderAttribute_t::dataType )
        .AddVariable( "divisor", &elShaderProgram::shaderAttribute_t::divisor );
    // TODO:
    //        .AddVariable< shaderAttribute_t, decltype(
    //        &shaderAttribute_t::pointer ),
    //                     GLvoid * >( "pointer", &shaderAttribute_t::pointer );
    lua::elLuaScript::RegisterLuaClass( newStruct, NAMESPACE_NAME );

    lua::elLuaScript::class_t newClass( "elShaderProgram" );
    newClass.AddPrivateConstructor< elShaderProgram >()
        .AddMethod< elShaderProgram >( "Bind", &elShaderProgram::Bind )
        .AddMethod< elShaderProgram >( "Unbind", &elShaderProgram::Unbind )
        .AddMethod< elShaderProgram >( "GetAttribute",
                                       &elShaderProgram::GetAttribute )
        .AddMethod< elShaderProgram >( "GetAttribLocation",
                                       &elShaderProgram::GetAttributeLocation )
        .AddMethod< elShaderProgram >( "GetUniformLocation",
                                       &elShaderProgram::GetUniformLocation );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif


} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
