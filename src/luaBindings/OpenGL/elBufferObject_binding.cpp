#include "luaBindings/OpenGL/elBufferObject_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elBufferObject.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elBufferObject )
{
    lua::elLuaScript::class_t newClass( "elBufferObject" );
    using namespace ::el3D::OpenGL;

    newClass.AddConstructor< elBufferObject, size_t, GLenum, GLenum >()
        .AddMethod< elBufferObject >( "Bind", &elBufferObject::Bind )
        .AddMethod< elBufferObject >( "Unbind", &elBufferObject::Unbind )
        .AddMethod< elBufferObject >( "SetBufferTarget",
                                      &elBufferObject::SetBufferTarget )
        .AddMethod< elBufferObject >( "SetBufferUsage",
                                      &elBufferObject::SetBufferUsage )
        .AddMethod< elBufferObject >( "GetBufferParameteriv",
                                      &elBufferObject::GetBufferParameteriv );

    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}

#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
