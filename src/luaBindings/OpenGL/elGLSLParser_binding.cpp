#include "luaBindings/OpenGL/elGLSLParser_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elGLSLParser.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elGLSLParser )
{
    using namespace ::el3D::OpenGL;
    lua::elLuaScript::class_t newClass( "elGLSLParser" );
    newClass.AddPrivateConstructor< elGLSLParser >();
    newClass.AddStaticMethod( "AddShaderIncludePath",
                              elGLSLParser::AddShaderIncludePath );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
