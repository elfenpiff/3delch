#include "luaBindings/OpenGL/elVertexArrayObject_binding.hpp"

#include "OpenGL/OpenGL_NAMESPACE_NAME.hpp"
#include "OpenGL/elVertexArrayObject.hpp"

namespace el3D
{
namespace luaBindings
{
namespace OpenGL
{

#ifdef ADD_LUA_BINDINGS
LUA_F( elVertexArrayObject )
{
    using namespace ::el3D::OpenGL;
    lua::elLuaScript::class_t newClass( "elVertexArrayObject" );
    newClass.AddConstructor< elVertexArrayObject, size_t >()
        .AddMethod< elVertexArrayObject >( "Bind", &elVertexArrayObject::Bind )
        .AddMethod< elVertexArrayObject >( "Unbind",
                                           &elVertexArrayObject::Unbind )
        .AddMethod< elVertexArrayObject >(
            "EnableVertexAttribArray",
            &elVertexArrayObject::EnableVertexAttribArray )
        .AddMethod< elVertexArrayObject >(
            "DisableVertexAttribArray",
            &elVertexArrayObject::DisableVertexAttribArray );
    lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif


} // namespace OpenGL
} // namespace luaBindings
} // namespace el3D
