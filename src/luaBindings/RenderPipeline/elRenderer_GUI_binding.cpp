#include "luaBindings/RenderPipeline/elRenderer_GUI_binding.hpp"

#include "GuiGL/handler/widgetFactory.hpp"
#include "RenderPipeline/RenderPipeline_NAMESPACE_NAME.hpp"
#include "RenderPipeline/elRenderer_GUI.hpp"
#include "luaBindings/GuiGL/handler/widgetFactory_binding.hpp"
#include "luaBindings/RenderPipeline/elRenderer_base_binding.hpp"

namespace el3D
{
namespace luaBindings
{
namespace RenderPipeline
{
#ifdef ADD_LUA_BINDINGS
LUA_F( elRenderer_GUI )
{
    // using namespace ::el3D::RenderPipeline;
    // lua::elLuaScript::class_t newClass( "elRenderer_GUI" );
    // AddMethodsToLuaClass_elRenderer_base< elRenderer_GUI >( newClass );

    // newClass.AddPrivateConstructor< elRenderer_GUI >()
    //     .AddMethod< elRenderer_GUI >( "GetRoot", &elRenderer_GUI::GetRoot );
    // //        .AddMethod<::RenderPipeline::elRenderer_GUI >(
    // //            "Reshape", &::RenderPipeline::elRenderer_GUI::Reshape );

    // //::el3D::luaBindings::GuiGL::handler::AddLuaWidgetFactoryCreateInterface<
    // //    elRenderer_GUI, elRenderer_GUI >( newClass );

    // lua::elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif
} // namespace RenderPipeline
} // namespace luaBindings
} // namespace el3D
