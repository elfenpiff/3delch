#include "concurrent/elBackgroundTask.hpp"

namespace el3D
{
namespace concurrent
{
elBackgroundTask::elBackgroundTask() noexcept
{
}

elBackgroundTask::~elBackgroundTask()
{
    for ( auto& t : this->tasks )
        if ( t.joinable() ) t.join();
}

void
elBackgroundTask::AddTask( const std::function< void() >& task ) noexcept
{
    auto   reuseableTaskID = this->reuseableTasks->Pop();
    size_t taskID          = 0;
    if ( reuseableTaskID.has_value() )
    {
        taskID = reuseableTaskID.value();
        this->tasks[taskID].join();
    }
    else
    {
        this->tasks.emplace_back();
        taskID = this->tasks.size() - 1;
    }

    this->tasks[taskID] = std::thread(
        [this, taskID, task]
        {
            task();
            this->reuseableTasks->Push( taskID );
        } );
}


} // namespace concurrent
} // namespace el3D
