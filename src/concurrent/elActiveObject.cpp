#include "concurrent/elActiveObject.hpp"

#include <mutex>

namespace el3D
{
namespace concurrent
{
elActiveObject::elActiveObject( elThreadPool& threadPool ) noexcept
    : threadPool( threadPool )
{
}

elActiveObject::~elActiveObject()
{
    this->AllowToAddNewTasks( false );
    this->WaitForTasksToFinish();
}

void
elActiveObject::AddTask( const std::function< void() >& f ) noexcept
{
    {
        std::lock_guard< std::mutex > lock( this->mtx );
        if ( !this->addingTasksIsAllowed ) return;
        this->numberOfActiveTasks++;
    }

    this->threadPool.AddTask(
        [this, f]
        {
            f();
            {
                std::lock_guard< std::mutex > lock( this->mtx );
                this->numberOfActiveTasks--;
            }
            this->waitForTasksToFinish.notify_one();
        } );
}

void
elActiveObject::AllowToAddNewTasks( const bool v ) noexcept
{
    std::lock_guard< std::mutex > lock( this->mtx );
    this->addingTasksIsAllowed = v;
}

void
elActiveObject::WaitForTasksToFinish() noexcept
{
    std::unique_lock< std::mutex > lock( this->mtx );
    this->waitForTasksToFinish.wait(
        lock, [&] { return this->numberOfActiveTasks == 0u; } );
}
} // namespace concurrent
} // namespace el3D
