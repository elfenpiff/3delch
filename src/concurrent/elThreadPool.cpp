#include "concurrent/elThreadPool.hpp"

namespace el3D
{
namespace concurrent
{
elThreadPool::elThreadPool( const uint64_t numberOfThreads ) noexcept
{
    for ( uint64_t k = 0; k < numberOfThreads; ++k )
        this->threads.emplace_back( [&] { this->MainLoop(); } );
}

elThreadPool::~elThreadPool()
{
    this->keepRunning.store( false );
    this->tasks.Terminate();

    for ( auto& t : this->threads )
        if ( t.joinable() ) t.join();
}

void
elThreadPool::MainLoop() noexcept
{
    while ( this->keepRunning.load() == true )
    {
        auto call = this->tasks.BlockingPop();
        if ( call.has_value() ) ( *call )();
    }
}

void
elThreadPool::AddTask( const std::function< void() >& f ) noexcept
{
    this->tasks.Push( f );
}

uint64_t
elThreadPool::GetNumberOfThreads() const noexcept
{
    return this->threads.size();
}

elThreadPool&
elThreadPool::GetGlobalThreadPool( const uint64_t numberOfThreads ) noexcept
{
    static elThreadPool globalThreadPool( numberOfThreads );
    return globalThreadPool;
}

} // namespace concurrent
} // namespace el3D
