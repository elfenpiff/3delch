#include "concurrent/elSemaphore.hpp"

#include "utils/smart_c.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace concurrent
{
elSemaphore::elSemaphore( const unsigned int value )
{
    this->Init( value )
        .OrElse( [&]( auto r ) {
            LOG_ERROR( 0 ) << "unable to create semaphore";
            this->create_isInitialized = false;
            this->create_error         = r;
        } )
        .AndThen( [&] { this->create_isInitialized = true; } );
}

elSemaphore::elSemaphore() : elSemaphore( 0 )
{
}

elSemaphore::~elSemaphore()
{
    this->Destroy().OrElse(
        [&] { LOG_ERROR( 0 ) << "error during semaphore resource cleanup"; } );
}

elSemaphore::elSemaphore( elSemaphore&& rhs )
{
    *this = std::move( rhs );
}

elSemaphore&
elSemaphore::operator=( elSemaphore&& rhs )
{
    if ( this != &rhs )
    {
        this->Destroy().OrElse(
            [&] { LOG_ERROR( 0 ) << "error in semaphore move process"; } );

        this->destroySemaphore = std::move( rhs.destroySemaphore );
        this->handle           = std::move( rhs.handle );

        rhs.destroySemaphore = false;
    }
    return *this;
}

bb::elExpected< bb::errno_t >
elSemaphore::Init( const unsigned int value )
{
    if ( !this->destroySemaphore )
    {
#ifndef _WIN32
        return utils::smart_c(
                   sem_init, []( auto v ) { return v == -1; }, &this->handle, 0,
                   value )
            .OrElse(
                [&] { LOG_ERROR( 0 ) << "unable to initialize semaphore"; } );
#else
        this->handle = CreateSemaphore( nullptr, INITIAL_SEMAPHORE_VALUE,
                                        MAX_SEMAPHORE_VALUE, nullptr );
        if ( this->handle == nullptr )
        {
            LOG_ERROR( 0 )
                << "unable to initialize semaphore (CreateSemaphore), "
                   "error code = "
                << std::to_string( GetLastError() );
            return bb::Error( -1 );
        }
#endif

        this->destroySemaphore = true;
    }

    return bb::Success<>();
}

bb::elExpected< bb::errno_t >
elSemaphore::Destroy()
{
    if ( this->destroySemaphore )
    {
#ifndef _WIN32
        return utils::smart_c(
                   sem_destroy, []( auto v ) { return v == -1; },
                   &this->handle )
            .OrElse( [] { LOG_ERROR( 0 ) << "unable not destroy semaphore"; } );
#else
        if ( !CloseHandle( this->handle ) )
        {
            LOG_ERROR( 0 )
                << "unable not destroy semaphore (CloseHandle), error code = "
                << std::to_string( GetLastError() );
            return bb::Error( -1 );
        }
#endif

        this->destroySemaphore = false;
    }

    return bb::Success<>();
}

bb::elExpected< bb::errno_t >
elSemaphore::Post()
{
#ifndef _WIN32
    return utils::smart_c(
               sem_post, []( auto v ) { return v == -1; }, &this->handle )
        .OrElse( [] { LOG_ERROR( 0 ) << "unable to increment semaphore"; } );
#else
    if ( ReleaseSemaphore( this->handle, 1, nullptr ) == 0 )
    {
        LOG_ERROR( 0 )
            << "unable to increment semaphore (ReleaseSemaphore), error code = "
            << std::to_string( GetLastError() );
        return bb::Error( -1 );
    }
#endif

    return bb::Success<>();
}

bb::elExpected< bb::errno_t >
elSemaphore::Wait()
{
#ifndef _WIN32
    return utils::smart_c(
               sem_wait, []( auto v ) { return v == -1; }, &this->handle )
        .OrElse( [] { LOG_ERROR( 0 ) << "unable to decrement semaphore"; } );
#else
    if ( WaitForSingleObject( this->handle, INFINITE ) == WAIT_FAILED )
    {
        LOG_ERROR( 0 )
            << "unable to decrement semaphore (WaitForSingleObject), "
               "error code = "
            << std::to_string( GetLastError() );
        return bb::Error( -1 );
    }
#endif
    return bb::Success<>();
}

bb::elExpected< bb::errno_t >
elSemaphore::Reset()
{
    while ( true )
    {
        auto waitCall = this->TryWait();
        if ( waitCall.HasError() )
            return bb::Error( waitCall.GetError() );
        else if ( !waitCall.GetValue() )
            return bb::Success<>();
    }
}

bb::elExpected< bool, bb::errno_t >
elSemaphore::TryWait()
{
#ifndef _WIN32
    auto tryCall = utils::smart_c(
        sem_trywait, []( auto v ) { return v == -1; }, &this->handle );
    if ( tryCall.HasError() )
    {
        if ( tryCall.GetError() == EAGAIN ) return bb::Success( false );

        LOG_ERROR( 0 ) << "unable to decrement semaphore";
        return bb::Error( tryCall.GetError() );
    }
#else
    auto returnCode = WaitForSingleObject( this->handle, 0 );
    if ( returnCode == WAIT_TIMEOUT )
    {
        return bb::Success( false );
    }
    else if ( returnCode == WAIT_FAILED )
    {
        LOG_ERROR( 0 )
            << "unable to decrement semaphore (WaitForSingleObject), "
               "error code = "
            << std::to_string( GetLastError() );
        return bb::Error( -1 );
    }
#endif

    return bb::Success( true );
}

#ifndef _WIN32
bb::elExpected< int, bb::errno_t >
elSemaphore::GetValue() const
{
    int  returnValue;
    auto getCall = utils::smart_c(
        sem_getvalue, []( auto v ) { return v == -1; },
        const_cast< sem_t* >( &this->handle ), &returnValue );
    if ( getCall.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to get semaphore value";
        return bb::Error( getCall.GetError() );
    }
    else
    {
        return bb::Success( returnValue );
    }
}
#endif

bb::elExpected< bool, bb::errno_t >
elSemaphore::TimedWait( const units::Time& timeout )
{
#ifndef __APPLE__
#ifndef _WIN32
    struct timespec waitTime = timeout;

    auto timeCall = utils::smart_c(
        sem_timedwait, []( auto v ) { return v == -1 && errno != ETIMEDOUT; },
        &this->handle, &waitTime );
    if ( timeCall.HasError() )
    {
        if ( timeCall.GetValue() == -1 ) return bb::Success( false );
        LOG_ERROR( 0 ) << "unable to decrement semaphore";
        return bb::Error( timeCall.GetError() );
    }
#else
    DWORD timeoutInMilliseconds =
        static_cast< DWORD >( timeout.GetMilliSeconds() );

    auto returnCode =
        WaitForSingleObject( this->handle, timeoutInMilliseconds );

    if ( returnCode == WAIT_TIMEOUT )
    {
        return bb::Success( false );
    }
    else if ( returnCode == WAIT_FAILED )
    {
        LOG_ERROR( 0 )
            << "unable to decrement semaphore (WaitForSingleObject), "
               "error code = "
            << std::to_string( GetLastError() );
        return bb::Error( -1 );
    }
    return bb::Success( true );
#endif
#endif
    return bb::Error( -1 );
}


} // namespace concurrent
} // namespace el3D
