#include "Apps/VoxelEditor/Coordinates.hpp"

#include "Apps/VoxelEditor/config.hpp"

namespace el3D
{
namespace Apps
{
namespace VoxelEditor
{
Coordinates::Coordinates( _3Delch::elScene&            scene,
                          const CoordinatesProperties& p,
                          const CoordinatesCallbacks&  callbacks ) noexcept
    : world::elWorld( scene ), properties{ p }, callbacks{ callbacks },
      gridSize{ static_cast< float >( this->properties.voxelResolution ) /
                ( NUMBER_OF_VOXELS_PER_UNIT * 2.0f ) },
      highlightPosition{ gridSize, 0.0f, gridSize }
{
    world::CoordinateSystemProperties coordinateProperties{
        .gridSize      = glm::vec2( this->gridSize ),
        .position      = { 0.0f, 0.0f, 0.0f },
        .gridStyle     = HighGL::elCoordinateSystemGenerator::GridStyle::Lines,
        .style         = world::CoordinateSystemStyle::MayorMinorPlane,
        .majorDrawSize = 1.0f,
        .minorDrawSize = 1.0f,
        .majorColor    = p.gridColor,
        .minorColor    = p.gridColor,
        .doConnectToCamera = false };

    this->x = this->Create< world::elCoordinateSystem >( coordinateProperties );
    this->x->SetPosition( { this->gridSize, 0.0f, this->gridSize } );

    this->y = this->Create< world::elCoordinateSystem >( coordinateProperties );
    this->y->SetRotation( { 1.0f, 0.0f, 0.0f, -M_PI_2 } );
    this->y->SetPosition(
        { this->gridSize, this->gridSize, 2.0f * this->gridSize } );

    this->z = this->Create< world::elCoordinateSystem >( coordinateProperties );
    this->z->SetRotation( { 0.0f, 0.0f, 1.0f, M_PI_2 } );
    this->z->SetPosition(
        { 2.0f * this->gridSize, this->gridSize, this->gridSize } );

    this->CreateHighlight();
}

glm::vec3
Coordinates::GetCenter() const noexcept
{
    return glm::vec3{ this->gridSize };
}

void
Coordinates::CreateHighlight() noexcept
{
    world::CoordinateSystemProperties drawCoordinates{
        .gridSize      = glm::vec2( this->gridSize ),
        .position      = { 0.0f, HIGHLIGHT_LEVEL_OFFSET, 0.0f },
        .gridStyle     = HighGL::elCoordinateSystemGenerator::GridStyle::Lines,
        .style         = world::CoordinateSystemStyle::MayorMinor,
        .majorDrawSize = 2.0f,
        .minorDrawSize = 2.0f,
        .majorColor    = this->properties.highlightColor,
        .minorColor    = this->properties.highlightColor,
        .doConnectToCamera = false };

    this->highlight =
        this->Create< world::elCoordinateSystem >( drawCoordinates );

    this->highlight->SetPosition( this->highlightPosition );

    this->highlightInteraction = this->Create< world::elInteractiveSet >();
    this->highlightInteraction->SetupInteractiveMesh(
        { .click =
              [this]( auto v )
          {
              if ( !this->callbacks.leftClick ) return;

              glm::uvec3 position{
                  static_cast< int >( v.x * NUMBER_OF_VOXELS_PER_UNIT + 0.5f ),
                  static_cast< int >( v.y * NUMBER_OF_VOXELS_PER_UNIT + 0.5f ),
                  static_cast< int >( v.z * NUMBER_OF_VOXELS_PER_UNIT +
                                      0.5f ) };

              this->callbacks.leftClick( position );
          } },
        this->gridSize, this->properties.voxelResolution );

    this->highlightInteraction->GetInteractiveMesh().SetPosition(
        this->highlightPosition +
        glm::vec3{ 0.0f, HIGHLIGHT_LEVEL_OFFSET, 0.0f } );
}

void
Coordinates::EnableHighlight() noexcept
{
    if ( !this->highlight ) this->CreateHighlight();
}

void
Coordinates::DisableHighlight() noexcept
{
    if ( this->highlight ) this->highlight.Reset();
    this->highlightInteraction.Reset();
}

void
Coordinates::ToggleHighlight() noexcept
{
    if ( this->highlight )
        this->DisableHighlight();
    else
        this->EnableHighlight();
}

void
Coordinates::UpdateHighlightPosition() noexcept
{
    this->highlight->MoveTo(
        this->highlightPosition +
            glm::vec3( 0.0f, HIGHLIGHT_LEVEL_OFFSET, 0.0f ),
        ANIMATION_SPEED, animation::AnimationVelocity::Relative );
    this->highlightInteraction->GetInteractiveMesh().SetPosition(
        this->highlightPosition +
        glm::vec3( 0.0f, HIGHLIGHT_LEVEL_OFFSET, 0.0f ) );
}

void
Coordinates::IncreaseHighlightLevel() noexcept
{
    if ( !this->highlight ||
         this->highlightPosition.y >= this->gridSize * 2.0f - LEVEL_VALUE )
        return;

    this->highlightPosition += glm::vec3( 0.0f, LEVEL_VALUE, 0.0f );
    this->UpdateHighlightPosition();
}

void
Coordinates::DecreaseHighlightLevel() noexcept
{
    if ( !this->highlight || this->highlightPosition.y < LEVEL_VALUE ) return;

    this->highlightPosition -= glm::vec3( 0.0f, LEVEL_VALUE, 0.0f );
    this->UpdateHighlightPosition();
}
} // namespace VoxelEditor
} // namespace Apps
} // namespace el3D
