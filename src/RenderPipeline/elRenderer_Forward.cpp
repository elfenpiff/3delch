#include "RenderPipeline/elRenderer_Forward.hpp"

#include "RenderPipeline/elRenderPipelineManager.hpp"


namespace el3D
{
namespace RenderPipeline
{
elRenderer_Forward::elRenderer_Forward(
    const internal::rendererShared_t shared,
    OpenGL::elTexture_2D* const      deferredOutput,
    OpenGL::elTexture_2D* const      deferredNormal,
    OpenGL::elTexture_2D* const      deferredPosition ) noexcept
    : elRenderer_base( elRenderer_Type::forward, 1, shared )
{
    this->settings.vertexObject =
        this->SetupSettingsEntry( ObjectType::VertexObject );
    this->settings.nonVertexObject =
        this->SetupSettingsEntry( ObjectType::NonVertexObject );
    this->settings.unifiedVertexObject =
        this->SetupSettingsEntry( ObjectType::UnifiedVertexObject );

    this->AttachTexture( this->texture.final, deferredOutput,
                         Attachment::Color );
    this->AttachTexture( this->texture.glow, shared.glowTexture,
                         Attachment::Color );
    this->AttachTexture( this->texture.normal, deferredNormal,
                         Attachment::Color );
    this->AttachTexture( this->texture.position, deferredPosition,
                         Attachment::Color );
    this->AttachTexture( this->texture.material, shared.materialTexture,
                         Attachment::Color );
    this->AttachTexture( this->texture.event, shared.eventTexture,
                         Attachment::Color );
    this->AttachTexture( this->texture.depth, shared.depthTexture,
                         Attachment::DepthStencil );
}

elRenderer_Forward::settingsEntry_t
elRenderer_Forward::SetupSettingsEntry( const ObjectType objectType ) noexcept
{
    settingsEntry_t newEntry;
    std::string     objectName =
        OBJECT_TYPE_NAME[static_cast< uint64_t >( objectType )];

    if ( objectType == ObjectType::UnifiedVertexObject )
    {
        OpenGL::elGLSLParser::staticDefines_t defines;
        defines[OpenGL::elGLSLParser::groupType_t{ "geometry",
                                                   GL_FRAGMENT_SHADER }]
            .emplace_back( OpenGL::elGLSLParser::define_t{
                "INVALID_TEXTURE_ID",
                std::to_string( HighGL::elGeometricObject_UnifiedVertexObject::
                                    INVALID_TEXTURE_ID ) } );
        newEntry.shader = this->CreateShaderProgram( "forward", objectName,
                                                     "geometry", defines );
    }
    else
    {
        newEntry.shader =
            this->CreateShaderProgram( "forward", objectName, "geometry" );
    }

    if ( !this->settings.directionalLightBuffer )
    {
        this->settings.directionalLightBuffer =
            this->CreateDirectionalLightBuffer( *newEntry.shader );
        newEntry.directionalLightBufferIndex = 0u;
    }
    else if ( newEntry.shader->HasUniformBlock( DIRECTIONAL_LIGHT_BLOCK_NAME ) )
        this->settings.directionalLightBuffer
            ->AddShader( newEntry.shader.get() )
            .AndThen( [&]( auto r )
                      { newEntry.directionalLightBufferIndex = r; } );

    if ( !this->settings.sceneBuffer )
    {
        this->settings.sceneBuffer =
            this->CreateSceneBuffer( *newEntry.shader );
        newEntry.sceneBufferIndex = 0u;
    }
    else if ( newEntry.shader->HasUniformBlock( SCENE_BLOCK_NAME ) )
        this->settings.sceneBuffer->AddShader( newEntry.shader.get() )
            .AndThen( [&]( auto r ) { newEntry.sceneBufferIndex = r; } );

    return newEntry;
}

std::unique_ptr< OpenGL::elUniformBufferObject >
elRenderer_Forward::CreateSceneBuffer(
    const OpenGL::elShaderProgram& shader ) const noexcept
{
    auto buffer = OpenGL::elUniformBufferObject::Create(
        SCENE_BLOCK_NAME,
        std::vector< std::string >{ std::string( SCENE_BLOCK_NAME ) +
                                    ".cameraPosition" },
        &shader );

    if ( buffer.HasError() )
    {
        LOG_FATAL( 0 ) << SCENE_BLOCK_NAME
                       << " uniform block must be implemented in "
                          "forward renderer shader!";
        std::terminate();
    }

    return std::make_unique< OpenGL::elUniformBufferObject >(
        std::move( **buffer ) );
}

std::unique_ptr< OpenGL::elUniformBufferObject >
elRenderer_Forward::CreateDirectionalLightBuffer(
    const OpenGL::elShaderProgram& shader ) const noexcept
{
    auto buffer = OpenGL::elUniformBufferObject::Create(
        DIRECTIONAL_LIGHT_BLOCK_NAME,
        std::vector< std::string >{
            std::string( DIRECTIONAL_LIGHT_BLOCK_NAME ) + ".color",
            std::string( DIRECTIONAL_LIGHT_BLOCK_NAME ) + ".direction",
            std::string( DIRECTIONAL_LIGHT_BLOCK_NAME ) + ".ambientIntensity",
            std::string( DIRECTIONAL_LIGHT_BLOCK_NAME ) + ".diffuseIntensity",
            std::string( DIRECTIONAL_LIGHT_BLOCK_NAME ) + ".numberOfLights" },
        &shader );

    if ( buffer.HasError() )
    {
        LOG_FATAL( 0 ) << DIRECTIONAL_LIGHT_BLOCK_NAME
                       << " uniform block must be implemented in "
                          "forward renderer shader!";
        std::terminate();
    }

    return std::make_unique< OpenGL::elUniformBufferObject >(
        std::move( **buffer ) );
}

void
elRenderer_Forward::RenderGeometricObjects(
    const OpenGL::elCamera& camera ) const noexcept
{
    this->fbo->Bind(
        this->GenerateRenderTargets(
            { this->texture.final.target, this->texture.glow.target,
              this->texture.normal.target, this->texture.position.target,
              this->texture.material.target, this->texture.event.target } ),
        0 );

    OpenGL::Enable( GL_BLEND );
    OpenGL::Enable( GL_DEPTH_TEST );
    OpenGL::Enable( GL_CULL_FACE );
    OpenGL::gl( glBlendFunc, static_cast< GLenum >( GL_SRC_ALPHA ),
                static_cast< GLenum >( GL_ONE_MINUS_SRC_ALPHA ) );
    OpenGL::gl( glCullFace, static_cast< GLenum >( GL_FRONT ) );
    OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_TRUE ) );

    for ( uint64_t i = 0; i < static_cast< uint64_t >( RenderOrder::END ); ++i )
    {
        for ( auto& vo :
              this->factory[i].GetProducts< OpenGL::elGeometricObject_base >() )
        {
            if ( vo.Extension().directionalLightBufferIndex )
                this->settings.directionalLightBuffer->BindToShader(
                    *vo.Extension().directionalLightBufferIndex );

            if ( vo.Extension().sceneBufferIndex )
                this->settings.sceneBuffer->BindToShader(
                    *vo.Extension().sceneBufferIndex );

            vo->Draw( vo.Extension().geometricShader, camera );
        }
    }

    OpenGL::Disable( GL_CULL_FACE );
    OpenGL::Disable( GL_DEPTH_TEST );
    OpenGL::Disable( GL_BLEND );

    this->fbo->Unbind();
}

void
elRenderer_Forward::UpdateDirectionalLightBuffer() noexcept
{
    for ( uint64_t k                     = 0,
                   directionalLightsSize = this->directionalLights->size();
          k < directionalLightsSize; ++k )
    {
        auto& light = ( *this->directionalLights )[k];
        if ( !light->HasUpdatedSettings() ) continue;

        this->settings.directionalLightBuffer->SetVariableData(
            0, static_cast< uint64_t >( settings_t::light::COLOR ),
            glm::value_ptr( light->GetColor() ), 3 * sizeof( GLfloat ) );

        this->settings.directionalLightBuffer->SetVariableData(
            0, static_cast< uint64_t >( settings_t::light::DIRECTION ),
            glm::value_ptr( light->GetDirection() ), 3 * sizeof( GLfloat ) );

        GLfloat ambientIntensity = light->GetAmbientIntensity();
        this->settings.directionalLightBuffer->SetVariableData(
            0, static_cast< uint64_t >( settings_t::light::AMBIENT_INTENSITY ),
            &ambientIntensity, sizeof( GLfloat ) );

        GLfloat diffuseIntensity = light->GetDiffuseIntensity();
        this->settings.directionalLightBuffer->SetVariableData(
            0, static_cast< uint64_t >( settings_t::light::DIFFUSE_INTENSITY ),
            &diffuseIntensity, sizeof( GLfloat ) );

        GLint numberOfLights =
            static_cast< GLint >( this->directionalLights->size() );
        this->settings.directionalLightBuffer->SetVariableData(
            0, static_cast< uint64_t >( settings_t::light::NUMBER_OF_LIGHTS ),
            &numberOfLights, sizeof( GLint ) );
    }
}

void
elRenderer_Forward::UpdateSceneBuffer(
    const OpenGL::elCamera_Perspective& camera ) noexcept
{
    this->settings.sceneBuffer->SetVariableData(
        0, static_cast< uint64_t >( settings_t::scene::CAMERA_POSITION ),
        glm::value_ptr( camera.GetState().position ), 3 * sizeof( GLfloat ) );
}

void
elRenderer_Forward::Render(
    const OpenGL::elCamera_Perspective& camera ) noexcept
{
    this->UpdateDirectionalLightBuffer();
    this->UpdateSceneBuffer( camera );
    this->RenderGeometricObjects( camera );
}

void
elRenderer_Forward::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->fbo->SetSize( x, y, 0 );
}

Forward::objectSettings_t
elRenderer_Forward::GenerateObjectSettings(
    OpenGL::elGeometricObject_base& object,
    const uint64_t                  geometricShaderId ) const noexcept
{
    Forward::objectSettings_t defaultSettings;
    if ( geometricShaderId != AUTO_SET_GEOMETRIC_SHADER )
    {
        defaultSettings.geometricShader = geometricShaderId;

        auto shader = object.GetShader( defaultSettings.geometricShader );
        if ( shader->HasUniformBlock( DIRECTIONAL_LIGHT_BLOCK_NAME ) )
        {
            this->settings.directionalLightBuffer->AddShader( shader ).AndThen(
                [&]( auto r )
                { defaultSettings.directionalLightBufferIndex = r; } );
        }

        if ( shader->HasUniformBlock( SCENE_BLOCK_NAME ) )
        {
            this->settings.sceneBuffer->AddShader( shader ).AndThen(
                [&]( auto r ) { defaultSettings.sceneBufferIndex = r; } );
        }

        return defaultSettings;
    }

    OpenGL::elShaderProgram* shader = nullptr;
    switch ( object.GetType() )
    {
        case OpenGL::elGeometricObject_Type::UnifiedVertexObject:
            shader = this->settings.unifiedVertexObject.shader.get();
            defaultSettings.directionalLightBufferIndex =
                this->settings.unifiedVertexObject.directionalLightBufferIndex;
            defaultSettings.sceneBufferIndex =
                this->settings.unifiedVertexObject.sceneBufferIndex;
            break;
        case OpenGL::elGeometricObject_Type::NonVertexObject:
            shader = this->settings.nonVertexObject.shader.get();
            defaultSettings.directionalLightBufferIndex =
                this->settings.nonVertexObject.directionalLightBufferIndex;
            defaultSettings.sceneBufferIndex =
                this->settings.nonVertexObject.sceneBufferIndex;
            break;
        case OpenGL::elGeometricObject_Type::VertexObject:
            shader = this->settings.vertexObject.shader.get();
            defaultSettings.directionalLightBufferIndex =
                this->settings.vertexObject.directionalLightBufferIndex;
            defaultSettings.sceneBufferIndex =
                this->settings.vertexObject.sceneBufferIndex;
            break;
        default:
            LOG_FATAL( 0 ) << "geometry shader does not fit: incompatible "
                              "object added to forward renderer";
            std::terminate();
            break;
    }

    defaultSettings.geometricShader =
        object.AddShader( shader )
            .OrElse(
                []
                {
                    LOG_FATAL( 0 )
                        << "geometry shader does not fit: incompatible "
                           "object added to forward renderer";
                    std::terminate();
                } )
            .GetValue();

    return defaultSettings;
}
} // namespace RenderPipeline
} // namespace el3D
