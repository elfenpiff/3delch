#include "RenderPipeline/elRenderer_Final.hpp"

#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "RenderPipeline/internal.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderer_Final::elRenderer_Final( const internal::rendererShared_t& shared,
                                    const finalTextures_t&            textures )
    : elRenderer_base( elRenderer_Type::final, 1, shared ),
      onScreenQuad( OpenGL::DrawMode::Triangles,
                    HighGL::elObjectGeometryBuilder::CreateScreenQuad()
                        .GetInnerObjectGeometry() )
{
    this->shader = this->CreateShaderProgram( "final", "", "default" );
    this->CreateTexture( this->texture.composedImage, "final:composedImage",
                         GL_RGBA8, GL_RGBA, GL_FLOAT, Attachment::Color );
    this->CreateTexture( this->texture.event, "final:event", GL_RGBA8, GL_RGBA,
                         GL_FLOAT, Attachment::Color )
        .SetMinMagFilter( GL_NEAREST, GL_NEAREST );

    auto buffer = OpenGL::elUniformBufferObject::Create(
        "sceneSettings_t",
        std::vector< std::string >{ "sceneSettings_t.resolution" },
        this->shader.get() );

    if ( buffer.HasError() )
    {
        LOG_FATAL( 0 ) << "unable to find required uniform buffer "
                          "\"sceneSettings_t\" in final renderer shader "
                       << this->shader->GetFileAndGroupName();
        throw std::runtime_error(
            "uniform buffer sceneSettings_t not found in finalRenderer" );
    }

    this->sceneSettings = std::move( buffer.GetValue() );

    this->onScreenQuad.AddShader( this->shader.get() );
    this->attachedTextures.finalWithPostProcessing =
        this->onScreenQuad.AttachTexture( textures.finalWithPostProcessing, 0,
                                          "diffuse", GL_TEXTURE0 );
    this->attachedTextures.gui =
        this->onScreenQuad.AttachTexture( textures.gui, 0, "gui", GL_TEXTURE1 );
    this->attachedTextures.guiEvent = this->onScreenQuad.AttachTexture(
        shared.eventTexture, 0, "guiEvent", GL_TEXTURE2 );
    this->attachedTextures.guiGlow = this->onScreenQuad.AttachTexture(
        textures.guiGlow, 0, "guiGlow", GL_TEXTURE3 );
    this->attachedTextures.glow = this->onScreenQuad.AttachTexture(
        textures.glow, 0, "glow", GL_TEXTURE4 );
    this->attachedTextures.screenSpaceReflection =
        this->onScreenQuad.AttachTexture( textures.screenSpaceReflection, 0,
                                          "screenSpaceReflection",
                                          GL_TEXTURE5 );
}

void
elRenderer_Final::Render( const OpenGL::elCamera_Perspective& ) noexcept
{
    OpenGL::Disable( GL_DEPTH_TEST );
    OpenGL::Enable( GL_CULL_FACE );

    this->fbo->Bind(
        this->GenerateRenderTargets( { this->texture.composedImage.target,
                                       this->texture.event.target } ),
        0 );

    OpenGL::gl( glClearColor, 0.0f, 0.0f, 0.0f, 0.0f );
    OpenGL::gl( glClear, static_cast< GLbitfield >( GL_COLOR_BUFFER_BIT |
                                                    GL_DEPTH_BUFFER_BIT ) );

    this->sceneSettings->BindToShader();
    static const OpenGL::elCamera_Perspective dummyCamera;
    this->onScreenQuad.Draw( 0, dummyCamera );
    this->fbo->Unbind();
    this->sceneSettings->UnbindFromShader();
}

void
elRenderer_Final::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->fbo->SetSize( x, y, 0 );
    this->sceneSettings->SetVariableData(
        0, 0, glm::value_ptr( glm::ivec2( x, y ) ), 2 * sizeof( GLint ) );
}

void
elRenderer_Final::DrawToScreen( const GLint x, const GLint y ) const noexcept
{
    OpenGL::Disable( GL_DEPTH_TEST );
    this->fbo->Read( this->GenerateRenderTargets(
                         { this->texture.composedImage.target } )[0],
                     0 );
    auto frameSize = this->fbo->GetSize( 0 );
    this->fbo->BlitFrameBuffer( 0, 0, frameSize.x, frameSize.y, 0, 0, x, y,
                                GL_COLOR_BUFFER_BIT, GL_NEAREST );
    this->fbo->ReadStop();
}

glm::vec4
elRenderer_Final::GetEventColorAtPosition(
    const glm::ivec2 position ) const noexcept
{
    auto result = this->fbo->ReadPixels< GLfloat >(
        position.x, position.y, 1, 1, 4, this->texture.event.target, 0 );
    return glm::vec4( result[0], result[1], result[2], result[3] );
}


} // namespace RenderPipeline
} // namespace el3D
