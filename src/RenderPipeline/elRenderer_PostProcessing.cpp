#include "RenderPipeline/elRenderer_PostProcessing.hpp"

#include "HighGL/elShaderTexture.hpp"
#include "OpenGL/elGLSLParser.hpp"
#include "OpenGL/elTexture_2D.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "RenderPipeline/elPostEffect_GTAO.hpp"
#include "RenderPipeline/elPostEffect_Glow.hpp"
#include "RenderPipeline/elPostEffect_ScreenSpaceReflections.hpp"
#include "RenderPipeline/internal.hpp"
#include "utils/elFile.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderer_PostProcessing::elRenderer_PostProcessing(
    const internal::rendererShared_t& shared,
    const OpenGL::elTexture_2D&       guiGlow,
    const OpenGL::elTexture_2D&       gBufferPosition,
    const OpenGL::elTexture_2D&       gBufferNormal,
    const OpenGL::elTexture_2D&       finalTexture ) noexcept
    : elRenderer_base( elRenderer_Type::postProcessing, 0, shared )
{
    this->postEffects[GLOW] = std::make_unique< elPostEffect_Glow >(
        shared.sceneName, "light", shared.config, shared.glowTexture );
    this->postEffects[GUI_GLOW] = std::make_unique< elPostEffect_Glow >(
        shared.sceneName, "gui", shared.config, &guiGlow );
    this->postEffects[SCREEN_SPACE_REFLECTIONS] =
        std::make_unique< elPostEffect_ScreenSpaceReflections >(
            shared.sceneName, *shared.config, gBufferPosition, gBufferNormal,
            *shared.materialTexture, finalTexture,
            shared.optionalSkyboxTexture );
}

void
elRenderer_PostProcessing::Render(
    const OpenGL::elCamera_Perspective& camera ) noexcept
{
    for ( auto& effect : this->postEffects )
        effect->Render( camera );
}

void
elRenderer_PostProcessing::Reshape( const uint64_t x,
                                    const uint64_t y ) noexcept
{
    for ( auto& effect : this->postEffects )
        effect->Reshape( x, y );
}

const OpenGL::elTexture_2D&
elRenderer_PostProcessing::GetOutput( const Output output ) const noexcept
{
    return *this->postEffects[static_cast< uint64_t >( output )]->GetOutput();
}

void
elRenderer_PostProcessing::UseSkyboxForReflection( const bool v ) noexcept
{
    dynamic_cast< elPostEffect_ScreenSpaceReflections* >(
        this->postEffects[SCREEN_SPACE_REFLECTIONS].get() )
        ->UseSkyboxForReflection( v );
}


} // namespace RenderPipeline
} // namespace el3D
