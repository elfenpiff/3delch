#include "RenderPipeline/elRenderer_Cleanup.hpp"

#include "OpenGL/elFrameBufferObject.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderer_Cleanup::elRenderer_Cleanup(
    internal::rendererShared_t shared ) noexcept
    : elRenderer_base( elRenderer_Type::cleanup, 1, shared )
{
    this->AttachTexture( this->texture.glow, shared.glowTexture,
                         Attachment::Color );
    this->AttachTexture( this->texture.glow, shared.materialTexture,
                         Attachment::Color );
}

void
elRenderer_Cleanup::Render( const OpenGL::elCamera_Perspective& ) noexcept
{
    this->fbo->Bind(
        this->GenerateRenderTargets(
            { this->texture.glow.target, this->texture.material.target } ),
        0 );

    OpenGL::gl( glClearColor, 0.0f, 0.0f, 0.0f, 0.0f );
    OpenGL::gl( glClear, static_cast< GLuint >( GL_COLOR_BUFFER_BIT ) );

    this->fbo->Unbind();
}

void
elRenderer_Cleanup::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->fbo->SetSize( x, y, 0 );
}

} // namespace RenderPipeline
} // namespace el3D
