#include "RenderPipeline/elPostEffect_Glow.hpp"

#include "GuiGL/common.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"
#include "logging/elLog.hpp"
#include "lua/elConfigHandler.hpp"

namespace el3D
{
namespace RenderPipeline
{
elPostEffect_Glow::elPostEffect_Glow(
    const std::string& sceneName, const std::string& blurType,
    const lua::elConfigHandler* const config,
    const OpenGL::elTexture_2D* const rawBlur ) noexcept
    : rawBlur{ rawBlur }
{
    this->scaling =
        config->Get< float >( { "global", "scenes", sceneName, "postEffects",
                                "glow", blurType, "scaling" },
                              { this->scaling } )[0] /
        GuiGL::GetDPIScaling();

    this->intensity =
        std::max( 0.0f, config->Get< float >( { "global", "scenes", sceneName,
                                                "postEffects", "glow", blurType,
                                                "intensity" },
                                              { this->intensity } )[0] );

    this->stepWidthFactor =
        std::max( 0.0f, config->Get< float >( { "global", "scenes", sceneName,
                                                "postEffects", "glow", blurType,
                                                "stepWidthFactor" },
                                              { this->stepWidthFactor } )[0] );


    this->CreateBlurShader( sceneName, blurType, config );
}

void
elPostEffect_Glow::CreateBlurShader(
    const std::string& sceneName, const std::string& blurType,
    const lua::elConfigHandler* const config ) noexcept
{
    auto shaderSource = GetShaderFileAndGroup(
        config, sceneName, blurType, "shader",
        "glsl/PostProcessing/gaussianBlur.glsl", "default" );

    OpenGL::textureProperties_t textureProperties{ "", GL_RGBA8, GL_RGBA,
                                                   GL_FLOAT };
    textureProperties.wrapS = GL_CLAMP_TO_EDGE;
    textureProperties.wrapT = GL_CLAMP_TO_EDGE;

    auto shaderCode =
        OpenGL::elGLSLParser::Create( shaderSource.file, 440 )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to open gaussian blur shader file "
                        << shaderSource.file;
                    std::terminate();
                } )
            .GetValue();

    this->blurShader.emplace( std::move(
        *HighGL::elShaderTexture::Create(
             std::vector< OpenGL::textureProperties_t >{ textureProperties },
             shaderCode->GetContent()
                 .at( shaderSource.group )
                 .at( GL_FRAGMENT_SHADER )
                 .code,
             512, 512, false, true )
             .OrElse(
                 [&]
                 {
                     LOG_FATAL( 0 )
                         << "unable to create vertical gaussian blur shader "
                            "from "
                            "source "
                         << shaderSource.file << ":" << shaderSource.group;
                     std::terminate();
                 } )
             .GetValue() ) );

    this->blurShader->AddInputTexture( "diffuse", this->rawBlur );

    this->blurShader->RegisterUniform(
        "invertedResolution",
        [this]( auto shader, auto uniformID )
        {
            auto viewPortSize = this->camera->GetViewPortSize();
            shader->SetUniform( uniformID,
                                1.0f / ( viewPortSize * this->scaling ) );
        } );

    this->blurShader->RegisterUniform(
        "iterations",
        []( auto shader, auto uniformID )
        {
            shader->SetUniform( uniformID,
                                static_cast< GLint >( GAUSS_5BI_ITERATIONS ) );
        } );

    this->blurShader->RegisterUniform(
        "intensity", [this]( auto shader, auto uniformID )
        { shader->SetUniform( uniformID, this->intensity ); } );

    this->blurShader->RegisterUniform(
        "stepWidthFactor", [this]( auto shader, auto uniformID )
        { shader->SetUniform( uniformID, this->stepWidthFactor ); } );

    this->blurShader->RegisterUniform(
        "offset", []( auto shader, auto uniformID )
        { shader->SetUniform( uniformID, GAUSS_5BI_OFFSET ); } );

    this->blurShader->RegisterUniform(
        "weight", []( auto shader, auto uniformID )
        { shader->SetUniform( uniformID, GAUSS_5BI_WEIGHT ); } );
}

elPostEffect_Glow::shader_t
elPostEffect_Glow::GetShaderFileAndGroup(
    const lua::elConfigHandler* const config, const std::string& sceneName,
    const std::string& blurType, const std::string& shaderEntry,
    const std::string& defaultShader,
    const std::string& defaultGroup ) const noexcept
{
    std::string file = config->Get< std::string >(
        { "global", "scenes", sceneName, "postEffects", "glow", blurType,
          shaderEntry, "file" },
        { defaultShader } )[0];
    std::string group = config->Get< std::string >(
        { "global", "scenes", sceneName, "postEffects", "glow", blurType,
          shaderEntry, "group" },
        { defaultGroup } )[0];
    return { file, group };
}

void
elPostEffect_Glow::Render( const OpenGL::elCamera_Perspective& c ) noexcept
{
    this->rawBlur->GenerateMipMap();
    this->camera = &c;
    this->blurShader->Refresh();
}

void
elPostEffect_Glow::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->blurShader->SetSize(
        static_cast< uint64_t >( this->scaling * static_cast< float >( x ) ),
        static_cast< uint64_t >( this->scaling * static_cast< float >( y ) ) );
}

OpenGL::elTexture_2D*
elPostEffect_Glow::GetOutput() noexcept
{
    return &this->blurShader->GetOutputTexture( 0 );
}

void
elPostEffect_Glow::SetIntensity( const float value ) noexcept
{
    this->intensity = value;
}


} // namespace RenderPipeline
} // namespace el3D
