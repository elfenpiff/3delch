#include "RenderPipeline/elPostEffect_ScreenSpaceReflections.hpp"

#include "OpenGL/elTexture_CubeMap.hpp"

namespace el3D
{
namespace RenderPipeline
{
elPostEffect_ScreenSpaceReflections::elPostEffect_ScreenSpaceReflections(
    const std::string& sceneName, const lua::elConfigHandler& config,
    const OpenGL::elTexture_2D&       positionTexture,
    const OpenGL::elTexture_2D&       normalTexture,
    const OpenGL::elTexture_2D&       materialTexture,
    const OpenGL::elTexture_2D&       finalTexture,
    const OpenGL::elTexture_CubeMap** optionalSkyboxTexture ) noexcept
    : optionalSkyboxTexture{ optionalSkyboxTexture }, skyboxPlaceholder{
                                                          "SSRplaceHolder" }
{
    std::string shaderFile = config.Get< std::string >(
        { "global", "scenes", sceneName, "postEffects", "screenSpaceReflection",
          "screenSpaceReflectionShader", "file" },
        { SSR_SHADER_FILE } )[0];

    std::string shaderGroup = config.Get< std::string >(
        { "global", "scenes", sceneName, "postEffects", "screenSpaceReflection",
          "screenSpaceReflectionShader", "group" },
        { SSR_SHADER_GROUP } )[0];

    this->scaling = std::max(
        0.0f,
        config.Get< float >( { "global", "scenes", sceneName, "postEffects",
                               "screenSpaceReflection", "scaling" },
                             { this->scaling } )[0] );

    auto shaderSource =
        OpenGL::elGLSLParser::Create( shaderFile, 440 )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to parse screenSpaceReflectionShader";
                    std::terminate();
                } )
            .GetValue();

    auto shaderTexture =
        HighGL::elShaderTexture::Create(
            std::vector< OpenGL::textureProperties_t >{
                { "ssrUV", GL_RGBA8, GL_RGBA, GL_FLOAT } },
            shaderSource->GetContent()
                .at( shaderGroup )
                .at( GL_FRAGMENT_SHADER )
                .code,
            512, 512, false, true )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to create screenSpaceReflection UV shader";
                    std::terminate();
                } )
            .GetValue();

    shaderTexture->RegisterUniform(
        "projection",
        [this]( auto shader, auto uniformID ) {
            shader->SetUniform( uniformID,
                                this->camera->GetMatrices().projection );
        } );
    shaderTexture->RegisterUniform(
        "invView",
        [this]( auto shader, auto uniformID )
        {
            shader->SetUniform(
                uniformID, glm::inverse( this->camera->GetMatrices().view ) );
        } );
    shaderTexture->RegisterUniform(
        "view",
        [this]( auto shader, auto uniformID ) {
            shader->SetUniform( uniformID, this->camera->GetMatrices().view );
        } );
    shaderTexture->RegisterUniform(
        "hasSkyboxAttached",
        [this]( auto shader, auto uniformID )
        {
            shader->SetUniform( uniformID, static_cast< GLint >(
                                               this->isOptionalSkyboxAttached &&
                                               this->useSkyboxForReflection ) );
        } );
    shaderTexture->RegisterUniform(
        "cameraPosition",
        [this]( auto shader, auto uniformID ) {
            shader->SetUniform( uniformID, this->camera->GetState().position );
        } );
    shaderTexture->RegisterUniform(
        "resolution", [this]( auto shader, auto uniformID )
        { shader->SetUniform( uniformID, this->camera->GetViewPortSize() ); } );


    shaderTexture->AddInputTexture( "position", &positionTexture );
    shaderTexture->AddInputTexture( "normal", &normalTexture );
    shaderTexture->AddInputTexture( "materialTexture", &materialTexture );
    shaderTexture->AddInputTexture( "finalImage", &finalTexture );
    shaderTexture->AddInputTexture( "skybox", &this->skyboxPlaceholder );


    this->ssrUVShader = std::move( shaderTexture );
}

void
elPostEffect_ScreenSpaceReflections::UseSkyboxForReflection(
    const bool v ) noexcept
{
    this->useSkyboxForReflection = v;
}

void
elPostEffect_ScreenSpaceReflections::Render(
    const OpenGL::elCamera_Perspective& c ) noexcept
{
    if ( this->isOptionalSkyboxAttached &&
         *this->optionalSkyboxTexture == nullptr )
    {
        this->ssrUVShader->RemoveInputTexture( this->skyboxTexture );
        this->ssrUVShader->AddInputTexture( "skybox",
                                            &this->skyboxPlaceholder );
        this->isOptionalSkyboxAttached = false;
        this->skyboxTexture            = nullptr;
    }
    else if ( !this->isOptionalSkyboxAttached &&
              *this->optionalSkyboxTexture != nullptr )
    {
        this->isOptionalSkyboxAttached = true;
        this->skyboxTexture            = *this->optionalSkyboxTexture;
        this->ssrUVShader->RemoveInputTexture( &this->skyboxPlaceholder );
        this->ssrUVShader->AddInputTexture( "skybox", this->skyboxTexture );
    }

    this->camera = &c;
    this->ssrUVShader->Refresh();
}

void
elPostEffect_ScreenSpaceReflections::Reshape( const uint64_t x,
                                              const uint64_t y ) noexcept
{
    this->ssrUVShader->SetSize(
        static_cast< uint64_t >( this->scaling * static_cast< float >( x ) ),
        static_cast< uint64_t >( this->scaling * static_cast< float >( y ) ) );
}

OpenGL::elTexture_2D*
elPostEffect_ScreenSpaceReflections::GetOutput() noexcept
{
    return &this->ssrUVShader->GetOutputTexture( 0 );
}


} // namespace RenderPipeline
} // namespace el3D
