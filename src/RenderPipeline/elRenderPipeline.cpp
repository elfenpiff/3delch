#include "RenderPipeline/elRenderPipeline.hpp"

#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "RenderPipeline/elRenderer_PostProcessing.hpp"
#include "RenderPipeline/elRenderer_base.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "lua/elConfigHandler.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderPipeline::elRenderPipeline( const std::string                &sceneName,
                                    const lua::elConfigHandler *const config,
                                    HighGL::elEventTexture *const eventTexture,
                                    const GLAPI::elWindow *const  window,
                                    GLAPI::elFontCache *const     fontCache,
                                    GLAPI::elEventHandler *const  eventHandler )
    : sceneName( sceneName ), config( config ), eventTexture( eventTexture )
{
    this->maxDirectionalLights = static_cast< uint64_t >(
        this->config->Get< int >( { "global", "scenes", this->sceneName,
                                    "light", "maxDirectionalLights" },
                                  { 1 } )[0] );

    auto zNear = this->config->Get< float >(
        { "global", "scenes", this->sceneName, "camera", "zNearInMeter" },
        { 1 } )[0];

    auto zFar = this->config->Get< float >(
        { "global", "scenes", this->sceneName, "camera", "zFarInMeter" },
        { 1 } )[0];

    this->camera.SetZFactor( units::Length::Meter( zNear ),
                             units::Length::Meter( zFar ) );

    this->outputTextures.emplace_back( std::make_unique< OpenGL::elTexture_2D >(
        "pipeline:final", 1, 1, GL_RGBA8, GL_RGBA, GL_FLOAT ) );
    this->outputTextures.emplace_back( std::make_unique< OpenGL::elTexture_2D >(
        "pipeline:glow", 1, 1, GL_RGBA8, GL_RGBA, GL_FLOAT ) );
    this->outputTextures.emplace_back( std::make_unique< OpenGL::elTexture_2D >(
        "pipeline:material", 1, 1, GL_RGBA8, GL_RGBA, GL_FLOAT ) );
    this->outputTextures.emplace_back( std::make_unique< OpenGL::elTexture_2D >(
        "pipeline:event", 1, 1, GL_RGBA8, GL_RGBA, GL_FLOAT ) );
    this->outputTextures.emplace_back( std::make_unique< OpenGL::elTexture_2D >(
        "pipeline:depth+stencil", 1, 1, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL,
        GL_UNSIGNED_INT_24_8 ) );

    constexpr GLint MAX_GLOW_MIPMAP_LEVEL = 7;
    this->outputTextures[TEX_GLOW]->SetMinMagFilter( GL_LINEAR_MIPMAP_LINEAR,
                                                     GL_LINEAR );
    this->outputTextures[TEX_GLOW]->TexParameteri( GL_TEXTURE_MAX_LEVEL,
                                                   MAX_GLOW_MIPMAP_LEVEL );
    this->outputTextures[TEX_GLOW]->SetWrap(
        GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER );
    float borderColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    this->outputTextures[TEX_GLOW]->TexParameterfv( GL_TEXTURE_BORDER_COLOR,
                                                    borderColor );

    this->outputTextures[TEX_EVENT]->SetMinMagFilter( GL_NEAREST, GL_NEAREST );

    this->renderer.cleanupTextures = std::unique_ptr< elRenderer_Cleanup >(
        this->AddRenderer< elRenderer_Cleanup >( window ) );

    this->renderer.shaderTexture = std::unique_ptr< elRenderer_ShaderTexture >(
        this->AddRenderer< elRenderer_ShaderTexture >( window ) );

    this->renderer.gui =
        std::unique_ptr< elRenderer_GUI >( this->AddRenderer< elRenderer_GUI >(
            window, fontCache,
            [&]( HighGL::elShaderTexture *v )
            { return this->RegisterShaderTexture( v ); },
            [&]( size_t v ) { this->DeregisterShaderTexture( v ); },
            eventHandler ) );

    this->renderer.deferred = std::unique_ptr< elRenderer_Deferred >(
        this->AddRenderer< elRenderer_Deferred >( window ) );

    // forward renderer writes into the deferred output
    // reason:
    //    - ambient occlusion needs to be overridden otherwise shadows on
    //      forward object
    //    - forward renderer draws transparent objects
    this->renderer.forward = std::unique_ptr< elRenderer_Forward >(
        this->AddRenderer< elRenderer_Forward >(
            window, this->renderer.deferred->GetOutput(),
            this->renderer.deferred->GetNormalTexture(),
            this->renderer.deferred->GetPositionTexture() ) );

    this->renderer.postProcessing =
        std::unique_ptr< elRenderer_PostProcessing >(
            this->AddRenderer< elRenderer_PostProcessing >(
                window, *this->renderer.gui->GetGuiGlowTexture(),
                *this->renderer.deferred->GetPositionTexture(),
                *this->renderer.deferred->GetNormalTexture(),
                *this->renderer.deferred->GetOutput() ) );

    this->renderer.final = std::unique_ptr< elRenderer_Final >(
        this->AddRenderer< elRenderer_Final >(
            window,
            elRenderer_Final::finalTextures_t{
                // contains forward output as well
                this->renderer.deferred->GetOutput(),
                this->renderer.gui->GetGuiTexture(),
                &this->renderer.postProcessing->GetOutput(
                    elRenderer_PostProcessing::GUI_GLOW ),
                &this->renderer.postProcessing->GetOutput(
                    elRenderer_PostProcessing::GLOW ),
                &this->renderer.postProcessing->GetOutput(
                    elRenderer_PostProcessing::SCREEN_SPACE_REFLECTIONS ) } ) );
}

elRenderPipeline::~elRenderPipeline()
{
    // need to be clean up after renderer since they hold some pipeline textures
    this->outputTextures.clear();
}

void
elRenderPipeline::UseSkyboxForReflection( const bool v ) noexcept
{
    this->renderer.postProcessing->UseSkyboxForReflection( v );
}

const OpenGL::elCamera_Perspective &
elRenderPipeline::GetCamera() const noexcept
{
    return this->camera;
}

OpenGL::elCamera_Perspective &
elRenderPipeline::GetCamera() noexcept
{
    return this->camera;
}

void
elRenderPipeline::Render() noexcept
{
    this->renderer.shaderTexture->Render( this->camera );
    this->renderer.cleanupTextures->Render( this->camera );
    this->renderer.deferred->Render( this->camera );
    this->renderer.forward->Render( this->camera );
    this->renderer.gui->Render( this->camera );
    this->renderer.postProcessing->Render( this->camera );
    this->renderer.final->Render( this->camera );

    for ( auto &l :
          this->factory.GetProducts< HighGL::elLight_DirectionalLight >() )
        l->SettingsUpdated();

    for ( auto &l : this->factory.GetProducts< HighGL::elLight_PointLight >() )
        l->SettingsUpdated();
}

void
elRenderPipeline::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->camera.SetViewPortSize( { x, y } );

    for ( auto &r : this->renderPipe.all )
        r->Reshape( x, y );
}

void
elRenderPipeline::DrawOutputToScreen( const GLint x,
                                      const GLint y ) const noexcept
{
    this->renderer.final->DrawToScreen( x, y );
}

glm::vec4
elRenderPipeline::GetEventColorAtPosition(
    const glm::ivec2 position ) const noexcept
{
    return this->renderer.final->GetEventColorAtPosition( position );
}

void
elRenderPipeline::renderPipe_t::emplace_back(
    elRenderer_base *const renderer ) noexcept
{
    this->all.emplace_back( renderer );
    if ( renderer->GetType() == elRenderer_Type::gui )
        this->top.emplace_back( renderer );
    else
        this->scene.emplace_back( renderer );
}

bool
elRenderPipeline::renderPipe_t::erase(
    const elRenderer_base *const renderer ) noexcept
{
    auto iter = bb::find( this->all, renderer );
    if ( iter != this->all.end() )
        this->all.erase( iter );
    else
        return false;

    if ( renderer->GetType() == elRenderer_Type::gui )
    {
        auto iter = bb::find( this->top, renderer );
        if ( iter != this->top.end() ) this->top.erase( iter );
    }
    else
    {
        auto iter = bb::find( this->scene, renderer );
        if ( iter != this->scene.end() ) this->scene.erase( iter );
    }

    return true;
}

size_t
elRenderPipeline::RegisterShaderTexture(
    const HighGL::elShaderTexture *const v ) noexcept
{
    return this->renderer.shaderTexture->AddShaderTexture( v );
}

void
elRenderPipeline::DeregisterShaderTexture( const size_t id ) noexcept
{
    this->renderer.shaderTexture->RemoveShaderTexture( id );
}

elRenderer_GUI *
elRenderPipeline::GetGUIRenderer() noexcept
{
    return this->renderer.gui.get();
}

elRenderer_Deferred *
elRenderPipeline::GetDeferredRenderer() noexcept
{
    return this->renderer.deferred.get();
}

elRenderer_Forward *
elRenderPipeline::GetForwardRenderer() noexcept
{
    return this->renderer.forward.get();
}

bb::product_ptr< HighGL::elLight_PointLight >
elRenderPipeline::CreatePointLight( const glm::vec3 &color,
                                    const glm::vec3 &attenuation,
                                    const glm::vec3 &position,
                                    const float      diffuseIntensity,
                                    const float      ambientIntensity ) noexcept
{
    return this->factory.CreateProduct< HighGL::elLight_PointLight >(
        color, attenuation, position, diffuseIntensity, ambientIntensity );
}

bb::product_ptr< HighGL::elLight_DirectionalLight >
elRenderPipeline::CreateDirectionalLight(
    const glm::vec3 &color, const glm::vec3 &direction,
    const GLfloat diffuseIntensity, const GLfloat ambientIntensity ) noexcept
{
    if ( this->factory.GetProducts< HighGL::elLight_DirectionalLight >()
             .size() >= this->maxDirectionalLights )
    {
        LOG_FATAL( 0 ) << "only " << this->maxDirectionalLights
                       << " directional lights are supported";
        std::terminate();
    }

    return this->factory.CreateProduct< HighGL::elLight_DirectionalLight >(
        color, direction, diffuseIntensity, ambientIntensity );
}

void
elRenderPipeline::RegisterSkyboxTexture(
    const OpenGL::elTexture_CubeMap &texture ) noexcept
{
    this->optionalSkyboxTexture = &texture;
}

void
elRenderPipeline::ResetSkyboxTexture() noexcept
{
    this->optionalSkyboxTexture = nullptr;
}


} // namespace RenderPipeline
} // namespace el3D
