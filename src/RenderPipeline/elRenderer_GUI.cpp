#include "RenderPipeline/elRenderer_GUI.hpp"

#include "GuiGL/common.hpp"
#include "OpenGL/elGLSLParser.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "RenderPipeline/RenderPipeline_NAMESPACE_NAME.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"
#include "RenderPipeline/internal.hpp"

namespace el3D
{
namespace RenderPipeline
{
elRenderer_GUI::elRenderer_GUI(
    internal::rendererShared_t shared, GLAPI::elFontCache *const fontCache,
    const std::function< size_t( HighGL::elShaderTexture * ) >
        &                                  shaderTextureRegistrator,
    const std::function< void( size_t ) > &shaderTextureDeregistrator,
    GLAPI::elEventHandler *const           eventHandler )
    : elRenderer_base( elRenderer_Type::gui, FBO_END, shared )
{
    this->CreateTexture( this->texture.final, "gui:final", GL_RGBA8, GL_RGBA,
                         GL_FLOAT, Attachment::Color, FBO_DIFFUSE );
    this->AttachTexture( this->texture.event, shared.eventTexture,
                         Attachment::Color, FBO_DIFFUSE );
    this->CreateTexture( this->texture.glow, "gui:glow", GL_RGBA8, GL_RGBA,
                         GL_FLOAT, Attachment::Color, FBO_GLOW );


    this->glowScaling = shared.config->Get< float >(
        { "global", "scenes", "main", "postEffects", "glow", "gaussianBlur",
          "guiScaling" },
        { this->glowScaling } )[0];

    std::string guiThemeFile = shared.config->Get< std::string >(
        { "global", "scenes", shared.sceneName, "gui", "theme" },
        { "cfg/guiTheme.lua" } )[0];

    this->shader        = this->CreateShaderProgram( "gui", "gui", "geometry" );
    this->guiGlowShader = this->CreateShaderProgram( "gui", "gui", "glow" );

    // setup layers
    this->AttachTexture( this->texture.depth, shared.depthTexture,
                         Attachment::DepthStencil );

    this->theme.SetConfig( guiThemeFile );
    GuiGL::constructor_t constructor{ nullptr,
                                      this->shader.get(),
                                      this->guiGlowShader.get(),
                                      shared.eventManager,
                                      eventHandler,
                                      nullptr,
                                      window,
                                      fontCache,
                                      nullptr,
                                      shaderTextureRegistrator,
                                      shaderTextureDeregistrator,
                                      this->window->GetShaderVersion(),
                                      &this->theme,
                                      GuiGL::widgetType_t::TYPE_EMPTY,
                                      &this->screenWidth,
                                      &this->screenHeight,
                                      "",
                                      GuiGL::TYPE_END,
                                      this->glowScaling,
                                      this->callbackContainer };

    this->cursorLayer = std::make_unique< GuiGL::elWidget_base >( constructor );
    this->mouseCursor =
        this->cursorLayer->Create< GuiGL::elWidget_MouseCursor >();

    constructor.mouseCursor = this->mouseCursor.Get();
    this->windowLayer = std::make_unique< GuiGL::elWidget_base >( constructor );
}

void
elRenderer_GUI::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    if ( x > 0 && y > 0 )
    {
        this->screenWidth  = static_cast< size_t >( x );
        this->screenHeight = static_cast< size_t >( y );
        this->fbo->SetSize( x, y, FBO_DIFFUSE );
        this->fbo->SetSize( static_cast< uint64_t >( static_cast< float >( x ) *
                                                     this->glowScaling ),
                            static_cast< uint64_t >( static_cast< float >( y ) *
                                                     this->glowScaling ),
                            FBO_GLOW );

        float dpiScaling = GuiGL::GetDPIScaling();
        this->windowLayer->SetSize( glm::vec2(
            static_cast< float >( this->screenWidth ) / dpiScaling,
            static_cast< float >( this->screenHeight ) / dpiScaling ) );

        this->cursorLayer->SetSize( glm::vec2(
            static_cast< float >( this->screenWidth ) / dpiScaling,
            static_cast< float >( this->screenHeight ) / dpiScaling ) );
    }

    this->windowLayer->Reshape();
    this->cursorLayer->Reshape();
}

void
elRenderer_GUI::Render( const OpenGL::elCamera_Perspective & ) noexcept
{
    OpenGL::Disable( GL_DEPTH_TEST );
    OpenGL::Enable( GL_BLEND );
    OpenGL::gl( glBlendEquation, static_cast< GLenum >( GL_FUNC_ADD ) );
    OpenGL::gl( glBlendFuncSeparate, static_cast< GLenum >( GL_SRC_ALPHA ),
                static_cast< GLenum >( GL_ONE_MINUS_SRC_ALPHA ),
                static_cast< GLenum >( GL_ONE ),
                static_cast< GLenum >( GL_ONE ) );

    this->fbo->Bind(
        this->GenerateRenderTargets(
            { this->texture.final.target, this->texture.event.target } ),
        FBO_DIFFUSE );
    this->fbo->Clear( FBO_DIFFUSE, this->texture.final.target );

    this->windowLayer->Draw( GuiGL::RENDER_PASS_DIFFUSE );
    this->cursorLayer->Draw( GuiGL::RENDER_PASS_DIFFUSE );

    this->fbo->Unbind();


    this->fbo->Bind(
        this->GenerateRenderTargets( { this->texture.glow.target } ),
        FBO_GLOW );

    OpenGL::gl( glClearColor, 0.0f, 0.0f, 0.0f, 0.0f );
    OpenGL::gl( glClear, static_cast< GLbitfield >( GL_COLOR_BUFFER_BIT |
                                                    GL_DEPTH_BUFFER_BIT ) );

    this->windowLayer->Draw( GuiGL::RENDER_PASS_GLOW );
    this->cursorLayer->Draw( GuiGL::RENDER_PASS_GLOW );

    this->fbo->Unbind();

    auto callbackTempContainer = this->callbackContainer;
    this->callbackContainer.clear();

    for ( auto &callback : callbackTempContainer )
        callback();
}

GuiGL::elWidget_base *
elRenderer_GUI::GetRoot() const noexcept
{
    return this->windowLayer.get();
}

GuiGL::elWidget_MouseCursor *
elRenderer_GUI::GetMouseCursor() noexcept
{
    return this->mouseCursor.Get();
}

void
elRenderer_GUI::SetShowMouseCursor( const bool v ) noexcept
{
    this->mouseCursor->SetDoRenderWidget( v );
    this->mouseCursor->SetDisableEventHandling( !v );
    if ( !v ) this->mouseCursor->SetPosition( { 0, 0 } );
}

bool
elRenderer_GUI::DoesShowMouseCursor() const noexcept
{
    return this->mouseCursor->DoRenderWidget();
}


const OpenGL::elTexture_2D *
elRenderer_GUI::GetGuiTexture() const noexcept
{
    return dynamic_cast< OpenGL::elTexture_2D * >(
        this->fboDetails[FBO_DIFFUSE]
            .outputTextures[this->texture.final.textureId] );
}

const OpenGL::elTexture_2D *
elRenderer_GUI::GetGuiGlowTexture() const noexcept
{
    return dynamic_cast< OpenGL::elTexture_2D * >(
        this->fboDetails[FBO_GLOW]
            .outputTextures[this->texture.glow.textureId] );
}

} // namespace RenderPipeline
} // namespace el3D
