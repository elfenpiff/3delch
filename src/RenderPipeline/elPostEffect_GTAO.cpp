#include "RenderPipeline/elPostEffect_GTAO.hpp"

#include "lua/elConfigHandler.hpp"

namespace el3D
{
namespace RenderPipeline
{
elPostEffect_GTAO::elPostEffect_GTAO(
    const std::string& sceneName, const lua::elConfigHandler* const config,
    const OpenGL::elTexture_2D* const depth,
    const OpenGL::elTexture_2D* const normal,
    const OpenGL::elTexture_2D* const diffuse ) noexcept
{
    this->scaling =
        config->Get< float >( { "global", "scenes", sceneName, "postEffects",
                                "ambientOcclusion", "gtao", "scaling" },
                              { this->scaling } )[0];

    this->lengthOfOneMeter = config->Get< float >(
        { "global", "scenes", sceneName, "postEffects", "ambientOcclusion",
          "gtao", "lengthOfOneMeter" },
        { this->lengthOfOneMeter } )[0];

    // config fetcher
    auto getShaderFileAndGroup = [&]( const std::string& entry,
                                      const std::string& defaultShader,
                                      const std::string& defaultGroup )
    {
        std::string file = config->Get< std::string >(
            { "global", "scenes", sceneName, "postEffects", "ambientOcclusion",
              "gtao", entry, "file" },
            { defaultShader } )[0];
        std::string group = config->Get< std::string >(
            { "global", "scenes", sceneName, "postEffects", "ambientOcclusion",
              "gtao", entry, "group" },
            { defaultGroup } )[0];
        return std::make_pair( file, group );
    };

    // create random texture
    this->randomTexture = std::make_unique< OpenGL::elTexture_2D >(
        "elPostEffect_GTAO::noise", 4, 4, GL_RG8, GL_RG, GL_UNSIGNED_BYTE );
    this->randomTexture->TexImage(
        OpenGL::elTexture_base::GenerateRandomTexture( 32 ).data() );

    // create gtao shader
    auto gtaoShaderFile = getShaderFileAndGroup(
        "gtaoShader", "glsl/PostProcessing/gtao.glsl", "default" );
    auto shader =
        OpenGL::elGLSLParser::Create( gtaoShaderFile.first, 440 )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to open GTAO post effect shader file "
                        << gtaoShaderFile.first;
                    std::terminate();
                } );
    auto shaderTexture =
        HighGL::elShaderTexture::Create(
            std::vector< OpenGL::textureProperties_t >{
                { "gtao", GL_R8, GL_RED, GL_FLOAT } },
            ( *shader )
                ->GetContent()
                .at( gtaoShaderFile.second )
                .at( GL_FRAGMENT_SHADER )
                .code,
            512, 512, false, true )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to create GTAO shader from source "
                        << gtaoShaderFile.first << ":" << gtaoShaderFile.second;
                    std::terminate();
                } );

    ( *shaderTexture )->AddInputTexture( "gbufferDepth", depth );
    ( *shaderTexture )->AddInputTexture( "gbufferNormals", normal );
    ( *shaderTexture )->AddInputTexture( "noise", this->randomTexture.get() );

    ( *shaderTexture )
        ->RegisterUniform(
            "invertedResolution",
            [&]( auto shader, auto uniformID )
            {
                shader->SetUniform(
                    uniformID, 1.0f / this->scaledCamera.GetViewPortSize() );
            } );
    ( *shaderTexture )
        ->RegisterUniform(
            "clipInfo",
            [&]( auto shader, auto uniformID )
            {
                auto& c            = this->scaledCamera.GetState();
                auto  viewPortSize = this->scaledCamera.GetViewPortSize();
                auto  z            = this->scaledCamera.GetZFactor();
                shader->SetUniform(
                    uniformID,
                    glm::vec4( z.zNear.GetMeter(), z.zFar.GetMeter(),
                               0.5f * ( viewPortSize.y /
                                        ( 2.0f * tanf( static_cast< float >(
                                                           c.fov.GetRadian() ) *
                                                       0.5f ) ) ),
                               1.0f / this->scaling ) );
            } );
    ( *shaderTexture )
        ->RegisterUniform(
            "projInfo",
            [&]( auto shader, auto uniformID )
            {
                auto  viewPortSize = this->scaledCamera.GetViewPortSize();
                auto& m            = this->scaledCamera.GetMatrices();
                shader->SetUniform(
                    uniformID,
                    glm::vec4( 2.0f / ( viewPortSize.x * m.projection[0][0] ),
                               2.0f / ( viewPortSize.y * m.projection[1][1] ),
                               -1.0f / m.projection[0][0],
                               -1.0f / m.projection[1][1] ) );
            } );
    ( *shaderTexture )
        ->RegisterUniform(
            "inverseView",
            [&]( auto shader, auto uniformID )
            {
                shader->SetUniform(
                    uniformID,
                    glm::inverse( this->scaledCamera.GetMatrices().view ) );
            } );

    ( *shaderTexture )
        ->RegisterUniform(
            "lengthOfOneMeter", [&]( auto shader, auto uniformID )
            { shader->SetUniform( uniformID, this->lengthOfOneMeter ); } );

    this->ambientOcclusion = std::move( shaderTexture.GetValue() );

    // create blur shader
    auto blurShaderFile = getShaderFileAndGroup(
        "blurShader", "glsl/PostProcessing/gtaoSpatialBlur.glsl", "default" );
    shader = OpenGL::elGLSLParser::Create( blurShaderFile.first, 440 )
                 .OrElse(
                     [&]
                     {
                         LOG_FATAL( 0 )
                             << "unable to open GTAO blur shader file "
                             << blurShaderFile.first;
                         std::terminate();
                     } );
    shaderTexture =
        HighGL::elShaderTexture::Create(
            std::vector< OpenGL::textureProperties_t >{
                { "gtaoSpatialBlur", GL_R8, GL_RED, GL_FLOAT } },
            ( *shader )
                ->GetContent()
                .at( blurShaderFile.second )
                .at( GL_FRAGMENT_SHADER )
                .code,
            512, 512, false, false )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to create GTAO blur shader from source "
                        << blurShaderFile.first << ":" << blurShaderFile.second;
                    std::terminate();
                } );
    ( *shaderTexture )
        ->RegisterUniform(
            "invertedResolution",
            [&]( auto shader, auto uniformID )
            {
                shader->SetUniform(
                    uniformID, 1.0f / this->scaledCamera.GetViewPortSize() );
            } );
    ( *shaderTexture )
        ->AddInputShaderTexture( "gtao", this->ambientOcclusion.get(), 0 );
    ( *shaderTexture )->AddInputTexture( "depth", depth );
    this->spatialBlur = std::move( shaderTexture.GetValue() );

    // create final shader
    auto finalShaderFile = getShaderFileAndGroup(
        "finalShader", "glsl/PostProcessing/gtaoFinal.glsl", "default" );
    shader = OpenGL::elGLSLParser::Create( finalShaderFile.first, 440 )
                 .OrElse(
                     [&]
                     {
                         LOG_FATAL( 0 )
                             << "unable to open final GTAO shader file "
                             << finalShaderFile.first;
                         std::terminate();
                     } );
    shaderTexture =
        HighGL::elShaderTexture::Create(
            std::vector< OpenGL::textureProperties_t >{ { "gtaoFinal" } },
            ( *shader )
                ->GetContent()
                .at( finalShaderFile.second )
                .at( GL_FRAGMENT_SHADER )
                .code,
            512, 512, false, false )
            .OrElse(
                [&]
                {
                    LOG_FATAL( 0 )
                        << "unable to create final GTAO shader from source "
                        << finalShaderFile.first << ":"
                        << finalShaderFile.second;
                    std::terminate();
                } );
    ( *shaderTexture )
        ->AddInputShaderTexture( "gtao", this->spatialBlur.get(), 0 );
    ( *shaderTexture )->AddInputTexture( "diffuse", diffuse );
    ( *shaderTexture )
        ->RegisterUniform( "scaling",
                           [&]( auto shader, auto uniformID ) {
                               shader->SetUniform( uniformID, this->scaling );
                           } );
    this->final = std::move( shaderTexture.GetValue() );
}

void
elPostEffect_GTAO::Render( const OpenGL::elCamera_Perspective& camera ) noexcept
{
    this->scaledCamera.SetState( camera.GetState() );
    this->scaledCamera.SetViewPortSize( camera.GetViewPortSize() *
                                        this->scaling );

    this->final->Refresh();
}

void
elPostEffect_GTAO::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    this->ambientOcclusion->SetSize(
        static_cast< uint64_t >( static_cast< float >( x ) * this->scaling ),
        static_cast< uint64_t >( static_cast< float >( y ) * this->scaling ) );
    this->spatialBlur->SetSize(
        static_cast< uint64_t >( static_cast< float >( x ) * this->scaling ),
        static_cast< uint64_t >( static_cast< float >( y ) * this->scaling ) );
    this->final->SetSize( x, y );
}

OpenGL::elTexture_2D*
elPostEffect_GTAO::GetOutput() noexcept
{
    return &this->final->GetOutputTexture( 0 );
}

void
elPostEffect_GTAO::SetLengthOfOneMeter( const float v ) noexcept
{
    this->lengthOfOneMeter = v;
}


} // namespace RenderPipeline

} // namespace el3D
