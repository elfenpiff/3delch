#include "RenderPipeline/elRenderer_Deferred.hpp"

#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "OpenGL/elCamera_Dummy.hpp"
#include "OpenGL/elCamera_Perspective.hpp"
#include "OpenGL/elGLSLParser.hpp"
#include "RenderPipeline/elRenderPipelineManager.hpp"

#include <glm/gtx/string_cast.hpp>

namespace el3D
{
namespace RenderPipeline
{
elRenderer_Deferred::elRenderer_Deferred(
    internal::rendererShared_t shared ) noexcept
    : elRenderer_base( elRenderer_Type::deferred, FBO_END, shared )
{
    this->passThrough.SetUp( this->CreateShaderProgram(
        "deferred", "passThroughShader", "default" ) );

    // create general textures
    attachmentId_t none;
    this->CreateTexture( this->texture.diffuse, "deferred:diffuse", GL_RGBA8,
                         GL_RGBA, GL_FLOAT, Attachment::Color,
                         FBO_FINAL_IMAGE );
    this->CreateTexture( this->texture.position, "deferred:position", GL_RGB32F,
                         GL_RGB, GL_FLOAT, Attachment::Color, FBO_FINAL_IMAGE );
    this->CreateTexture( this->texture.normal, "deferred:normal", GL_RGB32F,
                         GL_RGB, GL_FLOAT, Attachment::Color, FBO_FINAL_IMAGE );
    this->CreateTexture( this->texture.specular, "deferred:specular", GL_RGB8,
                         GL_RGB, GL_FLOAT, Attachment::Color, FBO_FINAL_IMAGE );
    this->AttachTexture( this->texture.material, shared.materialTexture,
                         Attachment::Color, FBO_FINAL_IMAGE );
    this->AttachTexture( this->texture.event, shared.eventTexture,
                         Attachment::Color, FBO_FINAL_IMAGE );

    // directionalLight
    auto maxDirectionalLights =
        this->config->Get< int >( { "global", "scenes", this->sceneName,
                                    "light", "maxDirectionalLights" },
                                  { 1 } )[0];

    OpenGL::elGLSLParser::staticDefines_t defines;
    defines[OpenGL::elGLSLParser::groupType_t{ "directionalLight",
                                               GL_FRAGMENT_SHADER }]
        .emplace_back( OpenGL::elGLSLParser::define_t{
            "MAX_LIGHTS", std::to_string( maxDirectionalLights ) } );

    this->directionalLightShaderSettings.SetUp( this->CreateShaderProgram(
        "deferred", "light", "directionalLight", defines ) );

    this->settings.unifiedVertexObject.SetUp( ObjectType::UnifiedVertexObject,
                                              this );
    this->settings.instancedObject.SetUp( ObjectType::InstancedObject, this );
    this->settings.vertexObject.SetUp( ObjectType::VertexObject, this );

    auto shadowMapSize = this->config->GetWithSizeRestriction< int, 2 >(
        { "global", "scenes", this->sceneName, "light",
          "shadowMapSize_DirectionalLight" },
        { 1024, 1024 } );
    this->directionalLightShaderSettings.shadowMapSize =
        glm::uvec2( shadowMapSize[0], shadowMapSize[1] );

    this->CreateTexture( none, "", GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT,
                         GL_FLOAT, Attachment::Depth, FBO_DIRECTIONAL_SHADOW )
        .SetWrap( GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE )
        .SetCompareParameters( GL_COMPARE_REF_TO_TEXTURE, GL_LEQUAL );

    this->CreateTextureArray( this->texture.directionalLightShadowMaps, "",
                              GL_R32F, GL_RED, GL_FLOAT,
                              static_cast< uint64_t >( maxDirectionalLights ),
                              FBO_DIRECTIONAL_SHADOW );

    // pointLightShader
    shadowMapSize = this->config->GetWithSizeRestriction< int, 2 >(
        { "global", "scenes", this->sceneName, "light",
          "shadowMapSize_PointLight" },
        { 256, 256 } );
    pointLightShaderSettings.SetUp(
        this->CreateShaderProgram( "deferred", "light", "pointLight" ),
        { shadowMapSize[0], shadowMapSize[1] } );

    this->CreateTexture( none, "", GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT,
                         GL_FLOAT, Attachment::Depth, FBO_POINTLIGHT_SHADOW )
        .SetWrap( GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE )
        .SetCompareParameters( GL_COMPARE_REF_TO_TEXTURE, GL_LEQUAL );
    this->CreateTextureCubeMap( this->texture.pointLightShadowMap, "", GL_R32F,
                                GL_RED, GL_FLOAT, FBO_POINTLIGHT_SHADOW )
        .SetWrap( GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE );

    // attach shared textures
    this->AttachTexture( this->texture.final, shared.finalTexture,
                         Attachment::Color, FBO_FINAL_IMAGE );
    this->AttachTexture( this->texture.depth, shared.depthTexture,
                         Attachment::DepthStencil, FBO_FINAL_IMAGE );
    this->AttachTexture( this->texture.glow, shared.glowTexture,
                         Attachment::Color, FBO_FINAL_IMAGE );

    this->ambientOcclusion = std::make_unique< elPostEffect_GTAO >(
        shared.sceneName, shared.config, shared.depthTexture,
        this->GetNormalTexture(), shared.finalTexture );
}

elRenderer_Deferred::~elRenderer_Deferred()
{
}

void
elRenderer_Deferred::Reshape( const uint64_t x, const uint64_t y ) noexcept
{
    if ( x > 0 && y > 0 )
    {
        this->fbo->SetSize(
            this->directionalLightShaderSettings.shadowMapSize.x,
            this->directionalLightShaderSettings.shadowMapSize.y,
            FBO_DIRECTIONAL_SHADOW );
        this->fbo->SetSize( x, y, FBO_FINAL_IMAGE );
        this->fbo->SetSize( this->pointLightShaderSettings.shadowMapSize.x,
                            this->pointLightShaderSettings.shadowMapSize.y,
                            FBO_POINTLIGHT_SHADOW );

        this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
            0, directionalLightShader_t::RESOLUTION,
            glm::value_ptr( glm::ivec2( x, y ) ), 2 * sizeof( GLint ) );

        for ( auto &block : this->pointLightShaderSettings.uniformBuffers )
            block.SetVariableData( 0, pointLightShader_t::RESOLUTION,
                                   glm::value_ptr( glm::ivec2( x, y ) ),
                                   2 * sizeof( GLint ) );

        this->ambientOcclusion->Reshape( x, y );
    }
}

void
elRenderer_Deferred::UpdateDirectionalLightBuffer(
    const HighGL::elLight_DirectionalLight *light,
    const size_t                            arrayIndex ) noexcept
{
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::COLOR,
        glm::value_ptr( light->GetColor() ), 3 * sizeof( GLfloat ) );

    auto direction = light->GetDirection();

    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::DIRECTION,
        glm::value_ptr( direction ), 3 * sizeof( GLfloat ) );

    GLfloat temp = light->GetAmbientIntensity();
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::AMBIENT_INTENSITY, &temp,
        sizeof( GLfloat ) );
    temp = light->GetDiffuseIntensity();
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::DIFFUSE_INTENSITY, &temp,
        sizeof( GLfloat ) );

    GLint bValue = light->HasFog();
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::HAS_FOG, &bValue,
        sizeof( GLint ) );

    temp = light->GetFogIntensity();
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::FOG_INTENSITY, &temp,
        sizeof( GLfloat ) );

    GLint hasShadow = light->DoesCastShadow();
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::HAS_SHADOW, &hasShadow,
        sizeof( GLint ) );
}

void
elRenderer_Deferred::UpdateDirectionalLightCameraBuffer(
    const HighGL::elLight_DirectionalLight *light,
    const size_t                            arrayIndex ) noexcept
{
    this->directionalLightShaderSettings.camera[arrayIndex] =
        light->GetOrthographicCamera();

    constexpr glm::mat4 BIAS =
        // clang-format off
            glm::mat4(0.5, 0.0, 0.0, 0.0,
                      0.0, 0.5, 0.0, 0.0,
                      0.0, 0.0, 0.5, 0.0,
                      0.5, 0.5, 0.5, 1.0);
    // clang-format on

    glm::mat4 biasedViewProjection =
        BIAS * this->directionalLightShaderSettings.camera[arrayIndex]
                   .GetMatrices()
                   .viewProjection;

    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        arrayIndex, directionalLightShader_t::BIASED_VIEW_PROJECTION,
        glm::value_ptr( biasedViewProjection ), 16 * sizeof( GLfloat ) );
}

void
elRenderer_Deferred::RenderDirectionalLightShadowMap(
    const OpenGL::elCamera_Perspective &camera ) noexcept
{
    if ( this->directionalLights->empty() ) return;

    uint64_t directionalLightsSize = this->directionalLights->size();
    if ( directionalLightsSize !=
         this->directionalLightShaderSettings.camera.size() )
        this->directionalLightShaderSettings.camera.resize(
            directionalLightsSize );

    float zFar = static_cast< float >( camera.GetZFactor().zFar.GetMeter() );

    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        0, directionalLightShader_t::CAMERA_POSITION,
        glm::value_ptr( camera.GetState().position ), 3 * sizeof( GLfloat ) );

    for ( size_t k = 0; k < directionalLightsSize; ++k )
    {
        auto &light = ( *this->directionalLights )[k];
        if ( !light ) continue;
        light->UpdateCameraParameters( camera.GetState().position, zFar );
        this->UpdateDirectionalLightCameraBuffer( light.Get(), k );

        if ( light->HasUpdatedSettings() )
            this->UpdateDirectionalLightBuffer( light.Get(), k );
    }

    OpenGL::gl( glClearColor, FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX );
    OpenGL::Enable( GL_CULL_FACE );
    OpenGL::Enable( GL_DEPTH_TEST );
    OpenGL::gl( glCullFace, static_cast< GLenum >( GL_BACK ) );
    OpenGL::gl( glDepthFunc, static_cast< GLenum >( GL_LEQUAL ) );
    OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_TRUE ) );

    for ( uint64_t i = 0; i < directionalLightsSize; ++i )
    {
        if ( !( *this->directionalLights )[i]->DoesCastShadow() ) continue;
        this->fbo->Bind(
            this->GenerateRenderTargets(
                { this->texture.directionalLightShadowMaps.target[i] } ),
            FBO_DIRECTIONAL_SHADOW );

        OpenGL::gl( glClear, static_cast< GLuint >( GL_COLOR_BUFFER_BIT |
                                                    GL_DEPTH_BUFFER_BIT ) );

        for ( auto &o :
              this->factory.GetProducts< OpenGL::elGeometricObject_base >() )
            o->Draw( o.Extension().directionalLightShadowMapShader,
                     this->directionalLightShaderSettings.camera[i] );

        this->fbo->Unbind();
    }

    OpenGL::Disable( GL_CULL_FACE );
    OpenGL::Disable( GL_DEPTH_TEST );
}

void
elRenderer_Deferred::RenderDirectionalLight(
    const OpenGL::elCamera &camera ) noexcept
{
    if ( this->directionalLights->empty() ) return;

    uint64_t directionalLightsSize = this->directionalLights->size();

    // set number of directional lights uniform
    this->directionalLightShaderSettings.shader->Bind();
    if ( this->directionalLightShaderSettings.numberOfLights !=
         static_cast< GLint >( directionalLightsSize ) )
    {
        this->directionalLightShaderSettings.numberOfLights =
            static_cast< GLint >( directionalLightsSize );

        this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
            0, directionalLightShader_t::NUMBER_OF_LIGHTS,
            &this->directionalLightShaderSettings.numberOfLights,
            sizeof( GLint ) );
    }

    float shadowBorderSize =
        static_cast< float >( camera.GetZFactor().zNear.GetMeter() /
                              ( camera.GetZFactor().zFar.GetMeter() * 4 ) );
    this->directionalLightShaderSettings.uniformBuffer->SetVariableData(
        0, directionalLightShader_t::SHADOW_BORDER_SIZE, &shadowBorderSize,
        sizeof( float ) );

    // bind fbo and render onScreenQuad
    this->fbo->GetTexture( this->texture.normal.textureId, FBO_FINAL_IMAGE )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texNormal, GL_TEXTURE0 );
    this->fbo->GetTexture( this->texture.diffuse.textureId, FBO_FINAL_IMAGE )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texDiffuse, GL_TEXTURE1 );
    this->fbo->GetTexture( this->texture.position.textureId, FBO_FINAL_IMAGE )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texPosition, GL_TEXTURE2 );
    this->fbo
        ->GetTexture( this->texture.directionalLightShadowMaps.textureId,
                      FBO_DIRECTIONAL_SHADOW )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texShadowMap, GL_TEXTURE3 );
    this->fbo->GetTexture( this->texture.material.textureId, FBO_FINAL_IMAGE )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texMaterial, GL_TEXTURE4 );
    this->fbo->GetTexture( this->texture.specular.textureId, FBO_FINAL_IMAGE )
        ->BindActivateAndSetUniform(
            this->directionalLightShaderSettings.texSpecular, GL_TEXTURE5 );


    this->fbo->Bind(
        this->GenerateRenderTargets( { this->texture.final.target } ),
        FBO_FINAL_IMAGE );


    HighGL::elLight_DirectionalLight::Bind(
        this->directionalLightShaderSettings.coordAttrib );

    this->directionalLightShaderSettings.uniformBuffer->BindToShader();
    OpenGL::gl( glDrawElements, static_cast< GLenum >( GL_TRIANGLES ), 6,
                static_cast< GLenum >( GL_UNSIGNED_INT ),
                static_cast< GLvoid * >( nullptr ) );

    HighGL::elLight_DirectionalLight::Unbind(
        this->directionalLightShaderSettings.coordAttrib );

    this->fbo->Unbind();
}

void
elRenderer_Deferred::UpdatePointLightBuffer( const size_t k ) noexcept
{
    auto &light         = ( *this->pointLights )[k];
    auto &uniformBuffer = this->pointLightShaderSettings.uniformBuffers[k];

    uniformBuffer.SetVariableData( 0, pointLightShader_t::COLOR,
                                   glm::value_ptr( light->GetColor() ),
                                   3 * sizeof( GLfloat ) );

    uniformBuffer.SetVariableData( 0, pointLightShader_t::POSITION,
                                   glm::value_ptr( light->GetPosition() ),
                                   3 * sizeof( GLfloat ) );

    uniformBuffer.SetVariableData( 0, pointLightShader_t::ATTENUATION,
                                   glm::value_ptr( light->GetAttenuation() ),
                                   3 * sizeof( GLfloat ) );

    float intensity = light->GetDiffuseIntensity();
    uniformBuffer.SetVariableData( 0, pointLightShader_t::INTENSITY, &intensity,
                                   sizeof( GLfloat ) );

    intensity = light->GetAmbientIntensity();
    uniformBuffer.SetVariableData( 0, pointLightShader_t::AMBIENT_INTENSITY,
                                   &intensity, sizeof( GLfloat ) );

    GLint hasShadow = light->DoesCastShadow();
    uniformBuffer.SetVariableData( 0, pointLightShader_t::HAS_SHADOW,
                                   &hasShadow, sizeof( GLint ) );
}

void
elRenderer_Deferred::CreatePointLightBufferBlock( const int s ) noexcept
{
    for ( int i = 0; i < s; ++i )
    {
        auto buffer = OpenGL::elUniformBufferObject::Create(
            "light_t",
            std::vector< std::string >{
                "light_t.color", "light_t.position", "light_t.attenuation",
                "light_t.intensity", "light_t.ambientIntensity",
                "light_t.resolution", "light_t.hasShadow",
                "light_t.cameraPosition" },
            this->pointLightShaderSettings.shader.get() );

        if ( buffer.HasError() )
        {
            LOG_FATAL( 0 )
                << "unable to find required uniform buffer \"light_t\" in "
                   "point light shader";
            std::terminate();
        }

        this->pointLightShaderSettings.uniformBuffers.emplace_back(
            std::move( *buffer.GetValue() ) );
        auto size = this->fbo->GetSize( FBO_FINAL_IMAGE );
        this->pointLightShaderSettings.uniformBuffers.back().SetVariableData(
            0, pointLightShader_t::RESOLUTION, glm::value_ptr( size ),
            2 * sizeof( GLint ) );
    }
}

void
elRenderer_Deferred::RenderPointLightShadowMap(
    const HighGL::elLight_PointLight *const light ) noexcept
{
    struct cameraParameter_t
    {
        GLenum    cubeFace;
        glm::vec3 lookat;
        glm::vec3 up;
    };

    static constexpr cameraParameter_t CAMERA_PARAMETERS[6] = {
        { GL_TEXTURE_CUBE_MAP_POSITIVE_X, glm::vec3( 1.0f, 0.0f, 0.0f ),
          glm::vec3( 0.0f, -1.0f, 0.0f ) },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_X, glm::vec3( -1.0f, 0.0f, 0.0f ),
          glm::vec3( 0.0f, -1.0f, 0.0f ) },
        { GL_TEXTURE_CUBE_MAP_POSITIVE_Y, glm::vec3( 0.0f, 1.0f, 0.0f ),
          glm::vec3( 0.0f, 0.0f, 1.0f ) },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, glm::vec3( 0.0f, -1.0f, 0.0f ),
          glm::vec3( 0.0f, 0.0f, -1.0f ) },
        { GL_TEXTURE_CUBE_MAP_POSITIVE_Z, glm::vec3( 0.0f, 0.0f, 1.0f ),
          glm::vec3( 0.0f, -1.0f, 0.0f ) },
        { GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, glm::vec3( 0.0f, 0.0f, -1.0f ),
          glm::vec3( 0.0f, -1.0f, 0.0f ) } };

    OpenGL::gl( glClearColor, FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX );
    OpenGL::Enable( GL_CULL_FACE );
    OpenGL::Enable( GL_DEPTH_TEST );
    OpenGL::gl( glCullFace, static_cast< GLenum >( GL_FRONT ) );
    OpenGL::gl( glDepthFunc, static_cast< GLenum >( GL_LEQUAL ) );
    OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_TRUE ) );

    glm::vec3 lightPosition = light->GetPosition();
    glm::mat4 frustum =
        glm::frustum( -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, light->GetRadius() );

    for ( uint64_t side = 0; side < 6; ++side )
    {
        OpenGL::elCamera_Dummy lightCamera{
            frustum *
            glm::lookAt( lightPosition,
                         lightPosition + CAMERA_PARAMETERS[side].lookat,
                         CAMERA_PARAMETERS[side].up ) };

        this->fbo->Bind(
            this->GenerateRenderTargets(
                { this->texture.pointLightShadowMap.target[side] } ),
            FBO_POINTLIGHT_SHADOW );

        OpenGL::gl( glClear, static_cast< GLuint >( GL_COLOR_BUFFER_BIT |
                                                    GL_DEPTH_BUFFER_BIT ) );

        for ( auto &o :
              this->factory.GetProducts< OpenGL::elGeometricObject_base >() )
        {
            if ( side == 0 )
                o->GetShader( o.Extension().pointLightShadowMapShader )
                    ->SetUniform( o.Extension().pointLightPositionUid,
                                  light->GetPosition() );
            o->Draw( o.Extension().pointLightShadowMapShader, lightCamera );
        }

        this->fbo->Unbind();
    }

    OpenGL::Disable( GL_CULL_FACE );
    OpenGL::Disable( GL_DEPTH_TEST );
}

void
elRenderer_Deferred::RenderPointLight(
    const OpenGL::elCamera_Perspective &camera ) noexcept
{
    uint64_t pointLightsSize = this->pointLights->size();
    int      bufferDiff      = static_cast< int >(
        pointLightsSize -
        this->pointLightShaderSettings.uniformBuffers.size() );
    bool doBufferUpdate = ( bufferDiff > 0 );
    if ( doBufferUpdate ) this->CreatePointLightBufferBlock( bufferDiff );

    for ( uint64_t k = 0; k < pointLightsSize; ++k )
    {
        auto &light = ( *this->pointLights )[k];
        if ( light->HasUpdatedSettings() ) this->UpdatePointLightBuffer( k );

        this->pointLightShaderSettings.uniformBuffers[k].SetVariableData(
            0, pointLightShader_t::CAMERA_POSITION,
            glm::value_ptr( camera.GetState().position ),
            3 * sizeof( GLfloat ) );


        if ( light->DoesCastShadow() )
            this->RenderPointLightShadowMap( light.Get() );

        OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_FALSE ) );

        // stencil pass
        glm::mat4 mvp =
            camera.GetMatrices().viewProjection * light->cube.GetModelMatrix();

        this->passThrough.shader->Bind();
        this->passThrough.shader->SetUniform( this->passThrough.uidVPMatrix,
                                              mvp );
        OpenGL::Enable( GL_STENCIL_TEST );
        GLboolean value = GL_FALSE;
        OpenGL::gl( glColorMask, value, value, value, value );
        this->fbo->Bind( {}, FBO_FINAL_IMAGE );
        light->Bind( this->passThrough.coordAttrib );

        OpenGL::Enable( GL_DEPTH_TEST );
        OpenGL::Disable( GL_CULL_FACE );
        OpenGL::gl( glClear, static_cast< GLenum >( GL_STENCIL_BUFFER_BIT ) );

        OpenGL::gl( glStencilFunc, static_cast< GLenum >( GL_ALWAYS ), 0, 0u );
        GLenum glKeep = GL_KEEP;
        OpenGL::gl( glStencilOpSeparate, static_cast< GLenum >( GL_FRONT ),
                    glKeep, static_cast< GLenum >( GL_INCR_WRAP ), glKeep );
        OpenGL::gl( glStencilOpSeparate, static_cast< GLenum >( GL_BACK ),
                    glKeep, static_cast< GLenum >( GL_DECR_WRAP ), glKeep );

        OpenGL::gl( glDrawElements, static_cast< GLenum >( GL_TRIANGLES ), 36,
                    static_cast< GLenum >( GL_UNSIGNED_INT ),
                    static_cast< GLvoid * >( nullptr ) );
        value = GL_TRUE;
        OpenGL::gl( glColorMask, value, value, value, value );
        this->fbo->Unbind();
        OpenGL::Disable( GL_DEPTH_TEST );

        // light pass
        this->pointLightShaderSettings.shader->Bind();

        this->pointLightShaderSettings.shader->SetUniform(
            this->pointLightShaderSettings.uidVPMatrix, mvp );

        this->fbo
            ->GetTexture( this->texture.position.textureId, FBO_FINAL_IMAGE )
            ->BindActivateAndSetUniform(
                this->pointLightShaderSettings.texPosition, GL_TEXTURE0 );
        this->fbo->GetTexture( this->texture.normal.textureId, FBO_FINAL_IMAGE )
            ->BindActivateAndSetUniform(
                this->pointLightShaderSettings.texNormal, GL_TEXTURE1 );
        this->fbo
            ->GetTexture( this->texture.diffuse.textureId, FBO_FINAL_IMAGE )
            ->BindActivateAndSetUniform(
                this->pointLightShaderSettings.texDiffuse, GL_TEXTURE2 );

        this->fbo
            ->GetTexture( this->texture.pointLightShadowMap.textureId,
                          FBO_POINTLIGHT_SHADOW )
            ->BindActivateAndSetUniform(
                this->pointLightShaderSettings.texShadowMap, GL_TEXTURE3 );

        this->fbo
            ->GetTexture( this->texture.material.textureId, FBO_FINAL_IMAGE )
            ->BindActivateAndSetUniform(
                this->pointLightShaderSettings.texMaterial, GL_TEXTURE4 );


        this->fbo->Bind(
            this->GenerateRenderTargets( { this->texture.final.target } ),
            FBO_FINAL_IMAGE );

        this->pointLightShaderSettings.uniformBuffers[k].BindToShader();

        OpenGL::gl( glStencilFunc, static_cast< GLenum >( GL_NOTEQUAL ), 0,
                    0xFFu );
        OpenGL::Enable( GL_CULL_FACE );
        OpenGL::gl( glCullFace, static_cast< GLenum >( GL_FRONT ) );
        OpenGL::Enable( GL_BLEND );
        OpenGL::gl( glBlendEquation, static_cast< GLenum >( GL_FUNC_ADD ) );
        OpenGL::gl( glBlendFunc, static_cast< GLenum >( GL_ONE ),
                    static_cast< GLenum >( GL_ONE ) );


        OpenGL::gl( glDrawElements, static_cast< GLenum >( GL_TRIANGLES ), 36,
                    static_cast< GLenum >( GL_UNSIGNED_INT ),
                    static_cast< GLvoid * >( nullptr ) );
        light->Unbind( this->pointLightShaderSettings.coordAttrib );

        this->fbo->Unbind();

        OpenGL::Disable( GL_CULL_FACE );
        OpenGL::Disable( GL_STENCIL_TEST );
        OpenGL::Disable( GL_BLEND );
    }
}

void
elRenderer_Deferred::RenderGeometricObjects(
    const OpenGL::elCamera &camera ) noexcept
{
    OpenGL::Disable( GL_BLEND );
    OpenGL::Enable( GL_DEPTH_TEST );
    OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_TRUE ) );
    OpenGL::gl( glDepthFunc, static_cast< GLenum >( GL_LEQUAL ) );
    OpenGL::Enable( GL_CULL_FACE );
    OpenGL::gl( glCullFace, static_cast< GLenum >( GL_FRONT ) );

    this->fbo->Bind(
        this->GenerateRenderTargets(
            { this->texture.diffuse.target, this->texture.position.target,
              this->texture.normal.target, this->texture.material.target,
              this->texture.glow.target, this->texture.specular.target,
              this->texture.event.target } ),
        FBO_FINAL_IMAGE );

    OpenGL::gl( glClearColor, 0.0f, 0.0f, 0.0f, 0.0f );
    OpenGL::gl( glClear, static_cast< GLbitfield >( GL_COLOR_BUFFER_BIT |
                                                    GL_DEPTH_BUFFER_BIT ) );

    for ( auto &o :
          this->factory.GetProducts< OpenGL::elGeometricObject_base >() )
        o->Draw( o.Extension().geometricShader, camera );

    this->fbo->Unbind();
    OpenGL::gl( glDepthMask, static_cast< GLboolean >( GL_FALSE ) );
}

void
elRenderer_Deferred::Render(
    const OpenGL::elCamera_Perspective &camera ) noexcept
{
    this->RenderGeometricObjects( camera );
    this->RenderDirectionalLightShadowMap( camera );
    this->RenderDirectionalLight( camera );
    this->RenderPointLight( camera );
    this->ambientOcclusion->Render( camera );
}

void
elRenderer_Deferred::passThroughShader_t::SetUp(
    std::unique_ptr< OpenGL::elShaderProgram > &&shader_ ) noexcept
{
    this->shader = std::move( shader_ );
    elRenderer_Deferred::AddCoordinateAttribute(
        this->shader.get(), this->coordAttrib, "passthrough" );

    this->uidVPMatrix = elRenderer_Deferred::AddUniform(
        "vp", this->shader.get(), "passthrough" );
}

void
elRenderer_Deferred::pointLightShader_t::SetUp(
    std::unique_ptr< OpenGL::elShaderProgram > &&shader_,
    const glm::uvec2                            &shadowMapSize_ ) noexcept
{
    this->shader        = std::move( shader_ );
    this->shadowMapSize = shadowMapSize_;

    elRenderer_Deferred::AddCoordinateAttribute(
        this->shader.get(), this->coordAttrib, "pointLight" );

    this->uidVPMatrix = elRenderer_Deferred::AddUniform(
        "vp", this->shader.get(), "pointLight" );
    this->texPosition = elRenderer_Deferred::AddUniform(
        "position", this->shader.get(), "pointLight" );
    this->texNormal = elRenderer_Deferred::AddUniform(
        "normal", this->shader.get(), "pointLight" );
    this->texDiffuse = elRenderer_Deferred::AddUniform(
        "diffuse", this->shader.get(), "pointLight" );
    this->texShadowMap = elRenderer_Deferred::AddUniform(
        "shadowMap", this->shader.get(), "pointLight" );
    this->texMaterial = elRenderer_Deferred::AddUniform(
        "material", this->shader.get(), "pointLight" );
    this->texSpecular = elRenderer_Deferred::AddUniform(
        "specular", this->shader.get(), "pointLight" );
}

void
elRenderer_Deferred::directionalLightShader_t::SetUp(
    std::unique_ptr< OpenGL::elShaderProgram > &&shader_ ) noexcept
{
    this->shader = std::move( shader_ );

    auto buffer = OpenGL::elUniformBufferObject::Create(
        "light_t",
        std::vector< std::string >{
            "light_t.color", "light_t.direction", "light_t.ambientIntensity",
            "light_t.diffuseIntensity", "light_t.biasedViewProjection",
            "light_t.numberOfLights", "light_t.resolution",
            "light_t.shadowBorderSize", "light_t.hasShadow",
            "light_t.cameraPosition", "light_t.hasFog",
            "light_t.fogIntensity" },
        this->shader.get() );

    if ( buffer.HasError() )
    {
        LOG_FATAL( 0 )
            << "unable to find required uniform buffer \"light_t\" in "
               "directional light shader";
        std::terminate();
    }

    this->uniformBuffer = std::move( buffer.GetValue() );

    elRenderer_Deferred::AddCoordinateAttribute(
        this->shader.get(), this->coordAttrib, "directionalLight" );

    this->texNormal = elRenderer_Deferred::AddUniform(
        "normal", this->shader.get(), "directionalLight" );
    this->texDiffuse = elRenderer_Deferred::AddUniform(
        "diffuse", this->shader.get(), "directionalLight" );
    this->texPosition = elRenderer_Deferred::AddUniform(
        "position", this->shader.get(), "directionalLight" );
    this->texShadowMap = elRenderer_Deferred::AddUniform(
        "shadowMap", this->shader.get(), "directionalLight" );
    this->texMaterial = elRenderer_Deferred::AddUniform(
        "material", this->shader.get(), "directionalLight" );
    this->texSpecular = elRenderer_Deferred::AddUniform(
        "specular", this->shader.get(), "directionalLight" );
}

void
elRenderer_Deferred::AddCoordinateAttribute(
    const OpenGL::elShaderProgram *const        shader,
    OpenGL::elShaderProgram::shaderAttribute_t &attribute,
    const std::string                          &shaderName ) noexcept
{
    if ( shader->GetAttribute( "coord", 3 )
             .AndThen( [&]( const auto &r ) { attribute = r; } )
             .HasError() )
    {
        LOG_FATAL( 0 ) << "unable to find required attribute \"coord\" in "
                       << shaderName << " shader";
        std::terminate();
    }
}

GLint
elRenderer_Deferred::AddUniform( const std::string &uniformName,
                                 const OpenGL::elShaderProgram *const shader,
                                 const std::string &shaderName ) noexcept
{
    GLint uniformID;
    shader->GetUniformLocation( uniformName )
        .AndThen( [&]( const auto &r ) { uniformID = r; } )
        .OrElse(
            [&]
            {
                LOG_FATAL( 0 )
                    << "unable to find uniform location for \"" << uniformName
                    << "\" in " << shaderName << " shader";
                std::terminate();
            } );
    return uniformID;
}

OpenGL::elTexture_2D *
elRenderer_Deferred::GetNormalTexture() noexcept
{
    return dynamic_cast< OpenGL::elTexture_2D * >(
        this->fboDetails[FBO_FINAL_IMAGE]
            .outputTextures[this->texture.normal.textureId] );
}

OpenGL::elTexture_2D *
elRenderer_Deferred::GetPositionTexture() noexcept
{
    return dynamic_cast< OpenGL::elTexture_2D * >(
        this->fboDetails[FBO_FINAL_IMAGE]
            .outputTextures[this->texture.position.textureId] );
}

OpenGL::elTexture_2D *
elRenderer_Deferred::GetDepthTexture() noexcept
{
    return dynamic_cast< OpenGL::elTexture_2D * >(
        this->fboDetails[FBO_FINAL_IMAGE]
            .outputTextures[this->texture.depth.textureId] );
}

OpenGL::elTexture_2D *
elRenderer_Deferred::GetOutput() noexcept
{
    return this->ambientOcclusion->GetOutput();
}

Deferred::objectSettings_t
elRenderer_Deferred::GenerateObjectSettings(
    OpenGL::elGeometricObject_base &object, const uint64_t geometricShaderId,
    const uint64_t directionalLightShadowShaderId,
    const uint64_t pointLightShadowShaderId ) const noexcept
{
    const geometricObjectShaderDefaults_t *defaults{ nullptr };
    switch ( object.GetType() )
    {
        case OpenGL::elGeometricObject_Type::UnifiedVertexObject:
            defaults = &this->settings.unifiedVertexObject;
            break;
        case OpenGL::elGeometricObject_Type::InstancedObject:
            defaults = &this->settings.instancedObject;
            break;
        case OpenGL::elGeometricObject_Type::VertexObject:
            defaults = &this->settings.vertexObject;
            break;
        default:
            LOG_FATAL( 0 ) << "geometry shader does not fit: incompatible "
                              "object added to deferred renderer";
            std::terminate();
            break;
    }

    Deferred::objectSettings_t objectSettings;

    objectSettings.geometricShader =
        ( geometricShaderId != INVALID_ID )
            ? geometricShaderId
            : object.AddShader( defaults->geometryShader.get() )
                  .OrElse(
                      []
                      {
                          LOG_FATAL( 0 )
                              << "geometry shader does not fit: incompatible "
                                 "object added to deferred renderer";
                          std::terminate();
                      } )
                  .GetValue();

    objectSettings.directionalLightShadowMapShader =
        ( directionalLightShadowShaderId != INVALID_ID )
            ? directionalLightShadowShaderId
            : object.AddShader( defaults->directionalLightShadowShader.get() )
                  .OrElse(
                      []
                      {
                          LOG_FATAL( 0 ) << "directional light shadow shader "
                                            "does not fit: "
                                            "incompatible object added to "
                                            "deferred renderer";
                          std::terminate();
                      } )
                  .GetValue();

    objectSettings.pointLightShadowMapShader =
        ( pointLightShadowShaderId != INVALID_ID )
            ? pointLightShadowShaderId
            : object.AddShader( defaults->pointLightShadowShader.get() )
                  .OrElse(
                      []
                      {
                          LOG_FATAL( 0 )
                              << "point light shadow shader does not "
                                 "fit: incompatible "
                                 "object added to deferred renderer";
                          std::terminate();
                      } )
                  .GetValue();

    objectSettings.pointLightPositionUid = elRenderer_Deferred::AddUniform(
        "lightPosition",
        object.GetShader( objectSettings.pointLightShadowMapShader ),
        "pointLightShadow" );

    return objectSettings;
}

void
elRenderer_Deferred::geometricObjectShaderDefaults_t::SetUp(
    const ObjectType objectType, elRenderer_Deferred *const self ) noexcept
{
    std::string objectName =
        OBJECT_TYPE_NAME[static_cast< uint64_t >( objectType )];

    if ( objectType == ObjectType::UnifiedVertexObject )
    {
        OpenGL::elGLSLParser::staticDefines_t defines;
        defines[OpenGL::elGLSLParser::groupType_t{ "geometry",
                                                   GL_FRAGMENT_SHADER }]
            .emplace_back( OpenGL::elGLSLParser::define_t{
                "INVALID_TEXTURE_ID",
                std::to_string( HighGL::elGeometricObject_UnifiedVertexObject::
                                    INVALID_TEXTURE_ID ) } );
        this->geometryShader = self->CreateShaderProgram(
            "deferred", objectName, "geometry", defines );
    }
    else
    {
        this->geometryShader =
            self->CreateShaderProgram( "deferred", objectName, "geometry" );
    }

    this->directionalLightShadowShader = self->CreateShaderProgram(
        "deferred", objectName, "directionalLightShadow" );

    this->pointLightShadowShader =
        self->CreateShaderProgram( "deferred", objectName, "pointLightShadow" );
}

void
elRenderer_Deferred::SetLengthOfOneMeter( const float v ) noexcept
{
    if ( !this->ambientOcclusion ) return;
    this->ambientOcclusion->SetLengthOfOneMeter( v );
}


} // namespace RenderPipeline
} // namespace el3D
