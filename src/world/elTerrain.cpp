#include "world/elTerrain.hpp"

#include "math/elPerlinNoise.hpp"

namespace el3D
{
namespace world
{
elTerrain::elTerrain( const objectSettings_t settings,
                      terrainProperties_t    properties ) noexcept
    : elObject( settings )
{
    math::elPerlinNoise perlinNoise( properties.seed );
    glm::vec2 f = glm::vec2{ static_cast< float >( properties.dimension.x ),
                             static_cast< float >( properties.dimension.y ) } /
                  properties.frequency;

    std::vector< uint8_t > data;
    data.reserve( properties.dimension.x * properties.dimension.y );

    for ( float x = 0.0f; x < static_cast< float >( properties.dimension.x );
          ++x )
        for ( float y = 0.0f;
              y < static_cast< float >( properties.dimension.y ); ++y )
        {
            uint8_t noise = static_cast< uint8_t >(
                255 * ( math::MapTo( perlinNoise.AccumulatedOctaveNoise(
                                         properties.octaves, x / f.x, y / f.y ),
                                     { -1.0, 1.0 }, { 0.0, 1.0 } ) ) );
            data.emplace_back( noise );
            data.emplace_back( noise );
            data.emplace_back( noise );
            data.emplace_back( 0xff );
        }

    this->generator.emplace(
        HighGL::elTerrainGenerator::heightmap_t{
            4, properties.dimension.x, properties.dimension.y, data.data() },
        HighGL::elTerrainGenerator::settings_t{ properties.widthScaling,
                                                properties.heightScaling,
                                                properties.normalGridRange } );

    auto& geometry = this->generator->GetGeometry();

    this->object = this->GetUvo().CreateObject(
        geometry.vertices, geometry.textureCoordinates, geometry.normals,
        geometry.tangents, geometry.bitangents, geometry.elements );

    auto image =
        GLAPI::elImage::Create( GLAPI::FromArray, properties.dimension.x,
                                properties.dimension.y, data.data() );
    if ( image.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to setup texture for terrain from heightmap";
        return;
    }

    auto uvoTexSize = this->GetUvo().GetTextureArraySize();
    image->get()->Resize( uvoTexSize.x, uvoTexSize.y );

    auto texture =
        this->object->CreateTexture( "", image->get()->GetBytePointer() );
    this->object->SetUintBuffer(
        HighGL::UnifiedVertexObject::TEX_DIFFUSE,
        HighGL::UnifiedVertexObject::uintBuffer_t(
            this->object->GetNumberOfVertices(), texture->GetId() ) );
}

std::vector< GLfloat >
elTerrain::GetVertices() const noexcept
{
    return this->generator->GetGeometry().vertices;
}

std::vector< GLuint >
elTerrain::GetElements() const noexcept
{
    return this->generator->GetGeometry().elements;
}

} // namespace world
} // namespace el3D
