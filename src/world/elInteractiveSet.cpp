#include "world/elInteractiveSet.hpp"

#include "3Delch/3Delch.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "world/elWorld.hpp"

namespace el3D
{
namespace world
{
elInteractiveSet::elInteractiveSet(
    const objectSettings_t            settings,
    const interactiveSetProperties_t& properties ) noexcept
    : elObject( settings )
{
    this->hoverObjectProperties.diffuseColor = properties.hoverDiffuseColor;
    this->hoverObjectProperties.glowColor    = properties.hoverGlowColor;
    this->hoverObjectProperties.effectType   = properties.hoverEffectType;

    this->selectObjectProperties.diffuseColor = properties.selectDiffuseColor;
    this->selectObjectProperties.glowColor    = properties.selectGlowColor;
    this->selectObjectProperties.effectType   = properties.selectEffectType;

    this->selectBoxProperties.borderLineColor = properties.selectBoxLineColor;
    this->selectBoxProperties.cornerLineColor = properties.selectBoxCornerColor;

    this->eventColorCallback = el3D::_3Delch::CreateMainLoopCallback(
        [this] { this->MainLoopCallback(); } );
    this->event = el3D::_3Delch::CreateEvent();
    this->event->Register();
    this->event->SetCallback( [this]( const SDL_Event e )
                              { this->EventCallback( e ); } );
}

void
elInteractiveSet::MainLoopCallback() noexcept
{
    uint32_t currentColorIndex = this->GetScene().GetCurrentEventColorIndex();
    if ( currentColorIndex == this->previousColorIndex ) return;
    this->previousColorIndex = currentColorIndex;

    this->isMouseHoveringInteractiveMesh = false;
    if ( this->interactiveMesh )
    {
        this->interactiveMesh->MainLoopCallback();
        this->isMouseHoveringInteractiveMesh =
            this->interactiveMesh->HasMouseHover();
        if ( this->isMouseHoveringInteractiveMesh ) this->ResetModelHover();
    }

    if ( !this->isMouseHoveringInteractiveMesh ) this->Color2ModelCallback();
}

void
elInteractiveSet::ResetModelHover() noexcept
{
    this->hoverColorIndex = INVALID_COLOR;
    this->hoverObject.Reset();
}

void
elInteractiveSet::ResetModelSelection() noexcept
{
    if ( this->selectColorIndex == INVALID_COLOR ) return;

    auto& variantObject = this->color2Object[this->selectColorIndex];

    auto deselect = [&]( auto& object )
    {
        if ( object.callbacks.deselect )
            object.callbacks.deselect( object.object );
    };
    switch ( variantObject.index() )
    {
        case MODEL:
        {
            deselect( std::get< MODEL >( variantObject ) );
            break;
        }
        case MODEL_GROUP:
        {
            deselect( std::get< MODEL_GROUP >( variantObject ) );
            break;
        }
    }

    this->selectColorIndex = INVALID_COLOR;
    this->selectObject.Reset();
    this->selectBoxObject.Reset();
}

void
elInteractiveSet::ActivateModelHover( const uint32_t colorIndex ) noexcept
{
    bool isObjectSelected = ( colorIndex == this->selectColorIndex );
    if ( isObjectSelected ) return;

    auto objectVariant = this->color2Object.find( colorIndex );
    if ( objectVariant == this->color2Object.end() )
    {
        this->ResetModelHover();
        return;
    }

    this->hoverColorIndex = colorIndex;
    switch ( objectVariant->second.index() )
    {
        case MODEL:
            this->hoverObject = this->GetWorld().Create< elModelEffect >(
                *std::get< MODEL >( objectVariant->second ).object,
                this->hoverObjectProperties );
            break;
        case MODEL_GROUP:
            this->hoverObject = this->GetWorld().Create< elModelEffect >(
                *std::get< MODEL_GROUP >( objectVariant->second ).object,
                this->hoverObjectProperties );
            break;
    }
}

void
elInteractiveSet::ActivateModelSelection( const uint32_t colorIndex ) noexcept
{
    auto variantObject = this->color2Object.find( colorIndex );
    if ( variantObject == this->color2Object.end() )
    {
        return;
    }

    this->selectColorIndex = colorIndex;
    this->ResetModelHover();

    auto select = [&]( auto& object )
    {
        this->selectBoxObject = this->GetWorld().Create< elBoundingBox >(
            *object.object, this->selectBoxProperties );
        this->selectObject = this->GetWorld().Create< elModelEffect >(
            *object.object, this->selectObjectProperties );

        if ( object.callbacks.select ) object.callbacks.select( object.object );
    };
    switch ( variantObject->second.index() )
    {
        case MODEL:
        {
            select( std::get< MODEL >( variantObject->second ) );
            break;
        }
        case MODEL_GROUP:
        {
            select( std::get< MODEL_GROUP >( variantObject->second ) );
            break;
        }
    }
}

void
elInteractiveSet::Color2ModelCallback() noexcept
{
    this->ActivateModelHover( this->GetScene().GetCurrentEventColorIndex() );
}

void
elInteractiveSet::EventCallback( const SDL_Event e ) noexcept
{
    switch ( e.button.button )
    {
        case SDL_BUTTON_LEFT:
            if ( e.button.type == SDL_MOUSEBUTTONUP )
            {
                if ( this->isMouseHoveringInteractiveMesh &&
                     this->interactiveMesh &&
                     this->interactiveMeshCallbacks.click )
                {
                    this->interactiveMeshCallbacks.click(
                        this->interactiveMesh->GetInteractiveCoordinates() );
                    return;
                }

                if ( this->hoverColorIndex == INVALID_COLOR ) return;

                this->ResetModelSelection();
                this->ActivateModelSelection( this->hoverColorIndex );
            }
            break;
        case SDL_BUTTON_MIDDLE:
            break;
        case SDL_BUTTON_RIGHT:
            if ( e.button.type == SDL_MOUSEBUTTONUP )
            {
                this->ResetModelSelection();
            }
            break;
    }
}

void
elInteractiveSet::Attach(
    elModel&                                 model,
    const interactiveCallbacks_t< elModel >& callbacks ) noexcept
{
    auto iter = bb::find_if( this->models,
                             [&]( auto& e ) { return e.object == &model; } );
    if ( iter != this->models.end() ) return;

    EventEntry_t< elModel > newEntry;
    newEntry.object     = &model;
    newEntry.eventColor = this->GetScene().AcquireEventColor();

    this->color2Object[newEntry.eventColor->GetIndex()] =
        ObjectDetails_t< elModel >{ &model, callbacks };

    model.SetBuffer( newEntry.eventColor->GetRGBAColor(),
                     el3D::HighGL::UnifiedVertexObject::EVENT_COLOR );

    this->models.emplace_back( std::move( newEntry ) );
}

void
elInteractiveSet::Attach(
    elModelGroup&                                 modelGroup,
    const interactiveCallbacks_t< elModelGroup >& callbacks ) noexcept
{
    auto iter = bb::find_if( this->modelGroups, [&]( auto& e )
                             { return e.object == &modelGroup; } );
    if ( iter != this->modelGroups.end() ) return;

    EventEntry_t< elModelGroup > newEntry;
    newEntry.object     = &modelGroup;
    newEntry.eventColor = this->GetScene().AcquireEventColor();

    this->color2Object[newEntry.eventColor->GetIndex()] =
        ObjectDetails_t< elModelGroup >{ &modelGroup, callbacks };

    for ( auto& model : modelGroup.models )
        model->SetBuffer( newEntry.eventColor->GetRGBAColor(),
                          el3D::HighGL::UnifiedVertexObject::EVENT_COLOR );

    this->modelGroups.emplace_back( std::move( newEntry ) );
}

void
elInteractiveSet::AttachMembers(
    elModelGroup&                            modelGroup,
    const interactiveCallbacks_t< elModel >& callbacks ) noexcept
{
    for ( auto& model : modelGroup.models )
        this->Attach( *model, callbacks );
}

template < typename T >
void
elInteractiveSet::RemoveFromColor2Object( const T& model ) noexcept
{
    for ( auto iter = this->color2Object.begin();
          iter != this->color2Object.end(); ++iter )
    {
        auto* ptr = std::get_if< ObjectDetails_t< T > >( &iter->second );
        if ( ptr != nullptr && ptr->object == &model )
        {
            this->color2Object.erase( iter );
            break;
        }
    }
}

void
elInteractiveSet::Detach( elModel& model ) noexcept
{
    auto iter = bb::find_if( this->models,
                             [&]( auto& e ) { return e.object == &model; } );
    if ( iter == this->models.end() ) return;

    model.SetBuffer( { 0x00, 0x00, 0x00, 0x00 },
                     el3D::HighGL::UnifiedVertexObject::EVENT_COLOR );
    this->RemoveFromColor2Object( model );
    this->models.erase( iter );
}

void
elInteractiveSet::Detach( elModelGroup& modelGroup ) noexcept
{
    auto iter = bb::find_if( this->modelGroups, [&]( auto& e )
                             { return e.object == &modelGroup; } );
    if ( iter == this->modelGroups.end() ) return;

    for ( auto& m : modelGroup.models )
        m->SetBuffer( { 0x00, 0x00, 0x00, 0x00 },
                      el3D::HighGL::UnifiedVertexObject::EVENT_COLOR );
    this->RemoveFromColor2Object( modelGroup );
    this->modelGroups.erase( iter );
}

void
elInteractiveSet::DetachMembers( elModelGroup& modelGroup ) noexcept
{
    for ( auto& model : modelGroup.models )
        this->Detach( *model );
}

elInteractiveMesh&
elInteractiveSet::GetInteractiveMesh() noexcept
{
    return *this->interactiveMesh.Get();
}

const elInteractiveMesh&
elInteractiveSet::GetInteractiveMesh() const noexcept
{
    return *this->interactiveMesh.Get();
}


} // namespace world
} // namespace el3D
