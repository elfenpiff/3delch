#include "world/elInteractiveMesh.hpp"

#include "3Delch/3Delch.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"

namespace el3D
{
namespace world
{
elInteractiveMesh::elInteractiveMesh(
    const objectSettings_t settings, const std::vector< GLfloat >& vertices,
    const std::vector< GLuint >&       elements,
    const interactiveMeshProperties_t& properties ) noexcept
    : elObject( settings ), properties( properties )
{
    this->Create( vertices, elements );
    this->SetStickToCamera( properties.doesStickToCamera );
}

elInteractiveMesh::elInteractiveMesh(
    const objectSettings_t settings, const float gridSize,
    const uint64_t                     numberOfGridElements,
    const interactiveMeshProperties_t& properties ) noexcept
    : elObject( settings ), properties( properties ),
      gridDiameter( 2.0f * gridSize /
                    static_cast< float >( numberOfGridElements ) )
{
    auto plane = HighGL::elObjectGeometryBuilder::CreatePlane(
                     gridSize * 2.0f, gridSize * 2.0f, numberOfGridElements + 1,
                     numberOfGridElements + 1 )
                     .GetObjectGeometry();
    this->Create( plane.vertices, plane.elements );
    this->SetStickToCamera( properties.doesStickToCamera );
}

void
elInteractiveMesh::Create( const std::vector< GLfloat >& vertices,
                           const std::vector< GLuint >&  elements ) noexcept
{
    uint64_t verticesSize = vertices.size();
    bb::Expects( verticesSize % 3 == 0 && verticesSize > 0 );

    std::vector< GLfloat > textureCoordinates, normals, tangents, bitangents;

    std::vector< GLfloat > noSharedVertices;
    std::vector< GLuint >  adjustedElements;

    for ( uint64_t i = 0; i < elements.size(); ++i )
    {
        noSharedVertices.emplace_back( vertices[3 * elements[i]] );
        noSharedVertices.emplace_back( vertices[3 * elements[i] + 1] );
        noSharedVertices.emplace_back( vertices[3 * elements[i] + 2] );

        adjustedElements.emplace_back( i );
    }

    this->object =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_VertexObject >(
            nullptr, RenderPipeline::RenderOrder::TransparentObject,
            OpenGL::DrawMode::Triangles,
            OpenGL::objectGeometry_t{ .vertices = noSharedVertices,
                                      .elements = adjustedElements } );

    this->objectColor = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS, "colorTexture",
        GL_TEXTURE0, "", 1U, 1U, static_cast< GLint >( GL_RGBA8 ),
        static_cast< GLenum >( GL_RGBA ), static_cast< GLenum >( GL_FLOAT ) );
    this->objectColor->TexImage(
        std::vector< GLfloat >{ 0.0f, 0.0f, 0.0f, 0.0f } );

    this->objectNormal = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
        "normalTexture", GL_TEXTURE1, "" );
    this->objectNormal->TexImage< utils::byte_t >( { 0x00, 0x00, 0x00, 0x00 } );

    this->objectSpecular = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
        "specularColorTexture", GL_TEXTURE2, "" );
    this->objectSpecular->TexImage< utils::byte_t >(
        { 0x00, 0x00, 0x00, 0x00 } );

    this->objectRoughness = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
        "roughnessTexture", GL_TEXTURE3, "" );
    this->objectRoughness->TexImage< utils::byte_t >(
        { 0x00, 0x00, 0x00, 0x00 } );

    this->objectEmission = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS,
        "emissionTexture", GL_TEXTURE4, "" );
    this->objectEmission->TexImage< utils::byte_t >(
        { 0x00, 0x00, 0x00, 0x00 } );


    this->numberOfVertices = elements.size();
    std::vector< GLfloat > eventColorVector( this->numberOfVertices * 4 );
    this->eventColorRange = this->GetScene().AcquireEventColorRange(
        static_cast< uint32_t >( this->numberOfVertices / 6 ) );

    for ( uint64_t i = 0, eventIndex = 0; i < this->numberOfVertices;
          i += 6, ++eventIndex )
    {
        auto color = this->eventColorRange->GetRGBAColor( eventIndex );

        glm::vec3 coordinates{ 0.0 };
        for ( uint64_t k = 0; k < 6; ++k )
        {
            eventColorVector[4 * i + 4 * k]     = color.x;
            eventColorVector[4 * i + 4 * k + 1] = color.y;
            eventColorVector[4 * i + 4 * k + 2] = color.z;
            eventColorVector[4 * i + 4 * k + 3] = 1.0f;

            coordinates += glm::dvec3( noSharedVertices[3 * i + 3 * k],
                                       noSharedVertices[3 * i + 3 * k + 1],
                                       noSharedVertices[3 * i + 3 * k + 2] ) /
                           6.0;
        }

        this->color2section[this->eventColorRange->GetColorIndex(
            eventIndex )] = section_t{ coordinates, i };
    }

    this->object->UpdateBuffer(
        OpenGL::elGeometricObject_VertexObject::BUFFER_EVENT_COLOR,
        eventColorVector );
}

void
elInteractiveMesh::MainLoopCallback() noexcept
{
    this->EventColorCallback();
    if ( this->doesStickToCamera ) this->RepositionToCamera();
}

void
elInteractiveMesh::RepositionToCamera() noexcept
{
    auto cameraPosition = this->GetScene().GetCamera().GetState().position;
    cameraPosition.x    = this->gridDiameter *
                       std::round( cameraPosition.x / this->gridDiameter );
    cameraPosition.y = this->GetPosition().y;
    cameraPosition.z = this->gridDiameter *
                       std::round( cameraPosition.z / this->gridDiameter );

    this->SetPosition( cameraPosition );
}

void
elInteractiveMesh::EventColorCallback() noexcept
{
    auto currentColorIndex = this->GetScene().GetCurrentEventColorIndex();
    auto iter              = this->color2section.find( currentColorIndex );

    auto setupColor =
        [&]( const color4_t&                                  color,
             OpenGL::elGeometricObject_VertexObject::buffer_t bufferType )
    {
        std::vector< GLfloat > colorBuffer( 24 );
        for ( uint64_t i = 0; i < 6; ++i )
            for ( uint64_t n = 0; n < 4; ++n )
                colorBuffer[4 * i + n] = color[n] / 255.0f;

        this->object->UpdateSubBuffer( bufferType, colorBuffer,
                                       this->activeElementOffset );
    };

    auto revertToPreviousState = [&]
    {
        if ( this->activeElementOffset != INVALID_OFFSET )
        {
            constexpr color4_t ZEROS{ 0x00, 0x00, 0x00, 0x00 };
            setupColor(
                ZEROS, OpenGL::elGeometricObject_VertexObject::BUFFER_DIFFUSE );
            setupColor(
                ZEROS,
                OpenGL::elGeometricObject_VertexObject::BUFFER_EMISSION_COLOR );
        }
    };

    if ( iter == this->color2section.end() )
    {
        revertToPreviousState();
        this->activeElementOffset = INVALID_OFFSET;
        this->activeColorIndex    = INVALID_COLOR;
        return;
    }
    else if ( currentColorIndex == this->activeColorIndex )
        return;

    revertToPreviousState();

    this->activeColorIndex    = currentColorIndex;
    this->activeElementOffset = iter->second.elementStart * 4;

    setupColor( this->properties.hoverDiffuseColor,
                OpenGL::elGeometricObject_VertexObject::BUFFER_DIFFUSE );
    setupColor( this->properties.hoverGlowColor,
                OpenGL::elGeometricObject_VertexObject::BUFFER_EMISSION_COLOR );
}

bool
elInteractiveMesh::HasMouseHover() const noexcept
{
    return this->activeElementOffset != INVALID_OFFSET;
}

void
elInteractiveMesh::SetStickToCamera( const bool value ) noexcept
{
    this->doesStickToCamera = value;
}

glm::vec3
elInteractiveMesh::GetInteractiveCoordinates() const noexcept
{
    if ( this->activeColorIndex != INVALID_COLOR )
    {
        glm::vec3 coordinates =
            this->color2section.at( this->activeColorIndex ).coordinates +
            this->GetPosition();

        for ( int i = 0; i < 3; ++i )
            coordinates[i] =
                this->gridDiameter * coordinates[i] / this->gridDiameter;

        return coordinates;
    }
    return glm::vec3( 0.0 );
}

void
elInteractiveMesh::SetPosition( const glm::vec3& position ) noexcept
{
    this->object->SetPosition( position );
}

void
elInteractiveMesh::SetScaling( const glm::vec3& scaling ) noexcept
{
    this->object->SetScaling( scaling );
}

void
elInteractiveMesh::SetRotation( const glm::vec4& rotation ) noexcept
{
    this->object->SetRotation( rotation );
}

void
elInteractiveMesh::SetModelMatrix( const glm::mat4& matrix ) noexcept
{
    this->object->SetModelMatrix( matrix );
}

glm::vec3
elInteractiveMesh::GetPosition() const noexcept
{
    return this->object->GetPosition();
}

glm::vec3
elInteractiveMesh::GetScaling() const noexcept
{
    return this->object->GetScaling();
}

glm::vec3
elInteractiveMesh::GetRotationAxis() const noexcept
{
    return this->object->GetRotationAxis();
}

float
elInteractiveMesh::GetRotationDegree() const noexcept
{
    return this->object->GetRotationDegree();
}


} // namespace world
} // namespace el3D
