#include "world/elSky.hpp"

#include "3Delch/3Delch.hpp"
#include "utils/convert.hpp"

namespace el3D
{
namespace world
{
elSky::elSky( const objectSettings_t settings, const internal::ColoredSkybox_t,
              const glm::vec4&       color ) noexcept
    : elObject( settings )
{
    this->SetupSkybox( color );
}

elSky::elSky( const objectSettings_t settings, const internal::TexturedSkybox_t,
              const HighGL::elSkybox::textureConfig_t& textureConfig ) noexcept
    : elObject( settings )
{
    this->SetupSkybox( textureConfig );
}

elSky::elSky( const objectSettings_t settings,
              const internal::ColoredSkyboxFromConfig_t ) noexcept
    : elObject( settings )
{
    auto& config      = _3Delch::GetConfig();
    auto  configColor = config.Get< float >( { "global", "scenes",
                                              this->GetScene().GetSceneName(),
                                              "skybox", "color" },
                                            { 0.0f, 0.0f, 0.0f, 0.0f } );

    if ( configColor.size() != 4 )
    {
        LOG_ERROR( 0 )
            << "global.scenes." << this->GetScene().GetSceneName()
            << ".skybox.color should contain 4 float but contains "
            << configColor.size()
            << ". Resizing it to 4 and set all added values to zero.";
        configColor.resize( 4, 0.0f );
    }

    this->SetupSkybox( utils::ToGLM< glm::vec4 >( configColor ) );
}

elSky::elSky( const objectSettings_t settings,
              const internal::TexturedSkyboxFromConfig_t ) noexcept
    : elObject( settings )
{
    auto& config        = _3Delch::GetConfig();
    auto  configTexture = config.Get< std::string >(
        { "global", "scenes", this->GetScene().GetSceneName(), "skybox",
          "textures" } );

    if ( configTexture.size() != 6 )
    {
        LOG_ERROR( 0 )
            << "global.scenes." << this->GetScene().GetSceneName()
            << ".skybox.texture should contain 6 strings but contains "
            << configTexture.size()
            << ". Resizing it to 6 and set all added values to empty.";
        configTexture.resize( 6, "" );
    }

    this->SetupSkybox( HighGL::elSkybox::textureConfig_t{
        configTexture[0], configTexture[1], configTexture[2], configTexture[3],
        configTexture[4], configTexture[5] } );
}

template < typename T >
void
elSky::SetupSkybox( const T& setting ) noexcept
{
    auto& config = _3Delch::GetConfig();

    std::string shaderFile = config.Get< std::string >(
        { "global", "scenes", this->GetScene().GetSceneName(), "skybox",
          "shader", "file" },
        { "glsl/forwardRenderer_Skybox.glsl" } )[0];

    std::string shaderGroup = config.Get< std::string >(
        { "global", "scenes", this->GetScene().GetSceneName(), "skybox",
          "shader", "group" },
        { "default" } )[0];

    auto shaderVersion = _3Delch::GetInstance().Window()->GetShaderVersion();

    auto result = this->CreateSkybox(
        [this] { return this->GetScene().GetCamera().GetState().position; },
        shaderFile, shaderVersion, shaderGroup, setting );

    if ( result.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to create skybox";
        std::terminate();
    }
    else
        this->skybox = std::move( result.GetValue() );
}

void
elSky::SetColor( const glm::vec4& color ) noexcept
{
    if ( this->skybox ) this->skybox->SetColor( color );
}

void
elSky::UseForReflection( const bool v ) noexcept
{
    this->GetScene().UseSkyboxForReflections( v );
}


} // namespace world
} // namespace el3D
