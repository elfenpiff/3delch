#include "world/elPhysics.hpp"

#include <glm/gtc/type_ptr.hpp>

namespace el3D
{
namespace world
{
elPhysics::elPhysics( const glm::vec3& gravity ) noexcept
{
    this->dynamicWorld.setGravity( ToBulletVector( gravity ) );
}

btVector3
ToBulletVector( const glm::vec3& v ) noexcept
{
    return btVector3( v.x, v.y, v.z );
}

btDiscreteDynamicsWorld&
elPhysics::GetDynamicsWorld() noexcept
{
    return this->dynamicWorld;
}


void
elPhysics::Update( const units::Time deltaTime ) noexcept
{
    constexpr int MAX_SUB_STEPS = 10;
    this->dynamicWorld.stepSimulation(
        static_cast< float >( deltaTime.GetSeconds() ), MAX_SUB_STEPS );

    auto& objects = this->factory.GetProducts< Object >();

    for ( int i = this->dynamicWorld.getNumCollisionObjects() - 1; i >= 0; --i )
    {
        auto& object = objects[static_cast< uint64_t >( i )];

        auto        shape = this->dynamicWorld.getCollisionObjectArray()[i];
        auto        body  = btRigidBody::upcast( shape );
        btTransform transform;

        if ( object->getCustomTransform )
        {
            transform = object->getCustomTransform();
        }
        else
        {
            if ( body && body->getMotionState() )
                body->getMotionState()->getWorldTransform( transform );
            else
                transform = shape->getWorldTransform();
        }

        object->geometry.position = glm::vec3{ transform.getOrigin().getX(),
                                               transform.getOrigin().getY(),
                                               transform.getOrigin().getZ() };

        object->geometry.rotation = glm::vec4{
            transform.getRotation().getX(), transform.getRotation().getY(),
            transform.getRotation().getZ(), transform.getRotation().getW() };

        btScalar modelMatrix[16];
        transform.getOpenGLMatrix( modelMatrix );
        glm::mat4 m = glm::make_mat4( modelMatrix );

        if ( object->setModelMatrix )
            object->setModelMatrix(
                m * glm::scale( glm::mat4( 1.0f ), object->geometry.scaling ) );
    }

    auto& vehicles = this->factory.GetProducts< internal::vehicle_t >();
    for ( auto& vehicle : vehicles )
        if ( vehicle.Extension() ) vehicle.Extension()();
}

bb::product_ptr< internal::vehicle_t >
elPhysics::CreateVehicle(
    elPhysics::Object&             object,
    const std::function< void() >& vehicleTransformUpdater ) noexcept
{
    auto vehicle =
        this->factory.CreateProductWithOnRemoveCallback< internal::vehicle_t >(
            [this]( auto v )
            { this->dynamicWorld.removeVehicle( &v->vehicle ); },
            this->dynamicWorld, *object.body );
    vehicle.Extension() = vehicleTransformUpdater;

    this->dynamicWorld.addVehicle( &vehicle->vehicle );

    return std::move( vehicle );
}

elPhysics::Object::Object(
    elPhysics& physics, btDiscreteDynamicsWorld& dynamicWorld,
    const std::function< btCollisionShape*() >& shapeCreator,
    const physicalProperties_t&                 physicalProperties,
    const geometricProperties_t&                geometry,
    const setModelMatrix_t&                     setModelMatrix ) noexcept
    : physics{ &physics }, dynamicWorld{ &dynamicWorld },
      shapeCreator{ shapeCreator }, shape{ shapeCreator() },
      setModelMatrix{ setModelMatrix },
      physicalProperties{ physicalProperties }, geometry{ geometry }
{
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin( ToBulletVector( geometry.position ) );

    btScalar  mass( physicalProperties.mass );
    btVector3 localInertia( 0, 0, 0 );
    if ( mass != 0.0f )
        this->shape->calculateLocalInertia( mass, localInertia );

    this->motionState = std::make_unique< btDefaultMotionState >( transform );
    this->body        = std::make_unique< btRigidBody >(
        btRigidBody::btRigidBodyConstructionInfo{
            mass, this->motionState.get(), this->shape.get(), localInertia } );

    this->dynamicWorld->addRigidBody( this->body.get() );
}

elPhysics::Object::~Object() noexcept
{
    this->dynamicWorld->removeRigidBody( this->body.get() );
}

void
elPhysics::Object::Reset() noexcept
{
    this->body->setLinearVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );
    this->body->setAngularVelocity( btVector3( 0.0f, 0.0f, 0.0f ) );

    this->dynamicWorld->getBroadphase()
        ->getOverlappingPairCache()
        ->cleanProxyFromPairs( this->body->getBroadphaseHandle(),
                               this->dynamicWorld->getDispatcher() );
}

void
elPhysics::Object::DisableDeactivation() noexcept
{
    this->body->setActivationState( DISABLE_DEACTIVATION );
}

void
elPhysics::Object::SetCustomTransformGetter(
    const std::function< const btTransform&() >& v ) noexcept
{
    this->getCustomTransform = v;
}

glm::vec3
elPhysics::Object::GetPosition() const noexcept
{
    return this->geometry.position;
}

glm::vec4
elPhysics::Object::GetRotation() const noexcept
{
    return this->geometry.rotation;
}

bb::product_ptr< elPhysics::Object >
elPhysics::Object::Clone(
    const setModelMatrix_t& setModelMatrix ) const noexcept
{
    return this->physics->factory.CreateProduct< elPhysics::Object >(
        *this->physics, *this->dynamicWorld, this->shapeCreator,
        this->physicalProperties, this->geometry, setModelMatrix );
}

} // namespace world
} // namespace el3D
