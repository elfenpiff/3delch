#include "world/elInstanceModel.hpp"

namespace el3D
{
namespace world
{
elInstanceModel::elInstanceModel(
    const objectSettings_t          settings,
    const OpenGL::objectGeometry_t& objectGeometry ) noexcept
    : elObject( settings )
{
    this->object =
        this->CreateCustomObject< OpenGL::RenderTarget::Deferred,
                                  OpenGL::elGeometricObject_InstancedObject >(
            OpenGL::DrawMode::Triangles, objectGeometry );

    this->diffuseTex = this->object->CreateTexture< OpenGL::elTexture_2D >(
        OpenGL::elGeometricObject_base::ATTACHED_TO_ALL_SHADERS, "diffuse",
        GL_TEXTURE0, "" );

    this->diffuseTex->TexImage< utils::byte_t >( { 0xaa, 0xaa, 0xaa, 0xff } );
}
void
elInstanceModel::SetGeometry(
    const std::vector< OpenGL::instanceGeometry_t >& geometry ) noexcept
{
    this->object->UpdateGeometry( geometry );
}
} // namespace world
} // namespace el3D
