#include "world/elBoundingBox.hpp"

#include "3Delch/3Delch.hpp"
#include "HighGL/elBoundingBoxGenerator.hpp"
#include "utils/convert.hpp"

namespace el3D
{
namespace world
{
elBoundingBox::elBoundingBox( const objectSettings_t        settings,
                              const std::vector< GLfloat >& vertices,
                              const ModelMatrixGetter_t&    modelMatrixGetter,
                              const BoundingBoxProperties& properties ) noexcept
    : elObject( settings ), modelMatrixGetter( modelMatrixGetter )
{
    HighGL::elBoundingBoxGenerator generator( vertices,
                                              properties.cornerLineLength );
    auto&                          geometry = generator.GetGeometry();

    this->objects[BORDER_LINES] =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_NonVertexObject >(
            nullptr, RenderPipeline::RenderOrder::SolidObject,
            OpenGL::DrawMode::Lines, geometry.borderLines );

    this->SetBorderLineDrawSize( properties.borderLineDrawSize );
    this->SetBorderLineColor( properties.borderLineColor );


    this->objects[CORNER_LINES] =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_NonVertexObject >(
            nullptr, RenderPipeline::RenderOrder::SolidObject,
            OpenGL::DrawMode::Lines, geometry.cornerLines );

    this->SetCornerLineDrawSize( properties.cornerLineDrawSize );
    this->SetCornerLineColor( properties.cornerLineColor );

    if ( this->modelMatrixGetter )
    {
        this->objects[CORNER_LINES]->SetPreDrawCallback(
            [this]
            {
                auto modelMatrix = this->modelMatrixGetter();
                this->objects[CORNER_LINES]->SetModelMatrix( modelMatrix );
            } );
        this->objects[BORDER_LINES]->SetPreDrawCallback(
            [this]
            {
                auto modelMatrix = this->modelMatrixGetter();
                this->objects[BORDER_LINES]->SetModelMatrix( modelMatrix );
            } );
    }
}

elBoundingBox::elBoundingBox( objectSettings_t settings, const elModel& model,
                              const BoundingBoxProperties& properties ) noexcept
    : elBoundingBox(
          settings, model.GetVertices(), [&] { return model.GetModelMatrix(); },
          properties )
{
}

elBoundingBox::elBoundingBox( objectSettings_t             settings,
                              const elModelGroup&          modelGroup,
                              const BoundingBoxProperties& properties ) noexcept
    : elBoundingBox(
          settings, modelGroup.GetVertices(),
          [&] { return modelGroup.GetModelMatrix(); }, properties )
{
}

void
elBoundingBox::SetBorderLineDrawSize( const float size ) noexcept
{
    if ( !this->objects[BORDER_LINES] ) return;

    this->objects[BORDER_LINES]->SetDrawSize( size * GuiGL::GetDPIScaling() );
}

void
elBoundingBox::SetBorderLineColor( const color4_t& color ) noexcept
{
    if ( !this->objects[BORDER_LINES] ) return;

    this->objects[BORDER_LINES]->UpdateBuffer(
        OpenGL::elGeometricObject_NonVertexObject::BUFFER_COLOR,
        utils::SetVectorFrom(
            this->objects[BORDER_LINES]->GetNumberOfVertices(),
            glm::vec4{ color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f,
                       color[3] / 255.0f } ) );
}

void
elBoundingBox::SetCornerLineDrawSize( const float size ) noexcept
{
    if ( !this->objects[CORNER_LINES] ) return;

    this->objects[CORNER_LINES]->SetDrawSize( size * GuiGL::GetDPIScaling() );
}

void
elBoundingBox::SetCornerLineColor( const color4_t& color ) noexcept
{
    if ( !this->objects[CORNER_LINES] ) return;

    this->objects[CORNER_LINES]->UpdateBuffer(
        OpenGL::elGeometricObject_NonVertexObject::BUFFER_COLOR,
        utils::SetVectorFrom(
            this->objects[CORNER_LINES]->GetNumberOfVertices(),
            glm::vec4{ color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f,
                       color[3] / 255.0f } ) );
}

void
elBoundingBox::SetPosition( const glm::vec3& position ) noexcept
{
    for ( auto& object : this->objects )
        if ( object ) object->SetPosition( position );
}

void
elBoundingBox::SetScaling( const glm::vec3& scaling ) noexcept
{
    for ( auto& object : this->objects )
        if ( object ) object->SetScaling( scaling );
}

void
elBoundingBox::SetRotation( const glm::vec4& rotation ) noexcept
{
    for ( auto& object : this->objects )
        if ( object ) object->SetRotation( rotation );
}

void
elBoundingBox::SetModelMatrix( const glm::mat4& matrix ) noexcept
{
    for ( auto& object : this->objects )
        if ( object ) object->SetModelMatrix( matrix );
}

glm::vec3
elBoundingBox::GetPosition() const noexcept
{
    for ( auto& object : this->objects )
        if ( object ) return object->GetPosition();
    return glm::vec3( 0.0f );
}

glm::vec3
elBoundingBox::GetScaling() const noexcept
{
    for ( auto& object : this->objects )
        if ( object ) return object->GetScaling();
    return glm::vec3( 0.0f );
}

glm::vec3
elBoundingBox::GetRotationAxis() const noexcept
{
    for ( auto& object : this->objects )
        if ( object ) return object->GetRotationAxis();
    return glm::vec3( 0.0f );
}

float
elBoundingBox::GetRotationDegree() const noexcept
{
    for ( auto& object : this->objects )
        if ( object ) return object->GetRotationDegree();
    return 0.0f;
}


} // namespace world
} // namespace el3D
