#include "world/elObject.hpp"

namespace el3D
{
namespace world
{

elObject::elObject( const objectSettings_t settings ) noexcept
    : settings{ settings }
{
}

_3Delch::elScene&
elObject::GetScene() const noexcept
{
    return *this->settings.scene;
}

HighGL::elGeometricObject_UnifiedVertexObject&
elObject::GetUvo() const noexcept
{
    return *this->settings.uvo;
}

HighGL::elGeometricObject_UnifiedVertexObject&
elObject::GetSilhouetteUvo() const noexcept
{
    return *this->settings.silhouetteUvo;
}

HighGL::elGeometricObject_UnifiedVertexObject&
elObject::GetWireframeUvo() const noexcept
{
    return *this->settings.wireframeUvo;
}

elPhysics&
elObject::GetPhysics() const noexcept
{
    return *this->settings.physics;
}

elWorld&
elObject::GetWorld() const noexcept
{
    return *this->settings.world;
}

bb::product_ptr< HighGL::elLight_DirectionalLight >
elObject::CreateDirectionalLight( const glm::vec3& color,
                                  const glm::vec3& direction,
                                  const GLfloat    diffuseIntensity,
                                  const GLfloat    ambientIntensity ) noexcept
{
    return this->settings.scene->CreateDirectionalLight(
        color, direction, diffuseIntensity, ambientIntensity );
}

bb::product_ptr< HighGL::elLight_PointLight >
elObject::CreatePointLight( const glm::vec3& color,
                            const glm::vec3& attenuation,
                            const glm::vec3& position,
                            const float      diffuseIntensity,
                            const float      ambientIntensity ) noexcept
{
    return this->settings.scene->CreatePointLight(
        color, attenuation, position, diffuseIntensity, ambientIntensity );
}

} // namespace world
} // namespace el3D
