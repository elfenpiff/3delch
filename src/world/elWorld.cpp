#include "world/elWorld.hpp"

#include "3Delch/3Delch.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "OpenGL/types.hpp"

namespace el3D
{
namespace world
{
elWorld::elWorld( _3Delch::elScene& scene, const glm::vec3& gravity ) noexcept
    : scene{ &scene }, physics{ gravity }
{
    this->defaultUVOTextureSize = _3Delch::GetConfig().Get< int >(
        { "global", "scenes", this->scene->GetSceneName(),
          "unifiedVertexObject", "textureSize" },
        this->defaultUVOTextureSize );

    this->uvo = this->scene->CreateCustomObject<
        OpenGL::RenderTarget::Deferred,
        HighGL::elGeometricObject_UnifiedVertexObject >(
        OpenGL::DrawMode::Triangles,
        static_cast< uint64_t >( this->defaultUVOTextureSize[0] ),
        static_cast< uint64_t >( this->defaultUVOTextureSize[1] ),
        static_cast< uint64_t >( this->defaultUVOTextureSize[2] ) );

    this->silhouette.shader =
        OpenGL::elShaderProgram::Create(
            OpenGL::shaderFromFile,
            _3Delch::GetConfig().Get< std::string >(
                { "global", "scenes", "main", "unifiedVertexObject",
                  "shaderFile_silhouette" },
                { "glsl/forwardRenderer_Silhouette.glsl" } )[0],
            "default", 440 )
            .OrElse(
                []
                {
                    LOG_FATAL( 0 )
                        << "unable to initialize silhouette shader in elWorld";
                    std::terminate();
                } )
            .GetValue();

    this->silhouette.uvo = this->scene->CreateCustomObject<
        OpenGL::RenderTarget::Forward,
        HighGL::elGeometricObject_UnifiedVertexObject >(
        this->silhouette.shader.get(), RenderPipeline::RenderOrder::SolidObject,
        OpenGL::DrawMode::TrianglesWithAdjacency, 1u, 1u, 1u );

    this->wireframe.shader =
        OpenGL::elShaderProgram::Create(
            OpenGL::shaderFromFile,
            _3Delch::GetConfig().Get< std::string >(
                { "global", "scenes", "main", "unifiedVertexObject",
                  "shaderFile_wireframe" },
                { "glsl/forwardRenderer_WireFrame.glsl" } )[0],
            "default", 440 )
            .OrElse(
                []
                {
                    LOG_FATAL( 0 )
                        << "unable to initialize wireframe shader in elWorld";
                    std::terminate();
                } )
            .GetValue();

    this->wireframe.uvo = this->scene->CreateCustomObject<
        OpenGL::RenderTarget::Forward,
        HighGL::elGeometricObject_UnifiedVertexObject >(
        this->wireframe.shader.get(), RenderPipeline::RenderOrder::SolidObject,
        OpenGL::DrawMode::Triangles, 1u, 1u, 1u );

    this->updateCallback = _3Delch::CreateMainLoopCallback(
        [this] { this->physics.Update( _3Delch::GetDeltaTime() ); } );
}

} // namespace world
} // namespace el3D
