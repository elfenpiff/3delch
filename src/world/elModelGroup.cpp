#include "world/elModelGroup.hpp"

#include "world/elWorld.hpp"

namespace el3D
{
namespace world
{
elModelGroup::elModelGroup( const objectSettings_t settings ) noexcept
    : elObject( settings )
{
}

elModelGroup::elModelGroup( const objectSettings_t settings,
                            const std::string&     fileName,
                            const float            scaling ) noexcept
    : elModelGroup( settings )
{
    auto loader = HighGL::elMeshLoader::Create( fileName, scaling );
    if ( loader.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to load model " << fileName
                       << " into model group";
        return;
    }

    auto uvoObjects =
        ( *loader )->LoadIntoUnifiedVertexObject( this->GetUvo() );

    for ( auto& object : uvoObjects )
        this->models.emplace_back(
            this->GetWorld().Create< elModel >( std::move( object ) ) );
}

bb::product_ptr< elModelGroup >
elModelGroup::Clone() const noexcept
{
    auto clone = this->GetWorld().Create< elModelGroup >();

    clone->models.reserve( this->models.size() );

    for ( auto& m : this->models )
        clone->models.emplace_back( m->Clone() );

    if ( this->physical )
        clone->physical = this->physical->Clone(
            [ptr = clone.Get()]( auto m ) { ptr->SetModelMatrix( m ); } );

    return clone;
}

void
elModelGroup::SetCustomPhysicalTransformGetter(
    const std::function< const btTransform&() >& v ) noexcept
{
    if ( this->physical ) this->physical->SetCustomTransformGetter( v );
}

void
elModelGroup::SetDiffuseColor( const color4_t& color ) noexcept
{
    bb::for_each( this->models,
                  [&]( auto& m ) { m->SetDiffuseColor( color ); } );
}

void
elModelGroup::SetEmissionColor( const color4_t& color ) noexcept
{
    bb::for_each( this->models,
                  [&]( auto& m ) { m->SetEmissionColor( color ); } );
}

void
elModelGroup::SetSpecularColor( const color3_t&     color,
                                const utils::byte_t metallic ) noexcept
{
    bb::for_each( this->models,
                  [&]( auto& m ) { m->SetSpecularColor( color, metallic ); } );
}

void
elModelGroup::SetShininessAndRoughness( const uint8_t shininess,
                                        const uint8_t roughness ) noexcept
{
    bb::for_each( this->models, [&]( auto& m )
                  { m->SetShininessAndRoughness( shininess, roughness ); } );
}

void
elModelGroup::SetPosition( const glm::vec3& value ) noexcept
{
    for ( auto& m : models )
        m->SetPosition( value );
}

void
elModelGroup::SetScaling( const glm::vec3& value ) noexcept
{
    for ( auto& m : models )
        m->SetScaling( value );
}

void
elModelGroup::SetRotation( const glm::vec4& value ) noexcept
{
    for ( auto& m : models )
        m->SetRotation( value );
}

void
elModelGroup::SetModelMatrix( const glm::mat4& matrix ) noexcept
{
    for ( auto& m : models )
        m->SetModelMatrix( matrix );
}

glm::vec3
elModelGroup::GetPosition() const noexcept
{
    if ( this->physical ) return this->physical->GetPosition();
    return ( this->models.empty() )
               ? glm::vec3{ 0.0f }
               : this->models.begin()->Get()->GetPosition();
}

glm::vec3
elModelGroup::GetScaling() const noexcept
{
    return ( this->models.empty() ) ? glm::vec3{ 0.0f }
                                    : this->models.begin()->Get()->GetScaling();
}

glm::vec3
elModelGroup::GetRotationAxis() const noexcept
{
    if ( this->physical )
    {
        auto r = this->physical->GetRotation();
        return glm::vec3( r.x, r.y, r.z );
    }
    return ( this->models.empty() )
               ? glm::vec3{ 0.0f }
               : this->models.begin()->Get()->GetRotationAxis();
}

float
elModelGroup::GetRotationDegree() const noexcept
{
    if ( this->physical ) return this->physical->GetRotation().w;
    return ( this->models.empty() )
               ? 0.0f
               : this->models.begin()->Get()->GetRotationDegree();
}

glm::vec3
elModelGroup::GetMin() const noexcept
{
    bool      hasFirstRun = true;
    glm::vec3 min;
    for ( auto& m : this->models )
    {
        if ( hasFirstRun )
        {
            min         = m->GetMin();
            hasFirstRun = false;
        }
        else
        {
            auto localMax = m->GetMin();
            min           = glm::vec3( std::min( min.x, localMax.x ),
                                       std::min( min.y, localMax.y ),
                                       std::min( min.z, localMax.z ) );
        }
    }

    return min;
}

glm::vec3
elModelGroup::GetMax() const noexcept
{
    bool      hasFirstRun = true;
    glm::vec3 max;
    for ( auto& m : this->models )
    {
        if ( hasFirstRun )
        {
            max         = m->GetMax();
            hasFirstRun = false;
        }
        else
        {
            auto localMax = m->GetMax();
            max           = glm::vec3( std::max( max.x, localMax.x ),
                                       std::max( max.y, localMax.y ),
                                       std::max( max.z, localMax.z ) );
        }
    }

    return max;
}

glm::vec3
elModelGroup::GetDimension() const noexcept
{
    glm::vec3 dimension = this->GetMax() - this->GetMin();
    return glm::vec3{ std::abs( dimension.x ), std::abs( dimension.y ),
                      std::abs( dimension.z ) };
}

std::vector< GLfloat >
elModelGroup::GetVertices() const noexcept
{
    std::vector< GLfloat > vertices;

    for ( auto& m : this->models )
    {
        auto modelVertices = m->GetVertices();
        vertices.insert( vertices.end(), modelVertices.begin(),
                         modelVertices.end() );
    }

    return vertices;
}

std::vector< GLuint >
elModelGroup::GetElements() const noexcept
{
    std::vector< GLuint > elements;
    GLuint                offset = 0;

    for ( auto& m : this->models )
    {
        auto modelElements = m->GetElements();
        bb::for_each( modelElements, [&]( auto& e ) { e += offset; } );
        elements.insert( elements.end(), modelElements.begin(),
                         modelElements.end() );
        offset += static_cast< GLuint >( m->GetVertices().size() ) / 3;
    }

    return elements;
}

glm::mat4
elModelGroup::GetModelMatrix() const noexcept
{
    return ( this->models.empty() ) ? glm::mat4( 1.0f )
                                    : this->models[0]->GetModelMatrix();
}

bool
elModelGroup::AttachCamera( OpenGL::elCamera_Perspective& camera,
                            const glm::vec3&              positionOffset,
                            const glm::vec3 lookAtOffset ) noexcept
{
    this->cameraAttachment.emplace( camera, *this, positionOffset,
                                    lookAtOffset );
    return static_cast< bool >( this->cameraAttachment->IsAttachedToCamera() );
}
} // namespace world
} // namespace el3D
