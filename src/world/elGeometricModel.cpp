#include "world/elGeometricModel.hpp"

namespace el3D
{
namespace world
{
elGeometricModel::elGeometricModel(
    const objectSettings_t            settings,
    const geometricModelProperties_t& properties ) noexcept
    : elObject( settings )
{
    this->object =
        this->CreateCustomObject< OpenGL::RenderTarget::Forward,
                                  OpenGL::elGeometricObject_NonVertexObject >(
            nullptr, RenderPipeline::RenderOrder::SolidObject,
            GeometricType2DrawMode( properties.type ), properties.vertices );

    if ( properties.colorPerVertex.empty() )
        this->SetColor( properties.uniformColor );
    else
        this->SetColorPerVertex( properties.colorPerVertex );


    this->object->SetDrawSize( properties.drawSize );
    this->object->SetPosition( properties.position );
}

void
elGeometricModel::SetDrawSize( const float size ) noexcept
{
    this->object->SetDrawSize( size );
}

void
elGeometricModel::SetColorPerVertex(
    const std::vector< GLfloat >& colorPerVertex ) noexcept
{
    auto colorSize        = colorPerVertex.size();
    auto numberOfVertices = this->object->GetNumberOfVertices();

    if ( colorSize != numberOfVertices * 4 )
        LOG_WARN( 0 )
            << "the color buffer size for a full per vertex coloring is "
               "wrong. Expected size is: "
            << 4 * numberOfVertices << " but the buffer size is " << colorSize;
    this->object->UpdateBuffer(
        OpenGL::elGeometricObject_NonVertexObject::BUFFER_COLOR,
        colorPerVertex );
}

void
elGeometricModel::SetColor( const color4_t& color ) noexcept
{
    auto numberOfVertices = this->object->GetNumberOfVertices();
    std::vector< GLfloat > colorBuffer;
    colorBuffer.reserve( 4 * numberOfVertices );
    for ( uint64_t i = 0; i < numberOfVertices; ++i )
    {
        colorBuffer.emplace_back( color[0] );
        colorBuffer.emplace_back( color[1] );
        colorBuffer.emplace_back( color[2] );
        colorBuffer.emplace_back( color[3] );
    }
    this->SetColorPerVertex( colorBuffer );
}

OpenGL::DrawMode
elGeometricModel::GeometricType2DrawMode(
    const GeometricType type ) const noexcept
{
    switch ( type )
    {
        case GeometricType::Points:
            return OpenGL::DrawMode::Points;
        case GeometricType::LineStrip:
            return OpenGL::DrawMode::LineStrip;
        case GeometricType::Lines:
            return OpenGL::DrawMode::Lines;
    }
    return OpenGL::DrawMode::Points;
}

void
elGeometricModel::SetPosition( const glm::vec3& position ) noexcept
{
    this->object->SetPosition( position );
}

void
elGeometricModel::SetScaling( const glm::vec3& scaling ) noexcept
{
    this->object->SetScaling( scaling );
}

void
elGeometricModel::SetRotation( const glm::vec4& rotation ) noexcept
{
    this->object->SetRotation( rotation );
}

void
elGeometricModel::SetModelMatrix( const glm::mat4& matrix ) noexcept
{
    this->object->SetModelMatrix( matrix );
}

glm::vec3
elGeometricModel::GetPosition() const noexcept
{
    return this->object->GetPosition();
}

glm::vec3
elGeometricModel::GetScaling() const noexcept
{
    return this->object->GetScaling();
}

glm::vec3
elGeometricModel::GetRotationAxis() const noexcept
{
    return this->object->GetRotationAxis();
}

float
elGeometricModel::GetRotationDegree() const noexcept
{
    return this->object->GetRotationDegree();
}
} // namespace world
} // namespace el3D
