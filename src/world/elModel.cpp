#include "world/elModel.hpp"

#include "3Delch/3Delch.hpp"
#include "GLAPI/elImage.hpp"
#include "HighGL/elGeometricObject_UnifiedVertexObject.hpp"
#include "world/elWorld.hpp"

namespace el3D
{
namespace world
{
elModel::elModel( const objectSettings_t          settings,
                  const OpenGL::objectGeometry_t& objectGeometry,
                  const GeometricObjectFacing     facing ) noexcept
    : elObject{ settings }
{
    switch ( facing )
    {
        case GeometricObjectFacing::Outer:
            this->object = this->GetUvo().CreateObject(
                objectGeometry.vertices, objectGeometry.textureCoordinates,
                objectGeometry.normals, objectGeometry.tangents,
                objectGeometry.bitangents, objectGeometry.elements );
            break;
        case GeometricObjectFacing::Inner:
            this->object = this->GetUvo().CreateObject(
                objectGeometry.vertices, objectGeometry.textureCoordinates,
                objectGeometry.innerNormals, objectGeometry.tangents,
                objectGeometry.bitangents, objectGeometry.innerElements );
            break;
    }
}

elModel::elModel(
    const objectSettings_t                                   settings,
    bb::product_ptr< HighGL::UnifiedVertexObject::Object >&& object ) noexcept
    : elObject( settings ), object( std::move( object ) )
{
}

void
elModel::SetPosition( const glm::vec3& v ) noexcept
{
    this->object->SetPosition( v );
}

void
elModel::SetScaling( const glm::vec3& v ) noexcept
{
    this->object->SetScaling( v );
}

void
elModel::SetRotation( const glm::vec4& rotation ) noexcept
{
    this->object->SetRotation( rotation );
}

void
elModel::SetModelMatrix( const glm::mat4& matrix ) noexcept
{

    this->object->SetModelMatrix( matrix );
}

void
elModel::SetCustomPhysicalTransformGetter(
    const std::function< const btTransform&() >& v ) noexcept
{
    if ( this->physical ) this->physical->SetCustomTransformGetter( v );
}

glm::vec3
elModel::GetPosition() const noexcept
{
    return ( this->physical ) ? this->physical->GetPosition()
                              : this->object->GetPosition();
}

glm::vec3
elModel::GetScaling() const noexcept
{
    return this->object->GetScaling();
}

glm::vec3
elModel::GetRotationAxis() const noexcept
{
    if ( this->physical )
    {
        auto r = this->physical->GetRotation();
        return glm::vec3( r.x, r.y, r.z );
    }
    return this->object->GetRotationAxis();
}

float
elModel::GetRotationDegree() const noexcept
{
    return ( this->physical ) ? this->physical->GetRotation().w
                              : this->object->GetRotationDegree();
}

const std::string&
elModel::GetName() const noexcept
{
    return this->object->GetName();
}

bb::product_ptr< elModel >
elModel::Clone() const noexcept
{
    auto clone = this->GetWorld().Create< elModel >( this->object->Clone() );
    clone->SetBuffer( { 0x00, 0x00, 0x00, 0x00 },
                      el3D::HighGL::UnifiedVertexObject::EVENT_COLOR );

    if ( this->physical )
        clone->physical = this->physical->Clone(
            [ptr = clone.Get()]( auto m ) { ptr->SetModelMatrix( m ); } );

    return clone;
}

glm::vec3
elModel::GetExtrema(
    const std::function< float( float, float ) >& extremaGetter ) const noexcept
{
    auto vertexBuffer = this->object->GetFloatBuffer(
        HighGL::UnifiedVertexObject::floatBufferList_t::VERTEX );

    bool      hasFirstRun = true;
    glm::vec3 min{ 0.0f };
    for ( uint64_t i = 0, limit = vertexBuffer.size() / 3; i < limit; ++i )
    {
        glm::vec3 nextVertex =
            glm::vec3( vertexBuffer[i * 3], vertexBuffer[i * 3 + 1],
                       vertexBuffer[i * 3 + 2] ) *
            this->object->GetScaling();
        if ( hasFirstRun )
        {
            min         = nextVertex;
            hasFirstRun = false;
        }
        else
            min = glm::vec3( extremaGetter( min.x, nextVertex.x ),
                             extremaGetter( min.y, nextVertex.y ),
                             extremaGetter( min.z, nextVertex.z ) );
    }

    return min;
}

glm::vec3
elModel::GetMin() const noexcept
{
    return this->GetExtrema( []( float a, float b )
                             { return std::min( a, b ); } );
}

glm::vec3
elModel::GetMax() const noexcept
{
    return this->GetExtrema( []( float a, float b )
                             { return std::max( a, b ); } );
}

glm::vec3
elModel::GetDimension() const noexcept
{
    glm::vec3 dimension = this->GetMax() - this->GetMin();
    return glm::vec3{ std::abs( dimension.x ), std::abs( dimension.y ),
                      std::abs( dimension.z ) };
}

bool
elModel::SetTexture( const TextureType type, const uint64_t x, const uint64_t y,
                     const utils::byte_t* const dataPtr ) noexcept
{
    auto image = GLAPI::elImage::Create( GLAPI::FromArray, x, y, dataPtr );
    if ( image.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to setup texture from array";
        return false;
    }
    auto uvoTexSize = this->GetUvo().GetTextureArraySize();
    image->get()->Resize( uvoTexSize.x, uvoTexSize.y );
    return this->AddTexture( "", type, image->get()->GetBytePointer() );
}

bool
elModel::SetTexture( const TextureType  type,
                     const std::string& fileName ) noexcept
{
    auto image = GLAPI::elImage::Create( GLAPI::FromFilePath, fileName,
                                         SDL_PIXELFORMAT_RGBA32 );
    if ( image.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to load image file " << fileName
                       << " for texture "
                       << TEXTURE_TYPE_STRING[static_cast< uint64_t >( type )];
        return false;
    }
    auto uvoTexSize = this->GetUvo().GetTextureArraySize();
    image->get()->Resize( uvoTexSize.x, uvoTexSize.y );

    return this->AddTexture( fileName, type, image->get()->GetBytePointer() );
}

bool
elModel::AddTexture( const std::string&  uniqueIdentifier,
                     const TextureType   type,
                     const GLvoid* const pixels ) noexcept
{
    auto texture = this->object->CreateTexture( uniqueIdentifier, pixels );
    if ( !texture )
    {
        LOG_ERROR( 0 ) << "unable to create texture in elModel";
        return false;
    }

    switch ( type )
    {
        case TextureType::Diffuse:
            SetBuffer( texture->GetId(),
                       HighGL::UnifiedVertexObject::TEX_DIFFUSE );
            break;
        case TextureType::Normal:
            SetBuffer( texture->GetId(),
                       HighGL::UnifiedVertexObject::TEX_NORMAL );
            break;
        case TextureType::Emission:
            SetBuffer( texture->GetId(),
                       HighGL::UnifiedVertexObject::TEX_EMISSION );
            break;
    }

    return true;
}

template < typename T, uint64_t N, typename EnumType >
void
elModel::SetBuffer( const std::array< T, N >& element,
                    const EnumType bufferType, const float adjustment ) noexcept
{
    HighGL::UnifiedVertexObject::floatBuffer_t buffer;
    buffer.reserve( N * this->object->GetNumberOfVertices() );

    for ( uint64_t i = 0u; i < this->object->GetNumberOfVertices(); ++i )
        for ( auto& e : element )
            buffer.emplace_back( e / adjustment );

    this->object->SetFloatBuffer( bufferType, buffer );
}

template < typename EnumType >
void
elModel::SetBuffer( const glm::vec4& element,
                    const EnumType   bufferType ) noexcept
{
    HighGL::UnifiedVertexObject::floatBuffer_t buffer;
    buffer.reserve( 4 * this->object->GetNumberOfVertices() );
    for ( uint64_t i = 0u; i < this->object->GetNumberOfVertices(); ++i )
        for ( int32_t n = 0; n < 4; ++n )
            buffer.emplace_back( element[n] );
    this->object->SetFloatBuffer( bufferType, buffer );
}

void
elModel::SetBuffer(
    const GLuint                                        element,
    const HighGL::UnifiedVertexObject::uintBufferList_t bufferType ) noexcept
{
    this->object->SetUintBuffer(
        bufferType, HighGL::UnifiedVertexObject::uintBuffer_t(
                        this->object->GetNumberOfVertices(), element ) );
}

void
elModel::SetDiffuseColor( const color4_t& color ) noexcept
{
    this->SetBuffer( color, HighGL::UnifiedVertexObject::DIFFUSE_COLOR,
                     255.0f );
    this->SetBuffer(
        HighGL::elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID,
        HighGL::UnifiedVertexObject::TEX_DIFFUSE );
}

void
elModel::SetSpecularColor( const color3_t&     color,
                           const utils::byte_t metallic ) noexcept
{
    color4_t c{ color[0], color[1], color[2], metallic };
    this->SetBuffer( c, HighGL::UnifiedVertexObject::SPECULAR_COLOR, 255.0f );
    this->SetBuffer(
        HighGL::elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID,
        HighGL::UnifiedVertexObject::TEX_SPECULAR_COLOR );
}

void
elModel::SetShininessAndRoughness( const uint8_t shininess,
                                   const uint8_t roughness ) noexcept
{
    this->SetBuffer< float, 2 >(
        { static_cast< float >( shininess ) / 255.0f,
          static_cast< float >( roughness ) / 255.0f },
        HighGL::UnifiedVertexObject::SHININESS_AND_ROUGHNESS );
    this->SetBuffer(
        HighGL::elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID,
        HighGL::UnifiedVertexObject::TEX_SHININESS_AND_ROUGHNESS );
}

void
elModel::SetEmissionColor( const color4_t& color ) noexcept
{
    this->SetBuffer( color, HighGL::UnifiedVertexObject::EMISSION_COLOR,
                     255.0f );
    this->SetBuffer(
        HighGL::elGeometricObject_UnifiedVertexObject::INVALID_TEXTURE_ID,
        HighGL::UnifiedVertexObject::TEX_EMISSION );
}

std::vector< GLfloat >
elModel::GetVertices() const noexcept
{
    return this->object->GetFloatBuffer( HighGL::UnifiedVertexObject::VERTEX );
}

std::vector< GLuint >
elModel::GetElements() const noexcept
{
    return this->object->GetUintBuffer( HighGL::UnifiedVertexObject::ELEMENTS );
}

glm::mat4
elModel::GetModelMatrix() const noexcept
{
    return this->object->GetModelMatrix();
}

bool
elModel::AttachCamera( OpenGL::elCamera_Perspective& camera,
                       const glm::vec3&              positionOffset,
                       const glm::vec3               lookAtOffset ) noexcept
{
    this->cameraAttachment.emplace( camera, *this, positionOffset,
                                    lookAtOffset );
    return static_cast< bool >( this->cameraAttachment->IsAttachedToCamera() );
}

template void
elModel::SetBuffer< HighGL::UnifiedVertexObject::floatBufferList_t >(
    const glm::vec4&, const HighGL::UnifiedVertexObject::floatBufferList_t );

} // namespace world
} // namespace el3D
