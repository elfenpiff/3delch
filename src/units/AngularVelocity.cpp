#include "units/AngularVelocity.hpp"

#include <cmath>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace units
{
AngularVelocity::AngularVelocity() noexcept : AngularVelocity( 0.0 )
{
}

AngularVelocity
AngularVelocity::RadiansPerSecond( const float_t value ) noexcept
{
    return AngularVelocity( value );
}

AngularVelocity
AngularVelocity::DegreePerSecond( const float_t value ) noexcept
{
    return AngularVelocity( value / ( 180.0 / M_PI ) );
}

BasicUnit< AngularVelocity >::float_t
AngularVelocity::GetRadiansPerSecond() const noexcept
{
    return this->value;
}

BasicUnit< AngularVelocity >::float_t
AngularVelocity::GetDegreePerSecond() const noexcept
{
    return this->value * ( 180.0 / M_PI );
}

AngularVelocity::AngularVelocity( const float_t radiansPerSeconds ) noexcept
    : BasicUnit< AngularVelocity >( radiansPerSeconds )
{
}

std::ostream&
operator<<( std::ostream&                       stream,
            const el3D::units::AngularVelocity& v ) noexcept
{
    stream << v.GetRadiansPerSecond() << "rad/s";
    return stream;
}
} // namespace units
} // namespace el3D
