#include "units/Velocity.hpp"

namespace el3D
{
namespace units
{
Velocity::Velocity() noexcept : Velocity( 0.0 )
{
}

Velocity
Velocity::MeterPerSecond( const float_t value ) noexcept
{
    return Velocity( value );
}

Velocity
Velocity::KilometerPerHour( const float_t value ) noexcept
{
    return Velocity( value / float_t( 3.6 ) );
}

BasicUnit< Velocity >::float_t
Velocity::GetMeterPerSecond() const noexcept
{
    return this->value;
}

BasicUnit< Velocity >::float_t
Velocity::GetKilometerPerHour() const noexcept
{
    return this->value * float_t( 3.6 );
}

Velocity::Velocity( const float_t meterPerSeconds ) noexcept
    : BasicUnit( meterPerSeconds )
{
}

namespace literals
{
Velocity operator""_m_s( unsigned long long v ) noexcept
{
    return Velocity::MeterPerSecond(
        static_cast< BasicUnit< Velocity >::float_t >( v ) );
}

Velocity operator""_m_s( long double v ) noexcept
{
    return Velocity::MeterPerSecond(
        static_cast< BasicUnit< Velocity >::float_t >( v ) );
}

Velocity operator""_km_h( unsigned long long v ) noexcept
{
    return Velocity::KilometerPerHour(
        static_cast< BasicUnit< Velocity >::float_t >( v ) );
}

Velocity operator""_km_h( long double v ) noexcept
{
    return Velocity::KilometerPerHour(
        static_cast< BasicUnit< Velocity >::float_t >( v ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Velocity& t ) noexcept
{
    stream << t.GetMeterPerSecond() << "m/s";
    return stream;
}
} // namespace units
} // namespace el3D
