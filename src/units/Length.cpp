#include "units/Length.hpp"

namespace el3D
{
namespace units
{
Length::Length() noexcept : Length( 0.0 )
{
}

Length
Length::Meter( const float_t value ) noexcept
{
    return Length( value );
}

BasicUnit< Length >::float_t
Length::GetMeter() const noexcept
{
    return this->value;
}

Length::Length( const float_t length ) noexcept : BasicUnit( length )
{
}
namespace literals
{
Length operator"" _m( unsigned long long value ) noexcept
{
    return Length::Meter(
        static_cast< BasicUnit< Length >::float_t >( value ) );
}

Length operator"" _m( long double value ) noexcept
{
    return Length::Meter(
        static_cast< BasicUnit< Length >::float_t >( value ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Length& t ) noexcept
{
    stream << t.GetMeter() << "m";
    return stream;
}
} // namespace units
} // namespace el3D
