#include "units/Confidence.hpp"

namespace el3D
{
namespace units
{
Confidence::Confidence() noexcept : Confidence( 0.0 )
{
}

Confidence::Confidence( const float_t value ) noexcept
    : BasicUnit< Confidence >( value )
{
}

BasicUnit< Confidence >::float_t
Confidence::Get() const noexcept
{
    return this->value;
}

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Confidence& c ) noexcept
{
    stream << c.Get() << "[confidence]";
    return stream;
}
} // namespace units
} // namespace el3D
