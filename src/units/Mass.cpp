#include "units/Mass.hpp"

namespace el3D
{
namespace units
{
Mass::Mass() noexcept : Mass( 0.0 )
{
}

Mass
Mass::Gramm( const float_t value ) noexcept
{
    return Mass( value * float_t( 1000.0 ) );
}

Mass
Mass::Kilogramm( const float_t value ) noexcept
{
    return Mass( value );
}

BasicUnit< Mass >::float_t
Mass::GetGramm() const noexcept
{
    return this->value * float_t( 1000.0 );
}

BasicUnit< Mass >::float_t
Mass::GetKilogramm() const noexcept
{
    return this->value;
}

Mass::Mass( const float_t kilogramm ) noexcept : BasicUnit( kilogramm )
{
}
namespace literals
{
Mass operator""_g( unsigned long long v ) noexcept
{
    return Mass::Gramm( static_cast< BasicUnit< Mass >::float_t >( v ) );
}
Mass operator""_g( long double v ) noexcept
{
    return Mass::Gramm( static_cast< BasicUnit< Mass >::float_t >( v ) );
}

Mass operator""_kg( unsigned long long v ) noexcept
{
    return Mass::Kilogramm( static_cast< BasicUnit< Mass >::float_t >( v ) );
}

Mass operator""_kg( long double v ) noexcept
{
    return Mass::Kilogramm( static_cast< BasicUnit< Mass >::float_t >( v ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Mass& v ) noexcept
{
    stream << v.GetKilogramm() << "kg";
    return stream;
}

} // namespace units
} // namespace el3D

