#include "units/AngularAcceleration.hpp"

namespace el3D
{
namespace units
{
AngularAcceleration::AngularAcceleration() noexcept : AngularAcceleration( 0.0 )
{
}

AngularAcceleration
AngularAcceleration::RadiansPerSecondSquare( const float_t value ) noexcept
{
    return AngularAcceleration( value );
}

BasicUnit< AngularAcceleration >::float_t
AngularAcceleration::GetRadiansPerSecondSquare() const noexcept
{
    return this->value;
}

AngularAcceleration::AngularAcceleration(
    const float_t radiansPerSecondSquare ) noexcept
    : BasicUnit< AngularAcceleration >( radiansPerSecondSquare )
{
}

std::ostream&
operator<<( std::ostream&                           stream,
            const el3D::units::AngularAcceleration& a ) noexcept
{
    stream << a.GetRadiansPerSecondSquare() << "rad/(s^-2)";
    return stream;
}
} // namespace units
} // namespace el3D
