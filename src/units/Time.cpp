#include "units/Time.hpp"

#include "units/units_NAMESPACE_NAME.hpp"

namespace el3D
{
namespace units
{
Time
Time::TimeSinceEpoch() noexcept
{
    int64_t now = std::chrono::duration_cast< std::chrono::nanoseconds >(
                      std::chrono::system_clock::now().time_since_epoch() )
                      .count();
    return Time( static_cast< float_t >( now ) / 1000000000.0 );
}

Time
Time::NanoSeconds( const float_t value ) noexcept
{
    return Time{ value / 1000000000.0 };
}

Time
Time::MicroSeconds( const float_t value ) noexcept
{
    return Time{ value / 1000000.0 };
}

Time
Time::MilliSeconds( const float_t value ) noexcept
{
    return Time{ value / 1000.0 };
}

Time
Time::Seconds( const float_t value ) noexcept
{
    return Time{ value };
}

Time
Time::Minutes( const float_t value ) noexcept
{
    return Time{ value * 60.0 };
}

Time
Time::Hours( const float_t value ) noexcept
{
    return Time{ value * 3600.0 };
}

Time
Time::Days( const float_t value ) noexcept
{
    return Time{ value * 3600.0 * 24.0 };
}


Time::Time( const float_t value ) noexcept : BasicUnit( value )
{
}

Time::Time() noexcept : Time( 0.0 )
{
}

BasicUnit< Time >::float_t
Time::GetNanoSeconds() const noexcept
{
    return this->value * 1000000000.0;
}

BasicUnit< Time >::float_t
Time::GetMicroSeconds() const noexcept
{
    return this->value * 1000000.0;
}

BasicUnit< Time >::float_t
Time::GetMilliSeconds() const noexcept
{
    return this->value * 1000.0;
}

BasicUnit< Time >::float_t
Time::GetSeconds() const noexcept
{
    return this->value;
}

BasicUnit< Time >::float_t
Time::GetMinutes() const noexcept
{
    return this->value / 60.0;
}

BasicUnit< Time >::float_t
Time::GetHours() const noexcept
{
    return this->value / 3600.0;
}

BasicUnit< Time >::float_t
Time::GetDays() const noexcept
{
    return this->value / ( 24.0 * 3600.0 );
}

Time::operator struct timeval() const noexcept
{
    struct timeval v;
    v.tv_sec = 0,
#ifndef _WIN32
    v.tv_usec = static_cast< suseconds_t >( this->GetMicroSeconds() );
#else
    v.tv_usec = static_cast< long >( this->GetMicroSeconds() );
#endif
    return v;
}

Time::operator struct timespec() const noexcept
{
    struct timespec v;
    errno = 0;

    int success = timespec_get( &v, TIME_UTC );
    if ( success == 0 ) return v;

    v.tv_sec += static_cast< long >( this->GetSeconds() );
    v.tv_nsec += static_cast< long >( this->GetNanoSeconds() ) -
                 static_cast< long >( this->GetSeconds() ) * 1000000000;
    return v;
}

Time::operator std::chrono::duration< double >() const noexcept
{
    return std::chrono::nanoseconds(
        static_cast< uint64_t >( this->GetNanoSeconds() ) );
}

namespace literals
{
Time operator"" _ns( unsigned long long value ) noexcept
{
    return Time::NanoSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _us( unsigned long long value ) noexcept
{
    return Time::MicroSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _ms( unsigned long long value ) noexcept
{
    return Time::MilliSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _s( unsigned long long value ) noexcept
{
    return Time::Seconds( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _min( unsigned long long value ) noexcept
{
    return Time::Minutes( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _h( unsigned long long value ) noexcept
{
    return Time::Hours( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _d( unsigned long long value ) noexcept
{
    return Time::Days( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _ns( long double value ) noexcept
{
    return Time::NanoSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _us( long double value ) noexcept
{
    return Time::MicroSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _ms( long double value ) noexcept
{
    return Time::MilliSeconds(
        static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _s( long double value ) noexcept
{
    return Time::Seconds( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _min( long double value ) noexcept
{
    return Time::Minutes( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _h( long double value ) noexcept
{
    return Time::Hours( static_cast< BasicUnit< Time >::float_t >( value ) );
}

Time operator"" _d( long double value ) noexcept
{
    return Time::Days( static_cast< BasicUnit< Time >::float_t >( value ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Time& t ) noexcept
{
    stream << t.GetSeconds() << "s";
    return stream;
}
} // namespace units
} // namespace el3D
