#include "units/Probability.hpp"

namespace el3D
{
namespace units
{
Probability::Probability() noexcept : Probability( 0.0 )
{
}

Probability
Probability::Percent( const float_t value ) noexcept
{
    return Probability( value );
}

Probability
Probability::ZeroToOneValue( const float_t value ) noexcept
{
    return Probability( value * 100.0 );
}

BasicUnit< Probability >::float_t
Probability::GetPercent() const noexcept
{
    return this->value;
}

BasicUnit< Probability >::float_t
Probability::GetZeroToOneValue() const noexcept
{
    return this->value / 100.0;
}

Probability::Probability( const float_t percent ) noexcept
    : BasicUnit< Probability >( percent )
{
}

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Probability& c ) noexcept
{
    stream << c.GetPercent() << "%";
    return stream;
}
} // namespace units
} // namespace el3D
