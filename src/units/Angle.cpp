#include "units/Angle.hpp"

#include <cmath>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace units
{
Angle::Angle() noexcept : Angle( 0.0 )
{
}

Angle
Angle::Degree( const float_t value ) noexcept
{
    return Angle( value );
}

Angle
Angle::Radian( const float_t value ) noexcept
{
    return Angle( value * ( 180.0 / M_PI ) );
}

BasicUnit< Angle >::float_t
Angle::GetDegree() const noexcept
{
    return this->value;
}

BasicUnit< Angle >::float_t
Angle::GetRadian() const noexcept
{
    return this->value / ( 180.0 / M_PI );
}

Angle::Angle( const float_t degree ) noexcept : BasicUnit( degree )
{
}
namespace literals
{
Angle operator""_deg( unsigned long long v ) noexcept
{
    return Angle::Degree( static_cast< BasicUnit< Angle >::float_t >( v ) );
}

Angle operator""_deg( long double v ) noexcept
{
    return Angle::Degree( static_cast< BasicUnit< Angle >::float_t >( v ) );
}

Angle operator""_rad( unsigned long long v ) noexcept
{
    return Angle::Radian( static_cast< BasicUnit< Angle >::float_t >( v ) );
}

Angle operator""_rad( long double v ) noexcept
{
    return Angle::Radian( static_cast< BasicUnit< Angle >::float_t >( v ) );
}
} // namespace literals

std::ostream&
operator<<( std::ostream& stream, const el3D::units::Angle& a ) noexcept
{
    stream << a.GetDegree() << "°";
    return stream;
}
} // namespace units
} // namespace el3D
