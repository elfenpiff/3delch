#include "GuiGL/elWidget_Tree.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Slider.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Tree::entry_t::entry_t(
    elWidget_Button_ptr&& base, elWidget_base_ptr&& subFrame,
    const std::shared_ptr< std::vector< entry_t > > sub ) noexcept
    : base{ std::move( base ) }, subFrame{ std::move( subFrame ) }, sub{ sub }
{
}

elWidget_Tree::entry_t::entry_t( entry_t&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elWidget_Tree::entry_t&
elWidget_Tree::entry_t::operator=( entry_t&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        this->sub      = std::move( rhs.sub );
        this->subFrame = std::move( rhs.subFrame );
        this->base     = std::move( rhs.base );
    }
    return *this;
}

elWidget_Tree::elWidget_Tree( const constructor_t& constructor )
    : elWidget_Window( constructor )
{
    this->ResetRootNode();
}

elWidget_Tree::~elWidget_Tree()
{
}

void
elWidget_Tree::ResetRootNode() noexcept
{
    this->rootNode =
        entry_t( elWidget_Button_ptr(), this->Create< elWidget_base >(),
                 std::make_shared< std::vector< entry_t > >() );
    this->rootNode.subFrame->SetClassName( "tree", true );
    this->rootNode.subFrame->EnforceThemeParentWidget( this->type, true );
    this->rootNode.subFrame->SetStickToParentSize( { 0.0f, -1.0f } );
}

void
elWidget_Tree::ButtonCallback( elWidget_base*                    subFrame,
                               const std::vector< std::string >& root,
                               const std::string&                entry,
                               const std::vector< entry_t >*     sub ) noexcept
{
    subFrame->SetDoRenderWidget( !subFrame->DoRenderWidget() );
    if ( !sub->empty() )
    {
        if ( this->callback.openEntry && subFrame->DoRenderWidget() )
            this->CallCallable( [this, root, entry]
                                { this->callback.openEntry( root, entry ); } );
        else if ( this->callback.closeEntry && !subFrame->DoRenderWidget() )
            this->CallCallable( [this, root, entry]
                                { this->callback.closeEntry( root, entry ); } );
    }
    // needs to be below sub->empty since a callback is allowed via clear
    // or remove entry to remove the sub pointer
    if ( this->callback.clickOnEntry )
        this->CallCallable( [this, root, entry]
                            { this->callback.clickOnEntry( root, entry ); } );

    this->runOnce.Call( _Method_Reshape_ );
}

elWidget_Button_ptr
elWidget_Tree::CreateButton( entry_t& node, elWidget_base* subFrame,
                             const std::string&                theme,
                             const std::vector< std::string >& root,
                             const std::string&                text,
                             const std::vector< entry_t >*     sub ) noexcept
{
    auto button = node.subFrame->Create< elWidget_Button >();
    button->EnforceThemeParentWidget( this->type, true );
    button->SetClassName( theme, true );

    button->SetStickToParentSize( { 0.0f, -1.0f } );
    button->textLabel->SetPositionPointOfOrigin( POSITION_CENTER_LEFT );
    button->textLabel->SetPosition( { 10.0f, 0.0f } );
    button->SetCallback( [this, subFrame, root, text, sub]()
                         { this->ButtonCallback( subFrame, root, text, sub ); },
                         STATE_CLICKED, CallCondition::OnEnter );
    button->SetText( text );
    return button;
}

void
elWidget_Tree::AddEntries(
    const std::vector< std::string > root,
    const std::vector< std::string > listOfEntries ) noexcept
{
    this->runOnce.Call( _Method_Reshape_ );
    auto node = this->GetEntry( root );
    if ( !node->sub || !node->subFrame ) return;

    for ( auto entry : listOfEntries )
    {
        if ( !this->doAllowDoubleEntries )
        {
            auto entryPath = root;
            entryPath.emplace_back( entry );
            auto existingEntry = this->GetEntry( entryPath );
            if ( existingEntry && existingEntry->base ) continue;
        }

        auto subFrame = node->subFrame->Create< elWidget_base >();
        subFrame->EnforceThemeParentWidget( this->type, true );
        subFrame->SetClassName( "tree", true );
        subFrame->SetStickToParentSize( { 20.0f, -1.0f } );
        subFrame->SetPositionPointOfOrigin( POSITION_TOP_RIGHT );
        subFrame->SetDoRenderWidget( false );

        auto sub = std::make_shared< std::vector< entry_t > >();

        node->sub->emplace_back( this->CreateButton( *node, subFrame.Get(),
                                                     BUTTON_CLASS_SUBTREE, root,
                                                     entry, sub.get() ),
                                 std::move( subFrame ), sub );
    }
}


void
elWidget_Tree::Clear() noexcept
{
    this->RemoveEntry( {} );
}

void
elWidget_Tree::RemoveEntry( const std::vector< std::string > path ) noexcept
{
    this->runOnce.Call( _Method_Reshape_ );

    if ( path.empty() )
    {
        this->ResetRootNode();
        return;
    }

    entry_t* currentNode  = &this->rootNode;
    entry_t* previousNode = &this->rootNode;

    for ( auto entry : path )
    {
        bool hasFound = false;
        for ( auto& subNode : *currentNode->sub )
        {
            if ( subNode.base->GetText() == entry )
            {
                previousNode = currentNode;
                currentNode  = &subNode;
                hasFound     = true;
                break;
            }
        }
        if ( !hasFound ) return;
    }

    for ( size_t i = 0, limit = previousNode->sub->size(); i < limit; ++i )
        if ( ( *previousNode->sub )[i].base->GetText() == path.back() )
        {
            previousNode->sub->erase( previousNode->sub->begin() +
                                      static_cast< long >( i ) );
            break;
        }

    // we need to use RemoveThis instead of Remove since this can be called
    // during an event loop while iterating through all widgets
    currentNode->base->RemoveThis();
    currentNode->subFrame->RemoveThis();
}

elWidget_Tree::entry_t*
elWidget_Tree::GetEntry( const std::vector< std::string > path ) noexcept
{
    entry_t* currentNode = &this->rootNode;

    for ( auto entry : path )
    {
        bool entryFound = false;
        for ( auto& subNode : *currentNode->sub )
        {
            if ( subNode.base->GetText() == entry )
            {
                currentNode = &subNode;
                entryFound  = true;
                break;
            }
        }
        if ( !entryFound ) return nullptr;
    }

    return currentNode;
}

void
elWidget_Tree::ReshapeEntry( entry_t& entry, const glm::vec2& offset ) noexcept
{
    if ( entry.base && entry.sub->empty() )
    {
        if ( entry.base->GetClassName() != BUTTON_CLASS_ENTRY )
            entry.base->SetClassName( BUTTON_CLASS_ENTRY, false );
    }
    else if ( entry.base && !entry.sub->empty() )
    {
        if ( entry.base->GetClassName() != BUTTON_CLASS_SUBTREE )
            entry.base->SetClassName( BUTTON_CLASS_SUBTREE, false );
    }

    glm::vec2 newOffset{ 0.0f, 0.0f };
    if ( entry.subFrame.Get() != this->rootNode.subFrame.Get() )
        entry.base->SetPosition( offset );

    for ( auto& subEntry : *entry.sub )
    {
        this->ReshapeEntry( subEntry, newOffset );
        newOffset.y += subEntry.base->GetSize().y;
        if ( subEntry.subFrame->DoRenderWidget() )
            newOffset.y += subEntry.subFrame->GetSize().y;
    }

    if ( entry.subFrame.Get() != this->rootNode.subFrame.Get() )
    {
        glm::vec2 subFrameOffset{ 0.0f, entry.base->GetSize().y };
        entry.subFrame->SetPosition( offset + subFrameOffset );
    }
    entry.subFrame->SetSize( { 0.0f, newOffset.y } );
}

void
elWidget_Tree::Reshape()
{
    elWidget_base::Reshape();
    this->ReshapeEntry( this->rootNode );
    elWidget_base::Reshape();
}

void
elWidget_Tree::SetClickOnEntryCallback( const treeCallback_t& v ) noexcept
{
    this->callback.clickOnEntry = v;
}

void
elWidget_Tree::SetEntryOpenCallback( const treeCallback_t& v ) noexcept
{
    this->callback.openEntry = v;
}

void
elWidget_Tree::SetEntryCloseCallback( const treeCallback_t& v ) noexcept
{
    this->callback.closeEntry = v;
}

void
elWidget_Tree::AllowDoubleEntries( const bool v ) noexcept
{
    this->doAllowDoubleEntries = v;
}

void
elWidget_Tree::Lua_AddEntries( const std::string& separator, // NOLINT
                               const std::string& rootPath,
                               const std::string& entryList ) noexcept
{
    this->AddEntries( this->SeparatedStringToVector( separator, rootPath ),
                      this->SeparatedStringToVector( separator, entryList ) );
}

void
elWidget_Tree::Lua_RemoveEntry( const std::string& separator, // NOLINT
                                const std::string& path ) noexcept
{
    this->RemoveEntry( this->SeparatedStringToVector( separator, path ) );
}

elWidget_Button*
elWidget_Tree::Lua_GetEntry( const std::string& separator, // NOLINT
                             const std::string& path ) noexcept
{
    return this->GetEntry( this->SeparatedStringToVector( separator, path ) )
        ->base.Get();
}


} // namespace GuiGL
} // namespace el3D
