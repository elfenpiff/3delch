#include "GuiGL/elWidget_Graph.hpp"

#include "HighGL/elShaderTexture.hpp"

namespace el3D
{
namespace GuiGL
{
const std::vector< std::string > elWidget_Graph::GRAPH_UNIFORM_STRUCTURE = {
    "graph_t.values", "graph_t.numberOfValues", "graph_t.yRange" };


elWidget_Graph::elWidget_Graph( const constructor_t& constructor )
    : elWidget_ShaderAnimation( constructor )
{
    this->graphDataCallback = this->CreateCallback();
}

elWidget_Graph::~elWidget_Graph()
{
}

bb::elExpected< elWidget_ShaderAnimation::Error >
elWidget_Graph::SetUpGraphShader( const std::string& shaderFile,
                                  const std::string& shaderGroup )
{
    auto setup = this->SetUp( shaderFile, shaderGroup, true );
    if ( setup.HasError() ) return bb::Error( setup.GetError() );

    auto ubo = OpenGL::elUniformBufferObject::Create(
        "graph_t", elWidget_Graph::GRAPH_UNIFORM_STRUCTURE,
        &this->GetShaderTexture()->GetShader() );
    if ( ubo.HasError() )
    {
        LOG_ERROR( 0 ) << "uniform buffer object graph_t could not be created";
        return bb::Error( Error::UnableToCreateUniformBufferObject );
    }

    this->graphUniformBuffer = std::move( ubo.GetValue() );

    this->GetShaderTexture()->SetPreDrawFunction(
        { [this]
          {
              this->graphDataCallback->Call();
              this->graphUniformBuffer->BindToShader();
              this->animationUniformBuffer->BindToShader();
          } } );

    this->SetDoInfinitLoop( true );
    this->Start();

    this->UpdateGraphData();
    return bb::Success<>();
}

void
elWidget_Graph::SetGraphData( const std::vector< float >& data,
                              const glm::vec2             yDrawRange )
{
    this->graphData       = data;
    this->graphDataYRange = yDrawRange;
    this->UpdateGraphData();
}

void
elWidget_Graph::SetGraphDataCallback( const std::function< void() >& v )
{
    this->graphDataCallback->Set( v );
}

void
elWidget_Graph::SetGraphDataYDrawRange( const glm::vec2 yDrawRange )
{
    this->graphDataYRange = yDrawRange;
}

void
elWidget_Graph::AddGraphDataValue( const float data )
{
    this->graphData.push_back( data );
}

void
elWidget_Graph::ResetGraphData()
{
    this->graphData.clear();
    this->graphDataYRange = glm::vec2( 0.0f );
}

void
elWidget_Graph::UpdateGraphData()
{
    if ( !this->graphUniformBuffer || this->graphData.empty() ) return;

    int dataSize = std::clamp( static_cast< int >( this->graphData.size() ), 0,
                               static_cast< int >( this->maximumGraphValues ) );

    this->graphUniformBuffer->SetVariableData(
        0, 0, this->graphData.data(),
        static_cast< size_t >( dataSize ) * sizeof( float ) );
    this->graphUniformBuffer->SetVariableData( 0, 1, &dataSize, sizeof( int ) );

    if ( this->graphDataYRange == glm::vec2{ 0.0f, 0.0f } )
    {
        float minElement = *std::min_element(
            this->graphData.begin(), this->graphData.begin() + dataSize );
        float maxElement = *std::max_element(
            this->graphData.begin(), this->graphData.begin() + dataSize );
        this->graphDataYRange = glm::vec2{ minElement, maxElement };
    }

    this->graphUniformBuffer->SetVariableData(
        0, 2, glm::value_ptr( this->graphDataYRange ), 2 * sizeof( GLfloat ) );
}


bb::elExpected< elWidget_ShaderAnimation::Error >
elWidget_Graph::SetUp(
    const std::string& shaderFile, const std::string& shaderGroup,
    const bool                                       doPerformDynamicResize,
    const std::vector< OpenGL::textureProperties_t > outputTextureProperties,
    const GLsizei sizeX, const GLsizei sizeY, const bool isFlipFlop ) noexcept
{
    return elWidget_ShaderAnimation::SetUp(
        shaderFile, shaderGroup, doPerformDynamicResize,
        outputTextureProperties, sizeX, sizeY, isFlipFlop );
}

void
elWidget_Graph::SetTextureUniformBlockData( const size_t arrayIndex,
                                            const size_t varIndex,
                                            const void*  varData,
                                            const size_t dataSize ) noexcept
{
    elWidget_ShaderAnimation::SetTextureUniformBlockData( arrayIndex, varIndex,
                                                          varData, dataSize );
}

void
elWidget_Graph::SetDoInfinitLoop( const bool v ) noexcept
{
    elWidget_ShaderAnimation::SetDoInfinitLoop( v );
}

void
elWidget_Graph::Start() noexcept
{
    elWidget_ShaderAnimation::Start();
}

void
elWidget_Graph::ReverseStart() noexcept
{
    elWidget_ShaderAnimation::ReverseStart();
}

void
elWidget_Graph::Stop() noexcept
{
    elWidget_ShaderAnimation::Stop();
}


} // namespace GuiGL
} // namespace el3D
