#include "GuiGL/elWidget_KeyValuePair.hpp"

#include "GuiGL/elWidget_Label.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_KeyValuePair::elWidget_KeyValuePair(
    const constructor_t &constructor ) noexcept
    : elWidget_base( constructor )
{
    this->key   = this->Create< elWidget_Label >( "Key" );
    this->value = this->Create< elWidget_Label >( "Value" );
    this->value->SetPosition( { 100.0, 0.0 } );
}

void
elWidget_KeyValuePair::SetKeyText( const std::string &value ) noexcept
{
    this->key->SetText( value );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_KeyValuePair::SetValueText( const std::string &value ) noexcept
{
    this->value->SetText( value );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_KeyValuePair::SetValueOffset( const glm::vec2 &offset ) noexcept
{
    this->value->SetPosition( offset );
    this->runOnce.Call( _Method_Reshape_ );
}

uint64_t
elWidget_KeyValuePair::GetFontHeight() const noexcept
{
    return this->value->GetFontHeight();
}


void
elWidget_KeyValuePair::Reshape()
{
    this->SetSize( this->GetContentDimension() );
    elWidget_base::Reshape();
}


} // namespace GuiGL
} // namespace el3D
