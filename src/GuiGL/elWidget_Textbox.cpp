#include "GuiGL/elWidget_Textbox.hpp"

#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_Selection.hpp"
#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_TextCursor.hpp"

#include <algorithm>
#include <limits>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Textbox::elWidget_Textbox( const constructor_t& constructor )
    : elWidget_Window( constructor ),
      cursor( *this, this->Create< elWidget_TextCursor >(),
              this->Create< elWidget_TextCursor >() ),
      selection( *this ), editEvent( this->CreateEvent() ),
      mouseClickEvent( this->CreateEventFor( UBO_EVENT_COLOR_INNER ) )
{
    this->SetIsEditable( true );
    this->editEvent->SetCallback( [this]( const SDL_Event e ) {
        if ( ( e.key.type == SDL_KEYDOWN || e.type == SDL_TEXTINPUT ) &&
             !this->GetStickyExclusiveEvent().IsRegistered() &&
             this->GetState() == STATE_SELECTED )
        {
            this->EditEventCallback( e );
        }
    } );

    this->mouseClickEvent->SetCallback( [this]( const SDL_Event e ) {
        if ( ( e.button.button == SDL_BUTTON_LEFT ||
               e.button.button == SDL_BUTTON_MIDDLE ) &&
             !this->GetStickyExclusiveEvent().IsRegistered() )
            this->MouseEventCallback( e );
    } );
}

elWidget_Textbox::~elWidget_Textbox()
{
    this->ClearText();
}

void
elWidget_Textbox::SetState( const widgetStates_t state ) noexcept
{
    elWidget_Window::SetState( state );

    if ( this->GetState() == STATE_SELECTED && this->isEditable &&
         !this->editEvent->IsRegistered() )
        this->editEvent->Register();
    else if ( this->GetState() != STATE_SELECTED &&
              this->editEvent->IsRegistered() )
        this->editEvent->Unregister();
}

glm::ivec2
elWidget_Textbox::MousePositionToTextPosition( const glm::vec2& mousePosition )
{
    glm::vec2 currentMousePosition =
        mousePosition - this->GetAbsolutPosition() -
        glm::vec2( this->GetBorderSize()[POSITION_LEFT],
                   this->GetBorderSize()[POSITION_TOP] );

    glm::vec2 scrollPosition{ this->slider.currentRelativePosition[0],
                              this->slider.currentRelativePosition[1] };

    glm::vec2 offset = scrollPosition *
                       ( this->GetContentDimension() - this->GetContentSize() );
    offset = { std::max( offset.x, 0.0f ), std::max( offset.y, 0.0f ) };

    for ( uint64_t k = 1, limit = this->lines.size(); k < limit; ++k )
    {
        auto& line     = this->lines[k - 1];
        auto& nextLine = this->lines[k];
        if ( nextLine.GetPosition().y >= currentMousePosition.y + offset.y )
        {
            uint64_t i = 1;
            for ( uint64_t iLimit = line.GetText().size(); i < iLimit; ++i )
            {
                if ( line.GetTextSize( line.GetText().substr( 0, i ) ) >=
                     static_cast< uint64_t >( currentMousePosition.x +
                                              offset.x ) )
                    break;
            }

            return { i - 1, k - 1 };
        }
    }

    return { 0, this->lines.size() };
}

void
elWidget_Textbox::MouseEventCallback( const SDL_Event e ) noexcept
{
    switch ( e.button.button )
    {
        case SDL_BUTTON_LEFT:
        {
            glm::ivec2 currentMouseTextPosition =
                this->MousePositionToTextPosition(
                    this->mouseCursor->GetPosition() );

            if ( e.type == SDL_MOUSEBUTTONUP &&
                 currentMouseTextPosition == this->mouseClickPosition )
            {
                this->selection.DoActivateSelection( false );
                this->cursor.MoveTo( currentMouseTextPosition );
            }
            else if ( e.type == SDL_MOUSEBUTTONDOWN )
            {
                this->selection.DoActivateSelection( false );
                this->mouseClickPosition = currentMouseTextPosition;
                this->cursor.MoveTo( this->mouseClickPosition );
                this->selection.DoActivateSelection( true );
                this->selection.AdjustSelection();
            }
            else
            {
                this->cursor.MoveTo( currentMouseTextPosition );
                this->selection.AdjustSelection();
            }
            break;
        }
    }
}

void
elWidget_Textbox::EditEventCallback( const SDL_Event e ) noexcept
{
    bool shiftKey = ( e.key.keysym.mod & KMOD_SHIFT );

    switch ( e.key.keysym.sym )
    {
        case SDLK_RIGHT:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveRight();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_LEFT:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveLeft();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_UP:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveUp();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_DOWN:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveDown();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_END:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveCursorToLineEnd();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_HOME:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveCursorToLineBegin();
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_PAGEUP:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveUp( this->property.pageJumpSize );
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_PAGEDOWN:
        {
            this->selection.DoActivateSelection( shiftKey );
            this->cursor.MoveDown( this->property.pageJumpSize );
            if ( shiftKey ) this->selection.AdjustSelection();
            break;
        }
        case SDLK_DELETE:
        {
            if ( this->selection.IsSelectionModeActive() )
            {
                this->RemoveSelectedText();
            }
            else
            {
                auto  pos     = this->cursor.GetCurrentPosition();
                auto& line    = this->lines[static_cast< uint64_t >( pos.y )];
                auto  content = line.GetLabelWidget().GetText();
                if ( content.size() > static_cast< uint64_t >( pos.x ) )
                {
                    content.erase( static_cast< uint64_t >( pos.x ), 1 );
                }
                else if ( this->lines.size() >
                          static_cast< uint64_t >( pos.y + 1 ) )
                {
                    auto& nextLine =
                        this->lines[static_cast< uint64_t >( pos.y + 1 )];
                    content += nextLine.GetLabelWidget().GetText();
                    this->lines.erase( this->lines.begin() + pos.y + 1 );
                    this->AdjustLinePosition(
                        static_cast< uint64_t >( pos.y ) );
                }

                line.GetLabelWidget().SetText( content );
            }
            break;
        }
        case SDLK_BACKSPACE:
        {
            if ( this->selection.IsSelectionModeActive() )
            {
                this->RemoveSelectedText();
            }
            else
            {
                auto  pos     = this->cursor.GetCurrentPosition();
                auto& line    = this->lines[static_cast< uint64_t >( pos.y )];
                auto  content = line.GetLabelWidget().GetText();
                if ( pos.x > 0 )
                {
                    content.erase( static_cast< uint64_t >( pos.x - 1 ), 1 );
                    line.GetLabelWidget().SetText( content );
                    this->cursor.MoveLeft();
                }
                else if ( pos.y > 0 && pos.x == 0 )
                {
                    auto& previousLine =
                        this->lines[static_cast< uint64_t >( pos.y - 1 )];
                    content = previousLine.GetLabelWidget().GetText() + content;
                    uint64_t width =
                        previousLine.GetLabelWidget().GetText().size();
                    this->lines.erase( this->lines.begin() + pos.y );
                    this->AdjustLinePosition(
                        static_cast< uint64_t >( pos.y - 1 ) );
                    previousLine.GetLabelWidget().SetText( content );
                    this->cursor.MoveTo( { width, pos.y - 1 } );
                }
            }
            break;
        }
        case SDLK_RETURN:
        {
            this->RemoveSelectedText();
            auto pos     = this->cursor.GetCurrentPosition();
            auto content = this->lines[static_cast< uint64_t >( pos.y )]
                               .GetLabelWidget()
                               .GetText();

            this->InsertLine(
                content.substr( 0, static_cast< uint64_t >( pos.x ) ),
                static_cast< uint64_t >( pos.y ) );
            this->lines[static_cast< uint64_t >( pos.y + 1 )].SetText(
                content.substr( static_cast< uint64_t >( pos.x ) ) );
            this->cursor.MoveTo( { 0, pos.y + 1 } );

            break;
        }
        case SDLK_TAB:
        {
            this->RemoveSelectedText();
            auto        pos = this->cursor.GetCurrentPosition();
            std::string tab( this->tabWidth, ' ' );
            this->InsertTextInLine( static_cast< uint64_t >( pos.y ),
                                    static_cast< uint64_t >( pos.x ), tab );
            this->cursor.MoveTo( pos + glm::ivec2( this->tabWidth, 0 ) );
            break;
        }
        default:
        {
            if ( e.type == SDL_TEXTINPUT )
            {
                this->RemoveSelectedText();
                auto pos = this->cursor.GetCurrentPosition();
                this->InsertTextInLine( static_cast< uint64_t >( pos.y ),
                                        static_cast< uint64_t >( pos.x ),
                                        e.text.text );
                this->cursor.MoveRight();
            }
            break;
        }
    }
}

void
elWidget_Textbox::InsertTextInLine( const uint64_t     line,
                                    const uint64_t     positionInLine,
                                    const std::string& text ) noexcept
{
    auto content = this->lines[line].GetLabelWidget().GetText();
    content.insert( positionInLine, text );
    this->lines[line].GetLabelWidget().SetText( content );
}

uint64_t
elWidget_Textbox::GetNumberOfLines() const noexcept
{
    return this->lines.size();
}

void
elWidget_Textbox::ClearText() noexcept
{
    this->lines.clear();
}

std::vector< std::string >
elWidget_Textbox::GetText() const noexcept
{
    std::vector< std::string > returnValue;
    for ( auto& line : this->lines )
        returnValue.emplace_back( line.GetText() );
    return returnValue;
}

std::string
elWidget_Textbox::GetLine( const uint64_t line ) const noexcept
{
    return this->lines[line].GetText();
}

void
elWidget_Textbox::ModifyLine( const std::string& v,
                              const uint64_t     line ) noexcept
{
    this->lines[line].SetText( v );
}

elWidget_base&
elWidget_Textbox::GetLineWidget( const uint64_t line ) noexcept
{
    return this->lines[line].GetLabelWidget();
}

void
elWidget_Textbox::InsertLine( const std::string& v,
                              const uint64_t     line ) noexcept
{
    if ( line == TEXT_END )
    {
        this->lines.emplace_back( line_t( this->Create< elWidget_Label >() ) );
        this->lines.back().SetText( v );
        this->AdjustLinePosition( this->lines.size() - 1 );
    }
    else
    {
        this->lines.insert( this->lines.begin() + static_cast< long >( line ),
                            line_t( this->Create< elWidget_Label >() ) );
        this->lines[line].SetText( v );
        this->AdjustLinePosition( line );
    }
}

void
elWidget_Textbox::RemoveLine( const uint64_t line ) noexcept
{
    if ( line >= this->lines.size() ) return;

    this->lines.erase( this->lines.begin() + static_cast< long >( line ) );
    this->AdjustLinePosition( line );
}

void
elWidget_Textbox::SetText( const std::vector< std::string >& v ) noexcept
{
    this->ClearText();
    for ( auto line : v )
    {
        this->lines.emplace_back( this->Create< elWidget_Label >() );
        this->lines.back().SetText( line );
    }
    this->AdjustLinePosition( 0 );
}

void
elWidget_Textbox::AdjustLinePosition( const uint64_t start ) noexcept
{
    if ( start >= this->lines.size() ) return;

    uint64_t height = ( start > 0 )
                          ? static_cast< uint64_t >(
                                this->lines[start - 1].GetPosition().y ) +
                                this->lines[start - 1].GetFontHeight() +
                                this->property.lineSpacing
                          : this->property.lineSpacing;

    for ( uint64_t k = start, limit = this->lines.size(); k < limit; ++k )
    {
        this->lines[k].SetPosition( { this->property.spaceToLeft, height } );
        height += this->lines[k].GetFontHeight() + this->property.lineSpacing;
    }

    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Textbox::SetIsEditable( const bool v ) noexcept
{
    if ( this->isEditable == v ) return;
    this->isEditable = v;

    if ( this->isEditable )
    {
        if ( this->lines.empty() ) this->InsertLine( "", 0 );

        this->cursor.GetCursor().SetDoRenderWidget( true );
        this->cursor.UpdateDimensions();
    }
    else
    {
        this->cursor.GetCursor().SetDoRenderWidget( false );
    }
}

void
elWidget_Textbox::RemoveSelectedText() noexcept
{
    if ( this->selection.IsSelectionModeActive() )
    {
        auto selectionRange = this->selection.GetSelection();

        if ( selectionRange.begin.y == selectionRange.end.y )
        {
            auto& line =
                this->lines[static_cast< uint64_t >( selectionRange.begin.y )];

            auto content = line.GetLabelWidget().GetText();
            content.erase( static_cast< uint64_t >( selectionRange.begin.x ),
                           static_cast< uint64_t >( selectionRange.end.x + 1 -
                                                    selectionRange.begin.x ) );
            line.GetLabelWidget().SetText( content );
        }
        else
        {
            auto& startLine =
                this->lines[static_cast< uint64_t >( selectionRange.begin.y )];
            std::string content = startLine.GetLabelWidget().GetText();
            content             = content.substr(
                0, static_cast< uint64_t >( selectionRange.begin.x ) );

            auto& endLine =
                this->lines[static_cast< uint64_t >( selectionRange.end.y )];
            if ( !endLine.GetLabelWidget().GetText().empty() )
            {
                auto text = endLine.GetLabelWidget().GetText();
                if ( text.size() >
                     static_cast< uint64_t >( selectionRange.end.x ) )
                {
                    content += endLine.GetLabelWidget().GetText().substr(
                        static_cast< uint64_t >( selectionRange.end.x + 1 ) );
                }
            }
            startLine.GetLabelWidget().SetText( content );

            if ( this->lines.size() >
                 static_cast< uint64_t >( selectionRange.end.y ) )
                this->lines.erase(
                    this->lines.begin() + selectionRange.begin.y + 1,
                    this->lines.begin() + selectionRange.end.y + 1 );
            else
                this->lines.erase( this->lines.begin() +
                                       selectionRange.begin.y + 1,
                                   this->lines.begin() + selectionRange.end.y );
        }

        this->AdjustLinePosition(
            static_cast< uint64_t >( selectionRange.begin.y ) );
        this->selection.DoActivateSelection( false );
        this->cursor.MoveTo( selectionRange.begin );
    }
}

void
elWidget_Textbox::AutoScrollToCurrentPosition() noexcept
{
    this->UpdateWindowDimensions();
    this->UpdateSliderOffsetAndSizeRemainder();

    glm::vec2 cursorPosition = this->cursor.GetGhostCursor().GetPosition();
    glm::vec2 currentScrollPosition{ this->GetFinalSliderPosition() };

    glm::vec2 scrollStepSize = { 0.0f, 0.0f };
    auto      lineContent    = this->lines[static_cast< uint64_t >(
                                       this->cursor.GetCurrentPosition().y )]
                           .GetText();
    std::string character = " ";
    if ( lineContent.size() >
         static_cast< uint64_t >( this->cursor.GetCurrentPosition().x + 1 ) )
    {
        character = lineContent.substr(
            static_cast< uint64_t >( this->cursor.GetCurrentPosition().x + 1 ),
            1 );
    }

    scrollStepSize.x = static_cast< float >(
        this->lines[static_cast< uint64_t >(
                        this->cursor.GetCurrentPosition().y )]
            .GetLabelWidget()
            .font->GetTextSize( character )
            .width );

    uint64_t numberOfLine =
        ( this->lines.size() >
          static_cast< uint64_t >( this->cursor.GetCurrentPosition().y + 1 ) )
            ? static_cast< uint64_t >( this->cursor.GetCurrentPosition().y + 1 )
            : static_cast< uint64_t >( this->cursor.GetCurrentPosition().y );

    scrollStepSize.y =
        static_cast< float >( this->lines[numberOfLine].GetFontHeight() );

    glm::vec2 unViewable = this->GetContentDimension() - this->GetContentSize();

    // vertical scrolling
    float verticalScrolling = currentScrollPosition.y;
    if ( unViewable.y != 0 )
    {
        float scrollUp =
            std::max( 0.0f, ( cursorPosition.y -
                              this->scrollStart.y * scrollStepSize.y ) /
                                unViewable.y );
        float scrollDown = std::max(
            0.0f, ( cursorPosition.y - this->GetContentSize().y +
                    ( this->scrollStart.y + 1.0f ) * scrollStepSize.y ) /
                      unViewable.y );

        if ( scrollUp < currentScrollPosition.y )
            verticalScrolling = scrollUp;
        else if ( scrollDown > currentScrollPosition.y )
            verticalScrolling = scrollDown;
    }

    // horizontal scrolling
    float horizontalScrolling = currentScrollPosition.x;
    if ( unViewable.x != 0 )
    {
        float scrollLeft =
            std::max( 0.0f, ( cursorPosition.x -
                              this->scrollStart.x * scrollStepSize.x ) /
                                unViewable.x );
        float scrollRight =
            std::max( 0.0f, ( cursorPosition.x - this->GetContentSize().x +
                              ( scrollStart.x + 1.0f ) * scrollStepSize.x ) /
                                unViewable.x );

        if ( scrollLeft < currentScrollPosition.x )
            horizontalScrolling = scrollLeft;
        else if ( scrollRight > currentScrollPosition.x )
            horizontalScrolling = scrollRight;
    }

    this->ScrollTo( { horizontalScrolling, verticalScrolling } );
}


/////////////////
/// line_t
///////////////////

elWidget_Textbox::line_t::line_t( elWidget_Label_ptr&& label ) noexcept
    : label( std::move( label ) ),
      selection( this->label->Create< elWidget_Selection >() )
{
    this->selection->SetStickToParentSize( { -1, 0 } );
    this->selection->SetDoRenderWidget( false );
}

elWidget_Textbox::line_t::line_t( line_t&& rhs ) noexcept
{
    *this = std::move( rhs );
}

elWidget_Textbox::line_t&
elWidget_Textbox::line_t::operator=( line_t&& rhs ) noexcept
{
    if ( this != &rhs )
    {
        // has to be done explicitly in reverse order since c++ uses normal
        // order in move which is used in vec.erase()
        this->selection = std::move( rhs.selection );
        this->label     = std::move( rhs.label );
    }
    return *this;
}

void
elWidget_Textbox::line_t::SetPosition( const glm::vec2& v ) noexcept
{
    this->label->SetPosition( v );
}

glm::vec2
elWidget_Textbox::line_t::GetPosition() const noexcept
{
    return this->label->GetPosition();
}

uint64_t
elWidget_Textbox::line_t::GetFontHeight() const noexcept
{
    return this->label->GetFontHeight();
}

void
elWidget_Textbox::line_t::SetText( const std::string& v ) noexcept
{
    this->label->SetText( v );
}

std::string
elWidget_Textbox::line_t::GetText() const noexcept
{
    return this->label->GetText();
}

elWidget_Label&
elWidget_Textbox::line_t::GetLabelWidget() noexcept
{
    return *this->label;
}

elWidget_Selection&
elWidget_Textbox::line_t::GetSelectionWidget() noexcept
{
    return *this->selection;
}

uint64_t
elWidget_Textbox::line_t::GetTextSize( const std::string& v ) const noexcept
{
    return this->label->font->GetTextSize( v ).width;
}

///////////////////
/// selection_t
//////////////////

elWidget_Textbox::selection_t::selection_t( elWidget_Textbox& me ) noexcept
    : me( me )
{
}

void
elWidget_Textbox::selection_t::DoActivateSelection( const bool v ) noexcept
{
    if ( v )
        this->BeginSelection();
    else
        this->EndSelection();
}

void
elWidget_Textbox::selection_t::BeginSelection() noexcept
{
    if ( !this->isSelectionModeActive )
    {
        this->startSelection         = me.cursor.GetCurrentPosition();
        this->isSelectionModeActive  = true;
        this->currentSelection.begin = this->startSelection;
        this->currentSelection.end   = this->startSelection;
    }
}

void
elWidget_Textbox::selection_t::AdjustSelection() noexcept
{
    auto pos                = me.cursor.GetCurrentPosition();
    this->previousSelection = this->currentSelection;

    this->isSelectingFromTopToBottom = ( pos.y > this->startSelection.y ||
                                         ( pos.y == this->startSelection.y &&
                                           pos.x >= this->startSelection.x ) );

    if ( this->isSelectingFromTopToBottom )
    {
        this->currentSelection.begin = this->startSelection;
        this->currentSelection.end   = pos;
    }
    else
    {
        this->currentSelection.begin = pos;
        this->currentSelection.end   = this->startSelection;
    }

    this->UpdateSelectionDimensions();
}

void
elWidget_Textbox::selection_t::EndSelection() noexcept
{
    this->isSelectionModeActive = false;
    this->previousSelection     = this->currentSelection;
    this->currentSelection      = range_t();
    this->UpdateSelectionDimensions();
    this->previousSelection = this->currentSelection;
}

void
elWidget_Textbox::selection_t::UpdateSelectionDimensions() noexcept
{
    for ( int k     = this->previousSelection.begin.y,
              limit = static_cast< int >( me.lines.size() );
          0 <= k && k <= this->previousSelection.end.y && k < limit; ++k )
    {
        if ( !( this->currentSelection.begin.y <= k &&
                k <= this->currentSelection.end.y ) )
        {
            auto& line = me.lines[static_cast< uint64_t >( k )];
            line.GetSelectionWidget().SetDoRenderWidget( false );
        }
    }

    for ( int k     = this->currentSelection.begin.y,
              limit = static_cast< int >( me.lines.size() );
          0 <= k && k <= this->currentSelection.end.y && k < limit; ++k )
    {
        auto&    line   = me.lines[static_cast< uint64_t >( k )];
        uint64_t pos    = 0;
        uint64_t length = 0;

        if ( this->currentSelection.begin.y == this->currentSelection.end.y )
        {
            pos    = line.GetTextSize( line.GetText().substr(
                0,
                static_cast< uint64_t >( this->currentSelection.begin.x ) ) );
            length = line.GetTextSize( line.GetText().substr(
                static_cast< uint64_t >( this->currentSelection.begin.x ),
                static_cast< uint64_t >( this->currentSelection.end.x + 1 -
                                         this->currentSelection.begin.x ) ) );
        }
        else if ( k == this->currentSelection.begin.y )
        {
            pos    = line.GetTextSize( line.GetText().substr(
                0,
                static_cast< uint64_t >( this->currentSelection.begin.x ) ) );
            length = line.GetTextSize( line.GetText().substr(
                static_cast< uint64_t >( this->currentSelection.begin.x ) ) );
        }
        else if ( k == this->currentSelection.end.y )
        {
            pos           = 0;
            auto     text = line.GetText();
            uint64_t endPosition =
                ( text.size() >
                  static_cast< uint64_t >( this->currentSelection.end.x ) )
                    ? static_cast< uint64_t >( this->currentSelection.end.x ) +
                          1
                    : static_cast< uint64_t >( this->currentSelection.end.x );

            length =
                line.GetTextSize( line.GetText().substr( 0, endPosition ) );
        }
        else
        {
            pos    = 0;
            length = line.GetTextSize(
                ( line.GetText().size() > 0 ) ? line.GetText() : " " );
        }

        if ( !line.GetSelectionWidget().DoRenderWidget() )
        {
            line.GetSelectionWidget().SetDoRenderWidget( true );
            line.GetSelectionWidget().SetPosition( { pos, 0 } );
            line.GetSelectionWidget().SetSize( { length, 0 } );
        }
        else
        {
            line.GetSelectionWidget().MoveTo( { pos, 0 } );
            line.GetSelectionWidget().ResizeTo( { length, 0 } );
        }
    }
}

elWidget_Textbox::range_t
elWidget_Textbox::selection_t::GetSelection() const noexcept
{
    return this->currentSelection;
}

bool
elWidget_Textbox::selection_t::IsSelectionModeActive() const noexcept
{
    return this->isSelectionModeActive;
}

/////
// cursor_t
/////

elWidget_Textbox::cursor_t::cursor_t( elWidget_Textbox&         me,
                                      elWidget_TextCursor_ptr&& widget,
                                      elWidget_TextCursor_ptr&& ghostWidget )
    : me( me ), widget( std::move( widget ) ),
      ghostWidget( std::move( ghostWidget ) )
{
    this->widget->SetPosition(
        { me.property.spaceToLeft, me.property.lineSpacing } );
    this->ghostWidget->SetPosition(
        { me.property.spaceToLeft, me.property.lineSpacing } );
    this->ghostWidget->SetIsGhostWidget( true );
}

void
elWidget_Textbox::cursor_t::UpdateDimensions() noexcept
{
    this->ClampPositionToText();
    auto& line = me.lines[static_cast< uint64_t >( this->position.y )];

    uint64_t height = line.GetFontHeight();
    uint64_t width =
        ( line.GetText().empty() ||
          position.x == static_cast< int >( line.GetText().size() ) )
            ? line.GetTextSize( " " )
            : line.GetTextSize( line.GetText().substr(
                  static_cast< uint64_t >( position.x ), 1 ) );

    this->ghostWidget->SetSize( { width, height } );
    this->widget->ResizeTo( { width, height } );

    uint64_t widgetXPosition = static_cast< uint64_t >( line.GetPosition().x );
    uint64_t widgetYPosition = static_cast< uint64_t >( line.GetPosition().y );
    if ( position.x > 0 )
        widgetXPosition += line.GetTextSize(
            line.GetText().substr( 0, static_cast< uint64_t >( position.x ) ) );

    this->ghostWidget->SetPosition( { widgetXPosition, widgetYPosition } );
    this->widget->MoveTo( { widgetXPosition, widgetYPosition } );

    me.AutoScrollToCurrentPosition();
}

void
elWidget_Textbox::cursor_t::ClampPositionToText() noexcept
{
    if ( this->position.x < 0 && this->position.y > 0 )
    {
        this->position.y--;
        this->position.x = LINE_END;
    }

    this->position.y = std::clamp(
        this->position.y, 0, static_cast< glm::i32 >( me.lines.size() - 1 ) );
    auto& line = me.lines[static_cast< uint64_t >( this->position.y )];

    if ( this->position.x > 0 &&
         this->position.x > static_cast< int >( line.GetText().size() ) &&
         this->position.x != LINE_END )
    {
        this->position.x =
            ( this->position.y == static_cast< int >( me.lines.size() - 1 ) )
                ? LINE_END
                : 0;
        this->position.y++;
        this->position.y =
            std::clamp( this->position.y, 0,
                        static_cast< glm::i32 >( me.lines.size() - 1 ) );
    }

    this->position.x =
        std::clamp( this->position.x, 0,
                    static_cast< glm::i32 >(
                        me.lines[static_cast< uint64_t >( this->position.y )]
                            .GetText()
                            .size() ) );
}

void
elWidget_Textbox::cursor_t::MoveRight() noexcept
{
    this->position.x++;
    this->UpdateDimensions();
}

void
elWidget_Textbox::cursor_t::MoveLeft() noexcept
{
    this->position.x--;
    this->UpdateDimensions();
}

void
elWidget_Textbox::cursor_t::MoveUp( const uint64_t steps ) noexcept
{
    this->position.y -= static_cast< int >( steps );
    this->UpdateDimensions();
}

void
elWidget_Textbox::cursor_t::MoveDown( const uint64_t steps ) noexcept
{
    this->position.y += static_cast< int >( steps );
    this->UpdateDimensions();
}

void
elWidget_Textbox::cursor_t::MoveCursorToLineEnd() noexcept
{
    this->position.x = LINE_END;
    this->UpdateDimensions();
}

void
elWidget_Textbox::cursor_t::MoveCursorToLineBegin() noexcept
{
    this->position.x = 0;
    this->UpdateDimensions();
}

glm::ivec2
elWidget_Textbox::cursor_t::GetCurrentPosition() const noexcept
{
    return this->position;
}

void
elWidget_Textbox::cursor_t::MoveTo( const glm::ivec2& position ) noexcept
{
    this->position = position;
    this->UpdateDimensions();
}

elWidget_TextCursor&
elWidget_Textbox::cursor_t::GetCursor() noexcept
{
    return *this->widget;
}

elWidget_TextCursor&
elWidget_Textbox::cursor_t::GetGhostCursor() noexcept
{
    return *this->ghostWidget;
}

} // namespace GuiGL
} // namespace el3D
