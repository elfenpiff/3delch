#include "GuiGL/elWidget_MouseCursor.hpp"

#include "GuiGL/elWidget_ShaderAnimation.hpp"
#include "GuiGL/elWidget_base.hpp"

static glm::vec2 cursorPosition         = glm::vec2( 0.0, 0.0 );
static glm::vec2 relativeCursorPosition = glm::vec2( 0.0, 0.0 );

namespace el3D
{
namespace GuiGL
{
elWidget_MouseCursor::elWidget_MouseCursor( const constructor_t& constructor )
    : elWidget_ShaderAnimation( constructor ), event( this->CreateEvent() )
{
    this->ThrowIfPropertyIsUnset< NoAnimationShaderFileSet_e >(
        PROPERTY_ANIMATION_SHADER_FILE );

    if ( this->SetUp( this->GetAnimationShaderFile() ).HasError() )
        throw InvalidShader_e( "unable to setup shader animation" );

    this->SetDoInfinitLoop( true );
    this->Start();

    this->event
        ->SetCallback(
            [this]( const SDL_Event& e )
            {
                if ( e.type == SDL_MOUSEMOTION )
                {
                    glm::vec2 position = this->GetPosition();
                    auto      speed    = this->GetAnimationSpeed();
                    if ( speed == 0.0f ) speed = 1.0f;

                    auto newPosition = glm::vec2{
                        position[0] +
                            speed * static_cast< float >( e.motion.xrel ) /
                                GetDPIScaling(),
                        position[1] +
                            speed * static_cast< float >( e.motion.yrel ) /
                                GetDPIScaling() };

                    newPosition[0] = std::clamp(
                        newPosition[0], 0.0f,
                        static_cast< float >( this->GetScreenWidth() ) /
                            GetDPIScaling() );
                    newPosition[1] = std::clamp(
                        newPosition[1], 0.0f,
                        static_cast< float >( this->GetScreenHeight() ) /
                            GetDPIScaling() );

                    relativeCursorPosition =
                        glm::vec2( newPosition[0], newPosition[1] ) -
                        cursorPosition;
                    cursorPosition =
                        glm::vec2( newPosition[0], newPosition[1] );
                    this->SetPosition( newPosition );
                    this->runOnce.Call( _Method_Reshape_ );
                }
            } )
        .Register();
}

glm::vec2
elWidget_MouseCursor::GetCursorPosition() const noexcept
{
    return glm::vec2( cursorPosition.x, cursorPosition.y );
}

glm::vec2
elWidget_MouseCursor::GetRelativeCursorPosition() const noexcept
{
    return glm::vec2( relativeCursorPosition.x, relativeCursorPosition.y );
}

bb::elExpected< elWidget_ShaderAnimation::Error >
elWidget_MouseCursor::SetUp(
    const std::string& shaderFile, const std::string& shaderGroup,
    const bool                                       doPerformDynamicResize,
    const std::vector< OpenGL::textureProperties_t > outputTextureProperties,
    const GLsizei sizeX, const GLsizei sizeY, const bool isFlipFlop ) noexcept
{
    return elWidget_ShaderAnimation::SetUp(
        shaderFile, shaderGroup, doPerformDynamicResize,
        outputTextureProperties, sizeX, sizeY, isFlipFlop );
}

void
elWidget_MouseCursor::SetTextureUniformBlockData(
    const size_t arrayIndex, const size_t varIndex, const void* varData,
    const size_t dataSize ) noexcept
{
    elWidget_ShaderAnimation::SetTextureUniformBlockData( arrayIndex, varIndex,
                                                          varData, dataSize );
}

void
elWidget_MouseCursor::SetDoInfinitLoop( const bool v ) noexcept
{
    elWidget_ShaderAnimation::SetDoInfinitLoop( v );
}

void
elWidget_MouseCursor::Start() noexcept
{
    elWidget_ShaderAnimation::Start();
}

void
elWidget_MouseCursor::ReverseStart() noexcept
{
    elWidget_ShaderAnimation::ReverseStart();
}

void
elWidget_MouseCursor::Stop() noexcept
{
    elWidget_ShaderAnimation::Stop();
}


} // namespace GuiGL
} // namespace el3D
