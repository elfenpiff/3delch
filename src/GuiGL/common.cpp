#include "GuiGL/common.hpp"

namespace el3D
{
namespace GuiGL
{
static float dpiScaling = 1.0f;

InvalidShader_e::InvalidShader_e( const std::string& msg )
    : std::runtime_error( msg )
{
}

InvalidShader_e::~InvalidShader_e()
{
}

InvalidFont_e::InvalidFont_e( const std::string& msg )
    : std::runtime_error( msg )
{
}

InvalidFont_e::~InvalidFont_e()
{
}


void
SetDPIScaling( const float s ) noexcept
{
    dpiScaling = s;
}

float
GetDPIScaling() noexcept
{
    return dpiScaling;
}
} // namespace GuiGL
} // namespace el3D
