#include "GuiGL/elWidget_ShaderAnimation.hpp"

#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "OpenGL/elTexture_base.hpp"
#include "utils/elFile.hpp"

#include <glm/detail/setup.hpp>


namespace el3D
{
namespace GuiGL
{
using Refresh = RenderPipeline::elRenderer_ShaderTexture::Refresh;
elWidget_ShaderAnimation::elWidget_ShaderAnimation(
    const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->SetDoReadTexture( true );
    this->SetDoDrawColors( false );
    this->SetDoInvertTextureCoordinates( true );
}

elWidget_ShaderAnimation::~elWidget_ShaderAnimation()
{
    this->RemoveShaderTexture();
}

void
elWidget_ShaderAnimation::RemoveShaderTexture() noexcept
{
    if ( this->shaderRendererID.has_value() )
    {
        this->shaderTextureDeregistrator( *this->shaderRendererID );

        this->attachedTexture.Reset();
        this->shaderRendererID = std::nullopt;
        this->animation.reset();
        this->animationUniformBuffer.reset();
    }
}

void
elWidget_ShaderAnimation::Draw( const renderPass_t renderPass ) noexcept
{
    if ( renderPass == RENDER_PASS_GLOW ) return;

    if ( this->animation )
    {
        this->UpdateTime();

        if ( this->mouseCursor != nullptr )
            this->SetTextureUniformBlockData(
                0, UBO_MOUSE_POSITION,
                glm::value_ptr( this->mouseCursor->GetPosition() ),
                2 * sizeof( GLfloat ) );
    }

    const auto& indices = this->animatedUniforms.get_index_vector();
    for ( auto i : indices )
    {
        this->animatedUniforms[i]->Update();
        if ( !this->animatedUniforms[i]->HasToPerformUpdate() )
        {
            this->animatedUniforms.erase( i );
        }
    }

    elWidget_base::Draw( renderPass );
}

bb::elExpected< elWidget_ShaderAnimation::Error >
elWidget_ShaderAnimation::SetUp(
    const std::string& shaderFile, const std::string& shaderGroup,
    const bool                                       doPerformDynamicResize,
    const std::vector< OpenGL::textureProperties_t > outputTextureProperties,
    const GLsizei sizeX, const GLsizei sizeY, const bool isFlipFlop ) noexcept
{
    this->RemoveShaderTexture();

    this->shaderFile = shaderFile;
    this->shaderSize = glm::vec2( sizeX, sizeY );
    auto shader =
        OpenGL::elGLSLParser::Create( this->shaderFile, this->shaderVersion );
    if ( shader.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to open shader animation file " << shaderFile;
        return bb::Error( Error::InvalidShaderFile );
    }

    auto shaderTexture =
        HighGL::elShaderTexture::Create( outputTextureProperties,
                                         ( *shader )
                                             ->GetContent()
                                             .at( shaderGroup )
                                             .at( GL_FRAGMENT_SHADER )
                                             .code,
                                         sizeX, sizeY, isFlipFlop );
    if ( shaderTexture.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to create shader texture";
        return bb::Error( Error::UnableToCreateShaderTexture );
    }

    this->animation = std::move( shaderTexture.GetValue() );

    this->doPerformDynamicResize = doPerformDynamicResize;

    auto ubo = OpenGL::elUniformBufferObject::Create(
        "settings_t", elWidget_base::uniformBufferStructure,
        &this->animation->GetShader() );
    if ( ubo.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to create uniform buffer object";
        return bb::Error( Error::UnableToCreateUniformBufferObject );
    }

    this->animationUniformBuffer = std::move( ubo.GetValue() );
    this->attachedTexture        = this->vo.AttachTexture(
               &this->animation->GetOutputTexture( 0 ), 0, "image", GL_TEXTURE0 );

    this->animation->SetPreDrawFunction(
        [&]() { this->animationUniformBuffer->BindToShader(); } );

    this->shaderRendererID = std::make_optional(
        this->shaderTextureRegistrator( this->animation.get() ) );
    return bb::Success<>();
}

void
elWidget_ShaderAnimation::Reshape()
{
    elWidget_base::Reshape();

    if ( this->animation )
    {
        this->SetTextureUniformBlockData(
            0, UBO_COLOR, glm::value_ptr( this->GetBaseColorSwapAdjusted() ),
            4 * sizeof( GLfloat ) );
        this->SetTextureUniformBlockData(
            0, UBO_COLOR_ACCENT,
            glm::value_ptr( this->GetAccentColorSwapAdjusted() ),
            4 * sizeof( GLfloat ) );
        this->SetTextureUniformBlockData(
            0, UBO_GLOW_COLOR,
            glm::value_ptr( this->GetBaseGlowColorSwapAdjusted() ),
            4 * sizeof( GLfloat ) );
        this->SetTextureUniformBlockData(
            0, UBO_GLOW_COLOR_ACCENT,
            glm::value_ptr( this->GetAccentGlowColorSwapAdjusted() ),
            4 * sizeof( GLfloat ) );

        if ( this->doPerformDynamicResize )
        {
            this->shaderSize   = this->GetSize() * GetDPIScaling();
            this->shaderSize.x = std::floor( this->shaderSize.x );
            this->shaderSize.y = std::floor( this->shaderSize.y );
        }

        this->SetTextureUniformBlockData( 0, UBO_RESOLUTION,
                                          glm::value_ptr( this->shaderSize ),
                                          2 * sizeof( GLfloat ) );

        this->animation->SetSize(
            static_cast< uint64_t >( this->shaderSize.x ),
            static_cast< uint64_t >( this->shaderSize.y ) );
    }
}

void
elWidget_ShaderAnimation::SetTextureUniformBlockData(
    const size_t arrayIndex, const size_t varIndex, const void* varData,
    const size_t dataSize ) noexcept
{
    this->animationUniformBuffer->SetVariableData( arrayIndex, varIndex,
                                                   varData, dataSize );
}

HighGL::elShaderTexture*
elWidget_ShaderAnimation::GetShaderTexture() noexcept
{
    return this->animation.get();
}

void
elWidget_ShaderAnimation::SetDoInfinitLoop( const bool v ) noexcept
{
    this->doInfinitLoop = v;
    if ( !this->doInfinitLoop )
    {
        float timeValue =
            static_cast< float >( this->animationRange.first.GetSeconds() );
        this->SetTextureUniformBlockData( 0, UBO_TIME, &timeValue,
                                          sizeof( GLfloat ) );
        this->Stop();
    }
    else
    {
        this->Start();
    }
}

void
elWidget_ShaderAnimation::Stop() noexcept
{
    this->doRun = false;
}

void
elWidget_ShaderAnimation::Start() noexcept
{
    this->doRun        = true;
    this->startTime    = this->window->GetRuntime();
    this->doRunReverse = false;
}

void
elWidget_ShaderAnimation::ReverseStart() noexcept
{
    this->Start();
    this->doRunReverse = true;
}

void
elWidget_ShaderAnimation::UpdateTime() noexcept
{
    units::Time now = this->window->GetRuntime();

    if ( this->doInfinitLoop && this->doRun )
    {
        float adjustedTime = static_cast< float >( now.GetSeconds() ) *
                             this->GetAnimationSpeed();
        this->SetTextureUniformBlockData( 0, UBO_TIME, &adjustedTime,
                                          sizeof( GLfloat ) );
    }
    else if ( !this->doInfinitLoop && this->doRun )
    {
        if ( !this->doRunReverse )
        {
            now -= this->startTime;
            now = units::Time::Seconds( now.GetSeconds() *
                                        this->GetAnimationSpeed() );
            if ( now > this->animationRange.second )
            {
                this->Stop();
                now = this->animationRange.second;
            }
        }
        else if ( this->doRunReverse )
        {
            now =
                this->animationRange.second -
                units::Time::Seconds( this->GetAnimationSpeed() *
                                      ( now - this->startTime ).GetSeconds() );
            if ( now < this->animationRange.first )
            {
                this->Stop();
                now = this->animationRange.first;
            }
        }

        float updatedTime = static_cast< float >( now.GetSeconds() );
        this->SetTextureUniformBlockData( 0, UBO_TIME, &updatedTime,
                                          sizeof( GLfloat ) );
    }
}
} // namespace GuiGL
} // namespace el3D
