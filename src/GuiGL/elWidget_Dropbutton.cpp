#include "GuiGL/elWidget_Dropbutton.hpp"

#include "GuiGL/elWidget_Listbox.hpp"
#include "HighGL/elEventTexture.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Dropbutton::elWidget_Dropbutton( const constructor_t& constructor )
    : elWidget_Button( constructor ), closeListOnClick( this->CreateEvent() )
{
    this->list = this->parent->Create< elWidget_Listbox >();
    this->list->EnforceThemeParentWidget( this->type, true );
    this->list->SetDoRenderWidget( false );
    this->list->SetIsResizable( false );
    this->list->SetIsAlwaysOnTop( true );
    this->list->SetHasMultiSelection( false );
    this->SetCallback( [this]() { this->ToggleList(); }, STATE_CLICKED,
                       CallCondition::OnLeave );
    this->list->SetElementSelectionCallback( [this] {
        auto selection = this->list->GetSelectedWidgets();
        this->SetText(
            this->list->GetElementAtIndex< elWidget_Button >( selection[0] )
                ->GetText() );
        this->selected = selection[0];

        this->ToggleList();

        if ( this->list->DoRenderWidget() && this->selectionCallback )
            this->selectionCallback();

        this->list->ResetListSelection();
    } );

    this->closeListOnClick->SetCallback( [this]( const SDL_Event e ) {
        if ( !this->list->DoesEventColorBelongsToMeOrChild(
                 this->GetColorToCurrentEvent() ) &&
             !this->DoesEventColorBelongsToMeOrChild(
                 this->GetColorToCurrentEvent() ) &&
             e.button.type == SDL_MOUSEBUTTONDOWN )
        {
            this->CloseList();
        }
    } );
}

void
elWidget_Dropbutton::Reshape()
{
    auto size = this->GetSize();
    auto pos  = this->GetPosition();
    this->list->AdjustHeightToFitElements();
    auto currentListSize = this->list->GetSize();

    if ( currentListSize.y > this->maxListHeight )
    {
        this->list->SetSize( { currentListSize.x, this->maxListHeight } );
        this->listSize = { size[0], this->maxListHeight };
    }
    else
    {
        this->listSize = { size[0], currentListSize.y };
    }

    this->list->SetPosition( { pos[0], pos[1] + size[1] } );

    elWidget_Button::Reshape();
}

void
elWidget_Dropbutton::ToggleList() noexcept
{
    if ( this->list->DoRenderWidget() )
    {
        this->CloseList();
        this->closeListOnClick->Unregister();
    }
    else
    {
        this->OpenList();
        this->closeListOnClick->Register();
    }
}

void
elWidget_Dropbutton::CloseList() noexcept
{
    this->list->ResizeTo( { listSize.x, 0 },
                          [&] { this->list->SetDoRenderWidget( false ); } );
}

void
elWidget_Dropbutton::OpenList() noexcept
{
    this->list->SetSize( { this->listSize.x, 0 } );
    this->list->SetDoRenderWidget( true );
    this->list->ResizeTo( this->listSize );
}

void
elWidget_Dropbutton::AddElements( const std::vector< std::string >& elements,
                                  const size_t position ) noexcept
{
    if ( elements.empty() ) return;

    if ( this->list->IsEmpty() && this->GetText().empty() )
        this->SetText( elements[0] );
    this->list->AddElements( elements, position );
}

void
elWidget_Dropbutton::AddElement( const std::string& element,
                                 const size_t       position ) noexcept
{
    this->AddElements( { element }, position );
}

void
elWidget_Dropbutton::RemoveElements( const size_t position,
                                     const size_t length ) noexcept
{
    this->list->RemoveElements( position, length );
}

uint64_t
elWidget_Dropbutton::GetSelection() noexcept
{
    return this->selected;
}

void
elWidget_Dropbutton::SetMaxListHeight( const float v ) noexcept
{
    this->maxListHeight = v;
}

void
elWidget_Dropbutton::SetCallbackOnSelection(
    const std::function< void() >& v ) noexcept
{
    this->selectionCallback = v;
}

void
elWidget_Dropbutton::SetPositionPointOfOrigin(
    const positionCorner_t v, const bool doKeepPosition ) noexcept
{
    handler::settings::SetPositionPointOfOrigin( v, doKeepPosition );
    this->list->SetPositionPointOfOrigin( v, doKeepPosition );
}
} // namespace GuiGL
} // namespace el3D
