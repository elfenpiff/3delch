#include "GuiGL/elWidget_Table.hpp"

#include "GuiGL/GuiGL_NAMESPACE_NAME.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Slider.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Table::elWidget_Table( const constructor_t& constructor )
    : elWidget_Window( constructor ), elementFactory{ [this] {
          return this->Create< elWidget_Label >();
      } }
{
    this->onSelectionCallback = this->CreateCallback();
}

bool
elWidget_Table::IsEmpty() const noexcept
{
    return this->elements.empty();
}

void
elWidget_Table::SetHasTitleRow( const bool v ) noexcept
{
    if ( v == this->GetHasTitleRow() ) return;
    this->widgetSettings.Set( PROPERTY_HAS_TITLE_ROW, std::vector< int >{ v } );
    this->AdjustTitleRow();
    this->runOnce.Call( _Method_Reshape_ );
}

bool
elWidget_Table::GetHasTitleRow() const noexcept
{
    return static_cast< bool >(
        this->widgetSettings.template Get< int >( PROPERTY_HAS_TITLE_ROW )[0] );
}

void
elWidget_Table::AdjustTitleRow() noexcept
{
    if ( !this->GetHasTitleRow() && !this->titleRow.empty() )
    {
        this->titleRow.clear();
    }
    else if ( this->GetHasTitleRow() && this->titleRow.empty() )
    {
        if ( this->elements.empty() || this->elements[0].empty() ) return;

        for ( size_t i = 0, limit = this->elements[0].size(); i < limit; ++i )
        {
            auto titleButton = this->Create< elWidget_Button >();
            titleButton->SetHasFixedPosition( false, true );
            titleButton->SetIsAlwaysOnTop( true );
            this->titleRow.emplace_back( std::move( titleButton ) );
        }
    }
}

void
elWidget_Table::ClearTable() noexcept
{
    this->titleRow.clear();
    this->elements.clear();
    this->ResetSelection();
}

elWidget_base_ptr
elWidget_Table::CreateTableElement( const size_t col,
                                    const size_t row ) noexcept
{
    auto newWidget = this->elementFactory();
    if ( newWidget->GetType() == TYPE_LABEL )
    {
        static_cast< elWidget_Label* >( newWidget.Get() )
            ->DoAdjustSizeToTextSize( false );
    }
    newWidget->SetDisableEventHandling( false );
    newWidget->SetIsUnselectable( true );
    newWidget->SetSize( { this->columnWidth, this->rowHeight } );
    newWidget->SetCallback( [this, col, row]()
                            { this->Hover( glm::uvec2( col, row ) ); },
                            STATE_HOVER, CallCondition::OnEnter );
    newWidget->SetCallback( [this, col, row]()
                            { this->Select( glm::uvec2( col, row ) ); },
                            STATE_CLICKED, CallCondition::OnLeave );
    return newWidget;
}

bool
elWidget_Table::AdjustSelectionWidgets(
    const glm::uvec2& selectionChangePosition ) noexcept
{
    bool addSelection = false;

    size_t position = 0;
    size_t limit    = this->highlight.selection.size();

    for ( ; position < limit; ++position )
        if ( this->highlight.selection[position] == selectionChangePosition )
            break;

    if ( position == limit )
    {
        if ( !this->GetHasMultiSelection() ) this->ResetSelection();

        this->highlight.selection.emplace_back( selectionChangePosition );
        this->highlight.selectionWidget.emplace_back(
            this->CreateHighlightWidget( this->highlight.selectionMode,
                                         true ) );
        addSelection = true;
    }
    else
    {
        if ( !this->GetHasMultiSelection() )
            this->ResetSelection();
        else
        {
            this->highlight.selectionWidget.erase(
                this->highlight.selectionWidget.begin() +
                static_cast< long >( position ) );
            this->highlight.selection.erase( this->highlight.selection.begin() +
                                             static_cast< long >( position ) );
        }
    }

    return addSelection;
}

void
elWidget_Table::Select( const glm::uvec2& v ) noexcept
{
    if ( this->highlight.selectionMode == TABLEMODE_NONE ) return;
    bool addSelection = this->AdjustSelectionWidgets( v );

    if ( addSelection )
    {
        elWidget_base* newSelection =
            this->highlight.selectionWidget.back().Get();
        auto position = this->GetElement( v.x, v.y )->GetPosition();
        auto size     = this->GetElement( v.x, v.y )->GetSize();

        switch ( this->highlight.selectionMode )
        {
            case TABLEMODE_COL:
                newSelection->SetPosition( { position.x, 0.0f } );
                newSelection->SetSize( { size.x, 0.0f } );
                break;
            case TABLEMODE_ROW:
                newSelection->SetPosition( { 0.0f, position.y } );
                newSelection->SetSize( { 0.0f, size.y } );
                break;
            case TABLEMODE_ENTRY:
                newSelection->SetPosition( position );
                newSelection->SetSize( size );
                break;
            default:
                break;
        }
        newSelection->SetState( STATE_SELECTED );
    }

    this->onSelectionCallback->Call();
}

void
elWidget_Table::SetOnSelectionCallback(
    const std::function< void() >& f ) noexcept
{
    this->onSelectionCallback->Set( f );
}

std::vector< glm::uvec2 >
elWidget_Table::GetSelection() const noexcept
{
    return this->highlight.selection;
}

void
elWidget_Table::ResetNumberOfColsAndRows( const size_t cols,
                                          const size_t rows ) noexcept
{
    this->ClearTable();
    this->elements.resize( rows );

    for ( size_t i = 0; i < rows; ++i )
        for ( size_t k = 0; k < cols; ++k )
            this->elements[i].emplace_back( this->CreateTableElement( k, i ) );

    this->AdjustTitleRow();
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Table::AlignTableCells() noexcept
{
    if ( this->elements.empty() || this->elements[0].empty() ) return;

    float yPos =
        ( this->GetHasTitleRow() && !this->titleRow.empty() )
            ? this->titleRow[0]->GetSize().y + this->titlebarToEntrySpace
            : 0.0f;

    for ( auto& row : this->elements )
    {
        float rowHeight = ( !row.empty() ) ? row[0]->GetSize().y : 0.0f;
        float xPos      = 0.0f;
        for ( auto& element : row )
        {
            element->SetPosition( { xPos, yPos } );
            xPos += element->GetSize().x;
        }
        yPos += rowHeight;
    }

    for ( size_t i = 0, limit = std::min( this->elements[0].size(),
                                          this->titleRow.size() );
          i < limit; ++i )
    {
        this->titleRow[i]->SetPosition(
            { this->elements[0][i]->GetPosition().x, 0.0f } );
        this->titleRow[i]->SetSize( { this->elements[0][i]->GetSize().x,
                                      this->titleRow[i]->GetSize().y } );
    }
}

void
elWidget_Table::SetElementDefaultDimensions( const size_t width,
                                             const size_t height ) noexcept
{
    this->columnWidth = width;
    this->rowHeight   = height;
}

void
elWidget_Table::SetRowHeight( const size_t row, const float height ) noexcept
{
    std::for_each( this->elements[row].begin(), this->elements[row].end(),
                   [=]( elWidget_base_ptr& e )
                   {
                       float width = e->GetSize().x;
                       e->SetSize( { width, height } );
                   } );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Table::SetColumnWidth( const size_t col, const float width ) noexcept
{
    std::for_each( this->elements.begin(), this->elements.end(),
                   [=]( std::vector< elWidget_base_ptr >& row )
                   {
                       float height = row[col]->GetSize().y;
                       row[col]->SetSize( { width, height } );
                   } );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Table::SetHoveringMode( const Mode_t mode ) noexcept
{
    this->AdjustMode( this->highlight.hoverMode, this->highlight.hoverWidget,
                      mode );
}

void
elWidget_Table::AdjustMode( Mode_t& modeToAdjust, elWidget_base_ptr& widget,
                            const Mode_t newMode ) noexcept
{
    if ( newMode == modeToAdjust ) return;

    modeToAdjust = newMode;
    if ( widget ) widget.Reset();

    if ( newMode != TABLEMODE_NONE )
    {
        widget = this->CreateHighlightWidget( newMode, false );
        widget->SetIsAlwaysOnBottom( true );
    }
}

void
elWidget_Table::SetTitleRowButtonText( const size_t       col,
                                       const std::string& text ) noexcept
{
    this->titleRow[col]->SetText( text );
}

elWidget_base_ptr
elWidget_Table::CreateHighlightWidget( const Mode_t mode,
                                       const bool   isSelected ) noexcept
{
    auto widget = this->Create< elWidget_base >();
    widget->SetClassName(
        ( isSelected )
            ? this->highlight.selectedClasses[static_cast< size_t >( mode )]
            : this->highlight.highlightClasses[static_cast< size_t >( mode )],
        true );

    switch ( mode )
    {
        case TABLEMODE_COL:
            widget->SetStickToParentSize( { -1.0f, 0.0f } );
            widget->SetHasFixedPosition( false, true );
            break;
        case TABLEMODE_ROW:
            widget->SetStickToParentSize( { 0.0f, -1.0f } );
            widget->SetHasFixedPosition( true, false );
            break;
        default:
            break;
    }

    widget->SetDisableEventHandling( true );
    widget->SetIsAlwaysOnBottom( true );
    return widget;
}

void
elWidget_Table::Reshape()
{
    elWidget_Window::Reshape();
    this->AlignTableCells();
    this->ReshapeHoverHighlight();
    elWidget_Window::Reshape();
}

void
elWidget_Table::ReshapeHoverHighlight() noexcept
{
    auto currentSelection = this->highlight.hoverPosition.Get();
    if ( this->elements.size() <= currentSelection.y ||
         this->elements[currentSelection.y].size() <= currentSelection.x )
        return;

    bool xPositionUpdate = ( currentSelection.x !=
                             this->highlight.hoverPosition.PreviousValue().x );
    bool yPositionUpdate = ( currentSelection.y !=
                             this->highlight.hoverPosition.PreviousValue().y );

    auto position = this->GetElement( currentSelection.x, currentSelection.y )
                        ->GetPosition();
    auto size =
        this->GetElement( currentSelection.x, currentSelection.y )->GetSize();

    auto& hover = this->highlight.hoverWidget;
    if ( hover )
    {
        if ( yPositionUpdate && this->highlight.hoverMode == TABLEMODE_ROW )
        {
            hover->SetPosition( { 0.0f, position.y } );
            hover->ResizeTo( { hover->GetSize().x, size.y } );
        }

        if ( xPositionUpdate && this->highlight.hoverMode == TABLEMODE_COL )
        {
            hover->SetPosition( { position.x, 0.0f } );
            hover->ResizeTo(
                { std::max( size.x, static_cast< float >( this->columnWidth ) ),
                  hover->GetSize().y } );
        }

        if ( ( xPositionUpdate || yPositionUpdate ) &&
             this->highlight.hoverMode == TABLEMODE_ENTRY )
        {
            hover->SetPosition( { position.x, position.y } );
            hover->ResizeTo( { size.x, size.y } );
        }
    }

    this->highlight.hoverPosition.ResetHistory();
}

void
elWidget_Table::Hover( const glm::uvec2& v ) noexcept
{
    this->highlight.hoverPosition = v;
    if ( this->highlight.hoverWidget )
        this->highlight.hoverWidget->SetDoRenderWidget( true );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Table::SetSelectionMode( const Mode_t mode ) noexcept
{
    this->highlight.selectionMode = mode;

    if ( mode == TABLEMODE_NONE ) this->ResetSelection();
}

void
elWidget_Table::ResetSelection() noexcept
{
    this->highlight.selectionWidget.clear();
    this->highlight.selection.clear();
    if ( this->highlight.hoverWidget )
        this->highlight.hoverWidget->SetDoRenderWidget( false );
}

void
elWidget_Table::SetElementBaseType( const widgetType_t baseType ) noexcept
{
    this->ClearTable();
    switch ( baseType )
    {
        case TYPE_BUTTON:
        {
            this->elementFactory = [this]()
            { return this->Create< elWidget_Button >(); };
            break;
        }
        case TYPE_ENTRY:
        {
            this->elementFactory = [this]
            { return this->Create< elWidget_Entry >(); };
            break;
        }
        case TYPE_LABEL:
        {
            this->elementFactory = [this]
            { return this->Create< elWidget_Label >(); };
            break;
        }
        default:
        {
            LOG_FATAL( 0 ) << "invalid widgetType, only TYPE_BUTTON, "
                              "TYPE_ENTRY, TYPE_LABEL";
            std::terminate();
        }
    }
}
} // namespace GuiGL
} // namespace el3D
