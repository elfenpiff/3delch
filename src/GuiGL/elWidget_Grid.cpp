#include "GuiGL/elWidget_Grid.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Grid::elWidget_Grid( const constructor_t& constructor )
    : elWidget_base( constructor )
{
}

void
elWidget_Grid::AdjustGridSizeTo( const glm::uvec2& position ) noexcept
{
    if ( this->gridSize.x <= position.x )
    {
        this->gridContents.resize( position.x + 1 );
        for ( uint64_t i = this->gridSize.x; i <= position.x; ++i )
            this->gridContents[i].resize( this->gridSize.y );
    }

    if ( this->gridSize.y <= position.y )
        for ( auto& v : this->gridContents )
            v.resize( position.y + 1 );

    this->gridSize = { std::max( position.x + 1, this->gridSize.x ),
                       std::max( position.y + 1, this->gridSize.y ) };
}

void
elWidget_Grid::Remove( const elWidget_base* const ptr ) noexcept
{
    for ( auto& gridLine : this->gridContents )
        for ( auto& element : gridLine )
            if ( element.ptr == ptr ) element = element_t();
}

void
elWidget_Grid::SetGridSpacing( const glm::vec2& spacing ) noexcept
{
    this->gridSpacing = spacing;
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Grid::SetUniformElementHeight(
    const std::optional< float > v ) noexcept
{
    this->uniformElementHeight = v;
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Grid::Reshape()
{
    glm::vec2 elementSize =
        ( this->GetContentSize() - 2.0f * this->gridSpacing -
          glm::vec2( this->gridSize - 1U ) * this->gridSpacing ) /
        glm::vec2( this->gridSize );

    if ( this->uniformElementHeight )
        elementSize.y = *this->uniformElementHeight;

    glm::vec2 currentPosition = this->gridSpacing;
    for ( uint64_t x = 0; x < this->gridSize.x; ++x )
    {
        for ( uint64_t y = 0; y < this->gridSize.y; ++y )
        {
            auto widget = this->gridContents[x][y];
            if ( widget.ptr != nullptr )
            {
                switch ( widget.properties.resizeOption )
                {
                    case Grid::Resize::None:
                        break;
                    case Grid::Resize::FitToColumn:
                        widget.ptr->SetSize( elementSize );
                        break;
                    default:
                    {
                        float n = static_cast< float >(
                            widget.properties.resizeOption );
                        glm::vec2 newSize =
                            glm::vec2( elementSize.x * n, elementSize.y ) +
                            glm::vec2( this->gridSpacing.x * ( n - 1.0f ),
                                       0.0f );
                        widget.ptr->SetSize( newSize );
                        break;
                    }
                }
                widget.ptr->SetPosition( currentPosition );
            }

            currentPosition.y += elementSize.y + this->gridSpacing.y;
        }
        currentPosition.x += elementSize.x + this->gridSpacing.x;
        currentPosition.y = this->gridSpacing.y;
    }

    elWidget_base::Reshape();
}

bool
elWidget_Grid::AdjustSizeToFitChildren() noexcept
{
    if ( !elWidget_base::AdjustSizeToFitChildren() ) return false;
    this->SetSize( this->GetSize() + this->gridSpacing );
    return true;
}

} // namespace GuiGL
} // namespace el3D
