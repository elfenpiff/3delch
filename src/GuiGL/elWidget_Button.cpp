#include "GuiGL/elWidget_Button.hpp"

#include "GuiGL/elWidget_Label.hpp"


namespace el3D
{
namespace GuiGL
{
elWidget_Button::elWidget_Button( const constructor_t& constructor )
    : elWidget_base( constructor ),
      textLabel( this->Create< elWidget_Label >() ),
      fontDimension( { 0, textLabel->GetFontHeight() } )
{
    this->textLabel->SetPositionPointOfOrigin( POSITION_CENTER );
}

void
elWidget_Button::SetTextAlignment( const positionCorner_t position,
                                   const glm::vec2&       offsetValue ) noexcept
{
    this->offset        = offsetValue;
    this->textAlignment = position;
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Button::SetText( const std::string _text ) noexcept
{
    if ( this->text == _text ) return;

    this->text = _text;
    this->textLabel->SetText( this->text );

    glm::vec2 newSize = this->GetSize();

    this->SetSize( newSize );

    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Button::Reshape()
{
    this->textLabel->SetPositionPointOfOrigin( this->textAlignment );
    this->textLabel->SetPosition( this->offset );
    if ( this->GetContentSize().y <
         static_cast< float >( this->GetFontHeight() ) )
    {
        this->SetSize(
            { this->GetSize().x, static_cast< float >( this->GetFontHeight() ) +
                                     this->GetBorderSize()[POSITION_TOP] +
                                     this->GetBorderSize()[POSITION_BOTTOM] } );
    }

    elWidget_base::Reshape();
}

std::string
elWidget_Button::GetText() const noexcept
{
    return this->text;
}

uint64_t
elWidget_Button::GetFontHeight() const noexcept
{
    return this->textLabel->GetFontHeight();
}

void
elWidget_Button::SetState( const widgetStates_t state ) noexcept
{
    if ( state == STATE_SELECTED && this->clickCallback )
        this->clickCallback->Call();
    elWidget_base::SetState( state );
}

void
elWidget_Button::SetClickCallback( const std::function< void() >& f ) noexcept
{
    if ( f )
        this->clickCallback = this->CreateCallback( f );
    else
        this->clickCallback.Reset();
}

} // namespace GuiGL
} // namespace el3D
