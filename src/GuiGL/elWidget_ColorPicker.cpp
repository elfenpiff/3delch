#include "GuiGL/elWidget_ColorPicker.hpp"

#include "utils/convert.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_ColorPicker::elWidget_ColorPicker( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->SetSize( { 430, 135 } );

    float width = 75.0f;

    this->preview = this->Create< elWidget_base >();
    this->preview->SetSize( { width, width } );
    this->preview->SetPosition( { 0.0f, 0.0f } );
    this->preview->SetDisableEventHandling( true );
    this->preview->SetIsMovable( false );
    this->preview->SetIsResizable( false );
    this->preview->SetBorderSize( { 0.0f, 0.0f, 0.0f, 0.0f } );
    this->UpdatePreview();

    this->CreateColorGuiElements( this->red, "red", { width + 0.0f, 0.0f } );
    this->CreateColorGuiElements( this->green, "green",
                                  { width + 0.0f, 25.0f } );
    this->CreateColorGuiElements( this->blue, "blue", { width + 0.0f, 50.0f } );

    this->pickButton = this->Create< elWidget_Button >();
    this->pickButton->SetText( "pick" );
    this->pickButton->SetPosition( { 0.0f, 75.0f } );

    this->closeButton = this->Create< elWidget_Button >();
    this->closeButton->SetPositionPointOfOrigin(
        positionCorner_t::POSITION_TOP_RIGHT );
    this->closeButton->SetText( "close" );
    this->closeButton->SetPosition( { 0.0f, 75.0f } );
    this->SetOnCloseCallback( std::function< void() >() );
}

void
elWidget_ColorPicker::SetOnCloseCallback(
    const std::function< void() >& v ) noexcept
{
    this->closeButton->SetClickCallback(
        [=, this]
        {
            if ( v ) v();
            this->RemoveThis();
        } );
}

void
elWidget_ColorPicker::SetOnPickCallback(
    const std::function< void() >& v ) noexcept
{
    this->pickButton->SetClickCallback( v );
}

glm::uvec3
elWidget_ColorPicker::GetUintColor() const noexcept
{
    return { this->red.colorValue * COLOR_FACTOR,
             this->green.colorValue * COLOR_FACTOR,
             this->blue.colorValue * COLOR_FACTOR };
}

glm::vec3
elWidget_ColorPicker::GetFloatColor() const noexcept
{
    return { this->red.colorValue, this->green.colorValue,
             this->blue.colorValue };
}

void
elWidget_ColorPicker::UpdatePreview() noexcept
{
    this->preview->SetBaseColorForState( { this->red.colorValue,
                                           this->green.colorValue,
                                           this->blue.colorValue, 1.0f },
                                         widgetStates_t::STATE_DEFAULT );
}

void
elWidget_ColorPicker::CreateColorGuiElements(
    color_t& c, const std::string& v, const glm::vec2& position ) noexcept
{
    c.entry = this->Create< elWidget_Entry >();
    c.entry->SetSize( { 40.0f, 23.0f } );
    c.entry->SetPosition( { 310.0f + position.x, position.y } );
    c.entry->SetText( "0" );
    c.entry->RestrictInputSizeTo( 3 );
    c.entry->RestrictInputCharacters( "0123456789" );

    c.slider = this->Create< elWidget_Slider >();
    c.slider->SetSize( { 300.0f, 20.0f } );
    c.slider->SetDescriptionText( v );
    c.slider->SetRelativeSliderButtonSize( 0.1f );
    c.slider->SetPosition( { position.x + 5.0f, position.y } );
    c.slider->SetSliderPositionChangeCallback(
        [entry = c.entry.Get(), color = &c.colorValue, this]( auto v )
        {
            entry->SetText( utils::ToString( v * COLOR_FACTOR, 0 ) );
            *color = v;
            this->UpdatePreview();
        } );

    c.entry->SetOnEnterKeyPressCallback(
        [slider = c.slider.Get(), entry = c.entry.Get()]
        {
            slider->SetSliderPosition(
                strtof( entry->GetText().c_str(), nullptr ) / 255.0f );
        } );
}
} // namespace GuiGL
} // namespace el3D
