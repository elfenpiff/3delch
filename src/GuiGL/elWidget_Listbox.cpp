#include "GuiGL/elWidget_Listbox.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Slider.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Listbox::elWidget_Listbox( const constructor_t& constructor )
    : elWidget_Table( constructor )
{
    this->elementSelectionCallback = this->CreateCallback();
    this->SetElementBaseType( TYPE_BUTTON );
}

void
elWidget_Listbox::SetLabelText() noexcept
{
    this->ResetNumberOfColsAndRows( 1, this->listboxLabels.size() );
    for ( size_t i = 0, limit = this->listboxLabels.size(); i < limit; ++i )
    {
        static_cast< elWidget_Button* >( this->elements[i][0].Get() )
            ->SetText( this->listboxLabels[i] );

        // selection callback
        this->elements[i][0]->SetCallback(
            [this, i]()
            {
                this->selection.emplace_back( i );
                this->elementSelectionCallback->Call();
            },
            STATE_CLICKED, CallCondition::OnEnter );

        // deselected callback
        this->elements[i][0]->SetCallback(
            [this, i]()
            {
                this->selection.erase( std::remove( this->selection.begin(),
                                                    this->selection.end(), i ),
                                       this->selection.end() );
            },
            STATE_DEFAULT, CallCondition::OnEnter );
    }
}

void
elWidget_Listbox::ResetListSelection() noexcept
{
    for ( auto widget : this->selection )
        this->GetElementAtIndex( widget )->SetState( STATE_DEFAULT );
    this->selection.clear();
}

void
elWidget_Listbox::SetElementSelectionCallback(
    const std::function< void() >& callback ) noexcept
{
    this->elementSelectionCallback->Set( callback );
}

std::vector< size_t >
elWidget_Listbox::GetSelectedWidgets() const noexcept
{
    return this->selection;
}

std::vector< std::string >
elWidget_Listbox::GetSelectedWidgetText() const noexcept
{
    std::vector< std::string > retVal;
    for ( uint64_t i = 0, limit = this->selection.size(); i < limit; ++i )
        retVal.emplace_back( this->listboxLabels[this->selection[i]] );

    return retVal;
}

void
elWidget_Listbox::AddElements( const std::vector< std::string >& newElements,
                               const size_t position ) noexcept
{
    this->listboxLabels.insert( this->listboxLabels.begin() +
                                    static_cast< int >( position ),
                                newElements.begin(), newElements.end() );

    this->SetLabelText();
}

void
elWidget_Listbox::AddElement( const std::string& v, const size_t position )
{
    this->AddElements( { v }, position );
}

void
elWidget_Listbox::RemoveElements( const size_t position,
                                  const size_t length ) noexcept
{
    if ( length > 1 )
        this->listboxLabels.erase(
            this->listboxLabels.begin() + static_cast< int >( position ),
            this->listboxLabels.begin() +
                static_cast< int >( position + length - 1 ) );
    else
        this->listboxLabels.erase( this->listboxLabels.begin() +
                                   static_cast< int >( position ) );

    this->SetLabelText();
}

void
elWidget_Listbox::Clear() noexcept
{
    if ( !this->listboxLabels.empty() )
        this->RemoveElements( 0, this->listboxLabels.size() );
}

void
elWidget_Listbox::SetTextAlignment( const positionCorner_t position,
                                    const glm::vec2& offsetValue ) noexcept
{
    this->textAlignment = position;
    this->offset        = offsetValue;
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Listbox::Reshape()
{
    this->SetColumnWidth( 0, this->GetContentSize().x );

    for ( size_t i = 0, limit = this->listboxLabels.size(); i < limit; ++i )
    {
        auto buttonBorderSize = this->elements[i][0]->GetBorderSize();
        dynamic_cast< elWidget_Button* >( this->elements[i][0].Get() )
            ->SetTextAlignment( this->textAlignment, this->offset );
        float rowHeight =
            ( static_cast< float >(
                  static_cast< elWidget_Button* >( this->elements[i][0].Get() )
                      ->GetFontHeight() ) +
              buttonBorderSize[POSITION_TOP] +
              buttonBorderSize[POSITION_BOTTOM] );
        this->SetRowHeight( i, rowHeight );
    }

    elWidget_Table::Reshape();
}

void
elWidget_Listbox::AdjustHeightToFitElements() noexcept
{
    float newHeight = 0.0f;
    for ( auto& row : this->elements )
    {
        if ( row.empty() ) continue;
        newHeight += row[0]->GetSize().y;
    }
    newHeight += this->GetBorderSize()[POSITION_TOP] +
                 this->GetBorderSize()[POSITION_BOTTOM];

    this->SetSize( { this->GetSize().x, newHeight } );
}

void
elWidget_Listbox::ResetNumberOfColsAndRows( const size_t cols,
                                            const size_t rows ) noexcept
{
    elWidget_Table::ResetNumberOfColsAndRows( cols, rows );
}

void
elWidget_Listbox::SetColumnWidth( const size_t col, const float width ) noexcept
{
    elWidget_Table::SetColumnWidth( col, width );
}

void
elWidget_Listbox::SetRowHeight( const size_t row, const float height ) noexcept
{
    elWidget_Table::SetRowHeight( row, height );
}

void
elWidget_Listbox::SetElementDefaultDimensions( const size_t width,
                                               const size_t height ) noexcept
{
    elWidget_Table::SetElementDefaultDimensions( width, height );
}


} // namespace GuiGL
} // namespace el3D
