#include "GuiGL/elWidget_base.hpp"

#include "GLAPI/elEvent.hpp"
#include "GLAPI/elWindow.hpp"
#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "HighGL/elObjectGeometryBuilder.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "lua/elConfigHandler.hpp"
#include "utils/elClassSettings.hpp"

#include <glm/gtc/epsilon.hpp>

namespace el3D
{
namespace GuiGL
{
std::vector< std::string > elWidget_base::uniformBufferStructure = {
    "settings_t.color",
    "settings_t.colorAccent",
    "settings_t.glowColor",
    "settings_t.glowColorAccent",
    "settings_t.glowScaling",
    "settings_t.borders",
    "settings_t.borderEventIncrease",
    "settings_t.resolution",
    "settings_t.size",
    "settings_t.textureSize",
    "settings_t.position",
    "settings_t.mousePosition",
    "settings_t.time",
    "settings_t.viewPort",
    "settings_t.doDrawEventColor",
    "settings_t.doReadTexture",
    "settings_t.doDrawColors",
    "settings_t.doInvertTextureCoordinates",
    "settings_t.isEmpty",
    "settings_t.eventColorTop",
    "settings_t.eventColorBottom",
    "settings_t.eventColorLeft",
    "settings_t.eventColorRight",
    "settings_t.eventColorTopLeft",
    "settings_t.eventColorTopRight",
    "settings_t.eventColorBottomLeft",
    "settings_t.eventColorBottomRight",
    "settings_t.eventColorInner" };

elWidget_base::elWidget_base( const constructor_t &constructor )
    : handler::callback( constructor.callbackContainer ),
      handler::settings( this, constructor.theme, constructor.className,
                         constructor.enforcedThemeParent ),
      handler::event( this, constructor.eventTexture, constructor.eventHandler,
                      constructor.stickyExclusiveEvent ),
      handler::widgetFactory(
          this, constructor.parent, &handler::event::GetStickyExclusiveEvent(),
          constructor.eventTexture, constructor.eventHandler,
          constructor.window, constructor.fontCache, constructor.mouseCursor,
          constructor.callbackContainer, constructor.shaderTextureRegistrator,
          constructor.shaderTextureDeregistrator, constructor.shaderVersion,
          constructor.theme, constructor.shader, constructor.glowShader,
          constructor.screenWidth, constructor.screenHeight,
          constructor.glowScaling, &handler::settings::GetState() ),
      type( constructor.type ), parent( constructor.parent ),
      runOnce( { std::bind( &elWidget_base::Reshape, this ) } ),
      vo( OpenGL::DrawMode::Triangles,
          HighGL::elObjectGeometryBuilder::CreateScreenQuad()
              .GetObjectGeometry() ),
      window( constructor.window ), fontCache{ constructor.fontCache },
      mouseCursor{ constructor.mouseCursor },
      shaderTextureRegistrator{ constructor.shaderTextureRegistrator },
      shaderTextureDeregistrator{ constructor.shaderTextureDeregistrator },
      shaderVersion{ constructor.shaderVersion }
{

    this->uniformBuffer =
        OpenGL::elUniformBufferObject::Create(
            "settings_t", uniformBufferStructure, this->GetShader() )
            .OrElse(
                []
                {
                    throw InvalidShader_e( LOG_EX(
                        0, "unable to acquire settings_t uniform buffer" ) );
                } )
            .GetValue();


    if ( this->vo.AddShader( this->GetShader() ).HasError() )
    {
        throw InvalidShader_e(
            LOG_EX( 0, "unable to add shader to vertex object" ) );
    }

    if ( this->vo.AddShader( this->GetGlowShader() ).HasError() )
    {
        throw InvalidShader_e(
            LOG_EX( 0, "unable to add glow shader to vertex object" ) );
    }

    for ( size_t i = UBO_EVENT_COLOR_TOP; i < UBO_EVENT_END; ++i )
    {
        this->uniformBuffer->SetVariableData(
            0, i, glm::value_ptr( this->GetRGBAColorToEvent( i ) ),
            4 * sizeof( GLfloat ) );
    }

    this->LoadDefaultsFromConfig();

    this->SetDoReadTexture( false );
    this->SetDoDrawColors( true );

    GLint isEmpty = ( this->type == TYPE_EMPTY ) ? 1 : 0;
    this->uniformBuffer->SetVariableData( 0, UBO_IS_EMPTY, &isEmpty,
                                          sizeof( GLint ) );
    this->uniformBuffer->SetVariableData(
        0, UBO_GLOW_SCALING, &constructor.glowScaling, sizeof( GLfloat ) );
}

elWidget_base::~elWidget_base()
{
}

void
elWidget_base::ReloadConfigSettings() noexcept
{
}

void
elWidget_base::Draw( const renderPass_t renderPass ) noexcept
{
    if ( !this->DoRenderWidget() ) return;

    this->CallCallback( this->GetState(), CallCondition::InState );

    if ( this->mouseCursor != nullptr && this->type == TYPE_SHADERANIMATION )
    {
        this->uniformBuffer->SetVariableData(
            0, UBO_MOUSE_POSITION,
            glm::value_ptr( this->mouseCursor->GetPosition() ),
            2 * sizeof( GLfloat ) );
    }

    this->UpdateAnimations();
    this->UpdateColors();

    if ( this->IsGhostWidget() ) return;

    this->DrawMe( renderPass );
    this->DrawChildren( renderPass );
}

void
elWidget_base::DrawMe( const renderPass_t renderPass ) noexcept
{
    this->runOnce.Run();
    float runTime =
        static_cast< float >( this->window->GetRuntime().GetSeconds() );
    this->uniformBuffer->SetVariableData( 0, UBO_TIME, &runTime,
                                          sizeof( GLfloat ) );

    this->uniformBuffer->BindToShader();

    static const OpenGL::elCamera_Perspective dummyCamera;
    this->vo.Draw( renderPass, dummyCamera );
}

void
elWidget_base::DrawChildren( const renderPass_t renderPass ) noexcept
{
    std::vector< elWidget_base * > onTopWidgets, onBottomWidgets,
        orderedWidgets;
    this->UpdateWindowDimensions();

    for ( auto child : this->GetChildren() )
    {
        if ( child == nullptr ) continue;

        if ( child->GetIsAlwaysOnTop() )
            onTopWidgets.emplace_back( child );
        else if ( child->GetIsAlwaysOnBottom() )
            onBottomWidgets.emplace_back( child );
        else
            orderedWidgets.emplace_back( child );
    }

    for ( auto child : onBottomWidgets )
        child->Draw( renderPass );

    for ( auto child : orderedWidgets )
        child->Draw( renderPass );

    for ( auto child : onTopWidgets )
        child->Draw( renderPass );
}

bool
elWidget_base::AdjustSizeToFitChildren() noexcept
{
    auto adjust = this->GetAdjustSizeToFitChildren();
    if ( adjust.x == false && adjust.y == false ) return false;

    glm::vec2 newSize         = glm::vec2( 0.0f );
    float     verticalBorders = this->GetBorderSize()[POSITION_TOP] +
                            this->GetBorderSize()[POSITION_BOTTOM];
    float horizontalBorders = this->GetBorderSize()[POSITION_LEFT] +
                              this->GetBorderSize()[POSITION_RIGHT];

    if ( adjust.x == true || adjust.y == true )
        for ( auto child : this->GetChildren() )
            if ( child != nullptr )
                newSize = glm::vec2(
                    std::max( child->GetSize().x + child->GetPosition().x +
                                  verticalBorders,
                              newSize.x ),
                    std::max( child->GetSize().y + child->GetPosition().y +
                                  horizontalBorders,
                              newSize.y ) );

    glm::vec2 originalSize = this->GetSize();
    if ( adjust.x == static_cast< int >( true ) ) originalSize.x = newSize.x;
    if ( adjust.y == static_cast< int >( true ) ) originalSize.y = newSize.y;
    if ( originalSize == this->GetSize() ) return false;

    this->SetSize( originalSize );
    return true;
}

void
elWidget_base::AlignWithOtherWidget() noexcept
{
    auto aligned = this->GetAlignedPositionWithWidget();
    if ( aligned.widget )
    {
        auto pos   = aligned.widget->GetPosition();
        auto myPos = this->GetPosition();
        if ( !aligned.xPos ) pos.x = myPos.x;
        if ( !aligned.yPos ) pos.y = myPos.y;
        this->SetPosition( pos );
    }
}

void
elWidget_base::UpdateColors() noexcept
{
    if ( !this->UpdateColorTransition() ) return;

    this->uniformBuffer->SetVariableData(
        0, UBO_COLOR, glm::value_ptr( this->GetBaseColorSwapAdjusted() ),
        4 * sizeof( GLfloat ) );
    this->uniformBuffer->SetVariableData(
        0, UBO_COLOR_ACCENT,
        glm::value_ptr( this->GetAccentColorSwapAdjusted() ),
        4 * sizeof( GLfloat ) );
    this->uniformBuffer->SetVariableData(
        0, UBO_GLOW_COLOR,
        glm::value_ptr( this->GetBaseGlowColorSwapAdjusted() ),
        4 * sizeof( GLfloat ) );
    this->uniformBuffer->SetVariableData(
        0, UBO_GLOW_COLOR_ACCENT,
        glm::value_ptr( this->GetAccentGlowColorSwapAdjusted() ),
        4 * sizeof( GLfloat ) );
}

void
elWidget_base::UpdateWindowResolution() noexcept
{
    this->uniformBuffer->SetVariableData(
        0, UBO_RESOLUTION,
        glm::value_ptr(
            glm::vec2( this->GetScreenWidth(), this->GetScreenHeight() ) ),
        2 * sizeof( GLfloat ) );
}

void
elWidget_base::UpdateEventColors() noexcept
{
    GLint doDrawEventColor = this->GetHasDisabledEventHandling();
    this->uniformBuffer->SetVariableData( 0, UBO_DO_DRAW_EVENT_COLOR,
                                          &doDrawEventColor, sizeof( GLint ) );
}

void
elWidget_base::AdjustStickySize() noexcept
{
    glm::vec2 currentSize = this->GetSize(), transformedSize = currentSize;
    if ( this->parent != nullptr )
    {
        auto stickToParentSize = this->GetStickToParentSize();
        if ( stickToParentSize[0] >= 0.0f )
            transformedSize.x =
                this->parent->GetContentSize().x - stickToParentSize[0];
        if ( stickToParentSize[1] >= 0.0f )
            transformedSize.y =
                this->parent->GetContentSize().y - stickToParentSize[1];

        if ( currentSize != transformedSize )
        {
            this->SetSize( transformedSize );
        }
    }
}

void
elWidget_base::ReshapeGeometry() noexcept
{
    this->AdjustStickySize();

    this->uniformBuffer->SetVariableData(
        0, UBO_BORDER_EVENT_INCREASE,
        glm::value_ptr( this->GetTransformedBorderEventIncrease() ),
        4 * sizeof( GLfloat ) );

    glm::vec2 trSize = this->GetTransformedSize();
    this->uniformBuffer->SetVariableData( 0, UBO_SIZE, glm::value_ptr( trSize ),
                                          2 * sizeof( GLfloat ) );

    glm::vec2 textureSize = trSize;
    if ( this->HasNonConformTextureSize() )
        textureSize = this->GetTransformedNonConformTextureSize();

    this->uniformBuffer->SetVariableData( 0, UBO_TEXTURE_SIZE,
                                          glm::value_ptr( textureSize ),
                                          2 * sizeof( GLfloat ) );

    // update relativePosition
    this->ReshapeAbsolutPosition();
    glm::vec2 trPosition = this->GetTransformedAbsolutPosition();
    this->uniformBuffer->SetVariableData(
        0, UBO_POSITION, glm::value_ptr( trPosition ), 2 * sizeof( GLfloat ) );

    // update border size
    glm::vec4 trBorderSize = this->GetTransformedBorderSize();
    this->uniformBuffer->SetVariableData(
        0, UBO_BORDERS, glm::value_ptr( trBorderSize ), 4 * sizeof( GLfloat ) );


    // update viewPort
    this->viewPort = glm::vec4( trPosition[1], trPosition[1] - trSize[1],
                                trPosition[0], trPosition[0] + trSize[0] );
    this->viewPortWithBorders =
        this->viewPort + glm::vec4( -trBorderSize[0], trBorderSize[1],
                                    trBorderSize[2], -trBorderSize[3] );
    if ( this->parent != nullptr )
    {
        this->viewPort = glm::vec4(
            // top
            std::min( this->parent->viewPortWithBorders[0], this->viewPort[0] ),
            // bottom
            std::max( this->parent->viewPortWithBorders[1], this->viewPort[1] ),
            // left
            std::max( this->parent->viewPortWithBorders[2], this->viewPort[2] ),
            // right
            std::min( this->parent->viewPortWithBorders[3],
                      this->viewPort[3] ) );
    }
    this->viewPortWithBorders = glm::vec4(
        std::min( this->viewPort[0], this->viewPortWithBorders[0] ),
        std::max( this->viewPort[1], this->viewPortWithBorders[1] ),
        std::max( this->viewPort[2], this->viewPortWithBorders[2] ),
        std::min( this->viewPort[3], this->viewPortWithBorders[3] ) );
    this->uniformBuffer->SetVariableData(
        0, UBO_VIEWPORT, glm::value_ptr( viewPort ), 4 * sizeof( GLfloat ) );

    this->AlignWithOtherWidget();
}

void
elWidget_base::ReshapeSettings() noexcept
{
    this->UpdateEventColors();
    this->UpdateWindowResolution();

    GLint value = this->GetDoInvertTextureCoordinates();
    this->uniformBuffer->SetVariableData( 0, UBO_DO_INVERT_TEXTURE_COORDINATES,
                                          &value, sizeof( GLint ) );
}

void
elWidget_base::Reshape()
{
    this->ReshapeGeometry();
    this->ReshapeSettings();

    // this is expensive
    for ( auto child : this->GetChildren() )
        if ( child != nullptr ) child->Reshape();

    if ( this->AdjustSizeToFitChildren() ) this->ReshapeGeometry();

    this->SetPositionOfStickyWidgets();
    // must be at the end since the methods above can request a
    // Method_Reshape call again
    this->runOnce.Reset( _Method_Reshape_ );
}

void
elWidget_base::SetState( const widgetStates_t state ) noexcept
{
    int isUnSelectable = this->GetIsUnselectable();

    widgetStates_t newState =
        ( this->doInheritStateFromParent ) ? this->parent->GetState() : state;

    if ( isUnSelectable == 1 && newState == STATE_SELECTED )
        newState = STATE_HOVER;

    if ( this->GetState() == newState ) return;
    auto previousState = this->GetState();

    this->UpdateState( newState );
    for ( auto widget : this->stateInheritanceVector )
    {
        widget->SetState( newState );
    }

    this->CallCallback( previousState, CallCondition::OnLeave );
    this->CallCallback( newState, CallCondition::OnEnter );

    this->SetupStateEvents( newState );

    if ( isUnSelectable == 1 && newState == STATE_SELECTED &&
         this->parent != nullptr )
        this->parent->SetState( STATE_SELECTED );

    if ( newState == STATE_SELECTED || newState == STATE_CLICKED )
        this->MoveWidgetToTop();
}

widgetType_t
elWidget_base::GetType() const noexcept
{
    return this->type;
}

std::vector< std::string >
elWidget_base::SeparatedStringToVector( const std::string &separator,
                                        const std::string &value ) noexcept
{
    std::vector< std::string > returnValue;
    std::string                temp = value;

    for ( auto pos = temp.find_first_of( separator ); pos != std::string::npos;
          temp     = temp.substr( pos + separator.size() ),
               pos = temp.find_first_of( separator ) )
        returnValue.push_back( temp.substr( 0, pos ) );

    if ( !temp.empty() ) returnValue.push_back( temp );

    return returnValue;
}

elWidget_base::NoAnimationShaderFileSet_e::NoAnimationShaderFileSet_e(
    const std::string &msg )
    : std::runtime_error( msg )
{
}

elWidget_base::NoAnimationShaderFileSet_e::~NoAnimationShaderFileSet_e()
{
}


} // namespace GuiGL
} // namespace el3D
