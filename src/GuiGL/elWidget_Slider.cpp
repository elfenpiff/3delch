#include "GuiGL/elWidget_Slider.hpp"

#include "GLAPI/elWindow.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "HighGL/elEventTexture.hpp"
#include "utils/convert.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Slider::elWidget_Slider( const constructor_t& constructor )
    : elWidget_base( constructor ),
      moveSliderToMouseClickEvent(
          this->CreateEventFor( UBO_EVENT_COLOR_INNER ) )
{
    this->description.text = this->Create< elWidget_Label >();
    this->button.widget    = this->Create< elWidget_Button >();
    this->CreateMovingSliderEvent();
    this->CreateMoveSliderToMouseClickEvent();
    this->SetSliderPosition( 0.0f );
}

void
elWidget_Slider::SetShowValueOnButton( const bool v, const float scaling,
                                       const uint64_t precision ) noexcept
{
    this->button.doShowValue   = v;
    this->button.scalingFactor = scaling;
    this->button.precision     = precision;
    this->UpdateSliderButtonText();
}

void
elWidget_Slider::SetIsHorizontal( const bool v ) noexcept
{
    if ( v != this->isHorizontal )
    {
        this->isHorizontal = v;
        this->runOnce.Call( _Method_Reshape_ );
    }
}

void
elWidget_Slider::SetDescriptionText( const std::string& text ) noexcept
{
    this->description.text->SetText( text );
    this->runOnce.Call( _Method_Reshape_ );
}

float
elWidget_Slider::GetSliderPosition() const noexcept
{
    return this->sliderPosition;
}

void
elWidget_Slider::SetSliderPosition( const float pos ) noexcept
{
    this->SetSliderPositionWithCallback( pos, true );
}

void
elWidget_Slider::UpdateSliderButtonText() noexcept
{

    if ( this->button.doShowValue )
    {
        float value = this->sliderPosition * this->button.scalingFactor;
        this->button.widget->SetText(
            utils::ToString( value, this->button.precision ) );
    }
    else
        this->button.widget->SetText( "" );
}

void
elWidget_Slider::SetSliderPositionWithCallback( const float pos,
                                                const bool doCallback ) noexcept
{
    if ( this->lastSliderPosition == pos ) return;
    this->sliderPosition = std::max( 0.0f, std::min( 1.0f, pos ) );
    this->runOnce.Call( _Method_Reshape_ );
    if ( doCallback && this->sliderPositionChangeCallback )
        this->CallCallable(
            [this]
            { this->sliderPositionChangeCallback( this->sliderPosition ); } );
    this->lastSliderPosition = this->sliderPosition;
    this->UpdateSliderButtonText();

    if ( this->sliderPosition > 0.5f && !this->description.isOnLeftSide )
    {
        this->description.text->MoveTo( this->GetDescriptionTextPosition() );
        this->description.isOnLeftSide = true;
    }
    else if ( this->sliderPosition <= 0.5f && this->description.isOnLeftSide )
    {
        this->description.text->MoveTo( this->GetDescriptionTextPosition() );
        this->description.isOnLeftSide = false;
    }
}

void
elWidget_Slider::SetSliderPositionChangeCallback(
    const std::function< void( const float ) >& f ) noexcept
{
    this->sliderPositionChangeCallback = f;
}

void
elWidget_Slider::MoveSliderToClickPosition()
{
    glm::vec2 clickPos = this->mouseCursor->GetCursorPosition();
    float     trSize, windowClickPos;

    auto borderSize = this->GetBorderSize();

    if ( !this->isHorizontal )
    {
        trSize         = this->GetContentSize().y;
        windowClickPos = clickPos.y - ( this->GetAbsolutPosition().y +
                                        borderSize[POSITION_TOP] );
    }
    else
    {
        trSize         = this->GetContentSize().x;
        windowClickPos = clickPos.x - ( this->GetAbsolutPosition().x +
                                        borderSize[POSITION_LEFT] );
    }

    this->SetSliderPosition( windowClickPos / trSize );
    this->button.widget->SetState( STATE_HOVER );
}

void
elWidget_Slider::CreateMoveSliderToMouseClickEvent()
{
    this->moveSliderToMouseClickEvent->SetCallback(
        [this]( const SDL_Event e )
        {
            if ( e.button.button == SDL_BUTTON_LEFT &&
                 !this->GetStickyExclusiveEvent().IsRegistered() )
            {
                this->MoveSliderToClickPosition();
            }
        } );
}

void
elWidget_Slider::CreateMovingSliderEvent()
{
    this->button.widget->SetCallback(
        [this]
        {
            this->SetState( STATE_SELECTED );
            this->MoveWidgetToTop();
            this->SliderCallback();
        },
        STATE_CLICKED, CallCondition::InState );
}

void
elWidget_Slider::SliderCallback()
{
    auto& stickyExclusiveEvent = this->GetStickyExclusiveEvent();
    if ( !stickyExclusiveEvent.IsRegistered() )
    {
        stickyExclusiveEvent.Reset();

        stickyExclusiveEvent
            .SetRemovalCondition(
                []( const SDL_Event e )
                { return e.button.type == SDL_MOUSEBUTTONUP; } )
            .SetCallback( [this]( const SDL_Event )
                          { this->MoveSliderButton(); } )
            .SetAfterRemovalCallback(
                [&]( const SDL_Event )
                {
                    uint32_t eventColor = this->GetColorToCurrentEvent();

                    if ( !this->DoesEventColorBelongsToMeOrChild( eventColor ) )
                    {
                        this->SetState( STATE_DEFAULT );
                        this->button.widget->SetState( STATE_DEFAULT );
                    }
                } );
        stickyExclusiveEvent.Register();
    }
}

void
elWidget_Slider::MoveSliderButton()
{
    glm::vec2 mouseMovement = this->mouseCursor->GetRelativeCursorPosition();
    auto      position      = this->button.widget->GetPosition();
    if ( this->isHorizontal )
    {
        position[0] += mouseMovement.x;
        position[0] = std::max( 0.0f, position[0] );

        float maxPosition = this->GetMaxPosition().x;
        position[0]       = std::min( maxPosition, position[0] );

        this->SetSliderPosition( position[0] / maxPosition );
    }
    else
    {
        position[1] += mouseMovement.y;
        position[1] = std::max( 0.0f, position[1] );

        float maxPosition = this->GetMaxPosition().y;
        position[1]       = std::min( maxPosition, position[1] );

        this->SetSliderPosition( position[1] / maxPosition );
    }

    this->button.widget->SetPosition( { position[0], position[1] } );
}

glm::vec2
elWidget_Slider::GetMaxPosition()
{
    return this->GetContentSize() - this->button.widget->GetSize();
}

void
elWidget_Slider::SetRelativeSliderButtonSize( const float v ) noexcept
{
    this->relativeSliderButtonSize = v;
}

void
elWidget_Slider::Reshape()
{
    float height = this->GetContentSize().y;
    float width  = this->GetContentSize().x;

    // updating slider button width
    if ( this->isHorizontal )
        width = std::max( 10.0f, width * this->relativeSliderButtonSize );
    else
        height = std::max( 10.0f, height * this->relativeSliderButtonSize );

    this->button.widget->SetSize( { width, height } );

    // updating slider button position from sliderPosition
    auto position = this->button.widget->GetPosition();
    if ( this->isHorizontal )
        position[0] = this->sliderPosition * this->GetMaxPosition().x;
    else
        position[1] = this->sliderPosition * this->GetMaxPosition().y;
    this->button.widget->SetPosition( { position[0], position[1] } );
    this->description.text->SetPosition( this->GetDescriptionTextPosition() );

    elWidget_base::Reshape();
}

glm::vec2
elWidget_Slider::GetDescriptionTextPosition() const noexcept
{
    glm::vec2 retVal{ 0.0f, 0.0f };

    if ( this->sliderPosition <= 0.5f )
    {
        if ( this->isHorizontal )
            retVal = glm::vec2{
                this->GetContentSize().x -
                    static_cast< float >(
                        this->description.text->GetTextSize().width ),
                0.0f };
        else
            retVal = glm::vec2{
                0.0f, this->GetContentSize().y -
                          static_cast< float >(
                              this->description.text->GetFontHeight() ) };
    }


    return retVal;
}

} // namespace GuiGL
} // namespace el3D
