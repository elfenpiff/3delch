#include "GuiGL/elWidget_FileBrowser.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Table.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "GuiGL/elWidget_Tree.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "utils/elFileSystem.hpp"


// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace fs = std::filesystem;

namespace el3D
{
namespace GuiGL
{
elWidget_FileBrowser::elWidget_FileBrowser( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->openCallback  = this->CreateCallback();
    this->abortCallback = this->CreateCallback();

    this->directoryTree = this->Create< elWidget_Tree >();
    this->directoryTree->AllowDoubleEntries( false );
    this->directoryTree->SetPosition( { 0, 0 } );
    this->directoryTree->SetSize( { 300, 0 } );
    this->directoryTree->SetStickToParentSize( { -1, 0 } );
    this->directoryTree->SetEntryOpenCallback(
        [this]( const std::vector< std::string >& root,
                const std::string&                directory )
        { this->OpenDirectoryCallback( root, directory ); } );
    this->directoryTree->SetClickOnEntryCallback(
        [this]( const std::vector< std::string >& root,
                const std::string&                directory )
        { this->ClickDirectoryCallback( root, directory ); } );

    this->setRootButton = this->Create< elWidget_Button >();
    this->setRootButton->SetText( " as root " );
    this->setRootButton->SetPosition( { this->directoryTree->GetSize().x, 0 } );
    this->setRootButton->SetAdjustSizeToFitChildren( true, false );
    this->setRootButton->SetClickCallback(
        [&]() { this->SetRootPath( this->pathEntry->GetText() ); } );

    this->pathEntry = this->Create< elWidget_Entry >();
    this->pathEntry->SetPositionPointOfOrigin( POSITION_TOP_RIGHT );
    this->pathEntry->SetSize( { 0.0f, this->setRootButton->GetSize().y } );
    this->pathEntry->SetStickToParentSize(
        { this->directoryTree->GetSize().x + this->setRootButton->GetSize().x,
          -1.0f } );

    this->fileTable = this->Create< elWidget_Table >();
    this->fileTable->SetElementBaseType( TYPE_LABEL );
    this->fileTable->SetPositionPointOfOrigin( POSITION_BOTTOM_RIGHT );
    this->fileTable->SetStickToParentSize(
        { this->directoryTree->GetSize().x,
          this->setRootButton->GetSize().y } );
    this->fileTable->SetElementDefaultDimensions( 200, 30 );
    this->fileTable->SetHoveringMode( elWidget_Table::TABLEMODE_ROW );
    this->fileTable->SetSelectionMode( elWidget_Table::TABLEMODE_ROW );

    this->SetHasTitlebar( true );
    this->GetTitlebar()->SetTitleText( "asd" );
    this->titleBarOpen = this->GetTitlebar()->Create< elWidget_Button >();
    this->titleBarOpen->SetText( this->openButtonText );
    this->titleBarOpen->SetAdjustSizeToFitChildren( false, true );
    this->titleBarOpen->SetPositionPointOfOrigin( POSITION_TOP_RIGHT );
    this->titleBarOpen->SetClickCallback( [&]() { this->ClickOnOpen(); } );

    this->titleBarAbort = this->GetTitlebar()->Create< elWidget_Button >();
    this->titleBarAbort->SetText( this->abortButtonText );
    this->titleBarAbort->SetAdjustSizeToFitChildren( false, true );
    this->titleBarAbort->SetPositionPointOfOrigin( POSITION_TOP_RIGHT );
    this->titleBarAbort->SetPosition(
        { this->titleBarOpen->GetSize().x, 0.0f } );
    this->titleBarAbort->SetClickCallback( [&]() { this->ClickOnAbort(); } );

    this->SetRootPath( utils::elFileSystem::GetCurrentPath() );
    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_FileBrowser::Reshape()
{
    elWidget_base::Reshape();

    this->pathEntry->SetStickToParentSize(
        { this->directoryTree->GetSize().x + this->setRootButton->GetSize().x,
          -1.0f } );

    float fileRowSize =
        std::max( 250.0f, this->fileTable->GetContentSize().x - 200.0f );

    this->fileTable->SetColumnWidth( 0, 100 );
    this->fileTable->SetColumnWidth( 2, 100 );
    this->fileTable->SetColumnWidth( 1, fileRowSize );
    elWidget_base::Reshape();
}

elWidget_FileBrowser::~elWidget_FileBrowser()
{
}

void
elWidget_FileBrowser::ClickOnOpen() noexcept
{
    this->openCallback->Call();
    this->RemoveThis();
}

void
elWidget_FileBrowser::ClickOnAbort() noexcept
{
    this->abortCallback->Call();
    this->RemoveThis();
}

void
elWidget_FileBrowser::SetAbortCallback(
    const std::function< void() >& v ) noexcept
{
    this->abortCallback->Set( v );
}

void
elWidget_FileBrowser::SetOpenCallback(
    const std::function< void() >& v ) noexcept
{
    this->openCallback->Set( v );
}

void
elWidget_FileBrowser::SetOpenButtonText( const std::string& v ) noexcept
{
    this->openButtonText = v;
    this->titleBarOpen->SetText( v );
}

void
elWidget_FileBrowser::SetAbortButtonText( const std::string& v ) noexcept
{
    this->abortButtonText = v;
    this->titleBarAbort->SetText( v );
}

void
elWidget_FileBrowser::ClickDirectoryCallback(
    const std::vector< std::string >& root,
    const std::string&                directory ) noexcept
{
    if ( directory.size() >= 3 && directory.substr( 0, 3 ) == "../" )
    {
        // last character is always elFileSystem::PATH_DELIMITER
        std::string temp =
            this->rootPath.substr( 0, this->rootPath.size() - 1 );
        if ( !temp.empty() )
        {
            auto pos = temp.find_last_of( utils::elFileSystem::PATH_DELIMITER );
            if ( pos != std::string::npos )
            {
                this->SetRootPath( temp.substr( 0, pos ) );
            }
        }
    }
    else
    {
        auto parentDirectory =
            utils::elFileSystem::ConvertStringToVectorPath( this->rootPath );
        if ( !parentDirectory.empty() ) parentDirectory.pop_back();

        this->FillFileTable(
            utils::elFileSystem::ConvertVectorPathToString( parentDirectory ) +
            utils::elFileSystem::PATH_DELIMITER +
            utils::elFileSystem::ConvertVectorPathToString( root ) +
            utils::elFileSystem::PATH_DELIMITER + directory );
    }
}

void
elWidget_FileBrowser::OpenDirectoryCallback(
    const std::vector< std::string >& root,
    const std::string&                directory ) noexcept
{
    std::string path =
        ( root.size() != 0 )
            ? utils::elFileSystem::ConvertVectorPathToString( root ) +
                  utils::elFileSystem::PATH_DELIMITER + directory
            : directory;

    auto parentDirectory =
        utils::elFileSystem::ConvertStringToVectorPath( this->rootPath );
    if ( !parentDirectory.empty() ) parentDirectory.pop_back();

    auto content = utils::elFileSystem::GetDirectoryContents(
        utils::elFileSystem::ConvertVectorPathToString( parentDirectory ) +
        utils::elFileSystem::PATH_DELIMITER + path );

    bb::for_each( content,
                  [&]( auto& e )
                  {
                      if ( e.type == fs::file_type::directory )
                      {
                          this->FillDirectoryTree(
                              std::string( e.path.string() )
                                  .substr( this->rootPath.size() ),
                              1 );
                      }
                  } );
}

void
elWidget_FileBrowser::SetRootPath( const std::string& v ) noexcept
{
    this->directoryTree->Clear();

    if ( v.empty() )
    {
        this->rootPath = std::string( utils::elFileSystem::ROOT_PATH );
        this->directoryTree->AddEntries( {}, { this->rootPath } );

        this->FillDirectoryTree( "", 1 );
        this->FillFileTable( this->rootPath );
    }
    else
    {
        this->rootPath = utils::elFileSystem::GetAbsolutPath( v );
        if ( this->rootPath.back() != utils::elFileSystem::PATH_DELIMITER )
            this->rootPath.push_back( utils::elFileSystem::PATH_DELIMITER );

        auto rootPathVector =
            utils::elFileSystem::ConvertStringToVectorPath( this->rootPath );

        if ( rootPathVector.size() == 1 )
            this->directoryTree->AddEntries( {},
                                             { "../", rootPathVector.back() } );
        else if ( rootPathVector.size() > 1 )
            this->directoryTree->AddEntries(
                {}, { "../" + rootPathVector[rootPathVector.size() - 2],
                      rootPathVector.back() } );

        this->FillDirectoryTree( "", 1 );
        this->FillFileTable( this->rootPath );
    }
}

void
elWidget_FileBrowser::FillDirectoryTree( const std::string& parentPath,
                                         const size_t       depth ) noexcept
{
    if ( depth == 0 ) return;

    std::string path = this->rootPath;
    if ( !parentPath.empty() )
        path += utils::elFileSystem::PATH_DELIMITER + parentPath;

    path = utils::elFileSystem::RemoveDoubleDelimiterFromStringPath( path );


    auto temp = utils::elFileSystem::GetDirectoryContents( path );
    std::vector< std::string > entries;
    bb::for_each( temp,
                  [&]( auto& e )
                  {
                      if ( e.type == fs::file_type::directory )
                      {
                          entries.emplace_back( e.path.filename().string() );
                      }
                  } );

    bb::sort( entries );

    auto currentDirectoryVector =
        utils::elFileSystem::ConvertStringToVectorPath( this->rootPath );
    auto currentDirectory = ( currentDirectoryVector.empty() )
                                ? std::string( utils::elFileSystem::ROOT_PATH )
                                : currentDirectoryVector.back();

    if ( parentPath.empty() )
    {
        this->directoryTree->AddEntries( { currentDirectory }, entries );
    }
    else
    {
        auto vectorPath =
            utils::elFileSystem::ConvertStringToVectorPath( parentPath );
        vectorPath.insert( vectorPath.begin(), currentDirectory );
        this->directoryTree->AddEntries( vectorPath, entries );
    }

    for ( auto directory : entries )
        this->FillDirectoryTree( parentPath + directory, depth - 1 );
}

void
elWidget_FileBrowser::FillFileTable( const std::string& path ) noexcept
{
    this->currentPath =
        utils::elFileSystem::RemoveDoubleDelimiterFromStringPath( path );
    this->pathEntry->SetText( this->currentPath );
    auto content =
        utils::elFileSystem::GetDirectoryContents( this->currentPath );
    bb::sort( content, []( auto& l, auto& r ) { return l.path < r.path; } );
    bb::erase_if( content, []( auto& e )
                  { return e.type == fs::file_type::directory; } );

    size_t numberOfFileProperties = 3;
    size_t numberOfFiles          = content.size();

    this->fileTable->ResetNumberOfColsAndRows( numberOfFileProperties,
                                               numberOfFiles );

    for ( size_t i = 0; i < numberOfFiles; ++i )
    {
        static_cast< elWidget_Label* >(
            this->fileTable->GetElement( FILE_TYPE, i ) )
            ->SetText(
                utils::elFileSystem::FileTypeToString( content[i].type ) );
        static_cast< elWidget_Label* >(
            this->fileTable->GetElement( FILE_NAME, i ) )
            ->SetText( content[i].path.filename().string() );
        static_cast< elWidget_Label* >(
            this->fileTable->GetElement( FILE_SIZE, i ) )
            ->SetText( content[i].sizeHumanReadable );
    }

    if ( numberOfFiles > 0 && this->fileTable->GetHasTitleRow() )
    {
        this->fileTable->SetTitleRowButtonText( 0, "type" );
        this->fileTable->SetTitleRowButtonText( 1, "name" );
        this->fileTable->SetTitleRowButtonText( 2, "size" );
    }

    this->runOnce.Call( _Method_Reshape_ );
}

std::vector< std::string >
elWidget_FileBrowser::GetSelection() const noexcept
{
    std::vector< std::string > retVal;
    auto                       selection = this->fileTable->GetSelection();

    for ( auto s : selection )
        retVal.emplace_back( this->currentPath +
                             utils::elFileSystem::PATH_DELIMITER +
                             static_cast< const elWidget_Label* >(
                                 this->fileTable->GetElement( FILE_NAME, s.y ) )
                                 ->GetText() );

    return retVal;
}

} // namespace GuiGL
} // namespace el3D
