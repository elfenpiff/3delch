#include "GuiGL/elWidget_Accordion.hpp"

#include "GuiGL/elWidget_Button.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Accordion::elWidget_Accordion(
    const constructor_t& constructor ) noexcept
    : elWidget_Window( constructor )
{
}

void
elWidget_Accordion::Reshape()
{
    auto& accordions = this->accordionFactory.GetProducts< accordion_t >();

    float currentPosition = 0.0f;
    for ( auto& a : accordions )
    {
        a->button->SetPosition( { 0.0f, currentPosition } );
        currentPosition += a->button->GetSize().y;
        a->frame->SetPosition( { 0.0f, currentPosition } );
        if ( a->frame->DoRenderWidget() )
        {
            float contentSize = a->frame->GetContentDimension().y;
            currentPosition += contentSize;
            a->frame->SetSize( { 0.0f, contentSize } );
        }
    }

    elWidget_Window::Reshape();
}

bb::product_ptr< elWidget_Accordion::accordion_t >
elWidget_Accordion::CreateAccordion( const std::string&    name,
                                     const AccordionState& state ) noexcept
{
    auto newAccordion =
        this->accordionFactory.CreateProductWithOnRemoveCallback< accordion_t >(
            [this]( accordion_t* accordion )
            { this->RemoveAccordion( *accordion ); } );

    newAccordion->frame = this->Create< elWidget_base >();
    newAccordion->frame->SetIsResizable( false );
    newAccordion->frame->SetIsMovable( false );
    newAccordion->frame->SetDoRenderWidget( state == AccordionState::Open );
    newAccordion->frame->SetStickToParentSize( { 0.0f, -1.0f } );
    newAccordion->frame->SetSize( { 0.0f, 0.0f } );

    newAccordion->button = this->Create< elWidget_Button >();
    newAccordion->button->SetText( name );
    newAccordion->button->SetStickToParentSize( { 0.0f, -1.0f } );
    newAccordion->button->SetClickCallback(
        [this, accordion = newAccordion.Get()]
        { this->ToggleAccordion( *accordion ); } );

    this->runOnce.Call( _Method_Reshape_ );
    return newAccordion;
}

void
elWidget_Accordion::RemoveAccordion( accordion_t& accordion ) noexcept
{
    auto& accordions = this->accordionFactory.GetProducts< accordion_t >();

    bool  doMoveAccordions = false;
    float moveFactor       = 0.0f;
    for ( auto& a : accordions )
    {
        if ( doMoveAccordions )
        {
            a->button->MoveTo( a->button->GetPosition() -
                               glm::vec2{ 0.0f, moveFactor } );
            a->frame->MoveTo( a->frame->GetPosition() -
                              glm::vec2{ 0.0f, moveFactor } );
        }

        if ( a.Get() == &accordion )
        {
            moveFactor =
                accordion.frame->GetSize().y + accordion.button->GetSize().y;
            doMoveAccordions = true;
        }
    }
}

void
elWidget_Accordion::ToggleAccordion( accordion_t& accordion ) noexcept
{
    auto& accordions = this->accordionFactory.GetProducts< accordion_t >();

    bool  doMoveAccordions = false;
    float moveFactor       = 0.0f;
    for ( auto& a : accordions )
    {
        if ( doMoveAccordions )
        {
            a->button->MoveTo( a->button->GetPosition() +
                               glm::vec2{ 0.0f, moveFactor } );
            a->frame->MoveTo( a->frame->GetPosition() +
                              glm::vec2{ 0.0f, moveFactor } );
        }

        if ( a.Get() == &accordion )
        {
            if ( accordion.frame->DoRenderWidget() )
            {
                moveFactor = -accordion.frame->GetSize().y;
                accordion.frame->ResizeTo(
                    { 0.0f, 0.0f }, [a = &accordion]
                    { a->frame->SetDoRenderWidget( false ); } );
            }
            else
            {
                moveFactor = accordion.frame->GetContentDimension().y;
                accordion.frame->SetDoRenderWidget( true );
                accordion.frame->ResizeTo( { 0.0f, moveFactor } );
            }

            doMoveAccordions = true;
        }
    }
}
} // namespace GuiGL
} // namespace el3D
