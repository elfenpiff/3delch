#include "GuiGL/elWidget_Checkbutton.hpp"

#include "GuiGL/elWidget_ShaderAnimation.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Checkbutton::elWidget_Checkbutton( const constructor_t &constructor )
    : elWidget_base( constructor )
{
    this->ThrowIfPropertyIsUnset< NoAnimationShaderFileSet_e >(
        PROPERTY_ANIMATION_SHADER_FILE );
    std::string animationFile = this->GetAnimationShaderFile();

    this->slider = this->Create< elWidget_ShaderAnimation >();
    this->slider->SetUp( animationFile );
    this->slider->SetPosition( { 0, 0 } );
    this->slider->SetStickToParentSize( { true, true } );
    this->slider->SetDoInfinitLoop( false );
}

void
elWidget_Checkbutton::SetState( const widgetStates_t state ) noexcept
{
    if ( state == STATE_SELECTED )
    {
        this->SetIsChecked( !this->isChecked );
    }
    elWidget_base::SetState( state );
    this->slider->SetState( this->GetState() );
}

bool
elWidget_Checkbutton::IsChecked() const noexcept
{
    return this->isChecked;
}

void
elWidget_Checkbutton::SetIsChecked( const bool v ) noexcept
{
    if ( v != this->isChecked )
    {
        this->isChecked = v;
        if ( this->isChecked )
            this->slider->Start();
        else
            this->slider->ReverseStart();
    }
}


} // namespace GuiGL
} // namespace el3D
