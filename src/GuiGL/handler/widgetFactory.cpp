#include "GuiGL/handler/widgetFactory.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Checkbutton.hpp"
#include "GuiGL/elWidget_Dropbutton.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_FileBrowser.hpp"
#include "GuiGL/elWidget_Graph.hpp"
#include "GuiGL/elWidget_Image.hpp"
#include "GuiGL/elWidget_InfoMessage.hpp"
#include "GuiGL/elWidget_KeyValuePair.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Listbox.hpp"
#include "GuiGL/elWidget_Menu.hpp"
#include "GuiGL/elWidget_Menubar.hpp"
#include "GuiGL/elWidget_MouseCursor.hpp"
#include "GuiGL/elWidget_PopupMenu.hpp"
#include "GuiGL/elWidget_Progress.hpp"
#include "GuiGL/elWidget_Selection.hpp"
#include "GuiGL/elWidget_ShaderAnimation.hpp"
#include "GuiGL/elWidget_Slider.hpp"
#include "GuiGL/elWidget_TabbedWindow.hpp"
#include "GuiGL/elWidget_Table.hpp"
#include "GuiGL/elWidget_TextCursor.hpp"
#include "GuiGL/elWidget_Textbox.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "GuiGL/elWidget_Tree.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "GuiGL/elWidget_YesNoDialog.hpp"
#include "GuiGL/elWidget_base.hpp"

namespace el3D
{
namespace GuiGL
{
namespace handler
{
widgetFactory::widgetFactory(
    elWidget_base* me, elWidget_base* parent,
    GLAPI::elEvent* const         stickyExclusiveEvent,
    HighGL::elEventTexture* const eventTexture,
    GLAPI::elEventHandler* const  eventHandler,
    const GLAPI::elWindow* const window, GLAPI::elFontCache* const fontCache,
    GuiGL::elWidget_MouseCursor* const mouseCursor,
    CallbackContainer_t* const         callbackContainer,
    const std::function< size_t( HighGL::elShaderTexture* ) >&
                                           shaderTextureRegistrator,
    const std::function< void( size_t ) >& shaderTextureDeregistrator,
    const int shaderVersion, const lua::elConfigHandler* theme,
    const OpenGL::elShaderProgram* const shader,
    const OpenGL::elShaderProgram* const glowShader, const size_t* screenWidth,
    const size_t* screenHeight, const float glowScaling,
    const widgetStates_t* widgetStatePointer )
    : me( me ), parent( parent ), stickyExclusiveEvent( stickyExclusiveEvent ),
      eventTexture( eventTexture ), eventHandler( eventHandler ),
      window( window ), fontCache( fontCache ), mouseCursor( mouseCursor ),
      callbackContainer( callbackContainer ),
      shaderTextureRegistrator( shaderTextureRegistrator ),
      shaderTextureDeregistrator( shaderTextureDeregistrator ),
      shaderVersion( shaderVersion ), widgetStatePointer( widgetStatePointer ),
      theme( theme ), screenWidth( screenWidth ), screenHeight( screenHeight ),
      shader( shader ), glowShader( glowShader ), glowScaling( glowScaling )
{
}

widgetFactory::~widgetFactory()
{
    if ( this->titleBar != nullptr )
    {
        delete this->titleBar;
    }
}

const lua::elConfigHandler*
widgetFactory::GetTheme() noexcept
{
    return this->theme;
}

size_t
widgetFactory::GetScreenWidth() const noexcept
{
    return *this->screenWidth;
}

size_t
widgetFactory::GetScreenHeight() const noexcept
{
    return *this->screenHeight;
}

const OpenGL::elShaderProgram*
widgetFactory::GetShader() const noexcept
{
    return this->shader;
}

const OpenGL::elShaderProgram*
widgetFactory::GetGlowShader() const noexcept
{
    return this->glowShader;
}

void
widgetFactory::RemoveThis() noexcept
{
    widgetFactory::RemoveFromParent( this->me );
}

void
widgetFactory::DeleteWidget( elWidget_base* const widget ) noexcept
{
    delete widget;
}

void
widgetFactory::RemoveFromParent( elWidget_base* const widget ) noexcept
{
    if ( widget->parent == nullptr ) return;

    widget->parent->Remove( widget );
}

void
widgetFactory::Remove( elWidget_base* widget ) noexcept
{
    auto iter = std::find( this->widgets.begin(), this->widgets.end(), widget );
    if ( iter != this->widgets.end() )
    {
        *iter = nullptr;
        if ( widget->GetType() == TYPE_TITLEBAR )
        {
            // order is important since SetHasTitlebar checks if titleBar !=
            // nullptr
            this->titleBar = nullptr;
            this->me->SetHasTitlebar( false );
        }
        widget->parent                 = nullptr;
        this->removeNullptrFromWidgets = true;
    }
}

const std::vector< elWidget_base* >&
widgetFactory::GetChildren() const noexcept
{
    return this->widgets;
}

elWidget_Titlebar*
widgetFactory::GetTitlebar() noexcept
{
    return this->titleBar;
}

void
widgetFactory::PrepareWidget( elWidget_base* w ) noexcept
{
    if ( *this->widgetStatePointer == STATE_INACTIVE )
        w->SetState( STATE_INACTIVE );

    if ( this->me->GetHasMultiSelection() )
        w->CreateDeMultiSelectEvent();
    else
        w->CreateDeSingleSelectEvent();

    auto childPosition = this->me->GetPositionPointOfOriginForChilds();
    if ( childPosition != POSITION_END )
        w->SetPositionPointOfOrigin( childPosition );

    this->me->runOnce.Call( _Method_Reshape_ );
}

void
widgetFactory::PrepareTitlebar() noexcept
{
    this->me->SetHasTitlebar( true );
    this->PrepareWidget( static_cast< elWidget_base* >( this->titleBar ) );
    this->titleBar->SetInheritStateFromParent( true );
}

void
widgetFactory::MoveWidgetToTop() noexcept
{
    if ( this->me->parent != nullptr )
    {
        this->me->parent->MoveMyWidgetToTop( this->me );
        this->me->parent->MoveWidgetToTop();
    }
}

void
widgetFactory::MoveMyWidgetToTop( elWidget_base* const widget ) noexcept
{
    if ( this->widgets.empty() || this->widgets.back() == widget ) return;
    auto iter = bb::find( this->widgets, widget );

    if ( iter != this->widgets.end() )
    {
        this->widgets.erase( iter );
        this->widgets.emplace_back( widget );
    }
}

elWidget_base*
widgetFactory::CreateTitlebar() noexcept
{
    elWidget_base* returnValue{ nullptr };
    constructor_t  constructor{ this->me,
                               this->shader,
                               this->glowShader,
                               this->eventTexture,
                               this->eventHandler,
                               this->stickyExclusiveEvent,
                               this->window,
                               this->fontCache,
                               this->mouseCursor,
                               this->shaderTextureRegistrator,
                               this->shaderTextureDeregistrator,
                               this->shaderVersion,
                               this->theme,
                               TYPE_TITLEBAR,
                               this->screenWidth,
                               this->screenHeight,
                               "",
                               TYPE_END,
                               this->glowScaling,
                               *this->callbackContainer };

    returnValue = new elWidget_Titlebar( constructor );

    this->titleBar = static_cast< elWidget_Titlebar* >( returnValue );
    this->PrepareTitlebar();
    this->widgets.emplace_back( returnValue );

    return returnValue;
}


} // namespace handler
} // namespace GuiGL
} // namespace el3D
