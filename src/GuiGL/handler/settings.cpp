#include "GuiGL/handler/settings.hpp"

#include "GLAPI/elWindow.hpp"
#include "GuiGL/common.hpp"
#include "GuiGL/elWidget_Titlebar.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "buildingBlocks/CoreGuidelines.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "logging/elLog.hpp"
#include "lua/elConfigHandler.hpp"
#include "utils/convert.hpp"
#include "utils/elRunOnce.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
namespace handler
{
static std::map< std::vector< std::string >, utils::elClassSettings >
    configCache;

settings::settings( elWidget_base* me, const lua::elConfigHandler* theme,
                    const std::string& className,
                    const widgetType_t enforcedThemeParent )
    : widgetSettings( widgetProperty ), me( me ), className( className ),
      theme( theme ), enforcedThemeParent( enforcedThemeParent )
{
    this->SetUpdateCallback(
        [this]
        {
            if ( this->HasActiveAnimations() )
                this->me->runOnce.Call( _Method_Reshape_ );
        } );
}

settings::~settings()
{
    for ( auto& w : this->stickyWidgetsSubscriber )
        w.widget->stickyWidgetSubcribedTo = nullptr;
    this->stickyWidgetsSubscriber.clear();

    this->UnsetStickToWidget();
}

void
settings::SetDisableEventHandling( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_DISABLE_EVENT_HANDLING,
                              std::vector< int >{ v } );
    this->me->SetActivateEventHandling( !v );
    this->me->runOnce.Call( _Method_Reshape_ );
}

bool
settings::GetHasDisabledEventHandling() noexcept
{
    return this->widgetSettings.Get< int >(
        PROPERTY_DISABLE_EVENT_HANDLING )[0];
}

bool
settings::GetDoInvertTextureCoordinates() const noexcept
{
    return this->doInvertTextureCoordinates;
}

void
settings::SetDoInvertTextureCoordinates( const bool v ) noexcept
{
    if ( v == this->doInvertTextureCoordinates ) return;
    this->doInvertTextureCoordinates = v;
    this->me->parent->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetBorderSize( const glm::vec4& border )
{
    glm::vec4 adjustedBorder = this->RoundVector( border * GetDPIScaling() );
    this->widgetSettings.Set(
        PROPERTY_BORDER_SIZE,
        std::vector< float >{
            adjustedBorder[POSITION_TOP], adjustedBorder[POSITION_BOTTOM],
            adjustedBorder[POSITION_LEFT], adjustedBorder[POSITION_RIGHT] } );
    this->me->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetPosition( const glm::vec2& pos )
{
    glm::vec2 adjustedPos = this->RoundVector( pos * GetDPIScaling() );

    this->widgetSettings.Set(
        PROPERTY_POSITION,
        std::vector< float >{ adjustedPos.x, adjustedPos.y } );
    this->me->runOnce.Call( _Method_Reshape_ );
    if ( this->me->parent != nullptr &&
         ( this->me->parent->hasVerticalScrolling ||
           this->me->parent->hasHorizontalScrolling ) )
        this->me->parent->runOnce.Call( _Method_Reshape_ );

    this->SetPositionOfStickyWidgets();
}

void
settings::SetPositionPointOfOrigin( const positionCorner_t v,
                                    const bool doKeepPosition ) noexcept
{
    if ( this->stickyWidgetSubcribedTo != nullptr ) return;

    for ( auto& w : this->stickyWidgetsSubscriber )
        w.widget->SetPositionPointOfOrigin( this->GetPositionPointOfOrigin() );

    this->widgetSettings.Set( PROPERTY_POSITION_POINT_OF_ORIGIN,
                              std::vector< int >{ v } );
    if ( doKeepPosition )
    {
        auto originalPosition = this->GetPositionFromAbsolutPosition();
        this->SetPosition( originalPosition );
    }

    this->me->runOnce.Call( _Method_Reshape_ );
    if ( this->me->parent != nullptr &&
         ( this->me->parent->hasVerticalScrolling ||
           this->me->parent->hasHorizontalScrolling ) )
        this->me->parent->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetPositionPointOfOriginForChilds(
    const positionCorner_t v, const bool doKeepPosition ) noexcept
{
    this->widgetSettings.Set( PROPERTY_POSITION_POINT_OF_ORIGIN_FOR_CHILDS,
                              std::vector< int >{ v } );
    for ( auto& w : this->me->GetChildren() )
        if ( w != nullptr ) w->SetPositionPointOfOrigin( v, doKeepPosition );

    this->me->parent->runOnce.Call( _Method_Reshape_ );
}

glm::vec2
settings::GetPosition() const noexcept
{
    auto pos = this->widgetSettings.Get< float >( PROPERTY_POSITION );
    return glm::vec2( pos[0], pos[1] ) / GetDPIScaling();
}

positionCorner_t
settings::GetPositionPointOfOriginForChilds() const noexcept
{
    return static_cast< positionCorner_t >( this->widgetSettings.Get< int >(
        PROPERTY_POSITION_POINT_OF_ORIGIN_FOR_CHILDS )[0] );
}

void
settings::SetIsAlwaysOnTop( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_IS_ALWAYS_ON_TOP,
                              std::vector< int >{ v } );
}

void
settings::SetIsAlwaysOnBottom( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_IS_ALWAYS_ON_BOTTOM,
                              std::vector< int >{ v } );
}

void
settings::SetHasVerticalScrolling( const bool v ) noexcept
{
    this->hasVerticalScrolling = v;
}

void
settings::SetHasHorizontalScrolling( const bool v ) noexcept
{
    this->hasHorizontalScrolling = v;
}

void
settings::SetSize( const glm::vec2& size )
{
    glm::vec2 adjustedSize = this->RoundVector( size * GetDPIScaling() );
    auto borderSize = this->widgetSettings.Get< float >( PROPERTY_BORDER_SIZE );
    adjustedSize    = {
        std::max( adjustedSize.x,
                     borderSize[POSITION_LEFT] + borderSize[POSITION_RIGHT] ),
        std::max( adjustedSize.y,
                     borderSize[POSITION_TOP] + borderSize[POSITION_BOTTOM] ) };

    this->widgetSettings.Set(
        PROPERTY_SIZE, std::vector< float >{ adjustedSize.x, adjustedSize.y } );

    if ( this->me->parent != nullptr )
        this->me->parent->runOnce.Call( _Method_Reshape_ );
    else
        this->me->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetHasFixedPosition( const bool xAxis, const bool yAxis ) noexcept
{
    this->widgetSettings.Set( PROPERTY_HAS_FIXED_POSITION,
                              std::vector< int >{ xAxis, yAxis } );
    this->me->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetStickToParentSize( const glm::vec2& v ) noexcept
{
    glm::vec2 adjustedSize = this->RoundVector( v * GetDPIScaling() );
    this->widgetSettings.Set(
        PROPERTY_STICK_TO_PARENT_SIZE,
        std::vector< float >{ adjustedSize.x, adjustedSize.y } );
    this->me->runOnce.Call( _Method_Reshape_ );
}

void
settings::SetBaseColorForState( const glm::vec4&     color,
                                const widgetStates_t state )
{
    size_t offset               = ( this->GetSwapColor() ) ? 4 : 0;
    this->currentColor.doUpdate = true;
    this->widgetSettings.Set( state, color, offset );
}

void
settings::SetAccentColorForState( const glm::vec4&     color,
                                  const widgetStates_t state )
{
    size_t offset               = ( this->GetSwapColor() ) ? 0 : 4;
    this->currentColor.doUpdate = true;
    this->widgetSettings.Set( state, color, offset );
}

glm::vec4
settings::GetBaseColorToState( const widgetStates_t state )
{
    auto   v      = this->widgetSettings.Get< float >( state );
    size_t offset = ( this->GetSwapColor() ) ? 4 : 0;
    return { v[offset + 0], v[offset + 1], v[offset + 2], v[offset + 3] };
}

glm::vec4
settings::GetAccentColorToState( const widgetStates_t state )
{
    auto   v      = this->widgetSettings.Get< float >( state );
    size_t offset = ( this->GetSwapColor() ) ? 0 : 4;
    return { v[offset + 0], v[offset + 1], v[offset + 2], v[offset + 3] };
}

void
settings::SetBaseGlowColorForState( const glm::vec4&     color,
                                    const widgetStates_t state )
{
    size_t offset               = ( this->GetSwapColor() ) ? 4 : 0;
    this->currentColor.doUpdate = true;
    this->widgetSettings.Set( state + STATE_END, color, offset );
}

void
settings::SetAccentGlowColorForState( const glm::vec4&     color,
                                      const widgetStates_t state )
{
    size_t offset               = ( this->GetSwapColor() ) ? 0 : 4;
    this->currentColor.doUpdate = true;
    this->widgetSettings.Set( state + STATE_END, color, offset );
}

glm::vec4
settings::GetBaseGlowColorToState( const widgetStates_t state )
{
    auto   v      = this->widgetSettings.Get< float >( state + STATE_END );
    size_t offset = ( this->GetSwapColor() ) ? 4 : 0;
    return { v[offset + 0], v[offset + 1], v[offset + 2], v[offset + 3] };
}

glm::vec4
settings::GetAccentGlowColorToState( const widgetStates_t state )
{
    auto   v      = this->widgetSettings.Get< float >( state + STATE_END );
    size_t offset = ( this->GetSwapColor() ) ? 0 : 4;
    return { v[offset + 0], v[offset + 1], v[offset + 2], v[offset + 3] };
}

void
settings::SetIsMovable( const bool v )
{
    this->widgetSettings.Set( PROPERTY_IS_MOVABLE, static_cast< int >( v ) );
}

void
settings::SetIsResizable( const bool v )
{
    this->widgetSettings.Set( PROPERTY_IS_RESIZABLE, static_cast< int >( v ) );
}

bool
settings::GetIsMovable() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_IS_MOVABLE )[0];
}

bool
settings::GetIsResizable() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_IS_RESIZABLE )[0];
}

void
settings::SetForwardMoveEventToParent( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_FORWARD_MOVE_EVENT_TO_PARENT,
                              static_cast< int >( v ) );
}

bool
settings::GetForwardMoveEventToParent() const noexcept
{
    return this->widgetSettings.Get< int >(
        PROPERTY_FORWARD_MOVE_EVENT_TO_PARENT )[0];
}

utils::elClassSettings
settings::GetSettingsHandle() const noexcept
{
    return this->widgetSettings;
}

void
settings::UpdateWindowDimensions() noexcept
{
    this->ResetWindowDimensions();

    for ( auto child : this->me->GetChildren() )
    {
        if ( child == nullptr || child == this->me->GetTitlebar() ) continue;

        switch ( child->GetHorizontalPointOfOrigin() )
        {
            case POSITION_LEFT:
            {
                this->dimension[POSITION_LEFT] = std::min(
                    this->dimension[POSITION_LEFT], child->GetPosition().x );
                this->dimension[POSITION_RIGHT] =
                    std::max( this->dimension[POSITION_RIGHT],
                              child->GetPosition().x + child->GetSize().x );

                break;
            }
            case POSITION_RIGHT:
            {
                this->dimension[POSITION_LEFT] =
                    std::min( this->dimension[POSITION_LEFT],
                              this->GetContentSize().x -
                                  child->GetPosition().x - child->GetSize().x );
                this->dimension[POSITION_RIGHT] = std::max(
                    this->dimension[POSITION_RIGHT],
                    this->GetContentSize().x - child->GetPosition().x );
                break;
            }
            case POSITION_MIDDLE:
            {
                auto origin =
                    ( this->me->GetContentSize().x - child->GetSize().x ) /
                    2.0f;

                this->dimension[POSITION_LEFT] =
                    std::min( this->dimension[POSITION_LEFT],
                              origin + child->GetPosition().x );
                this->dimension[POSITION_RIGHT] = std::max(
                    this->dimension[POSITION_RIGHT],
                    origin + child->GetPosition().x + child->GetSize().x );
                break;
            }
            default:
            {
            }
        }

        float titleBarHeight = this->GetTitlebarAdjustmentHeight( this->me );
        switch ( child->GetVerticalPointOfOrigin() )
        {
            case POSITION_TOP:
            {
                this->dimension[POSITION_TOP] = std::min(
                    this->dimension[POSITION_TOP], child->GetPosition().y );
                this->dimension[POSITION_BOTTOM] =
                    std::max( this->dimension[POSITION_BOTTOM],
                              child->GetPosition().y + child->GetSize().y +
                                  titleBarHeight );
                break;
            }
            case POSITION_BOTTOM:
            {
                this->dimension[POSITION_TOP] =
                    std::min( this->dimension[POSITION_TOP],
                              this->GetContentSize().y -
                                  child->GetPosition().y - child->GetSize().y );
                this->dimension[POSITION_BOTTOM] =
                    std::max( this->dimension[POSITION_BOTTOM],
                              this->GetContentSize().y -
                                  child->GetPosition().y + titleBarHeight );
                break;
            }
            case POSITION_MIDDLE:
            {
                auto origin =
                    ( this->me->GetContentSize().y - child->GetSize().y ) /
                    2.0f;

                this->dimension[POSITION_TOP] = std::min(
                    this->dimension[POSITION_TOP],
                    child->GetPosition().y + origin - titleBarHeight / 2.0f );
                this->dimension[POSITION_BOTTOM] = std::max(
                    this->dimension[POSITION_BOTTOM],
                    origin + child->GetSize().y + child->GetPosition().y +
                        titleBarHeight / 2.0f );
                break;
            }
            default:
            {
            }
        }
    }


    this->dimension = this->RoundVector( this->dimension * GetDPIScaling() );
}

positionCorner_t
settings::GetPositionPointOfOrigin() const noexcept
{
    return static_cast< positionCorner_t >( this->widgetSettings.Get< int >(
        PROPERTY_POSITION_POINT_OF_ORIGIN )[0] );
}

position_t
settings::GetVerticalPointOfOrigin() const noexcept
{
    if ( this->GetPositionPointOfOrigin() == POSITION_TOP_LEFT ||
         this->GetPositionPointOfOrigin() == POSITION_TOP_RIGHT ||
         this->GetPositionPointOfOrigin() == POSITION_TOP_CENTER )
        return POSITION_TOP;
    else if ( this->GetPositionPointOfOrigin() == POSITION_CENTER ||
              this->GetPositionPointOfOrigin() == POSITION_CENTER_LEFT ||
              this->GetPositionPointOfOrigin() == POSITION_CENTER_RIGHT )
        return POSITION_MIDDLE;
    else
        return POSITION_BOTTOM;
}

position_t
settings::GetHorizontalPointOfOrigin() const noexcept
{
    if ( this->GetPositionPointOfOrigin() == POSITION_TOP_LEFT ||
         this->GetPositionPointOfOrigin() == POSITION_BOTTOM_LEFT ||
         this->GetPositionPointOfOrigin() == POSITION_CENTER_LEFT )
        return POSITION_LEFT;
    else if ( this->GetPositionPointOfOrigin() == POSITION_CENTER ||
              this->GetPositionPointOfOrigin() == POSITION_TOP_CENTER ||
              this->GetPositionPointOfOrigin() == POSITION_BOTTOM_CENTER )
        return POSITION_MIDDLE;
    else
        return POSITION_RIGHT;
}

float
settings::GetTitlebarAdjustmentHeight(
    const elWidget_base* widget ) const noexcept
{
    auto titleBar = this->me->GetTitlebar();
    if ( titleBar == nullptr || widget == titleBar )
        return 0.0f;
    else
        return titleBar->GetSize().y;
}

void
settings::ReshapeAbsolutPosition() noexcept
{
    if ( this->me->parent != nullptr )
    {
        float titleBarHeight =
            this->me->parent->GetTitlebarAdjustmentHeight( this->me );

        auto horizontalPointOfOrigin = this->GetHorizontalPointOfOrigin();
        if ( horizontalPointOfOrigin == POSITION_RIGHT )
        {
            this->absolutPosition.x =
                this->me->parent->GetAbsolutPosition().x +
                this->me->parent->GetSize().x -
                this->me->parent->GetBorderSize()[POSITION_RIGHT] -
                this->GetPosition().x - this->GetSize().x;
        }
        else if ( horizontalPointOfOrigin == POSITION_LEFT )
        {
            this->absolutPosition.x =
                this->GetPosition().x +
                this->me->parent->GetAbsolutPosition().x +
                this->me->parent->GetBorderSize()[POSITION_LEFT];
        }
        else if ( horizontalPointOfOrigin == POSITION_MIDDLE )
        {
            auto origin =
                ( this->me->parent->GetContentSize().x - this->GetSize().x ) /
                2.0f;

            this->absolutPosition.x =
                this->GetPosition().x +
                this->me->parent->GetAbsolutPosition().x +
                this->me->parent->GetBorderSize()[POSITION_LEFT] + origin;
        }

        auto verticalPointOfOrigin = this->GetVerticalPointOfOrigin();
        if ( verticalPointOfOrigin == POSITION_BOTTOM )
        {
            this->absolutPosition.y =
                this->me->parent->GetAbsolutPosition().y +
                this->me->parent->GetSize().y -
                this->me->parent->GetBorderSize()[POSITION_BOTTOM] -
                this->GetPosition().y - this->GetSize().y;
        }
        else if ( verticalPointOfOrigin == POSITION_TOP )
        {
            this->absolutPosition.y =
                this->GetPosition().y +
                this->me->parent->GetAbsolutPosition().y +
                this->me->parent->GetBorderSize()[POSITION_TOP] +
                titleBarHeight;
        }
        else if ( verticalPointOfOrigin == POSITION_MIDDLE )
        {
            auto origin =
                ( this->me->parent->GetContentSize().y - this->GetSize().y ) /
                2.0f;

            this->absolutPosition.y =
                this->GetPosition().y +
                this->me->parent->GetAbsolutPosition().y +
                this->me->parent->GetBorderSize()[POSITION_TOP] + origin +
                titleBarHeight / 2.0f;
        }
    }

    auto offset = this->GetAbsolutPositionOffset();

    if ( !this->GetHasFixedPosition().x ) this->absolutPosition.x += offset[0];
    if ( !this->GetHasFixedPosition().y ) this->absolutPosition.y += offset[1];

    this->absolutPosition =
        this->RoundVector( this->absolutPosition * GetDPIScaling() );
}

glm::vec2
settings::GetPositionFromAbsolutPosition() const noexcept
{
    glm::vec2 returnValue;

    auto horizontalPointOfOrigin = this->GetHorizontalPointOfOrigin();
    if ( horizontalPointOfOrigin == POSITION_RIGHT )
    {
        returnValue.x = this->me->parent->GetAbsolutPosition().x +
                        this->me->parent->GetSize().x -
                        this->me->parent->GetBorderSize()[POSITION_RIGHT] -
                        this->GetSize().x - this->absolutPosition.x;
    }
    else if ( horizontalPointOfOrigin == POSITION_LEFT )
    {
        returnValue.x = this->absolutPosition.x -
                        this->me->parent->GetAbsolutPosition().x -
                        this->me->parent->GetBorderSize()[POSITION_LEFT];
    }
    else if ( horizontalPointOfOrigin == POSITION_MIDDLE )
    {
        auto origin =
            ( this->me->parent->GetContentSize().x - this->GetSize().x ) / 2.0f;

        returnValue.x =
            this->absolutPosition.x - this->me->parent->GetAbsolutPosition().x -
            this->me->parent->GetBorderSize()[POSITION_LEFT] - origin;
    }

    float titleBarHeight =
        this->me->parent->GetTitlebarAdjustmentHeight( this->me );
    auto verticalPointOfOrigin = this->GetVerticalPointOfOrigin();
    if ( verticalPointOfOrigin == POSITION_BOTTOM )
    {
        returnValue.y = this->me->parent->GetAbsolutPosition().y +
                        this->me->parent->GetSize().y -
                        this->me->parent->GetBorderSize()[POSITION_BOTTOM] -
                        this->GetSize().y - this->absolutPosition.y;
    }
    else if ( verticalPointOfOrigin == POSITION_TOP )
    {
        returnValue.y =
            this->absolutPosition.y - this->me->parent->GetAbsolutPosition().y -
            this->me->parent->GetBorderSize()[POSITION_TOP] - titleBarHeight;
    }
    else if ( verticalPointOfOrigin == POSITION_MIDDLE )
    {
        auto origin =
            ( this->me->parent->GetContentSize().y - this->GetSize().y ) / 2.0f;

        returnValue.y = this->absolutPosition.y -
                        this->me->parent->GetAbsolutPosition().y -
                        this->me->parent->GetBorderSize()[POSITION_TOP] -
                        origin - titleBarHeight / 2.0f;
    }

    return returnValue;
}

void
settings::LoadDefaultsFromConfig( const bool doUpdatePositionAndSize )
{
    std::vector< std::string > cfgPath =
        this->GetWidgetLine( this->settingsLookupDepth - 1 );

    auto iter = configCache.find( cfgPath );
    if ( iter != configCache.end() )
    {
        if ( !doUpdatePositionAndSize )
        {
            auto oldSize     = this->GetSize();
            auto oldPosition = this->GetPosition();

            this->widgetSettings = iter->second;

            this->SetSize( oldSize );
            this->SetPosition( oldPosition );
        }
        else
        {
            this->widgetSettings = iter->second;
        }
    }
    else
    {
        for ( size_t property = STATE_DEFAULT; property < PROPERTY_END;
              ++property )
        {
            if ( !doUpdatePositionAndSize && ( property == PROPERTY_SIZE ||
                                               property == PROPERTY_POSITION ) )
                continue;

            switch ( widgetProperty[property].type )
            {
                case utils::elClassSettings::dataType_t::FLOAT:
                {
                    auto defaultValue = std::get< static_cast< uint64_t >(
                        utils::elClassSettings::dataType_t::FLOAT ) >(
                        widgetProperty[property].defaultValue );
                    auto value =
                        this->GetSettingsEntry< float >(
                                property,
                                [&]( const std::vector< std::string >& cfgPath,
                                     const std::vector< float >&       v )
                                {
                                    auto pathWithoutProperty = cfgPath;
                                    pathWithoutProperty.pop_back();
                                    if ( this->theme->IsEntryAvailable(
                                             pathWithoutProperty ) )
                                        return this->theme->Get< float >(
                                            cfgPath, defaultValue );

                                    return this->theme->Get< float >( cfgPath,
                                                                      v );
                                },
                                defaultValue )
                            .second;
                    if ( property == PROPERTY_SIZE ||
                         property == PROPERTY_STICK_TO_PARENT_SIZE ||
                         property == PROPERTY_BORDER_SIZE ||
                         property == PROPERTY_POSITION ||
                         property == PROPERTY_BORDER_EVENT_INCREASE ||
                         property == PROPERTY_ABSOLUT_POSITION_OFFSET ||
                         property == PROPERTY_SCROLLBAR_WIDTH )
                    {
                        bb::for_each( value,
                                      []( float& v ) {
                                          v = std::round( v * GetDPIScaling() );
                                      } );
                    }

                    this->widgetSettings.Set( property, value );
                }
                break;
                case utils::elClassSettings::dataType_t::STRING:
                {
                    auto defaultValue = std::get< static_cast< uint64_t >(
                        utils::elClassSettings::dataType_t::STRING ) >(
                        widgetProperty[property].defaultValue );
                    auto value =
                        this->GetSettingsEntry< std::string >(
                                property,
                                [&]( const std::vector< std::string >& cfgPath,
                                     const std::vector< std::string >& v )
                                {
                                    auto pathWithoutProperty = cfgPath;
                                    pathWithoutProperty.pop_back();
                                    if ( this->theme->IsEntryAvailable(
                                             pathWithoutProperty ) )
                                        return this->theme->Get< std::string >(
                                            cfgPath, defaultValue );

                                    return this->theme->Get< std::string >(
                                        cfgPath, v );
                                },
                                defaultValue )
                            .second;

                    this->widgetSettings.Set( property, value );
                }
                break;
                case utils::elClassSettings::dataType_t::INT:
                {
                    auto defaultValue = std::get< static_cast< uint64_t >(
                        utils::elClassSettings::dataType_t::INT ) >(
                        widgetProperty[property].defaultValue );
                    auto value =
                        this->GetSettingsEntry< int >(
                                property,
                                [&]( const std::vector< std::string >& cfgPath,
                                     const std::vector< int >&         v )
                                {
                                    auto pathWithoutProperty = cfgPath;
                                    pathWithoutProperty.pop_back();
                                    if ( this->theme->IsEntryAvailable(
                                             pathWithoutProperty ) )
                                        return this->theme->Get< int >(
                                            cfgPath, defaultValue );

                                    return this->theme->Get< int >( cfgPath,
                                                                    v );
                                },
                                defaultValue )
                            .second;

                    this->widgetSettings.Set( property, value );
                }
                break;
            }
        }
        configCache.insert( std::make_pair( cfgPath, this->widgetSettings ) );
    }

    this->currentColor.doUpdate = true;

    // create titlebar if needed
    this->SetHasTitlebar( this->GetHasTitlebar() );
    this->SetDisableEventHandling( this->GetHasDisabledEventHandling() );

    // updating size/position and underlying animation
    this->SetSize( this->GetSize() );
    this->SetPosition( this->GetPosition() );
    this->me->ReloadConfigSettings();
    this->me->runOnce.Call( _Method_Reshape_ );
}

void
settings::MoveTo( const glm::vec2&               position,
                  const std::function< void() >& onArrivalCallback ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   AnimationInterpolation >( &settings::SetPosition )
        .SetSource( this->GetPosition() )
        .SetDestination( position )
        .SetVelocity(
            this->widgetSettings.Get< float >( PROPERTY_ANIMATION_SPEED )[0] )
        .SetOnArrivalCallback( onArrivalCallback );

    this->SetPositionOfStickyWidgets();
}

void
settings::ResizeTo( const glm::vec2&               size,
                    const std::function< void() >& onArrivalCallback ) noexcept
{
    this->me->parent->AdjustStickySize();

    auto adjustedSize = size;
    auto v            = this->GetStickToParentSize();
    if ( v.x >= 0.0f ) adjustedSize.x = this->GetSize().x;
    if ( v.y >= 0.0f ) adjustedSize.y = this->GetSize().y;

    this->Animate< animation::elAnimate_SourceDestination,
                   AnimationInterpolation >( &settings::SetSize )
        .SetSource( this->GetSize() )
        .SetDestination( adjustedSize )
        .SetVelocity(
            this->widgetSettings.Get< float >( PROPERTY_ANIMATION_SPEED )[0] )
        .SetOnArrivalCallback( onArrivalCallback );

    this->SetPositionOfStickyWidgets();
}

bool
settings::GetIsAlwaysOnTop() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_IS_ALWAYS_ON_TOP )[0];
}

bool
settings::GetIsAlwaysOnBottom() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_IS_ALWAYS_ON_BOTTOM )[0];
}

bool
settings::GetHasVerticalScrolling() const noexcept
{
    return this->hasVerticalScrolling;
}

bool
settings::GetHasHorizontalScrolling() const noexcept
{
    return this->hasHorizontalScrolling;
}

glm::ivec2
settings::GetHasFixedPosition() const noexcept
{
    auto val = this->widgetSettings.Get< int >( PROPERTY_HAS_FIXED_POSITION );
    return { val[0], val[1] };
}

void
settings::ResetWindowDimensions() noexcept
{
    this->dimension = glm::vec4{ 0.0f };
}

glm::vec4
settings::GetWindowDimensions() const noexcept
{
    return this->dimension / GetDPIScaling();
}

const glm::vec2
settings::GetStickToParentSize() const noexcept
{
    return utils::ToGLM< glm::vec2 >( this->widgetSettings.Get< float >(
               PROPERTY_STICK_TO_PARENT_SIZE ) ) /
           GetDPIScaling();
}

bool
settings::UpdateColorTransition()
{
    if ( !this->currentColor.doUpdate )
    {
        if ( this->currentColor.numberOfColorTransitions == 0u )
            return false;
        else if ( this->currentColor.numberOfColorTransitions == 1u )
            this->currentColor.numberOfColorTransitions = 0u;

        return true;
    }

    this->currentColor.doUpdate = false;

    this->currentColor.numberOfColorTransitions = 5u;
    // base color
    this->Animate< animation::elAnimate_SourceDestination,
                   ColorTransitionInterpolation >( &settings::SetBaseColor )
        .SetSource( this->GetBaseColor() )
        .SetDestination( this->GetBaseColorToState( this->widgetState ) )
        .SetVelocity( this->widgetSettings.Get< float >(
            PROPERTY_COLOR_TRANSITION_SPEED )[0] )
        .SetOnArrivalCallback(
            [&] { --this->currentColor.numberOfColorTransitions; } );

    this->Animate< animation::elAnimate_SourceDestination,
                   ColorTransitionInterpolation >( &settings::SetBaseGlowColor )
        .SetSource( this->GetBaseGlowColor() )
        .SetDestination( this->GetBaseGlowColorToState( this->widgetState ) )
        .SetVelocity( this->widgetSettings.Get< float >(
            PROPERTY_COLOR_TRANSITION_SPEED )[0] )
        .SetOnArrivalCallback(
            [&] { --this->currentColor.numberOfColorTransitions; } );

    // accent color
    this->Animate< animation::elAnimate_SourceDestination,
                   ColorTransitionInterpolation >( &settings::SetAccentColor )
        .SetSource( this->GetAccentColor() )
        .SetDestination( this->GetAccentColorToState( this->widgetState ) )
        .SetVelocity( this->widgetSettings.Get< float >(
            PROPERTY_COLOR_TRANSITION_SPEED )[0] )
        .SetOnArrivalCallback(
            [&] { --this->currentColor.numberOfColorTransitions; } );

    this->Animate< animation::elAnimate_SourceDestination,
                   ColorTransitionInterpolation >(
            &settings::SetAccentGlowColor )
        .SetSource( this->GetAccentGlowColor() )
        .SetDestination( this->GetAccentGlowColorToState( this->widgetState ) )
        .SetVelocity( this->widgetSettings.Get< float >(
            PROPERTY_COLOR_TRANSITION_SPEED )[0] )
        .SetOnArrivalCallback(
            [&] { --this->currentColor.numberOfColorTransitions; } );

    return true;
}

const widgetStates_t&
settings::GetState() const noexcept
{
    return this->widgetState;
}

glm::vec2
settings::GetSize() const noexcept
{
    auto value = this->widgetSettings.Get< float >( PROPERTY_SIZE );
    return glm::vec2( value[0], value[1] ) / GetDPIScaling();
}

glm::vec4
settings::GetBorderSize() const noexcept
{
    auto value = this->widgetSettings.Get< float >( PROPERTY_BORDER_SIZE );
    return glm::vec4( value[0], value[1], value[2], value[3] ) /
           GetDPIScaling();
}

glm::vec2
settings::GetContentSize() const noexcept
{
    auto borderSize = this->GetBorderSize();
    return this->GetSize() -
           glm::vec2( borderSize[POSITION_LEFT] + borderSize[POSITION_RIGHT],
                      borderSize[POSITION_TOP] + borderSize[POSITION_BOTTOM] +
                          this->GetTitlebarAdjustmentHeight( this->me ) );
}

void
settings::UpdateState( const widgetStates_t state ) noexcept
{
    this->currentColor.doUpdate = true;

    this->previousWidgetState = this->widgetState;
    this->widgetState         = state;

    switch ( this->widgetState )
    {
        case STATE_SELECTED:
            if ( this->me->parent != nullptr &&
                 this->me->parent->widgetState != STATE_SELECTED )
                this->me->parent->SetState( STATE_SELECTED );

            break;
        case STATE_HOVER:
            if ( this->me->parent != nullptr &&
                 this->me->parent->widgetState != STATE_SELECTED &&
                 this->me->parent->widgetState != STATE_HOVER )
                this->me->parent->SetState( STATE_HOVER );

            break;
        case STATE_INACTIVE:
            // states are calling callbacks which potentially can remove a child
            // therefore we have to perform a nullptr check
            for ( auto child : this->me->GetChildren() )
                if ( child != nullptr ) child->SetState( STATE_INACTIVE );
            break;
        default:
            break;
    }

    this->me->runOnce.Call( _Method_Reshape_ );
}

glm::vec4
settings::GetBaseColorSwapAdjusted() const noexcept
{
    return ( this->GetSwapColor() ) ? this->currentColor.accentColor
                                    : this->currentColor.baseColor;
}

glm::vec4
settings::GetAccentColorSwapAdjusted() const noexcept
{
    return ( this->GetSwapColor() ) ? this->currentColor.baseColor
                                    : this->currentColor.accentColor;
}

glm::vec4
settings::GetBaseGlowColorSwapAdjusted() const noexcept
{
    return ( this->GetSwapColor() ) ? this->currentColor.accentGlowColor
                                    : this->currentColor.baseGlowColor;
}

glm::vec4
settings::GetAccentGlowColorSwapAdjusted() const noexcept
{
    return ( this->GetSwapColor() ) ? this->currentColor.baseGlowColor
                                    : this->currentColor.accentGlowColor;
}

glm::vec4
settings::GetBaseColor() const noexcept
{
    return this->currentColor.baseColor;
}

glm::vec4
settings::GetAccentColor() const noexcept
{
    return this->currentColor.accentColor;
}

glm::vec4
settings::GetBaseGlowColor() const noexcept
{
    return this->currentColor.baseGlowColor;
}

glm::vec4
settings::GetAccentGlowColor() const noexcept
{
    return this->currentColor.accentGlowColor;
}

void
settings::SetAccentColor( const glm::vec4& v ) noexcept
{
    this->currentColor.accentColor = v;
}

void
settings::SetBaseColor( const glm::vec4& v ) noexcept
{
    this->currentColor.baseColor = v;
}

void
settings::SetAccentGlowColor( const glm::vec4& v ) noexcept
{
    this->currentColor.accentGlowColor = v;
}

void
settings::SetBaseGlowColor( const glm::vec4& v ) noexcept
{
    this->currentColor.baseGlowColor = v;
}

glm::vec2
settings::GetAbsolutPosition() const noexcept
{
    return this->absolutPosition / GetDPIScaling();
}

void
settings::StateChangeTo( const widgetStates_t newState ) noexcept
{
    if ( this->GetState() != STATE_INACTIVE )
        this->me->SetState( newState );
    else
        this->me->parent->StateChangeTo( newState );
}

void
settings::EnforceThemeParentWidget( const widgetType_t enforcedType,
                                    const bool         updateGeometry ) noexcept
{
    this->enforcedThemeParent = enforcedType;
    this->LoadDefaultsFromConfig( updateGeometry );
}

std::vector< std::string >
settings::GetWidgetLine( const size_t maxDepth ) const noexcept
{
    std::vector< std::string > value = { widgetTypeString[this->me->type] };
    if ( this->className.size() != 0 ) value[0].append( "_" + this->className );

    if ( maxDepth > 0 && this->enforcedThemeParent != TYPE_END )
    {
        value.push_back( widgetTypeString[this->enforcedThemeParent] );
    }
    else
    {
        elWidget_base* nextParent = this->me->parent;
        for ( size_t i = 0; i < maxDepth && nextParent != nullptr &&
                            nextParent->parent != nullptr;
              ++i, nextParent = nextParent->parent )
        {
            if ( nextParent->className.size() != 0 )
                value.push_back( widgetTypeString[nextParent->type] + "_" +
                                 nextParent->className );
            else
                value.push_back( widgetTypeString[nextParent->type] );
        }
    }

    std::reverse( value.begin(), value.end() );

    return value;
}

void
settings::SetDoRenderWidget( const bool v ) noexcept
{
    if ( this->doRenderWidget == v ) return;
    this->doRenderWidget = v;

    if ( this->doRenderWidget ) this->me->runOnce.Call( _Method_Reshape_ );
}

bool
settings::DoRenderWidget() const noexcept
{
    return this->doRenderWidget;
}

void
settings::SetDoReadTexture( const bool v ) noexcept
{
    GLint doReadTexture = v;
    this->me->uniformBuffer->SetVariableData( 0, UBO_DO_READ_TEXTURE,
                                              &doReadTexture, sizeof( GLint ) );
}

void
settings::SetDoDrawColors( const bool v ) noexcept
{
    GLint doReadTexture = v;
    this->me->uniformBuffer->SetVariableData( 0, UBO_DO_DRAW_COLORS,
                                              &doReadTexture, sizeof( GLint ) );
}

std::string
settings::UnsetPropertyErrorMessage( const size_t property ) const noexcept
{
    return "property " + widgetTypeString[this->me->type] + "." +
           widgetProperty[property].name + " is not set!";
}

settings::alignedPosition_t
settings::GetAlignedPositionWithWidget() noexcept
{
    return this->alignedPosition;
}

void
settings::SetAlignedPositionWithWidget( const elWidget_base* widget,
                                        const bool           xPos,
                                        const bool           yPos ) noexcept
{
    this->alignedPosition = { widget, xPos, yPos };
}

void
settings::SetHasVerticalAlignment( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_HAS_VERTICAL_ALIGNMENT,
                              std::vector< int >{ v } );
    this->me->runOnce.Call( _Method_Reshape_ );
}

bool
settings::GetHasVerticalAlignment() const noexcept
{
    return this->widgetSettings.Get< int >(
        PROPERTY_HAS_VERTICAL_ALIGNMENT )[0];
}

void
settings::SetButtonAlignment( const position_t v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_BUTTON_ALIGNMENT,
                              std::vector< int >{ v } );
    this->me->runOnce.Call( _Method_Reshape_ );
}

position_t
settings::GetButtonAlignment() const noexcept
{
    return static_cast< position_t >(
        this->widgetSettings.Get< int >( PROPERTY_BUTTON_ALIGNMENT )[0] );
}

void
settings::SetClassName( const std::string& v,
                        const bool         updateGeometry ) noexcept
{
    this->className = v;
    this->LoadDefaultsFromConfig( updateGeometry );
}

std::string
settings::GetClassName() const noexcept
{
    return this->className;
}

void
settings::SetHasTitlebar( const bool v ) noexcept
{

    if ( v && this->me->GetTitlebar() == nullptr )
        this->me->CreateTitlebar();
    else if ( !v && this->me->GetTitlebar() != nullptr )
        this->me->GetTitlebar()->RemoveThis();

    this->widgetSettings.Set( PROPERTY_HAS_TITLEBAR, std::vector< int >{ v } );
}

bool
settings::GetHasTitlebar() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_HAS_TITLEBAR )[0];
}

void
settings::SetNonConformTextureSize( const glm::vec2& v ) noexcept
{
    this->nonConformTextureSize = this->RoundVector( v * GetDPIScaling() );
}

bool
settings::HasNonConformTextureSize() const noexcept
{
    return this->nonConformTextureSize != glm::vec2( 0.0f );
}

glm::vec2
settings::GetNonConformTextureSize() const noexcept
{
    return this->nonConformTextureSize / GetDPIScaling();
}

void
settings::SetHasMultiSelection( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_HAS_MULTISELECTION,
                              std::vector< int >{ v } );
}

bool
settings::GetHasMultiSelection() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_HAS_MULTISELECTION )[0];
}

void
settings::SetIsUnselectable( const bool v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_IS_UNSELECTABLE,
                              std::vector< int >{ v } );
}

bool
settings::GetIsUnselectable() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_IS_UNSELECTABLE )[0];
}

glm::ivec2
settings::GetAdjustSizeToFitChildren() const noexcept
{
    auto val =
        this->widgetSettings.Get< int >( PROPERTY_ADJUST_SIZE_TO_FIT_CHILDREN );
    return { val[0], val[1] };
}

void
settings::SetAdjustSizeToFitChildren( const bool xSize,
                                      const bool ySize ) noexcept
{
    this->widgetSettings.Set( PROPERTY_ADJUST_SIZE_TO_FIT_CHILDREN,
                              std::vector< int >{ xSize, ySize } );
}

glm::vec4
settings::GetBorderEventIncrease() const noexcept
{
    auto v =
        this->widgetSettings.Get< float >( PROPERTY_BORDER_EVENT_INCREASE );
    return glm::vec4( v[0], v[1], v[2], v[3] ) / GetDPIScaling();
}

void
settings::SetBorderEventIncrease( const glm::vec4& v ) noexcept
{
    glm::vec4 adjusted = this->RoundVector( v * GetDPIScaling() );
    this->widgetSettings.Set( PROPERTY_BORDER_EVENT_INCREASE,
                              std::vector< float >{ adjusted.x, adjusted.y,
                                                    adjusted.z, adjusted.w } );
}

void
settings::SetSwapColor( const bool v ) noexcept
{
    if ( v == this->GetSwapColor() ) return;
    this->widgetSettings.Set( PROPERTY_SWAP_COLOR, std::vector< int >{ v } );
}

bool
settings::GetSwapColor() const noexcept
{
    return this->widgetSettings.Get< int >( PROPERTY_SWAP_COLOR )[0];
}

glm::vec2
settings::GetAbsolutPositionOffset() const noexcept
{
    auto v =
        this->widgetSettings.Get< float >( PROPERTY_ABSOLUT_POSITION_OFFSET );
    return glm::vec2( v[0], v[1] ) / GetDPIScaling();
}

void
settings::SetAbsolutPositionOffset( const glm::vec2& v ) noexcept
{
    glm::vec2 adjusted = this->RoundVector( v * GetDPIScaling() );
    this->widgetSettings.Set( PROPERTY_ABSOLUT_POSITION_OFFSET,
                              std::vector< float >{ adjusted.x, adjusted.y } );
}

std::string
settings::GetAnimationShaderFile() const noexcept
{
    return this->widgetSettings.Get< std::string >(
        PROPERTY_ANIMATION_SHADER_FILE )[0];
}

bool
settings::GetCopySelectedTextToClipboard() const noexcept
{
    return static_cast< bool >( this->widgetSettings.Get< int >(
        PROPERTY_COPY_SELECTED_TEXT_TO_CLIPBOARD )[0] );
}

float
settings::GetAnimationSpeed() const noexcept
{
    return this->widgetSettings.Get< float >( PROPERTY_ANIMATION_SPEED )[0];
}

glm::vec2
settings::GetTransformedSize() const noexcept
{
    auto value = this->widgetSettings.Get< float >( PROPERTY_SIZE );
    return glm::vec2( value[0], value[1] ) /
           glm::vec2( this->me->GetScreenWidth(), this->me->GetScreenHeight() );
}

glm::vec2
settings::GetTransformedAbsolutPosition() const noexcept
{
    return glm::vec2(
        this->absolutPosition.x /
            static_cast< float >( this->me->GetScreenWidth() ),
        1.0f - this->absolutPosition.y /
                   static_cast< float >( this->me->GetScreenHeight() ) );
}

glm::vec4
settings::GetTransformedBorderEventIncrease() const noexcept
{
    float height = static_cast< float >( this->me->GetScreenHeight() );
    float width  = static_cast< float >( this->me->GetScreenWidth() );
    auto  v =
        this->widgetSettings.Get< float >( PROPERTY_BORDER_EVENT_INCREASE );
    return glm::vec4( v[0], v[1], v[2], v[3] ) /
           glm::vec4( height, height, width, width );
}

glm::vec2
settings::GetTransformedNonConformTextureSize() const noexcept
{
    return this->nonConformTextureSize /
           glm::vec2( this->me->GetScreenWidth(), this->me->GetScreenHeight() );
}

glm::vec4
settings::GetTransformedBorderSize() const noexcept
{
    float height = static_cast< float >( this->me->GetScreenHeight() );
    float width  = static_cast< float >( this->me->GetScreenWidth() );
    auto  value  = this->widgetSettings.Get< float >( PROPERTY_BORDER_SIZE );
    return glm::vec4( value[0], value[1], value[2], value[3] ) /
           glm::vec4( height, height, width, width );
}

void
settings::SetInheritStateFromParent( const bool& value ) noexcept
{
    if ( this->me->parent == nullptr ) return;
    if ( value == this->doInheritStateFromParent ) return;

    this->doInheritStateFromParent = value;

    if ( this->doInheritStateFromParent )
        this->me->parent->stateInheritanceVector.emplace_back( this->me );
    else
    {
        auto iter =
            bb::find( this->me->parent->stateInheritanceVector, this->me );
        if ( iter != this->me->parent->stateInheritanceVector.end() )
            this->me->parent->stateInheritanceVector.erase( iter );
    }
}

std::vector< elWidget_base* >
settings::GetStateInheritanceVector() const noexcept
{
    return this->stateInheritanceVector;
}

void
settings::SetIsGhostWidget( const bool v ) noexcept
{
    this->isGhostWidget = v;

    if ( !this->isGhostWidget ) this->me->runOnce.Call( _Method_Reshape_ );
}

bool
settings::IsGhostWidget() const noexcept
{
    return this->isGhostWidget;
}

glm::vec2
settings::GetContentDimension() const noexcept
{
    return glm::vec2{ fabsf( this->dimension[POSITION_RIGHT] -
                             this->dimension[POSITION_LEFT] ),
                      fabsf( this->dimension[POSITION_BOTTOM] -
                             this->dimension[POSITION_TOP] ) } /
           GetDPIScaling();
}

void
settings::SetStickToWidget( elWidget_base* const       widget,
                            const verticalPosition_t   vPosition,
                            const horizontalPosition_t hPosition ) noexcept
{
    if ( widget->parent == this->me->parent )
    {
        this->UnsetStickToWidget();
        this->SetIsMovable( false );

        this->SetPositionPointOfOrigin( widget->GetPositionPointOfOrigin() );

        widget->stickyWidgetsSubscriber.emplace_back(
            stickyWidget_t{ this->me, vPosition, hPosition } );
        this->stickyWidgetSubcribedTo = widget;

        widget->runOnce.Call( _Method_Reshape_ );
    }
}

void
settings::UnsetStickToWidget() noexcept
{
    if ( this->stickyWidgetSubcribedTo != nullptr )
    {
        auto iter =
            bb::find_if( this->stickyWidgetSubcribedTo->stickyWidgetsSubscriber,
                         [&]( auto& v ) { return v.widget == this->me; } );

        if ( iter !=
             this->stickyWidgetSubcribedTo->stickyWidgetsSubscriber.end() )
            this->stickyWidgetSubcribedTo->stickyWidgetsSubscriber.erase(
                iter );

        this->stickyWidgetSubcribedTo = nullptr;
    }
}

void
settings::SetPositionOfStickyWidgets() noexcept
{
    glm::vec2 position{ 0, 0 };
    for ( auto& w : this->stickyWidgetsSubscriber )
    {
        glm::vec2 mySize = this->GetSize();
        auto      origin = this->GetPositionPointOfOrigin();

        switch ( w.vPosition )
        {
            case VPOSITION_TOP:
            {
                position.y = this->GetPosition().y - w.widget->GetSize().y;
                break;
            }
            case VPOSITION_TOP_ALIGNED:
            {
                position.y = this->GetPosition().y;
                break;
            }
            case VPOSITION_CENTER:
            {
                position.y = this->GetPosition().y +
                             ( mySize.y - w.widget->GetSize().y ) / 2.0f;
                break;
            }
            case VPOSITION_BOTTOM_ALIGNED:
            {
                position.y =
                    this->GetPosition().y + mySize.y - w.widget->GetSize().y;
                break;
            }
            case VPOSITION_BOTTOM:
            {
                position.y = this->GetPosition().y + mySize.y;
                break;
            }
        }

        switch ( w.hPosition )
        {
            case HPOSITION_LEFT:
            {
                position.x = this->GetPosition().x - w.widget->GetSize().x;
                break;
            }
            case HPOSITION_LEFT_ALIGNED:
            {
                position.x = this->GetPosition().x;
                break;
            }
            case HPOSITION_CENTER:
            {
                position.x = this->GetPosition().x +
                             ( mySize.x - w.widget->GetSize().x ) / 2.0f;
                break;
            }
            case HPOSITION_RIGHT_ALIGNED:
            {
                position.x =
                    this->GetPosition().x + mySize.x - w.widget->GetSize().x;
                break;
            }
            case HPOSITION_RIGHT:
            {
                position.x = this->GetPosition().x + mySize.x;
                break;
            }
        }

        if ( origin == POSITION_CENTER || origin == POSITION_TOP_CENTER ||
             origin == POSITION_BOTTOM_CENTER )
            position.x -=
                this->GetSize().x / 2.0f - w.widget->GetSize().x / 2.0f;

        if ( origin == POSITION_CENTER || origin == POSITION_CENTER_LEFT ||
             origin == POSITION_CENTER_RIGHT )
            position.y -=
                this->GetSize().y / 2.0f - w.widget->GetSize().y / 2.0f;


        w.widget->SetPosition( position );
    }
}


} // namespace handler
} // namespace GuiGL
} // namespace el3D
