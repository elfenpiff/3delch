#include "GuiGL/elWidget_InfoMessage.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Textbox.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_InfoMessage::elWidget_InfoMessage( const constructor_t& constructor )
    : elWidget_base( constructor ),
      textBox( this->Create< elWidget_Textbox >() ),
      button( this->Create< elWidget_Button >() )
{
    this->onCloseCallback = this->CreateCallback();
    this->button->SetText( "Okay" );
    this->button->SetPositionPointOfOrigin( POSITION_BOTTOM_LEFT );
    this->button->SetClickCallback( [&]() { this->ButtonCallback(); } );
    this->textBox->SetStickToParentSize( { 0.0f, this->button->GetSize().y } );
    this->textBox->SetIsEditable( false );
}

elWidget_InfoMessage::~elWidget_InfoMessage()
{
}

void
elWidget_InfoMessage::SetText( const std::vector< std::string >& text ) noexcept
{
    this->textBox->SetText( text );
}

void
elWidget_InfoMessage::InsertLine( const std::string& value,
                                  const size_t       line ) noexcept
{
    this->textBox->InsertLine( value, line );
}

void
elWidget_InfoMessage::ModifyLine( const std::string& value,
                                  const size_t       line ) noexcept
{
    this->textBox->ModifyLine( value, line );
}

void
elWidget_InfoMessage::RemoveLine( const size_t line ) noexcept
{
    this->textBox->RemoveLine( line );
}

void
elWidget_InfoMessage::SetCallbackOnClose(
    const std::function< void() >& f ) noexcept
{
    this->onCloseCallback->Set( f );
}

void
elWidget_InfoMessage::ButtonCallback() noexcept
{
    this->onCloseCallback->Call();
    this->RemoveThis();
}

} // namespace GuiGL
} // namespace el3D
