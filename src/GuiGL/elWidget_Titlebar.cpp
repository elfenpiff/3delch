#include "GuiGL/elWidget_Titlebar.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Titlebar::elWidget_Titlebar( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->titleText = this->Create< elWidget_Label >( "title" );
    this->titleText->SetPositionPointOfOrigin( GuiGL::POSITION_CENTER_LEFT );
    this->titleText->SetPosition( { 10, 0 } );
}

void
elWidget_Titlebar::SetState( const widgetStates_t state ) noexcept
{
    if ( this->parent != nullptr && state == STATE_CLICKED )
        this->parent->SetState( STATE_SELECTED );
    else
        elWidget_base::SetState( state );
}

void
elWidget_Titlebar::SetHasCloseButton( const bool v ) noexcept
{
    if ( ( !v && !this->closeButton ) || ( v && this->closeButton ) ) return;

    if ( v )
    {
        this->closeButton = this->Create< elWidget_Button >();
        this->closeButton->SetClassName( "close", true );
        this->closeButton->SetText( "X" );
        this->closeButton->SetPositionPointOfOrigin( POSITION_CENTER_RIGHT );
        this->closeButton->SetClickCallback( [&] {
            if ( this->parent ) this->parent->RemoveThis();
        } );
    }
    else
    {
        this->closeButton.Reset();
    }
}

const elWidget_Button&
elWidget_Titlebar::GetCloseButton() const noexcept
{
    return *this->closeButton;
}

elWidget_Button&
elWidget_Titlebar::GetCloseButton() noexcept
{
    return *this->closeButton;
}

void
elWidget_Titlebar::SetTitleText( const std::string& text ) noexcept
{
    this->titleText->SetText( text );
}

} // namespace GuiGL
} // namespace el3D
