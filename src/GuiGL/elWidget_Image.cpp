#include "GuiGL/elWidget_Image.hpp"

#include "OpenGL/elTexture_2D.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_Image::elWidget_Image( const constructor_t& constructor )
    : elWidget_base( constructor )
{
    this->SetDoReadTexture( true );
}

elWidget_Image::~elWidget_Image()
{
}

void
elWidget_Image::SetImageFromTexture(
    const OpenGL::elTexture_base* const tex ) noexcept
{
    this->AddTextureToVertexObject( tex );
}

void
elWidget_Image::SetImage( const GLAPI::elImage* img ) noexcept
{
    this->image = std::make_unique< OpenGL::elTexture_2D >(
        "elWidget_Image:(source=image)", *img );

    this->AddTextureToVertexObject( this->image.get() );
}

void
elWidget_Image::SetImageFromBytePointer(
    const utils::elRawPointer< utils::byte_t > data, const uint64_t width,
    const uint64_t height ) noexcept
{
    this->image = std::make_unique< OpenGL::elTexture_2D >(
        "elWidget_Image:(source=pointer)" );
    this->image->SetSize( width, height );
    this->image->TexImage( data.Get() );

    this->AddTextureToVertexObject( this->image.get() );
}

void
elWidget_Image::SetImageFromByteStream( const utils::byteStream_t& byteStream,
                                        const uint64_t             width,
                                        const uint64_t height ) noexcept
{
    this->SetImageFromBytePointer( byteStream.stream.data(), width, height );
}

void
elWidget_Image::AddTextureToVertexObject(
    const OpenGL::elTexture_base* const texture ) noexcept
{
    this->usedTexture =
        this->vo.AttachTexture( texture, 0, "image", GL_TEXTURE0 );
}

} // namespace GuiGL
} // namespace el3D
