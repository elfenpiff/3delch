#include "GuiGL/elWidget_TabbedWindow.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Window.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_TabbedWindow::elWidget_TabbedWindow( const constructor_t& constructor )
    : elWidget_base( constructor )
{
}

void
elWidget_TabbedWindow::Reshape()
{
    float currentPosition = 0.0f;
    auto  alignment       = this->GetButtonAlignment();

    auto& tabs = this->tabFactory.GetProducts< tab_t >();
    if ( alignment == POSITION_TOP || alignment == POSITION_BOTTOM )
    {
        float buttonSize =
            this->GetContentSize().x / static_cast< float >( tabs.size() );
        for ( auto& b : tabs )
        {
            float buttonHeight = b->button->GetSize().y;
            b->button->SetPositionPointOfOrigin( ( alignment == POSITION_TOP )
                                                     ? POSITION_TOP_LEFT
                                                     : POSITION_BOTTOM_LEFT );
            b->button->SetSize( glm::vec2( buttonSize, buttonHeight ) );
            b->button->SetPosition( glm::vec2( currentPosition, 0.0f ) );
            currentPosition += buttonSize;

            b->window->SetPositionPointOfOrigin( ( alignment == POSITION_TOP )
                                                     ? POSITION_TOP_LEFT
                                                     : POSITION_BOTTOM_LEFT );
            b->window->SetPosition( glm::vec2( 0.0f, buttonHeight ) );
            b->window->SetStickToParentSize( glm::vec2( 0.0f, buttonHeight ) );
        }
    }
    else if ( alignment == POSITION_LEFT || alignment == POSITION_RIGHT )
    {
        for ( auto& b : tabs )
        {
            auto buttonSize = b->button->GetSize();
            b->button->SetPositionPointOfOrigin( ( alignment == POSITION_LEFT )
                                                     ? POSITION_TOP_LEFT
                                                     : POSITION_TOP_RIGHT );
            b->button->SetPosition( glm::vec2( 0.0, currentPosition ) );
            currentPosition += buttonSize.y;

            b->window->SetPositionPointOfOrigin( ( alignment == POSITION_LEFT )
                                                     ? POSITION_TOP_LEFT
                                                     : POSITION_TOP_RIGHT );
            b->window->SetPosition( glm::vec2( buttonSize.x, 0.0f ) );
            b->window->SetStickToParentSize( glm::vec2( buttonSize.x, 0.0f ) );
        }
    }

    elWidget_base::Reshape();
}

bb::product_ptr< elWidget_TabbedWindow::tab_t >
elWidget_TabbedWindow::CreateTab( const std::string& name ) noexcept
{
    auto newTab = this->tabFactory.CreateProductWithOnRemoveCallback< tab_t >(
        [this]( auto tab ) {
            auto& tabs = this->tabFactory.GetProducts< tab_t >();
            if ( tabs.empty() )
                this->activeTab = nullptr;
            else if ( tab == this->activeTab )
            {
                this->activeTab =
                    this->tabFactory.GetProducts< tab_t >().begin()->Get();
                this->activeTab->window->SetDoRenderWidget( true );
            }
            this->runOnce.Call( _Method_Reshape_ );
        } );
    newTab->window = this->Create< elWidget_Window >();
    newTab->window->SetIsResizable( false );
    newTab->window->SetIsMovable( false );
    newTab->button = this->Create< elWidget_Button >();
    newTab->button->SetClassName( "tabs", false );
    newTab->button->SetText( name );
    newTab->button->SetClickCallback( [this, tab = newTab.Get()]() {
        if ( this->activeTab != tab )
        {
            this->activeTab->window->SetDoRenderWidget( false );
            this->activeTab = tab;
            this->activeTab->window->SetDoRenderWidget( true );
        }
    } );
    if ( this->activeTab == nullptr )
        this->activeTab = newTab.Get();
    else
        newTab->window->SetDoRenderWidget( false );

    this->runOnce.Call( _Method_Reshape_ );
    return newTab;
}
} // namespace GuiGL
} // namespace el3D
