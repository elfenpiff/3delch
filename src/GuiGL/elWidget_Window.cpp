#include "GuiGL/elWidget_Window.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Slider.hpp"

#include <cmath>

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Window::elWidget_Window( const constructor_t& constructor )
    : elWidget_base( constructor ), scrollEvent( this->CreateEvent() )
{
    auto scrollBarEnabled = this->GetIsScrollbarEnabled();
    this->CreateSlider(
        static_cast< bool >( scrollBarEnabled[POSITION_TOP] ),
        static_cast< bool >( scrollBarEnabled[POSITION_BOTTOM] ),
        static_cast< bool >( scrollBarEnabled[POSITION_LEFT] ),
        static_cast< bool >( scrollBarEnabled[POSITION_RIGHT] ) );
    this->SetHasVerticalScrolling( true );
    this->SetHasHorizontalScrolling( true );
    this->CreateScrollEvent();
}

void
elWidget_Window::SetState( const widgetStates_t state ) noexcept
{
    elWidget_base::SetState( state );

    auto currentState = this->GetState();
    if ( currentState == STATE_SELECTED || currentState == STATE_HOVER ||
         currentState == STATE_CLICKED )
    {
        if ( !this->scrollEvent->IsRegistered() ) this->scrollEvent->Register();
    }
    else
    {
        if ( this->scrollEvent->IsRegistered() )
            this->scrollEvent->Unregister();
    }
}

void
elWidget_Window::Reshape()
{
    auto enableSlider = this->GetIsScrollbarEnabled();
    this->CreateSlider(
        enableSlider[POSITION_TOP], enableSlider[POSITION_BOTTOM],
        enableSlider[POSITION_LEFT], enableSlider[POSITION_RIGHT] );
    this->AdjustChildPositionInWindow();
    this->AdjustSliderPositionAndWidth();

    elWidget_base::Reshape();
}

void
elWidget_Window::AdjustSliderPositionAndWidth() noexcept
{
    if ( !this->GetHasVerticalScrolling() &&
         !this->GetHasHorizontalScrolling() )
        return;

    this->UpdateSliderOffsetAndSizeRemainder();

    glm::vec2 sliderSize = this->GetContentSize();

    if ( sliderSize.x <= 0 || sliderSize.y <= 0 ) return;
    sliderSize /= ( sliderSize + this->slider.sizeRemainder );
    glm::vec2 sliderPosition{ 0.0f };

    if ( this->slider.sizeRemainder.x != 0 )
    {
        sliderPosition.x =
            ( this->slider.absolutPositionOffset[0] + this->slider.offset.x ) /
            -this->slider.sizeRemainder.x;
    }
    if ( this->slider.sizeRemainder.y != 0 )
    {
        sliderPosition.y =
            ( this->slider.absolutPositionOffset[1] + this->slider.offset.y ) /
            -this->slider.sizeRemainder.y;
    }

    if ( this->slider.top )
    {
        this->slider.top->SetRelativeSliderButtonSize( sliderSize.x );
        this->slider.top->SetSliderPositionWithCallback( sliderPosition.x,
                                                         false );
    }
    if ( this->slider.bottom )
    {
        this->slider.bottom->SetRelativeSliderButtonSize( sliderSize.x );
        this->slider.bottom->SetSliderPositionWithCallback( sliderPosition.x,
                                                            false );
    }
    if ( this->slider.left )
    {
        this->slider.left->SetRelativeSliderButtonSize( sliderSize.y );
        this->slider.left->SetSliderPositionWithCallback( sliderPosition.y,
                                                          false );
    }
    if ( this->slider.right )
    {
        this->slider.right->SetRelativeSliderButtonSize( sliderSize.y );
        this->slider.right->SetSliderPositionWithCallback( sliderPosition.y,
                                                           false );
    }
}

void
elWidget_Window::CreateSlider( const bool enableTop, const bool enableBottom,
                               const bool enableLeft,
                               const bool enableRight ) noexcept
{
    auto scrollBarWidth = this->GetScrollbarWidth();
    this->SetupSlider( enableTop, true, scrollBarWidth[POSITION_TOP],
                       POSITION_TOP_LEFT, this->slider.top );
    this->SetupSlider( enableBottom, true, scrollBarWidth[POSITION_BOTTOM],
                       POSITION_BOTTOM_LEFT, this->slider.bottom );
    this->SetupSlider( enableLeft, false, scrollBarWidth[POSITION_LEFT],
                       POSITION_TOP_LEFT, this->slider.left );
    this->SetupSlider( enableRight, false, scrollBarWidth[POSITION_RIGHT],
                       POSITION_TOP_RIGHT, this->slider.right );
}

void
elWidget_Window::ReshapeSlider( elWidget_Slider_ptr& s,
                                const bool           isHorizontal ) noexcept
{
    if ( s )
    {
        glm::vec2 stickTo{ 0.0f };

        stickTo[( isHorizontal ) ? 1 : 0] = -1.0f;

        s->SetStickToParentSize( stickTo );

        // slider and slider button are transparent if they arent used
        auto sliderColor = s->GetBaseColorToState( STATE_DEFAULT );
        sliderColor[3]   = 0.0f;
        s->SetBaseColorForState( sliderColor, STATE_DEFAULT );

        auto sliderButtonColor =
            s->button.widget->GetBaseColorToState( STATE_DEFAULT );
        sliderButtonColor[3] = 0.0f;
        s->button.widget->SetBaseColorForState( sliderButtonColor,
                                                STATE_DEFAULT );
    }
}

void
elWidget_Window::SetupSlider( const bool enable, const bool isHorizontal,
                              const float            width,
                              const positionCorner_t pointOfOrigin,
                              elWidget_Slider_ptr&   s ) noexcept
{
    if ( enable && !s )
    {
        s = this->Create< elWidget_Slider >();
        s->SetIsHorizontal( isHorizontal );
        this->ReshapeSlider( s, isHorizontal );
        s->SetIsUnselectable( true );
        s->SetBorderSize( glm::vec4( 0.0f ) );
        s->SetSize( glm::vec2( ( isHorizontal ) ? 0.0f : width,
                               ( isHorizontal ) ? width : 0.0f ) );
        s->SetPositionPointOfOrigin( pointOfOrigin );
        s->SetIsAlwaysOnTop( true );
        s->SetHasFixedPosition( true, true );
        s->SetSliderPositionChangeCallback(
            [this, isHorizontal]( const float v )
            { this->SliderPositionChange( isHorizontal, v ); } );
    }
    else if ( !enable && s )
    {
        s.Reset();
    }
}

void
elWidget_Window::UpdateSliderOffsetAndSizeRemainder() noexcept
{
    if ( !this->GetHasVerticalScrolling() &&
         !this->GetHasHorizontalScrolling() )
        return;

    auto size       = this->GetSize();
    auto borderSize = this->GetBorderSize();
    auto dimension  = this->GetWindowDimensions();

    this->slider.sizeRemainder = glm::vec2(
        dimension[POSITION_RIGHT] - dimension[POSITION_LEFT] - size[0] +
            borderSize[POSITION_LEFT] + borderSize[POSITION_RIGHT],
        dimension[POSITION_BOTTOM] - dimension[POSITION_TOP] - size[1] +
            borderSize[POSITION_TOP] + borderSize[POSITION_BOTTOM] );

    this->slider.sizeRemainder =
        glm::vec2( std::max( 0.0f, this->slider.sizeRemainder.x ),
                   std::max( 0.0f, this->slider.sizeRemainder.y ) );

    this->slider.offset =
        glm::vec2( std::min( 0.0f, dimension[POSITION_LEFT] ),
                   std::min( 0.0f, dimension[POSITION_TOP] ) );
}

void
elWidget_Window::UpdateWindowDimensions() noexcept
{
    auto oldDimension    = this->GetWindowDimensions();
    auto oldSliderOffset = this->slider.offset;
    settings::UpdateWindowDimensions();
    this->AdjustRelativeScrollPosition( oldDimension, oldSliderOffset );
}

void
elWidget_Window::AdjustRelativeScrollPosition(
    const glm::vec4& oldDimension, const glm::vec2& oldSliderOffset ) noexcept
{
    if ( oldDimension != this->GetWindowDimensions() )
    {
        this->UpdateSliderOffsetAndSizeRemainder();
        glm::vec4 deltaDimension = oldDimension - this->GetWindowDimensions();
        glm::vec2 deltaSliderOffset = oldSliderOffset - this->slider.offset;

        this->slider.absolutPositionOffset[0] +=
            deltaDimension[POSITION_LEFT] - deltaSliderOffset.x;
        this->slider.absolutPositionOffset[1] +=
            deltaDimension[POSITION_TOP] - deltaSliderOffset.y;

        if ( this->slider.sizeRemainder.x != 0.0f )
        {
            this->SetScrollFactor( { ( this->slider.absolutPositionOffset[0] +
                                       this->slider.offset.x ) /
                                         ( -this->slider.sizeRemainder.x ),
                                     this->GetScrollFactor().y } );
        }
        else
        {
            this->SetScrollFactor( { 0.0f, this->GetScrollFactor().y } );
        }

        if ( this->slider.sizeRemainder.y != 0.0f )
        {
            this->SetScrollFactor( { this->GetScrollFactor().x,
                                     ( this->slider.absolutPositionOffset[1] +
                                       this->slider.offset.y ) /
                                         ( -this->slider.sizeRemainder.y ) } );
        }
        else
        {
            this->SetScrollFactor( { this->GetScrollFactor().x, 0.0f } );
        }
    }
}

glm::vec2
elWidget_Window::CalculatePositionOffsetForChilds() const noexcept
{
    auto retVal = -this->slider.sizeRemainder *
                      glm::vec2( this->slider.currentRelativePosition[0],
                                 this->slider.currentRelativePosition[1] ) -
                  this->slider.offset;

    return { retVal.x, retVal.y };
}

void
elWidget_Window::AdjustChildPositionInWindow() noexcept
{
    this->slider.absolutPositionOffset =
        this->CalculatePositionOffsetForChilds();

    for ( auto child : this->GetChildren() )
        if ( child != nullptr )
            child->SetAbsolutPositionOffset(
                { this->slider.absolutPositionOffset[0],
                  this->slider.absolutPositionOffset[1] } );
}

void
elWidget_Window::SetSliderCurrentRelativePosition( const bool  isHorizontal,
                                                   const float value ) noexcept
{
    float limitedValue = fmaxf( fminf( value, 1.0f ), 0.0f );

    if ( isHorizontal )
    {
        this->slider.currentRelativePosition[0] = limitedValue;
    }
    else
    {
        this->slider.currentRelativePosition[1] = limitedValue;
    }
}

glm::vec2
elWidget_Window::GetScrollFactor() const noexcept
{
    return this->slider.currentRelativePosition;
}

void
elWidget_Window::SetScrollFactor( const glm::vec2& v ) noexcept
{
    this->slider.currentRelativePosition = glm::vec2{
        std::clamp( v.x, 0.0f, 1.0f ), std::clamp( v.y, 0.0f, 1.0f ) };
}

void
elWidget_Window::SliderPositionChange( const bool  isHorizontal,
                                       const float value ) noexcept
{
    if ( !this->GetHasVerticalScrolling() &&
         !this->GetHasHorizontalScrolling() )
        return;

    if ( isHorizontal )
        this->SetScrollFactor( { value, this->GetScrollFactor().y } );
    else
        this->SetScrollFactor( { this->GetScrollFactor().x, value } );
    this->UpdateSliderOffsetAndSizeRemainder();
    this->AdjustChildPositionInWindow();

    if ( isHorizontal )
    {
        if ( this->slider.top )
            this->slider.top->SetSliderPositionWithCallback( value, false );
        if ( this->slider.bottom )
            this->slider.bottom->SetSliderPositionWithCallback( value, false );
    }
    else
    {
        if ( this->slider.left )
            this->slider.left->SetSliderPositionWithCallback( value, false );
        if ( this->slider.right )
            this->slider.right->SetSliderPositionWithCallback( value, false );
    }

    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Window::ScrollEventCallback( const SDL_Event e ) noexcept
{
    if ( !this->GetHasVerticalScrolling() &&
         !this->GetHasHorizontalScrolling() )
    {
        event::ScrollEventCallback( e );
    }
    else
    {
        this->UpdateSliderOffsetAndSizeRemainder();
        glm::vec2 newScrollPosition =
            glm::vec2( this->slider.currentRelativePosition[0],
                       this->slider.currentRelativePosition[1] );
        auto naturalScrolling = this->GetHasNaturalScrolling();
        if ( this->GetHasHorizontalScrolling() )
        {
            float coefficent = this->slider.sizeRemainder.x *
                               static_cast< float >( e.wheel.x ) *
                               naturalScrolling[0];
            if ( coefficent != 0.0f )
                newScrollPosition.x = this->GetScrollSpeed().x / coefficent +
                                      this->slider.currentRelativePosition[0];
        }

        if ( this->GetHasVerticalScrolling() )
        {
            float coefficent = this->slider.sizeRemainder.y *
                               static_cast< float >( e.wheel.y ) *
                               naturalScrolling[1];
            if ( coefficent != 0.0f )
                newScrollPosition.y = this->GetScrollSpeed().y / coefficent +
                                      this->slider.currentRelativePosition[1];
        }

        this->ScrollTo( newScrollPosition );
    }
}

void
elWidget_Window::CreateScrollEvent() noexcept
{
    this->scrollEvent->SetCallback(
        [this]( const SDL_Event e )
        {
            if ( this->DoesEventColorBelongsToMeOrChild(
                     this->GetColorToCurrentEvent() ) &&
                 e.type == SDL_MOUSEWHEEL )
            {
                uint32_t eventColor = this->GetColorToCurrentEvent();
                if ( this->DoesEventColorBelongsToMe( eventColor ) )
                    this->ScrollEventCallback( e );
                else
                {
                    auto child = GetChildFromEventColor( eventColor );
                    if ( child ) ( *child )->ScrollEventCallback( e );
                }
            }
        } );
}

glm::vec2
elWidget_Window::GetFinalSliderPosition() const noexcept
{
    auto ptr = this->GetAnimation< animation::elAnimate_SourceDestination,
                                   AnimationInterpolation >(
        &elWidget_Window::SetScrollFactor );

    return ( ptr != nullptr ) ? ptr->GetDestination()
                              : this->slider.currentRelativePosition;
}

void
elWidget_Window::ScrollTo( const glm::vec2& p ) noexcept
{
    this->Animate< animation::elAnimate_SourceDestination,
                   AnimationInterpolation >( &elWidget_Window::SetScrollFactor,
                                             this )
        .SetSource( this->GetScrollFactor() )
        .SetDestination(
            { std::clamp( p.x, 0.0f, 1.0f ), std::clamp( p.y, 0.0f, 1.0f ) } )
        .SetVelocity(
            this->widgetSettings.Get< float >( PROPERTY_ANIMATION_SPEED )[0] );
}

const glm::vec2&
elWidget_Window::GetScrollSpeed() const noexcept
{
    return this->scrollSpeed;
}

glm::vec4
elWidget_Window::GetScrollbarWidth() const noexcept
{
    auto v = this->widgetSettings.Get< float >( PROPERTY_SCROLLBAR_WIDTH );
    return glm::vec4( v[0], v[1], v[2], v[3] ) / GetDPIScaling();
}

void
elWidget_Window::SetScrollbarWidth( const glm::vec4& v ) noexcept
{
    glm::vec4 adjusted = this->RoundVector( v * GetDPIScaling() );
    this->widgetSettings.Set( PROPERTY_SCROLLBAR_WIDTH,
                              std::vector< float >{ adjusted.x, adjusted.y,
                                                    adjusted.z, adjusted.w } );
}

glm::vec2
elWidget_Window::GetHasNaturalScrolling() const noexcept
{
    auto v = this->widgetSettings.Get< float >( PROPERTY_NATURAL_SCROLLING );
    return glm::vec2( v[0], v[1] );
}

void
elWidget_Window::SetHasNaturalScrolling( const glm::vec2& v ) noexcept
{
    this->widgetSettings.Set( PROPERTY_NATURAL_SCROLLING,
                              std::vector< float >{ v.x, v.y } );
}

glm::ivec4
elWidget_Window::GetIsScrollbarEnabled() const noexcept
{
    auto v = this->widgetSettings.Get< int >( PROPERTY_SCROLLBAR_ENABLED );
    return glm::ivec4( v[0], v[1], v[2], v[3] );
}

void
elWidget_Window::SetIsScrollbarEnabled( const bool enableTop,
                                        const bool enableBottom,
                                        const bool enableLeft,
                                        const bool enableRight ) noexcept
{
    this->widgetSettings.Set( PROPERTY_SCROLLBAR_ENABLED,
                              std::vector< int >{ enableTop, enableBottom,
                                                  enableLeft, enableRight } );
    this->runOnce.Call( _Method_Reshape_ );
}


} // namespace GuiGL
} // namespace el3D
