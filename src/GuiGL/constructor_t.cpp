#include "GuiGL/constructor_t.hpp"

#include "OpenGL/elShaderProgram.hpp"

namespace el3D
{
namespace GuiGL
{
constructor_t::constructor_t(
    elWidget_base* parent, const OpenGL::elShaderProgram* shader,
    const OpenGL::elShaderProgram* glowShader,
    HighGL::elEventTexture* const  eventTexture,
    GLAPI::elEventHandler* const   eventHandler,
    GLAPI::elEvent* const          stickyExclusiveEvent,
    const GLAPI::elWindow* const window, GLAPI::elFontCache* const fontCache,
    GuiGL::elWidget_MouseCursor* const mouseCursor,
    const std::function< size_t( HighGL::elShaderTexture* ) >&
                                           shaderTextureRegistrator,
    const std::function< void( size_t ) >& shaderTextureDeregistrator,
    const int shaderVersion, const lua::elConfigHandler* theme,
    const widgetType_t type, const size_t* screenWidth,
    const size_t* screenHeight, const std::string& className,
    const widgetType_t enforcedThemeParent, const float glowScaling,
    CallbackContainer_t& callbackContainer ) noexcept
    : parent{ parent }, shader{ shader }, glowShader{ glowShader },
      eventTexture{ eventTexture }, eventHandler{ eventHandler },
      stickyExclusiveEvent{ stickyExclusiveEvent }, window{ window },
      fontCache{ fontCache }, mouseCursor{ mouseCursor },
      shaderTextureRegistrator{ shaderTextureRegistrator },
      shaderTextureDeregistrator{ shaderTextureDeregistrator },
      shaderVersion{ shaderVersion }, theme{ theme }, type{ type },
      screenWidth{ screenWidth },
      screenHeight{ screenHeight }, className{ className },
      enforcedThemeParent{ enforcedThemeParent }, glowScaling{ glowScaling },
      callbackContainer( &callbackContainer )
{
}


} // namespace GuiGL
} // namespace el3D
