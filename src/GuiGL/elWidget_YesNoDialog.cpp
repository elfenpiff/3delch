#include "GuiGL/elWidget_YesNoDialog.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Textbox.hpp"

namespace el3D
{
namespace GuiGL
{
elWidget_YesNoDialog::elWidget_YesNoDialog( const constructor_t& constructor )
    : elWidget_InfoMessage( constructor ),
      noButton( this->Create< elWidget_Button >() )
{
    this->yesCallback = this->CreateCallback();
    this->noCallback  = this->CreateCallback();
    this->button->SetClickCallback(
        [&]() { this->ButtonPressCallback( true ); } );
    this->button->SetText( "Yes" );

    this->noButton->SetClickCallback(
        [&]() { this->ButtonPressCallback( false ); } );
    this->noButton->SetPositionPointOfOrigin( POSITION_BOTTOM_RIGHT );
    this->noButton->SetText( "No" );
}

elWidget_YesNoDialog::~elWidget_YesNoDialog()
{
}

void
elWidget_YesNoDialog::ButtonPressCallback( const bool yes ) noexcept
{
    if ( yes )
        yesCallback->Call();
    else
        noCallback->Call();

    this->RemoveThis();
}

void
elWidget_YesNoDialog::Reshape()
{
    float contentSize  = this->GetContentSize().x;
    float buttonSize   = std::min( contentSize / 2.0f, 150.0f );
    float buttonHeight = this->button->GetSize().y;

    this->button->SetSize( { buttonSize, buttonHeight } );
    this->noButton->SetSize( { buttonSize, buttonHeight } );

    elWidget_base::Reshape();
}

void
elWidget_YesNoDialog::SetYesNoCallback(
    const std::function< void() >& yes,
    const std::function< void() >& no ) noexcept
{
    this->yesCallback->Set( yes );
    this->noCallback->Set( no );
}

void
elWidget_YesNoDialog::SetCallbackOnClose(
    const std::function< void() >& v ) noexcept
{
    elWidget_InfoMessage::SetCallbackOnClose( v );
}

} // namespace GuiGL
} // namespace el3D
