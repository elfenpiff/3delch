#include "GuiGL/elWidget_Menu.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "HighGL/elEventTexture.hpp"

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on
namespace el3D
{
namespace GuiGL
{
elWidget_Menu::entry_t::entry_t( const char* entry )
    : value( std::string( entry ) )
{
}

elWidget_Menu::entry_t::entry_t( const std::initializer_list< entry_t >& sub )
    : value( std::vector< entry_t >( sub ) )
{
}

elWidget_Menu::elWidget_Menu( const constructor_t& constructor )
    : elWidget_base( constructor ), closeSubMenuOnClick( this->CreateEvent() )
{
    this->SetIsAlwaysOnTop( true );
    this->closeSubMenuOnClick->SetCallback( [this]( const SDL_Event e ) {
        if ( !this->DoesEventColorBelongsToMeOrChild(
                 this->GetColorToCurrentEvent() ) &&
             e.button.type == SDL_MOUSEBUTTONUP )
        {
            if ( this->activeMenu )
            {
                this->HideMenu( this->activeMenu );
                this->activeMenu = nullptr;
            }
        }
    } );
}

void
elWidget_Menu::CreateMenuButton( menu_t&            point,
                                 const std::string& name ) noexcept
{
    point.button = this->Create< elWidget_Button >();
    point.button->SetText( name );

    auto borderSizeButton = point.button->GetBorderSize();
    auto borderSizeThis   = this->GetBorderSize();
    this->maxWidth        = std::max(
        this->maxWidth,
        static_cast< size_t >(
            static_cast< float >(
                point.button->textLabel->GetTextSize().width ) +
            borderSizeButton[POSITION_LEFT] + borderSizeButton[POSITION_RIGHT] +
            borderSizeThis[POSITION_LEFT] + borderSizeThis[POSITION_RIGHT] ) );
}

void
elWidget_Menu::CreateSubMenu( menu_t& point ) noexcept
{
    auto submenu  = this->parent->Create< elWidget_Menu >();
    point.submenu = submenu.Get();
    point.submenu->SetDoRenderWidget( false );
    point.buttonArrow = point.button->Create< elWidget_Label >();
    point.buttonArrow->SetText( "> " );
    point.buttonArrow->SetPositionPointOfOrigin( POSITION_CENTER_RIGHT );
    this->subMenus.emplace_back( std::move( submenu ) );
}

void
elWidget_Menu::AddEntriesToMountPoint( menu_t&        point,
                                       const entry_t& e ) noexcept
{
    if ( auto val = std::get_if< std::string >( &e.value ) )
    {
        this->CreateMenuButton( point, *val );
    }
    else if ( auto val = std::get_if< std::vector< entry_t > >( &e.value ) )
    {
        for ( size_t i = 0, limit = val->size(); i < limit; ++i )
        {
            if ( i == 0 )
            {
                if ( auto buttonName =
                         std::get_if< std::string >( &( *val )[i].value ) )
                {
                    this->CreateMenuButton( point, *buttonName );
                }
                else
                {
                    LOG_ERROR( 0 )
                        << "The first entry of a sub menu must be a name "
                           "and not another sub menu. The syntax is "
                           "{\"subEntryName\", \"subEntry1\", \"subEntry2\"} "
                           "and not {{\"subEntry1\", \"subEntry2\"}}";
                }
            }
            else
            {
                if ( i == 1 ) this->CreateSubMenu( point );
                point.submenu->root.emplace_back();
                point.submenu->AddEntriesToMountPoint(
                    point.submenu->root.back(), ( *val )[i] );
            }
        }
    }

    point.button->SetCallback(
        [this, button = point.button.Get(), submenu = point.submenu]() {
            this->MenuButtonCallback( button, submenu );
        },
        STATE_HOVER, CallCondition::OnEnter );

    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Menu::AlignButtonInMenu( elWidget_Button* const button,
                                  elWidget_Menu* const   submenu ) noexcept
{
    glm::vec2 newPosition = button->GetPosition() + this->GetPosition();
    if ( this->GetHasVerticalAlignment() )
    {
        newPosition.x += button->GetSize().x;
        if ( !this->HasEnoughSpaceOnRightSide( newPosition, submenu ) )
            newPosition.x -= submenu->GetSize().x + button->GetSize().x;

        if ( !this->HasEnoughSpaceOnTheBottom( newPosition, submenu ) )
            newPosition.y -= submenu->GetSize().y - button->GetSize().y;
    }
    else
    {
        newPosition.y += button->GetSize().y;
        if ( !this->HasEnoughSpaceOnRightSide( newPosition, submenu ) )
            newPosition.x += button->GetSize().x - submenu->GetSize().x;
        if ( !this->HasEnoughSpaceOnTheBottom( newPosition, submenu ) )
            newPosition.y -= submenu->GetSize().y + button->GetSize().y;
    }
    submenu->SetPosition( newPosition );
}

void
elWidget_Menu::MenuButtonCallback( elWidget_Button* const button,
                                   elWidget_Menu* const   submenu ) noexcept
{
    if ( this->activeMenu ) this->HideMenu( this->activeMenu );
    if ( submenu )
    {
        this->activeMenu = submenu;
        submenu->SetDoRenderWidget( true );
        this->activeMenu->MoveWidgetToTop();
        this->AlignButtonInMenu( button, submenu );
        this->closeSubMenuOnClick->Register();
    }
}

bool
elWidget_Menu::HasEnoughSpaceOnRightSide(
    const glm::vec2& pos, const elWidget_Menu* menu ) const noexcept
{
    auto parentBorderSize = this->parent->GetBorderSize();
    return ( pos.x + menu->GetSize().x <=
             this->parent->GetSize().x - parentBorderSize[POSITION_LEFT] -
                 parentBorderSize[POSITION_RIGHT] );
}

bool
elWidget_Menu::HasEnoughSpaceOnTheBottom(
    const glm::vec2& pos, const elWidget_Menu* menu ) const noexcept
{
    auto parentBorderSize = this->parent->GetBorderSize();
    return ( pos.y + menu->GetSize().y <=
             this->parent->GetSize().y - parentBorderSize[POSITION_TOP] -
                 parentBorderSize[POSITION_BOTTOM] );
}

void
elWidget_Menu::AlignSubMenus() noexcept
{
    auto hasVerticalAlignment = this->GetHasVerticalAlignment();
    for ( auto& entry : this->root )
        if ( entry.submenu )
            entry.submenu->SetHasVerticalAlignment( hasVerticalAlignment );
}

void
elWidget_Menu::AdjustPositionPointOfOrigin() noexcept
{
    auto origin = this->GetPositionPointOfOrigin();
    for ( auto& entry : this->root )
        if ( entry.submenu ) entry.submenu->SetPositionPointOfOrigin( origin );
}

void
elWidget_Menu::AdjustSize( const glm::vec2& entryEndPosition ) noexcept
{
    auto borderSize = this->GetBorderSize();
    this->SetSize( { entryEndPosition.x + borderSize[POSITION_LEFT] +
                         borderSize[POSITION_RIGHT],
                     entryEndPosition.y + borderSize[POSITION_TOP] +
                         borderSize[POSITION_BOTTOM] } );
}

void
elWidget_Menu::Reshape()
{
    this->AlignSubMenus();
    this->AdjustPositionPointOfOrigin();

    glm::vec2 entryEndPosition{ 0.0f };
    for ( auto& entry : this->root )
    {
        if ( entry.button )
        {
            if ( this->GetHasVerticalAlignment() )
            {
                entry.button->SetPosition( { 0.0f, entryEndPosition.y } );
                entry.button->SetSize( { this->maxWidth + this->widthPadding,
                                         entry.button->GetSize().y } );
                entryEndPosition.y += entry.button->GetSize().y;
                entryEndPosition.x = entry.button->GetSize().x;
            }
            else
            {
                entry.button->SetPosition( { entryEndPosition.x, 0.0f } );
                entry.button->SetSize( { this->maxWidth + this->widthPadding,
                                         entry.button->GetSize().y } );
                entryEndPosition.y = entry.button->GetSize().y;
                entryEndPosition.x += entry.button->GetSize().x;
            }
        }
    }

    this->AdjustSize( entryEndPosition );
    elWidget_base::Reshape();
}

void
elWidget_Menu::Clear() noexcept
{
    this->maxWidth = 0;
    this->root.clear();
}

void
elWidget_Menu::SetEntries( const std::initializer_list< entry_t >& e ) noexcept
{
    this->Clear();

    for ( auto& entry : e )
    {
        this->root.emplace_back();
        this->AddEntriesToMountPoint( this->root.back(), entry );
    }
}

void
elWidget_Menu::AddEntry( const std::string& separator,
                         const std::string& entry ) noexcept
{
    auto entryVector = this->SeparatedStringToVector( separator, entry );
    if ( entryVector.empty() ) return;

    if ( this->root.empty() )
    {
        this->root.emplace_back();
        this->CreateMenuButton( this->root.back(), entryVector.front() );
        this->root.back().button->SetCallback(
            [me = this, button = this->root.back().button.Get(),
             submenu = this->root.back().submenu]() {
                me->MenuButtonCallback( button, submenu );
            },
            STATE_HOVER, CallCondition::OnEnter );
    }

    elWidget_Menu* currentRoot = this;

    for ( size_t i = 0, limit = entryVector.size(); i < limit; ++i )
    {
        bool    entryFound{ false };
        menu_t* subEntry{ nullptr };

        for ( auto& menu : currentRoot->root )
        {
            if ( menu.button->GetText() == entryVector[i] )
            {
                subEntry   = &menu;
                entryFound = true;
                break;
            }
        }

        if ( !entryFound )
        {
            currentRoot->root.emplace_back();
            currentRoot->CreateMenuButton( currentRoot->root.back(),
                                           entryVector[i] );

            if ( i + 1 < limit )
                currentRoot->CreateSubMenu( currentRoot->root.back() );

            currentRoot->root.back().button->SetCallback(
                [currentRoot = currentRoot,
                 button      = currentRoot->root.back().button.Get(),
                 submenu     = currentRoot->root.back().submenu]() {
                    currentRoot->MenuButtonCallback( button, submenu );
                },
                STATE_HOVER, CallCondition::OnEnter );
            currentRoot = currentRoot->root.back().submenu;
        }
        else
        {
            if ( !subEntry->submenu && i + 1 < limit )
            {
                currentRoot->CreateSubMenu( *subEntry );
                subEntry->button->SetCallback(
                    [currentRoot = currentRoot, button = subEntry->button.Get(),
                     submenu = subEntry->submenu]() {
                        currentRoot->MenuButtonCallback( button, submenu );
                    },
                    STATE_HOVER, CallCondition::OnEnter );
            }
            currentRoot = subEntry->submenu;
        }
    }

    this->runOnce.Call( _Method_Reshape_ );
}

void
elWidget_Menu::HideMenu( elWidget_Menu* menu ) noexcept
{
    menu->SetDoRenderWidget( false );
    for ( auto& w : menu->subMenus )
        HideMenu( w.Get() );
    this->closeSubMenuOnClick->Unregister();
}

elWidget_Button*
elWidget_Menu::GetEntryFromVector(
    const std::vector< std::string >& entry ) noexcept
{
    std::vector< menu_t >* currentRoot = &this->root;
    for ( size_t i = 0, limit = entry.size(); i < limit; ++i )
    {
        for ( auto& r : *currentRoot )
        {
            if ( r.button )
                if ( r.button->GetText() == entry[i] )
                {
                    if ( i == limit - 1 )
                        return r.button.Get();
                    else if ( r.submenu )
                        currentRoot = &r.submenu->root;
                    else
                        return nullptr;
                    break;
                }
        }
    }

    return nullptr;
}

elWidget_Button*
elWidget_Menu::GetEntry( const std::string& separator,
                         const std::string& entry ) noexcept
{
    auto entryVector = this->SeparatedStringToVector( separator, entry );
    return this->GetEntryFromVector( entryVector );
}

} // namespace GuiGL
} // namespace el3D
