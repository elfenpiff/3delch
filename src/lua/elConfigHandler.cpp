#include "lua/elConfigHandler.hpp"

#include <filesystem>


namespace el3D
{
namespace lua
{
elConfigHandler::elConfigHandler()
{
    this->create_isInitialized = true;
}

elConfigHandler::elConfigHandler( const std::string &path )
{
    this->SetConfig( path )
        .AndThen( [&] { this->create_isInitialized = true; } )
        .OrElse( [&]( const auto &r ) {
            this->create_isInitialized = false;
            this->create_error         = r;
        } );
}

bb::elExpected< elConfigHandler_Error >
elConfigHandler::SetConfig( const std::string &path,
                            const bool         recursiveDirectorySearch )
{
    namespace fs = std::filesystem;
    fs::path p( path );

    if ( fs::is_directory( p ) )
        return this->SetConfigDirectory( path, recursiveDirectorySearch );
    else if ( fs::is_regular_file( p ) )
        return this->SetConfigFile( path );
    else
    {
        std::cerr << path << " is neither file nor directory" << std::endl;
        return bb::Error( elConfigHandler_Error::InvalidPath );
    }
}

bb::elExpected< elConfigHandler_Error >
elConfigHandler::SetConfigFile( const std::string &file )
{
    if ( file.size() < 4 ||
         file.substr( file.size() - 4 ).compare( configFileSuffix ) )
    {
        std::cerr << "only lua files (*.lua) are supported as "
                     "config files: invalid file "
                  << file << std::endl;
        return bb::Error( elConfigHandler_Error::FileHasNoLuaSuffix );
    }

    utils::elFile rawFile( file );
    if ( !rawFile.Read() )
    {
        std::cerr << "unable to read file " << file << std::endl;
        return bb::Error( elConfigHandler_Error::UnableToReadFile );
    }

    auto luaScript = lua::elLuaScript::Create( rawFile.GetContentAsString() );

    if ( luaScript.HasError() )
        return bb::Error( elConfigHandler_Error::UnableToCreateLuaScript );

    this->configFiles.emplace( file, std::move( luaScript.GetValue() ) );

    return bb::Success<>();
}

bb::elExpected< elConfigHandler_Error >
elConfigHandler::SetConfigDirectory( const std::string &dir,
                                     const bool         recursive )
{
    namespace fs = std::filesystem;
    fs::path p( dir );

    if ( !fs::is_directory( p ) )
    {
        std::cerr << dir + " is not a directory" << std::endl;
        return bb::Error( elConfigHandler_Error::IsNotADirectory );
    }

    for ( auto iter = fs::directory_iterator( p );
          iter != fs::directory_iterator(); ++iter )
        if ( fs::is_regular_file( iter->path() ) )
            this->SetConfigFile( iter->path().string() );

        else if ( fs::is_directory( iter->path() ) && recursive )
            this->SetConfigDirectory( iter->path().string() );

    return bb::Success<>();
}

void
elConfigHandler::ClearFiles() noexcept
{
    this->configFiles.clear();
}

std::set< std::string >
elConfigHandler::GetConfigFileList() const noexcept
{
    std::set< std::string > retVal;

    for ( auto &e : this->configFiles )
        retVal.insert( e.first );

    return retVal;
}

bb::elExpected< elConfigHandler_Error >
elConfigHandler::IsEntryAvailable(
    const std::vector< std::string > &configPath ) const noexcept
{
    for ( auto &f : configFiles )
        if ( f.second->HasGlobal( configPath ) ) return bb::Success<>();
    return bb::Error( elConfigHandler_Error::UnableToFindEntry );
}

std::string
elConfigHandler::PathToString(
    const std::vector< std::string > &path ) const noexcept
{
    std::string retVal;
    for ( uint64_t i = 0, limit = path.size(); i < limit; ++i )
    {
        retVal += path[i];
        if ( i + 1 != limit ) retVal += ".";
    }
    return retVal;
}

} // namespace lua
} // namespace el3D
