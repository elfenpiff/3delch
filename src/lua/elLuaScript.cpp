#include "lua/elLuaScript.hpp"

#include "lua/lua_NAMESPACE_NAME.hpp"

#ifdef ADD_LUA_BINDINGS
#include <SDL.h>
#endif

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

namespace el3D
{
namespace lua
{
#ifdef ADD_LUA_BINDINGS
std::map< std::string, elLuaScript::class_t > elLuaScript::classes;

LUA_F( elLuaScript )
{
    elLuaScript::class_t newClass( "elLuaScript" );
    newClass.AddPrivateConstructor< elLuaScript >()
        .AddMethod< elLuaScript >( "AddThisPointer",
                                   &elLuaScript::AddThisPointer )
        .AddMethod< elLuaScript >( "SetSource", &elLuaScript::SetSource )
        .AddMethod< elLuaScript >( "Exec", &elLuaScript::Exec )
        .AddMethod< elLuaScript >( "RequireGlobal",
                                   &elLuaScript::RequireGlobal )
        .AddMethod< elLuaScript >( "RequireClass", &elLuaScript::RequireClass )
        .AddMethod< elLuaScript >( "RequireNamespace",
                                   &elLuaScript::RequireNamespace )
        .AddMethod< elLuaScript >( "CreateCallback",
                                   &elLuaScript::CreateCallback )
        .AddMethod< elLuaScript >(
            "CreateCallbackToFunction",
            &elLuaScript::CreateCallbackToFunction< void > )
        .AddMethod< elLuaScript >(
            "CreateCallbackToEventVerificationFunction",
            &elLuaScript::CreateCallbackToFunction< bool, SDL_Event > )
        .AddMethod< elLuaScript >(
            "CreateCallbackToEventCallbackFunction",
            &elLuaScript::CreateCallbackToFunction< void, SDL_Event > );

    newClass.AddStaticMethod( "CreateFromSource",
                              elLuaScript::Lua_Create< std::string > );
    newClass.AddStaticMethod( "Create", elLuaScript::Lua_Create<> );

    elLuaScript::RegisterLuaClass( newClass, NAMESPACE_NAME );
}
#endif

bool elLuaScript::preMainPhase{ true };
std::map< std::string, elLuaScript::namespace_t > elLuaScript::nameSpaces;
std::map< size_t, std::string >                   luaRegisteredTypes;
std::map< size_t, std::string >                   luaRegisteredTypesPtr;
std::map< std::string, std::function< void( elLuaScript *const ) > >
    elLuaScript::globalRegistrators;


int
LuaRegistrator( const std::function< void() > &f, const bool &doApply )
{
    static std::vector< std::function< void() > > registeredFunctions;
    if ( !doApply )
        registeredFunctions.push_back( f );
    else
    {
        for ( auto func : registeredFunctions )
            func();
    }

    return 0;
}

elLuaScript::elLuaScript() : luaState( luaL_newstate() )
{
    if ( this->luaState == nullptr )
    {
        LOG_ERROR( 0 ) << "unable to create new lua state";
        this->create_isInitialized = false;
        this->create_error         = elLuaScript_Error::UnableToCreateLuaState;
        return;
    }
    luaL_openlibs( this->luaState );
    this->create_isInitialized = true;
}

elLuaScript::elLuaScript( const std::string &source ) : elLuaScript()
{
    if ( !this->create_isInitialized ) return;

    auto call = this->SetSource( source );
    if ( call.HasError() )
    {
        this->create_isInitialized = false;
        this->create_error         = call.GetError();
    }
}

elLuaScript::elLuaScript( elLuaScript &&rhs )
    : luaState( std::move( rhs.luaState ) ),
      doExecBeforeCall( std::move( rhs.doExecBeforeCall ) ),
      source( std::move( rhs.source ) ),
      globalBlackList( std::move( rhs.globalBlackList ) ),
      active( std::move( rhs.active ) )
{
    rhs.luaState = nullptr;
}

elLuaScript::~elLuaScript()
{
    if ( this->luaState != nullptr ) lua_close( this->luaState );
}

bool
elLuaScript::HasLuaBindings() noexcept
{
#ifdef ADD_LUA_BINDINGS
    return true;
#else
    return false;
#endif
}

elLuaScript &
elLuaScript::operator=( elLuaScript &&rhs )
{
    if ( &rhs != this )
    {
        if ( this->luaState != nullptr ) lua_close( this->luaState );
        this->luaState         = std::move( rhs.luaState );
        this->doExecBeforeCall = std::move( rhs.doExecBeforeCall );
        this->source           = std::move( rhs.source );
        this->globalBlackList  = std::move( rhs.globalBlackList );
        this->active           = std::move( rhs.active );
        rhs.luaState           = nullptr;
    }

    return *this;
}

bb::elExpected< elLuaScript_Error >
elLuaScript::ErrorHandler( const int &state ) const
{
    switch ( state )
    {
        case LUA_ERRSYNTAX:
            LOG_ERROR( 0 )
                << "syntax error during pre-compilation in lua script : "
                << LuaTo< std::string >( this->luaState, -1 );
            return bb::Error( elLuaScript_Error::InvalidSyntax );
        case LUA_ERRRUN:
            LOG_ERROR( 0 ) << "runtime error : "
                           << LuaTo< std::string >( this->luaState, -1 );
            return bb::Error( elLuaScript_Error::RuntimeError );
        case LUA_ERRMEM:
            LOG_ERROR( 0 ) << "memory allocation error : "
                           << LuaTo< std::string >( this->luaState, -1 );
            return bb::Error( elLuaScript_Error::MemoryAllocationFailed );
        case LUA_ERRERR:
            LOG_ERROR( 0 )
                << "error while running the error handler function : "
                << LuaTo< std::string >( this->luaState, -1 );
            return bb::Error( elLuaScript_Error::UndefinedError );
        default:
            return bb::Success<>();
    }
}

bb::elExpected< elLuaScript_Error >
elLuaScript::SetSource( const std::string &_source )
{
    auto call = this->ErrorHandler(
        luaL_loadstring( this->luaState, _source.c_str() ) );
    if ( call.HasError() ) return bb::Error( call.GetError() );

    this->doExecBeforeCall = true;
    this->source           = _source;

    this->PopulateGlobalBlacklist();
    return bb::Success<>();
}

bb::elExpected< elLuaScript_Error >
elLuaScript::Exec() const
{
    return ErrorHandler( lua_pcall( this->luaState, 0, 0, 0 ) )
        .AndThen( [&] { this->doExecBeforeCall = false; } );
}

std::vector< std::string >
elLuaScript::GetGlobalTableEntries(
    const std::vector< std::string > &lst ) const noexcept
{
    if ( this->doExecBeforeCall ) this->Exec();
    if ( lst.empty() ) return std::vector< std::string >();

    // find the right place in table
    for ( auto entry = lst.begin(); entry != lst.end(); ++entry )
    {
        if ( entry == lst.begin() )
        { // get table name
            lua_getglobal( this->luaState, entry->c_str() );
            if ( !lua_istable( this->luaState, -1 ) )
            {
                LOG_WARN( 0 ) << "table " << *entry << " is not a table";
                return std::vector< std::string >();
            }
        }
        else
        { // iterate through entries till the right one is found
            bool hasFound = false;

            if ( lua_istable( this->luaState, -1 ) )
            {
                for ( lua_pushnil( this->luaState );
                      lua_next( this->luaState, -2 );
                      lua_pop( this->luaState, 1 ) )
                {
                    if ( !LuaTo< std::string >( this->luaState, -2 )
                              .compare( *entry ) )
                    {
                        if ( !lua_istable( this->luaState, -1 ) )
                        {
                            LOG_WARN( 0 )
                                << "table entry " << *entry << " in table "
                                << *lst.begin() << " is not a table";
                            lua_pop( this->luaState,
                                     lua_gettop( this->luaState ) );
                            return std::vector< std::string >();
                        }

                        hasFound = true;
                        break;
                    }
                }
            }

            if ( !hasFound )
            {
                LOG_WARN( 0 ) << "table entry " << *entry << " in table "
                              << *lst.begin() << " not found";
                lua_pop( this->luaState, lua_gettop( this->luaState ) );
                return std::vector< std::string >();
            }
        }
    }


    // collect table entries at current position
    std::vector< std::string > value;
    if ( lua_istable( this->luaState, -1 ) )
    {
        for ( lua_pushnil( this->luaState ); lua_next( this->luaState, -2 );
              lua_pop( this->luaState, 1 ) )
            value.push_back( LuaTo< std::string >( this->luaState, -2 ) );
    }

    lua_pop( this->luaState, lua_gettop( this->luaState ) );
    return value;
}

bb::elExpected< elLuaScript_Error >
elLuaScript::GetGlobalFunc( const std::string &val ) const
{
    lua_getglobal( this->luaState, val.c_str() );
    if ( lua_isfunction( this->luaState, -1 ) != 1 )
    {
        LOG_ERROR( 0 ) << val << " is not a function";
        return bb::Error( elLuaScript_Error::NotAFunction );
    }
    return bb::Success<>();
}

void
elLuaScript::RequireNamespace( const std::string &name )
{
    if ( this->active.namespaces.find( name ) != this->active.namespaces.end() )
        return;

    auto iter = elLuaScript::nameSpaces.find( name );
    if ( iter != elLuaScript::nameSpaces.end() )
    {
        for ( auto registerFunc : iter->second.functions )
            registerFunc( this->luaState );
        for ( auto registerFunc : iter->second.variables )
            registerFunc( this );
        for ( auto registerFunc : iter->second.classes )
            registerFunc( this );
        for ( auto registerFunc : iter->second.constants )
            registerFunc( this );
    }

    this->active.namespaces.insert( name );
}

void
elLuaScript::SetField( const std::string &value ) const noexcept
{
    LuaPush( this->luaState, value );
    lua_insert( this->luaState, -2 );
    lua_rawset( this->luaState, -3 );
}

#ifdef ADD_LUA_BINDINGS
void
elLuaScript::CreateClassTable( const elLuaScript::class_t &c,
                               const bool &isPtrToClass ) const noexcept
{
    std::string name = c.name;
    if ( isPtrToClass ) name += "_ptr";

    luaL_newmetatable( this->luaState,
                       std::string( "luaClass_" + name ).c_str() );

    if ( !isPtrToClass )
    {
        // constructor
        LuaPush( this->luaState, std::string( "luaClass_" + name ) );
        lua_pushcclosure( this->luaState, c.ctor, 1 );
        this->SetField( "new" );

        // destructor
        lua_pushcclosure( this->luaState, c.dtor, 0 );
        this->SetField( "__gc" );
    }
    else
    {
        // manual destructor
        lua_pushcclosure( this->luaState, c.dtor, 0 );
        this->SetField( "delete" );
    }

    // methods
    for ( auto method : c.methods )
    {
        std::get< 2 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 1 >( method ), 1 );
        this->SetField( std::get< 0 >( method ) );
    }

    // static methods
    for ( auto method : c.staticMethods )
    {
        std::get< 2 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 1 >( method ), 1 );
        this->SetField( std::get< 0 >( method ) );
    }

    // Get/Set Var
    for ( auto method : c.methodsGetSetVar )
    {
        std::get< 2 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 1 >( method ), 1 );
        this->SetField( "Set_" + std::get< 0 >( method ) );

        std::get< 4 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 3 >( method ), 1 );
        this->SetField( "Get_" + std::get< 0 >( method ) );
    }

    // Get/Set Static Var
    for ( auto method : c.methodsGetSetStaticVar )
    {
        std::get< 2 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 1 >( method ), 1 );
        this->SetField( "Set_" + std::get< 0 >( method ) );

        std::get< 4 >( method )( this->luaState );
        lua_pushcclosure( this->luaState, std::get< 3 >( method ), 1 );
        this->SetField( "Get_" + std::get< 0 >( method ) );
    }

    c.thisPointerGetCreator( this->luaState );
    this->SetField( "ThisPointer" );

    // register table
    lua_pushvalue( this->luaState, -1 );
    lua_setfield( this->luaState, -1, "__index" );
    lua_setglobal( this->luaState, name.c_str() );
}

bb::elExpected< elLuaScript_Error >
elLuaScript::RequireClass( const std::string &className ) const
{
    auto iter = elLuaScript::classes.find( className );
    if ( iter == elLuaScript::classes.end() )
    {
        LOG_ERROR( 0 ) << "class " << className << " is not registered";
        return bb::Error( elLuaScript_Error::ClassIsNotRegistered );
    }

    this->CreateClassTable( iter->second, false );
    this->CreateClassTable( iter->second, true );

    this->active.classes.insert( className );
    return bb::Success<>();
}

void
elLuaScript::RegisterLuaClass( const elLuaScript::class_t &newClass,
                               const std::string &         nameSpace )
{
    if ( !newClass.constructorDefined )
        throw class_t::NoConstructorDefined_e( std::string(
            "The class " + newClass.name + " has no constructer defined!" ) );

    auto iter = elLuaScript::classes.find( newClass.name );
    if ( iter != elLuaScript::classes.end() )
        LOG_WARN( 0 ) << "A class with the name " << newClass.name
                      << "is already registered and will be overridden!";

    elLuaScript::classes.insert( std::make_pair( newClass.name, newClass ) );

    luaRegisteredTypes[newClass.typeHashCode] = "luaClass_" + newClass.name;
    luaRegisteredTypesPtr[newClass.typeHashCodePtr] =
        "luaClass_" + newClass.name + "_ptr";

    if ( !nameSpace.empty() )
        elLuaScript::nameSpaces[nameSpace].classes.push_back(
            std::bind( &elLuaScript::RequireClass, std::placeholders::_1,
                       newClass.name ) );
}
#endif

std::vector< std::string >
elLuaScript::GetGlobalNames() const noexcept
{
    if ( this->doExecBeforeCall ) this->Exec();

    std::vector< std::string > retVal;
#if LUA_VERSION_NUM >= 502
    lua_pushglobaltable( this->luaState );
#else
    lua_pushvalue( this->luaState, LUA_GLOBALSINDEX );
#endif
    lua_pushnil( this->luaState );

    while ( lua_next( this->luaState, -2 ) != 0 )
    {
        std::string globalVarName = LuaTo< std::string >( this->luaState, -2 );
        auto        iter          = this->globalBlackList.find( globalVarName );

        if ( iter == this->globalBlackList.end() )
            retVal.emplace_back( globalVarName );

        lua_pop( this->luaState, 1 );
    }

    lua_pop( this->luaState, lua_gettop( this->luaState ) );

    return retVal;
}

void
elLuaScript::PopulateGlobalBlacklist() noexcept
{
    this->globalBlackList.clear();

#if LUA_VERSION_NUM >= 502
    lua_pushglobaltable( this->luaState );
#else
    lua_pushvalue( this->luaState, LUA_GLOBALSINDEX );
#endif
    lua_pushnil( this->luaState );

    while ( lua_next( this->luaState, -2 ) != 0 )
    {
        this->globalBlackList.insert(
            LuaTo< std::string >( this->luaState, -2 ) );

        lua_pop( this->luaState, 1 );
    }

    lua_pop( this->luaState, 1 );
}

int
elLuaScript::GetGlobalType( const std::string &varName ) const noexcept
{
    if ( this->doExecBeforeCall ) this->Exec();

    lua_getglobal( this->luaState, varName.c_str() );

    return lua_type( this->luaState, -1 );
}

bool
elLuaScript::IterateToEntryInTable( const std::string &entry ) const noexcept
{
    if ( lua_istable( this->luaState, -1 ) )
    {
        for ( lua_pushnil( this->luaState );
              lua_next( this->luaState, -2 ) != 0;
              lua_pop( this->luaState, 1 ) )
        {
            if ( !LuaTo< std::string >( this->luaState, -2 ).compare( entry ) )
                return true;
        }
    }
    return false;
}

bool
elLuaScript::HasGlobal( const std::vector< std::string > &lst ) const noexcept
{
    if ( this->doExecBeforeCall ) this->Exec();
    if ( lst.empty() ) return true;

    for ( auto entry = lst.begin(); entry != lst.end(); ++entry )
    {
        if ( entry == lst.begin() )
        { // get table name
            lua_getglobal( this->luaState, entry->c_str() );
            // not a table return var value
            if ( !lua_isnil( this->luaState, -1 ) &&
                 !lua_istable( this->luaState, -1 ) )
            {
                return true;
            }
            else if ( lua_isnil( this->luaState, -1 ) )
            {
                return false;
            }
        }
        else
        {
            if ( !this->IterateToEntryInTable( *entry ) )
            {
                lua_pop( this->luaState, lua_gettop( this->luaState ) );
                return false;
            }
        }
    }

    return true;
}

std::function< void() >
elLuaScript::CreateCallback()
{
    return std::function< void() >( [this] { this->Exec(); } );
}

bb::elExpected< elLuaScript_Error >
elLuaScript::RequireGlobal( const std::string &groupName )
{
    auto iter = globalRegistrators.find( groupName );
    if ( iter != globalRegistrators.end() )
    {
        iter->second( this );
        return bb::Success<>();
    }
    else
    {
        LOG_ERROR( 0 ) << "global " + groupName + " is not registered";
        return bb::Error( elLuaScript_Error::GlobalNotRegistered );
    }
}

void
elLuaScript::AddThisPointer()
{
    this->SetGlobal< elLuaScript * >( "this", this );
}

bb::elExpected< elLuaScript_Error >
elLuaScript::AddModuleSearchPath( const std::string &path )
{
    lua_getglobal( this->luaState, "package" );
    if ( lua_isnil( this->luaState, -1 ) )
    {
        LOG_ERROR( 0 ) << "unable to find package.path global";
        return bb::Error( elLuaScript_Error::PackagePathGlobalNotFound );
    }

    lua_getfield( this->luaState, -1, "path" );
    if ( lua_isnil( this->luaState, -1 ) )
    {
        LOG_ERROR( 0 ) << "unable to find package.path global";
        return bb::Error( elLuaScript_Error::PackagePathGlobalNotFound );
    }

    std::string currentPath = LuaTo< std::string >( this->luaState, -1 );
    currentPath += ";" + path;
    lua_pop( this->luaState, 1 );
    LuaPush( this->luaState, currentPath );
    lua_setfield( this->luaState, -2, "path" );
    lua_pop( this->luaState, 1 );

    return bb::Success<>();
}

bb::elExpected< elLuaScript_Error >
elLuaScript::CallVoid( const std::string &func ) const
{
    if ( this->doExecBeforeCall ) this->Exec();

    auto call = this->GetGlobalFunc( func ).AndThen(
        [&] {
            return ErrorHandler(
                lua_pcall( this->luaState, 0, LUA_MULTRET, 0 ) );
        } );
    if ( call.HasError() ) return bb::Error( call.GetError() );

    return bb::Success<>();
}

} // namespace lua
} // namespace el3D
