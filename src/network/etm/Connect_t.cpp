#include "network/etm/Connect_t.hpp"

#include "logging/elLog.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/elByteDeserializer.hpp"
#include "utils/elByteSerializer.hpp"

namespace el3D
{
namespace network
{
namespace etm
{
uint64_t
Connect_t::Request::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize(
        this->protocol_version, this->connection_id,
        this->rpc_interval_timeout_ms );
}

uint64_t
Connect_t::Request::Serialize( const utils::Endian  endianness,
                               utils::byteStream_t& byteStream,
                               const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->protocol_version,
        this->connection_id, this->rpc_interval_timeout_ms );
}

bb::elExpected< utils::ByteStreamError >
Connect_t::Request::Deserialize( const utils::Endian        endianness,
                                 const utils::byteStream_t& byteStream,
                                 const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->protocol_version, this->connection_id,
                  this->rpc_interval_timeout_ms )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Connect_t::Request_t<T>", r, byteStream, byteStreamOffset );
        } );
}

uint64_t
Connect_t::Response::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->connection_id,
                                                       this->port );
}

uint64_t
Connect_t::Response::Serialize( const utils::Endian  endianness,
                                utils::byteStream_t& byteStream,
                                const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->connection_id,
        this->port );
}

bb::elExpected< utils::ByteStreamError >
Connect_t::Response::Deserialize( const utils::Endian        endianness,
                                  const utils::byteStream_t& byteStream,
                                  const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->connection_id, this->port )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Connect_t::Response_t<T>", r, byteStream, byteStreamOffset );
        } );
}


} // namespace etm
} // namespace network
} // namespace el3D
