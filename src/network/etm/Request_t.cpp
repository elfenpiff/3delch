#include "network/etm/Request_t.hpp"

namespace el3D
{
namespace network
{
namespace etm
{

uint64_t
Request_t< void >::GetSerializedSize() const noexcept
{
    return utils::elByteSerializer::GetSerializedSize( this->id );
}

uint64_t
Request_t< void >::Serialize( const utils::Endian  endianness,
                              utils::byteStream_t& byteStream,
                              const uint64_t byteStreamOffset ) const noexcept
{
    return utils::elByteSerializer::FillByteStreamWithSerialization(
        endianness, byteStream, byteStreamOffset, this->id );
}

bb::elExpected< utils::ByteStreamError >
Request_t< void >::Deserialize( const utils::Endian        endianness,
                                const utils::byteStream_t& byteStream,
                                const uint64_t byteStreamOffset ) noexcept
{
    return utils::elByteDeserializer( byteStream, byteStreamOffset )
        .Extract( endianness, this->id )
        .OrElse( [&]( const auto& r ) {
            utils::byteStream_t::ByteStreamErrorToStderr(
                "Request_t<void>", r, byteStream, byteStreamOffset );
        } );
}
} // namespace etm
} // namespace network
} // namespace el3D
