#include "network/elNetworkSocket_Base.hpp"

#include "concurrent/elThreadPool.hpp"
#include "logging/elLog.hpp"
#include "network/network_NAMESPACE_NAME.hpp"
#include "utils/elGenericRAII.hpp"
#include "utils/smart_c.hpp"

#include <cstring>


#ifndef _WIN32
#include <netinet/tcp.h>
#include <unistd.h>
#else
#include <io.h>
#endif

using namespace ::el3D::network::elNetworkSocket::internal;

// clang-format off
#include "buildingBlocks/WindowsFuckupCorrection.hpp"
// clang-format on

#ifdef _WIN32
void
InitWindowsWSASystem()
{
    static bool isInitialized{ false };

    if ( !isInitialized )
    {
        WSADATA wsaData;
        int     wsaStartupCode = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
        if ( wsaStartupCode != 0 )
        {
            LOG_FATAL( 0 ) << "WSAStartup call failed, error code = "
                           << std::to_string( wsaStartupCode );
            throw std::runtime_error( "WSAStartup failed" );
        }
        else
        {
            isInitialized = true;
        }
    }
}

void
CleanupWindowsWSASystem()
{
    if ( WSACleanup() != 0 )
    {
        LOG_FATAL( 0 ) << "WSACleanup call failed, error code = "
                       << std::to_string( WSAGetLastError() );
        throw std::runtime_error( "WSACleanup failed" );
    }
}

static el3D::utils::elGenericRAII wsaSystem( [] { InitWindowsWSASystem(); },
                                             [] {
                                                 CleanupWindowsWSASystem();
                                             } );

#endif


namespace el3D
{
namespace network
{
elNetworkSocket_Base::elNetworkSocket_Base()
    : activeObject( concurrent::elThreadPool::GetGlobalThreadPool() )
{
}

elNetworkSocket_Base::~elNetworkSocket_Base()
{
    if ( this->socketFileDescriptor != INVALID_SOCKET )
        this->Close( this->socketFileDescriptor );
}

bb::elExpected< std::optional< utils::byteStream_t >, elNetworkSocket_Error >
elNetworkSocket_Base::Read(
    const socketHandleType_t fileDescriptor ) const noexcept
{
    utils::byteStream_t socketBuffer( this->maxSocketBufferSize );

#ifndef _WIN32
    auto recvCall = utils::smart_c(
        recv, []( auto v ) { return v == -1; }, fileDescriptor,
        socketBuffer.stream.data(), this->maxSocketBufferSize - 1, 0 );

    if ( recvCall.HasError() )
    {
        LOG_ERROR( 0 ) << "could not read from socket";
        return bb::Error( elNetworkSocket_Error::UnableToReadSocket );
    }

    ssize_t socketBufferSize = recvCall.GetValue();
#else
    int socketBufferSize = recv(
        fileDescriptor, reinterpret_cast< char* >( socketBuffer.stream.data() ),
        this->maxSocketBufferSize - 1, 0 );

    if ( socketBufferSize < 0 )
    {
        LOG_ERROR( 0 ) << "could not read from socket ::: "
                       << "file descriptor " << fileDescriptor
                       << " (recv) : " << WSAGetLastError();
        return bb::Error( elNetworkSocket_Error::UnableToReadSocket );
    }
#endif
    if ( socketBufferSize == 0 )
        return bb::Success(
            std::optional< utils::byteStream_t >( std::nullopt ) );

    socketBuffer.stream.resize( static_cast< size_t >( socketBufferSize ) );
    return bb::Success( std::make_optional( socketBuffer ) );
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Base::Open()
{
#ifndef _WIN32
    utils::smart_c(
        socket, []( auto v ) { return v == -1; }, AF_INET, SOCK_STREAM, 0 )
        .AndThen( [&]( const auto& r ) { this->socketFileDescriptor = r; } )
        .OrElse( [] { LOG_ERROR( 0 ) << "unable to open socket"; } );
#else
    this->socketFileDescriptor = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
    if ( this->socketFileDescriptor == INVALID_SOCKET )
    {
        LOG_ERROR( 0 ) << "socket call failed, error code "
                       << std::to_string( WSAGetLastError() );
    }
#endif

    if ( this->socketFileDescriptor == INVALID_SOCKET )
        return bb::Error( elNetworkSocket_Error::UnableToCreateSocket );

    this->EnableTCPNoDelay( this->socketFileDescriptor );

    return bb::Success<>();
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Base::Shutdown( const socketHandleType_t fileDescriptor )
{
#ifndef _WIN32
    auto call = utils::smart_c(
        shutdown, []( auto v ) { return v == -1; }, fileDescriptor, SHUT_RDWR );
    if ( call.HasError() )
    {
        LOG_ERROR( 0 ) << "unable to set socket in connection shut down state";
        return bb::Error( elNetworkSocket_Error::UnableToShutdownSocket );
    }
#else
    if ( shutdown( fileDescriptor, SD_SEND ) != 0 )
    {
        LOG_ERROR( 0 ) << "shutdown call failed, error code = "
                       << std::to_string( WSAGetLastError() );
        return bb::Error( elNetworkSocket_Error::UnableToShutdownSocket );
    }
#endif

    return bb::Success<>();
}

void
elNetworkSocket_Base::Close( const socketHandleType_t fileDescriptor )
{
#ifndef _WIN32
    close( fileDescriptor );
#else
    closesocket( fileDescriptor );
#endif
}

bool
elNetworkSocket_Base::IsConnected() const noexcept
{
    return this->isConnected.load();
}

bb::elExpected< elNetworkSocket_Error >
elNetworkSocket_Base::SendToSocket(
    const socketHandleType_t   socketDescriptor,
    const utils::byteStream_t& data ) const noexcept
{
#ifndef _WIN32
    auto sendCall = utils::smart_c(
        send, []( auto v ) { return v == SEND_ERROR; }, socketDescriptor,
        data.stream.data(), data.stream.size(), 0 );
    if ( sendCall.HasError() )
    {
        switch ( sendCall.GetError() )
        {
            case ECONNRESET:
                return bb::Error(
                    elNetworkSocket_Error::ConnectionResetByPeer );
            default:
                return bb::Error(
                    elNetworkSocket_Error::UnableToSendMessageToSocket );
        }
    }
#else
    if ( send( socketDescriptor,
               reinterpret_cast< const char* >( data.stream.data() ),
               data.stream.size(), 0 ) == SEND_ERROR )
    {
        LOG_ERROR( 0 ) << "unable to send message to socket";
        return bb::Error( elNetworkSocket_Error::UnableToSendMessageToSocket );
    }
#endif

    return bb::Success<>();
}

void
elNetworkSocket_Base::EnableTCPNoDelay(
    const socketHandleType_t fileDescriptor ) const noexcept
{
#ifdef _WIN32
    const char enableTcpNoDelay = 1;
    utils::smart_c(
        setsockopt, []( auto v ) { return v == -1; }, fileDescriptor,
        IPPROTO_TCP, TCP_NODELAY, &enableTcpNoDelay,
        static_cast< unsigned int >( sizeof( enableTcpNoDelay ) ) );
#else
    int enableTcpNoDelay = 1;
    utils::smart_c(
        setsockopt, []( auto v ) { return v == -1; }, fileDescriptor,
        IPPROTO_TCP, TCP_NODELAY, &enableTcpNoDelay,
        static_cast< unsigned int >( sizeof( enableTcpNoDelay ) ) );
#endif
}

} // namespace network
} // namespace el3D
