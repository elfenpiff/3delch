#include "network/elEasyTCPMessaging_Server.hpp"

#include "buildingBlocks/algorithm_extended.hpp"
#include "concurrent/elThreadPool.hpp"
#include "logging/elLog.hpp"
#include "network/elNetworkSocket_internal.hpp"
#include "network/etm/Connect_t.hpp"
#include "network/etm/Identify_t.hpp"
#include "network/etm/Request_t.hpp"
#include "network/etm/Transmission_t.hpp"

#include <glm/fwd.hpp>


namespace el3D
{
namespace network
{

elEasyTCPMessaging_Server::elEasyTCPMessaging_Server(
    const std::string &serviceID, const uint32_t serviceVersion ) noexcept
    : messageReceivedTrigger( 0 ),
      activeObject( concurrent::elThreadPool::GetGlobalThreadPool() ),
      serviceID( serviceID ), serviceVersion( serviceVersion )
{
    this->server.SetOnConnectCallback(
        [this]( const network::socketHandleType_t handle )
        { this->ManagementConnectionCallback( handle ); } );
}

bb::elExpected< elNetworkSocket_Error >
elEasyTCPMessaging_Server::Listen( const uint16_t port ) noexcept
{
    auto result = this->server.Listen( port );
    if ( !result.HasError() )
        LOG_INFO( 0 ) << "ETM Server [ " << this->serviceID << ":"
                      << this->serviceVersion << " ] is listening on port "
                      << port;
    return result;
}

void
elEasyTCPMessaging_Server::StopListen() noexcept
{
    LOG_INFO( 0 ) << "ETM Server [" << this->serviceID << ":"
                  << this->serviceVersion << "] shutting down ";
    this->server.StopListen();
}

void
elEasyTCPMessaging_Server::ManagementConnectionCallback(
    const network::socketHandleType_t handle ) noexcept
{
    this->activeObject.AddTask( [this, handle]
                                { this->ManagementPortThread( handle ); } );
}

bool
elEasyTCPMessaging_Server::SendError( const network::socketHandleType_t handle,
                                      const uint64_t transmissionID ) noexcept
{
    etm::Transmission_t< void > error;
    error.transmission_id = transmissionID;
    error.payload.transport_type_id =
        static_cast< uint32_t >( etm::MessageType::Error );

    return this->SendInternalMessage(
        handle,
        utils::elByteSerializer()
            .Serialize( utils::Endian::Big, error )
            .GetByteStream(),
        "error message" );
}

bool
elEasyTCPMessaging_Server::SendInternalMessage(
    const network::socketHandleType_t handle,
    const utils::byteStream_t &       message,
    const std::string &               messageType ) noexcept
{
    if ( this->server.Send( handle, message ).HasError() )
    {
        auto info = this->server.GetConnectionInformation( handle );
        if ( info.has_value() )
            LOG_ERROR( 0 ) << "unable to send " << messageType << " to "
                           << info->hostName << "(" << info->hostIP << ")";
        else
            LOG_ERROR( 0 ) << "unable to send " << messageType
                           << " - connection terminated?";
        return false;
    }
    return true;
}

bool
elEasyTCPMessaging_Server::SendRawMessage( const utils::byteStream_t &message,
                                           const size_t connectionID ) noexcept
{
    auto &c = this->connectionVector[connectionID];
    if ( !c->hasConnection ) return false;
    if ( c->connection->GetConnections().empty() )
    {
        this->Disconnect( connectionID );
        return false;
    }

    if ( c->connection->Send( c->handle, message )
             .OrElse( [&] { this->Disconnect( connectionID ); } )
             .HasError() )
        return false;

    return true;
}


void
elEasyTCPMessaging_Server::RespondToIdentifyRequest(
    const network::socketHandleType_t handle,
    const utils::byteStream_t &       byteStream,
    const uint64_t                    transmissionID ) noexcept
{
    etm::Transmission_t< etm::Request_t< etm::Identify_t::Request > > request;

    if ( utils::elByteDeserializer( byteStream )
             .Extract( utils::Endian::Big, request )
             .HasError() )
    {
        LOG_WARN( 0 ) << "received invalid identification request ["
                      << byteStream.GetHumanReadableString() << "]";
        this->SendError( handle, transmissionID );
    }
    else
    {
        etm::Transmission_t< etm::Request_t< etm::Identify_t::Response > >
            response;

        response.transmission_id = request.transmission_id;
        response.payload.transport_type_id =
            static_cast< uint32_t >( etm::MessageType::Response );
        response.payload.payload.id =
            static_cast< uint32_t >( etm::RequestID::Identify );
        response.payload.payload.payload.id = this->serviceID;
        response.payload.payload.payload.service_protocol_version =
            this->serviceVersion;

        this->SendInternalMessage(
            handle,
            utils::elByteSerializer()
                .Serialize( utils::Endian::Big, response )
                .GetByteStream(),
            "identify response" );
        LOG_DEBUG( 0 ) << "sending identification response [" << this->serviceID
                       << ":" << this->serviceVersion << "]";
    }
}

void
elEasyTCPMessaging_Server::RespondToConnectRequest(
    const network::socketHandleType_t handle,
    const utils::byteStream_t &       byteStream,
    const uint64_t                    transmissionID ) noexcept
{
    etm::Transmission_t< etm::Request_t< etm::Connect_t::Request > > request;
    if ( utils::elByteDeserializer( byteStream )
             .Extract( utils::Endian::Big, request )
             .HasError() )
    {
        LOG_WARN( 0 ) << "received invalid connection request ["
                      << byteStream.GetHumanReadableString() << "]";
        this->SendError( handle, transmissionID );
    }
    else
    {
        auto port = this->OpenPortForIncomingConnection();
        if ( port.has_value() )
        {
            etm::Transmission_t< etm::Request_t< etm::Connect_t::Response > >
                response;

            response.transmission_id = request.transmission_id;
            response.payload.transport_type_id =
                static_cast< uint32_t >( etm::MessageType::Response );
            response.payload.payload.id =
                static_cast< uint32_t >( etm::RequestID::Connect );
            response.payload.payload.payload.connection_id =
                request.payload.payload.payload.connection_id;
            response.payload.payload.payload.port = *port;

            this->SendInternalMessage(
                handle,
                utils::elByteSerializer()
                    .Serialize( utils::Endian::Big, response )
                    .GetByteStream(),
                "connect response" );
            LOG_DEBUG( 0 ) << "ETM server [" << this->serviceID << ":"
                           << this->serviceVersion
                           << "] sending connection invitation for port "
                           << *port;
        }
        else
        {
            LOG_ERROR( 0 ) << "unable to find open port to listen to";
        }
    }
}

std::optional< uint16_t >
elEasyTCPMessaging_Server::OpenPortForIncomingConnection() noexcept
{
    auto   freeConnectionID = this->freeConnectionIDs->Pop();
    size_t connectionID     = 0;

    if ( !freeConnectionID.has_value() )
    {
        this->connectionVector.emplace_back( new connection_t );
        this->connectionVector.back()->connection->SetOnReceiveCallback(
            [this]( const network::socketHandleType_t )
            {
                this->messageReceivedTrigger.SetWithSetter(
                    []( const uint64_t &n ) { return n + 1; } );
                this->messageReceivedTrigger.TriggerAll();
            } );
        connectionID = this->connectionVector.size() - 1;
    }
    else
    {
        connectionID = freeConnectionID.value();
    }

    auto &serverRef = this->connectionVector[connectionID]->connection;
    auto  result    = serverRef->Listen();
    if ( !result.HasError() )
    {
        uint16_t port = result.GetValue();
        serverRef->SetOnConnectCallback(
            [this, connectionID]( const socketHandleType_t handle )
            { this->ServiceConnectionCallback( handle, connectionID ); } );

        // make a copy of the pointer otherwise the vector gets changed
        // during running this thread and at an arbitrary point inside the
        // AddTask thread the this->connectionVector elements become invalid
        connection_t *c = this->connectionVector[connectionID].get();
        this->activeObject.AddTask(
            [this, c, connectionID, port] {
                this->CloseConnectionAfterTimeoutThread( c, connectionID,
                                                         port );
            } );

        return std::make_optional( port );
    }
    else
        return std::nullopt;
}

// this method runs asynchronously to all other methods
void
elEasyTCPMessaging_Server::CloseConnectionAfterTimeoutThread(
    connection_t *const c, const size_t connectionID,
    const uint16_t port ) noexcept
{
    std::unique_lock< std::mutex > lock( c->predicateMutex );
    if ( !c->isConnectedTrigger.wait_for(
             lock, std::chrono::duration< double >( this->timeout ),
             [&]() -> bool { return c->hasConnection; } ) )
    {
        LOG_ERROR( 0 ) << "timeout, received no connection on port " << port
                       << ". closing port!";
        this->Disconnect( connectionID );
    }
}

void
elEasyTCPMessaging_Server::ServiceConnectionCallback(
    const network::socketHandleType_t handle,
    const size_t                      connectionID ) noexcept
{
    {
        auto &                         c = this->connectionVector[connectionID];
        std::unique_lock< std::mutex > lock( c->predicateMutex );
        c->hasConnection = true;
        c->isConnectedTrigger.notify_one();
        c->handle = handle;
    }

    if ( this->asyncConnectionCallback )
    {
        this->activeObject.AddTask(
            [this, handle, connectionID]
            {
                this->asyncConnectionCallback(
                    &this->connectionVector[connectionID]->connection, handle );
            } );
    }
}

void
elEasyTCPMessaging_Server::HandleManagementRequests(
    const network::socketHandleType_t handle, const uint32_t requestID,
    const utils::byteStream_t &byteStream,
    const uint64_t             transmissionID ) noexcept
{
    switch ( static_cast< etm::RequestID >( requestID ) )
    {
        case etm::RequestID::Identify:
        {
            this->RespondToIdentifyRequest( handle, byteStream,
                                            transmissionID );
            break;
        }
        case etm::RequestID::Connect:
        {
            this->RespondToConnectRequest( handle, byteStream, transmissionID );
            break;
        }
        default:
        {
            LOG_WARN( 0 ) << "received invalid request id (" << requestID
                          << ")";
            this->SendError( handle, transmissionID );
            break;
        }
    }
}

// this method runs asynchronously to all other methods
void
elEasyTCPMessaging_Server::ManagementPortThread(
    const network::socketHandleType_t handle ) noexcept
{
    auto info = this->server.GetConnectionInformation( handle );
    if ( info.has_value() )
    {
        auto received = this->server.TimedReceive( handle, this->timeout );

        if ( received.has_value() && !received->stream.empty() )
        {
            etm::Transmission_t< etm::Request_t< void > > rawRequest;

            if ( utils::elByteDeserializer( *received )
                     .Extract( utils::Endian::Big, rawRequest )
                     .HasError() )
            {
                LOG_WARN( 0 ) << "received invalid request ["
                              << received->GetHumanReadableString() << "] from "
                              << info->hostName << " (" << info->hostIP << ")";
                this->SendError( handle, 0 );
            }
            else if ( rawRequest.payload.transport_type_id !=
                      static_cast< uint32_t >( etm::MessageType::Request ) )
            {
                LOG_WARN( 0 )
                    << "received transport type "
                    << rawRequest.payload.transport_type_id
                    << " on an request type ("
                    << static_cast< uint32_t >( etm::MessageType::Request )
                    << ") only port";
                this->SendError( handle, rawRequest.transmission_id );
            }
            else
            {
                auto requestID = rawRequest.payload.payload.id;
                this->HandleManagementRequests( handle, requestID, *received,
                                                rawRequest.transmission_id );
            }
        }

        this->server.CloseConnection( handle );
    }
}

units::Time
elEasyTCPMessaging_Server::GetTimeout() const noexcept
{
    return this->timeout;
}

void
elEasyTCPMessaging_Server::SetTimeout( const units::Time &timeout ) noexcept
{
    this->timeout = timeout;
}

void
elEasyTCPMessaging_Server::SetAsynchronousConnectionCallback(
    const asyncConnectionCallback_t &v ) noexcept
{
    this->asyncConnectionCallback = v;
}

std::vector< elEasyTCPMessaging_Server::receivedMessage_t >
elEasyTCPMessaging_Server::BlockingReceiveMessages() noexcept
{
    auto messageCounter = this->messageReceivedTrigger.Get();

    std::vector< receivedMessage_t > messages = this->TryReceiveMessages();

    if ( messages.empty() )
    {
        this->messageReceivedTrigger.BlockingWait(
            [&]( const uint64_t &n ) { return n > messageCounter; } );
        messages = this->TryReceiveMessages();
    }

    return messages;
}

std::vector< elEasyTCPMessaging_Server::receivedMessage_t >
elEasyTCPMessaging_Server::TryReceiveMessages() noexcept
{
    std::vector< receivedMessage_t > returnValue;

    for ( size_t k = 0, limit = this->connectionVector.size(); k < limit; ++k )
    {
        auto &c       = this->connectionVector[k];
        auto  message = c->connection->TryReceive( c->handle );
        if ( message.has_value() )
            returnValue.emplace_back( receivedMessage_t{ k, message.value() } );
    }

    return returnValue;
}

std::vector< size_t >
elEasyTCPMessaging_Server::RefreshAndReturnConnections() noexcept
{
    std::vector< size_t > returnValue;

    for ( size_t k = 0, limit = this->connectionVector.size(); k < limit; ++k )
    {
        auto &c = this->connectionVector[k];
        if ( c->hasConnection )
        {
            auto connectionList = c->connection->GetConnections();
            if ( connectionList.empty() )
            {
                this->Disconnect( k );
            }
            else
            {
                returnValue.emplace_back( k );
            }
        }
    }

    return returnValue;
}

void
elEasyTCPMessaging_Server::Disconnect( const size_t connectionID ) noexcept
{
    auto &c = this->connectionVector[connectionID];
    LOG_DEBUG( 0 ) << "ETM server [" << this->serviceID << ":"
                   << this->serviceVersion << "] closing connection";

    c->hasConnection = false;
    c->connection->SetOnConnectCallback(
        elNetworkSocket_Server::connectCallback_t() );
    c->connection->StopListen();
    this->freeConnectionIDs->Push( connectionID );
}


} // namespace network
} // namespace el3D
