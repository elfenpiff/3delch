#include "network/elEasyTCPMessaging_Client.hpp"

#include "concurrent/elThreadPool.hpp"
#include "logging/elLog.hpp"
#include "network/etm/Identify_t.hpp"
#include "network/etm/Request_t.hpp"
#include "network/etm/Transmission_t.hpp"

namespace el3D
{
namespace network
{

elEasyTCPMessaging_Client::elEasyTCPMessaging_Client() noexcept
    : activeObject( concurrent::elThreadPool::GetGlobalThreadPool() )
{
}

bool
elEasyTCPMessaging_Client::IsServiceAvailableAt(
    const std::string &hostname, const uint16_t port ) const noexcept
{
    return this->connection.IsPortOpen( hostname, port );
}

std::optional< elEasyTCPMessaging_Client::identify_t >
elEasyTCPMessaging_Client::GetServerIdentification(
    const std::string &hostname, const uint16_t port ) noexcept
{
    if ( !this->connection.Connect( hostname, port ).get().HasError() )
    {
        etm::Transmission_t< etm::Request_t< etm::Identify_t::Request > >
            request( 0xaffeu, etm::MessageType::Request );
        request.payload.payload.id =
            static_cast< uint32_t >( etm::RequestID::Identify );

        if ( this->connection
                 .Send( utils::elByteSerializer()
                            .Serialize( utils::Endian::Big, request )
                            .GetByteStream() )
                 .HasError() )
            return std::nullopt;

        auto rawAnswer = this->connection.TimedReceive( this->timeout );
        if ( !rawAnswer.has_value() )
        {
            LOG_WARN( 0 ) << "server " << hostname << ":" << port
                          << " did not respond to identification request";
            return std::nullopt;
        }

        etm::Transmission_t< etm::Request_t< etm::Identify_t::Response > >
            answer;
        if ( utils::elByteDeserializer( *rawAnswer )
                 .Extract( utils::Endian::Big, answer )
                 .HasError() )
        {
            LOG_ERROR( 0 ) << "server " << hostname << ":" << port
                           << " did send invalid identification response";
            return std::nullopt;
        }

        return std::make_optional< identify_t >(
            { answer.payload.payload.payload.id,
              answer.payload.payload.payload.service_protocol_version,
              answer.payload.payload.payload.protocol_version } );
    }

    LOG_WARN( 0 ) << "unable to connect to " << hostname << ":" << port
                  << " to acquire server identification";

    return std::nullopt;
}

std::shared_future< bool >
elEasyTCPMessaging_Client::Connect( const std::string &hostname,
                                    const uint16_t     port ) noexcept
{
    return this->activeObject
        .AddTaskWithReturn< bool >(
            [this, hostname, port]
            { return this->ConnectImplementation( hostname, port ); } )
        .value();
}

void
elEasyTCPMessaging_Client::Disconnect() noexcept
{
    this->hasEstablishedPermanentConnection = false;
    LOG_DEBUG( 0 ) << "disconnecting";
    this->connection.Disconnect();
}

bool
elEasyTCPMessaging_Client::ConnectImplementation( const std::string &hostname,
                                                  const uint16_t port ) noexcept
{
    this->hasEstablishedPermanentConnection = false;
    this->connection.Disconnect();

    if ( !this->connection.Connect( hostname, port ).get() )
    {
        LOG_ERROR( 0 ) << "no etm service running on " << hostname << ":"
                       << port;
        return false;
    }
    etm::Transmission_t< etm::Request_t< etm::Connect_t::Request > > request{
        0u, etm::MessageType::Request };
    request.payload.payload.id =
        static_cast< uint32_t >( etm::RequestID::Connect );

    if ( this->connection
             .Send( utils::elByteSerializer()
                        .Serialize( utils::Endian::Big, request )
                        .GetByteStream() )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "unable to send connection request to " << hostname
                       << ":" << port;
        this->connection.Disconnect();
        return false;
    }

    auto answer = this->connection.TimedReceive( this->timeout );
    this->connection.Disconnect();

    if ( !answer.has_value() || answer->stream.empty() )
    {
        LOG_ERROR( 0 ) << "received no response to connection request from "
                       << hostname << ":" << port;
        return false;
    }

    etm::Transmission_t< etm::Request_t< etm::Connect_t::Response > >
        connectAnswer;

    if ( utils::elByteDeserializer( *answer )
             .Extract( utils::Endian::Big, connectAnswer )
             .HasError() )
    {
        LOG_ERROR( 0 ) << "received invalid answer to connection request from "
                       << hostname << ":" << port;
        return false;
    }

    uint16_t permanentConnectionPort =
        connectAnswer.payload.payload.payload.port;
    if ( !this->connection.Connect( hostname, permanentConnectionPort ).get() )
    {
        LOG_ERROR( 0 )
            << "unable to establish connection to given permanent port "
            << permanentConnectionPort << " from " << hostname << ":" << port;
        return false;
    }

    LOG_DEBUG( 0 ) << "established connection to ETM server at " << hostname
                   << ":" << port;
    this->hasEstablishedPermanentConnection = true;
    return true;
}

bool
elEasyTCPMessaging_Client::IsConnected() const noexcept
{
    return this->connection.IsConnected() &&
           this->hasEstablishedPermanentConnection;
}

units::Time
elEasyTCPMessaging_Client::GetTimeout() const noexcept
{
    return this->timeout;
}

void
elEasyTCPMessaging_Client::SetTimeout( const units::Time &timeout ) noexcept
{
    this->timeout = timeout;
}

} // namespace network
} // namespace el3D
