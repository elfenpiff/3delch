#include "math/math.hpp"

#include <glm/gtx/quaternion.hpp>

namespace el3D
{
namespace math
{
glm::vec3
ClosestPoint( const glm::vec3 rayOrigin, const glm::vec3 rayDirection,
              const glm::vec3 point ) noexcept
{
    glm::vec3 rp                 = point - rayOrigin;
    float     rayDirectionLength = glm::length( rayDirection );

    float distanceToClosesPoint =
        glm::dot( rp, rayDirection ) / rayDirectionLength;

    return rayOrigin +
           distanceToClosesPoint / rayDirectionLength * rayDirection;
}

glm::vec3
Rotate( const glm::vec3& v, const glm::quat& q ) noexcept
{
    glm::vec3 u( q.x, q.y, q.z );
    return 2.0f * glm::dot( u, v ) * u + ( q.w * q.w - glm::dot( u, u ) ) * v +
           2.0f * q.w * cross( u, v );
}

glm::vec3
Rotate( const glm::vec3& v, const glm::vec4 rotation ) noexcept
{
    return Rotate( v, glm::quat( glm::vec3( rotation.x * rotation.w,
                                            rotation.y * rotation.w,
                                            rotation.z * rotation.w ) ) );
}

} // namespace math
} // namespace el3D
