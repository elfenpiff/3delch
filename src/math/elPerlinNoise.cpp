#include "math/elPerlinNoise.hpp"

#include <numeric>

namespace el3D
{
namespace math
{
elPerlinNoise::elPerlinNoise( const uint32_t seed ) noexcept
{
    std::iota( std::begin( this->hash ),
               std::begin( this->hash ) + HASH_LENGTH / 2, 0 );
    std::shuffle( std::begin( this->hash ),
                  std::begin( this->hash ) + HASH_LENGTH / 2,
                  std::default_random_engine( seed ) );

    std::iota( std::begin( this->hash ) + HASH_LENGTH / 2,
               std::end( this->hash ), 0 );
}

double
elPerlinNoise::Noise( const double x, const double y,
                      const double z ) const noexcept
{
    int64_t xHash = static_cast< int64_t >( floor( x ) ) & 255;
    int64_t yHash = static_cast< int64_t >( floor( y ) ) & 255;
    int64_t zHash = static_cast< int64_t >( floor( z ) ) & 255;

    int64_t a  = this->hash[xHash] + yHash;
    int64_t aa = this->hash[a] + zHash;
    int64_t ab = this->hash[a + 1] + zHash;
    int64_t b  = this->hash[xHash + 1] + yHash;
    int64_t ba = this->hash[b] + zHash;
    int64_t bb = this->hash[b + 1] + zHash;

    double xFract = Fract( x );
    double yFract = Fract( y );
    double zFract = Fract( z );

    double u = Fade( xFract );
    double v = Fade( yFract );
    double w = Fade( zFract );

    return Lerp(
        w,
        Lerp( v,
              Lerp( u, Grad( this->hash[aa], xFract, yFract, zFract ),
                    Grad( this->hash[ba], xFract - 1, yFract, zFract ) ),
              Lerp( u, Grad( this->hash[ab], xFract, yFract - 1, zFract ),
                    Grad( this->hash[bb], xFract - 1, yFract - 1, zFract ) ) ),
        Lerp(
            v,
            Lerp( u, Grad( this->hash[aa + 1], xFract, yFract, zFract - 1 ),
                  Grad( this->hash[ba + 1], xFract - 1, yFract, zFract - 1 ) ),
            Lerp( u, Grad( this->hash[ab + 1], xFract, yFract - 1, zFract - 1 ),
                  Grad( this->hash[bb + 1], xFract - 1, yFract - 1,
                        zFract - 1 ) ) ) );
}

double
elPerlinNoise::AccumulatedOctaveNoise( const uint64_t octaves, const double x,
                                       const double y,
                                       const double z ) const noexcept
{
    double result = 0;
    double amp    = 1;
    double xPart = x, yPart = y, zPart = z;

    for ( uint64_t i = 0; i < octaves; ++i )
    {
        result += this->Noise( xPart, yPart, zPart ) * amp;
        xPart *= 2;
        yPart *= 2;
        zPart *= 2;
        amp /= 2;
    }

    return result;
}

double
elPerlinNoise::NormalizedOctaveNoise( const uint64_t octaves, const double x,
                                      const double y,
                                      const double z ) const noexcept
{
    return this->AccumulatedOctaveNoise( octaves, x, y, z ) /
           elPerlinNoise::Weight( octaves );
}

double
elPerlinNoise::Weight( const uint64_t octaves ) noexcept
{
    double amp   = 1.0;
    double value = 0.0;

    for ( uint64_t i = 0; i < octaves; ++i )
    {
        value += amp;
        amp /= 2;
    }

    return value;
}

double
elPerlinNoise::Grad( const uint8_t hash, const double x, const double y,
                     const double z ) noexcept
{
    const uint8_t h = hash & 15;
    const double  u = h < 8 ? x : y;
    const double  v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ( ( h & 1 ) == 0 ? u : -u ) + ( ( h & 2 ) == 0 ? v : -v );
}


} // namespace math
} // namespace el3D
