#include "math/RayTriangleIntersection.hpp"

namespace el3D
{
namespace math
{
bool
DoesRayIntersectTriangle( const glm::vec3 &                 rayOrigin,
                          const glm::vec3 &                 rayDirection,
                          const std::array< glm::vec3, 3 > &triangle ) noexcept
{
    constexpr float epsilon      = 0.000001f;
    glm::vec3       edge1        = triangle[1] - triangle[0];
    glm::vec3       edge2        = triangle[2] - triangle[0];
    glm::vec3       pvec         = glm::cross( rayDirection, edge2 );
    float           determinante = glm::dot( edge1, pvec );
    if ( -epsilon < determinante && determinante < epsilon ) return false;

    float     inverseDeterminate = 1.0f / determinante;
    glm::vec3 tvec               = rayOrigin - triangle[0];
    float     u                  = glm::dot( tvec, pvec ) * inverseDeterminate;
    if ( u < 0.0f || u > 1.0f ) return false;

    glm::vec3 qvec = glm::cross( tvec, edge1 );
    float     v    = glm::dot( rayDirection, qvec ) * inverseDeterminate;
    if ( v < 0.0f || u + v > 1.0f ) return false;

    return true;
}

bool
DoesRayIntersectTriangleWithCulling(
    const glm::vec3 &rayOrigin, const glm::vec3 &rayDirection,
    const std::array< glm::vec3, 3 > &triangle ) noexcept
{
    constexpr float epsilon      = 0.000001f;
    glm::vec3       edge1        = triangle[1] - triangle[0];
    glm::vec3       edge2        = triangle[2] - triangle[0];
    glm::vec3       pvec         = glm::cross( rayDirection, edge2 );
    float           determinante = glm::dot( edge1, pvec );
    if ( determinante < epsilon ) return false;

    glm::vec3 tvec = rayOrigin - triangle[0];
    float     u    = glm::dot( tvec, pvec );
    if ( u < 0.0f || u > determinante ) return false;

    glm::vec3 qvec = glm::cross( tvec, edge1 );
    float     v    = glm::dot( rayDirection, qvec );
    if ( v < 0.0f || u + v > determinante ) return false;

    return true;
}
} // namespace math
} // namespace el3D
